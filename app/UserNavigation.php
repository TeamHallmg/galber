<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNavigation extends Model
{
    protected $table = 'user_navigation';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'method', 'path', 'ip_address', 'prefix'
    ];

    public function toHumanReadable() {
        $message = '';
        switch($this->method) {
            case 'GET':
                $message.= 'Consultó ';
            break;
            case 'POST':
                $message.= 'Creó ';
            break;
            case 'PUT':
                $message.= 'Actualizó ';
            break;
            case 'DELETE':
                $message.= 'Eliminó ';
            break;
            default:
                $message.= $this->method;
            break;
        }
        switch($this->path) {
            case 'cumplea%C3%B1os':
                $message.= 'cumpleaños';
            break;
            case 'proximos-eventos':
                $message.= 'próximos eventos';
            break;
            default:
                $message.= $this->path;
            break;
        }

        return $message;
    }
}
