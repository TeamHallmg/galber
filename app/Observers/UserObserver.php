<?php

namespace App\Observers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserObserver
{

    public function updating(User $user)
    {
        try {
            $data['session_user'] = [
                'id' => Auth::user()->id,
                'name' => Auth::user()->FullName,
                'email' => Auth::user()->email,
            ];
            $data['user_updating'] = [
                'id' => $user->id,
                'name' => $user->FullName,
                'email' => $user->email,
            ];
            $data['user_updating']['changes'] = [];
            foreach ($user->getDirty() as $key => $value) {
                if($key != 'remember_token'){
                    $data['user_updating']['changes'][] = [
                        'old' . $key => $user->getOriginal($key),
                        'new' . $key => $value,
                    ];
                }
            }
            $data['request'] = [
                'url' => request()->fullUrl(),
                'method' => request()->method(),
                'ip' => request()->ip(),
                'date' => date('Y-m-d H:i:s'),
            ];
            if(count($data['user_updating']['changes'])){
                $data = json_encode($data);            
                Storage::disk('local')->append('user_update_log', $data);
            }
        } catch (\Throwable $th) {}

    }
}