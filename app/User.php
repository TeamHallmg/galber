<?php

namespace App;

use DB;
use App\Employee;
use Carbon\Carbon;
use App\Models\Role;
use App\Models\Permission;
use App\Models\Profile\Profile;
use App\Models\Scale\ModelData;
use App\Models\Scale\Plan;
use App\Models\Vacations\Event;
use App\Models\Vacations\Balances;
use App\Models\Vacations\Incident;
use App\Models\NotificationForUser;
use App\Models\Vacations\ClockLog;;
use Illuminate\Notifications\Notifiable;
use App\Models\ClimaOrganizacional\Period;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ClimaOrganizacional\PeriodUser;
use App\Notifications\ResetPasswordNotification;
use App\Models\Moodle\Enrol;
use App\Models\Moodle\User as MoodleUser;
use App\Models\Moodle\RoleAssigment;
use App\Models\Moodle\UserEnrolment;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id', 'first_name', 'last_name', 'email', 'password', 'role', 'active', 'external',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
    
    public function getFullNameAttribute(){
        return $this->first_name . ' ' . $this->last_name;
    }
    
    public function isSuperAdmin() {
        return in_array($this->email, ['soporte@hallmg.com'])?true:false;
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    public function employee_wt()
    {
        return $this->belongsTo(Employee::class, 'employee_id')->withTrashed();
    }

    public function balances()
    {
        return $this->hasMany(Balances::class);
    }

    public function vacationBalances(){
        return $this->hasMany(Balances::class)->whereHas('benefit', function($q){
            $q->where('name', 'Vacaciones');
        });
    }

    public function incidents()
    {
        return $this->hasMany(Incident::class, 'from_id', 'id');
    }

    public function events()
    {
        return $this->hasMany(Event::class, 'user_id', 'id');
    }

    public function periods()
    {
        return $this->belongsToMany(Period::class,
            with(new PeriodUser)->getTable(),
            'user_id',
            'period_id')
            ->withPivot('status');
    }

    public function userPermissions()
    {
        return $this->belongsToMany(Permission::class, 'permission_user', 'user_id', 'permission_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }

    public function clocklogs()
    {
        return $this->hasMany(ClockLog::class);
    }

    public function notificationMessages()
    {
        return $this->hasMany(NotificationForUser::class, 'user_id', 'id');
    }

    public function getMyPermission()
    {
        $id = $this->id;
        $rolePermissions = Permission::whereHas('roles', function ($q) use ($id) {
            $q->whereHas('users', function ($q1) use ($id) {
                $q1->where('users.id', $id);
            });
        })->pluck('id', 'action')->toArray();

        $userPermissions = Permission::whereHas('users', function ($q) use ($id) {
            $q->where('users.id', $id);
        })->pluck('id', 'action')->toArray();

        $permissions = array_merge($rolePermissions, $userPermissions);
        return $permissions;
    }

    public function hasPermissionOfModule($module){
        $user_p = $this->userPermissions()->where('permissions.module', 'Incidencias')->exists();
        $role_p = $this->roles()
        ->whereHas('rolePermissions', function ($q) use ($module) {
            $q->where('module', $module);
        })
        ->exists();
        return $user_p || $role_p;
    }

    public function hasRolePermission($permission)
    {
        return $this->roles()
        ->whereHas('rolePermissions', function ($q) use ($permission) {
            $q->where('action', $permission);
        })
        ->exists();
    }

    public function hasPermission($permission)
    {
        return $this->userPermissions()
            ->where('action', $permission)
            ->exists();
    }

    public function hasAccess($permission){
        return $this->hasRolePermission($permission) || $this->hasPermission($permission);
    }

    public function isAdminOrHasRolePermission($permission) {
        if($this->role == 'admin' || $this->hasRolePermission($permission)) {
            return true;
        } else {
            return false;
        }
    }

    public function isAdmin() {
        if($this->role == 'admin') {
            return true;
        } else {
            return false;
        }
    }

    public function existsProfile()
    {
        $id = $this->id;
        $profile = Profile::where('user_id', $id)->exists();

        if ($profile) {
            return true;
        } else {
            return false;
        }
    }

    public function getEmployeePhoto(){

      $employeePhoto = 'img/vacantes/sinimagen.png';
      
      if (file_exists(getcwd() . '/img/empleados/' . $this->employee->idempleado . '.jpg')){

        $employeePhoto =  '/img/empleados/' . $this->employee->idempleado . '.jpg';
      }

      return $employeePhoto;
    }

    public function getProfileImage()
    {
        $profilePhoto = 'img/vacantes/sinimagen.png';
        if(class_exists(Profile::class)){
            $profile = Profile::where('user_id', $this->id)->first();
            if ($profile != null && !is_null($profile->image)) {
                $profilePhoto = 'uploads/profile/' . $profile->image;
            }
        }
        return $profilePhoto;
    }

    public function getProfileImagen() {
        $profile = Profile::where('user_id', $this->id)->first();
        $profilePhoto = 'img/vacantes/sinimagen.png';
        if($profile != null && !is_null($profile->image)) {
           
            if( \File::exists(public_path('uploads/profile/'.$profile->image)) ){
                $profilePhoto = 'uploads/profile/'.$profile->image;
            }

        }
        return $profilePhoto;
    }

    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_id');
    }

    /**
     * Obtener todos mis subordinados asignados a mi perfil de empleado
     *
     * @param  array $with Relaciones de modelo para usar con with()
     * @param  mixed $has       Relaciones de modelo para usar en Has()
     *
     * @return Illuminate\Support\Collection
     */
    public function getSubordinates($with = [], $has = '')
    {
        if ($this->employee) {
            return $this->employee->employees()->with($with)
                ->when(!empty($has), function ($q) use ($has) {
                    return $q->has($has);
                })
                ->get();
        }
        return collect([]);
    }

    /**
     * Obtenemos todas las IDs de empleado de mis subordinados
     *
     * @return array
     */
    public function mySubordinatesIDs()
    {
        $subordinates = $this->getSubordinates();
        return $subordinates->isNotEmpty() ? $subordinates->pluck('id')->toArray() : [];
    }

    /**
     * Obtenemos un array con los modelos de usario de mis subordinados
     *
     * @return array
     */
    public function getSubordinatesAsUsers()
    {
        $subordinates = $this->getSubordinates(['user'], 'user');
        $users = [];
        foreach ($subordinates as $subordinate) {
            $users[] = $subordinate->user;
        }
        return $users;
    }

    /**
     * Obtenemos las ID`s de los usuarios que sean mis subordinados
     *
     * @return void
     */
    public function getSubordinatesAsUsersIDs()
    {
        $subordinates = $this->getSubordinatesAsUsers();
        $users = [];
        foreach ($subordinates as $user) {
            $users[] = $user->id;
        }
        return $users;
    }

    public function getSubordinatesRegionIDs(){
        $subordinates = $this->getSubordinates(['region'], 'region');
        $regions = [];
        foreach ($subordinates as $subordinate) {
            if(!isset($regions[$subordinate->region->id]))
                $regions[$subordinate->region->id] = $subordinate->region->id;
        }
        return $regions;
    }

    public function getJobPositionID(){
        return $this->employee?$this->employee->getPuestoId():null;
    }

    public function getRegionID()
    {
        if($region = $this->getRegion()){
            return $region->id;
        }
        return null;
    }

    public function getRegion()
    {
        if ($this->employee && $this->employee->region) {
            return $this->employee->region;
        }
        return null;
    }

    public function isBoss(){
        return $this->employee && $this->employee->isBoss();
    }

    public function hasAuthorizator()
    {
        if ($this->employee && $this->employee->boss && $this->employee->boss->user) {
            return true;
        }
        return false;
    }

    public function getAuthorizator()
    {
        if ($this->hasAuthorizator()) {
            return $this->employee->boss->user;
        }
        return null;
    }

    public function getAuthorizatorID()
    {
        if ($boss = $this->getAuthorizator()) {
            return $boss->id;
        }
        return null;
    }

    public function isDayWorkable($date)
    {
        if ($this->getRegion()) {
            return $this->employee->region->isDayWorkable($date);
        }
        return false;

    }

    public function getSchedule($begin, $end = null, $whole_week = false)
    {
        if ($this->getRegion()) {
            return $this->employee->region->getSchedule($begin, $end, $whole_week);
        }
        return null;
    }

    public function getDaysCountInMySchedule($begin, $end, $onlyLaboral = false)
    {
        if ($this->getRegion()) {
            return $this->employee->region->getDays($begin, $end, $onlyLaboral)->count();
        }
        return 0;
    }

    public function getYears($date = null){
        $in = Carbon::createFromFormat('Y-m-d', $this->employee->ingreso);
        $today = Carbon::createFromFormat('Y-m-d', $date?date('Y-m-d', $date):date('Y-m-d'));
        return $in->diffInYears($today);
    }

    public function getSeniorityDays($date = null){
        return $this->employee?$this->employee->getSeniorityDays($date):null;
    }

    public function getMyNextYear($date = null)
    {
        $time  = strtotime($this->employee->ingreso);
        $day   = date('d',$time);
        $month = date('m',$time);
        $year  = date('Y');
        $today = (is_null($date))?strtotime(date('Y-m-d')):$date;
        $when  = mktime(0,0,0,$month,$day,$year);
        
        if($when > $today) {
            return($year);
        } else {
            return($year+1);
        }
    }

    public function getMyVacations($date = null)
    {
        $currDate = (is_null($date))?strtotime(date('Y-m-d')):$date;
        $datetime1 = date_create(date('Y-m-d', $currDate));
        $datetime2 = date_create($this->employee->ingreso);
        $interval = date_diff($datetime1, $datetime2);
        $y = $interval->format('%y');
        $m = $interval->format('%m');
        $d = $interval->format('%d');
        
        $t = 6;
        $t+= (($y >= 1 && ( ( $d > 0 && $m >= 0 ) || ($d >= 0 && $m > 0) ))|| $y >= 2 )?2:0;
        $t+= (($y >= 2 && ( ( $d > 0 && $m >= 0 ) || ($d >= 0 && $m > 0) ))|| $y >= 3 )?2:0;
        $t+= (($y >= 3 && ( ( $d > 0 && $m >= 0 ) || ($d >= 0 && $m > 0) ))|| $y >= 4 )?2:0;
        $m = floor($y/5);
        $t += ($m * 2);
        return($t);
    }

    public function getWorkStartTime($date){
        if($this->getSchedule($date) && $this->getSchedule($date)){
            return $this->getSchedule($date)->schedule->in;
        }
        return null;
    }

    public function getWorkEndTime($date){
        if($this->getSchedule($date) && $this->getSchedule($date)){
            return $this->getSchedule($date)->schedule->out;
        }
        return null;
    }

    public function getWeekWorkMinutes(){
        return $this->employee?$this->employee->getWeekWorkMinutes():null;
    }
    
    /**
     * Find attr in employee relation if exists
     *
     * @param  string $attr existing in employee fields
     * @return string
     */
    public function getEmployeeAttr($attr){
        return $this->employee && $this->employee->$attr?$this->employee->$attr:'';
    }

    public function getScaleEnrolledCourses() {
        return ModelData::where([
            'model_name' => 'User',
            'model_id' => $this->id,
            'scale_data_type' => 'course' // Scale Course
        ])->get();
    }

    public function getScaleEnrolledCoursesIDs() {
        return ModelData::select('scale_data_id')->where([
            'model_name' => 'User',
            'model_id' => $this->id,
            'scale_data_type' => 'course' // Scale Course
        ])->distinct('scale_data_id')->pluck('scale_data_id')->toArray();
    }

    public function getMoodleEnrolledCourses($with_grades = false){
        $mdl_user = MoodleUser::where('icq', $this->id)->first();
        $role_assigment_course = [];
        if($mdl_user){
            $role_assigment_course = [];
            $role_assigment = RoleAssigment::with('context.instance')
            ->where('roleid' , 5)
            ->where('userid', $mdl_user->id)
            ->whereHas('context', function($q){
                $q->where('contextlevel', 50);
                // ->whereIn('instances', $courses)
            })
            ->get();

            foreach($role_assigment as $assigment){
                $course_id = $assigment->context->instance->id;
                if(!$with_grades) {
                    $role_assigment_course[$course_id] = $assigment->context->instance->fullname;
                } else {
                    $grade = self::getUserCourseGrade($course_id, $this->id);
                    if(!empty($grade)) {
                        $grade = $grade[0];
                    }
                    $role_assigment_course[$course_id] = [
                        'name' => $assigment->context->instance->fullname,
                        'grade' => $grade?$grade->grade:0
                    ];
                }
            }
        }
        return $role_assigment_course;
    }

    public function getUserCourseGrade($course_id, $user_id) {
        $minApproveGrade = 80-1;
        $moodleDB = env('DB_DATABASE_SECOND', 'moodle_db');
        $query = "
            SELECT
                gg.finalgrade, ROUND(gg.finalgrade / gg.rawgrademax * 100 ,2) AS 'grade', u.id as 'userid', c.id AS 'courseid',
                IF (ROUND(gg.finalgrade / gg.rawgrademax * 100 ,2) > $minApproveGrade, true , false) AS 'pass'
                
            FROM
                $moodleDB.mdl_course AS c
            JOIN
                $moodleDB.mdl_context AS ctx ON c.id = ctx.instanceid
            JOIN
                $moodleDB.mdl_role_assignments AS ra ON ra.contextid = ctx.id
            JOIN
                $moodleDB.mdl_user AS u ON u.id = ra.userid
            JOIN
                $moodleDB.mdl_grade_grades AS gg ON gg.userid = u.id
            JOIN
                $moodleDB.mdl_grade_items AS gi ON gi.id = gg.itemid
                
            WHERE
                gi.courseid = c.id AND gi.itemtype = 'course' AND c.id = $course_id AND u.icq = $user_id
        ";

        return DB::connection('moodle_mysql')->select($query);
    }

    public function courseModelData($user_id, $moodle_course_id) {
        $model_data = ModelData::where([
            'model_name' => 'User',
            'model_id' => $user_id,
            'scale_data_type' => 'moodle_course',
            'scale_data_id' => $moodle_course_id
        ])->first();

        return $model_data;
    }

    public function getScaleCoursePrice($moodle_course_id) {
        $model_data = self::courseModelData($this->id, $moodle_course_id);

        if($model_data) {
            if($model_data->extra2) {
                return ('$ '. number_format($model_data->extra2, 2));
            } else {
                return ('$ '. number_format(0, 2));
            }
        }
        return null;
    }

    public function getScalePlanName($moodle_course_id) {
        $model_data = self::courseModelData($this->id, $moodle_course_id);

        if($model_data) {
            $plan = Plan::find($model_data->extra1);
            if($plan) {
                return $plan->name;
            }
        }
        return null;
    }

    public function getScalePlanPrice($moodle_course_id) {
        $model_data = self::courseModelData($this->id, $moodle_course_id);

        if($model_data) {
            return Plan::find($model_data->extra2);
        }
        return 0;
    }

    public function getFormattedScalePlanPrice($moodle_course_id) {
        return '$ '. number_format($this->getScalePlanPrice($moodle_course_id), 2);
    }
}
