<?php

namespace App;

use App\Models\Region;
use App\Models\JobPosition;
use App\Models\Department;
use App\Models\Enterprise;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employees';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idempleado', 'nombre', 'paterno', 'materno', 'fuente', 'rfc', 'curp', 'nss', 'correoempresa', 'correopersonal', 'nacimiento', 'sexo', 'civil', 'telefono', 'extension', 'celular', 'ingreso', 'fechapuesto', 'jefe', 'direccion', 'department', 'seccion', 'job_position_id', 'grado', 'region_id', 'sucursal', 'enterprise_id', 'division', 'marca', 'centro', 'checador', 'turno', 'tiponomina', 'clavenomina', 'nombrenomina', 'generalista', 'relacion', 'contrato', 'horario', 'jornada', 'calculo', 'vacaciones', 'flotante', 'base', 'rol', 'password', 'extra1', 'extra2', 'extra3', 'extra4', 'extra5', 'fecha', 'version', 'external'];

    public function getFullNameAttribute(){
        return $this->nombre . ' ' . $this->paterno . ' ' . $this->materno;
    }

    public function getLastNamesAttribute(){
        return $this->paterno . ' ' . $this->materno;
    }

    public function getPuestoName($trashed = false){
        $job = $trashed?$this->jobPosition:$this->jobPosition_wt;
        return $job?$job->name:'-';
    }

    public function getPuestoId($trashed = false){
        $job = $trashed?$this->jobPosition:$this->jobPosition_wt;
        return $job?$job->id:'';
    }

    public function getDepartmentName($trashed = false){
        if($department = $this->department($trashed)){
            return $department->name;
        }
        return "";
    }

    public function getDepartmentId($trashed = false){
        if($department = $this->department($trashed)){
            return $department->id;
        }
        return "";
    }

    public function fixed_department() {
        return $this->belongsTo(Department::class, 'division', 'id');
    }

    public function getSucursal() {
        return strtoupper($this->sucursal);
    }

    public function enterprise(){
        return $this->belongsTo(Enterprise::class, 'enterprise_id', 'id');
    }
    public function getEmpresaName(){
        return $this->enterprise?$this->enterprise->name:'N/A';
    }

    public function getNacimiento($format = ''){
        return $this->nacimiento?Carbon::createFromFormat('Y-m-d', $this->nacimiento)->format($format?$format:$this->defaultDateFormat):'';
    }

    public function getNacimientoLocalized(){
        if($date = $this->getNacimiento()){
            $date = Carbon::createFromFormat('Y-m-d', $this->nacimiento)->formatLocalized('%d de %B');
            return $date;
        }
        return '';
    }
    
    public function getIngreso($format = ''){
        return $this->ingreso?Carbon::createFromFormat('Y-m-d', $this->ingreso)->format($format?$format:$this->defaultDateFormat):'';
    }

    public function getCarbon($field){
        return $this->{$field}?Carbon::createFromFormat('Y-m-d', $this->{$field}):null;
    }

    public function user()
    {
        return $this->hasOne(User::class, 'employee_id');
    }

    public function employees()
    {
        return $this->hasMany(Employee::class, 'jefe', 'idempleado');
    }
  
    public function boss(){
        return $this->hasOne(Employee::class, 'idempleado', 'jefe');
    }

    public function boss_wt(){
        return $this->hasOne(Employee::class, 'idempleado', 'jefe')->withTrashed();
    }

    public function jobPosition(){
        return $this->belongsTo(JobPosition::class, 'job_position_id', 'id');
    }

    public function jobPosition_wt(){
        return $this->belongsTo(JobPosition::class, 'job_position_id', 'id')->withTrashed();
    }
    
    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id', 'id');
    }

    public function isBoss(){
        return $this->employees->isNotEmpty();
    }

    public function area($trashed = false){
        if($job = $trashed?$this->jobPosition_wt:$this->jobPosition){
            return $trashed?$job->area_wt:$job->area;
        }
        return null;
    }

    public function department($trashed = false){
        if($area = $this->area($trashed)){
            return $trashed?$area->department_wt:$area->department;
        }
        return null;
    }

    public function direction($trashed = false){
        if($department = $this->department($trashed)){
            return $trashed?$department->direction_wt:$department->direction;
        }
        return null;
    }

    public function getAge() {
        $calculo = Carbon::parse($this->fechanacimiento)->age;

        return ($calculo == 0?'N/A': $calculo);
    }

    public function getSeniorityDays($date = null){
        $today = $date?Carbon::createFromFormat('Y-m-d', $date):Carbon::now();
        $ingreso = $this->getCarbon('ingreso');
        return $this->getCarbon('ingreso')->diffInDays($today);
    }
    public function getWeekWorkMinutes(){
        return $this->region?$this->region->getWeekWorkMinutes():null;
    }
}
