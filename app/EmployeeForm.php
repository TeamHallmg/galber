<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeForm extends Model
{
    protected $table = 'employees_form';

    protected $fillable = [
        'name', 'name_show', 'active', 'mandatory',
    ];
}
