<?php

namespace App\Handlers;

use Exception;
use App\User;
use App\Models\Vacations\Incident;

class UserHandler{
    
    protected $user;
    
    public function __construct(User $user){
        $this->user = $user;
        $this->user->load('employee');
    }

    public function updateIncidentData($old_boss){
        $boss = $this->user->getAuthorizator();
        if($boss){
            try {
                Incident::where('from_id', $this->user->id)
                ->where('status', 'pending')
                ->update(['user_id' => $old_boss]);
            } catch (\Throwable $th) {
                throw $th;
            }
        }
    }
}