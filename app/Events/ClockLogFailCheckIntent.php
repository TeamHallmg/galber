<?php

namespace App\Events;

use App\User;
use Carbon\Carbon;
use App\Models\Vacations\Incident;
use Illuminate\Broadcasting\Channel;
use App\Http\Helpers\ClockLogManager;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ClockLogFailCheckIntent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $problem;
    public $date;
    protected $clocklogManager;
    public $reason;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, $problem, $date = null)
    {
        $this->clocklogManager = new ClockLogManager();
        $this->user = $user;
        $this->problem = $problem;
        $this->date = $date?Carbon::createFromFormat('Y-m-d H:i:s', $date):Carbo::now();
        $this->reason = $this->getFailReason($problem);
    }

    public function getFailReason($problem){
        switch($problem){
            case 'no-labor':
                return $this->defaultMessage("No labora este día en base a su horario definido");
            break;
            case 'incident-blocked':
                return $this->getIncidentMessage();
            break;
            case 'event-blocked':
                return $this->getEventMessage();
            break;
        }
    }

    public function defaultMessage($addMessage){
        $msg = "El empleado '{$this->user->FullName}' intento checar hoy a las {$this->date->format('H:i')}";
        if($addMessage){
            $msg .= " - " . $addMessage;
        }
        return $msg;
    }

    public function getIncidentMessage(){
        $incident = $this->clocklogManager->isDayAnDayIncident($this->user, $this->date->format('Y-m-d'), false);
        return $this->defaultMessage("Hay una solicitud '{$incident->info}' bloquea este día");
    }

    public function getEventMessage(){
        $event = $this->clocklogManager->isDayBlockByEvent($this->user, $this->date->format('Y-m-d'), false);
        return $this->defaultMessage("El evento '{$event->title}' bloquea este día");
    }

}
