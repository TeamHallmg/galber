<?php

namespace App\Providers;

use App\User;
use App\Observers\UserObserver;
use App\Models\Vacations\Incident;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        User::observe(UserObserver::class);

        view()->composer('*', function ($view) 
        {
            if(Auth::check()) {
                $pending_incidents = Incident::whereHas('benefit', function($q) {
                    $q->where('group','benefit');
                    // $q->orWhere('group','incident');
                    // $q->orWhere('group','incapacity');
                })
                ->where('event_id','<>','0')
                ->where('user_id',Auth::user()->id)
                ->whereIn('status', ['pending'])
                ->orderBy('user_id')
                ->orderBy('created_at','DESC')->get();
                // Mejorar query porque trae incidencias de checadas

                $view->with('pending_incidents', $pending_incidents);
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
