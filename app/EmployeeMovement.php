<?php

namespace App;

use App\Models\JobPosition;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeMovement extends Model
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->withTrashed();
    }

    public function old_job_position()
    {
        return $this->belongsTo(JobPosition::class, 'old_job_position_id', 'id')->withTrashed();
    }

    public function new_job_position()
    {
        return $this->belongsTo(JobPosition::class, 'new_job_position_id', 'id')->withTrashed();
    }
}
