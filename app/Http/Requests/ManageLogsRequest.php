<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ManageLogsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'log_id' => ['required', Rule::exists('clock_logs', 'id')->whereIn('status', ['pending'])],
            'log_reason' => 'required',
            'incident_benefit' => 'sometimes',
        ];
    }

    public function messages()
    {
        return [
            'log_reason.required' => 'Es necesario escribir un motivo',
            'log_id.required' => 'ID is missign',
            'log_id.exists' => 'No se puede trabajar con este registro',
        ];
    }
}
