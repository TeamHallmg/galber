<?php

namespace App\Http\Requests;

use App\Models\Vacations\Benefits;
use Illuminate\Foundation\Http\FormRequest;

class IncidentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'benefit_id' => 'required|exists:benefits,id',
            'title' => 'sometimes',
            'start' => 'required|date',
            'end' => 'required|date|after_or_equal:start',
        ];
        $benefit = Benefits::with('timeDescription')->find($this->benefit_id);
        if($benefit && $benefit->type === 'time')
            $rules += ['time' => 'required|integer|min:1|max:' . ($benefit->getMaxTimePerRequest()?$benefit->getMaxTimePerRequest():'8')];
        
        if($benefit && $benefit->week_calendar)
            $rules += ['date' => 'required|date|after_or_equal:start|before_or_equal:end'];

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'end.after_or_equal' => 'La fecha de fin debe ser el mismo día o después',
        ];
    }
}
