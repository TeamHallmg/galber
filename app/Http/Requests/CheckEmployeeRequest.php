<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CheckEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => ['required', Rule::exists('employees', 'idempleado')->whereNull('deleted_at')]
        ];
    }

    public function messages()
    {
        return [
            'number.required' => 'Es necesario proporcionar un número de empleado',
            'number.exists' => 'No encontramos coincidencias con el nímero de empleado que proporcionaste',
        ];
    }
}
