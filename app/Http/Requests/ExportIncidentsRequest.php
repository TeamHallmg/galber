<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExportIncidentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start' => 'required|date',
            'end' => 'required|date|after_or_equal:start',
            'process' => 'sometimes',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'start.required' => 'Al parecer no hay incidencias para exportar',
            'end.after_or_equal' => 'La fecha de fin debe ser el mismo día o después',
        ];
    }
}
