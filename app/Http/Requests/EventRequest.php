<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'blocked' => 'sometimes',
            'start' => 'required|date',
            'end' => 'required|date|after_or_equal:start',
        ];
        if($this->method() === 'POST'){
            $rules += [
                'benefit_id' => 'required|exists:benefits,id',
                'regions' => 'required|array',
                'regions.*' => 'required|exists:regions,id',
            ];
        }
        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'benefit_id.required' => 'Eliga el tipo de incidencia',
            'regions.required' => 'Eligar por lo menos un horario',
            'end.after_or_equal' => 'La fecha de fin debe ser el mismo día o después',
        ];
    }
}