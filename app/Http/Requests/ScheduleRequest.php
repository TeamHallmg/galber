<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'workshift' => ['required', Rule::in(['5x2'])],
            'labor' => 'required|array|min:1',
            'labor.*' => ['required', Rule::in(['1'])],
            'schedule' => 'required|array',
            'schedule.*' => 'nullable|exists:schedules,id',
        ];
        if($this->method() === 'POST'){
            $rules += ['name' => 'required|unique:regions,name'];
        }else{
            $rules += [
                'name' => [
                    'required',
                    Rule::unique('regions', 'name')->whereNull('deleted_at')->ignore($this->route('schedule'))
                ]
            ];
        }
        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.unique' => 'Ya existe un horario con este nombre',
            'labor.min' => 'Es necesario al menos :min días',
            'labor.required' => 'Elija al menos un día para laborar',
        ];
    }
}
