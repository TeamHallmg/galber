<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('admin_de_usuario');
        $user = User::has('employee')->findOrFail($id);
        return [
            "idempleado" => 'nullable',
            "nombre" => 'required',
            "paterno" => 'required',
            "materno" => 'nullable',
            "fuente" => 'nullable',
            "rfc" => ['required', Rule::unique('employees', 'rfc')->whereNull('deleted_at')->ignore($user->employee)],
            "curp" => ['required', Rule::unique('employees', 'curp')->whereNull('deleted_at')->ignore($user->employee)],
            "nss" => ['required', Rule::unique('employees', 'nss')->whereNull('deleted_at')->ignore($user->employee)],
            "correoempresa" => ['required', Rule::unique('employees', 'correoempresa')->whereNull('deleted_at')->ignore($user->employee)],
            "old_email" => ['required', Rule::unique('employees', 'correoempresa')->whereNull('deleted_at')->ignore($user->employee)],
            "correopersonal" => ['nullable', Rule::unique('employees', 'correopersonal')->whereNull('deleted_at')->ignore($user->employee)],
            "nacimiento" => 'required|date',
            "sexo" => ['required', Rule::in(['M', 'F'])],
            "civil" => 'required',
            "telefono" => 'nullable',
            "extension" => 'nullable',
            "celular" => 'nullable',
            "ingreso" => 'required|date',
            "fechapuesto" => 'nullable|date',
            "jefe" => 'nullable|exists:employees,idempleado',
            "direccion" => 'nullable',
            "job_position_id" => 'nullable|exists:job_positions,id',
            "seccion" => 'nullable',
            "grado" => 'nullable',
            "region_id" => 'nullable|exists:regions,id',
            "sucursal" => 'nullable',
            "enterprise_id" => 'nullable|exists:enterprises,id',
            "division" => 'nullable',
            "marca" => 'nullable',
            "centro" => 'nullable',
            "checador" => 'nullable',
            "turno" => 'nullable',
            "tiponomina" => 'nullable',
            "clavenomina" => 'nullable',
            "nombrenomina" => 'nullable',
            "generalista" => 'nullable',
            "relacion" => 'nullable',
            "contrato" => 'nullable',
            "horario" => 'nullable',
            "jornada" => 'nullable',
            "calculo" => 'nullable',
            "vacaciones" => 'nullable',
            "flotante" => 'nullable',
            "base" => 'nullable',
            "rol" => ['required', Rule::in(['employee'])],
            "password" => 'nullable',
            "oldPassword" => 'nullable',
            "extra1" => 'nullable',
            "extra2" => 'nullable',
            "extra3" => 'nullable',
            "extra4" => 'nullable',
            "extra5" => 'nullable',
            "fecha" => 'nullable|date',
        ];
    }
}
