<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if (!empty($permission)) {
            if(!Auth::user()->hasRolePermission($permission) && !Auth::user()->hasPermission($permission) && Auth::user()->role !== 'admin') {
                // abort(403, 'Unauthorized action.');
                flash('No tienes acceso a esta acción...')->error();
                return redirect('home');
            }
        }
        return $next($request);
    }
}
