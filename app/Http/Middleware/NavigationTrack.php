<?php

namespace App\Http\Middleware;

use DB;
use Closure;
use App\UserNavigation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class NavigationTrack
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            // dd($request->method(), $request->path(), $request->url(), $request->route()->getPrefix());
            DB::beginTransaction();
            $data = [
                'user_id' => null,
                'method' => $request->method(),
                'path' => $request->path(),
                'ip_address' => \Request::ip(),
                'prefix' => $request->route()->getPrefix()
            ];
            if(Auth::check()) {
                $data['user_id'] = Auth::user()->id;
            }
            UserNavigation::create($data);
            DB::commit();
        } catch(\Throwable $th) {
            DB::rollback();
        }

        return $next($request);
    }
}
