<?php
namespace App\Http\Controllers\EvaluacionDesempeno;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\EvaluacionDesempeno\EvaluadorEvaluado;
use App\User;
use App\Models\EvaluacionDesempeno\Admin\AreasGruposAreas;
use App\Models\EvaluacionDesempeno\Admin\Periodos;
use App\Models\EvaluacionDesempeno\Admin\PuestosNivelesPuestos;
use DB;
use Session;

class EvaluadoresEvaluadosController extends Controller
{
    /**
     * Create a new Evaluacion Desempeno controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

//Listado de las personas que va a evaluar al que esta logueado
    public function listaEvaluados(){

      if (Session::has('primer_login')){

        flash('Debes cambiar tu contraseña antes de continuar a otra sección');
        return redirect('/');
      }

      $tipos_evaluacion = DB::table('tipos_evaluadores')->pluck('descripcion_empresa', 'id')->toArray();
      $periodoStatus = Periodos::where('status', '=', 'Abierto')->get();

      if($periodoStatus->isEmpty()){
        return view('evaluacion-desempeno.evaluaciones', compact('periodoStatus'));
      }

      if($periodoStatus->count() == 1){
        $periodoAbierto = Periodos::where('status', '=', 'Abierto')->first();

        $evaluados = EvaluadorEvaluado::where('id_evaluador', '=', auth()->user()->id)->where('id_periodo', '=', $periodoAbierto->id)->get();

        return view('evaluacion-desempeno.evaluaciones', compact('evaluados', 'periodoStatus', 'tipos_evaluacion'));
      }

      if($periodoStatus->count() > 1){
        $evaluados = EvaluadorEvaluado::all();

        return view('evaluacion-desempeno.evaluaciones', compact('evaluados', 'periodoStatus', 'tipos_evaluacion'));
      }

      /*if(is_null($periodoAbierto)){
        $evaluados = "";
        return view('evaluacion-desempeno.evaluaciones', compact('evaluados'));
      }

      if($periodoAbierto->status == 'Abierto'){
        
        $evaluados = EvaluadorEvaluado::where('id_evaluador', '=', auth()->user()->id)->where('id_periodo', '=', $periodoAbierto->id)->get();

        return view('evaluacion-desempeno.evaluaciones', compact('evaluados'));
      }*/
      
    }

    /*Listado o semaforizaci�n en el �rea del admin para ver los status de todas los usuarios
    public function calificaciones(){
        $calificaciones = EvaluadorEvaluado::orderBy('id', 'ASC')->get(); 

        return view('evaluacion-desempeno.Admin.calificaciones', compact('calificaciones'));
    }*/

    // Formato de Evaluaci�n
    public function formatoEvaluacion($id_evaluado){

      if (Session::has('primer_login')){

        flash('Debes cambiar tu contraseña antes de continuar a otra sección');
        return redirect('/');
      }

      // Id del evaluador
      $id_evaluador = auth()->user()->id;
	  
	  $factores = array();
    $results = array();	
	  $periodos = DB::select('SELECT id FROM periodos WHERE status = ?', ['Abierto']);
	  $id_periodo = 0;
		
	  if (count($periodos) > 0){
		  
		$id_periodo = $periodos[0]->id;
	  }
		
	  $factores_periodo = DB::select("SELECT id_factor FROM periodo_factor WHERE id_periodo = ?", [$id_periodo]);

      foreach ($factores_periodo as $key => $value){
        
        $factores[] = $value->id_factor;
      }

      // Obtiene los datos del empleado
      $empleado = DB::select('SELECT users.id AS id_evaluado, users.first_name, users.last_name, employees.idempleado, employees.jefe, directions.name, grupo_areas.id AS grupo_area, niveles_puestos.* FROM users JOIN employees ON (employees.id = users.employee_id) LEFT JOIN job_positions ON (job_positions.id = employees.job_position_id) LEFT JOIN areas ON (areas.id = job_positions.area_id) LEFT JOIN departments ON (departments.id = areas.department_id) LEFT JOIN directions ON (directions.id = departments.direction_id) LEFT JOIN puesto_nivel_puesto ON (puesto_nivel_puesto.id_puesto = job_positions.id) LEFT JOIN niveles_puestos ON (niveles_puestos.id = puesto_nivel_puesto.id_nivel_puesto) LEFT JOIN departments_grupo_areas ON (departments_grupo_areas.id_departments = departments.id) LEFT JOIN grupo_areas ON (grupo_areas.id = departments_grupo_areas.id_grupos_areas) WHERE users.id = ?', [$id_evaluado]);

      // El empleado cuenta con un puesto y a la vez un nivel de puesto
      if (!empty($empleado[0]->nombre)){

        $id_nivel_puesto = $empleado[0]->id;

        // Obtiene los factores genericos
        //$results = DB::select('SELECT factores.id, factores.nombre, factores.descripcion, factores.comentarios, factores.orden, factores_niveles_dominios.nivel_dominio, factores_niveles_dominios.calificacion FROM familias_factores INNER JOIN factores ON (factores.id_familia = familias_factores.id) INNER JOIN factores_niveles_dominios ON (factores.id = factores_niveles_dominios.id_factor) WHERE familias_factores.nombre LIKE "%genericas%" ORDER BY factores.orden');

        $results = DB::select('SELECT factores.id, factores.nombre, factores.descripcion, factores.comentarios, factores.orden, factores_niveles_dominios.nivel_dominio, factores_niveles_dominios.id AS id_pregunta FROM familias_factores INNER JOIN factores ON (factores.id_familia = familias_factores.id) INNER JOIN factores_niveles_dominios ON (factores.id = factores_niveles_dominios.id_factor) INNER JOIN factores_niveles_puestos ON (factores_niveles_puestos.id_factor = factores.id) WHERE familias_factores.nombre LIKE ? AND factores_niveles_puestos.nivel_esperado != ? AND factores_niveles_puestos.id_nivel_puesto = ? ORDER BY factores.orden', ["%genericas%", 0, $id_nivel_puesto]);
		
	      $periodo_factores = array();

        foreach ($results as $key => $value){
        
          if (in_array($value->id, $factores)){

            $periodo_factores[] = $value;
          }
        }

        $results = $periodo_factores;
      }
      
      // Obtiene los datos del empleado
      /*$empleado = DB::select('SELECT users.id AS id_evaluado, users.first_name, users.last_name, employees.idempleado, employees.jefe, directions.name, grupo_areas.id AS grupo_area, niveles_puestos.* FROM users JOIN employees ON (employees.id = users.employee_id) LEFT JOIN job_positions ON (job_positions.id = employees.job_position_id) LEFT JOIN areas ON (areas.id = job_positions.area_id) LEFT JOIN departments ON (departments.id = areas.department_id) LEFT JOIN directions ON (directions.id = departments.direction_id) LEFT JOIN puesto_nivel_puesto ON (puesto_nivel_puesto.id_puesto = job_positions.id) LEFT JOIN niveles_puestos ON (niveles_puestos.id = puesto_nivel_puesto.id_nivel_puesto) LEFT JOIN departments_grupo_areas ON (departments_grupo_areas.id_departments = departments.id) LEFT JOIN grupo_areas ON (grupo_areas.id = departments_grupo_areas.id_grupos_areas) WHERE users.id = ?', [$id_evaluado]);*/

      /*if (empty($empleado)){

        $temp = $id_evaluado . '';

        while (strlen($temp) < 0){
          
          $temp = '0' . $temp;
        }

        // Obtiene los datos del empleado
        $empleado = DB::select('SELECT users.id AS id_evaluado, users.first_name, users.last_name, users.boss_id, niveles_puestos.* FROM users LEFT JOIN puestos ON (puestos.puesto = users.subdivision) LEFT JOIN puesto_nivel_puesto ON (puesto_nivel_puesto.id_puesto = puestos.id) LEFT JOIN niveles_puestos ON (niveles_puestos.id = puesto_nivel_puesto.id_nivel_puesto) LEFT JOIN areas ON (areas.nombre = users.division) LEFT JOIN areas_grupo_areas ON (areas_grupo_areas.id_areas = areas.id) WHERE users.id = ?', [$temp]);
      }*/

      $evaluacion = DB::table('evaluador_evaluado')->where('id_evaluado', $id_evaluado)->where('id_evaluador', $id_evaluador)->where('id_periodo', $id_periodo)->select('status', 'tipo_evaluacion')->first();

      /*if ($evaluacion->status == 'Terminada' && (($evaluacion->tipo_evaluacion != 2 && $empleado[0]->boss_id != $id_evaluador) || (($evaluacion->tipo_evaluacion == 2 || $empleado[0]->boss_id == $id_evaluador) && strpos($empleado[0]->workstation, 'DE ') !== 0 && strpos($empleado[0]->workstation, 'DAF ') !== 0 && strpos($empleado[0]->workstation, 'DIRTIL ') !== 0 && strpos($empleado[0]->workstation, 'UREPI ') !== 0))){

        if ($evaluacion->status == 'Terminada' && ($id_evaluador != $id_evaluado || ($id_evaluador == $id_evaluado && $id_evaluado != 234 && $id_evaluado != 221 && $id_evaluado != 26 && $id_evaluado != 175 && $id_evaluado != 166))){

          flash('Ya no es posible modificar la evaluación');
          return redirect('/evaluaciones');
        }
      }*/

      // El evaluador es jefe del evaluado
      if ($empleado[0]->jefe == auth()->user()->employee->idempleado && $empleado[0]->jefe != $empleado[0]->idempleado){
        
        // Obtiene los factores para evaluaciones de los jefes
        $results2 = DB::select('SELECT factores.id, factores.nombre, factores.descripcion, factores.comentarios, factores.orden, factores_niveles_dominios.nivel_dominio, factores_niveles_dominios.id AS id_pregunta FROM familias_factores INNER JOIN factores ON (factores.id_familia = familias_factores.id) INNER JOIN factores_niveles_dominios ON (factores.id = factores_niveles_dominios.id_factor) WHERE familias_factores.nombre LIKE "%jefe%" ORDER BY factores.orden');
		  
		$periodo_factores = array();

      	foreach ($results2 as $key => $value){
        
        	if (in_array($value->id, $factores)){

          		$periodo_factores[] = $value;
        	}
      	}

      	$results2 = $periodo_factores;
        
        // Une los factores genericos y los de los jefes a sus colaboradores
        $results = array_merge($results, $results2);
      }

      // El empleado cuenta con un puesto y a la vez un nivel de puesto
      //if (!empty($empleado[0]->nombre) && $empleado[0]->mando == 1){
      if (!empty($empleado[0]->nombre)){

        //$employees = DB::table('employees')->where('jefe', $empleado[0]->idempleado)->get();

        //if (count($employees) > 0){

          $id_nivel_puesto = $empleado[0]->id;
          $results3 = DB::select('SELECT factores.id, factores.nombre, factores.descripcion, factores.comentarios, factores.orden, factores_niveles_dominios.nivel_dominio, factores_niveles_dominios.id AS id_pregunta FROM familias_factores INNER JOIN factores ON (factores.id_familia = familias_factores.id) INNER JOIN factores_niveles_dominios ON (factores.id = factores_niveles_dominios.id_factor) INNER JOIN factores_niveles_puestos ON (factores_niveles_puestos.id_factor = factores.id) WHERE familias_factores.nombre LIKE ? AND factores_niveles_puestos.nivel_esperado != ? AND factores_niveles_puestos.id_nivel_puesto = ? ORDER BY factores.orden', ["%mando%", 0, $id_nivel_puesto]);
		      $periodo_factores = array();

      	  foreach ($results3 as $key => $value){
        
        	  if (in_array($value->id, $factores)){

          		$periodo_factores[] = $value;
        	  }
      	  }

      	  $results3 = $periodo_factores;
          $results = array_merge($results, $results3);
        //}
      }

      // El empleado es de un area en algun grupo de area
      if (!empty($empleado[0]->grupo_area)){

          $results4 = DB::select('SELECT factores.id, factores.nombre, factores.descripcion, factores.comentarios, factores.orden, factores_niveles_dominios.nivel_dominio, factores_niveles_dominios.id AS id_pregunta FROM familias_factores INNER JOIN factores ON (factores.id_familia = familias_factores.id) INNER JOIN factores_niveles_dominios ON (factores.id = factores_niveles_dominios.id_factor) WHERE factores.id_grupo_area = ? ORDER BY factores.orden', [$empleado[0]->grupo_area]);
          $periodo_factores = array();

          foreach ($results4 as $key => $value){
        
            if (in_array($value->id, $factores)){

              $periodo_factores[] = $value;
            }
          }

          $results4 = $periodo_factores;
          $results = array_merge($results, $results4);
      }

      $ordered_results = array();

      foreach ($results as $key => $value){
        
        if (empty($ordered_results)){

          $ordered_results[] = $value;
        }

        else{

          $temp = array();

          if ($value->orden < $ordered_results[0]->orden){

            $temp[] = $value;

            foreach ($ordered_results as $key2 => $value2){
              
              $temp[] = $value2;
            }
          }

          else{

            if ($value->orden > $ordered_results[count($ordered_results) - 1]->orden){

              $ordered_results[count($ordered_results)] = $value;
            }

            else{

              $band = false;

              foreach ($ordered_results as $key2 => $value2){
                
                if ($value->orden < $value2->orden && !$band){

                  $temp[] = $value;
                  $band = true;
                }

                $temp[] = $value2;
              }

              if (!$band){

                $temp[] = $value;
              }
            }
          }

          if (count($temp) > 0){

            $ordered_results = $temp;
          }
        }
      }

      $results = $ordered_results;

      /*$current_factor = 0;

      return view('evaluacion-desempeno/formato-evaluacion', compact('results', 'current_factor', 'empleado'));*/

      $resultados = DB::select('SELECT id_factor, nivel_desempeno, id_pregunta, comentario FROM resultados WHERE id_evaluado = ? AND id_evaluador = ? AND id_periodo = ? ORDER BY id_factor', [$id_evaluado, $id_evaluador, $id_periodo]);

      $factores = array();
      $niveles_dominio = array();
      $comentarios = array();

      foreach ($resultados as $key => $value){
        
        $factores[] = $value->id_factor;
        $niveles_dominio[$value->id_pregunta] = $value->nivel_desempeno;
        $comentarios[$value->id_factor] = $value->comentario;
      }

      $current_factor = 0;
      $evaluation = EvaluadorEvaluado::where('id_periodo', $id_periodo)->where('id_evaluador', $id_evaluador)->where('id_evaluado', $id_evaluado)->select('status')->first();
      $etiquetas = DB::table('etiquetas_desempeno')->orderBy('valor')->get();
      return view('evaluacion-desempeno/formato-evaluacion', compact('results', 'current_factor', 'empleado', 'factores', 'niveles_dominio', 'comentarios', 'id_evaluado', 'evaluation', 'etiquetas'));
    }

    /**
     * Guarda una respuesta del formato de evaluacion
     *
     * @return void
     */
    public function guardar_respuesta(){

      // Id del evaluador
      $id_evaluador = auth()->user()->id;
		
	  $periodos = DB::select('SELECT id FROM periodos WHERE status = ?', ['Abierto']);
	  $id_periodo = 0;
		
	  if (count($periodos) > 0){
		  
		$id_periodo = $periodos[0]->id;

      	// Si existe el id del empleado
      	if (!empty($_POST['id_evaluado'])){

        	$id_evaluado = $_POST['id_evaluado'] * 1;
        	$nivel_dominio = $_POST['nivel_dominio'];
        	$id_factor = $_POST['id_factor'];
          $id_pregunta = $_POST['id_pregunta'];
        	$total_preguntas = $_POST['total_preguntas'];
          $fecha = date('Y-m-d H:i:s');

        	// Busca si la respuesta ya existe
        	$resultado = DB::select('select id from resultados where id_evaluado = ? AND id_evaluador = ? AND id_factor = ? AND id_pregunta = ? AND id_periodo = ?', [$id_evaluado, $id_evaluador, $id_factor, $id_pregunta, $id_periodo]);
        
        	if ($resultado){

          		// Actualiza el resultado
          		DB::update('update resultados set updated_at = "' . $fecha . '", nivel_desempeno = "' . $nivel_dominio . '" WHERE id_evaluado = ? AND id_evaluador = ? AND id_periodo = ? AND id_factor = ? AND id_pregunta = ?', [$id_evaluado, $id_evaluador, $id_periodo, $id_factor, $id_pregunta]);
        	}

        	else{

          		// Agrega el resultado
          		DB::insert('insert into resultados (id_evaluado, id_evaluador, id_factor, id_pregunta, id_periodo, nivel_desempeno, created_at) values (?,?,?,?,?,?,?)', [$id_evaluado, $id_evaluador, $id_factor, $id_pregunta, $id_periodo, $nivel_dominio, $fecha]);
        	}

        	$resultados = DB::select('SELECT resultados.id, resultados.comentario, factores.comentarios FROM resultados INNER JOIN factores ON (factores.id = resultados.id_factor) WHERE resultados.id_evaluado = ? AND resultados.id_evaluador = ? AND resultados.id_periodo = ? AND resultados.nivel_desempeno != ?', [$id_evaluado, $id_evaluador, $id_periodo, 0]);
          $band = true;

          foreach ($resultados as $key => $value){
            
            if ($value->comentarios == 1 && empty($value->comentario)){

              $band = false;
              break;
            }
          }

          if ($total_preguntas <= count($resultados) && $band){

          		// Actualiza el status
          		DB::update('update evaluador_evaluado set status = "Terminada" WHERE id_evaluado = ? AND id_evaluador = ? AND id_periodo = ?', [$id_evaluado, $id_evaluador, $id_periodo]);
        	}

        	else{

          		// Actualiza el status
          		DB::update('update evaluador_evaluado set status = "Iniciada" WHERE id_evaluado = ? AND id_evaluador = ? AND id_periodo = ?', [$id_evaluado, $id_evaluador, $id_periodo]);
        	}

        	echo $id_evaluador;
        	echo $id_evaluado;
		}
      }
    }

    /**
     * Redirecciona a la pagina de evaluaciones
     *
     * @return void
     */
    public function guardar_respuestas(){

      // Id del evaluador
      $id_evaluador = auth()->user()->id;

      $id_evaluado = $_POST['id_evaluado'] * 1;
      $periodos = DB::select('SELECT id FROM periodos WHERE status = ?', ['Abierto']);
      $total_preguntas = $_POST['total_preguntas'];
      $id_periodo = 0;
    
      if (count($periodos) > 0){
      
        $id_periodo = $periodos[0]->id;
      }

      if (!empty($_POST['comentarios'])){

        foreach ($_POST['comentarios'] as $key => $value){

          $fecha = date('Y-m-d H:i:s');
          $results = DB::select("SELECT id FROM resultados WHERE id_periodo = ? AND id_evaluador = ? AND id_evaluado = ? AND id_factor = ?", [$id_periodo, $id_evaluador, $id_evaluado, $_POST['factores'][$key]]);

          if (count($results) > 0){

            DB::update("UPDATE resultados SET updated_at = '$fecha', comentario = '$value' WHERE id_periodo = ? AND id_evaluador = ? AND id_evaluado = ? AND id_factor = ?", [$id_periodo, $id_evaluador, $id_evaluado, $_POST['factores'][$key]]);
          }

          else{

            $preguntas = DB::table('factores_niveles_dominios')->where('id_factor', $_POST['factores'][$key])->select('id')->get();

            if (count($preguntas) > 0){

              foreach ($preguntas as $key2 => $value2){
                
                DB::insert("INSERT INTO resultados (id_periodo,id_evaluador,id_evaluado,id_factor,id_pregunta,nivel_desempeno,comentario,created_at) VALUES (?,?,?,?,?,?,?,?)", [$id_periodo, $id_evaluador, $id_evaluado, $_POST['factores'][$key], $value2->id, 0, $value, $fecha]);
              }
            }

            //DB::insert("INSERT INTO resultados (id_periodo,id_evaluador,id_evaluado,id_factor,nivel_desempeno,comentario,created_at) VALUES (?,?,?,?,?,?,?)", [$id_periodo, $id_evaluador, $id_evaluado, $_POST['factores'][$key], 0, $value, $fecha]);
          }
        }
      }

      $resultados = DB::select('SELECT resultados.id, resultados.comentario, resultados.nivel_desempeno, factores.comentarios FROM resultados INNER JOIN factores ON (factores.id = resultados.id_factor) WHERE resultados.id_evaluado = ? AND resultados.id_evaluador = ? AND resultados.id_periodo = ?', [$id_evaluado, $id_evaluador, $id_periodo]);
      $band = true;

      foreach ($resultados as $key => $value){
            
        if (($value->comentarios == 1 && empty($value->comentario)) || (empty($value->nivel_desempeno) && $value->nivel_desempeno != '0')){
          
          $band = false;
          break;
        }
      }

      if ($total_preguntas <= count($resultados) && $band){

        // Actualiza el status
        DB::update('update evaluador_evaluado set status = "Terminada" WHERE id_evaluado = ? AND id_evaluador = ? AND id_periodo = ?', [$id_evaluado, $id_evaluador, $id_periodo]);
      }

      else{

        // Actualiza el status
        DB::update('update evaluador_evaluado set status = "Iniciada" WHERE id_evaluado = ? AND id_evaluador = ? AND id_periodo = ?', [$id_evaluado, $id_evaluador, $id_periodo]);
      }

      flash('Se guardaron las respuestas');
      return redirect()->to('/evaluaciones');
    }
	
	/**
     * Muestra la pagina para generar la matriz de evaluaciones
     *
     * @return void
     */
    public function matriz_evaluaciones($id_periodo){

      // Obtiene el periodo con status Preparatorio
      $periodos = DB::select('SELECT id, descripcion, status FROM periodos WHERE status = ? OR status = ? ORDER BY id DESC', ['Preparatorio', 'Abierto']);

      if (count($periodos) == 0){

        flash('No hay periodos preparatorios ni abiertos');
        return redirect()->to('/');
      }

      $descripcion = '';
      $status = '';

      if (empty($id_periodo)){

        if (Session::has('id_periodo')){

          $id_periodo = Session::get('id_periodo');

          foreach ($periodos as $key => $value){
          
            if ($id_periodo == $value->id){

              $descripcion = $value->descripcion;
              $status = $value->status;
              break;
            } 
          }
        }

        else{

          $id_periodo = $periodos[0]->id;
          $descripcion = $periodos[0]->descripcion;
          $status = $periodos[0]->status;
        }
      }

      else{

        Session::put('id_periodo', $id_periodo);

        foreach ($periodos as $key => $value){
          
          if ($id_periodo == $value->id){

            $descripcion = $value->descripcion;
            $status = $value->status;
            break;
          } 
        }
      }

      /*if (empty($descripcion)){

        if ($id_periodo == $periodos[0]->id){

          $descripcion = $periodos[0]->descripcion;
          $status = $periodos[0]->status;
        }

        else{

          $descripcion = $periodos[1]->descripcion;
          $status = $periodos[1]->status;
        }
      }*/

      return view('evaluacion-desempeno/matriz-evaluaciones', compact('periodos', 'id_periodo', 'descripcion', 'status'));
    }
	
	/**
     * Genera la matriz de evaluaciones
     *
     * @return void
     */
    public function generar_matriz(){

      $id_periodo = 0;

      if (!empty($_POST['id_periodo'])){

        $id_periodo = $_POST['id_periodo'];
      }

      else{

        flash('No hay periodos abiertos ni cerrados');
        return redirect()->to('/');        
      }

      // Obtiene el periodo con status Preparatorio
      $periodos = DB::select('SELECT descripcion, status FROM periodos WHERE id = ? AND (status = ? OR status = ?)', [$id_periodo, 'Abierto', 'Preparatorio']);

      if (count($periodos) == 0){

        flash('No hay periodos abiertos ni cerrados');
        return redirect()->to('/');
      }

      $descripcion = $periodos[0]->descripcion;
      $status = $periodos[0]->status;
      $preparatorio = false;

      if ($status == 'Preparatorio'){

        DB::delete("DELETE FROM evaluador_evaluado WHERE id_periodo = ?", [$id_periodo]);
        $preparatorio = true;
      }
            
      $users = DB::select("SELECT id, boss_id FROM users WHERE active = ?", [1]);
      $new = array();
      $new['id_periodo'] = $id_periodo;
      $new['status'] = 'No Iniciada';
      $jefes = array();

      foreach ($users as $key => $user){
        
        if (!in_array($user->boss_id, $jefes)){

          $jefes[] = $user->boss_id;
        }
      }

      foreach ($users as $key => $user){

        if (empty($user->boss_id) || $user->boss_id == 1){

          continue;
        }

        if (!in_array($user->boss_id, $jefes)){

          continue;
        }

        $id = $user->id;
        $jefe = $user->boss_id;

        $evaluacion = DB::select("SELECT id FROM evaluador_evaluado WHERE id_evaluador = ? AND id_evaluado = ? AND id_periodo = ?", [$jefe, $id, $id_periodo]);

        if (count($evaluacion) > 0){

          continue;
        }

        if ($id != $jefe){

          $new['id_evaluador'] = $jefe;
          $new['id_evaluado'] = $id;
				  $new['created_at'] = date('Y-m-d H:i:s');
          DB::table('evaluador_evaluado')->insert($new);
        }
      }

      flash('Se creó la matriz de evaluaciones para el periodo ' . $descripcion, 'success');
	    return redirect()->to('/matriz-evaluaciones/' . $id_periodo);
    }

    /**
     * Descarga en excel la red de interacción
     */
    public function export_interaction_red($id_periodo){

      if (!auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin')){

        flash('No tiene permiso para exportar la red de interacción');
        return redirect('/que-es-evaluacion-desempeno');
      }

      $periodo = DB::table('periodos')->where('id', $id_periodo)->select('descripcion')->first();
      
      // Obtiene la red de interacción
      $red = EvaluadorEvaluado::where('id_periodo', $id_periodo)->get();

      require_once 'PHPExcel.php';
      require_once 'PHPExcel/IOFactory.php';

      // Create new PHPExcel object
      $objPHPExcel = new \PHPExcel();
      $objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID Evaluador');
      $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Evaluador');
      $objPHPExcel->getActiveSheet()->setCellValue('C1', 'ID Evaluado');
      $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Evaluado');
      $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Tipo de Evaluación');
      $row = 3;

      if (count($red) > 0){

        foreach ($red as $key => $evaluador_evaluado){

          $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $evaluador_evaluado->nameCompleteUserEvaluador->employee_wt->idempleado);
          $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $evaluador_evaluado->nameCompleteUserEvaluador->fullname);
          $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $evaluador_evaluado->nameCompleteUserEvaluado->employee_wt->idempleado);
          $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $evaluador_evaluado->nameCompleteUserEvaluado->fullname);
          $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, (!empty($evaluador_evaluado->tipoEvaluacion) ? $evaluador_evaluado->tipoEvaluacion->descripcion_empresa : ''));
          $row++;
        }
      }

      // Rename sheet
      $objPHPExcel->getActiveSheet()->setTitle('Red de Interacción');

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: text/csv');
      header('Content-Disposition: attachment;filename="Red Periodo ' . $periodo->descripcion . '.csv"');
      header('Cache-Control: max-age=0');
      $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
      $objWriter->save('php://output');
    }
}