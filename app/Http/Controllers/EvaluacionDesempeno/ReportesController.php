<?php

namespace App\Http\Controllers\EvaluacionDesempeno;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use DB;
use Session;
use App\User;
use App\Employee;

class ReportesController extends Controller
{
    
    /**
     * Create a instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Muestra la pagina de Reporte de Desempeño
     *
     * @return void
     */
    public function reporte_desempeno(){

      if (Session::has('primer_login')){

        flash('Debes cambiar tu contraseña antes de continuar a otra sección');
        return redirect('/');
      }

      $users = array();
      $id_jefe = auth()->user()->id;
      $id_periodo = 0;
      $status = '';
      $retro = false;
      $periodos = DB::table('periodos')->where('status', '<>', 'Preparatorio')->where('status', '<>', 'Cancelado')->select('id', 'descripcion', 'status')->orderBy('id', 'DESC')->get();
      
      if (count($periodos) > 0){

        $id_periodo = $periodos[0]->id;
        $status = $periodos[0]->status;

        if (!empty($_POST['id_periodo']) || Session::has('id_periodo')){

          if (!empty($_POST['id_periodo'])){

            $id_periodo = $_POST['id_periodo'];
            Session::put('id_periodo', $id_periodo);
          }

          else{

            $id_periodo = Session::get('id_periodo');
          }

          foreach ($periodos as $key => $periodo){
            
            if ($periodo->id == $id_periodo){

              $status = $periodo->status;
            }
          }
        }

        if (auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin') || auth()->user()->role == 'supervisor'){

          // Obtiene los datos de todos los usuarios
          $users = DB::select('SELECT users.first_name, users.last_name, users.id, employees.idempleado, departments.name AS department, job_positions.name as job_position, employees.jefe, comentarios_desempeno_jefe.comentario, objetivos_entregables_desempeno.objetivo, evaluador_evaluado.status FROM users INNER JOIN employees ON (employees.id = users.employee_id) LEFT JOIN job_positions ON (job_positions.id = employees.job_position_id) LEFT JOIN areas ON (areas.id = job_positions.area_id) LEFT JOIN departments ON (departments.id = areas.department_id) INNER JOIN evaluador_evaluado ON (evaluador_evaluado.id_evaluado = users.id) LEFT JOIN comentarios_desempeno_jefe ON (comentarios_desempeno_jefe.id_evaluado = users.id AND comentarios_desempeno_jefe.id_periodo = evaluador_evaluado.id_periodo) LEFT JOIN objetivos_entregables_desempeno ON (objetivos_entregables_desempeno.id_evaluado = users.id AND evaluador_evaluado.id_periodo = objetivos_entregables_desempeno.id_periodo) WHERE evaluador_evaluado.id_periodo = ? ORDER BY users.id', [$id_periodo]);
        }

        else{

          //if (auth()->user()->role != 'director'){

            //if ($status == 'Cerrado'){

            // Obtiene los datos de todos los subordinados del usuario logueado
            /*$users = DB::select('SELECT users.first_name, users.last_name, users.id, employees.idempleado, departments.name AS department, job_positions.name as job_position, employees.jefe, comentarios_desempeno_jefe.comentario, objetivos_entregables_desempeno.objetivo, evaluador_evaluado.status, evaluador_evaluado.id_evaluador FROM users INNER JOIN employees ON (employees.id = users.employee_id) LEFT JOIN job_positions ON (job_positions.id = employees.job_position_id) LEFT JOIN areas ON (areas.id = job_positions.area_id) LEFT JOIN departments ON (departments.id = areas.department_id) INNER JOIN evaluador_evaluado ON (evaluador_evaluado.id_evaluado = users.id) LEFT JOIN comentarios_desempeno_jefe ON (comentarios_desempeno_jefe.id_evaluado = users.id AND comentarios_desempeno_jefe.id_periodo = evaluador_evaluado.id_periodo) LEFT JOIN objetivos_entregables_desempeno ON (objetivos_entregables_desempeno.id_evaluado = users.id AND evaluador_evaluado.id_periodo = objetivos_entregables_desempeno.id_periodo) WHERE (employees.jefe = ? OR users.id = ?) AND evaluador_evaluado.id_periodo = ? ORDER BY users.id', [auth()->user()->employee->idempleado, $id_jefe, $id_periodo]);*/
            //}

            //else{

              // Obtiene los datos de todos los subordinados del usuario logueado
              //$users = DB::select('SELECT users.first_name, users.last_name, users.id, users.division, users.subdivision, users.boss_id, comentarios_desempeno_jefe.comentario, objetivos_entregables_desempeno.objetivo, evaluador_evaluado.status FROM users INNER JOIN evaluador_evaluado ON (evaluador_evaluado.id_evaluado = users.id) LEFT JOIN comentarios_desempeno_jefe ON (comentarios_desempeno_jefe.id_evaluado = users.id AND comentarios_desempeno_jefe.id_periodo = evaluador_evaluado.id_periodo) LEFT JOIN objetivos_entregables_desempeno ON (objetivos_entregables_desempeno.id_evaluado = users.id AND evaluador_evaluado.id_periodo = objetivos_entregables_desempeno.id_periodo) WHERE users.id = ? AND evaluador_evaluado.id_periodo = ?', [$id_jefe, $id_periodo]);
            //}
          //}

          //else{

            if ($status == 'Cerrado' || $status == 'Retroalimentacion'){

              $users = array(auth()->user()->employee->idempleado);
            }

            else{

              $users = array();
              $id_jefe = 0;
            }

            $ids = array();

            do{

              $ids = array_merge($ids, $users);
              $users = DB::table('employees')->whereIn('jefe', $users)->pluck('idempleado')->toArray();

            }while(count($users) > 0);

            $users = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->join('evaluador_evaluado', 'evaluador_evaluado.id_evaluado', '=', 'users.id')->leftJoin('comentarios_desempeno_jefe', function($join){$join->on('comentarios_desempeno_jefe.id_evaluado', '=', 'users.id');$join->on('comentarios_desempeno_jefe.id_periodo', '=', 'evaluador_evaluado.id_periodo');})->leftJoin('objetivos_entregables_desempeno', function($join){$join->on('objetivos_entregables_desempeno.id_evaluado', '=', 'users.id');$join->on('objetivos_entregables_desempeno.id_periodo', '=', 'evaluador_evaluado.id_periodo');})->where('evaluador_evaluado.id_periodo', $id_periodo)->whereIn('jefe', $ids)->orWhere('users.id', $id_jefe)->select('first_name', 'last_name', 'users.id', 'departments.name AS department', 'job_positions.name AS job_position', 'jefe', 'comentario', 'objetivo', 'status', 'id_evaluador', 'idempleado', 'evaluador_evaluado.id_periodo')->orderBy('users.id')->get();

            $temp_users = array();

            foreach ($users as $key => $value) {
              
              if ($value->id_periodo == $id_periodo){

                $temp_users[] = $value;
              }
            }

            $users = $temp_users;

          //}
        }

        $temp_users = array();
        $jefes = array();
        $current_user = 0;

        foreach ($users as $key => $user){
      
          if ($current_user != $user->id){

            $user->boss = '';

            if (!empty($user->jefe)){

              if (!empty($jefes[$user->jefe])){

                $user->boss = $jefes[$user->jefe];
              }

              else{

                $jefe = DB::select("SELECT nombre, paterno, materno FROM employees WHERE idempleado = ?", [$user->jefe]);

                if (count($jefe) > 0){

                  $user->boss = $jefe[0]->nombre . ' ' . $jefe[0]->paterno . ' ' . $jefe[0]->materno;
                  $jefes[$user->jefe] = $user->boss;
                }

                else{

                  $jefes[$user->jefe] = '';
                }
              }
            }

            $current_user = $user->id;

            if (!empty($user->comentario) && !empty($user->objetivo)){

              $retro = true;
            }
          }

          $temp_users[] = $user;
        }
      
        $users = $temp_users;
      }

      return view('evaluacion-desempeno/reportes/reporte-desempeno', compact('users', 'periodos', 'id_periodo', 'status', 'retro'));
    }

    /**
     * Muestra la pagina de Reporte Consolidado
     *
     * @return void
     */
    public function reporte_consolidado(){

      if (Session::has('primer_login')){

        flash('Debes cambiar tu contraseña antes de continuar a otra sección');
        return redirect('/');
      }

      $users = array();
      $id_jefe = auth()->user()->id;
      $id_periodo = 0;
      $status = '';
      $retro = false;
      $consolidado = true;
      $periodos = DB::table('periodos')->where('status', 'Abierto')->orWhere('status', 'Cerrado')->select('id', 'descripcion', 'status')->orderBy('id', 'DESC')->get();
      
      if (count($periodos) > 0){

        $id_periodo = $periodos[0]->id;
        $status = $periodos[0]->status;

        if (!empty($_POST['id_periodo'])){

          $id_periodo = $_POST['id_periodo'];

          foreach ($periodos as $key => $periodo){
            
            if ($periodo->id == $id_periodo){

              $status = $periodo->status;
            }
          }
        }

        //if (auth()->user()->role == 'admin' || auth()->user()->role == 'supervisor'){
        if (auth()->user()->role == 'supervisor'){

          // Obtiene los datos de todos los usuarios
          $users = DB::select('SELECT users.first_name, users.last_name, users.id, users.division, users.subdivision, users.boss_id, comentarios_desempeno_jefe.comentario, objetivos_entregables_desempeno.objetivo, evaluador_evaluado.status FROM users INNER JOIN evaluador_evaluado ON (evaluador_evaluado.id_evaluado = users.id) LEFT JOIN comentarios_desempeno_jefe ON (comentarios_desempeno_jefe.id_evaluado = users.id AND comentarios_desempeno_jefe.id_periodo = evaluador_evaluado.id_periodo) LEFT JOIN objetivos_entregables_desempeno ON (objetivos_entregables_desempeno.id_evaluado = users.id AND evaluador_evaluado.id_periodo = objetivos_entregables_desempeno.id_periodo) WHERE evaluador_evaluado.id_periodo = ? AND users.id != ? ORDER BY users.id', [$id_periodo, $id_jefe]);
        }

        else{

          if (auth()->user()->role != 'director' && !auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin')){

            //if ($status == 'Cerrado'){

            // Obtiene los datos de todos los subordinados del usuario logueado
            $users = DB::select('SELECT users.first_name, users.last_name, users.id, users.division, users.subdivision, users.boss_id, comentarios_desempeno_jefe.comentario, objetivos_entregables_desempeno.objetivo, evaluador_evaluado.status, evaluador_evaluado.id_evaluador FROM users INNER JOIN evaluador_evaluado ON (evaluador_evaluado.id_evaluado = users.id) LEFT JOIN comentarios_desempeno_jefe ON (comentarios_desempeno_jefe.id_evaluado = users.id AND comentarios_desempeno_jefe.id_periodo = evaluador_evaluado.id_periodo) LEFT JOIN objetivos_entregables_desempeno ON (objetivos_entregables_desempeno.id_evaluado = users.id AND evaluador_evaluado.id_periodo = objetivos_entregables_desempeno.id_periodo) WHERE users.boss_id = ? AND users.id <> ? AND evaluador_evaluado.id_periodo = ? ORDER BY users.id', [$id_jefe, $id_jefe, $id_periodo]);
            //}

            //else{

              // Obtiene los datos de todos los subordinados del usuario logueado
              //$users = DB::select('SELECT users.first_name, users.last_name, users.id, users.division, users.subdivision, users.boss_id, comentarios_desempeno_jefe.comentario, objetivos_entregables_desempeno.objetivo, evaluador_evaluado.status FROM users INNER JOIN evaluador_evaluado ON (evaluador_evaluado.id_evaluado = users.id) LEFT JOIN comentarios_desempeno_jefe ON (comentarios_desempeno_jefe.id_evaluado = users.id AND comentarios_desempeno_jefe.id_periodo = evaluador_evaluado.id_periodo) LEFT JOIN objetivos_entregables_desempeno ON (objetivos_entregables_desempeno.id_evaluado = users.id AND evaluador_evaluado.id_periodo = objetivos_entregables_desempeno.id_periodo) WHERE users.id = ? AND evaluador_evaluado.id_periodo = ?', [$id_jefe, $id_periodo]);
            //}
          }

          else{

            if ($id_jefe == 1 || $id_jefe == 26 || $id_jefe == 175){

              $id_jefe = 194;
            }

            $users = array($id_jefe);
            $ids = array();
            $nivel = 0;

            do{

              //print_r($users);
              $ids = array_merge($ids, $users);
              $users = DB::table('users')->whereIn('boss_id', $users)->whereNotIn('id', $ids)->pluck('id')->toArray();
              $nivel++;

            }while(count($users) > 0);

            //exit();

            $users = DB::table('users')->join('evaluador_evaluado', 'evaluador_evaluado.id_evaluado', '=', 'users.id')->leftJoin('comentarios_desempeno_jefe', function($join){$join->on('comentarios_desempeno_jefe.id_evaluado', '=', 'users.id');$join->on('comentarios_desempeno_jefe.id_periodo', '=', 'evaluador_evaluado.id_periodo');})->leftJoin('objetivos_entregables_desempeno', function($join){$join->on('objetivos_entregables_desempeno.id_evaluado', '=', 'users.id');$join->on('objetivos_entregables_desempeno.id_periodo', '=', 'evaluador_evaluado.id_periodo');})->whereIn('boss_id', $ids)->where('users.id', '!=', $id_jefe)->where('evaluador_evaluado.id_periodo', $id_periodo)->select('first_name', 'last_name', 'users.id', 'division', 'subdivision', 'boss_id', 'comentario', 'objetivo', 'status', 'id_evaluador')->orderBy('users.id')->get();
          }
        }

        $temp_users = array();
        $jefes = array();
        $current_user = 0;

        foreach ($users as $key => $user){
      
          if ($current_user != $user->id){

            $user->boss = '';

            if (!empty($user->boss_id)){

              if (!empty($jefes[$user->boss_id])){

                $user->boss = $jefes[$user->boss_id];
              }

              else{

                $jefe = DB::select("SELECT first_name, last_name FROM users WHERE id = ?", [$user->boss_id]);

                if (count($jefe) > 0){

                  $user->boss = $jefe[0]->first_name . ' ' . $jefe[0]->last_name;
                  $jefes[$user->boss_id] = $user->boss;
                }

                else{

                  $jefes[$user->boss_id] = '';
                }
              }
            }

            $current_user = $user->id;

            if (!empty($user->comentario) && !empty($user->objetivo)){

              $retro = true;
            }
          }

          $temp_users[] = $user;
        }
      
        $users = $temp_users;
      }

      return view('evaluacion-desempeno/reportes/reporte-consolidado', compact('users', 'periodos', 'id_periodo', 'status', 'retro'));
    }

    /**
     * Muestra el reporte
     */
    public function reporte_desempeno_individual($id_evaluado){

      if (Session::has('primer_login')){

        flash('Debes cambiar tu contraseña antes de continuar a otra sección');
        return redirect('/');
      }

      $id_jefe = auth()->user()->id;
      $competencias = array();
      $evaluacion_jefe = false;
      /*$user = DB::select("SELECT first_name, last_name, areas.name, job_positions.name AS puesto, jefe FROM users INNER JOIN employees ON (employees.id = users.employee_id) INNER JOIN WHERE id = ?", [$id_evaluado]);*/
      $user = User::withTrashed()->find($id_evaluado);
      $employee = $user->employee_wt;
      $etiquetas = DB::table('etiquetas_desempeno')->orderBy('valor', 'DESC')->get();


      /*if (auth()->user()->role != 'admin' && $user[0]->boss_id != $id_jefe){

        return redirect('/reporte_desempeno')->with('danger','No es jefe del empleado');
      }*/

      $id_periodo = 0;
      $modalidad = 1;
      $modalidad_comparar = 1;

      if (!empty($_POST['id_periodo'])){

        $id_periodo = $_POST['id_periodo'];
        $periodos = DB::select('SELECT id_modalidad FROM periodos WHERE id = ?', [$id_periodo]);
        $modalidad = $periodos[0]->id_modalidad;
      }

      else{

        $periodos = DB::select('SELECT id, id_modalidad FROM periodos WHERE status = ? ORDER BY id DESC', ['Cerrado']);
    
        if (count($periodos) > 0){
      
          $id_periodo = $periodos[0]->id;
          $modalidad = $periodos[0]->id_modalidad;
        }
      }

      $evaluaciones_terminadas = false;
      $evaluaciones = DB::select("SELECT id FROM evaluador_evaluado WHERE id_evaluado = ? AND id_periodo = ?", [$id_evaluado, $id_periodo]);
      $terminadas = DB::select("SELECT id, id_evaluador FROM evaluador_evaluado WHERE id_evaluado = ? AND id_periodo = ? AND status = ?", [$id_evaluado, $id_periodo, 'Terminada']);

      if (count($evaluaciones) > 0 && count($evaluaciones) == count($terminadas)){

        $evaluaciones_terminadas = true;
      }

      if (!auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin') && auth()->user()->role != 'supervisor'){

        foreach ($terminadas as $key => $value){
          
          if ($value->id_evaluador == $id_jefe){

            $evaluacion_jefe = true;
            break;
          }
        }
      }

      else{

        $evaluacion_jefe = true;
      }

      $id_nivel_puesto = 0;
      $puesto_nivel_puesto = DB::table('puesto_nivel_puesto')->where('id_puesto', $user->employee_wt->job_position_id)->select('id_nivel_puesto')->first();
      $niveles_esperados = array();
      $factores_con_nivel_de_puesto = array();
      $pesos_factores = array();

      if (!empty($puesto_nivel_puesto)){

        $id_nivel_puesto = $puesto_nivel_puesto->id_nivel_puesto;

        $niveles_esperados = DB::table('factores_niveles_puestos')->where('id_nivel_puesto', $puesto_nivel_puesto->id_nivel_puesto)->select('nivel_esperado', 'id_factor', 'peso')->get();


        foreach ($niveles_esperados as $key => $value){
          
          $factores_con_nivel_de_puesto[] = $value->id_factor;
          $pesos_factores[$value->id_factor] = $value->peso;
        }
      }

      //$resultados = DB::select("SELECT resultados.nivel_desempeno, resultados.id_factor, resultados.comentario, resultados.id_evaluador, factores.nombre, users.boss_id FROM resultados INNER JOIN users ON (users.id = resultados.id_evaluador) INNER JOIN factores ON (factores.id = resultados.id_factor) WHERE id_evaluado = ? AND id_periodo = ? ORDER BY resultados.id_factor", [$id_evaluado, $id_periodo]);
      $resultados = DB::table('resultados')->join('users', 'users.id', '=', 'resultados.id_evaluador')->join('employees', 'employees.id', '=', 'users.employee_id')->join('factores', 'factores.id', '=', 'resultados.id_factor')->join('evaluador_evaluado', function($join){$join->on('evaluador_evaluado.id_evaluado', '=', 'resultados.id_evaluado');$join->on('evaluador_evaluado.id_evaluador', '=', 'resultados.id_evaluador');$join->on('evaluador_evaluado.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('tipos_evaluadores', 'tipos_evaluadores.id', '=', 'evaluador_evaluado.tipo_evaluacion')->where('resultados.id_evaluado', $id_evaluado)->where('resultados.id_periodo', $id_periodo)->select(array(DB::raw('AVG(nivel_desempeno) AS nivel_desempeno'), 'id_factor', 'comentario', 'tipos_evaluadores.nombre AS nombre_evaluacion', 'resultados.id_evaluador', 'factores.nombre', 'jefe', 'idempleado'))->groupBy('id_factor', 'resultados.id_evaluador')->orderBy('id_factor')->get();

      $comentario = DB::select("SELECT comentario FROM comentarios_desempeno_jefe WHERE id_evaluado = ? AND id_periodo = ?", [$id_evaluado, $id_periodo]);
      $objetivos_entregables_desempeno = DB::select("SELECT objetivo, accion, entregable FROM objetivos_entregables_desempeno WHERE id_evaluado = ? AND id_periodo = ?", [$id_evaluado, $id_periodo]);
      //$user = DB::select("SELECT id, first_name, last_name, division, subdivision, boss_id FROM users WHERE id = ?", [$id_evaluado]);
      //$jefe = DB::select("SELECT first_name, last_name, division, subdivision FROM users WHERE id = ?", [$user[0]->boss_id]);
      $meses = array(0,'ene','feb','mar','abr','may','jun','jul','ago','sep','oct','nov','dic');
      $periodos = array();
      $resultados_a_comparar = array();

      // El usuario ya tiene registros con comentarios y objetivos para el periodo actual y ademas su jefe es el que esta viendo el reporte 
      //if (count($comentario) > 0 || count($objetivos_entregables_desempeno) > 0 && $user[0]->boss_id == auth()->user()->id){
      if (!empty(auth()->user()->employee) && $user->employee->jefe == auth()->user()->employee->idempleado){

        // Obtiene los periodos donde el usuario tiene registrados objetivos, acciones y entregables (excepto el periodo actual)
        $periodos_con_objetivos_entregables = DB::select("SELECT id_periodo FROM objetivos_entregables_desempeno WHERE id_evaluado = ? AND id_periodo != ? GROUP BY id_periodo", [$id_evaluado, $id_periodo]);

        // Existen periodos diferentes al actual con registros de la evaluacion de desempeño
        if (count($periodos_con_objetivos_entregables) > 0){

          // Se guardan los ids de los periodos
          foreach ($periodos_con_objetivos_entregables as $key => $value){
            
            $periodos[] = $value->id_periodo;
          }

          $periodos = implode(',', $periodos);
          
          // Se obtienen los datos de los periodos
          $periodos = DB::select("SELECT id, descripcion, id_modalidad FROM periodos WHERE id IN ($periodos) ORDER BY id DESC");

          // Se seleccionó un periodo a comparar
          if (!empty($_POST['periodo_a_comparar'])){

            $periodo_a_comparar = $_POST['periodo_a_comparar'];

            foreach ($periodos as $key => $value){
              
              if ($value->id == $periodo_a_comparar){

                $modalidad_comparar = $value->id_modalidad;
                break;
              }
            }
            
            // Se obtienen los datos de la evaluación del periodo a comparar
            //$resultados_a_comparar = DB::select("SELECT resultados.nivel_desempeno, resultados.id_factor, resultados.comentario, resultados.id_evaluador, factores.nombre, users.boss_id FROM resultados INNER JOIN users ON (users.id = resultados.id_evaluador) INNER JOIN factores ON (factores.id = resultados.id_factor) WHERE id_evaluado = ? AND id_periodo = ? ORDER BY resultados.id_factor", [$id_evaluado, $periodo_a_comparar]);
            $resultados_a_comparar = DB::table('resultados')->join('users', 'users.id', '=', 'resultados.id_evaluador')->join('employees', 'employees.id', '=', 'users.employee_id')->join('factores', 'factores.id', '=', 'resultados.id_factor')->join('evaluador_evaluado', function($join){$join->on('evaluador_evaluado.id_evaluado', '=', 'resultados.id_evaluado');$join->on('evaluador_evaluado.id_evaluador', '=', 'resultados.id_evaluador');$join->on('evaluador_evaluado.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('tipos_evaluadores', 'tipos_evaluadores.id', '=', 'evaluador_evaluado.tipo_evaluacion')->where('resultados.id_evaluado', $id_evaluado)->where('resultados.id_periodo', $periodo_a_comparar)->select(array(DB::raw('AVG(nivel_desempeno) AS nivel_desempeno'), 'id_factor', 'comentario', 'tipos_evaluadores.nombre AS nombre_evaluacion', 'resultados.id_evaluador', 'factores.nombre', 'jefe', 'idempleado'))->orderBy('id_factor')->get();
          }
        }
      }

      $competencias = DB::table('periodo_factor')->join('factores', 'periodo_factor.id_factor', '=', 'factores.id')->where('id_periodo', $id_periodo)->select('nombre')->get();
      $tipos_evaluadores = DB::table('tipos_evaluadores')->pluck('descripcion_empresa', 'id')->toArray();
      $ponderaciones = DB::table('ponderaciones')->where('id_periodo', $id_periodo)->where('id_nivel_puesto', $id_nivel_puesto)->select('jefe', 'autoevaluacion', 'colaborador', 'par', 'psicometricos')->first();

      // No hay ponderaciones registradas para el usuario de este periodo
      if (empty($ponderaciones)){

        $last = DB::table('ponderaciones')->select('id_periodo')->orderBy('created_at', 'DESC')->first();

        if (!empty($last->id_periodo)){

          // Se obtienen las ultimas ponderaciones registradas
          $ponderaciones = DB::table('ponderaciones')->where('id_periodo', $last->id_periodo)->where('id_nivel_puesto', $id_nivel_puesto)->select('jefe', 'autoevaluacion', 'colaborador', 'par', 'psicometricos')->first();
        }
      }

      return view('evaluacion-desempeno/reportes/reporte-desempeno-individual', compact('resultados', 'user', 'employee', 'meses', 'id_periodo', 'modalidad', 'comentario', 'objetivos_entregables_desempeno', 'evaluaciones_terminadas', 'periodos', 'resultados_a_comparar', 'competencias', 'evaluacion_jefe', 'modalidad_comparar', 'factores_con_nivel_de_puesto', 'niveles_esperados', 'tipos_evaluadores', 'pesos_factores', 'id_nivel_puesto', 'ponderaciones', 'etiquetas'));
    }

    /**
     * Muestra el reporte consolidado individual
     */
    public function reporte_consolidado_individual($id_evaluado){

      if (Session::has('primer_login')){

        flash('Debes cambiar tu contraseña antes de continuar a otra sección');
        return redirect('/');
      }

      $id_jefe = auth()->user()->id;
      $competencias = array();
      $evaluacion_jefe = false;
      $user = DB::select("SELECT first_name, last_name, division, subdivision, boss_id FROM users WHERE id = ?", [$id_evaluado]);

      /*if (auth()->user()->role != 'admin' && $user[0]->boss_id != $id_jefe){

        return redirect('/reporte_desempeno')->with('danger','No es jefe del empleado');
      }*/

      $id_periodo = 0;
      $modalidad = 1;

      if (!empty($_POST['id_periodo'])){

        $id_periodo = $_POST['id_periodo'];
        $periodos = DB::select('SELECT id_modalidad FROM periodos WHERE id = ?', [$id_periodo]);
        $modalidad = $periodos[0]->id_modalidad;
      }

      else{

        $periodos = DB::select('SELECT id, id_modalidad FROM periodos WHERE status = ? ORDER BY id DESC', ['Cerrado']);
    
        if (count($periodos) > 0){
      
          $id_periodo = $periodos[0]->id;
          $modalidad = $periodos[0]->id_modalidad;
        }
      }

      $evaluaciones_terminadas = false;
      $evaluaciones = DB::select("SELECT id FROM evaluador_evaluado WHERE id_evaluado = ? AND id_periodo = ?", [$id_evaluado, $id_periodo]);
      $terminadas = DB::select("SELECT id, id_evaluador FROM evaluador_evaluado WHERE id_evaluado = ? AND id_periodo = ? AND status = ?", [$id_evaluado, $id_periodo, 'Terminada']);

      if (count($evaluaciones) > 0 && count($evaluaciones) == count($terminadas)){

        $evaluaciones_terminadas = true;
      }

      if (!auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin') && auth()->user()->role != 'supervisor'){

        foreach ($terminadas as $key => $value){
          
          if ($value->id_evaluador == $id_jefe){

            $evaluacion_jefe = true;
            break;
          }
        }
      }

      else{

        $evaluacion_jefe = true;
      }

      //$resultados = DB::select("SELECT resultados.nivel_desempeno, resultados.id_factor, resultados.comentario, resultados.id_evaluador, factores.nombre, users.boss_id FROM resultados INNER JOIN users ON (users.id = resultados.id_evaluador) INNER JOIN factores ON (factores.id = resultados.id_factor) WHERE id_evaluado = ? AND id_periodo = ? ORDER BY resultados.id_factor", [$id_evaluado, $id_periodo]);
      $resultados = DB::table('resultados')->join('users', 'users.id', '=', 'resultados.id_evaluador')->join('factores', 'factores.id', '=', 'resultados.id_factor')->join('evaluador_evaluado', function($join){$join->on('evaluador_evaluado.id_evaluado', '=', 'resultados.id_evaluado');$join->on('evaluador_evaluado.id_evaluador', '=', 'resultados.id_evaluador');$join->on('evaluador_evaluado.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('tipos_evaluadores', 'tipos_evaluadores.id', '=', 'evaluador_evaluado.tipo_evaluacion')->where('resultados.id_evaluado', $id_evaluado)->where('resultados.id_periodo', $id_periodo)->select('nivel_desempeno', 'id_factor', 'comentario', 'tipos_evaluadores.nombre AS nombre_evaluacion', 'resultados.id_evaluador', 'factores.nombre', 'boss_id')->orderBy('id_factor')->get();
      $comentario = DB::select("SELECT comentario FROM comentarios_desempeno_jefe WHERE id_evaluado = ? AND id_periodo = ?", [$id_evaluado, $id_periodo]);
      $user = DB::select("SELECT id, first_name, last_name, division, subdivision, boss_id FROM users WHERE id = ?", [$id_evaluado]);
      $jefe = DB::select("SELECT first_name, last_name, division, subdivision FROM users WHERE id = ?", [$user[0]->boss_id]);
      $meses = array(0,'ene','feb','mar','abr','may','jun','jul','ago','sep','oct','nov','dic');
      $competencias = DB::table('periodo_factor')->join('factores', 'periodo_factor.id_factor', '=', 'factores.id')->where('id_periodo', $id_periodo)->select('nombre')->get();
      return view('evaluacion-desempeno/reportes/reporte-consolidado-individual', compact('resultados', 'user', 'jefe', 'meses', 'id_periodo', 'modalidad', 'comentario', 'evaluaciones_terminadas', 'competencias', 'evaluacion_jefe'));
    }

  /**
  * Muestra el Reporte del Colaborador al Jefe
  *
  * @return void
  */
  public function rol_colaborador_jefe(){

    if (Session::has('primer_login')){

      flash('Debes cambiar tu contraseña antes de continuar a otra sección');
      return redirect('/');
    }

    $users = array();
    $id_jefe = auth()->user()->id;
    $id_periodo = 0;
    $status = '';
    $retro = false;
    $periodos = DB::table('periodos')->where('status', 'Abierto')->orWhere('status', 'Cerrado')->select('id', 'descripcion', 'status')->orderBy('id', 'DESC')->get();
      
    if (count($periodos) > 0){

      $id_periodo = $periodos[0]->id;
      $status = $periodos[0]->status;

      if (!empty($_POST['id_periodo'])){

        $id_periodo = $_POST['id_periodo'];

        foreach ($periodos as $key => $periodo){
            
          if ($periodo->id == $id_periodo){

            $status = $periodo->status;
          }
        }
      }

      //if (auth()->user()->role == 'admin' || auth()->user()->role == 'supervisor'){
      if (auth()->user()->role == 'supervisor'){

        // Obtiene los datos de todos los usuarios
        $users = DB::select('SELECT users.first_name, users.last_name, users.id, users.division, users.subdivision, users.boss_id, comentarios_desempeno_jefe.comentario, objetivos_entregables_desempeno.objetivo, evaluador_evaluado.status FROM users INNER JOIN evaluador_evaluado ON (evaluador_evaluado.id_evaluado = users.id) LEFT JOIN comentarios_desempeno_jefe ON (comentarios_desempeno_jefe.id_evaluado = users.id AND comentarios_desempeno_jefe.id_periodo = evaluador_evaluado.id_periodo) LEFT JOIN objetivos_entregables_desempeno ON (objetivos_entregables_desempeno.id_evaluado = users.id AND evaluador_evaluado.id_periodo = objetivos_entregables_desempeno.id_periodo) WHERE evaluador_evaluado.id_periodo = ? AND users.id != ? ORDER BY users.id', [$id_periodo, $id_jefe]);
      }

      else{

        if (auth()->user()->role != 'director' && !auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin')){

          //if ($status == 'Cerrado'){

          // Obtiene los datos de todos los subordinados del usuario logueado
          $users = DB::select('SELECT users.first_name, users.last_name, users.id, users.division, users.subdivision, users.boss_id, comentarios_desempeno_jefe.comentario, objetivos_entregables_desempeno.objetivo, evaluador_evaluado.status, evaluador_evaluado.id_evaluador FROM users INNER JOIN evaluador_evaluado ON (evaluador_evaluado.id_evaluado = users.id) LEFT JOIN comentarios_desempeno_jefe ON (comentarios_desempeno_jefe.id_evaluado = users.id AND comentarios_desempeno_jefe.id_periodo = evaluador_evaluado.id_periodo) LEFT JOIN objetivos_entregables_desempeno ON (objetivos_entregables_desempeno.id_evaluado = users.id AND evaluador_evaluado.id_periodo = objetivos_entregables_desempeno.id_periodo) WHERE users.boss_id = ? AND users.id <> ? AND evaluador_evaluado.id_periodo = ? ORDER BY users.id', [$id_jefe, $id_jefe, $id_periodo]);
          //}

          //else{

            // Obtiene los datos de todos los subordinados del usuario logueado
            //$users = DB::select('SELECT users.first_name, users.last_name, users.id, users.division, users.subdivision, users.boss_id, comentarios_desempeno_jefe.comentario, objetivos_entregables_desempeno.objetivo, evaluador_evaluado.status FROM users INNER JOIN evaluador_evaluado ON (evaluador_evaluado.id_evaluado = users.id) LEFT JOIN comentarios_desempeno_jefe ON (comentarios_desempeno_jefe.id_evaluado = users.id AND comentarios_desempeno_jefe.id_periodo = evaluador_evaluado.id_periodo) LEFT JOIN objetivos_entregables_desempeno ON (objetivos_entregables_desempeno.id_evaluado = users.id AND evaluador_evaluado.id_periodo = objetivos_entregables_desempeno.id_periodo) WHERE users.id = ? AND evaluador_evaluado.id_periodo = ?', [$id_jefe, $id_periodo]);
          //}
        }

        else{

          if ($id_jefe == 1 || $id_jefe == 26 || $id_jefe == 175){

            $id_jefe = 194;
          }

          $users = array($id_jefe);
          $ids = array();
          $nivel = 0;

          do{

            //print_r($users);
            $ids = array_merge($ids, $users);
            $users = DB::table('users')->whereIn('boss_id', $users)->whereNotIn('id', $ids)->pluck('id')->toArray();
            $nivel++;

          }while(count($users) > 0);

          //exit();
          $jefes = DB::table('users')->pluck('boss_id')->toArray();
          $temp_ids = array();

          foreach ($ids as $key => $value){
            
            if (in_array($value, $jefes)){

              $temp_ids[] = $value;
            }
          }

          $ids = $temp_ids;
          $users = DB::table('users')->join('evaluador_evaluado', 'evaluador_evaluado.id_evaluado', '=', 'users.id')->leftJoin('comentarios_desempeno_jefe', function($join){$join->on('comentarios_desempeno_jefe.id_evaluado', '=', 'users.id');$join->on('comentarios_desempeno_jefe.id_periodo', '=', 'evaluador_evaluado.id_periodo');})->leftJoin('objetivos_entregables_desempeno', function($join){$join->on('objetivos_entregables_desempeno.id_evaluado', '=', 'users.id');$join->on('objetivos_entregables_desempeno.id_periodo', '=', 'evaluador_evaluado.id_periodo');})->whereIn('users.id', $ids)->where('evaluador_evaluado.id_periodo', $id_periodo)->select('first_name', 'last_name', 'users.id', 'division', 'subdivision', 'boss_id', 'comentario', 'objetivo', 'status', 'id_evaluador')->orderBy('users.id')->get();
        }
      }

      $temp_users = array();
      $jefes = array();
      $current_user = 0;

      foreach ($users as $key => $user){
      
        if ($current_user != $user->id){

          $user->boss = '';

          if (!empty($user->boss_id)){

            if (!empty($jefes[$user->boss_id])){

              $user->boss = $jefes[$user->boss_id];
            }

            else{

              $jefe = DB::select("SELECT first_name, last_name FROM users WHERE id = ?", [$user->boss_id]);

              if (count($jefe) > 0){

                $user->boss = $jefe[0]->first_name . ' ' . $jefe[0]->last_name;
                $jefes[$user->boss_id] = $user->boss;
              }

              else{

                $jefes[$user->boss_id] = '';
              }
            }
          }

          $current_user = $user->id;

          if (!empty($user->comentario) && !empty($user->objetivo)){

            $retro = true;
          }
        }

        $temp_users[] = $user;
      }
      
      $users = $temp_users;
    }

    return view('evaluacion-desempeno/reportes/rol-colaborador-jefe', compact('users', 'periodos', 'id_periodo', 'status', 'retro'));
  }

  /**
  * Muestra el reporte del Colaborador al Jefe Individual
  */
  public function rol_colaborador_jefe_individual($id_evaluado){

    if (Session::has('primer_login')){

      flash('Debes cambiar tu contraseña antes de continuar a otra sección');
      return redirect('/');
    }

    $id_jefe = auth()->user()->id;
    $competencias = array();
    $evaluacion_jefe = false;
    $user = DB::select("SELECT first_name, last_name, division, subdivision, boss_id FROM users WHERE id = ?", [$id_evaluado]);

    /*if (auth()->user()->role != 'admin' && $user[0]->boss_id != $id_jefe){

      return redirect('/reporte_desempeno')->with('danger','No es jefe del empleado');
    }*/

    $id_periodo = 0;
    $modalidad = 1;

    if (!empty($_POST['id_periodo'])){

      $id_periodo = $_POST['id_periodo'];
      $periodos = DB::select('SELECT id_modalidad FROM periodos WHERE id = ?', [$id_periodo]);
      $modalidad = $periodos[0]->id_modalidad;
    }

    else{

      $periodos = DB::select('SELECT id, id_modalidad FROM periodos WHERE status = ? ORDER BY id DESC', ['Cerrado']);
    
      if (count($periodos) > 0){
      
        $id_periodo = $periodos[0]->id;
        $modalidad = $periodos[0]->id_modalidad;
      }
    }

    $evaluaciones_terminadas = false;
    $evaluaciones = DB::select("SELECT id FROM evaluador_evaluado WHERE id_evaluado = ? AND id_periodo = ?", [$id_evaluado, $id_periodo]);
    $terminadas = DB::select("SELECT id, id_evaluador FROM evaluador_evaluado WHERE id_evaluado = ? AND id_periodo = ? AND status = ?", [$id_evaluado, $id_periodo, 'Terminada']);

    if (count($evaluaciones) > 0 && count($evaluaciones) == count($terminadas)){

      $evaluaciones_terminadas = true;
    }

    if (!auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin') && auth()->user()->role != 'supervisor'){

      foreach ($terminadas as $key => $value){
          
        if ($value->id_evaluador == $id_jefe){

          $evaluacion_jefe = true;
          break;
        }
      }
    }

    else{

      $evaluacion_jefe = true;
    }

    //$resultados = DB::select("SELECT resultados.nivel_desempeno, resultados.id_factor, resultados.comentario, resultados.id_evaluador, factores.nombre, users.boss_id FROM resultados INNER JOIN users ON (users.id = resultados.id_evaluador) INNER JOIN factores ON (factores.id = resultados.id_factor) WHERE id_evaluado = ? AND id_periodo = ? ORDER BY resultados.id_factor", [$id_evaluado, $id_periodo]);
    $resultados = DB::table('resultados')->join('users', 'users.id', '=', 'resultados.id_evaluador')->join('factores', 'factores.id', '=', 'resultados.id_factor')->join('evaluador_evaluado', function($join){$join->on('evaluador_evaluado.id_evaluado', '=', 'resultados.id_evaluado');$join->on('evaluador_evaluado.id_evaluador', '=', 'resultados.id_evaluador');$join->on('evaluador_evaluado.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('tipos_evaluadores', 'tipos_evaluadores.id', '=', 'evaluador_evaluado.tipo_evaluacion')->where('resultados.id_evaluado', $id_evaluado)->where('resultados.id_periodo', $id_periodo)->select('nivel_desempeno', 'id_factor', 'comentario', 'tipos_evaluadores.nombre AS nombre_evaluacion', 'resultados.id_evaluador', 'factores.nombre', 'boss_id')->orderBy('id_factor')->get();
    $comentario = DB::select("SELECT comentario FROM comentarios_desempeno_jefe WHERE id_evaluado = ? AND id_periodo = ?", [$id_evaluado, $id_periodo]);
    $user = DB::select("SELECT id, first_name, last_name, division, subdivision, boss_id FROM users WHERE id = ?", [$id_evaluado]);
    $jefe = DB::select("SELECT first_name, last_name, division, subdivision FROM users WHERE id = ?", [$user[0]->boss_id]);
    $meses = array(0,'ene','feb','mar','abr','may','jun','jul','ago','sep','oct','nov','dic');
    $competencias = DB::table('periodo_factor')->join('factores', 'periodo_factor.id_factor', '=', 'factores.id')->where('id_periodo', $id_periodo)->select('nombre')->get();
    return view('evaluacion-desempeno/reportes/rol-colaborador-jefe-individual', compact('resultados', 'user', 'jefe', 'meses', 'id_periodo', 'modalidad', 'comentario', 'evaluaciones_terminadas', 'competencias', 'evaluacion_jefe'));
  }

    /**
     * Muestra la pagina de Reporte con Graficas
     *
     * @return void
     */
    public function reporte_graficas($id_periodo){

      if (Session::has('primer_login')){

        flash('Debes cambiar tu contraseña antes de continuar a otra sección');
        return redirect('/');
      }

      if (!auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin') && auth()->user()->role != 'supervisor'){

        flash('No tiene permiso de ver la sección');
        return redirect('/');
      }

      $resultados = $users = array();
      //$periodos = DB::select("SELECT id, descripcion FROM periodos WHERE status = ? OR status = ? ORDER BY id DESC", ['Cerrado', 'Abierto']);
      $periodos = DB::table('periodos')->where('status', '<>', 'Preparatorio')->where('status', '<>', 'Cancelado')->select('id', 'descripcion')->orderBy('id', 'DESC')->get();
      
      if (count($periodos) > 0){

        if (empty($id_periodo)){

          if (Session::has('id_periodo')){

            $id_periodo = Session::get('id_periodo');
          }

          else{

            $id_periodo = $periodos[0]->id;
          }
        }

        else{

          Session::put('id_periodo', $id_periodo);
        }

        // Obtiene los datos de todos los usuarios
        //$resultados = DB::select("SELECT SUM(resultados.nivel_desempeno) AS total_evaluacion, COUNT(resultados.id) AS numero_evaluaciones, resultados.id_factor, factores.orden, resultados.id_evaluado, areas.id AS area, puestos.id AS puesto, niveles_puestos.id AS nivel_puesto, grupo_areas.id AS grupo_area, workstation FROM resultados INNER JOIN factores ON (factores.id = resultados.id_factor) INNER JOIN users ON (users.id = resultados.id_evaluado) LEFT JOIN puestos ON (users.subdivision = puestos.puesto) LEFT JOIN areas ON (users.division = areas.nombre) LEFT JOIN puesto_nivel_puesto ON (puestos.id = puesto_nivel_puesto.id_puesto) LEFT JOIN areas_grupo_areas ON (areas.id = areas_grupo_areas.id_areas) LEFT JOIN niveles_puestos ON (niveles_puestos.id = puesto_nivel_puesto.id_nivel_puesto) LEFT JOIN grupo_areas ON (grupo_areas.id = areas_grupo_areas.id_grupos_areas) WHERE resultados.id_periodo = ? GROUP BY resultados.id_evaluado, resultados.id_factor ORDER BY factores.orden", [$id_periodo]);
        //$resultados = DB::table('resultados')->join('factores', 'factores.id', '=', 'resultados.id_factor')->join('users', 'users.id', '=', 'resultados.id_evaluado')->leftJoin('puestos', 'users.subdivision', '=', 'puestos.puesto')->leftJoin('areas', 'users.division', '=', 'areas.nombre')->leftJoin('puesto_nivel_puesto', 'puestos.id', '=', 'puesto_nivel_puesto.id_puesto')->leftJoin('areas_grupo_areas', 'areas.id', '=', 'areas_grupo_areas.id_areas')->leftJoin('niveles_puestos', 'niveles_puestos.id', '=', 'puesto_nivel_puesto.id_nivel_puesto')->leftJoin('grupo_areas', 'grupo_areas.id', '=', 'areas_grupo_areas.id_grupos_areas')->where('id_periodo', $id_periodo)->select(array(DB::raw('SUM(nivel_desempeno) AS total_evaluacion'), DB::raw('COUNT(resultados.id) AS numero_evaluaciones'), 'id_factor', 'orden', 'id_evaluado', 'id_evaluador', 'areas.id AS area', 'puestos.id AS puesto', 'niveles_puestos.id AS nivel_puesto', 'grupo_areas.id AS grupo_area', 'workstation'))->groupBy('id_evaluado', 'id_evaluador', 'id_factor')->orderBy('orden')->get();
        $resultados = DB::table('resultados')->join('factores', 'factores.id', '=', 'resultados.id_factor')->join('users', 'users.id', '=', 'resultados.id_evaluado')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('directions', 'directions.id', '=', 'departments.direction_id')->leftJoin('puesto_nivel_puesto', 'job_positions.id', '=', 'puesto_nivel_puesto.id_puesto')->leftJoin('departments_grupo_areas', 'departments.id', '=', 'departments_grupo_areas.id_departments')->leftJoin('niveles_puestos', 'niveles_puestos.id', '=', 'puesto_nivel_puesto.id_nivel_puesto')->leftJoin('grupo_areas', 'grupo_areas.id', '=', 'departments_grupo_areas.id_grupos_areas')->join('evaluador_evaluado', function($join){$join->on('evaluador_evaluado.id_evaluado', '=', 'resultados.id_evaluado');$join->on('evaluador_evaluado.id_evaluador', '=', 'resultados.id_evaluador');$join->on('evaluador_evaluado.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('tipos_evaluadores', 'tipos_evaluadores.id', '=', 'evaluador_evaluado.tipo_evaluacion')->where('resultados.id_periodo', $id_periodo)->select('nivel_desempeno', 'id_factor', 'orden', 'resultados.id_evaluado', 'resultados.id_evaluador', 'departments.id AS department', 'job_positions.id AS puesto', 'niveles_puestos.id AS nivel_puesto', 'grupo_areas.id AS grupo_area', 'directions.id AS direction', 'jefe', 'idempleado', 'tipos_evaluadores.nombre AS nombre_evaluacion')->orderBy('orden')->orderBy('id_factor')->get();
      }

      $ids_grupos_areas = $ids_niveles_puestos = $ids_departments = $ids_puestos = $ids_direcciones = array();

      foreach ($resultados as $key => $resultado){
        
        if (!empty($resultado->department) && !in_array($resultado->department, $ids_departments)){

          $ids_departments[] = $resultado->department;
        }

        if (!empty($resultado->grupo_area) && !in_array($resultado->grupo_area, $ids_grupos_areas)){

          $ids_grupos_areas[] = $resultado->grupo_area;
        }

        if (!empty($resultado->puesto) && !in_array($resultado->puesto, $ids_puestos)){

          $ids_puestos[] = $resultado->puesto;
        }

        if (!empty($resultado->nivel_puesto) && !in_array($resultado->nivel_puesto, $ids_niveles_puestos)){

          $ids_niveles_puestos[] = $resultado->nivel_puesto;
        }

        if (!empty($resultado->direction) && !in_array($resultado->direction, $ids_direcciones)){

          $ids_direcciones[] = $resultado->direction;
        }
      }

      //$factores = DB::select("SELECT factores.id, factores.nombre FROM factores INNER JOIN periodo_factor ON (periodo_factor.id_factor = factores.id) WHERE periodo_factor.id_periodo = ? ORDER BY factores.orden", [$id_periodo]);
      $factores = DB::table('factores')->join('periodo_factor', 'periodo_factor.id_factor', '=', 'factores.id')->where('periodo_factor.id_periodo', $id_periodo)->select('factores.id', 'factores.nombre')->orderBy('factores.orden')->orderBy('factores.id')->get();

      //$grupo_areas = DB::select("SELECT id, nombre FROM grupo_areas WHERE id != ? ORDER BY nombre", [1]);
      $grupo_areas = DB::table('grupo_areas')->whereIn('id', $ids_grupos_areas)->select('id AS data', 'nombre AS value')->orderBy('nombre')->get();
      //$niveles_puestos = DB::select("SELECT id, nombre FROM niveles_puestos WHERE id != ? ORDER BY nombre", [1]);
      $niveles_puestos = DB::table('niveles_puestos')->whereIn('id', $ids_niveles_puestos)->select('id AS data', 'nombre AS value')->orderBy('nombre')->get();
      //$areas = DB::select("SELECT id, nombre FROM areas WHERE nombre IS NOT NULL AND nombre != '' GROUP BY nombre ORDER BY nombre");
      $departments = DB::table('departments')->leftJoin('departments_grupo_areas', 'departments_grupo_areas.id_departments', '=', 'departments.id')->leftJoin('grupo_areas', 'grupo_areas.id', '=', 'departments_grupo_areas.id_grupos_areas')->whereIn('departments.id', $ids_departments)->select('departments.id AS data', 'departments.name AS value', 'id_grupos_areas', 'direction_id')->groupBy('departments.name')->orderBy('departments.name')->get();
      //$puestos = DB::select("SELECT id, puesto FROM puestos WHERE puesto IS NOT NULL AND puesto != '' ORDER BY puesto");
      $puestos = DB::table('job_positions')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('puesto_nivel_puesto', 'puesto_nivel_puesto.id_puesto', '=', 'job_positions.id')->leftJoin('niveles_puestos', 'niveles_puestos.id', '=', 'puesto_nivel_puesto.id_nivel_puesto')->whereIn('job_positions.id', $ids_puestos)->whereNotNull('job_positions.name')->where('job_positions.name', '!=', '')->select('job_positions.id AS data', 'job_positions.name AS value', 'id_nivel_puesto', 'area_id', 'department_id')->orderBy('job_positions.name')->get();
      //$direcciones = DB::select("SELECT DISTINCT workstation FROM users WHERE workstation IS NOT NULL AND workstation != '' ORDER BY workstation");
      $direcciones = DB::table('directions')->whereIn('id', $ids_direcciones)->whereNotNull('name')->where('directions.name', '!=', '')->select('name AS value', 'id AS data')->orderBy('name')->get();

      $temp_results = array();
      $jefes = array();
      $idsempleados = array();

      foreach ($resultados as $key => $resultado){

        if (empty($jefes[$resultado->id_evaluador])){

          $user = User::withTrashed()->find($resultado->id_evaluador);

          if (!empty($user->employee_wt->boss)){

            $jefes[$resultado->id_evaluador] = $user->employee_wt->boss->user->id;
            $idsempleados[$user->employee_wt->boss->user->id] = $user->employee_wt->jefe;
          }

          $idsempleados[$resultado->id_evaluador] = $user->employee_wt->idempleado;
        }

        if (!empty($jefes[$resultado->id_evaluador])){

          $resultado->id_jefe_evaluador = $jefes[$resultado->id_evaluador];
          $resultado->idempleado_jefe_evaluador = $idsempleados[$jefes[$resultado->id_evaluador]];
        }

        else{

          $resultado->id_jefe_evaluador = 0;
          $resultado->idempleado_jefe_evaluador = 0;
        }

        $resultado->idempleado_evaluador = $idsempleados[$resultado->id_evaluador];
        $temp_results[] = $resultado;
      }

      $resultados = $temp_results;

      /*$temp_direcciones = array();
      $actual_direccion = $actual_area = $actual_puesto = '';
      $contador_direccion = $contador_area = -1;

      foreach ($direcciones as $key => $direccion){
        
        if ($actual_direccion != $direccion->workstation){

          $contador_direccion++;
          $temp_direcciones[$contador_direccion] = new \stdClass();
          $temp_direcciones[$contador_direccion]->workstation = $direccion->workstation;
          $temp_direcciones[$contador_direccion]->areas = array();
          $contador_area = -1;
          $actual_direccion = $direccion->workstation;
        }

        if ($actual_area != $direccion->division){

          $contador_area++;
          $temp_direcciones[$contador_direccion]->areas[$contador_area] = new \stdClass();
          $temp_direcciones[$contador_direccion]->areas[$contador_area]->nombre = $direccion->division;
          $temp_direcciones[$contador_direccion]->areas[$contador_area]->puestos = array();
          $actual_area = $direccion->division;
        }

        if ($actual_puesto != $direccion->subdivision){

          $temp_direcciones[$contador_direccion]->areas[$contador_area]->puestos[] = $direccion->subdivision;
          $actual_puesto = $direccion->subdivision;
        }
      }

      $direcciones = $temp_direcciones;*/
      $tipos_evaluadores = DB::table('tipos_evaluadores')->pluck('descripcion_empresa', 'id')->toArray();
      return view('evaluacion-desempeno/reportes/reporte-graficas', compact('resultados', 'periodos', 'factores', 'id_periodo', 'grupo_areas', 'niveles_puestos', 'departments', 'puestos', 'direcciones', 'tipos_evaluadores'));
    }

    /**
     * Muestra la pagina de Reporte con Graficas para las Competencias
     *
     * @return void
     */
    public function reporte_graficas_competencias($id_periodo){

      if (Session::has('primer_login')){

        flash('Debes cambiar tu contraseña antes de continuar a otra sección');
        return redirect('/');
      }

      if (!auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin') && auth()->user()->role != 'supervisor'){

        flash('No tiene permiso de ver la sección');
        return redirect('/');
      }

      $resultados = array();
      $periodos = DB::table('periodos')->where('status', '<>', 'Preparatorio')->where('status', '<>', 'Cancelado')->select('id', 'descripcion')->orderBy('id', 'DESC')->get();
      
      if (count($periodos) > 0){

        if (empty($id_periodo)){

          $id_periodo = $periodos[0]->id;
        }

        // Obtiene los datos de todos los usuarios
        $resultados = DB::table('resultados')->join('factores', 'factores.id', '=', 'resultados.id_factor')->join('users', 'users.id', '=', 'resultados.id_evaluado')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('directions', 'directions.id', '=', 'departments.direction_id')->leftJoin('puesto_nivel_puesto', 'job_positions.id', '=', 'puesto_nivel_puesto.id_puesto')->leftJoin('departments_grupo_areas', 'departments.id', '=', 'departments_grupo_areas.id_departments')->leftJoin('niveles_puestos', 'niveles_puestos.id', '=', 'puesto_nivel_puesto.id_nivel_puesto')->leftJoin('grupo_areas', 'grupo_areas.id', '=', 'departments_grupo_areas.id_grupos_areas')->where('id_periodo', $id_periodo)->select(array(DB::raw('AVG(nivel_desempeno) AS nivel_desempeno'), 'id_factor', 'id_evaluado', 'id_evaluador', 'areas.id AS area', 'departments.id AS department', 'job_positions.id AS puesto', 'niveles_puestos.id AS nivel_puesto', 'grupo_areas.id AS grupo_area', 'directions.id AS direction'))->groupBy('id_factor')->orderBy('orden')->orderBy('id_factor')->get();
      }

      $factores = DB::table('factores')->join('periodo_factor', 'periodo_factor.id_factor', '=', 'factores.id')->where('periodo_factor.id_periodo', $id_periodo)->select('factores.id', 'factores.nombre')->orderBy('factores.orden')->orderBy('factores.id')->get();
      $grupo_areas = DB::table('grupo_areas')->select('id', 'nombre')->orderBy('nombre')->get();
      $niveles_puestos = DB::table('niveles_puestos')->select('id', 'nombre')->orderBy('nombre')->get();
      $areas = DB::table('areas')->leftJoin('departments', 'departments.id', 'areas.department_id')->leftJoin('departments_grupo_areas', 'departments_grupo_areas.id_departments', '=', 'departments.id')->leftJoin('grupo_areas', 'grupo_areas.id', '=', 'departments_grupo_areas.id_grupos_areas')->whereNotNull('areas.name')->where('areas.name', '!=', '')->select('areas.id', 'areas.name', 'id_grupos_areas', 'department_id')->groupBy('areas.name')->orderBy('areas.name')->get();
      $direcciones = DB::table('directions')->whereNotNull('name')->where('name', '!=', '')->select('id', 'name')->orderBy('name')->get();
      /*$temp_direcciones = array();
      $actual_direccion = $actual_area = '';
      $contador_direccion = -1;

      foreach ($direcciones as $key => $direccion){
        
        if ($actual_direccion != $direccion->workstation){

          $contador_direccion++;
          $temp_direcciones[$contador_direccion] = new \stdClass();
          $temp_direcciones[$contador_direccion]->workstation = $direccion->workstation;
          $temp_direcciones[$contador_direccion]->areas = array();
          $actual_direccion = $direccion->workstation;
        }

        if ($actual_area != $direccion->division){

          $temp_direcciones[$contador_direccion]->areas[] = $direccion->division;
          $actual_area = $direccion->division;
        }
      }

      $direcciones = $temp_direcciones;*/
      return view('evaluacion-desempeno/reportes/reporte-graficas-competencias', compact('resultados', 'periodos', 'factores', 'id_periodo', 'grupo_areas', 'niveles_puestos', 'areas', 'direcciones'));
    }
  
    /**
     * Muestra el reporte de todos los empleados
     */
    public function reporte_desempeno_completo($id_periodo){

      if (Session::has('primer_login')){

        flash('Debes cambiar tu contraseña antes de continuar a otra sección');
        return redirect('/');
      }

      $resultados = $results = array();
      $factores = $calificaciones_factores = array();
      $ponderaciones = array();
      $niveles_puestos = $posiciones = array();
      //$periodos = DB::select("SELECT id, descripcion FROM periodos WHERE status = ? OR status = ? ORDER BY id DESC", ['Cerrado', 'Abierto']);
      $periodos = DB::table('periodos')->where('status', '<>', 'Preparatorio')->where('status', '<>', 'Cancelado')->select('id', 'descripcion')->orderBy('id', 'DESC')->get();
      $tipos_evaluadores = DB::table('tipos_evaluadores')->pluck('descripcion_empresa', 'id')->toArray();
      $pesos_factores = array();
      $niveles_esperados = DB::table('factores_niveles_puestos')->select('id_nivel_puesto', 'id_factor', 'peso')->get();

      foreach ($niveles_esperados as $key => $value){
          
        if (!isset($pesos_factores[$value->id_nivel_puesto])){

          $pesos_factores[$value->id_nivel_puesto] = array();
        }

        $pesos_factores[$value->id_nivel_puesto][$value->id_factor] = $value->peso;
      }

      if (count($periodos) > 0){

        if (empty($id_periodo)){

          if (Session::has('id_periodo')){

            $id_periodo = Session::get('id_periodo');
          }

          else{

            $id_periodo = $periodos[0]->id;
          }
        }

        else{

          Session::put('id_periodo', $id_periodo);
        }

        $ponderaciones = DB::table('ponderaciones')->where('id_periodo', $id_periodo)->select('id_nivel_puesto', 'jefe', 'autoevaluacion', 'colaborador', 'par', 'psicometricos')->orderBy('id_nivel_puesto')->get();

        // No hay ponderaciones registradas para el periodo
        if (count($ponderaciones) == 0){

          $last = DB::table('ponderaciones')->select('id_periodo')->orderBy('created_at', 'DESC')->first();

          if (!empty($last->id_periodo)){

            // Se obtienen las ultimas ponderaciones registradas
            $ponderaciones = DB::table('ponderaciones')->where('id_periodo', $last->id_periodo)->select('id_nivel_puesto', 'jefe', 'autoevaluacion', 'colaborador', 'par', 'psicometricos')->orderBy('id_nivel_puesto')->get();
          }
        }

        $ponderaciones_separadas = array();

        foreach ($ponderaciones as $key => $value){

          $ponderaciones_separadas[$value->id_nivel_puesto] = $value;
        }

        if (!auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin') && auth()->user()->role != 'supervisor'){

          //$users = DB::select("SELECT id FROM users WHERE boss_id = ?", [auth()->user()->id]);
          $users = DB::table('employees')->where('jefe', auth()->user()->employee->idempleado)->select('id')->get();

          if (count($users) == 0){

            return redirect('/')->with('danger','No tiene gente a su cargo');
          }

          $id_jefe = auth()->user()->id;
          $nombre_jefe = auth()->user()->last_name . ' ' . auth()->user()->first_name;

          if (auth()->user()->role != 'director'){

            //$resultados = DB::select("SELECT SUM(resultados.nivel_desempeno) AS total_evaluacion, COUNT(resultados.id) AS numero_evaluaciones, resultados.id_factor, resultados.id_evaluado, puesto_nivel_puesto.id_nivel_puesto, factores.orden, comentarios_desempeno_jefe.comentario, objetivos_entregables_desempeno.objetivo FROM resultados INNER JOIN users ON (resultados.id_evaluado = users.id) LEFT JOIN puestos ON (puestos.puesto = users.subdivision) LEFT JOIN puesto_nivel_puesto ON (id_puesto = puestos.id) LEFT JOIN comentarios_desempeno_jefe ON (comentarios_desempeno_jefe.id_evaluado = resultados.id_evaluado) LEFT JOIN objetivos_entregables_desempeno ON (objetivos_entregables_desempeno.id_evaluado = resultados.id_evaluado) INNER JOIN factores ON (factores.id = resultados.id_factor) WHERE resultados.id_periodo = ? AND users.boss_id = ? GROUP BY resultados.id_evaluado, resultados.id_factor ORDER BY resultados.id_evaluado, factores.orden", [$id_periodo, auth()->user()->id]);
            $resultados = DB::table('resultados')->join('users', 'users.id', '=', 'resultados.id_evaluado')->join('employees', 'employees.id', '=', 'users.employee_id')->join('factores', 'factores.id', '=', 'resultados.id_factor')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('puesto_nivel_puesto', 'puesto_nivel_puesto.id_puesto', '=', 'job_positions.id')->leftJoin('comentarios_desempeno_jefe', function($join){$join->on('comentarios_desempeno_jefe.id_evaluado', '=', 'resultados.id_evaluado');$join->on('comentarios_desempeno_jefe.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('objetivos_entregables_desempeno', function($join){$join->on('objetivos_entregables_desempeno.id_evaluado', '=', 'resultados.id_evaluado');$join->on('objetivos_entregables_desempeno.id_periodo', '=', 'resultados.id_periodo');})->where('resultados.id_periodo', $id_periodo)->where('boss_id', auth()->user()->id)->select(array(DB::raw('SUM(nivel_desempeno) AS total_evaluacion'), DB::raw('COUNT(resultados.id) AS numero_evaluaciones'), 'id_factor', 'resultados.id_evaluado', 'id_nivel_puesto', 'orden', 'comentarios_desempeno_jefe.comentario', 'objetivo', 'idempleado'))->groupBy('resultados.id_evaluado', 'id_factor')->orderBy('resultados.id_evaluado')->orderBy('orden')->orderBy('id_factor')->get();
            //$results = DB::select("SELECT SUM(nivel_desempeno) AS total_evaluacion, COUNT(resultados.id) AS numero_evaluaciones, id_evaluado, id_evaluador, first_name, last_name, division, workstation FROM resultados INNER JOIN users ON (id_evaluado = users.id) WHERE id_periodo = ? AND boss_id = ? GROUP BY id_evaluado, id_evaluador ORDER BY id_evaluado", [$id_periodo, $id_jefe]);
            $results = DB::table('resultados')->join('users', 'users.id', '=', 'resultados.id_evaluado')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('puesto_nivel_puesto', 'puesto_nivel_puesto.id_puesto', '=', 'job_positions.id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('directions', 'directions.id', '=', 'departments.direction_id')->join('evaluador_evaluado', function($join){$join->on('evaluador_evaluado.id_evaluado', '=', 'resultados.id_evaluado');$join->on('evaluador_evaluado.id_evaluador', '=', 'resultados.id_evaluador');$join->on('evaluador_evaluado.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('tipos_evaluadores', 'tipos_evaluadores.id', '=', 'evaluador_evaluado.tipo_evaluacion')->where('resultados.id_periodo', $id_periodo)->where('jefe', auth()->user()->employee->idempleado)->select(array(DB::raw('SUM(nivel_desempeno) AS total_evaluacion'), DB::raw('COUNT(resultados.id) AS numero_evaluaciones'), 'id_factor', 'resultados.id_evaluado', 'resultados.id_evaluador', 'tipos_evaluadores.nombre', 'first_name', 'last_name', 'areas.name AS area', 'departments.name AS department', 'directions.name AS direction', 'id_nivel_puesto'))->groupBy('resultados.id_evaluado', 'resultados.id_evaluador', 'id_factor')->orderBy('resultados.id_evaluado')->orderBy('id_factor')->get();
          }

          else{

            $ids = array();
            $users = array(auth()->user()->employee->idempleado);

            do{

              $ids = array_merge($ids, $users);
              $users = DB::table('employees')->whereIn('jefe', $users)->pluck('idempleado')->toArray();

            }while(count($users) > 0);

            $resultados = DB::table('resultados')->join('users', 'users.id', '=', 'resultados.id_evaluado')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('factores', 'factores.id', '=', 'resultados.id_factor')->leftJoin('puesto_nivel_puesto', 'puesto_nivel_puesto.id_puesto', '=', 'job_positions.id')->leftJoin('comentarios_desempeno_jefe', function($join){$join->on('comentarios_desempeno_jefe.id_evaluado', '=', 'resultados.id_evaluado');$join->on('comentarios_desempeno_jefe.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('objetivos_entregables_desempeno', function($join){$join->on('objetivos_entregables_desempeno.id_evaluado', '=', 'resultados.id_evaluado');$join->on('objetivos_entregables_desempeno.id_periodo', '=', 'resultados.id_periodo');})->where('resultados.id_periodo', $id_periodo)->whereIn('jefe', $ids)->select(array(DB::raw('SUM(nivel_desempeno) AS total_evaluacion'), DB::raw('COUNT(resultados.id) AS numero_evaluaciones'), 'id_factor', 'resultados.id_evaluado', 'id_nivel_puesto', 'orden', 'comentarios_desempeno_jefe.comentario', 'objetivo', 'idempleado'))->groupBy('resultados.id_evaluado', 'id_factor')->orderBy('resultados.id_evaluado')->orderBy('orden')->orderBy('id_factor')->get();
            //$results = DB::select("SELECT SUM(nivel_desempeno) AS total_evaluacion, COUNT(resultados.id) AS numero_evaluaciones, id_evaluado, id_evaluador, first_name, last_name, division, workstation FROM resultados INNER JOIN users ON (id_evaluado = users.id) WHERE id_periodo = ? AND boss_id = ? GROUP BY id_evaluado, id_evaluador ORDER BY id_evaluado", [$id_periodo, $id_jefe]);
            $results = DB::table('resultados')->join('users', 'users.id', '=', 'resultados.id_evaluado')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('puesto_nivel_puesto', 'puesto_nivel_puesto.id_puesto', '=', 'job_positions.id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('directions', 'directions.id', '=', 'departments.direction_id')->join('evaluador_evaluado', function($join){$join->on('evaluador_evaluado.id_evaluado', '=', 'resultados.id_evaluado');$join->on('evaluador_evaluado.id_evaluador', '=', 'resultados.id_evaluador');$join->on('evaluador_evaluado.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('tipos_evaluadores', 'tipos_evaluadores.id', '=', 'evaluador_evaluado.tipo_evaluacion')->where('resultados.id_periodo', $id_periodo)->whereIn('jefe', $ids)->select(array(DB::raw('SUM(nivel_desempeno) AS total_evaluacion'), DB::raw('COUNT(resultados.id) AS numero_evaluaciones'), 'id_factor', 'resultados.id_evaluado', 'resultados.id_evaluador', 'tipos_evaluadores.nombre', 'first_name', 'last_name', 'areas.name AS area', 'departments.name AS department', 'directions.name AS direction', 'id_nivel_puesto'))->groupBy('resultados.id_evaluado', 'resultados.id_evaluador', 'id_factor')->orderBy('resultados.id_evaluado')->orderBy('id_factor')->get();
          }

          $promedios_tipo_evaluacion = array();
          $autoevaluacion = 0;
          $jefe = 0;
          $colaborador = 0;
          $par = 0;
          $psicometricos = 0;
          $jefes = array();
          $evaluaciones_par = 0;
          $evaluaciones_psicometricos = 0;
          $evaluaciones_colaborador = 0;
          $total_par = 0;
          $total_psicometricos = 0;
          $total_colaborador = 0;
          $actual_factor = 0;
          $final_autoevaluacion = $final_par = $final_colaborador = $final_cliente = $final_jefe = $final_ponderado = 0;
          $contador_autoevaluacion = $contador_jefe = $contador_colaborador = $contador_par = $contador_clientes = $contador_promedio = 0;
          $totales_factores = array();

          foreach ($results as $key => $value){
            
            $actual_evaluado = $value->id_evaluado;

            if ($actual_factor != $value->id_factor){

              if ($actual_factor != 0){

                //if (isset($pesos_factores[$value->id_nivel_puesto][$actual_factor])){

                  $ponderacion_a_usar = 0;
                  $total_sin_ponderacion = 0;
                  $contador_sin_ponderacion = 0;
                  $promedio = 0;

                  if ($autoevaluacion != 0){

                    if ($ponderaciones_separadas[$value->id_nivel_puesto]->autoevaluacion != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->autoevaluacion;
                      $promedio += $autoevaluacion * $ponderaciones_separadas[$value->id_nivel_puesto]->autoevaluacion / 100;
                    }

                    else{

                      $total_sin_ponderacion += $autoevaluacion;
                      $contador_sin_ponderacion++;
                    }
                  }

                  if ($jefe != 0){

                    if ($ponderaciones_separadas[$value->id_nivel_puesto]->jefe != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->jefe;
                      $promedio += $jefe * $ponderaciones_separadas[$value->id_nivel_puesto]->jefe / 100;
                    }

                    else{

                      $total_sin_ponderacion += $jefe;
                      $contador_sin_ponderacion++;
                    }
                  }

                  if ($evaluaciones_par != 0){

                    if ($ponderaciones_separadas[$value->id_nivel_puesto]->par != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->par;
                      $promedio += ($total_par / $evaluaciones_par) * $ponderaciones_separadas[$value->id_nivel_puesto]->par / 100;
                    }

                    else{

                      $total_sin_ponderacion += $total_par;
                      $contador_sin_ponderacion += $evaluaciones_par;
                    }
                  }

                  if ($evaluaciones_colaborador != 0){

                    if ($ponderaciones_separadas[$value->id_nivel_puesto]->colaborador != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->colaborador;
                      $promedio += ($total_colaborador / $evaluaciones_colaborador) * $ponderaciones_separadas[$value->id_nivel_puesto]->colaborador / 100;
                    }

                    else{

                      $total_sin_ponderacion += $total_colaborador;
                      $contador_sin_ponderacion += $evaluaciones_colaborador;
                    }
                  }

                  if ($evaluaciones_psicometricos != 0){

                    if ($ponderaciones_separadas[$value->id_nivel_puesto]->psicometricos != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->psicometricos;
                      $promedio += ($total_psicometricos / $evaluaciones_psicometricos) * $ponderaciones_separadas[$value->id_nivel_puesto]->psicometricos / 100;
                    }

                    else{

                      $total_sin_ponderacion += $total_psicometricos;
                      $contador_sin_ponderacion += $evaluaciones_psicometricos;
                    }
                  }

                  if ($total_sin_ponderacion != 0){

                    $total_sin_ponderacion = $total_sin_ponderacion / $contador_sin_ponderacion;
                    $ponderacion_restante = 100 - $ponderacion_a_usar;
                    $promedio += $total_sin_ponderacion * $ponderacion_restante / 100;
                  }

                  else{

                    if ($ponderacion_a_usar < 100){

                      $ponderacion_restante = 100 - $ponderacion_a_usar;
                      $promedio += $ponderacion_restante;
                    }
                  }

                  if ($colaborador != 0){
                    $contador_colaborador++;
                  }
                  if ($psicometricos != 0){
                    $contador_clientes++;
                  }
                  if ($par != 0){
                    $contador_par++;
                  }
                  $contador_promedio++;
                  $totales_factores[$actual_factor] = $promedio;

                  /*$promedio = $total_colaborador + $total_par + $total_psicometricos;
                  $promedio = $promedio / ($evaluaciones_colaborador + $evaluaciones_psicometricos + $evaluaciones_par);
                  $promedio = $promedio * 71 / 100;
                  $promedio = $promedio + ($autoevaluacion * 4 / 100) + ($jefe * 25 / 100);*/
                  $final_ponderado += $promedio/* * $pesos_factores[$value->id_nivel_puesto][$actual_factor] / 105*/;
                  $final_autoevaluacion += $autoevaluacion/* * $pesos_factores[$value->id_nivel_puesto][$actual_factor] / 105*/;
                  $final_jefe += $jefe/* * $pesos_factores[$id_nivel_puesto][$actual_factor] / 105*/;
                  $final_colaborador += $colaborador/* * $pesos_factores[$id_nivel_puesto][$actual_factor] / 105*/;
                  $final_cliente += $psicometricos/* * $pesos_factores[$id_nivel_puesto][$actual_factor] / 105*/;
                  $final_par += $par/* * $pesos_factores[$id_nivel_puesto][$actual_factor] / 105*/;
                //}
              }

              $actual_factor = $value->id_factor;
              $autoevaluacion = $jefe = $colaborador = $psicometricos = $par = 0;
              $evaluaciones_colaborador = $evaluaciones_psicometricos = $evaluaciones_par = 0;
              $total_colaborador = $total_psicometricos = $total_par = 0;
            }

            if (!empty($value->nombre)){

              if ($value->nombre == 'AUTOEVALUACION'){

                $autoevaluacion = $value->total_evaluacion / $value->numero_evaluaciones;
                $contador_autoevaluacion++;
              }

              else{

                if ($value->nombre == 'JEFE'){

                  $jefe = $value->total_evaluacion / $value->numero_evaluaciones;
                  $contador_jefe++;

                }

                else{

                  if ($value->nombre == 'SUBORDINADO'){

                    $evaluaciones_colaborador += $value->numero_evaluaciones;
                    $total_colaborador += $value->total_evaluacion;
                    $colaborador = $total_colaborador / $evaluaciones_colaborador;
                  }

                  else{

                    if ($value->nombre == 'CLIENTES'){

                      $evaluaciones_psicometricos += $value->numero_evaluaciones;
                      $total_psicometricos += $value->total_evaluacion;
                      $psicometricos = $total_psicometricos / $evaluaciones_psicometricos;
                    }

                    else{

                      $evaluaciones_par += $value->numero_evaluaciones;
                      $total_par += $value->total_evaluacion;
                      $par = $total_par / $evaluaciones_par;
                    }
                  }
                }
              }
            }

            else{

              if ($value->id_evaluador == $value->id_evaluado){

                $autoevaluacion = $value->total_evaluacion / $value->numero_evaluaciones;
                $contador_autoevaluacion++;
              }

              else{

                if ($value->id_evaluador == $id_jefe){

                  $jefe = $value->total_evaluacion / $value->numero_evaluaciones;
                  $contador_jefe++;
                }

                else{

                  $jefe_evaluador = 0;

                  if (!empty($jefes[$value->id_evaluador])){

                    $jefe_evaluador = $jefes[$value->id_evaluador];
                  }

                  else{

                    //$result = DB::select("SELECT boss_id FROM users WHERE id = ?", [$value->id_evaluador]);
                    //$result = DB::table('users')->where('id', $value->id_evaluador)->select('boss_id')->first();
                    $result = User::find($value->id_evaluador);
                    $jefe_evaluador = $result->employee->boss->user->id;
                    $jefes[$value->id_evaluador] = $jefe_evaluador;
                  }

                  if ($jefe_evaluador == $value->id_evaluado){

                    $evaluaciones_colaborador += $value->numero_evaluaciones;
                    $total_colaborador += $value->total_evaluacion;
                    $colaborador = $total_colaborador / $evaluaciones_colaborador;
                  }

                  else{

                    if ($jefe_evaluador == $id_jefe){

                      $evaluaciones_par += $value->numero_evaluaciones;
                      $total_par += $value->total_evaluacion;
                      $par = $total_par / $evaluaciones_par;
                    }

                    else{

                      $evaluaciones_psicometricos += $value->numero_evaluaciones;
                      $total_psicometricos += $value->total_evaluacion;
                      $psicometricos = $total_psicometricos / $evaluaciones_psicometricos;
                    }
                  }
                }
              }
            }

            if (empty($results[$key + 1]) || $results[$key + 1]->id_evaluado != $actual_evaluado){

              //if (isset($pesos_factores[$value->id_nivel_puesto][$actual_factor])){

                $ponderacion_a_usar = 0;
                  $total_sin_ponderacion = 0;
                $contador_sin_ponderacion = 0;
                  $promedio = 0;

                  if ($autoevaluacion != 0){

                    if ($ponderaciones_separadas[$value->id_nivel_puesto]->autoevaluacion != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->autoevaluacion;
                      $promedio += $autoevaluacion * $ponderaciones_separadas[$value->id_nivel_puesto]->autoevaluacion / 100;
                    }

                    else{

                      $total_sin_ponderacion += $autoevaluacion;
                      $contador_sin_ponderacion++;
                    }
                  }

                  if ($jefe != 0){

                    if ($ponderaciones_separadas[$value->id_nivel_puesto]->jefe != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->jefe;
                      $promedio += $jefe * $ponderaciones_separadas[$value->id_nivel_puesto]->jefe / 100;
                    }

                    else{

                      $total_sin_ponderacion += $jefe;
                      $contador_sin_ponderacion++;
                    }
                  }

                  if ($evaluaciones_par != 0){

                    if ($ponderaciones_separadas[$value->id_nivel_puesto]->par != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->par;
                      $promedio += ($total_par / $evaluaciones_par) * $ponderaciones_separadas[$value->id_nivel_puesto]->par / 100;
                    }

                    else{

                      $total_sin_ponderacion += $total_par;
                      $contador_sin_ponderacion += $evaluaciones_par;
                    }
                  }

                  if ($evaluaciones_colaborador != 0){

                    if ($ponderaciones_separadas[$value->id_nivel_puesto]->colaborador != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->colaborador;
                      $promedio += ($total_colaborador / $evaluaciones_colaborador) * $ponderaciones_separadas[$value->id_nivel_puesto]->colaborador / 100;
                    }

                    else{

                      $total_sin_ponderacion += $total_colaborador;
                      $contador_sin_ponderacion += $evaluaciones_colaborador;
                    }
                  }

                  if ($evaluaciones_psicometricos != 0){

                    if ($ponderaciones_separadas[$value->id_nivel_puesto]->psicometricos != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->psicometricos;
                      $promedio += ($total_psicometricos / $evaluaciones_psicometricos) * $ponderaciones_separadas[$value->id_nivel_puesto]->psicometricos / 100;
                    }

                    else{

                      $total_sin_ponderacion += $total_psicometricos;
                      $contador_sin_ponderacion += $evaluaciones_psicometricos;
                    }
                  }

                  if ($total_sin_ponderacion != 0){

                    $total_sin_ponderacion = $total_sin_ponderacion / $contador_sin_ponderacion;
                    $ponderacion_restante = 100 - $ponderacion_a_usar;
                    $promedio += $total_sin_ponderacion * $ponderacion_restante / 100;
                  }

                  else{

                    if ($ponderacion_a_usar < 100){

                      $ponderacion_restante = 100 - $ponderacion_a_usar;
                      $promedio += $ponderacion_restante;
                    }
                  }

                  if ($colaborador != 0){
                    $contador_colaborador++;
                  }
                  if ($psicometricos != 0){
                    $contador_clientes++;
                  }
                  if ($par != 0){
                    $contador_par++;
                  }
                  $contador_promedio++;
                  $totales_factores[$actual_factor] = $promedio;
                  $calificaciones_factores[$actual_evaluado] = $totales_factores;
                /*$promedio = $total_colaborador + $total_par + $total_psicometricos;
                $promedio = $promedio / ($evaluaciones_colaborador + $evaluaciones_psicometricos + $evaluaciones_par);
                $promedio = $promedio * 71 / 100;
                $promedio = $promedio + ($autoevaluacion * 4 / 100) + ($jefe * 25 / 100);*/
                $final_ponderado += $promedio/* * $pesos_factores[$value->id_nivel_puesto][$actual_factor] / 105*/;
                $final_autoevaluacion += $autoevaluacion/* * $pesos_factores[$id_nivel_puesto][$actual_factor] / 105*/;
                $final_jefe += $jefe/* * $pesos_factores[$id_nivel_puesto][$actual_factor] / 105*/;
                $final_colaborador += $colaborador/* * $pesos_factores[$id_nivel_puesto][$actual_factor] / 105*/;
                $final_cliente += $psicometricos/* * $pesos_factores[$id_nivel_puesto][$actual_factor] / 105*/;
                $final_par += $par/* * $pesos_factores[$id_nivel_puesto][$actual_factor] / 105*/;
              //}
                if ($contador_autoevaluacion == 0){
                  $contador_autoevaluacion = 1;
                }
                if ($contador_jefe == 0){
                  $contador_jefe = 1;
                }
                if ($contador_colaborador == 0){
                  $contador_colaborador = 1;
                }
                if ($contador_par == 0){
                  $contador_par = 1;
                }
                if ($contador_clientes == 0){
                  $contador_clientes = 1;
                }
                if ($contador_promedio == 0){
                  $contador_promedio = 1;
                }

              $value->promedio_autoevaluacion = $final_autoevaluacion / $contador_autoevaluacion;
              $value->promedio_jefe = $final_jefe / $contador_jefe;
              $value->promedio_colaborador = $final_colaborador / $contador_colaborador;
              $value->promedio_par = $final_par / $contador_par;
              $value->promedio_psicometricos = $final_cliente / $contador_clientes;
              $value->jefe = $nombre_jefe;
              $value->promedio_ponderado = $final_ponderado / $contador_promedio;
              $value->totales_factores = $totales_factores;
              $promedios_tipo_evaluacion[] = $value;
              $autoevaluacion = $jefe = $colaborador = $par = $psicometricos = $evaluaciones_colaborador = $evaluaciones_par = $evaluaciones_psicometricos = $total_colaborador = $total_par = $total_psicometricos = 0;
              $final_cliente = $final_par = $final_colaborador = $final_jefe = $final_autoevaluacion = $final_ponderado = 0;
              $contador_autoevaluacion = $contador_jefe = $contador_colaborador = $contador_par = $contador_clientes = $contador_promedio = 0;
              $actual_factor = 0;
              $totales_factores = array();
            }
          }
        }

        else{

          //$resultados = DB::select("SELECT SUM(resultados.nivel_desempeno) AS total_evaluacion, COUNT(resultados.id) AS numero_evaluaciones, resultados.id_factor, resultados.id_evaluado, puesto_nivel_puesto.id_nivel_puesto, factores.orden, comentarios_desempeno_jefe.comentario, objetivos_entregables_desempeno.objetivo FROM resultados INNER JOIN users ON (resultados.id_evaluado = users.id) LEFT JOIN puestos ON (puestos.puesto = users.subdivision) LEFT JOIN puesto_nivel_puesto ON (id_puesto = puestos.id) LEFT JOIN comentarios_desempeno_jefe ON (comentarios_desempeno_jefe.id_evaluado = resultados.id_evaluado) LEFT JOIN objetivos_entregables_desempeno ON (objetivos_entregables_desempeno.id_evaluado = resultados.id_evaluado) INNER JOIN factores ON (factores.id = resultados.id_factor) WHERE resultados.id_periodo = ? GROUP BY resultados.id_evaluado, resultados.id_factor ORDER BY resultados.id_evaluado, factores.orden", [$id_periodo]);
          $resultados = DB::table('resultados')->join('users', 'users.id', '=', 'resultados.id_evaluado')->join('employees', 'employees.id', '=', 'users.employee_id')->join('factores', 'factores.id', '=', 'resultados.id_factor')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('puesto_nivel_puesto', 'puesto_nivel_puesto.id_puesto', '=', 'job_positions.id')->leftJoin('comentarios_desempeno_jefe', function($join){$join->on('comentarios_desempeno_jefe.id_evaluado', '=', 'resultados.id_evaluado');$join->on('comentarios_desempeno_jefe.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('objetivos_entregables_desempeno', function($join){$join->on('objetivos_entregables_desempeno.id_evaluado', '=', 'resultados.id_evaluado');$join->on('objetivos_entregables_desempeno.id_periodo', '=', 'resultados.id_periodo');})->where('resultados.id_periodo', $id_periodo)->select(array(DB::raw('SUM(nivel_desempeno) AS total_evaluacion'), DB::raw('COUNT(resultados.id) AS numero_evaluaciones'), 'id_factor', 'resultados.id_evaluado', 'id_nivel_puesto', 'orden', 'comentarios_desempeno_jefe.comentario', 'objetivo', 'idempleado'))->groupBy('resultados.id_evaluado', 'id_factor')->orderBy('resultados.id_evaluado')->orderBy('orden')->orderBy('id_factor')->get();
          //$results = DB::select("SELECT SUM(nivel_desempeno) AS total_evaluacion, COUNT(resultados.id) AS numero_evaluaciones, id_evaluado, id_evaluador, first_name, last_name, division, workstation, boss_id FROM resultados INNER JOIN users ON (id_evaluado = users.id) WHERE id_periodo = ? GROUP BY id_evaluado, id_evaluador ORDER BY id_evaluado", [$id_periodo]);
          /*$results = DB::table('resultados')->join('users', 'users.id', '=', 'resultados.id_evaluado')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('puesto_nivel_puesto', 'puesto_nivel_puesto.id_puesto', '=', 'job_positions.id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('directions', 'directions.id', '=', 'departments.direction_id')->join('evaluador_evaluado', function($join){$join->on('evaluador_evaluado.id_evaluado', '=', 'resultados.id_evaluado');$join->on('evaluador_evaluado.id_evaluador', '=', 'resultados.id_evaluador');$join->on('evaluador_evaluado.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('tipos_evaluadores', 'tipos_evaluadores.id', '=', 'evaluador_evaluado.tipo_evaluacion')->where('resultados.id_periodo', $id_periodo)->select(array(DB::raw('SUM(nivel_desempeno) AS total_evaluacion'), DB::raw('COUNT(resultados.id) AS numero_evaluaciones'), 'id_factor', 'resultados.id_evaluado', 'resultados.id_evaluador', 'tipos_evaluadores.nombre', 'first_name', 'last_name', 'areas.name AS area', 'departments.name AS department', 'directions.name AS direction', 'jefe', 'id_nivel_puesto'))->groupBy('resultados.id_evaluado', 'resultados.id_evaluador', 'id_factor')->orderBy('resultados.id_evaluado')->orderBy('id_factor')->get();*/

          $results = DB::table('resultados')->join('users', 'users.id', '=', 'resultados.id_evaluado')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('puesto_nivel_puesto', 'puesto_nivel_puesto.id_puesto', '=', 'job_positions.id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('directions', 'directions.id', '=', 'departments.direction_id')->join('evaluador_evaluado', function($join){$join->on('evaluador_evaluado.id_evaluado', '=', 'resultados.id_evaluado');$join->on('evaluador_evaluado.id_evaluador', '=', 'resultados.id_evaluador');$join->on('evaluador_evaluado.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('tipos_evaluadores', 'tipos_evaluadores.id', '=', 'evaluador_evaluado.tipo_evaluacion')->where('resultados.id_periodo', $id_periodo)->select(array(DB::raw('AVG(nivel_desempeno) AS nivel_desempeno'), 'id_factor', 'resultados.id_evaluado', 'resultados.id_evaluador', 'tipos_evaluadores.nombre', 'first_name', 'last_name', 'areas.name AS area', 'departments.name AS department', 'directions.name AS direction', 'jefe', 'id_nivel_puesto'))->groupBy('resultados.id_evaluado', 'resultados.id_evaluador', 'id_factor')->orderBy('resultados.id_evaluado')->orderBy('id_factor')->get();
          $promedios_tipo_evaluacion = array();
          $autoevaluacion = 0;
          $jefe = 0;
          $colaborador = 0;
          $par = 0;
          $psicometricos = 0;
          $ids_jefes_evaluadores = array();
          $jefes = DB::table('employees')->select(array(DB::raw('CONCAT(paterno, " ", materno, " ", nombre) AS usuario'), 'idempleado'))->pluck('usuario', 'idempleado')->toArray();
          $nombres_jefes = array();
          $evaluaciones_colaborador = 0;
          $evaluaciones_par = 0;
          $evaluaciones_psicometricos = 0;
          $total_colaborador = 0;
          $total_par = 0;
          $total_psicometricos = 0;
          $user_employees = array();
          $actual_factor = 0;
          $final_autoevaluacion = $final_par = $final_colaborador = $final_cliente = $final_jefe = $final_ponderado = 0;
          $contador_autoevaluacion = $contador_jefe = $contador_colaborador = $contador_par = $contador_clientes = $contador_promedio = 0;
          $totales_factores = array();
          $contador_colaborador_por_factor = $contador_par_por_factor = $contador_cliente_por_factor = 0;
          $autoevaluacion_por_factor = $jefe_por_factor = $colaborador_por_factor = $par_for_factor = $cliente_por_factor = 0;
          $promedios_ponderados = $promedio_ponderado = array();
          $promedio_ponderado['period_id'] = $id_periodo;
          $promedio_ponderado['created_by'] = auth()->user()->id;

          foreach ($results as $key => $value){
            
            $actual_evaluado = $value->id_evaluado;

            if ($actual_factor != $value->id_factor){

              if ($actual_factor != 0){

                //if (isset($pesos_factores[$value->id_nivel_puesto][$actual_factor])){

                  $ponderacion_a_usar = 0;
                  $total_sin_ponderacion = 0;
                  $contador_sin_ponderacion = 0;
                  $promedio = 0;
                  //$promedio_sin_ponderacion = 0;
                  //$contador_para_promedio_sin_ponderacion = 0;

                  if ($autoevaluacion_por_factor != 0){

                    //$promedio_sin_ponderacion += $autoevaluacion;
                    //$contador_para_promedio_sin_ponderacion++;

                    if (!empty($ponderaciones_separadas[$value->id_nivel_puesto]) && $ponderaciones_separadas[$value->id_nivel_puesto]->autoevaluacion != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->autoevaluacion;
                      $promedio += $autoevaluacion_por_factor * $ponderaciones_separadas[$value->id_nivel_puesto]->autoevaluacion / 100;
                    }

                    else{

                      $total_sin_ponderacion += $autoevaluacion_por_factor;
                      $contador_sin_ponderacion++;
                    }
                  }

                  if ($jefe_por_factor != 0){

                    //$promedio_sin_ponderacion += $jefe;
                    //$contador_para_promedio_sin_ponderacion++;

                    if (!empty($ponderaciones_separadas[$value->id_nivel_puesto]) && $ponderaciones_separadas[$value->id_nivel_puesto]->jefe != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->jefe;
                      $promedio += $jefe_por_factor * $ponderaciones_separadas[$value->id_nivel_puesto]->jefe / 100;
                    }

                    else{

                      $total_sin_ponderacion += $jefe_por_factor;
                      $contador_sin_ponderacion++;
                    }
                  }

                  if ($contador_par_por_factor != 0){

                    //$promedio_sin_ponderacion += $par / $contador_par;
                    //$contador_para_promedio_sin_ponderacion++;

                    if (!empty($ponderaciones_separadas[$value->id_nivel_puesto]) && $ponderaciones_separadas[$value->id_nivel_puesto]->par != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->par;
                      $promedio += ($par_for_factor / $contador_par_por_factor) * $ponderaciones_separadas[$value->id_nivel_puesto]->par / 100;
                    }

                    else{

                      $total_sin_ponderacion += $par_for_factor / $contador_par_por_factor;
                      $contador_sin_ponderacion++;
                    }
                  }

                  if ($contador_colaborador_por_factor != 0){

                    //$promedio_sin_ponderacion += $colaborador / $contador_colaborador;
                    //$contador_para_promedio_sin_ponderacion++;

                    if (!empty($ponderaciones_separadas[$value->id_nivel_puesto]) && $ponderaciones_separadas[$value->id_nivel_puesto]->colaborador != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->colaborador;
                      $promedio += ($colaborador_por_factor / $contador_colaborador_por_factor) * $ponderaciones_separadas[$value->id_nivel_puesto]->colaborador / 100;
                    }

                    else{

                      $total_sin_ponderacion += $colaborador_por_factor / $contador_colaborador_por_factor;
                      $contador_sin_ponderacion++;
                    }
                  }

                  if ($contador_cliente_por_factor != 0){

                    //$promedio_sin_ponderacion += $psicometricos / $contador_clientes;
                    //$contador_para_promedio_sin_ponderacion++;

                    if (!empty($ponderaciones_separadas[$value->id_nivel_puesto]) && $ponderaciones_separadas[$value->id_nivel_puesto]->psicometricos != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->psicometricos;
                      $promedio += ($cliente_por_factor / $contador_cliente_por_factor) * $ponderaciones_separadas[$value->id_nivel_puesto]->psicometricos / 100;
                    }

                    else{

                      $total_sin_ponderacion += $cliente_por_factor / $contador_cliente_por_factor;
                      $contador_sin_ponderacion++;
                    }
                  }

                  if ($total_sin_ponderacion != 0){

                    $total_sin_ponderacion = $total_sin_ponderacion / $contador_sin_ponderacion;
                    $ponderacion_restante = 100 - $ponderacion_a_usar;
                    $promedio += $total_sin_ponderacion * $ponderacion_restante / 100;
                  }

                  else{

                    if ($ponderacion_a_usar < 100){

                      $ponderacion_restante = 100 - $ponderacion_a_usar;
                      $promedio += $ponderacion_restante;
                    }
                  }

                  /*if ($colaborador != 0){
                    $contador_colaborador++;
                  }
                  if ($psicometricos != 0){
                    $contador_clientes++;
                  }
                  if ($par != 0){
                    $contador_par++;
                  }*/
                  $contador_promedio++;
                  $totales_factores[$actual_factor] = $promedio;
                  //$totales_factores[$actual_factor] = $promedio_sin_ponderacion / $contador_para_promedio_sin_ponderacion;
                  //$totales_factores[$actual_factor] = $total_por_factor / $contador_por_factor;

                  /*$promedio = $total_colaborador + $total_par + $total_psicometricos;
                  $promedio = $promedio / ($evaluaciones_colaborador + $evaluaciones_psicometricos + $evaluaciones_par);
                  $promedio = $promedio * 71 / 100;
                  $promedio = $promedio + ($autoevaluacion * 4 / 100) + ($jefe * 25 / 100);*/
                  $final_ponderado += $promedio/* * $pesos_factores[$value->id_nivel_puesto][$actual_factor] / 105*/;
                  //$final_autoevaluacion += $autoevaluacion/* * $pesos_factores[$value->id_nivel_puesto][$actual_factor] / 105*/;
                  //$final_jefe += $jefe/* * $pesos_factores[$value->id_nivel_puesto][$actual_factor] / 105*/;
                  //$final_colaborador += $colaborador/* * $pesos_factores[$value->id_nivel_puesto][$actual_factor] / 105*/;
                  //$final_cliente += $psicometricos/* * $pesos_factores[$value->id_nivel_puesto][$actual_factor] / 105*/;
                  //$final_par += $par/* * $pesos_factores[$value->id_nivel_puesto][$actual_factor] / 105*/;
                //}
              }

              $actual_factor = $value->id_factor;
              $total_colaborador = $total_psicometricos = $total_par = 0;
              $contador_por_factor = $total_por_factor = 0;
              $autoevaluacion_por_factor = $jefe_por_factor = $colaborador_por_factor = $par_for_factor = $cliente_por_factor = 0;
              $contador_colaborador_por_factor = $contador_par_por_factor = $contador_cliente_por_factor = 0;
            }

            if (!empty($value->nombre)){

              //$total_por_factor += $value->nivel_desempeno;
              //$contador_por_factor++;

              if ($value->nombre == 'AUTOEVALUACION'){

                $autoevaluacion += $value->nivel_desempeno;
                $contador_autoevaluacion++;
              }

              else{

                if ($value->nombre == 'JEFE'){

                  //$jefe = $value->nivel_desempeno;
                  //$contador_jefe++;
                  $jefe += $value->nivel_desempeno;
                  $contador_jefe++;
                  $jefe_por_factor = $value->nivel_desempeno;
                }

                else{

                  if ($value->nombre == 'SUBORDINADO'){

                    /*$evaluaciones_colaborador += $value->numero_evaluaciones;
                    $total_colaborador += $value->total_evaluacion;
                    $colaborador = $total_colaborador / $evaluaciones_colaborador;*/
                    $colaborador += $value->nivel_desempeno;
                    $contador_colaborador++;
                  }

                  else{

                    if ($value->nombre == 'CLIENTES'){

                      /*$evaluaciones_psicometricos += $value->numero_evaluaciones;
                      $total_psicometricos += $value->total_evaluacion;
                      $psicometricos = $total_psicometricos / $evaluaciones_psicometricos;*/
                      $psicometricos += $value->nivel_desempeno;
                      $contador_clientes++;
                    } 

                    else{

                      /*$evaluaciones_par += $value->numero_evaluaciones;
                      $total_par += $value->total_evaluacion;
                      $par = $total_par / $evaluaciones_par;*/
                      $par += $value->nivel_desempeno;
                      $contador_par++;
                    }
                  }
                }
              }
            }

            else{

              //$total_por_factor += $value->nivel_desempeno;
              //$contador_por_factor++;

              if ($value->id_evaluador == $value->id_evaluado){

                //$autoevaluacion = $value->total_evaluacion / $value->numero_evaluaciones;
                $autoevaluacion += $value->nivel_desempeno;
                $contador_autoevaluacion++;
                $autoevaluacion_por_factor = $value->nivel_desempeno;
              }

              else{

                if (empty($user_employees[$value->jefe])){

                  $employee = Employee::where('idempleado', $value->jefe)->first();

                  if (!empty($employee)){

                    $user_employees[$value->jefe] = $employee->user->id;
                  }  
                }

                if (!empty($user_employees[$value->jefe]) && $value->id_evaluador == $user_employees[$value->jefe]){

                  //$jefe = $value->total_evaluacion / $value->numero_evaluaciones;
                  $jefe += $value->nivel_desempeno;
                  $contador_jefe++;
                  $jefe_por_factor = $value->nivel_desempeno;
                }

                else{

                  $jefe_evaluador = 0;

                  if (!empty($ids_jefes_evaluadores[$value->id_evaluador])){

                    $jefe_evaluador = $ids_jefes_evaluadores[$value->id_evaluador];
                  }

                  else{

                    //$result = DB::select("SELECT boss_id, first_name, last_name FROM users WHERE id = ?", [$value->id_evaluador]);
                    //$result = DB::table('users')->where('id', $value->id_evaluador)->select('boss_id', 'first_name', 'last_name')->first();
                    $result = User::find($value->id_evaluador);

                    if (!empty($result)){

                      if (!empty($result->employee->boss)){

                        $jefe_evaluador = $result->employee->boss->user->id;
                        $ids_jefes_evaluadores[$value->id_evaluador] = $jefe_evaluador;

                        if (empty($nombres_jefes[$value->id_evaluador])){

                          $nombres_jefes[$value->id_evaluador] = $result->employee->boss->user->last_name . ' ' . $result->employee->boss->user->first_name;
                        }
                      }
                    }

                    else{

                      $jefe_evaluador = 0;
                    }
                  }

                  if ($jefe_evaluador == $value->id_evaluado){

                    /*$evaluaciones_colaborador += $value->numero_evaluaciones;
                    $total_colaborador += $value->total_evaluacion;
                    $colaborador = $total_colaborador / $evaluaciones_colaborador;*/
                    $colaborador += $value->nivel_desempeno;
                    $contador_colaborador++;
                    $colaborador_por_factor += $value->nivel_desempeno;
                    $contador_colaborador_por_factor++;
                  }

                  else{

                    if (!empty($user_employees[$value->jefe]) && $jefe_evaluador == $user_employees[$value->jefe]){

                      /*$evaluaciones_par += $value->numero_evaluaciones;
                      $total_par += $value->total_evaluacion;
                      $par = $total_par / $evaluaciones_par;*/
                      $par += $value->nivel_desempeno;
                      $contador_par++;
                      $par_for_factor += $value->nivel_desempeno;
                      $contador_par_por_factor++;
                    }

                    else{

                      /*$evaluaciones_psicometricos += $value->numero_evaluaciones;
                      $total_psicometricos += $value->total_evaluacion;
                      $psicometricos = $total_psicometricos / $evaluaciones_psicometricos;*/
                      $psicometricos += $value->nivel_desempeno;
                      $contador_clientes++;
                      $cliente_por_factor += $value->nivel_desempeno;
                      $contador_cliente_por_factor++;
                    }
                  }
                }
              }
            }

            if (empty($results[$key + 1]) || $results[$key + 1]->id_evaluado != $actual_evaluado){

              //if (isset($pesos_factores[$value->id_nivel_puesto][$actual_factor])){

                $ponderacion_a_usar = 0;
                  $total_sin_ponderacion = 0;
                $contador_sin_ponderacion = 0;
                  $promedio = 0;
                //$promedio_sin_ponderacion = 0;
                //$contador_para_promedio_sin_ponderacion = 0;

                  if ($autoevaluacion_por_factor != 0){

                    //$promedio_sin_ponderacion += $autoevaluacion;
                    //$contador_para_promedio_sin_ponderacion++;

                    if (!empty($ponderaciones_separadas[$value->id_nivel_puesto]) && $ponderaciones_separadas[$value->id_nivel_puesto]->autoevaluacion != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->autoevaluacion;
                      $promedio += $autoevaluacion_por_factor * $ponderaciones_separadas[$value->id_nivel_puesto]->autoevaluacion / 100;
                    }

                    else{

                      $total_sin_ponderacion += $autoevaluacion_por_factor;
                      $contador_sin_ponderacion++;
                    }
                  }

                  if ($jefe_por_factor != 0){

                    //$promedio_sin_ponderacion += $jefe;
                    //$contador_para_promedio_sin_ponderacion++;

                    if (!empty($ponderaciones_separadas[$value->id_nivel_puesto]) && $ponderaciones_separadas[$value->id_nivel_puesto]->jefe != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->jefe;
                      $promedio += $jefe_por_factor * $ponderaciones_separadas[$value->id_nivel_puesto]->jefe / 100;
                    }

                    else{

                      $total_sin_ponderacion += $jefe_por_factor;
                      $contador_sin_ponderacion++;
                    }
                  }

                  if ($contador_par_por_factor != 0){

                    //$promedio_sin_ponderacion += $par / $contador_par;
                    //$contador_para_promedio_sin_ponderacion++;

                    if (!empty($ponderaciones_separadas[$value->id_nivel_puesto]) && $ponderaciones_separadas[$value->id_nivel_puesto]->par != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->par;
                      $promedio += ($par_for_factor / $contador_par_por_factor) * $ponderaciones_separadas[$value->id_nivel_puesto]->par / 100;
                    }

                    else{

                      $total_sin_ponderacion += $par_for_factor / $contador_par_por_factor;
                      $contador_sin_ponderacion++;
                    }
                  }

                  if ($contador_colaborador_por_factor != 0){

                    //$promedio_sin_ponderacion += $colaborador / $contador_colaborador;
                    //$contador_para_promedio_sin_ponderacion++;

                    if (!empty($ponderaciones_separadas[$value->id_nivel_puesto]) && $ponderaciones_separadas[$value->id_nivel_puesto]->colaborador != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->colaborador;
                      $promedio += ($colaborador_por_factor / $contador_colaborador_por_factor) * $ponderaciones_separadas[$value->id_nivel_puesto]->colaborador / 100;
                    }

                    else{

                      $total_sin_ponderacion += $colaborador_por_factor / $contador_colaborador_por_factor;
                      $contador_sin_ponderacion++;
                    }
                  }

                  if ($contador_cliente_por_factor != 0){

                    //$promedio_sin_ponderacion += $psicometricos / $contador_clientes;
                    //$contador_para_promedio_sin_ponderacion++;

                    if (!empty($ponderaciones_separadas[$value->id_nivel_puesto]) && $ponderaciones_separadas[$value->id_nivel_puesto]->psicometricos != 0){

                      $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->psicometricos;
                      //$promedio += ($total_psicometricos / $evaluaciones_psicometricos) * $ponderaciones_separadas[$value->id_nivel_puesto]->psicometricos / 100;
                      $promedio += ($cliente_por_factor / $contador_cliente_por_factor) * $ponderaciones_separadas[$value->id_nivel_puesto]->psicometricos / 100;
                    }

                    else{

                      $total_sin_ponderacion += $cliente_por_factor / $contador_cliente_por_factor;
                      $contador_sin_ponderacion++;
                    }
                  }

                  if ($total_sin_ponderacion != 0){

                    $total_sin_ponderacion = $total_sin_ponderacion / $contador_sin_ponderacion;
                    $ponderacion_restante = 100 - $ponderacion_a_usar;
                    $promedio += $total_sin_ponderacion * $ponderacion_restante / 100;
                  }

                  else{

                    if ($ponderacion_a_usar < 100){

                      $ponderacion_restante = 100 - $ponderacion_a_usar;
                      $promedio += $ponderacion_restante;
                    }
                  }

                  $contador_promedio++;
                  //$totales_factores[$actual_factor] = $total_por_factor / $contador_por_factor;
                  $totales_factores[$actual_factor] = $promedio;
                  $calificaciones_factores[$actual_evaluado] = $totales_factores;

                  if ($contador_autoevaluacion == 0){
                  $contador_autoevaluacion = 1;
                }
                if ($contador_jefe == 0){
                  $contador_jefe = 1;
                }
                if ($contador_colaborador == 0){
                  $contador_colaborador = 1;
                }
                if ($contador_par == 0){
                  $contador_par = 1;
                }
                if ($contador_clientes == 0){
                  $contador_clientes = 1;
                }
                if ($contador_promedio == 0){
                  $contador_promedio = 1;
                }

                $final_ponderado += $promedio;
                $final_autoevaluacion = $autoevaluacion / $contador_autoevaluacion;
                $final_jefe = $jefe / $contador_jefe;
                $final_colaborador = $colaborador / $contador_colaborador;
                $final_cliente = $psicometricos / $contador_clientes;
                $final_par = $par / $contador_par;

              $value->promedio_autoevaluacion = $final_autoevaluacion;
              $value->promedio_jefe = $final_jefe;
              $value->promedio_colaborador = $final_colaborador;
              $value->promedio_par = $final_par;
              $value->promedio_psicometricos = $final_cliente;

              $ponderacion_a_usar = 0;
              $total_sin_ponderacion = 0;
              $contador_sin_ponderacion = 0;
              $promedio = 0;

              if ($value->promedio_autoevaluacion != 0){

                if (!empty($ponderaciones_separadas[$value->id_nivel_puesto]) && $ponderaciones_separadas[$value->id_nivel_puesto]->autoevaluacion != 0){

                  $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->autoevaluacion;
                  $promedio += $value->promedio_autoevaluacion * $ponderaciones_separadas[$value->id_nivel_puesto]->autoevaluacion / 100;
                }

                else{

                  $total_sin_ponderacion += $value->promedio_autoevaluacion;
                  $contador_sin_ponderacion++;
                }
              }

              if ($value->promedio_jefe != 0){

                if (!empty($ponderaciones_separadas[$value->id_nivel_puesto]) && $ponderaciones_separadas[$value->id_nivel_puesto]->jefe != 0){

                  $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->jefe;
                  $promedio += $value->promedio_jefe * $ponderaciones_separadas[$value->id_nivel_puesto]->jefe / 100;
                }

                else{

                  $total_sin_ponderacion += $value->promedio_jefe;
                  $contador_sin_ponderacion++;
                }
              }

              if ($value->promedio_par != 0){

                if (!empty($ponderaciones_separadas[$value->id_nivel_puesto]) && $ponderaciones_separadas[$value->id_nivel_puesto]->par != 0){

                  $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->par;
                  $promedio += $value->promedio_par * $ponderaciones_separadas[$value->id_nivel_puesto]->par / 100;
                }

                else{

                  $total_sin_ponderacion += $value->promedio_par;
                  $contador_sin_ponderacion++;
                }
              }

              if ($value->promedio_colaborador != 0){

                if (!empty($ponderaciones_separadas[$value->id_nivel_puesto]) && $ponderaciones_separadas[$value->id_nivel_puesto]->colaborador != 0){

                  $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->colaborador;
                  $promedio += $value->promedio_colaborador * $ponderaciones_separadas[$value->id_nivel_puesto]->colaborador / 100;
                }

                else{

                  $total_sin_ponderacion += $value->promedio_colaborador;
                  $contador_sin_ponderacion++;
                }
              }

              if ($value->promedio_psicometricos != 0){

                if (!empty($ponderaciones_separadas[$value->id_nivel_puesto]) && $ponderaciones_separadas[$value->id_nivel_puesto]->psicometricos != 0){

                  $ponderacion_a_usar += $ponderaciones_separadas[$value->id_nivel_puesto]->psicometricos;
                  $promedio += $value->promedio_psicometricos * $ponderaciones_separadas[$value->id_nivel_puesto]->psicometricos / 100;
                }

                else{

                  $total_sin_ponderacion += $value->promedio_psicometricos;
                  $contador_sin_ponderacion++;
                }
              }

              if ($total_sin_ponderacion != 0){

                $total_sin_ponderacion = $total_sin_ponderacion / $contador_sin_ponderacion;
                $ponderacion_restante = 100 - $ponderacion_a_usar;
                $promedio += $total_sin_ponderacion * $ponderacion_restante / 100;
              }

              else{

                if ($ponderacion_a_usar < 100){

                  $ponderacion_restante = 100 - $ponderacion_a_usar;
                  $promedio += $ponderacion_restante;
                }
              }

              $value->promedio_ponderado = $promedio;
              $promedio_ponderado['user_id'] = $actual_evaluado;
              $promedio_ponderado['total'] = number_format($promedio, 2);
              $promedios_ponderados[] = $promedio_ponderado;

              if (!empty($jefes[$value->jefe])){

                $value->jefe = $jefes[$value->jefe];
              }

              else{

                $value->jefe = '';
              }

              $value->totales_factores = $totales_factores;
              $promedios_tipo_evaluacion[] = $value;
              $autoevaluacion = $jefe = $colaborador = $par = $psicometricos = $evaluaciones_colaborador = $evaluaciones_par = $evaluaciones_psicometricos = $total_colaborador = $total_par = $total_psicometricos = 0;
              $final_cliente = $final_par = $final_colaborador = $final_jefe = $final_autoevaluacion = $final_ponderado = 0;
              $contador_autoevaluacion = $contador_jefe = $contador_colaborador = $contador_par = $contador_clientes = $contador_promedio = 0;
              $actual_factor = 0;
              $totales_factores = array();
            }
          }

          if (!empty($promedios_ponderados)){

            DB::table('resultados_desempeno')->where('period_id', $id_periodo)->delete();
            DB::table('resultados_desempeno')->insert($promedios_ponderados);
          }
        }

        //$factores = DB::select("SELECT factores.id, factores.nombre, factores.orden FROM factores INNER JOIN periodo_factor ON (periodo_factor.id_factor = factores.id) WHERE periodo_factor.id_periodo = ? ORDER BY factores.orden", [$id_periodo]);
        $factores = DB::table('factores')->join('periodo_factor', 'periodo_factor.id_factor', '=', 'factores.id')->where('periodo_factor.id_periodo', $id_periodo)->select('factores.id', 'factores.nombre', 'factores.orden')->orderBy('factores.orden')->orderBy('factores.id')->get();
        
        // Se obtienen las ponderaciones para el periodo
        //$ponderaciones = DB::select("SELECT id_nivel_puesto, jefe, autoevaluacion, colaborador, par, nombre FROM ponderaciones INNER JOIN niveles_puestos ON (niveles_puestos.id = ponderaciones.id_nivel_puesto) WHERE id_periodo = ? ORDER BY id_nivel_puesto", [$id_periodo]);

        //$niveles_puestos = DB::table('niveles_puestos')->where('mando', 1)->orderBy('id')->pluck('id');
        $niveles_puestos = DB::table('niveles_puestos')->orderBy('id')->pluck('id');
        $temp = array();

        foreach ($niveles_puestos as $key => $value){
              
          $posiciones[$value] = $key;
          $temp[] = $value;
        }

        $niveles_puestos = $temp;
      }

      return view('evaluacion-desempeno/reportes/reporte-desempeno-completo', compact('resultados', 'id_periodo', 'factores', 'periodos', 'promedios_tipo_evaluacion', 'ponderaciones', 'niveles_puestos', 'posiciones', 'tipos_evaluadores', 'pesos_factores', 'calificaciones_factores'));
    }

    /**
     * Muestra el reporte de todas las evaluaciones de la evaluacion de desempeño
     */
    public function reporte_desempeno_sabana($id_periodo){

      if (Session::has('primer_login')){

        flash('Debes cambiar tu contraseña antes de continuar a otra sección');
        return redirect('/');
      }

      if (!auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin') && auth()->user()->role != 'supervisor'){

        flash('No tiene permiso de ver la sección');
        return redirect('/');
      }

      $resultados = array();
      $factores = $fechas_termino = array();
      $periodos = DB::select("SELECT id, descripcion FROM periodos WHERE status != ? AND status != ? ORDER BY id DESC", ['Preparatorio', 'Cancelado']);

      if (count($periodos) > 0){

        if (empty($id_periodo)){

          if (Session::has('id_periodo')){

            $id_periodo = Session::get('id_periodo');
          }

          else{

            $id_periodo = $periodos[0]->id;
          }
        }

        else{

          Session::put('id_periodo', $id_periodo);
        }

        //$resultados = DB::select("SELECT resultados.nivel_desempeno, resultados.id_factor, resultados.id_evaluador, resultados.id_evaluado, users.first_name, users.division, users.subdivision, users.boss_id, factores.orden FROM resultados INNER JOIN users ON (users.id = resultados.id_evaluado) INNER JOIN factores ON (factores.id = resultados.id_factor) WHERE resultados.id_periodo = ? ORDER BY resultados.id_evaluado, resultados.id_evaluador, factores.orden", [$id_periodo]);
        $resultados = DB::table('resultados')->join('users', 'users.id', '=', 'resultados.id_evaluado')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('puesto_nivel_puesto', 'puesto_nivel_puesto.id_puesto', '=', 'job_positions.id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->join('factores', 'factores.id', '=', 'resultados.id_factor')->join('evaluador_evaluado', function($join){$join->on('evaluador_evaluado.id_evaluado', '=', 'resultados.id_evaluado');$join->on('evaluador_evaluado.id_evaluador', '=', 'resultados.id_evaluador');$join->on('evaluador_evaluado.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('tipos_evaluadores', 'tipos_evaluadores.id', '=', 'evaluador_evaluado.tipo_evaluacion')->where('resultados.id_periodo', $id_periodo)->select(array(DB::raw('AVG(nivel_desempeno) AS nivel_desempeno'), 'id_factor', 'resultados.id_evaluador', 'resultados.id_evaluado', 'descripcion_empresa', 'first_name', 'last_name', 'areas.name as area', 'job_positions.name as puesto', 'id_nivel_puesto', 'departments.name AS department', 'jefe', 'idempleado', 'orden'))->groupBy('id_evaluado', 'id_evaluador', 'id_factor')->orderBy('resultados.id_evaluado')->orderBy('resultados.id_evaluador')->orderBy('orden')->orderBy('id_factor')->get();
        $fechas = DB::table('resultados')->where('id_periodo', $id_periodo)->select('id_evaluador', 'id_evaluado', 'updated_at')->groupBy('id_evaluador', 'id_evaluado')->orderBy('updated_at', 'DESC')->get();

        foreach ($fechas as $key => $value){
        
          if (empty($fechas_termino[$value->id_evaluador])){

            $fechas_termino[$value->id_evaluador] = array();
          }

          $fechas_termino[$value->id_evaluador][$value->id_evaluado] = substr($value->updated_at, 0, 10);
        }


        $factores = DB::select("SELECT factores.id, factores.nombre, factores.orden FROM factores INNER JOIN periodo_factor ON (periodo_factor.id_factor = factores.id) WHERE periodo_factor.id_periodo = ? ORDER BY factores.orden, factores.id", [$id_periodo]);
        $users = DB::table('users')->select('first_name', 'last_name', 'id')->get();
        $jefes = array();
        $idsempleados = array();
        $temp = array();

        foreach ($users as $key => $value){
          
          $temp[$value->id] = $value->last_name . ' ' . $value->first_name;
        }

        $users = $temp;
        $temp_results = array();
        $idsempleados_jefes = array();

        foreach ($resultados as $key => $resultado){
          
          if (!empty($users[$resultado->id_evaluador])){

            $resultado->evaluador = $users[$resultado->id_evaluador];

            if (!isset($jefes[$resultado->id_evaluador])){

              $user = User::withTrashed()->find($resultado->id_evaluador);

              if (!empty($user->employee_wt->boss)){

                $jefes[$resultado->id_evaluador] = $user->employee_wt->boss->user->id;
                $idsempleados_jefes[$resultado->id_evaluador] = $user->employee_wt->jefe;
              }

              else{

                $jefes[$resultado->id_evaluador] = 0;
                $idsempleados_jefes[$resultado->id_evaluador] = '';
              }

              $idsempleados[$resultado->id_evaluador] = $user->employee_wt->idempleado;
            }

            $resultado->id_jefe_evaluador = $jefes[$resultado->id_evaluador];
            $resultado->idempleado_evaluador = $idsempleados[$resultado->id_evaluador];
            $resultado->idempleado_jefe_evaluador = $idsempleados_jefes[$resultado->id_evaluador];
          }

          $temp_results[] = $resultado;
        }

        $resultados = $temp_results;
      }

      $pesos_factores = array();
      $niveles_esperados = DB::table('factores_niveles_puestos')->select('id_nivel_puesto', 'id_factor', 'peso')->get();

      foreach ($niveles_esperados as $key => $value){
          
        if (!isset($pesos_factores[$value->id_nivel_puesto])){

          $pesos_factores[$value->id_nivel_puesto] = array();
        }

        $pesos_factores[$value->id_nivel_puesto][$value->id_factor] = $value->peso;
      }

      $tipos_evaluadores = DB::table('tipos_evaluadores')->pluck('descripcion_empresa', 'id')->toArray();
      return view('evaluacion-desempeno/reportes/reporte-desempeno-sabana', compact('resultados', 'id_periodo', 'factores', 'periodos', 'tipos_evaluadores', 'pesos_factores', 'fechas_termino'));
    }

    /**
     * Guarda los comentarios adicionales y objetivos entregables del jefe a su colaborador
     */
    public function guardar_comentarios_jefe(){

      $id_jefe = auth()->user()->id;
      $id_evaluado = $_POST['id_evaluado'];
      $id_periodo = $_POST['id_periodo'];
      DB::delete("DELETE FROM objetivos_entregables_desempeno WHERE id_evaluado = ? AND id_jefe = ? AND id_periodo = ?", [$id_evaluado, $id_jefe, $id_periodo]);

      for ($i = 1;$i <= 5;$i++){

        if (!empty($_POST['objetivo' . $i]) || !empty($_POST['accion' . $i]) || !empty($_POST['entregable' . $i])){

          $objetivo = (!empty($_POST['objetivo' . $i]) ? $_POST['objetivo' . $i] : '');
          $accion = (!empty($_POST['accion' . $i]) ? $_POST['accion' . $i] : '');
          $entregable = (!empty($_POST['entregable' . $i]) ? $_POST['entregable' . $i] : '');
          $fecha = date('Y-m-d H:i:s');
          DB::insert("INSERT INTO objetivos_entregables_desempeno (id_evaluado,id_jefe,id_periodo,objetivo,accion,entregable,created_at) VALUES (?,?,?,?,?,?,?)", [$id_evaluado,$id_jefe,$id_periodo,$objetivo,$accion,$entregable,$fecha]);
        }
      }

      DB::delete("DELETE FROM comentarios_desempeno_jefe WHERE id_evaluado = ? AND id_jefe = ? AND id_periodo = ?", [$id_evaluado, $id_jefe, $id_periodo]);

      if (!empty($_POST['comentarios_adicionales'])){

        $comentario = $_POST['comentarios_adicionales'];
        $fecha = date('Y-m-d H:i:s');
        DB::insert("INSERT INTO comentarios_desempeno_jefe (id_evaluado,id_jefe,id_periodo,comentario,created_at) VALUES (?,?,?,?,?)", [$id_evaluado, $id_jefe, $id_periodo, $comentario, $fecha]);
      }

      return redirect()->to('avance-colaboradores');
    }

    /**
     * Descarga en excel todos los objetivos acordados de los subordinados
     */
    public function reporte_objetivos($id_periodo){

      $id_jefe = auth()->user()->id;
      $nombre_jefe = auth()->user()->first_name . ' ' . auth()->user()->last_name;
      $periodo = DB::table('periodos')->where('id', $id_periodo)->select('descripcion')->first();
      
      // Obtienen los objetivos acordados
      $objetivos = DB::select("SELECT objetivo, accion, entregable, id_evaluado, first_name, last_name, areas.name AS area FROM objetivos_entregables_desempeno INNER JOIN users ON (users.id = objetivos_entregables_desempeno.id_evaluado) INNER JOIN employees ON (employees.id = users.employee_id) LEFT JOIN job_positions ON (job_positions.id = employees.job_position_id) LEFT JOIN areas ON (areas.id = job_positions.area_id) WHERE id_periodo = ? AND jefe = ?", [$id_periodo, auth()->user()->employee->idempleado]);

      require_once 'PHPExcel.php';
      require_once 'PHPExcel/IOFactory.php';

      // Create new PHPExcel object
      $objPHPExcel = new \PHPExcel();

      $current_user = 0;
      $counter = 0;
      $nombre = '';
      $row = 0;

      foreach ($objetivos as $key => $objetivo){

        if ($current_user != $objetivo->id_evaluado){

          if ($current_user != 0){

            // Rename sheet
            $objPHPExcel->getActiveSheet()->setTitle($nombre);

            $counter++;
          }

          $current_user = $objetivo->id_evaluado;
          $row = 4;

          if ($counter > 0){

            // Create a new worksheet, after the default sheet
            $objPHPExcel->createSheet();
          }

          // Create a new sheet
          $objPHPExcel->setActiveSheetIndex($counter);

          $objDrawing = new \PHPExcel_Worksheet_Drawing();
          $objDrawing->setPath(getcwd() . '/img/logo.png');
          $objDrawing->setCoordinates('A1');
          $objDrawing->setOffsetY(22);
          $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
          $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(34);
          $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(23);
          $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
          $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(63);
          $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(25);
          $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(25);
          $objPHPExcel->getActiveSheet()->mergeCells('B1:D1');
          $objPHPExcel->getActiveSheet()->getStyle("B1:D1")->getFont()->setSize(18);
          $objPHPExcel->getActiveSheet()->getStyle('B1:D1')->getFont()->setBold(true);
          $objPHPExcel->getActiveSheet()->getStyle('B1:D1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('B1:D1')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('203864');
          $objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(true)->getColor()->setRGB('FFFFFF');
          $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('203864');
          $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->getColor()->setRGB('FFFFFF');
          $objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('B3')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('203864');
          $objPHPExcel->getActiveSheet()->getStyle("C2")->getFont()->setBold(true)->getColor()->setRGB('FFFFFF');
          $objPHPExcel->getActiveSheet()->getStyle('C3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C3')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('203864');
          $objPHPExcel->getActiveSheet()->getStyle("C3")->getFont()->setBold(true)->getColor()->setRGB('FFFFFF');
          $objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('D3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('D3')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8EAADC');
          $objPHPExcel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('B4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8EAADC');
          $objPHPExcel->getActiveSheet()->getStyle('C4')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8EAADC');
          $objPHPExcel->getActiveSheet()->getStyle('D4')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('D4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8EAADC');
          $objPHPExcel->getActiveSheet()->setCellValue('B1', $periodo->descripcion);
          $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Número de empleado:');
          $objPHPExcel->getActiveSheet()->setCellValue('C2', 'Área:');
          $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Nombre:');
          $objPHPExcel->getActiveSheet()->setCellValue('C3', 'Jefe Directo:');
          $objPHPExcel->getActiveSheet()->setCellValue('A4', 'Objetivo');
          $objPHPExcel->getActiveSheet()->setCellValue('B4', 'Acciones');
          $objPHPExcel->getActiveSheet()->setCellValue('C4', 'Entregables');
          $objPHPExcel->getActiveSheet()->setCellValue('D4', 'Fecha compromiso');
          $objPHPExcel->getActiveSheet()->setCellValue('B2', $objetivo->id_evaluado);
          $objPHPExcel->getActiveSheet()->setCellValue('D2', $objetivo->area);
          $objPHPExcel->getActiveSheet()->setCellValue('B3', $objetivo->first_name . ' ' . $objetivo->last_name);
          $objPHPExcel->getActiveSheet()->setCellValue('D3', $nombre_jefe);

          //setOffsetX works properly
          $objDrawing->setOffsetX(32); 

          //set width
          $objDrawing->setWidth(110);

          $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
          $nombre = $objetivo->first_name . ' ' . $objetivo->last_name;

          if (strlen($nombre) > 31){

            $nombre = substr($nombre, 0, 31);
          }
        }

        $row++;
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(100);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $objetivo->objetivo);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $objetivo->accion);
        $objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $objetivo->entregable);
        $objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
      }

      if ($current_user != 0){

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle($nombre);
      }

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="Galber - Objetivos Acordados.xls"');
      header('Cache-Control: max-age=0');
      $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      $objWriter->save('php://output');
    }

    /**
     * Guarda las ponderaciones y muestra la pagina para editarlas
     */
    public function ponderaciones(){

      $ponderaciones = array();
      $id_periodo = 0;
      $message_flash = false;

      // Requiere mostrar o guardar las ponderaciones para cierto periodo
      if (!empty($_POST['id_periodo'])){

        $id_periodo = $_POST['id_periodo'];
        Session::put('id_periodo', $id_periodo);

        // Se requiere guardar las ponderaciones
        if (!empty($_POST['ponderacion'])){

          DB::table('ponderaciones')->where('id_periodo', $id_periodo)->delete();
          $ponderacion = array();
          $ponderacion['id_periodo'] = $id_periodo;

          foreach ($_POST['ponderacion']['id_nivel_puesto'] as $key => $value){
            
            $ponderacion['id_nivel_puesto'] = $value;
            $ponderacion['par'] = $_POST['ponderacion']['par'][$key];
            $ponderacion['jefe'] = $_POST['ponderacion']['jefe'][$key];
            $ponderacion['colaborador'] = $_POST['ponderacion']['colaborador'][$key];
            $ponderacion['autoevaluacion'] = $_POST['ponderacion']['autoevaluacion'][$key];
            $ponderacion['psicometricos'] = $_POST['ponderacion']['psicometricos'][$key];
            DB::table('ponderaciones')->insert($ponderacion);
          }

          $message_flash = true;
        }

        // Se obtienen las ponderaciones del periodo
        $ponderaciones = DB::select("SELECT * FROM ponderaciones WHERE id_periodo = ? ORDER BY id_nivel_puesto", [$id_periodo]);
      }

      // Se obtienen todos los periodos
      $periodos = DB::select("SELECT id, descripcion FROM periodos ORDER BY id DESC");

      // Existen periodos y aun no se han obtenido las ponderaciones 
      if (count($periodos) > 0 && count($ponderaciones) == 0){

        if ($id_periodo == 0){

          $id_periodo = $periodos[0]->id;              
        }

        // Se obtienen las ponderaciones del periodo
        $ponderaciones = DB::select("SELECT * FROM ponderaciones WHERE id_periodo = ? ORDER BY id_nivel_puesto", [$id_periodo]);

        if (count($ponderaciones) == 0){

          $last = DB::table('ponderaciones')->select('id_periodo')->orderBy('created_at', 'DESC')->first();

          if (!empty($last->id_periodo)){

            // Se obtienen las ultimas ponderaciones registradas
            $ponderaciones = DB::table('ponderaciones')->where('id_periodo', $last->id_periodo)->select('id_nivel_puesto', 'jefe', 'autoevaluacion', 'colaborador', 'par', 'psicometricos')->orderBy('id_nivel_puesto')->get();

            $ponderacion = array();
            $ponderacion['id_periodo'] = $id_periodo;

            foreach ($ponderaciones as $key => $value){
            
              $ponderacion['id_nivel_puesto'] = $value->id_nivel_puesto;
              $ponderacion['par'] = $value->par;
              $ponderacion['jefe'] = $value->jefe;
              $ponderacion['colaborador'] = $value->colaborador;
              $ponderacion['autoevaluacion'] = $value->autoevaluacion;
              $ponderacion['psicometricos'] = $value->psicometricos;
              DB::table('ponderaciones')->insert($ponderacion);
            }
          }
        }
      }

      $niveles_puestos = DB::table('niveles_puestos')->select('id', 'nombre')->orderBy('id')->get();
      $tipos_evaluadores = DB::table('tipos_evaluadores')->pluck('descripcion_empresa', 'id')->toArray();
      
      // Se muestra la vista para las ponderaciones
      return view('evaluacion-desempeno/reportes/ponderaciones', compact('ponderaciones', 'periodos', 'message_flash', 'niveles_puestos', 'tipos_evaluadores'));
    }

  /**
  * Descarga en excel todos los usuarios con competencias y acciones acordadas
  */
  public function reporte_usuarios_competencias_acciones($id_periodo){
      
    // Usuarios con competencias y acciones
    $users = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('directions', 'directions.id', '=', 'departments.direction_id')->join('objetivos_entregables_desempeno', 'objetivos_entregables_desempeno.id_evaluado', '=', 'users.id')->where('id_periodo', $id_periodo)->select('first_name', 'last_name', 'directions.name AS direction', 'areas.name AS area', 'objetivo', 'accion')->orderBy('id_evaluado')->get();

    if (!$users->isEmpty()){

      require_once 'PHPExcel.php';
      require_once 'PHPExcel/IOFactory.php';

      // Create new PHPExcel object
      $objPHPExcel = new \PHPExcel();

      // Create a new sheet
      $objPHPExcel->setActiveSheetIndex(0);

      // Rename sheet
      $objPHPExcel->getActiveSheet()->setTitle('Competencias y Acciones');

      $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Nombre');
      $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Departamento');
      $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Dirección / Unidad');
      $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Competencia');
      $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Acciones');
      $counter = 2;

      foreach ($users as $key => $user){

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $counter, $user->first_name . ' ' . $user->last_name);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $counter, $user->area);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $counter, $user->direction);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $counter, $user->objetivo);
        $objPHPExcel->getActiveSheet()->setCellValue('E' . $counter, $user->accion);
        $counter++;  
      }

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="Galber - Usuarios con Competencias y Acciones.xls"');
      header('Cache-Control: max-age=0');
      $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      $objWriter->save('php://output');
    }
  }

  /**
  * Descarga en excel todas competencias y acciones acordadas para cada usuario
  */
  public function reporte_competencias_acciones($id_periodo){
      
    // Competencias y acciones
    $competencias = DB::table('users')->join('objetivos_entregables_desempeno', 'objetivos_entregables_desempeno.id_evaluado', '=', 'users.id')->where('id_periodo', $id_periodo)->select('first_name', 'last_name', 'objetivo', 'accion')->orderBy('objetivo')->get();

    if (!$competencias->isEmpty()){

      require_once 'PHPExcel.php';
      require_once 'PHPExcel/IOFactory.php';

      // Create new PHPExcel object
      $objPHPExcel = new \PHPExcel();

      // Create a new sheet
      $objPHPExcel->setActiveSheetIndex(0);

      // Rename sheet
      $objPHPExcel->getActiveSheet()->setTitle('Competencias y Acciones');

      $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Competencia');
      $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Nombre');
      $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Acciones');
      $counter = 2;

      foreach ($competencias as $key => $competencia){

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $counter, $competencia->objetivo);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $counter, $competencia->first_name . ' ' . $competencia->last_name);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $counter, $competencia->accion);
        $counter++;  
      }

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="Galber - Competencias y Acciones.xls"');
      header('Cache-Control: max-age=0');
      $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      $objWriter->save('php://output');
    }
  }

  /**
  * Descarga en excel todos los usuarios con alguna retroalimentación
  */
  public function reporte_retroalimentacion($id_periodo){
      
    // Competencias y acciones
    $users = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('directions', 'directions.id', '=', 'departments.direction_id')->join('comentarios_desempeno_jefe', 'comentarios_desempeno_jefe.id_evaluado', '=', 'users.id')->where('id_periodo', $id_periodo)->select('first_name', 'last_name', 'directions.name AS direction', 'areas.name AS area', 'comentario')->orderBy('direction')->orderBy('area')->get();

    if (!$users->isEmpty()){

      require_once 'PHPExcel.php';
      require_once 'PHPExcel/IOFactory.php';

      // Create new PHPExcel object
      $objPHPExcel = new \PHPExcel();

      // Create a new sheet
      $objPHPExcel->setActiveSheetIndex(0);

      // Rename sheet
      $objPHPExcel->getActiveSheet()->setTitle('Retroalimentación');

      $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Dirección');
      $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Departamento');
      $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Nombre');
      $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Retroalimentación');
      $counter = 2;

      foreach ($users as $key => $user){

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $counter, $user->direction);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $counter, $user->area);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $counter, $user->first_name . ' ' . $user->last_name);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $counter, $user->comentario);
        $counter++;  
      }

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="Galber - Retroalimentacion.xls"');
      header('Cache-Control: max-age=0');
      $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      $objWriter->save('php://output');
    }
  }
}