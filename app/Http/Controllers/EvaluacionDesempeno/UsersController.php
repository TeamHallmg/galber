<?php

namespace App\Http\Controllers\EvaluacionDesempeno;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\Http\Requests;
/*use Finvivir\User;
use App\Models\Event;
//use App\Models\Region;
use App\Models\Strings;
use App\Models\Benefits;
use App\Models\Balances;
use App\Models\Incident;
use App\Models\Userfields;

use App\Mail\Welcome;*/
use DB;
use Session;
use App\User;

class UsersController extends Controller
{
  
  // Muestra un listado con todos los empleados activos
  public function lista_usuarios(){

    if (!auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin') && auth()->user()->role != 'supervisor'){

      flash('No tiene permiso para ver esta sección');
      return redirect('/');
    }

    $id_user = auth()->user()->id;
    $users = User::where('active', 1)->where('id', '!=', 1)->get();
    return view('evaluacion-desempeno.usuarios.lista-usuarios', compact('users'));
  }

  // Cambia la contraseña de un usuario
  public function cambiar_contrasena($id_user){

    if (!auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin') && $id_user != auth()->user()->id){

      flash('No tiene permiso para ver esta sección');
      return redirect('/');
    }

    if (empty($id_user)){

      $id_user = auth()->user()->id;
    }

    if (!empty($_POST['nueva_contrasena'])){

      $password = $_POST['nueva_contrasena'];
      $encrypted_password = password_hash($password, PASSWORD_DEFAULT);
      DB::update("UPDATE users SET password = '$encrypted_password', external = NULL WHERE id = ?", [$id_user]);
      flash('La contraseña fue guardada correctamente');
      Session::forget('primer_login');
      flash('Contraseña cambiada correctamente');

      if ($id_user == auth()->user()->id){

        return redirect('/');
      }

      else{

        DB::update("UPDATE users SET external = 1 WHERE id = ?", [$id_user]);
        return redirect('/lista-empleados');
      }
    }

    $user = DB::select("SELECT id, first_name, last_name FROM users WHERE active = ? AND id = ?", [1, $id_user]);
    return view('evaluacion-desempeno.usuarios.cambiar-contrasena', compact('user'));
  }

  // Cambio de jefe
  public function cambiar_jefe($id_user){

    if (!auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin')){

      flash('No tiene permiso para ver esta sección');
      return redirect('/');
    }

    if (!empty($_POST['nuevo_jefe'])){

      //$results = DB::select("SELECT boss_id, rfc FROM users WHERE id = ?", [$id_user]);
      $user = User::find($id_user);
      $nuevo_jefe = $_POST['nuevo_jefe'];

      if (!empty($user->employee->boss)){

        $anterior_jefe = $user->employee->boss->user->id;
        $rfc = $user->employee->rfc;
        $cambio_jefe = array();
        $cambio_jefe['id_user'] = $id_user;
        $cambio_jefe['rfc'] = $rfc;
        $cambio_jefe['anterior_jefe'] = $anterior_jefe;
        $cambio_jefe['nuevo_jefe'] = $nuevo_jefe;
        DB::table('cambios_jefe')->insert($cambio_jefe);
      }

      $user2 = User::find($nuevo_jefe);
      $employee = array();
      $employee['jefe'] = $user2->employee->idempleado;
      DB::table('employees')->where('id', $user->employee_id)->update($employee);
      //DB::update("UPDATE users SET boss_id = '$nuevo_jefe' WHERE id = ?", [$id_user]);
      flash('El jefe fue guardado correctamente');
      return redirect('/cambiar-jefe/' . $id_user);
    }

    //$user = DB::select("SELECT id, first_name, last_name, boss_id FROM users WHERE active = ? AND id = ?", [1, $id_user]);
    $user = User::find($id_user);
    //$users = DB::select("SELECT id, first_name, last_name FROM users WHERE active = ? ORDER BY first_name", [1]);
    $users = User::where('active', 1)->orderBy('first_name')->get();
    return view('evaluacion-desempeno.usuarios.cambiar-jefe', compact('user', 'users'));
  }
}
