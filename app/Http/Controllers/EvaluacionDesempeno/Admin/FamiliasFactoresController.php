<?php

namespace App\Http\Controllers\EvaluacionDesempeno\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class FamiliasFactoresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      
      $familias_factores = DB::select('select * from familias_factores');
      return view('evaluacion-desempeno/Admin/familias-factores/index', compact('familias_factores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(){

      if (!empty($_POST['id'])){

        $familia_factores = DB::update('update familias_factores set nombre_para_organizacion = "' . $_POST['nombre_para_organizacion'] . '" where id = ?', [$_POST['id']]);
        
        flash('La familia de factores fue guardada correctamente');
      }

      return redirect('familias-factores');
    }
}
