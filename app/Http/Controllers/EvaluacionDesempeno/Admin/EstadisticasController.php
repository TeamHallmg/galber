<?php

namespace App\Http\Controllers\EvaluacionDesempeno\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\EvaluacionDesempeno\EvaluadorEvaluado;
use App\Models\EvaluacionDesempeno\Admin\DepartmentsGruposAreas;
use App\Models\EvaluacionDesempeno\Admin\PuestosNivelesPuestos;
use App\Models\EvaluacionDesempeno\Admin\Periodos;
use App\User;
use DB;
use Session;

class EstadisticasController extends Controller
{
    /**
     * Create a instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('admin');
    }

    public function estadisticas(){

        $id_periodo = 0;
        $periodos = Periodos::where('status', '!=', 'Cancelado')->orWhere('status', '=', 'Preparatorio')->orderBy('id', 'DESC')->get();
        $tipos_evaluacion = DB::table('tipos_evaluadores')->pluck('descripcion_empresa', 'id')->toArray();

        if ($periodos->isEmpty()){
          
          return view('evaluacion-desempeno/Admin/estadisticas', compact('periodos', 'id_periodo'));          
        }

        if (!empty($_POST['id_periodo'])){

          $id_periodo = $_POST['id_periodo'];
          Session::put('id_periodo', $id_periodo);
        }

        else{

          if (Session::has('id_periodo')){

            $id_periodo = Session::get('id_periodo');
          }

          else{

            $id_periodo = $periodos[0]->id;
          }
        }

        $terminado = EvaluadorEvaluado::where('status', '=', 'Terminada')->where('id_periodo', '=', $id_periodo)->count();
        $iniciado = EvaluadorEvaluado::where('status', '=', 'Iniciada')->where('id_periodo', '=', $id_periodo)->count();
        $noIniciado = EvaluadorEvaluado::where('status', '=', 'No Iniciada')->where('id_periodo', '=', $id_periodo)->count();
        $totalEvaluaciones = $terminado + $iniciado + $noIniciado;

        $porcentajeTerminado = ($terminado * 100) / ($totalEvaluaciones == 0 ? 1 : $totalEvaluaciones);
        $porcentajeIniciado = ($iniciado * 100) / ($totalEvaluaciones == 0 ? 1 : $totalEvaluaciones);
        $porcentajeNoIniciado = ($noIniciado * 100) / ($totalEvaluaciones == 0 ? 1 : $totalEvaluaciones);
        $estadisticas = EvaluadorEvaluado::where('id_periodo', '=', $id_periodo)->orderBy('id', 'ASC')->get();

        $evaluados = array();
        $temporal = array();
        $retros = array();
        $total_retros = 0;

        foreach ($estadisticas as $key => $value){

          if (in_array($value->id_evaluado, $evaluados)){

            $value->retro = $retros[$value->id_evaluado];
          }

          else{

            $retro = 0;
            $evaluados[] = $value->id_evaluado;
            $retros[$value->id_evaluado] = $retro;

            if ($value->status == 'Terminada'){

              $evaluaciones = DB::select("SELECT id_evaluado FROM evaluador_evaluado WHERE id_periodo = ? AND id_evaluado = ? AND status != ?", [$id_periodo, $value->id_evaluado, 'Terminada']);

              if (count($evaluaciones) == 0){

                $comentario_jefe = DB::select("SELECT id_jefe FROM comentarios_desempeno_jefe WHERE id_evaluado = ? AND id_periodo = ?", [$value->id_evaluado, $id_periodo]);

                if (count($comentario_jefe) > 0){

                  $objetivos_entregables = DB::select("SELECT id_jefe FROM objetivos_entregables_desempeno WHERE id_evaluado = ? AND id_periodo = ?", [$value->id_evaluado, $id_periodo]);

                  if (count($objetivos_entregables) > 0){

                    $retro = 1;
                    $total_retros++;
                  }
                }
              }

              $retros[$value->id_evaluado] = $retro;
              $value->retro = $retro;
            }
          }

          $temporal[] = $value;
        }

        $totalEvaluados = count($evaluados);
        $porcentajeRetro = ($total_retros * 100) / ($totalEvaluados == 0 ? 1 : $totalEvaluados);

        return view('evaluacion-desempeno/Admin/estadisticas', compact('estadisticas', 'periodos', 'id_periodo', 'total_retros', 'terminado', 'iniciado', 'noIniciado', 'totalEvaluaciones', 'totalEvaluados', 'porcentajeTerminado', 'porcentajeIniciado', 'porcentajeNoIniciado', 'porcentajeRetro', 'tipos_evaluacion'));          
    }

    public function estadisticasConAreasYPuestos(){
      $periodoStatus = Periodos::where('status', '=', 'Abierto')->get();
      
      if($periodoStatus->isEmpty()){
          return view('evaluacion-desempeno/Admin/estadisticasAP', compact('periodoStatus'));          
        }

        if($periodoStatus->count() == 1){
          $periodoAbierto = Periodos::where('status', '=', 'Abierto')->first();

          $estadisticasAP = EvaluadorEvaluado::where('id_periodo', '=', $periodoAbierto->id)->orderBy('id', 'ASC')->get();

          return view('evaluacion-desempeno/Admin/estadisticasAP', compact('estadisticasAP', 'periodoStatus', 'periodoAbierto'));          
        }

        if($periodoStatus->count() > 1){
          $estadisticasAP = EvaluadorEvaluado::all();

          return view('evaluacion-desempeno/Admin/estadisticasAP', compact('estadisticasAP', 'periodoStatus'));
        }
        /*$periodoStatus = Periodos::where('status', '=', 'Abierto')->orderBy('id', 'DESC')->first();
        $estadisticasAP = EvaluadorEvaluado::where('id_periodo', '=', $periodoStatus->id)->orderBy('id', 'ASC')->get();
        
        return view('evaluacion-desempeno/Admin/estadisticasAP', compact('estadisticasAP'));*/
    }
}
