<?php

namespace App\Http\Controllers\EvaluacionDesempeno\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\EvaluacionDesempeno\EvaluadorEvaluado;
use App\User;

class CalificacionesController extends Controller
{
    /**
     * Create a new Colaborador Akron controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('admin');
    }

    public function calificaciones(){
        $calificaciones = EvaluadorEvaluado::orderBy('id', 'ASC')->get();  

        return view('evaluacion-desempeno/Admin/calificaciones', compact('calificaciones'));
    }

    public function estadisticasConAreasYPuestos(){
        $estadisticasAP = EvaluadorEvaluado::orderBy('id', 'ASC')->get();
        
        return view('evaluacion-desempeno/Admin/estadisticasAP', compact('estadisticasAP'));
    }
}
