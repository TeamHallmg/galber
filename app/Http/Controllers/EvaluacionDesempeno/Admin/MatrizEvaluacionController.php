<?php

namespace App\Http\Controllers\EvaluacionDesempeno\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

use App\Models\EvaluacionDesempeno\EvaluadorEvaluado;
use App\Models\EvaluacionDesempeno\Admin\Periodos;
use App\User;
use DB;
use Session;

class MatrizEvaluacionController extends Controller
{
    
    /**
     * Create a instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $usuarios = User::where('active', '=', 1)->whereNotNull('employee_id')->select('id AS data', DB::raw("CONCAT(first_name, ' ', last_name) AS value"))->orderBy('first_name', 'ASC')->get();

      $periodos = Periodos::where('status', '=', 'Preparatorio')->orWhere('status', '=', 'Abierto')->get();
      $countPeriodos = $periodos->count();
      $url = $_SERVER['REQUEST_URI'];
      $url_components = parse_url($url);
      parse_str($url_components['query'], $params);
      $_GET['id_periodo'] = $params['id_periodo'];
      $periodo = $_GET['id_periodo'];
      $tipos_evaluacion = DB::table('tipos_evaluadores')->where('id', '!=', 1)->pluck('descripcion_empresa', 'id')->toArray();

      if(empty($periodos) || is_null($periodos) || $countPeriodos == 0){
        
        $matrizEva = "";
        
        return view('evaluacion-desempeno/Admin/matrizEvaluacion/index', compact('matrizEva', 'countPeriodos', '$periodos', 'usuarios', 'tipos_evaluacion'));
      
      /*}else if($countPeriodos > 1){

        $matrizEva = EvaluadorEvaluado::all();

        return view('evaluacion-desempeno/Admin/matrizEvaluacion/index', compact('matrizEva', 'usuarios', 'idPeriodoStatus', 'countPeriodos', 'periodosPreparatorios'));*/
      
      }else{

        Session::put('id_periodo', $_GET['id_periodo']);

        //$periodosPreparatoriosFirst = Periodos::where('status', '=', 'Preparatorio')->orderBy('id', 'DESC')->first();

        //$idPeriodoStatus = $periodosPreparatoriosFirst->id;

        $matrizEva = EvaluadorEvaluado::where('id_periodo', '=', $periodo)->orderBy('id', 'ASC')->get();

        return view('evaluacion-desempeno/Admin/matrizEvaluacion/index', compact('matrizEva', 'usuarios', 'countPeriodos', 'periodos', 'tipos_evaluacion'));
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $evaluador = $request->evaluador;
        $evaluado = $request->evaluado;
        $periodo = $request->periodo;

        //$periodos = Periodos::where('status', '=', 'Preparatorio')->orWhere('status', '=', 'Abierto')->get();

        /*if($periodoStatus->count() > 1){
          $periodo = $request->periodo;
          $matrizEvaluacion = EvaluadorEvaluado::where('id_evaluador', '=', $evaluador)->where('id_evaluado', '=', $evaluado)->where('id_periodo', '=', $periodo)->exists();
          if($matrizEvaluacion){
            return response()->json([
                'success' => false,
                'msn' => 'Esta combinacion de personas ya existe en este periodo, por favor selecciona otra combinación.'
            ]);
          }else{
            $guardarMatriz = new EvaluadorEvaluado;

            $guardarMatriz->id_evaluador = $evaluador;
            $guardarMatriz->id_evaluado = $evaluado;
            $guardarMatriz->status = 'No Iniciada';
            $guardarMatriz->id_periodo = $periodo;

            $guardarMatriz->save();

            return response()->json([
                'success' => true,
                'msn' => 'Se guardo correctamente.'
            ]);
          }
        }else{*/
          //$periodoStatus = Periodos::where('status', '=', 'Preparatorio')->orderBy('id', 'DESC')->first();

          $matrizEvaluacion = EvaluadorEvaluado::where('id_evaluador', '=', $evaluador)->where('id_evaluado', '=', $evaluado)->where('id_periodo', '=', $periodo)->exists();

          if($matrizEvaluacion){
            return response()->json([
                'success' => false,
                'msn' => 'Esta combinacion de personas ya existe en este periodo, por favor selecciona otra combinación.'
            ]);
          }else{
            $guardarMatriz = new EvaluadorEvaluado;

            $guardarMatriz->id_evaluador = $evaluador;
            $guardarMatriz->id_evaluado = $evaluado;

            if (!empty($request->tipo)){

              $guardarMatriz->tipo_evaluacion = $request->tipo;
            }

            $guardarMatriz->status = 'No Iniciada';
            $guardarMatriz->id_periodo = $periodo;

            $guardarMatriz->save();
            
            return response()->json([
              'success' => true,
              'msn' => 'Se guardo correctamente.'
            ]);
          }
        //}  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $editEvaluadorEvaluado = EvaluadorEvaluado::find($id);

      $periodosPreparatorios = Periodos::where('status', '=', 'Preparatorio')->get();
      $countPeriodos = $periodosPreparatorios->count();
      $periodo = $_POST['id_periodo'];

      $usuarios = User::where('active', '=', 1)->orderBy('first_name', 'ASC')->get();

      return view('evaluacion-desempeno/Admin/matrizEvaluacion/edit', compact('editEvaluadorEvaluado', 'usuarios', 'id', 'countPeriodos', 'periodosPreparatorios', 'periodo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $evaluador = $request->evaluador . '';
        $evaluado = $request->evaluado . '';
        $periodo = $request->id_periodo;

        $editEvaluadorEvaluado = EvaluadorEvaluado::find($id);

        $periodosPreparatorios = Periodos::where('status', '=', 'Preparatorio')->get();
        $countPeriodos = $periodosPreparatorios->count();

        $usuarios = User::where('active', '=', 1)->orderBy('first_name', 'ASC')->get();

        if(is_null($periodo) || empty($periodo)){
          $checkPeriodoPreparatorio = Periodos::where('status', '=', 'Preparatorio')->first(); 

          $checkCombinacion = EvaluadorEvaluado::where('id_evaluador', '=', $evaluador)->where('id_evaluado', '=', $evaluado)->where('id_periodo', '=', $checkPeriodoPreparatorio->id)->exists();
          
          if($checkCombinacion){
            Flash::error('Esta combinación de usuarios ya existe en este periodo. Intentalo nuevamente con otros usuarios.');

            return view('evaluacion-desempeno/Admin/matrizEvaluacion/edit', compact('editEvaluadorEvaluado', 'usuarios', 'id', 'countPeriodos', 'periodosPreparatorios'));

          }else{
            $editEvaluadorEvaluado->id_evaluador = $evaluador;
            $editEvaluadorEvaluado->id_evaluado = $evaluado;

            if (!empty($request->tipo)){

              $editEvaluadorEvaluado->tipo_evaluacion = $request->tipo;
            }

            $editEvaluadorEvaluado->save();

            return redirect()->to('Matriz-Evaluacion');
          }
        }else{
          $checkCombinacion = EvaluadorEvaluado::where('id_evaluador', '=', $evaluador)->where('id_evaluado', '=', $evaluado)->where('id', '!=', $id)->where('id_periodo', '=', $periodo)->exists();

          if($checkCombinacion){
            Flash::error('Esta combinación de usuarios ya existe en este periodo. Intentalo nuevamente con otros usuarios.');

            return view('evaluacion-desempeno/Admin/matrizEvaluacion/edit', compact('editEvaluadorEvaluado', 'usuarios', 'id', 'countPeriodos', 'periodosPreparatorios', 'periodo'));
          }else{
            $editEvaluadorEvaluado->id_evaluador = $evaluador;
            $editEvaluadorEvaluado->id_evaluado = $evaluado;
            $editEvaluadorEvaluado->id_periodo = $periodo;

            if (!empty($request->tipo)){

              $editEvaluadorEvaluado->tipo_evaluacion = $request->tipo;
            }

            $editEvaluadorEvaluado->save();

            Flash::success('La evaluación fue guardada correctamente.');

            return redirect('Matriz-Evaluacion?id_periodo=' . $periodo);
          }
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteEvaluadorEvalaudo = EvaluadorEvaluado::find($id);

        if (!empty($deleteEvaluadorEvalaudo)){

          DB::delete("DELETE FROM resultados WHERE id_evaluador = ? AND id_evaluado = ? AND id_periodo = ?", [$deleteEvaluadorEvalaudo->id_evaluador,$deleteEvaluadorEvalaudo->id_evaluado,$deleteEvaluadorEvalaudo->id_periodo]);

        //if ($deleteEvaluadorEvalaudo->status == 'No Iniciada'){

          $deleteEvaluadorEvalaudo->delete();
        }

        flash('La evaluación fue borrada');
        /*}

        else{

          flash('Esta evaluacion ya fue iniciada. No puede borrarse');
        }*/

        return redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Muestra los tipos de evaluación
     */
    public function tipos_evaluacion(){

      $tipos_evaluacion = DB::table('tipos_evaluadores')->select('id', 'nombre', 'descripcion', 'descripcion_empresa')->orderBy('id')->get();
      return view('evaluacion-desempeno/Admin/matrizEvaluacion/tipos-evaluacion', compact('tipos_evaluacion'));
    }

    /**
     * Edita los tipos de evaluación
     */
    public function edit_tipo_evaluacion(){

      if (!empty($_POST['id'])){

        $tipo_evaluacion = array();
        $tipo_evaluacion['descripcion_empresa'] = $_POST['descripcion_empresa'];
        $tipo_evaluacion['updated_at'] = date('Y-m-d H:i:s');
        DB::table('tipos_evaluadores')->where('id', $_POST['id'])->update($tipo_evaluacion);
        flash('El tipo de evaluación fue guardado correctamente');
      }

      return redirect('tipos-evaluacion');
    }
}
