<?php

namespace App\Http\Controllers\EvaluacionDesempeno\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

use App\Models\JobPosition;
use App\Models\EvaluacionDesempeno\Admin\PuestosNivelesPuestos;
use App\Models\EvaluacionDesempeno\Admin\NivelesPuestos;

class NivelesPuestosController extends Controller
{
    /**
     * Create a instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nivelesPuestos = NivelesPuestos::select('id', 'nombre', 'mando')->get();

      //$puestosNivelesPuestos = PuestosNivelesPuestos::PuestoNivelPuesto($gruposNiveles)->get();
      $job_positions = array();

      foreach($nivelesPuestos as $nPuestos){
        $puestos_NivelesPuestos[] = PuestosNivelesPuestos::PuestoNivelPuesto($nPuestos->id)->get();
      }

      $job_positions_in_group = PuestosNivelesPuestos::pluck('id_puesto')->toArray();

      if (!empty($job_positions_in_group)){

        $job_positions = JobPosition::whereNotIn('id', $job_positions_in_group)->orderBy('name')->get();
      }

      else{

        $job_positions = JobPosition::orderBy('name')->get();
      }

      return view('evaluacion-desempeno/Admin/niveles-puestos/index', compact('puestos_NivelesPuestos', 'nivelesPuestos', 'job_positions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      $job_positions_in_use = PuestosNivelesPuestos::pluck('id_puesto')->toArray();
      $puestos = array();

      if (!empty($job_positions_in_use)){

        $puestos = JobPosition::whereNotIn('id', $job_positions_in_use)->get();
      }

      else{

        $puestos = JobPosition::all();
      }
      //$otrosPuestos = PuestosNivelesPuestos::where('id_nivel_puesto', '=', 1)->get();

      return view('evaluacion-desempeno/Admin/niveles-puestos/add', compact('puestos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //dd($request);
      $nPuestos = NivelesPuestos::select('id')->where('nombre', 'LIKE', $request->nombreNivelPuesto)->exists();

      if($nPuestos){
        Flash::error('Este registro ya existe');

        return view('evaluacion-desempeno/Admin/niveles-puestos/index');
      }else{
        NivelesPuestos::create([
          'nombre' => $request->nombreNivelPuesto,
          'mando' => $request->mandoNivelPuesto
          ]);
      }

      $nPuestos = NivelesPuestos::select('id')->where('nombre', 'LIKE', $request->nombreNivelPuesto)->first();

      $checks = $request->id_puestos;
		
	  if (!empty($checks)){

      $checks = explode(',', $checks);

      	foreach($checks as $check){
        	/*PuestosNivelesPuestos::where('id_puesto', '=', $check)->update([
          		'id_nivel_puesto' => $nPuestos->id,
          		'id_puesto' => $check
          	]);*/
          PuestosNivelesPuestos::create([
            'id_nivel_puesto' => $nPuestos->id,
            'id_puesto' => $check
            ]);
      	}
	  }

      return redirect()->to('puestos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
      $idNivelesPuestos = NivelesPuestos::where('id', '=', $id)->get();
	  $selected = array();
	  $selectedOtros = array();

      //dd($idNivelesPuestos);

      foreach($idNivelesPuestos as $idNP){
        $nombreNP = $idNP->nombre;
        $mando = $idNP->mando;
      }      
      
      $puestos = JobPosition::all();

      $nivelesPuestos = PuestosNivelesPuestos::PuestoNivelPuesto($id)->get();

      //$nivelesOtros = PuestosNivelesPuestos::PuestoNivelPuesto(1)->get();
      $nivelesOtros = PuestosNivelesPuestos::where('id_nivel_puesto', '!=', $id)->get();

      foreach($puestos as $puesto){ 
        foreach($nivelesPuestos as $nPuestos){
          if($puesto->id == $nPuestos->id_puesto){
            $selected[] = $nPuestos->id_puesto;
          } 
        }
      }

      foreach($puestos as $puesto){ 
        foreach($nivelesOtros as $no){
          if($puesto->id == $no->id_puesto){
            $selectedOtros[] = $no->id_puesto;
          } 
        }
      }

      //dd($selected);

      return view('evaluacion-desempeno/Admin/niveles-puestos/edit', compact('puestos', 'nombreNP', 'selected', 'id', 'mando', 'nivelesPuestos', 'nivelesOtros', 'selectedOtros'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $checksUpdate = $request->id_puestos;
		$selected = array();
	  	$selectedOtros = array();

        if(is_null($checksUpdate) || empty($checksUpdate)){
          $idNivelesPuestos = NivelesPuestos::where('id', '=', $id)->get();

          foreach($idNivelesPuestos as $idNP){
            $nombreNP = $idNP->nombre;
            $mando = $idNP->mando;
          }      
          
          $puestos = JobPosition::all();

          $puestosNivelesPuestos = PuestosNivelesPuestos::PuestoNivelPuesto($id)->get();

          $nivelesOtros = PuestosNivelesPuestos::PuestoNivelPuesto(1)->get();

          foreach($puestos as $puesto){ 
            foreach($puestosNivelesPuestos as $puestosNP){
              if($puesto->id == $puestosNP->id_puesto){
                $selected[] = $puestosNP->id_puesto;
              } 
            }
          }

          foreach($puestos as $puesto){ 
            foreach($nivelesOtros as $no){
              if($puesto->id == $no->id_puesto){
                $selectedOtros[] = $no->id_puesto;
              } 
            }
          }

          //Flash::error('Debes seleccionar al menos un puesto.');

          //return view('evaluacion-desempeno/Admin/niveles-puestos/edit', compact('puestos', 'nombreNP', 'selected', 'id', 'mando', 'nivelesPuestos', 'nivelesOtros', 'selectedOtros'));
        }

        $nombreNivelPuesto = NivelesPuestos::find($id);

        $nombreNivelPuesto->nombre = $request->nombreNivelPuesto;
        $nombreNivelPuesto->mando = $request->mandoNivelPuesto;

        $nombreNivelPuesto->save();

        //$selDel = PuestosNivelesPuestos::where('id_nivel_puesto', '=', $id)->get();

        /*$selAdd = array();
        $delSel = array();*/

        PuestosNivelesPuestos::where('id_nivel_puesto', '=', $id)->delete();
		
		if (!empty($checksUpdate)){

        $checksUpdate = explode(',', $checksUpdate);

        	foreach($checksUpdate as $c){
          		/*$exist = PuestosNivelesPuestos::where('id_puesto', '=', $c)->where('id_nivel_puesto', '=', $id)->first();
          		if(!$exist){
            		$selAdd[] = $c;
          		}*/

              PuestosNivelesPuestos::create([
            'id_puesto' => $c,
            'id_nivel_puesto' => $id
            ]);
        	}
		}

        /*for($i = 0; $i < count($selDel); $i++){
          if(!in_array($selDel[$i]['id_puesto'], $checksUpdate)){
            $delSel[] = $selDel[$i]['id_puesto'];
          }
        }

        if(!empty($selAdd)){

          foreach($selAdd as $ad){
            PuestosNivelesPuestos::where('id_puesto', '=', $ad)->update(['id_nivel_puesto' => $id]);
          }
        }

        if(!empty($delSel)){

          foreach($delSel as $del){
            PuestosNivelesPuestos::where('id_puesto', '=', $del)->update(['id_nivel_puesto' => 1]);
          }
        }*/

      return redirect()->to('puestos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nivelesPuestos = NivelesPuestos::where('id', '=', $id)->delete();
        $puestosNivelesPuestos = PuestosNivelesPuestos::where('id_nivel_puesto', '=', $id)->delete();
        Flash::success('El Nivel de Puesto ha sido borrado');

      return redirect()->to('puestos');
    }
}
