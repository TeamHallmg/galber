<?php

namespace App\Http\Controllers\EvaluacionDesempeno\Admin;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

use App\Models\EvaluacionDesempeno\EvaluadorEvaluado;
use App\Models\EvaluacionDesempeno\Admin\Periodos;
use App\User;
use App\Employee;
use DB;

class ImportacionEvaluadoresController extends Controller
{
    protected $path;
    protected $folder = 'ImportFileEvaluadorEvaluado/';

    /**
     * Create a instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('admin');

        $this->path = \Storage::disk('local')
                        ->getDriver()
                        ->getAdapter()
                        ->getPathPrefix();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $periodos = Periodos::where('status', '=', 'Preparatorio')->orWhere('status', '=', 'Abierto')->get();
      return view('evaluacion-desempeno/Admin/importacion-evaluados/index', compact('periodos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->mostrar == 'mostrar'){
        $file = $request->file('archivo');

        if($file->getClientOriginalExtension() == 'csv'){
          $file->move($this->path.$this->folder, 'archivo-import.'.$file->getClientOriginalExtension());

          $fileName = 'archivo-import.csv';
        
          $csvFile = fopen($this->path.$this->folder.$fileName, 'r');
          $csvData = array();

          while($data = fgetcsv($csvFile, 0, ',')){
            //if(!empty($data[0]) && !empty($data[2])){
              $csvData[] = $data;
            //}
          }

          fclose($csvFile);

          $usuarios = Employee::all();

          $countCsvData = count($csvData);

          $id_periodo = $request->id_periodo;
          $periodo = Periodos::where('id', '=', $id_periodo)->first();
          $preguntar_borrar = false;

          if ($periodo->status == 'Preparatorio'){

            $preguntar_borrar = true;
          }

          $tipos_evaluacion = DB::table('tipos_evaluadores')->where('id', '!=', 1)->select('id', 'descripcion_empresa')->get();
          return view('evaluacion-desempeno/Admin/importacion-evaluados/edit', compact('csvData', 'usuarios', 'countCsvData', 'preguntar_borrar', 'tipos_evaluacion'));

        }else{
          Flash::error('El tipo de archivo es diferente, debe de ser ".csv". Intentalo nuevamente.');
            //return redirect()->to('importacion-evaluadores-csv'); //view('evaluacion-desempeno/Admin/importacion-evaluados/index');
            return redirect($_SERVER['HTTP_REFERER']); //view('evaluacion-desempeno/Admin/importacion-evaluados/index');
        }
      }

      if($request->reemplazar == 'reemplazar'){

        $id_periodo = $request['id_periodo'];
        $periodo = Periodos::where('id', '=', $id_periodo)->first();

        if ($periodo->status == 'Preparatorio' && !empty($request->borrar_red)){

          $evaluaciones = DB::select("SELECT id FROM evaluador_evaluado WHERE (status = ? OR status = ?) AND id_periodo = ?", ['Iniciada', 'Terminada', $id_periodo]);

          //if (count($evaluaciones) == 0){

            $borrarEvaluadorEvaluado = EvaluadorEvaluado::where('id_periodo', '=', $id_periodo)->delete();
          //}
        }

        $fileName = 'archivo-import.csv';
        
        $csvFile = fopen($this->path.$this->folder.$fileName, 'r');
		$users = DB::select("SELECT id FROM users WHERE active = ?", [1]);
        $activos = array();

      foreach ($users as $key => $user){

          $activos[] = $user->id . '';
      }

        $i = 0;
		$j = 0;
    $k = 0;
    $evaluadores = array();
    $evaluados = array();
    $errores = '';
    $contador_errores = 0;
    $tipos_evaluacion = DB::table('tipos_evaluadores')->where('id', '!=', 1)->select('id', 'descripcion_empresa')->get();
    $user_employees = $employees = array();
    $employee1 = $employee2 = 0;

        while($data = fgetcsv($csvFile, 0, ',')){
          //if(!empty($data[0]) && !empty($data[2])){
          if ($i > 1){
            $id = $jefe = $tipo_evaluacion = '';
            if (isset($data[0]) && isset($data[2])){
              $id = $data[2] . '';
              $jefe = $data[0] . '';
              if (isset($data[4])){
                $tipo_evaluacion = $data[4] . '';
              }

              if (empty($employees[$id])){

                $employee1 = Employee::where('idempleado', $id)->first();

                if (empty($employee1)){

                  $contador_errores++;
                  $errores = $errores .  $contador_errores . '. Fila ' . ($i + 1) . ': Evaluado No Existe<br>';
                  $i++;
                  $j++;
                  continue;
                }

                $employees[$id] = $employee1->id;
                $user_employees[$employee1->id] = $employee1->user->id;
              }

              if (empty($employees[$jefe])){

                $employee2 = Employee::where('idempleado', $jefe)->first();

                if (empty($employee2)){

                  $contador_errores++;
                  $j++;
                  $errores = $errores . $contador_errores . '. Fila ' . ($i + 1) . ': Evaluador No Existe<br>';
                  $i++;
                  continue;
                }

                $employees[$jefe] = $employee2->id;
                $user_employees[$employee2->id] = $employee2->user->id;
              }

              $id = $user_employees[$employees[$id]];
              $jefe = $user_employees[$employees[$jefe]];

              if(in_array($id, $activos) && in_array($jefe, $activos)){
			          $evaluacion = DB::select("SELECT id FROM evaluador_evaluado WHERE id_evaluador = ? AND id_evaluado = ? AND id_periodo = ?", [$jefe, $id, $id_periodo]);

        	      if (count($evaluacion) == 0){
              
				          $insertarCsv = new EvaluadorEvaluado;
              	  $insertarCsv->id_evaluado = $id;
              	  $insertarCsv->id_evaluador = $jefe;

                  if (!empty($tipo_evaluacion)){

                    foreach ($tipos_evaluacion as $key => $tipo){
                  
                      if (strpos($tipo->descripcion_empresa, $tipo_evaluacion) !== false || $tipo_evaluacion == $tipo->id){

                        $insertarCsv->tipo_evaluacion = $tipo->id;
                        break;
                      }
                    }
                  }
                
              	  $insertarCsv->status = 'No Iniciada';
                	$insertarCsv->id_periodo = $id_periodo;
              	  $insertarCsv->save();
                  $k++;
                  $evaluadores[] = $jefe;
                  $evaluados[] = $id;
			          }

                else{

                  $band = false;
                  $pos = array_search($jefe, $evaluadores);
                  $j++;
                  $contador_errores++;
                  $errores = $errores . $contador_errores . '. Fila ' . ($i + 1) . ': Fila Duplicada<br>';

                  if (!empty($pos)){

                    for ($x = $pos;$x < count($evaluadores);$x++){

                      if ($evaluadores[$x] != $jefe){

                        break;
                      }

                      if ($evaluados[$x] == $id){

                        /*$contador_errores++;
                        $band = true;
                        $j++;
                        $errores = $errores . $contador_errores . '. Fila ' . ($i + 1) . ': Fila Duplicada<br>';*/
                        break;
                      }
                    }
                  }

                  if (!$band){

                    $evaluadores[] = $jefe;
                    $evaluados[] = $id;
                  }
                }
              }

			        else{

                $contador_errores++;

                if (!in_array($jefe, $activos)){

                  $errores = $errores . $contador_errores . '. Fila ' . ($i + 1) . ': Evaluador Inactivo<br>';
                }

                else{

                  $errores = $errores .  $contador_errores . '. Fila ' . ($i + 1) . ': Evaluado Inactivo<br>';
                }

				        $j++;
              }
            }
          }

          $i++;
        }

        fclose($csvFile);

        Flash::success('Se agregaron ' . $k . ' evaluaciones. Total ' . ($i - 2) . '. Se rechazaron ' . $j . '<br>' . $errores);

        //return redirect()->to('importacion-evaluadores-csv');
        return redirect('/importacion-evaluadores-csv?id_periodo=' . $id_periodo);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Create the interaction red automatically
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create_red($period_id)
    {
        $period = Periodos::find($period_id);
        $dayToCheck = Carbon::now()->subDays(90);
        $employees = Employee::whereNotNull('jefe')->where('jefe', '!=', 0)->whereNull('deleted_at')->where('ingreso', '<', $dayToCheck)->orderBy('jefe')->get();

        if (count($employees) > 0){

          EvaluadorEvaluado::where('id_periodo', $period_id)->delete();
          $evaluados = array();
          $alls = array();
          $current_boss = '';
          $evaluador_evaluado = array();
          $evaluador_evaluado['id_periodo'] = $period_id;
          $boss = 0;
          $boss_model = 0;
          $duplicates = array();

          foreach ($employees as $key => $value){
            
            if ($current_boss != $value->jefe){

              if ($current_boss != ''){

                $temp_evaluados = $evaluados;

                foreach ($evaluados as $key2 => $value2){
                  
                  $temp = $value2 . '-' . $value2;

                  if (!in_array($temp, $duplicates)){

                    $evaluador_evaluado['id_evaluador'] = $value2;
                    $evaluador_evaluado['id_evaluado'] = $value2;
                    $alls[] = $evaluador_evaluado;

                    if (count($alls) > 20000){

                      DB::table('evaluador_evaluado')->insert($alls);
                      $alls = array();
                    }

                    $duplicates[] = $temp;
                  }

                  $temp = $boss . '-' . $value2;

                  if (!in_array($temp, $duplicates)){

                    $evaluador_evaluado['id_evaluador'] = $boss;
                    $evaluador_evaluado['id_evaluado'] = $value2;
                    $alls[] = $evaluador_evaluado;

                    if (count($alls) > 20000){

                      DB::table('evaluador_evaluado')->insert($alls);
                      $alls = array();
                    }

                    $duplicates[] = $temp;
                  }

                  if ($period->id_modalidad > 2){

                    $temp = $value2 . '-' . $boss;

                    if (!in_array($temp, $duplicates) && $boss_model->ingreso < $dayToCheck){

                      $evaluador_evaluado['id_evaluador'] = $value2;
                      $evaluador_evaluado['id_evaluado'] = $boss;
                      $alls[] = $evaluador_evaluado;

                      if (count($alls) > 20000){

                        DB::table('evaluador_evaluado')->insert($alls);
                        $alls = array();
                      }

                      $duplicates[] = $temp;
                    }
                  }

                  if ($period->id_modalidad > 1){

                    foreach ($temp_evaluados as $key3 => $value3){
                    
                      if ($value3 != $value2){

                        $temp = $value3 . '-' . $value2;

                        if (!in_array($temp, $duplicates)){

                          $evaluador_evaluado['id_evaluador'] = $value3;
                          $evaluador_evaluado['id_evaluado'] = $value2;
                          $alls[] = $evaluador_evaluado;

                          if (count($alls) > 20000){

                            DB::table('evaluador_evaluado')->insert($alls);
                            $alls = array();
                          }

                          $duplicates[] = $temp;
                        }
                      }
                    }
                  }
                }
              }

              $current_boss = $value->jefe;
              $boss_model = $value->boss;

              if (!empty($boss_model)){

                $boss = $boss_model->user->id;
                $evaluados = array();
              }

              else{

                $current_boss = '';
              }
            }

            $evaluados[] = $value->user->id;
          }

          if ($current_boss != ''){

            $temp_evaluados = $evaluados;

            foreach ($evaluados as $key => $value){
                  
              $temp = $value . '-' . $value;

              if (!in_array($temp, $duplicates)){

                $evaluador_evaluado['id_evaluador'] = $value;
                $evaluador_evaluado['id_evaluado'] = $value;
                $alls[] = $evaluador_evaluado;

                if (count($alls) > 20000){

                  DB::table('evaluador_evaluado')->insert($alls);
                  $alls = array();
                }

                $duplicates[] = array();
              }
              
              $temp = $boss . '-' . $value;

              if (!in_array($temp, $duplicates)){

                $evaluador_evaluado['id_evaluador'] = $boss;
                $evaluador_evaluado['id_evaluado'] = $value;
                $alls[] = $evaluador_evaluado;

                if (count($alls) > 20000){

                  DB::table('evaluador_evaluado')->insert($alls);
                  $alls = array();
                }

                $duplicates[] = $temp;
              }

              if ($period->id_modalidad > 2){

                $temp = $value . '-' . $boss;

                if (!in_array($temp, $duplicates) && $boss_model->ingreso < $dayToCheck){  

                  $evaluador_evaluado['id_evaluador'] = $value;
                  $evaluador_evaluado['id_evaluado'] = $boss;
                  $alls[] = $evaluador_evaluado;

                  if (count($alls) > 20000){

                    DB::table('evaluador_evaluado')->insert($alls);
                    $alls = array();
                  }

                  $duplicates[] = $temp;
                }
              }

              if ($period->id_modalidad > 1){

                foreach ($temp_evaluados as $key2 => $value2){
                    
                  if ($value2 != $value){

                    $temp = $value2 . '-' . $value;

                    if (!in_array($temp, $duplicates)){

                      $evaluador_evaluado['id_evaluador'] = $value2;
                      $evaluador_evaluado['id_evaluado'] = $value;
                      $alls[] = $evaluador_evaluado;

                      if (count($alls) > 20000){

                        DB::table('evaluador_evaluado')->insert($alls);
                        $alls = array();
                      }
                      
                      $duplicates[] = $temp;
                    }
                  }
                }
              }
            }
          }

          DB::table('evaluador_evaluado')->insert($alls);
          Flash::success('Se agregaron ' . count($alls) . ' evaluaciones.');
        }

        else{

          Flash::danger('Se agregaron 0 evaluaciones.');
        }

        return redirect('/matriz-evaluaciones/' . $period_id);
    }
}
