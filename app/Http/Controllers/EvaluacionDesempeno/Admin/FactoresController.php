<?php

namespace App\Http\Controllers\EvaluacionDesempeno\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

use App\Models\EvaluacionDesempeno\Admin\TiposFactores;
use App\Models\EvaluacionDesempeno\Admin\Factores;
use App\Models\EvaluacionDesempeno\Admin\FactoresNivelesDominios;
use App\Models\EvaluacionDesempeno\Admin\FactoresNivelesPuestos;
use App\Models\EvaluacionDesempeno\Admin\FamiliasFactores;
use App\Models\EvaluacionDesempeno\Admin\NivelesPuestos;
use App\Models\EvaluacionDesempeno\Admin\GruposAreas;
use App\Models\EvaluacionDesempeno\Admin\SubFamiliasFactores;
use App\Models\EvaluacionDesempeno\Admin\Etiquetas;
use DB;

class FactoresController extends Controller
{
	/**
     * Create a instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('admin');
    }
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$factores = Factores::orderBy('orden', 'ASC')->get();
		$FamiliasFactores = FamiliasFactores::pluck('nombre_para_organizacion', 'id');
		$subFamiliasFactores = SubFamiliasFactores::pluck('nombre', 'id');
		//$nivelesPuestos = NivelesPuestos::where('id', '!=', 1)->get();
		$nivelesPuestos = NivelesPuestos::all();
		$gruposAreas = GruposAreas::pluck('nombre', 'id');
		$tiposFactores = TiposFactores::pluck('name', 'id');
		$etiquetas = Etiquetas::pluck('name', 'valor');
		$orders = array();
		
		for($i = 1;$i <= count($factores) + 2;$i++){
			
			$orders[$i] = 'Orden: ' . $i;
		}

		if($nivelesPuestos->isEmpty()){
			unset($FamiliasFactores[3]);
		}

		if($gruposAreas->isEmpty()){
			unset($FamiliasFactores[5]);
		}

		return view('evaluacion-desempeno/Admin/factores/index', compact('factores', 'FamiliasFactores', 'nivelesPuestos', 'gruposAreas', 'subFamiliasFactores', 'orders', 'tiposFactores', 'etiquetas'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//dd($request);
		$factores = new Factores;

		$factores->nombre = $request->nombre;
		$factores->descripcion = $request->descripcion;
		$factores->status = 'Activado';
		$factores->id_tipo_factor = $request->id_tipo_factor;
		$factores->id_sub_familia = $request->subFamiliasFactores;
		$factores->id_familia = $request->familia;
		$factores->id_grupo_area = 0;

		if ($factores->id_familia == 5){

			$factores->id_grupo_area = $request->gruposAreas;
		}

		$factores->comentarios = $request->comentarios;
		$factores->orden = $request->orden;
		$factores->save();

		$factor = Factores::all();

		$lastFactor = $factor->last();

		$nivelDominio = new FactoresNivelesDominios;

		$dominios = $request->niveles_de_dominio;
		$califDominios = $request->calificacionDominio;
		$etiquetas = $request->etiqueta;

		if (!empty($dominios)){
		
			foreach($dominios as $key => $dominio){
				$nivelDominio = new FactoresNivelesDominios;
				$nivelDominio->id_factor = $lastFactor->id;
				$nivelDominio->nivel_dominio = $dominio;
				$nivelDominio->calificacion = 1;
				$nivelDominio->etiqueta = 'Nada';
				$nivelDominio->status = 'Activado';
				$nivelDominio->save();
			}
		}

			$califEsperada = $request->Calificación_Esperada;
			$peso = $request->peso;

			if (!empty($request->id_niveles_puesto)){

				foreach($request->id_niveles_puesto as $keyP => $idnivlP){
			
					if (!empty($califEsperada[$keyP])){

						$nivelPuesto = new FactoresNivelesPuestos;
						$nivelPuesto->id_factor = $lastFactor->id;
						$nivelPuesto->id_nivel_puesto = $idnivlP;
						$nivelPuesto->nivel_esperado = $califEsperada[$keyP];

						if (!empty($peso[$keyP])){

							$nivelPuesto->peso = $peso[$keyP];
						}

						$nivelPuesto->save();
					}
				}
			}

		Flash::success('El factor fue guardado correctamente.');

		return redirect()->to('factores');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$FamiliasFactores = FamiliasFactores::pluck('nombre_para_organizacion', 'id');
		//$nivelesPuestos = NivelesPuestos::where('id', '!=', 1)->get();
		$nivelesPuestos = NivelesPuestos::all();
		$gruposAreas = GruposAreas::pluck('nombre', 'id');
		$tiposFactores = TiposFactores::pluck('name', 'id');
		$etiquetas = Etiquetas::pluck('name', 'valor');

		$factores = Factores::find($id);
		$all_factores = Factores::all();
		
		$orders = array();
		$i = 1;
		
		for($i = 1;$i <= count($all_factores) + 2;$i++){
			
			$orders[$i] = 'Orden: ' . $i;
		}

		$i--;

		if ($i < $factores->orden){

			$orders[$factores->orden] = 'Orden: ' . $factores->orden;
		}

		$subFamiliasFactores = SubFamiliasFactores::pluck('nombre', 'id');

		$nivelesDominios = FactoresNivelesDominios::where('id_factor', '=', $factores->id)->get();

		$nivelesPuestosEdit = FactoresNivelesPuestos::where('id_factor', '=', $factores->id)->get();

		$nivelCount = $nivelesDominios->count();

		return view('evaluacion-desempeno/Admin/factores/edit', compact('FamiliasFactores', 'nivelesPuestos', 'gruposAreas', 'factores', 'nivelesDominios', 'nivelesPuestosEdit', 'nivelCount', 'id', 'subFamiliasFactores', 'orders', 'all_factores', 'tiposFactores', 'etiquetas'));

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//dd($request);
		$factores = Factores::find($id);

		$factores->nombre = $request->nombre;
		$factores->descripcion = $request->descripcion;
		$factores->id_tipo_factor = $request->id_tipo_factor;
		$factores->status = 'Activado';
		$factores->id_sub_familia = $request->subFamiliasFactores;
		$factores->id_familia = $request->familia;
		$factores->id_grupo_area = 0;

		if ($factores->id_familia == 5){

			$factores->id_grupo_area = $request->gruposAreas;
		}

		$factores->comentarios = $request->comentarios;
		$factores->orden = $request->orden;
		$factores->save();

		//$factor = Factores::all();

		//$lastFactor = $factor->last();
		$factors_with_results = DB::table('resultados')->groupBy('id_factor')->pluck('id_factor')->toArray();

		if (!in_array($id, $factors_with_results)){

			$nivelDominio = FactoresNivelesDominios::where('id_factor', '=', $factores->id)->delete();
		}

		//$nivelDominio->delete();

		$dominios = $request->niveles_de_dominio;
		$califDominios = $request->calificacionDominio;
		$etiquetas = $request->etiqueta;
		
		if (!empty($dominios) && !in_array($id, $factors_with_results)){

			foreach($dominios as $key => $dominio){
			
				$nivelDominio = new FactoresNivelesDominios;
				$nivelDominio->id_factor = $factores->id;
				$nivelDominio->nivel_dominio = $dominio;
				$nivelDominio->calificacion = 1;
				$nivelDominio->etiqueta = 'Nada';
				$nivelDominio->status = 'Activado';
				$nivelDominio->save();
			}
		}

		if (!in_array($id, $factors_with_results)){
		
			$nivelPuesto = FactoresNivelesPuestos::where('id_factor', '=', $factores->id)->delete();
		}

		//$nivelPuesto->delete();

		//if($request->familia == 3){

			if (!empty($request->id_niveles_puesto) && !in_array($id, $factors_with_results)){
			
				foreach($request->id_niveles_puesto as $keyP => $nivlP){

					if (!empty($request->Calificación_Esperada[$keyP])){
				
						$nivelPuesto = new FactoresNivelesPuestos;
						$nivelPuesto->id_factor = $factores->id;
						$nivelPuesto->id_nivel_puesto = $nivlP;
						$nivelPuesto->nivel_esperado = $request->Calificación_Esperada[$keyP];

						if (!empty($request->peso[$keyP])){

							$nivelPuesto->peso = $request->peso[$keyP];
						}

						$nivelPuesto->save();
					}
				}
			}
		//}

		Flash::success('El factor fue guardado correctamente.');

		return redirect()->to('factores');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
