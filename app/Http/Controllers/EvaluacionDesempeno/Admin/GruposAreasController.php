<?php

namespace App\Http\Controllers\EvaluacionDesempeno\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

use App\Models\Department;
use App\Models\EvaluacionDesempeno\Admin\DepartmentsGruposAreas;
use App\Models\EvaluacionDesempeno\Admin\GruposAreas;

class GruposAreasController extends Controller
{
    /**
     * Create a instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $gruposAreas = GruposAreas::select('id', 'nombre')->get();
      $departments_gruposAreas = array();
      $departments = array();

      foreach($gruposAreas as $gr){
        $departments_gruposAreas[] = DepartmentsGruposAreas::DepartmentGrupoArea($gr->id)->get();
      }

      $departments_in_group = DepartmentsGruposAreas::pluck('id_departments')->toArray();

      if (!empty($departments_in_group)){

        $departments = Department::whereNotIn('id', $departments_in_group)->orderBy('name')->get();
      }

      else{

        $departments = Department::orderBy('name')->get();
      }

      return view('evaluacion-desempeno/Admin/grupo-areas/index', compact('departments_gruposAreas', 'gruposAreas', 'departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments_in_use = DepartmentsGruposAreas::pluck('id_departments')->toArray();
        $departments = Department::whereNotIn('id', $departments_in_use)->get();
        //$otrasAreas = AreasGruposAreas::where('id_grupos_areas', '=', 1)->get();
        //dd($areas);

        return view('evaluacion-desempeno/Admin/grupo-areas/add', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*GruposAreas::create([
            'nombre' => $request->nombreGrupoArea
            ]);*/

        $gArea = GruposAreas::select('id')->where('nombre', 'LIKE', $request->nombreGrupoArea)->exists();

        if($gArea){
          Flash::error('Este registro ya existe');

          return view('evaluacion-desempeno/Admin/grupo-areas/index');
        }else{
          GruposAreas::create([
            'nombre' => $request->nombreGrupoArea
            ]);
        }

        $grupoArea = GruposAreas::select('id')->where('nombre', 'LIKE', $request->nombreGrupoArea)->first();

        $checks = $request->id_departments;

        foreach($checks as $check){
          //AreasGruposAreas::where('id_areas', '=', $check)->delete();
          /*AreasGruposAreas::where('id_areas', '=', $check)->update([
            'id_areas' => $check,
            'id_grupos_areas' => $grupoArea->id
            ]);*/
          DepartmentsGruposAreas::create([
            'id_departments' => $check,
            'id_grupos_areas' => $grupoArea->id
            ]);
        }

        /*$gruposAreas = GruposAreas::select('id', 'nombre')->get();

        $areasGruposAreas = AreasGruposAreas::AreaGrupoArea($gruposAreas)->get();

        foreach($gruposAreas as $gr){
          $areas_gruposAreas[] = AreasGruposAreas::AreaGrupoArea($gr->id)->get();
        }*/

        //Flash::success('Nombre del Grupo y Areas registrado satisfactoriamente.');

        return redirect()->to('areas');
        //return view('evaluacion-desempeno/Admin/grupo-areas/index', compact('areas_gruposAreas', 'gruposAreas'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //$gruposAreas = GruposAreas::select('id', 'nombre')->get();
      $idGrupoAreas = GruposAreas::where('id', $id)->get();

      foreach($idGrupoAreas as $idGA){
        $nombreGrupoAreas = $idGA->Name;
      }      
      
      $departments = Department::all();

      $departmentsGruposAreas = DepartmentsGruposAreas::DepartmentGrupoArea($id)->get();

      //$departmentsOtros = DepartmentsGruposAreas::DepartmentGrupoArea(1)->get();
      $departmentsOtros = DepartmentsGruposAreas::where('id_grupos_areas', '!=', $id)->get();
      $selected = $selectedOtros = array();

      foreach($departments as $d){ 
        foreach($departmentsGruposAreas as $gra){
          if($d->id == $gra->id_departments){
            $selected[] = $gra->id_departments;
          } 
        }
      }

      foreach($departments as $d){ 
        foreach($departmentsOtros as $do){
          if($d->id == $do->id_departments){
            $selectedOtros[] = $do->id_departments;
          } 
        }
      }

      return view('evaluacion-desempeno/Admin/grupo-areas/edit', compact('departments', 'nombreGrupoAreas', 'selected', 'id', 'selectedOtros', 'departmentsOtros', 'departmentsGruposAreas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $checksUpdate = $request->id_departments;

        if(is_null($checksUpdate) || empty($checksUpdate)){
          $idGrupoAreas = GruposAreas::where('id', $id)->get();

          foreach($idGrupoAreas as $idGA){
            $nombreGrupoAreas = $idGA->Name;
          }      
          
          $departments = Department::all();

          $departmentsGruposAreas = DepartmentsGruposAreas::DepartmentGrupoArea($id)->get();

          $departmentsOtros = DepartmentsGruposAreas::DepartmentGrupoArea(1)->get();
          $selected = $selectedOtros = array();

          foreach($departments as $d){ 
            foreach($departmentsGruposAreas as $gra){
              if($d->id == $gra->id_departments){
                $selected[] = $gra->id_departments;
              } 
            }
          }

          foreach($departments as $d){ 
            foreach($departmentsOtros as $do){
              if($d->id == $do->id_departments){
                $selectedOtros[] = $do->id_departments;
              } 
            }
          }

          Flash::error('Debes seleccionar al menos un departamento.');

          return view('evaluacion-desempeno/Admin/grupo-areas/edit', compact('departments', 'nombreGrupoAreas', 'selected', 'id', 'departmentsGruposAreas', 'departmentsOtros', 'selectedOtros'));
        }

        /*foreach($checksUpdate as $checkUp){
          AreasGruposAreas::create([
            'id_areas' => $checkUp,
            'id_grupos_areas' => $id
            ]);
        }

        $areasGruposAreas = AreasGruposAreas::AreaGrupoArea($id)->get();

        foreach($areasGruposAreas as $a){ 
          $areas_GruposAreas[] = $a->id_areas; 
        }*/

        //$result = array_diff($request->id_areas, $areas_GruposAreas );

        //dd($result);
        $nombreGruAre = GruposAreas::find($id);

        $nombreGruAre->nombre = $request->nombreGrupoArea;

        $nombreGruAre->save();

        $checksUpdate = $request->id_departments;

        //$selDel = AreasGruposAreas::where('id_grupos_areas', '=', $id)->get();

        //$selAdd = array();
        //$delSel = array();

        DepartmentsGruposAreas::where('id_grupos_areas', '=', $id)->delete();
        
        foreach($checksUpdate as $checkUp){

          DepartmentsGruposAreas::create([
            'id_departments' => $checkUp,
            'id_grupos_areas' => $id
            ]);
          /*$exist = AreasGruposAreas::where('id_areas', '=', $checkUp)->where('id_grupos_areas', '=', $id)->first();
          if(!$exist){
            $selAdd[] = $checkUp;
          }*/
        }

        /*for($i = 0; $i < count($selDel); $i++){
          if(!in_array($selDel[$i]['id_areas'], $checksUpdate)){
            $delSel[] = $selDel[$i]['id_areas'];
          }
        }

        if(!empty($selAdd)){
          foreach($selAdd as $ad){
            AreasGruposAreas::where('id_areas', '=', $ad)->update(['id_grupos_areas' => $id]);
          }
        }

        if(!empty($delSel)){
          foreach($delSel as $del){
            AreasGruposAreas::where('id_areas', '=', $del)->update(['id_grupos_areas' => 1]);
          }
        }*/

        return redirect()->to('areas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grupoArea = GruposAreas::where('id', '=', $id)->delete();
        $areaGrupoArea = DepartmentsGruposAreas::where('id_grupos_areas', '=', $id)->delete();
        Flash::success('El Área ha sido borrada');

      return redirect()->to('areas');
    }
}
