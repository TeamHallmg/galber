<?php
namespace App\Http\Controllers\EvaluacionDesempeno\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

use App\Models\EvaluacionDesempeno\Admin\Periodos;
use App\Employee;
use DB;

class PeriodosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('admin');
    }

    /**
     * Muestra los periodos
     *
     */
    public function index(){
      
      $preparatorio = false;

      // Obtiene todos los periodos
      $periodos = DB::select('select periodos.*, modalidades_periodos.nombre from periodos inner join modalidades_periodos ON (periodos.id_modalidad = modalidades_periodos.id)');

      // Checa todos los periodos
      foreach ($periodos as $key => $periodo){
        
        // El status del periodo es Preparatorio
        if ($periodo->status == 'Preparatorio'){

          $preparatorio = true;
          break;
        }
      }

      return view('evaluacion-desempeno/Admin/periodos/index', compact('periodos', 'preparatorio'));
    }

    /**
     * Crea un nuevo periodo
     *
     */
    public function create(){

      if (!empty($_POST['descripcion'])){

        $periodo = DB::insert('insert into periodos (fecha_inicio, fecha_cierre, descripcion, id_modalidad, status, organizacion) values (?, ?, ?, ?, ?, ?)', [$_POST['fecha_inicio'], $_POST['fecha_cierre'], $_POST['descripcion'], $_POST['modalidad'], $_POST['status'], 1]);
        
        if (!empty($periodo)){
			
		      $result = DB::select('SELECT MAX(id) AS max_id FROM periodos');
          $id = $result[0]->max_id;

          if (!empty($_POST['factores'])){

            foreach ($_POST['factores'] as $key => $value){
              
              DB::insert('INSERT INTO periodo_factor (id_periodo, id_factor) values (?, ?)', [$id, $value]);
            }
          }

          flash('El periodo fue creado correctamente');
        }

        else{

          flash('Error al crear el periodo. Intente de nuevo');
        }

        return redirect('periodos');
      }

      // Obtiene los periodos con status Preparatorio
      $periodos = DB::select('SELECT id FROM periodos WHERE status = ?', ['Preparatorio']);

      // Hay periodos con estado Preparatorio
      if (!empty($periodos)){

        // Redirecciona a la pagina del listado de periodos
        return redirect('periodos');
      }
		
	    $factores = DB::select('SELECT id, nombre, orden FROM factores WHERE status = ?', ['Activado']);
      return view('evaluacion-desempeno/Admin/periodos/create', compact('factores'));
    }

    /**
     * Edita o actualiza un periodo
     *
     */
    public function edit($id = 0){

      // No existe el id para la pagina de edicion
	   if (empty($id)){
		   
		  $fecha = date('Y-m-d H:i:s');
      $periodo = Periodos::find($_POST['id']);

      if ($periodo->status == 'Preparatorio' && $_POST['status'] == 'Abierto'){

        $end = 1000;
        $start = 0;
        $employees = array();

        do{

          if ($start > 0){

            $employees = Employee::where('correoempresa', 'not like', '%hallmg%')->whereNull('deleted_at')->offset($start)->limit($end)->get()->toArray();
          }

          else{

            $employees = Employee::where('correoempresa', 'not like', '%hallmg%')->whereNull('deleted_at')->limit($end)->get()->toArray();
          }

          if (!empty($employees)){

            DB::table('employees_desempeno')->insert($employees);
            $start = $end;
            $end = $end + 1000;
          }

          }while(!empty($employees));

          $employees_desempeno = array();
          $employees_desempeno['id_periodo'] = $_POST['id'];
          DB::table('employees_desempeno')->where('id_periodo', 0)->update($employees_desempeno);
        }

        else{

          if ($_POST['status'] == 'Cancelado'){

          
            DB::table('employees_desempeno')->where('id_periodo', $_POST['id'])->delete();
          } 
        }

        DB::update('update periodos set fecha_inicio = "' . $_POST['fecha_inicio'] . '", fecha_cierre = "' . $_POST['fecha_cierre'] . '", descripcion = "' . $_POST['descripcion'] . '", id_modalidad = ' . $_POST['modalidad'] . ', status = "' . $_POST['status'] . '", updated_at = "' . $fecha . '" where id = ?', [$_POST['id']]);
		    DB::delete("DELETE FROM periodo_factor WHERE id_periodo = ?", [$_POST['id']]);

        if (!empty($_POST['factores'])){

          foreach ($_POST['factores'] as $key => $value){
              
            DB::insert('INSERT INTO periodo_factor (id_periodo, id_factor) values (?, ?)', [$_POST['id'], $value]);
          }
        }
		  
		// El status del periodo cambio a Cerrado
        /*if ($nuevo_status != $actual_status && $nuevo_status == 'Cerrado'){

          // El usuario logueado es admin
          if (auth()->user()->role == 'admin'){

            // Existe el archivo CSV
            if (file_exists('/csv/Finvivirleasing.csv')){

              // Abrimos el archivo CSV para leerlo
              if (($handle = fopen("/csv/Finvivirleasing.csv", "r")) !== FALSE){

                // Leemos la primer fila
                if (($fileop = fgetcsv($handle, 1000, ",")) !== false){

                  $id_periodo = $_POST['id'];

                  // Borramos todos los registros de la matriz de evaluacion
                  DB::delete("DELETE FROM evaluador_evaluado WHERE id_periodo = $id_periodo");
                  
                  $indice_id = $indice_jefe = $i = 0;

                  // Leemos todas las columnas de la primer fila del archivo CSV
                  while (!empty($fileop[$i])){
              
                    // Convertimos el nombre de la columna a mayusculas
                    $nombre_columna = strtoupper($fileop[$i]);

                    // Revisamos el nombre de la columna
                    switch ($nombre_columna){
                
                      case 'NOMINA':
                        $indice_id = $i;
                        break;

                      case 'NOMINA JEFE INMEDIATO':
                        $indice_jefe = $i;
                    }

                    $i++;
                  }

                  // Leemos todos los empleados del archivo CSV
                  while(($fileop = fgetcsv($handle, 1000, ",")) !== false){

                    $id = $fileop[$indice_id];
                    $id_jefe = $fileop[$indice_jefe];
                
                    // Insertamos la evaluacion del jefe
                    DB::insert("INSERT INTO evaluador_evaluado (id_evaluador,id_evaluado,status,id_periodo) VALUES (?,?,?,?)", [$id_jefe,$id,'No Iniciada',$id_periodo]);

                    // Insertamos la autoevaluacion
                    DB::insert("INSERT INTO evaluador_evaluado (id_evaluador,id_evaluado,status,id_periodo) VALUES (?,?,?,?)", [$id,$id,'No Iniciada',$id_periodo]);
                  }
                }
              }
            }
          }
        }*/
        
        //if (!empty($periodo)){

          flash('El periodo fue guardado correctamente');
        /*}

        else{

          flash('Error al guardar el periodo. Intente de nuevo');
        }*/

        return redirect('periodos');
      }

      $periodo_abierto = false;
      
      // Obtiene todos los periodos abiertos
      $periodos = DB::select("SELECT status FROM periodos WHERE status = ? AND id != ?", ['Abierto', $id]);

      // Hay periodos abiertos
      if (count($periodos) > 0){

        $periodo_abierto = true;
      }

      $periodo = DB::select('select periodos.*, modalidades_periodos.nombre from periodos inner join modalidades_periodos ON (periodos.id_modalidad = modalidades_periodos.id) where periodos.id = ?', [$id]);
	    $factores = DB::select('SELECT id, nombre, orden FROM factores WHERE status = ?', ['Activado']);
      $periodo_factores = DB::select('SELECT id_factor FROM periodo_factor WHERE id_periodo = ?', [$id]);
      $factores_periodo = array();

      foreach ($periodo_factores as $key => $factor){
        
        $factores_periodo[] = $factor->id_factor;
      }
		
      return view('evaluacion-desempeno/Admin/periodos/edit', compact('periodo', 'factores', 'factores_periodo', 'periodo_abierto'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = 0){
      
      if (!empty($_POST['id'])){

        $periodo = DB::delete('DELETE FROM periodos WHERE id = ?', [$_POST['id']]);
        
        if (!empty($periodo)){

          DB::delete('DELETE FROM evaluador_evaluado WHERE id_periodo = ?', [$_POST['id']]);
          DB::delete('DELETE FROM resultados WHERE id_periodo = ?', [$_POST['id']]);
          DB::delete('DELETE FROM periodo_factor WHERE id_periodo = ?', [$_POST['id']]);

          flash('El periodo fue borrado correctamente');
        }

        else{

          flash('Error al borrar el periodo. Intente de nuevo');
        }

        return redirect('/periodos');
      }

      $periodo = DB::select('SELECT id, descripcion FROM periodos WHERE id = ?', [$id]);
      return view('evaluacion-desempeno/Admin/periodos/delete', compact('periodo'));
    }
}
