<?php
namespace App\Http\Controllers\EvaluacionDesempeno\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

use App\Models\EvaluacionDesempeno\Admin\TiposFactores;
use DB;

class TiposFactoresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Muestra los tipos de factores
     *
     */
    public function index(){

      // Obtiene todos los tipos de factores
      $tiposFactores = TiposFactores::all();

      return view('evaluacion-desempeno/Admin/tipos-factores/index', compact('tiposFactores'));
    }

    /**
     * Crea un nuevo tipo de factor
     *
     */
    public function create(){

      if (!empty($_POST['name'])){

        $tipoFactor = DB::insert('insert into tipos_factores (name) values (?)', [$_POST['name']]);
        
        if (!empty($tipoFactor)){

          flash('El tipo de factor fue creado correctamente');
        }

        else{

          flash('Error al crear el tipo de factor. Intente de nuevo');
        }

        return redirect('tipos-factores');
      }

      return view('evaluacion-desempeno/Admin/tipos-factores/create');
    }

    /**
     * Edita o actualiza un tipo de factor
     *
     */
    public function edit($id = 0){

      // No existe el id para la pagina de edicion
	    if (empty($id)){
		   
		    $fecha = date('Y-m-d H:i:s');
        DB::update('update tipos_factores set name = "' . $_POST['name'] . '", updated_at = "' . $fecha . '" where id = ?', [$_POST['id']]);
		    flash('El tipo de factor fue guardado correctamente');
        return redirect('tipos-factores');
      }

      $tipoFactor = DB::select('select * FROM tipos_factores where id = ?', [$id]);
      return view('evaluacion-desempeno/Admin/tipos-factores/edit', compact('tipoFactor'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = 0){
      
      if (!empty($_POST['id'])){

        $tipoFactor = DB::delete('DELETE FROM tipos_factores WHERE id = ?', [$_POST['id']]);
        
        if (!empty($tipoFactor)){

          flash('El tipo de factor fue borrado correctamente');
        }

        else{

          flash('Error al borrar el tipo de factor. Intente de nuevo');
        }

        return redirect('/tipos-factores');
      }

      $tipoFactor = DB::select('SELECT id, name FROM tipos_factores WHERE id = ?', [$id]);
      return view('evaluacion-desempeno/Admin/tipos-factores/delete', compact('tipoFactor'));
    }
}
