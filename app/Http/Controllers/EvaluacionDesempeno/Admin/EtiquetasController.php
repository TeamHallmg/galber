<?php
namespace App\Http\Controllers\EvaluacionDesempeno\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

use App\Models\EvaluacionDesempeno\Admin\Etiquetas;
use DB;

class EtiquetasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Muestra las etiquetas
     *
     */
    public function index(){

      // Obtiene todas las etiquetas
      $etiquetas = Etiquetas::all();

      return view('evaluacion-desempeno/Admin/etiquetas/index', compact('etiquetas'));
    }

    /**
     * Crea una nueva etiqueta
     *
     */
    public function create(){

      if (!empty($_POST['name'])){

        $etiqueta = DB::insert('insert into etiquetas_desempeno (name, valor) values (?,?)', [$_POST['name'], $_POST['valor']]);
        
        if (!empty($etiqueta)){

          flash('La etiqueta fue creada correctamente');
        }

        else{

          flash('Error al crear la etiqueta. Intente de nuevo');
        }

        return redirect('etiquetas');
      }

      return view('evaluacion-desempeno/Admin/etiquetas/create');
    }

    /**
     * Edita o actualiza una etiqueta
     *
     */
    public function edit($id = 0){

      // No existe el id para la pagina de edicion
	    if (empty($id)){
		   
		    $fecha = date('Y-m-d H:i:s');
        DB::update('update etiquetas_desempeno set name = "' . $_POST['name'] . '", valor = "' . $_POST['valor'] . '", updated_at = "' . $fecha . '" where id = ?', [$_POST['id']]);
		    flash('La etiqueta fue guardada correctamente');
        return redirect('etiquetas');
      }

      $etiqueta = DB::select('select * FROM etiquetas_desempeno where id = ?', [$id]);
      return view('evaluacion-desempeno/Admin/etiquetas/edit', compact('etiqueta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = 0){
      
      if (!empty($_POST['id'])){

        $etiqueta = DB::delete('DELETE FROM etiquetas_desempeno WHERE id = ?', [$_POST['id']]);
        
        if (!empty($etiqueta)){

          flash('La etiqueta fue borrada correctamente');
        }

        else{

          flash('Error al borrar la etiqueta. Intente de nuevo');
        }

        return redirect('/etiquetas');
      }

      $etiqueta = DB::select('SELECT id, name FROM etiquetas_desempeno WHERE id = ?', [$id]);
      return view('evaluacion-desempeno/Admin/etiquetas/delete', compact('etiqueta'));
    }
}
