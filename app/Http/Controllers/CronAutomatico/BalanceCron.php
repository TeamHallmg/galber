<?php

namespace App\Http\Controllers\CronAutomatico;

use App\Models\Cron\CronRunLog;
use App\Http\Controllers\EmployeeController;

class BalanceCron{

    public function __construct(){

    }

    private function daily()
    {
        $currDay = strtotime(date('Y-m-d H:m:s'));
        $lastDayRun = CronRunLog::where('cron_type', 'vacation_balance')->where('frequence','daily')->orderBy('date','DESC')->first();
        
        if(!$lastDayRun){
            $lastDayRun = $currDay;
        }else{                   
            $lastDayRun = strtotime($lastDayRun->date);
            $lastDayRun = strtotime('+1 day',$lastDayRun);
        }
        $notifications = [];
        while($lastDayRun <= $currDay)
        {
            // Si el usuario no tiene balances del tipo vacaciones con la fecha 'until' en null, se crean registros nuevos con su proximo año vacacional y dias de vacaciones con fecha until en 'null', de lo contrario solo se actualizan las vacaciones de esos dias
            // 
            // Este le va actualizando las vacaciones proporcionalmente a los usuarios
            // se lleva todo el getMyNextYear() del usuario, modificando y agregandoles dias, cuando el usuario cumple un año en escencia su unica linea con fecha 'until' en 'null', se le agregara fecha de caducacion, y al siguiente dia que se ejecute el cron se le creara una nueva linea de balances
            // 
            $mensajes = [];
            $mensajes[] = EmployeeController::setPartials($lastDayRun);
            // Actualiza el balance de todos los registros con fecha 'until' en null que cumplan un año trabajando
            $mensajes[] = EmployeeController::processVacations($lastDayRun);//Actualiza los benefits que no tiene "Until"
            EmployeeController::removeBenefits($lastDayRun);//Si la fecha "until" ya paso se borran
            
            $run = CronRunLog::create([
                'date' => date('Y-m-d', $lastDayRun),
                'cron_type' => 'vacation_balance',
                'frequence' => 'daily',
            ]);

            $notifications[] = [
                'date' => date('Y-m-d', $lastDayRun),
                'msj' => $mensajes,
            ];
            $lastDayRun = strtotime("+1 day",$lastDayRun);
        }
        // if(!empty($notifications)){
        //     try {
        //         \Mail::send('emails.cronErrors', ['notifications' => $notifications], function ($message) {
        //             $message->subject("Reporte de Errores en el Cron");
        //             $message->to('victor.gonzalez@hallmg.com');
        //         });
        //     } catch (\Exception $th) {
        //         dd($th->getMessage(), $notifications);
        //     }
        // }
    }
    private function weekly($date)
    {
        BenefitsController::process('weekly', $date);
        Cronlog::create([
            'date' => date('Y-m-d', $date),
            'cron_type', 'vacation_balance',
            'frequence' => 'weekly'
        ]);
    }
    private function monthly($date)
    {
        BenefitsController::process('monthly', $date);
        Cronlog::create([
            'date' => date('Y-m-d', $date),
            'cron_type', 'vacation_balance',
            'frequence' => 'monthly'
        ]);
        CronController::removelog();
    }
    private function yearly($date)
    {
        EmployeeController::fixVacations();
        EmployeeController::processComodin($date);
    }

    public function wasProcess($date,$type,$margin = null)
    {
        switch ($type) {
            case 'daily':
                $cron = CronRunLog::where('date', $date)
                ->where('cron_type', 'vacation_balance')
                ->where('frequence','daily')
                ->first();
                if( is_null($cron) ){  $this->daily();  }
                break;
            
            case 'weekly':
                $date2 = $date;
                $date = strtotime($date);
                $cont = 0;
                while ('Tue' != date('D',$date)) {
                    $cont++;
                    $date = strtotime("-1 day",$date);
                }
                if($cont <= $margin){
                    $cron = CronRunLog::where('date',date('Y-m-d',$date))
                    ->where('cron_type', 'vacation_balance')
                    ->where('frequence','weekly')
                    ->first();
                    if(is_null($cron)){
                        $this->weekly($date);    
                    }
                }
                break;
            
            case 'monthly':
                $date2 = $date;
                $date = strtotime($date);
                $cont = 0;
                while ('01' != date('d',$date)) {
                    $cont++;
                    $date = strtotime("-1 day",$date);
                }                
                if($cont <= $margin){
                    $cron = CronRunLog::where('date',date('Y-m-d',$date))
                    ->where('cron_type', 'vacation_balance')
                    ->where('frequence','monthly')
                    ->first();
                    if(is_null($cron)){
                        $this->monthly($date);    
                    }
                }
                break;
            
            case 'yearly':
                $date2 = $date;
                $date = strtotime($date);
                $cont = 0;
                while ('0' != date('z',$date)) {
                    $cont++;
                    $date = strtotime("-1 day",$date);
                }
                if($cont <= $margin){
                    $cron = CronRunLog::where('date',date('Y-m-d',$date))
                    ->where('cron_type', 'vacation_balance')
                    ->where('frequence','yearly')
                    ->first();
                    if(is_null($cron)){
                        $this->yearly($date);
                    }
                }
                break;
            
            default:
                break;
        }
    }

    public function index()
    {
        $today = date('Y-m-d');
        $this->wasProcess($today, 'daily');
        // $this->wasProcess($today,'weekly',4);
        // $this->wasProcess($today,'monthly',7);
        // $this->wasProcess($today,'yearly',31);
            
    }

    public static function removelog()
    {
        $time = date('Y-m-d H:i:s', strtotime('-3 months'));
        // Responselog::where('created_at','<',$time)->delete();
        // Errorlog::where('created_at','<',$time)->delete();
    }
}

