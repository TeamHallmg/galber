<?php

namespace App\Http\Controllers\CronAutomatico;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Models\Moodle\User as mdl_user;

class MoodleCronController extends Controller
{
    public static function import(){
	    set_time_limit(0); // Necesario por la cantidad de registros

		if($qry = User::whereNotNull('employee_id')->get()){
			mdl_user::query()->update(['suspended' => 1]);
			foreach ($qry as $key => $r) {
				$user = mdl_user::where('icq',$r->id)->first();
				if($user){
					$user->update([
                        'confirmed' => 1,
                        'suspended' => 0,
                        'username' => $r->email,
                        'firstname' => $r->employee->nombre,
                        'lastname' => $r->employee->paterno,
                        'email' => $r->email,
                        'department' => $r->employee->puesto
                    ]);
				}else{
					if($r->employee->fuente == 'manual'){
						$manual = mdl_user::where('email',$r->email)->first();
						if($manual){
							$manual->update([
                                'confirmed' => 1,
                                'suspended' => 0,
                                'username' => $r->email,
                                'password' => $r->password,
                                'firstname' => $r->employee->nombre,
                                'lastname' => $r->employee->paterno,
                                'email' => $r->email,
                                'department' => $r->employee->puesto,
                                'lang' => 'es',
                                'icq' => $r->id
                            ]);
						}else{
							mdl_user::insert([
                                'confirmed' => 1,
                                'mnethostid' => 1,
                                'username' => $r->email,
                                'password' => $r->password,
                                'firstname' => $r->employee->nombre,
                                'lastname' => $r->employee->paterno,
                                'email' => $r->email,
                                'department' => $r->employee->puesto,
                                'lang' => 'es',
                                'icq' => $r->id
                            ]);
						}
					}else{
						mdl_user::insert([
                            'confirmed' => 1,
                            'mnethostid' => 1,
                            'username' => $r->email,
                            'password' => $r->password,
                            'firstname' => $r->employee->nombre,
                            'lastname' => $r->employee->paterno,
                            'email' => $r->email,
                            'department' => $r->employee->puesto,
                            'lang' => 'es',
                            'icq' => $r->id
                        ]);
					}
				}
			}
		}
	}
}
/*
					$DB_MOODLE->query(
						"INSERT INTO mdl_user_info_data (userid, fieldid, data)
						VALUES
							($mdlmdl_user->id,1,'$r->empresa'),
							($con->insert_id,2,'$r->fuente'),
							($mdlmdl_user->id,3,'$r->puesto'),
							($mdlmdl_user->id,4,'$r->region'),
							($mdlmdl_user->id,5,'$r->sucursal')
						ON DUPLICATE KEY UPDATE data = VALUES (data)"
					);*/

/*$DB_MOODLE->query(
						"INSERT INTO mdl_user_info_data (userid, fieldid, data)
						VALUES
							($con->insert_id,1,'$r->empresa'),
							($con->insert_id,2,'$r->fuente'),
							($con->insert_id,3,'$r->puesto'),
							($con->insert_id,4,'$r->region'),
							($con->insert_id,5,'$r->sucursal')"
					);*/