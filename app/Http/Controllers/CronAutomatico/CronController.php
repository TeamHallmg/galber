<?php

namespace App\Http\Controllers\CronAutomatico;

use Hash;
use Mail;
use Exception;

use App\User;
use App\Models\Moodle\User as UserMdl;
use App\Employee;
use Carbon\Carbon;
use App\Models\Area;
use App\Models\Region;
use App\Models\Direction;
use App\Models\Department;
use App\Models\Enterprise;
use App\Models\JobPosition;
use App\Models\Cron\LogChanges;
use App\Models\Vacations\Balances;

use App\Models\Vacations\Benefits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Schema\ColumnDefinition;
use App\Http\Controllers\CronAutomatico\MoodleCronController as MoodleImport;

class CronController extends Controller
{
    public function __construct() {
        //$this->middleware('auth');
        $this->middleware('auth')->only(['manualImport', 'processImport']);
        $this->middleware('permission:user_admin')->only(['manualImport', 'processImport']);

        $this->separador = env('IMPORT_DELIMITER', ',');
        $this->pathLayout = 'Interface/';
        $this->fileName = env('IMPORT_FILENAME', 'layout_galber.csv'); //Nombre del archivo a importar
        $this->backUpFolder = 'filesProcessed/';

        $this->strictEncoding = true;
        // $this->encoding = 'UTF-8';
        //   $this->extern_fileName = 'csv_externo';

        $this->dataError = [];
        $this->created_users = [];
        $this->create_errors = [];
        $this->updated_users = [];
        $this->update_errors = [];
        $this->deleted_users = [];
        $this->delete_errors = [];
        $this->balance_errors = [];
        $this->restored_users = [];
        $this->restore_errors = [];
        $this->changes = [];

        $this->lastReadedRow = [];
        $this->pivotKey = 'rfc';
        $this->folder = '';

        $this->commitData = true;
        $this->failedToBkpFile = false;
        $this->checkColumnsCount = true;
        $this->checkIfHasHeaders = true;
        $this->allowEmptyFile = false;
        $this->usesEmployeeModel = true;
        $this->usesMoodleIntegration = false;
        $this->columnTypes = [];
        $this->setToNull_ifCantConvert = true;
        $this->keepTrackOfChanges = true;
        $this->detectedSources = [];
        $this->firstDetectedSource = null;
        $this->workOnlyWithSameSource = false;
        $this->workWithVacations = false;
        $this->substractToCalculateAmount = true;
        $this->newUsers = [];
        $this->sendMailToNewUsers = false;
        $this->vacationBenefit = null;
        $this->defaultPassword = 'Soygalber!'; // Contraseña por defecto para nuevos usuarios
        $this->fieldsToEncryp = ['password']; // Campos a encriptar
        $this->unsetIfEmpty = ['password']; // Eliminar campos que vengan vacios, para no actulizar con información en blanco
        $this->userOriginalPassword = [];

        $this->manualImport = false;
        $this->allowedfileExtensions = ['csv'];
        $this->originalFileName = null;
        $this->defaultFileName = 'manual_import.csv';

        /*
         * Este bloque es por el momento para cuando usamos los dos modelos 
         * Para la tabla de personal el arreglo de datos debe de venir en arreglo separado por comas
         * Para la tabla de users el arreglo debe de venir arreglo con llaves, 
         *      La llave referencia a los datos en el archivo y el valor a la tabla de users
         */

        /* Esta variable nos sirve para indicarle que campo tomara del modelo de personal
         * para usar en el usuario. Se incluye la linea de codigo para Usar. Ejemplo
         * 
         * $personal->{$this->personalFieldToUser['email']},
         */
        $this->personalFieldToUser = ['email' => 'correoempresa'];
        $this->uniquePersonalSimulatedFields = ['correoempresa', 'rfc'];
        $this->uniqueUserSimulatedFields = ['correoempresa' => 'email'];
        /*****/
        $this->canNotBeEmpty = ['correoempresa', 'rfc']; // idempleado
        $this->hierarchyFields = ['direccion' => 'Sin Dirección', 'departamento' => 'Sin Departamento', 'area' => 'Sin Área', 'puesto' => 'Sin Puesto'];
        $this->except_model_fields = ['departamento', 'puesto', 'empresa', 'idempresa', 'area', 'region'];
        $this->fieldsToIgnoreTrack = ['updated_at', 'created_at', 'fecha', 'password'];
        $this->fileHeaders = [
            'idempleado',
            'nombre',
            'paterno',
            'materno',
            'fuente',
            'rfc',
            'curp',
            'nss',
            'correoempresa',
            'correopersonal',
            'nacimiento',
            'sexo',
            'civil',
            'telefono',
            'extension',
            'celular',
            'ingreso',
            'fechapuesto',
            'jefe',
            'direccion',
            'departamento',
            'seccion',
            'puesto',
            'grado',
            'region',
            'sucursal',
            'idempresa',
            'empresa',
            'division',
            'marca',
            'centro',  
            'checador',
            'turno',
            'tiponomina',
            'clavenomina',
            'nombrenomina',
            'generalista',
            'relacion',
            'contrato',
            'horario',
            'jornada',
            'calculo',
            'vacaciones',
            'flotante',
            'base',
            'rol',
            'password',
            'extra1',
            'extra2',
            'extra3',
            'extra4',
            'extra5',
            'fecha',
            'version'
        ];

        $this->resumeTitles = [
            'data_error' => 'Error de Información',
            'created_users' => 'Usuarios Creados',
            'create_error' => 'Error al Crear',
            'updated_users' => 'Usuarios Actualizados',
            'update_errors' => 'Error al Actualizar',
            'deleted_users' => 'Usuarios Eliminados',
            'delete_errors' => 'Error al Eliminar',
            'balance_errors' => 'Error en Balance',
            'restored_users' => 'Usuarios Restaurados',
            'restore_errors' => 'Error al Restaurar'
        ];

        $this->exceptFields = []; // Campos no a comparar en el historial de cambios
    
        $this->campos = array();

        $this->table = 'people'; //Cambiar el nombre de la tabla

        $this->path = Storage::disk('local')
                        ->getDriver()
                        ->getAdapter()
                        ->getPathPrefix();
    }

    public function bkpCronFile() {
        $timestamp = Carbon::now()->timestamp.' - ';
        $to_copy = $this->path . $this->pathLayout . $this->fileName;
        $to_store = $this->path . $this->pathLayout . $this->backUpFolder;
        if(!is_dir($to_store)) {
            mkdir($to_store, 0777, true);
        }
        $to_store .= $timestamp;
        if($this->manualImport) {
            $to_store .= $this->originalFileName;
        } else {
            $to_store .= $this->fileName;
        }
        try {
            $success = \File::copy($to_copy, $to_store);
        } catch(\Throwable $th) {
            dd($th);
            $this->failedToBkpFile = true;
        }
    }

    /**
     * Funcion principal del cron, la cual es llamada por el archivo de rutas
     *
     * @return void
     */
    public function mainFunction() {
        try {
            $data = $this->readFile();
        } catch(\Throwable $th) {
            dd($th);
        }
        $this->proccessLayout($data);
        // $this->sendReportEmail();
    }

    public function proccessLayout($data) {
        $this->loadColumnTypes();
        if($this->allowEmptyFile || count($data) > 0) {
            $this->bkpCronFile();
            // dd('rawr');
            $data = $this->formatData($data);
            $data = $this->fillHierarchyInUsers($data);
            $hierarchy = $this->loadDirections($data);
            $hierarchy = $this->loadDepartments($data, $hierarchy);
            $hierarchy = $this->loadAreas($data, $hierarchy);
            $hierarchy = $this->loadJobPositions($data, $hierarchy);
            $enterprises = $this->loadEnterprise($data);
            $regions = $this->loadRegions($data);
            $this->loadVacationBenefit();
            $this->loadInformationToDatabase($data, $hierarchy, $enterprises, $regions);
            $this->sendWelcomeEmail();
        }
    }

    public function processImport($filename) {
        $this->manualImport = true;
        $this->fileName = $this->defaultFileName;
        $this->originalFileName = $filename;
        // $this->workOnlyWithSameSource = true; // Para evitar desastres
        try {
            $data = $this->readFile();
        } catch(\Throwable $th) {
            return redirect('admin-de-usuarios')->with("alert-danger", "Error al procesar archivo... ".$th->getMessage());
        }
        $this->proccessLayout($data);

        $report_data = $this->getReportData();
        $resume = [];

        foreach($report_data as $key => $row) {
            $resume[$key] = [
                'title' => $this->resumeTitles[$key],
                'count' => count($row),
                'data' => $row
            ];
        }
        $changes = $this->changes;

        return view('cron.resume', compact('resume', 'changes'));
        // $this->sendReportEmail();
    }

    public function manualImport(Request $request) {
        try {
            $file = $request->file;
            $extension = $file->getClientOriginalExtension();
            if (in_array($extension, $this->allowedfileExtensions)) {
                $this->originalFileName = $file->getClientOriginalName();
                $file->move($this->path . $this->pathLayout, $this->defaultFileName);
                $this->fileName = $this->defaultFileName;
            } else {
                return redirect()->back()->with("alert-danger", "Solo se permiten archivos con extensión csv.");
            }

            $data = [
                'layout' => $this->readFile(),
                'headers' => $this->fileHeaders,
                'filename' => $this->originalFileName,
                'summary' => [
                    'created_users' => [
                        'title' => 'Tentativo de Usuarios a Crear',
                        'count' => 0
                    ],
                    'updated_users' => [
                        'title' => 'Tentativo de Usuarios a Actualizar',
                        'count' => 0
                    ],
                    'deleted_users' => [
                        'title' => 'Tentativo de Usuarios a Eliminar',
                        'count' => 0
                    ]
                ]
            ];

            $data = $this->getPreviewSummary($data);
            
            return view('cron.preview', compact('data'));
        } catch(\Throwable $th) {
            dd($th);
            flash($th->getMessage())->error();
            return redirect()->back();
        }
    }

    private function getPreviewSummary($data) {
        // Haremos un estimado previo
        foreach($data['layout'] as $row) {
            $personal = Employee::withTrashed()
            ->where($this->pivotKey, $row[$this->pivotKey])
            ->first();

            if($personal) {
                $data['summary']['updated_users']['count'] += 1;
            } else {
                $data['summary']['created_users']['count'] += 1;
            }

            if($this->workOnlyWithSameSource && is_null($this->firstDetectedSource)){
                $this->firstDetectedSource = $row['fuente'];
            }
            if(!in_array($row['fuente'], $this->detectedSources)) {
                $this->detectedSources[] = $row['fuente'];
            }

            $personalNotToDelete[] = $row[$this->pivotKey];
        }

        $queryPersonals = Employee::whereNotIn($this->pivotKey, $personalNotToDelete)->when($this->workOnlyWithSameSource, function($q) {
            $q->where('fuente', $this->firstDetectedSource);
        })->when(!$this->workOnlyWithSameSource, function($q) {
            $q->whereIn('fuente', $this->detectedSources);
        });
        $data['summary']['deleted_users']['count'] = $queryPersonals->count();

        return $data;
    }

    /**
     * Leeremos el archivo usando la definición de variables en el construnctor
     * Se validaran ciertas cosas formateandolo todo en un array de datos
     * 
     * Nota: Si el separadador no es el correcto el archivo funciona pero se acomoda mal 
     *
     * @return void
     */
    public function readFile(){
        $File = fopen($this->path . $this->pathLayout . $this->fileName, 'r');
        $i = 0;
        $csvData = [];
        while ($line = fgetcsv($File, 0, $this->separador)) {
            $data = [];
            if($this->checkIfHasHeaders) {
                if($this->check_if_firstrow_match_headers($line)) {
                    $this->checkIfHasHeaders = false;
                } else{
                    // dd('Aqui muere porque las cabeceras no cuadran');
                    throw new Exception('Aqui muere porque las cabeceras no cuadran', 1);
                }
                if(!$this->check_if_delimiter_is_correct($line)) {
                    // dd('Muero, posible fallo con el delimitador', $line);
                    throw new Exception('Muero, posible fallo con el delimitador -> '.$line, 1);
                }
            } else {
                if(!$this->check_if_delimiter_is_correct($line)) {
                    // dd('Muero, posible fallo con el delimitador', $line);
                    throw new Exception('Muero, posible fallo con el delimitador -> '.$line, 1);
                }
                //Leemos toda las lineas y las acomodamos ordenadas
                for($i = 0; $i < count($line); $i++) {
                    $readed_line = trim($line[$i]);
                    if($this->strictEncoding) {
                        // if(mb_detect_encoding($readed_line) != $this->encoding) {
                        //     throw new Exception('Muero, posible fallo con la codificación del archivo...', 1);
                        // }
                        if(!mb_check_encoding($readed_line, 'UTF-8') || !($readed_line === mb_convert_encoding(mb_convert_encoding($readed_line, 'UTF-32', 'UTF-8' ), 'UTF-8', 'UTF-32'))) {
                            $readed_line = utf8_encode($readed_line);
                        }
                    }
                    $data[$this->fileHeaders[$i]] = $readed_line;
                }
                $csvData[] = $data;
            }
        }
        if(count($csvData) > 0){
            return $csvData;
        }else if($this->allowEmptyFile){
            return [];
        }
        else{
            dd('No hay datos asi que muero');
        }
    }
    
    /**
     * Formatea la información generada por la función readFile()
     * tomando la estructura de la base de datos, intenta converitir la información 
     * al tipo de dato asignado, si falla quita el registro y lo guarda para reportes
     * 
     * Nota: Tiene mas tipos de validaciones internas definidias en el constructor 
     *
     * @param  array $data
     *
     * @return array
     */
    public function formatData($data){
        $problems = [];
        foreach($data as $pos => &$row){
            foreach($row as $key => &$field){
                switch($this->columnTypes[$key]) {
                    case "string":
                        if(in_array($key, $this->canNotBeEmpty) && empty($field)){
                            $this->dataError[] = 'El campo de la columna _' . $key. '_ no puede venir vacio. Fila: ' . strval($pos + 1);
                            unset($data[$pos]);
                        }
                        break;
                    case "integer":
                        if(is_numeric($field)){
                            $field = intval($field);
                        }else{
                            if($this->setToNull_ifCantConvert){
                                $field = null;
                            }else{
                                $this->dataError[] = 'Este campo en la columna _' . $key. '_ es de tipo INTEGER y el valor (' . $field .') no se puede convertir correctamente. Fila: ' . strval($pos + 1);
                                unset($data[$pos]);
                            }
                        }
                        break;
                    case "date":
                        try {
                            if(empty($field)){
                                $field = null;
                            }else{
                                $field = preg_replace('"/"', '-', $field, -1, $count);
                                $date = new Carbon($field);
                                $field = $date->format('Y-m-d');
                            }
                        } catch (\Throwable $th) {
                            if($this->setToNull_ifCantConvert){
                                $field = null;
                            }else{
                                $this->dataError[] = 'Este campo en la columna _' . 
                                    $key. '_ es de tipo DATE y el valor (' . 
                                    $field . ') no se puede convertir correctamente. Fila: ' .
                                    strval($pos + 1);
                                unset($data[$pos]);
                            }
                        }
                        break;
                    default:
                        break;
                }
                if($this->workOnlyWithSameSource && is_null($this->firstDetectedSource)){
                    $this->firstDetectedSource = $row['fuente'];
                }
                if(!in_array($row['fuente'], $this->detectedSources)) {
                    $this->detectedSources[] = $row['fuente'];
                }

                if(in_array($key, $this->unsetIfEmpty) && empty($field)){
                    unset($row[$key]);
                }else if(in_array($key, $this->fieldsToEncryp)){
                    $this->userOriginalPassword[$row[$this->pivotKey]] = trim($field);
                    $field = Hash::make(trim($field));
                }
            }
        }
        return $data;
    }

    /**
     * Crea o trae la información de la Empresa guardandolo en un arreglo con llaves
     * donde la llave es el nombre de la Empresa y el valor el ID
     *
     * @param array $data
     *
     * @return void
     */
    public function loadEnterprise($data){
        $enterprises = [];
        \DB::beginTransaction();
        foreach($data as $line){
            if(trim($line['empresa']) !== ""){
                try{
                    $enterprise = Enterprise::firstOrCreate([
                        'name' => trim($line['empresa']),
                        'enterprise_code' => $this->issetOrNull($line,'idempresa'),
                    ]);
                    $id = $enterprise->id;
                } catch (\Throwable $th) {
                    $id = null;
                }
                $enterprises[(trim($line['empresa']))] = $id;
            }
        }
        if($this->commitData) {
            \DB::commit();
        }
        return $enterprises;
    }

    /**
     * Crea o trae la información de la Region guardandolo en un arreglo con llaves
     * donde la llave es el nombre de la Region y el valor el ID
     *
     * @param array $data
     *
     * @return void
     */
    public function loadRegions($data){
        $regions = [];
        \DB::beginTransaction();
        foreach($data as $line){
            if(trim($line['region']) !== ""){
                try{
                    $region = Region::firstOrCreate([
                        'name' => trim($line['region']),
                    ]);
                    $id = $region->id;
                } catch (\Throwable $th) {
                    dd($th->getMessage());
                    $id = null;
                }
                $regions[(trim($line['region']))] = $id;
            }
        }
        if($this->commitData) {
            \DB::commit();
        }
        return $regions;
    }

    /**
     * Crea o trae la información de las Direcciones guardandolo en un arreglo con llaves
     * donde la llave es el nombre de la Direccion y el valor es un arreglo donde el ID
     * de la empresa viene en el campo ['_id'];
     *
     * @param array $data
     *
     * @return array
     */
    public function loadDirections($data){
        $directions = [];
        \DB::beginTransaction();
        foreach($data as $line){
            try {
                $direction = Direction::firstOrCreate([
                    'name' => $line['direccion'],
                ]);
                $directions[$line['direccion']] = ['_id' => $direction->id];
            } catch (\Throwable $th) {
                dd($th->getMessage());
            }
        }
        if($this->commitData) {
            \DB::commit();
        }
        return $directions;
    }

    /**
     * Crea o trae la información de los Departamentos guardandolo en un arreglo con llaves
     * donde la llave es el nombre del Departamento y el valor es un arreglo donde el ID
     * de la empresa viene en el campo ['_id'];
     *
     * @param  array $data
     * @param  array $hierarchy //Esta información debe de ser 
     * generada por la funcion loadDirections();
     *
     * @return array 
     */
    public function loadDepartments($data, $hierarchy){
        \DB::beginTransaction();
        foreach($data as $line){
            try{
                $department = Department::firstOrCreate([
                    'name' => $line['departamento'],
                    'direction_id' => $hierarchy[$line['direccion']]['_id'],
                ]);
                $hierarchy[$line['direccion']][$line['departamento']] = ['_id' => $department->id];
            } catch (\Throwable $th) {
                dd($th->getMessage());
            }
        }
        if($this->commitData) {
            \DB::commit();
        }
        return $hierarchy;
    }

    /**
     * Crea o trae la información de las Areas guardandolo en un arreglo con llaves
     * donde la llave es el nombre del Area y el valor es un arreglo donde el ID
     * de la empresa viene en el campo ['_id'];
     *
     * @param  array $data
     * @param  array $hierarchy //Esta información debe de ser 
     * generada por la funcion loadDepartments();
     *
     * @return array
     */
    public function loadAreas($data, $hierarchy){
        \DB::beginTransaction();
        foreach($data as $line){
            try{
                $area = Area::firstOrCreate([
                    'name' => $line['area'],
                    'department_id' => $hierarchy[$line['direccion']][$line['departamento']]['_id'],
                ]);
                $hierarchy[$line['direccion']][$line['departamento']][$line['area']] = ['_id' => $area->id];
            } catch (\Throwable $th) {
                dd($th->getMessage());
            }
        }
        if($this->commitData) {
            \DB::commit();
        }
        return $hierarchy;
    }

    /**
     * Crea o trae la información de los Puestos guardandolo en un arreglo con llaves
     * donde la llave es el nombre del Puesto y el valor es el ID
     *
     * @param  array $data
     * @param  array $hierarchy //Esta información debe de ser 
     * generada por la funcion loadAreas();
     *
     * @return array
     */
    public function loadJobPositions($data, $hierarchy){
        \DB::beginTransaction();
        foreach($data as $line){
            try {
                $jobPosition = JobPosition::firstOrCreate([
                    'name' => $line['puesto'],
                    'area_id' => $hierarchy[$line['direccion']][$line['departamento']][$line['area']]['_id'],
                ]);
                $hierarchy[$line['direccion']][$line['departamento']][$line['area']][$line['puesto']] = $jobPosition->id;
            } catch (\Throwable $th) {
                dd($th->getMessage(), $line['puesto'], $hierarchy[$line['direccion']][$line['departamento']][$line['area']]['_id'], 'puestos');
            }
        }
        if($this->commitData) {
            \DB::commit();
        }
        return $hierarchy;
    }
    
    /**
     * Fill the array given with the data set in the constructor var "hierarchyFields"
     * when the array does not have this information.
     * 
     * @param  Array $data
     *
     * @return void
     */
    public function fillHierarchyInUsers($data){
        foreach($data as &$line){
            foreach($this->hierarchyFields as $key => $field){
                if(isset($line[$key]) && empty($line[$key])){
                    $line[$key] = $field;
                }elseif(!isset($line[$key])){
                    $line[$key] = $field;
                
                }
            }
        }
        return $data;
    }

    /**
     * Redirige el trabajo a la funcion correspondiente dependiendo si el cron
     * maneja 1 o los 2 modelos que manejamos
     * 
     * NOTA IMPORTANTE: La parte de loadSigleModel() no esta bien probada ya que no se usa con frecuencia
     *
     * @param  array $data
     * @param  array $jobs
     * @param  array $enterprise
     *
     * @return void
     */
    public function loadInformationToDatabase($data, $jobs, $enterprise, $regions){
        if($this->usesEmployeeModel){
            $this->loadWithBothModels($data, $jobs, $enterprise, $regions);
        }else{
            $this->loadSigleModel($data, $jobs);
        }
    }

    private function compareDataChanges($row) {
    	try{
            $oldEmployee = Employee::withTrashed()->where($this->pivotKey, $row[$this->pivotKey])->first()->toArray();
	    	$newEmployee = $row;
	    	$changes = [];
	    	foreach ($oldEmployee as $key => $value) {
				if(!in_array($key, $this->exceptFields) && isset($newEmployee[$key]) && $value != $newEmployee[$key]){
					$changes[$key]['before'] = $value;
					$changes[$key]['after'] = $newEmployee[$key];
				}
	    	}
			// dd($changes);
            if(empty($changes)){
                return null;
            }
	    	return $changes;
	    }catch(\Exception $exception){
	    	return null;
	    }
    }

    /**
     * Aqui es donde hacemos todas las inserciones y actualizaciones a la base de datos
     * utilizando los dos modelos
     * 
     * Nota: Tiene multiples configuraciones, revisar a fondo.
     *
     * @param  array $data
     * @param  array $hierachy
     * @param  array $enterprise
     *
     * @return void
     */
    public function loadWithBothModels($data, $hierachy, $enterprise, $regions){
        $personalUpdated = [];
        $personalCreated = [];
        foreach ($data as $key => $user) {
            $personal = Employee::withTrashed()
            ->with([
                'user' => function($q){
                    $q->withTrashed()->when($this->workWithVacations, function($q2){
                        $q2->with('vacationBalances');
                    });
                }
            ])
            ->where($this->pivotKey, $user[$this->pivotKey])
            ->first();

            $this->lastReadedRow = $user;

            if($personal){
                if(!is_null($this->firstDetectedSource)){
                    if($user['fuente'] != $this->firstDetectedSource){
                        $this->update_errors[] = $this->formatMessageWithPersonnelData($user, 'FUENTE');
                        continue;
                    }
                }
                if($this->activatedIfNeeded($personal)){
                    $beforePersonal = $personal->replicate();
                    if(!$this->verfyUniqueFields($user, $personal)){
                        $this->update_errors[] = $this->formatMessageWithPersonnelData($user, 'DUPLICATE_FIELDS');
                        continue;
                    }
                    $personal = $this->changePersonalData($personal, $user);
                    $personal->job_position_id = $this->getJobInHierarchy($user, $hierachy);
                    $personal->enterprise_id = $this->issetOrNull($enterprise, $user['empresa']);
                    $personal->region_id = $this->issetOrNull($regions, $user['region']);
    
                    $hasChanged = $personal->isDirty();

                    if(!is_null($auxx = $this->compareDataChanges($user))) {
                        $this->changes[$user[$this->pivotKey]] = $auxx;
                    }

                    unset($personal->grado);

                    \DB::beginTransaction();
                    try {
                        $personal->save();
                    } catch (\Throwable $th) {
                        \DB::rollback();
                        $this->update_errors[] = $this->formatMessageWithPersonnelData($user, 'EXCEPTION', $th->getMessage());
                        continue;
                    }
                    try{
                        if($personal->user){
                            $personal->user()->update([
                                'first_name' => $personal->nombre,
                                'last_name' => $personal->paterno . ' ' . $personal->materno,
                                'email' => $personal->{$this->personalFieldToUser['email']},
                                //'password' => $personal->password,
                            ]);
                            $laravel_user = $personal->user;
                        }else{
                            $_user = User::create([
                                'employee_id' => $personal->id,
                                'first_name' => $personal->nombre,
                                'last_name' => $personal->paterno . ' ' . $personal->materno,
                                'email' => $personal->{$this->personalFieldToUser['email']},
                                'password' => $personal->password,
                                'role' => 'employee',
                                'active' => 1,
                            ]);
                            $this->created_users[] = $this->formatMessageWithPersonnelData($_user);
                            $this->newUsers[] = $_user;
                            $laravel_user = $_user;
                        }

                        if($this->usesMoodleIntegration) {
                            $this->manageMoodleAccess($laravel_user);
                        }
                    } catch (\Throwable $th) {
                        \DB::rollback();
                        $this->update_errors[] = $this->formatMessageWithPersonnelData($user, 'EXCEPTION', $th->getMessage());
                        continue;
                    }
                    if(!$this->saveChangesToLog($personal, $beforePersonal)){
                        //Aqui truena si no pudimos guardar el LOG
                        // continue;
                    }
                    if($this->workWithVacations) {
                        $this->manageVacations($personal, $user);
                    }
                    if($this->commitData) {
                        \DB::commit();
                    }
                    if($hasChanged){
                        $this->updated_users[] = $this->formatMessageWithPersonnelData($user);
                    }
                    $personalUpdated[] = $personal->id;
                };
            }else{
                if(!is_null($this->firstDetectedSource)){
                    if($user['fuente'] != $this->firstDetectedSource){
                        $this->create_errors[] = $this->formatMessageWithPersonnelData($user, 'FUENTE');
                        continue;
                    }
                }
                if(!$this->verfyUniqueFields($user)){
                    $this->create_errors[] = $this->formatMessageWithPersonnelData($user, 'DUPLICATE_FIELDS');
                    continue;
                }
                \DB::beginTransaction();
                $user['job_position_id'] = $this->getJobInHierarchy($user, $hierachy);
                $user['enterprise_id'] = $this->issetOrNull($enterprise, $user['empresa']);
                $user['region_id'] = $this->issetOrNull($regions, $user['region']);
                unset($user['puesto']);
                unset($user['empresa']);
                unset($user['region']);
                $user['password'] = $this->generateUserPassword($user);
                try {
                    $personal = Employee::create($user);
                } catch (\Throwable $th) {
                    DB::rollback();
                    $this->create_errors[] = $this->formatMessageWithPersonnelData($user, 'EXCEPTION', $th->getMessage());
                    continue;                    
                }
                try {
                    $_user = User::create([
                        'employee_id' => $personal->id,
                        'first_name' => $personal->nombre,
                        'last_name' => $personal->paterno . ' ' . $personal->materno,
                        'email' => $personal->{$this->personalFieldToUser['email']},
                        'password' => $personal->password,
                        'role' => 'employee',
                        'active' => 1,
                    ]);

                    if($this->usesMoodleIntegration) {
                        $this->manageMoodleAccess($_user);
                    }
                } catch (\Throwable $th) {
                    $this->create_errors[] = $this->formatMessageWithPersonnelData($user, 'EXCEPTION', $th->getMessage());
                    \DB::rollback();
                    continue;
                }
                if($this->workWithVacations) {
                    $this->manageVacations($personal, $user);
                }
                if($this->commitData) {
                    \DB::commit();
                }
                $personalCreated[] = $personal->id;
                $this->created_users[] = $this->formatMessageWithPersonnelData($user);
                $this->newUsers[] = $_user;
            }
        }
        $personalDeleted = 0;
        $personalNotToDelete = array_merge($personalUpdated, $personalCreated);

        if(count($personalNotToDelete) > 0){
            $queryPersonals = Employee::whereNotIn('id', $personalNotToDelete)->when($this->workOnlyWithSameSource, function($q) {
                $q->where('fuente', $this->firstDetectedSource);
            })->when(!$this->workOnlyWithSameSource, function($q) {
                $q->whereIn('fuente', $this->detectedSources);
            });
            $personalDeleted = $queryPersonals->count();
            $personals = $queryPersonals->get();
            foreach ($personals as $personal) {
                try {
                    \DB::beginTransaction();
                    $user = $personal->toArray();
                    if($personal->user) {
                        if($this->usesMoodleIntegration) {
                            $mdlUser = UserMdl::where('icq', $personal->user->id)->first();
                            if($mdlUser) {
                                $mdlUser->update([
                                    'suspended' => 1,
                                ]);
                            }
                        }
                        $personal->user->delete();
                    }
                    $personal->delete();
                    $this->deleted_users[] = $this->formatMessageWithPersonnelData($user);
                    if($this->commitData) {
                        \DB::commit();
                    }
                } catch (\Throwable $th) {
                    \DB::rollback();
                    $this->delete_errors[] = $this->formatMessageWithPersonnelData($user, 'EXCEPTION', $th->getMessage());
                }    
            }   
        }
    }

    public function manageMoodleAccess($user) {
        $mdlUser = UserMdl::updateOrCreate([
            'icq' => $user->id,
        ], [
            'username' => $user->email,
            'password' => ($user->password?$user->password:'-'),
            'firstname' => ($user->first_name?$user->first_name:'-'),
            'lastname' => ($user->last_name?$user->last_name:'-'),
            'email' => $user->email,
            'skype' => ($user->employee->nacimiento?$user->employee->nacimiento:Carbon::now()),
            'yahoo' => ($user->employee->curp?$user->employee->curp:'-'),
            'aim' => ($user->employee->sexo?$user->employee->sexo:'-'),
            'confirmed' => 1,
            'mnethostid' => 1,
            'suspended' => $user->trashed()?1:0,
            'lang' => 'es_mx'
        ]);
    }

    /**
     * Aqui es donde hacemos todas las inserciones y actualizaciones a la base de datos
     * utilizando un unico modelo
     * 
     * ADVERTENCIA: NO SE HA TERMINADO, FALLARA SEGURAMENTE
     *
     * @param  mixed $data
     *
     * @return void
     */
    public function loadSigleModel($data){
        $personalUpdated = [];
        $personalCreated = [];
        foreach ($data as $key => $user) {
            $personal = User::withTrashed()
            ->where($this->pivotKey, $user[$this->pivotKey])
            ->first();
            
            if($personal){
                if(!is_null($this->firstDetectedSource)){
                    if($user['fuente'] != $this->firstDetectedSource){
                        $this->update_errors[] = 'Se quiere asignar la fuente('.$user['fuente'].') que no corresponde a la primera(' . $this->firstDetectedSource . ') con el ' . strtoupper($this->pivotKey) . ': ' . $user[$this->pivotKey];
                        continue;
                    }
                }
                if($this->activatedIfNeeded($personal)){
                    $beforePersonal = $personal->replicate();
                    if(!$this->verfyUniqueFields($user, $personal)){
                        $this->update_errors[] = 'Ya existe un usuario con ese correo _' . $user['correoempresa'] . '_ con el ' . strtoupper($this->pivotKey) . ': ' . $user[$this->pivotKey];
                        continue;
                    }
                    $personal = $this->changePersonalData($personal, $user);
                    \DB::beginTransaction();
                    try {
                        $personal->save();
                    } catch (\Throwable $th) {
                        \DB::rollback();
                        $this->update_errors[] = 'No se pudo actualizar el USUARIO con el campo('.$user[$this->pivotKey].')  |  ' . $th->getMessage();
                        continue;
                    } 
                    if(!$this->saveChangesToLog($personal, $beforePersonal)){
                        //Aqui truena si no pudimos guardar el LOG
                        // continue;
                    }
                    if($this->commitData) {
                        \DB::commit();
                    }

                    $personalUpdated[] = $personal->id;
                };
            }else{
                if(!is_null($this->firstDetectedSource)){
                    if($user['fuente'] != $this->firstDetectedSource){
                        $this->create_errors[] = 'Se quiere asignar la fuente('.$user['fuente'].') que no corresponde a la primera(' . $this->firstDetectedSource . ') con el ' . strtoupper($this->pivotKey) . ': ' . $user[$this->pivotKey];
                        continue;
                    }
                }
                if(!$this->verfyUniqueFields($user)){
                    $this->create_errors[] = 'Ya existe un usuario con ese correo _' . $user['correoempresa'] . '_ con el ' . strtoupper($this->pivotKey) . ': ' . $user[$this->pivotKey];
                    continue;
                }
                \DB::beginTransaction();
                $user['password'] = $this->generateUserPassword($user);
                try {
                    $personal = User::create($user);    
                } catch (\Throwable $th) {
                    DB::rollback();
                    $this->create_errors[] = 'No se pudo crear el EMPLEADO con el campo('.$user[$this->pivotKey].')  |  ' . $th->getMessage();
                    continue;                    
                }
                if($this->commitData) {
                    \DB::commit();
                }
                $personalCreated[] = $personal->id;
                $this->newUsers[] = $personal;
            }
        }
        $personalDeleted = 0;
        $personalNotToDelete = array_merge($personalUpdated, $personalCreated);
        if(count($personalNotToDelete) > 0){
            try {
                $queryPersonals = User::whereNotIn('id', $personalNotToDelete);
                $personalDeleted = $queryPersonals->count();
                $queryPersonals->delete();
            } catch (\Throwable $th) {

            }            
        }
        dd($this->dataError, $this->update_errors, $this->create_errors, count($personalUpdated), $personalDeleted);
    }

    /**
     * Intenta reactivar un usuario borrado logicamente si es necesario
     *
     * @param  Illuminate\Database\Eloquent\Model $personal
     * @param  bool $first
     *
     * @return bool
     */
    public function activatedIfNeeded($personal, $first = true){
        $val = false;
        if($personal){
            if(!is_null($personal->deleted_at)){
                try {
                    $personal->restore();
                    $this->restored_users[] = $this->formatMessageWithPersonnelData($this->lastReadedRow);
                    $val = true;
                } catch (\Throwable $th) {
                    $this->restore_errors[] = $this->formatMessageWithPersonnelData($this->lastReadedRow, 'EXCEPTION', $th->getMessage());
                    return false;
                }
            }
            $val = true;
        }
        if($this->usesEmployeeModel && $first){
            return $val && $this->activatedIfNeeded($personal->user, false);
        }
        return $val;
    }

    /**
     * Revisa si la primer fila en el archivo cuadra con las cabeceras definidas
     * en la var $this->fileHeaders, si estas no cuadran falla
     *
     * @param  mixed $data
     *
     * @return bool
     */
    public function check_if_firstrow_match_headers($data){
        $fileHeaders = [];
        $readFileHeaders = [];
        /*
         * Convertimos a MAYUSCULAS las cabeceras para comparar el orden y la cantidad
         */
        foreach($this->fileHeaders as $header){
            $fileHeaders[] = strtoupper($header);
        }
        foreach($data as $header){
            $readFileHeaders[] = strtoupper($header);
        }
        for ($i = 0; $i < count($fileHeaders); $i++) { 
            if($fileHeaders[$i] !== $readFileHeaders[$i]){
                dd($fileHeaders[$i], $readFileHeaders[$i]);
                return false;
            }
        }
        return true;
    }

    /**
     * Comprueba si el delimitador proporcionado es correcto comparando el tamaño
     * del arreglo obtenido contra el total de cabeceras definidas en @var $this->fileHeaders
     *
     * @param  array $data
     *
     * @return bool
     */
    public function check_if_delimiter_is_correct($data){
        if(count($data) == count($this->fileHeaders)){
            return true;
        }
        return false;
    }

    /**
     * Carga los tipos de columnas a partir de la estructura de la base de datos
     * formando un arreglo donde la llave es el nombre y el valor es el tipo de dato
     *
     * @return void
     */
    public function loadColumnTypes(){
        $array = [];
        if($this->usesEmployeeModel){
            $tableName = with(new Employee)->getTable();
        }else{
            $tableName = with(new User)->getTable();
        }
        foreach($this->fileHeaders as $header){ 
            if(Schema::hasColumn($tableName, $header)){
                $type = \DB::getSchemaBuilder()->getColumnType($tableName, $header);
                $array[$header] = $type;
            }else{
                $array[$header] = null;
            }
        }
        $this->columnTypes = $array;
    }

    /**
     * Actualiza la información del modelo con la información proporcionada
     * ignorado los elementos listados en @var $this->except_model_fields
     *
     * @param  Illuminate\Database\Eloquent\Model $personal
     * @param  array $data
     *
     * @return void
     */
    public function changePersonalData($personal, $data){
        foreach ($data as $key => $value) {
            if(!in_array($key, $this->except_model_fields))
                $personal->$key = $value;
        }
        return $personal;
    }

    public function manageVacations($personal, $user){
        if($this->workWithVacations && $this->vacationBenefit && $personal->user){
            if(trim($personal->vacaciones) !== "" && $personal->vacaciones >= 0){
                $balance = $personal->user->vacationBalances()->whereNotNull('until')->orderBy('year', 'DESC')->first();
                if($this->substractToCalculateAmount && $balance){
                    $balance->update([
                        'amount' => $balance->pending - $personal->vacaciones,
                    ]);
                }else{
                    if($balance){
                        try {
                            $balance->update([
                                'benefit_id' => $this->vacationBenefit->id,
                                'year' => date('Y') + 1,
                                'pending' => $personal->vacaciones,
                                'until' => null,
                            ]); 
                        } catch (\Throwable $th) {
                            $this->balance_errors[] = $this->formatMessageWithPersonnelData($user, 'BALANCE_UPDATE', $th->getMessage());
                        }
                    }else{
                        try {
                            
                            $personal->user->vacationBalances()->create([
                                'benefit_id' => $this->vacationBenefit->id,
                                'year' => date('Y') + 1,
                                'amount' => 0,
                                'pending' => $personal->vacaciones,
                                'until' => null,
                            ]);
                        } catch (\Throwable $th) {
                            $this->balance_errors[] = $this->formatMessageWithPersonnelData($user, 'BALANCE_CREATE', $th->getMessage());
                        }
                    }
                }
            }
        }
    }

    /**
     * Scope a query to only include 
     *
     * @param  
     * @return 
     */

    /**
     * Hace consultas orWhere por cada elemento en 
     * @var $this->uniqueUserSimulatedFields para la tabla de users
     * @var $this->uniquePersonalSimulatedFields para la tabla de empleado
     * para simular la unicidad de datos en la base de datos
     * 
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  array $user
     * @param  string $type
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function whereClausesForUnique($query, $user, $type = 'user'){
        $whereClauses = [];
        if($type == "user"){
            foreach($this->uniqueUserSimulatedFields as $key => $field){
                $query->orWhere($field, $user[$key]);
            }
        }else{
            foreach($this->uniquePersonalSimulatedFields as $field){
                $query->orWhere($field, $user[$field]);
            }
        }
        return $query;
    }

    /**
     * Aqui estructuramos las consultas necesarias para verificar 
     * si no hay complictos con campos unicos en la base de datos
     *
     * @param  array $userData
     * @param  array $personalData
     *
     * @return bool
     */
    public function verfyUniqueFields($userData, $personalData = null){
        $unique = false;
        if($this->usesEmployeeModel){
            $personal = Employee::when($personalData, function($q) use($personalData){
                $q->where('id', '!=', $personalData->id);
            })
            ->where(function($q) use($userData){
                $this->whereClausesForUnique($q, $userData, 'employee');
            })
            ->first();
            
            $personalAux = null;
            if($personalData){
                $personalAux = Employee::with('user')->find($personalData->id);
            }

            $user = null;
            $user = User::when($personalAux && $personalAux->user, function($q) use ($personalAux){
                $q->where('id', '!=', $personalAux->user->id);
            })
            ->where(function($q) use ($userData){
                $this->whereClausesForUnique($q, $userData);
            })
            ->first();

            $unique = !$personal && !$user;
        }else{
            $user = User::when(!is_null($personalData), function($q) use($personalData){
                $q->where('id', '!=', $personalData->id);
            })
            ->where(function($q) use ($userData){
                $this->whereClausesForUnique($q, $userData);
            })
            ->count();
            $unique = ($user > 0)?false:true;
        }
        return $unique;
    }

    /**
     * Apartir de la estructura generara en las funciones de Hierarchy
     * regresaremos el id del puesto apartir de la información generada del archivo
     *
     * @param  array $line
     * @param  array $hierarchy
     *
     * @return integer //ID
     */
    public function getJobInHierarchy($line, $hierarchy){
        try {
            return $hierarchy[$line['direccion']][$line['departamento']][$line['area']][$line['puesto']];
        } catch (\Throwable $th) {
            dd($th->getMessage());
        }
    }

    public function generateUserPassword($user) {
        $pre_password = null;
        if(!isset($user['password']) || empty($user['password'])) {
            $this->userOriginalPassword[$user[$this->pivotKey]] = $this->defaultPassword;
            return Hash::make($this->defaultPassword);
        } else {
            return $user['password'];
        }
    }

    /**
     * Enviamos los correos de bien a los usuarios nuevos
     *
     * @return void
     */
    public function sendWelcomeEmail(){
        if($this->sendMailToNewUsers){
            foreach($this->newUsers as $user){
                try {
                    $userOriginalPassword = $this->userOriginalPassword[$user->employee->{$this->pivotKey}];
                    Mail::raw('Bienvenido a la plataforma tu Usuario es: ' . 
                        $user->email . 
                        ' y tu Contraseña es: ' . $userOriginalPassword , function ($message) use ($user) {
                        $message->from('soporte@cron.com', 'Cron Soporte');
                        $message->to($user->email, $user->FullName);
                        $message->subject('Bienvenido');
                    });
                } catch (\Throwable $th) {

                }
            }
        }
    }

    /**
     * Enviamos el correo de reporte
     *
     * @return void
     */
    public function sendReportEmail(){
        $data = $this->getReportData();
        try {
            Mail::send('emails.cron.status_report', ['data' => $data], function ($message) {
                $message->from('soporte@cron.com', 'Cron Soporte');
                $message->to('soporte@hallmg.com', 'Soporte Hallmg');
                $message->subject('Resumen de importación de Usuarios | Galber');
            });
        } catch (\Throwable $th) {

        }
    }

    public function getReportData() {
        return [
            'data_error' => $this->dataError,
            'created_users' => $this->created_users,
            'create_error' => $this->create_errors,
            'updated_users' => $this->updated_users,
            'update_errors' => $this->update_errors,
            'deleted_users' => $this->deleted_users,
            'delete_errors' => $this->delete_errors,
            'balance_errors' => $this->balance_errors,
            'restored_users' => $this->restored_users,
            'restore_errors' => $this->restore_errors
        ];
    }

    /**
     * Guardamos los cambios detectados en la base de datos
     *
     * @param  Illuminate\Database\Eloquent\Model $personal
     * @param  array $user
     *
     * @return void
     */
    public function saveChangesToLog($personal, $user){
        if($this->keepTrackOfChanges){
            $changes = $personal->getDirty();
            if(count($changes) > 0){
                foreach ($this->fieldsToIgnoreTrack as $key) {
                    if(isset($changes[$key])){
                        unset($changes[$key]);
                    }
                }
                $logChanges = [];
                foreach ($changes as $key => $value) {
                    if($personal->$key !== $user->$key){
                        $logChanges[] = [
                            'old-'.$key => $user->$key,
                            'new-'.$key => $personal->$key,
                        ];
                    }
                }
                if(count($logChanges) <= 0){
                    return true;
                }
                try {
                    if($this->usesEmployeeModel){
                        LogChanges::create([
                            'personal_id' => $personal->id,
                            'changes' => json_encode($logChanges),
                        ]);
                    }else{
                        LogChanges::create([
                            'user_id' => $personal->id,
                            'changes' => json_encode($logChanges),
                        ]);
                    }
                    return true;
                } catch (\Throwable $th) {
                    return false;
                }
            }
        }
        return true;
    }


    /**
     * Almacenamos el layout en la carpeta designada
     *
     * @param  mixed $request
     *
     * @return void
     */
    public function storeLayout(){
        
    }

    /**
     * Verifica si existe el index y regresa el valor
     * Si no existe regresa Null
     *
     * @param  array $value
     * @param  mixed $index
     *
     * @return mixed
     */
    public function issetOrNull($value, $index){
        return isset($value[$index])?$value[$index]:null;
    }

    /**
     * Formateamos la información del empleado para informar sobre quien se trabajo
     *
     * @param  array $data
     * @param  string $error
     *
     * @return void
     */
    public function formatMessageWithPersonnelData($data, $error = "", $specific = ""){
        $msg = "";
        if(!empty($error)){
            $msg = $this->formatErrorMessage($data, $error, $specific) . ' || ';
        }
        $aux = trim($data['nombre'] . ' ' . $data['paterno'] . ' ' . $data['materno']);
        $msg .= (!empty($aux))?'Nombre: ' . $aux . ' -- ' : '';
        $aux = trim($data['rfc']);
        $msg .= (!empty($aux))?'RFC: ' . $aux . ' -- ' : '';
        $aux = trim($data['correoempresa']);
        $msg .= (!empty($aux))?'CORREOEMPRESA: ' . $aux . ' -- ' : '';
        $aux = trim($data['correopersonal']);
        $msg .= (!empty($aux))?'CORREOPERSONAL: ' . $aux . ' -- ' : '';
        $msg = trim($msg);
        $msg = rtrim($msg, ' --');
        return $msg;
    }

    /**
     * A partir de los parametros formeatea el error solicitado
     *
     * @param  array $data
     * @param  string $error
     * @param  string $specific
     *
     * @return string
     */
    public function formatErrorMessage($data, $error, $specific = ""){
        $msg = "";
        switch($error){
            case "FUENTE":
                $msg = 'Se quiere asignar la fuente(' . $data['fuente'] . 
                ') que no corresponde a la primera(' . $this->firstDetectedSource . ')';
            break;
            case "DUPLICATE_FIELDS":
                $msg = 'Conflicto de duplicidad con alguno de los siguientes campos (';
                $msg .= implode(', ', $this->uniquePersonalSimulatedFields);
                $msg .= ')';
            break;
            case "BALANCE_CREATE":
                $msg = "Error al intentar crear la linea de balance de vacaciones" .
                " ( " . $specific . " )"; 
            break;
            case "BALANCE_UPDATE":
                $msg = "Error al intentar actualizar la linea de balance de vacaciones" .
                " ( " . $specific . " )"; 
            break;
            case "EXCEPTION":
                $msg = 'Excepción encontrada (' . $specific . ')';
        }
        return $msg;
    }

    public function loadVacationBenefit(){
        if($this->workWithVacations){
            $this->vacationBenefit = Benefits::where('name', 'Vacaciones')->first();
        }
    }
}
