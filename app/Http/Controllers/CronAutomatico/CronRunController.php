<?php

namespace App\Http\Controllers\CronAutomatico;

use App\User;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Cron\CronRunLog;
use App\Models\Vacations\Event;
use App\Models\Vacations\Balances;
use App\Models\Vacations\Benefits;
use App\Models\Vacations\ClockLog;
use App\Models\Vacations\Incident;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ClockLogManager;
use App\Http\Controllers\Vacations\BenefitManager;
use App\Http\Controllers\CronAutomatico\BalanceCron;

class CronRunController extends Controller
{
    protected $today;
    protected $yesterday;
    protected $timeToday;
    protected $cronType;
    protected $errors;
    protected $bosses;
    protected $clocklogManager;

    public function __construct(ClockLogManager $clocklogManager){
        $this->today = Carbon::now()->format('Y-m-d');
        $this->yesterday = Carbon::now()->subDay()->format('Y-m-d');
        $this->timeToday = strtotime($this->yesterday);
        $this->errors = [];
        $this->clocklogManager = $clocklogManager;
    }

    public function run($cronType, $date = null){
        if($date){
            try {
                $this->today = Carbon::createFromFormat('Y-m-d', $date);
                $this->yesterday = $this->today->copy()->subDay()->format('Y-m-d');
                $this->today = $this->today->format('Y-m-d');
            } catch (\Throwable $th) {
                dd($th->getMessage(), 'las fechas tan mal');
            }
            
        }
        switch($cronType){
            case "missing_clocklog":
                if(!$this->checkIfCronHasToRun($cronType)){
                    dd('El cron del tipo: ' . $cronType . '  para el día: ' . $this->today . ' ya se ha ejecutado con anterioridad');
                }else{
                    $this->missing_clocklog();
                }
            break;
            case "vacation_balance":
                $balanceCron = new BalanceCron();
                $balanceCron->index();
            break;
            case "clocklogs_with_time_incidents":
                if(!$this->checkIfCronHasToRun($cronType)){
                    dd('El cron del tipo: ' . $cronType . '  para el día: ' . $this->today . ' ya se ha ejecutado con anterioridad');
                }else{
                    $this->matchClocklogsWithTimeIncidents();
                }
            break;
            case "compressed_week":
                $this->today = $date?$date:$this->today;
                if(!$this->checkIfCronHasToRun($cronType)){
                    dd('El cron del tipo: ' . $cronType . '  para el día: ' . $this->today . ' ya se ha ejecutado con anterioridad, cron semanal');
                }else{
                    $this->compressed_week($date);
                }
            break;
            case "create_absents":

            break;
        }
    }

    public function checkIfCronHasToRun($cronType, $frequence = ''){
        $run = CronRunLog::where('cron_type', $cronType)
        ->when(trim($frequence), function($q) use($frequence){
            $q->where('frequence', $frequence);
        })
        ->where('date', $this->today)
        ->exists();
        return !$run;
    }

    static function arrayValuesMapKeys($array){
        $newArray = [];
        foreach($array as $value){
            $newArray[$value] = $value;
        }
        return $newArray;
    }

    public function missing_clocklog(){
        $logsIn = ClockLog::from('clock_logs as c1')
        ->where(function ($q){
            $q->where('c1.type', 'in')
            ->whereDate('c1.date', $this->yesterday)
            ->whereNotExists(function($q1){
                $q1->select(DB::raw(1))
                ->from('clock_logs as c2')
                ->whereRaw('date(c2.date) = date(c1.date)')
                ->where('c2.type', 'out');
            });
        })
        ->get()->pluck('user_id')->toArray();
        $logsInKeys = CronRunController::arrayValuesMapKeys($logsIn);

        $logsOut = ClockLog::from('clock_logs as c1')
        ->where(function ($q){
            $q->where('c1.type', 'out')
            ->whereDate('c1.date', $this->yesterday)
            ->whereNotExists(function($q1){
                $q1->select(DB::raw(1))
                ->from('clock_logs as c2')
                ->whereRaw('date(c2.date) = date(c1.date)')
                ->where('c2.type', 'in');
            });
        })
        ->get()->pluck('user_id')->toArray();
        $logsOutKeys = CronRunController::arrayValuesMapKeys($logsOut);

        $users = User::has('employee')->whereDoesntHave('clocklogs', function($q){
            $q->whereDate('date', $this->yesterday);
        })
        ->get()->pluck('id')->toArray();
        $data = $this->getUsers(array_merge($logsIn, $logsOut, $users));
        if($benefit = $this->clocklogManager->getAbsentBenefit()){
            foreach($data as $user){
                if(isset($logsInKeys[$user->id]))
                    $this->createAbsent($benefit->id, $user, 'in', strtotime($this->yesterday));
                elseif(isset($logsOutKeys[$user->id]))
                    $this->createAbsent($benefit->id, $user, 'out', strtotime($this->yesterday));
                else
                    $this->createAbsent($benefit->id, $user, 'both', strtotime($this->yesterday));

            }
            
            CronRunLog::create([
                'date' => $this->today,
                'cron_type' => 'missing_clocklog',
                'frequence' => 'daily',
            ]);
            try {
                \Mail::raw('Cron "missing_clocklog" de Ucin para el día: ' . $this->today, function ($message){
                    $message->to('victor.gonzalez@hallmg.com', 'Victor González');
                    $message->subject('Cron "missing_clocklog" de Ucin para el día: ' . $this->today);
                });
            } catch (\Throwable $th) {
                dd($this->errors, $th);
            }
        }else{
            dd('No se encontro la incidencia para Faltas');
        }
        
    }

    private function createAbsent($benefit, $user_id, $type, $date = null, $msg = 'No checho en el día'){
        $user = ($user_id instanceof User)?$user_id:User::find($user_id);
        if($user){
            $title = $type === 'in'?'Falto checada de entrada':$msg;
            $title = $type === 'out'?'Falto checada de salida':$title;

            $boss_id = $user->getAuthorizatorID();
            $date = $date ? $date : $this->timeToday;
            if($boss_id){
                try {
                    $balance = Balances::firstOrCreate([
                        'year' => date('Y') + 1,
                        'benefit_id' => $benefit,
                        'user_id' => $user->id,
                    ],[
                        'amount' => 0,
                        'pending' => 0,
                        'until' => null,
                    ]);

                    $balance->increment('pending');

                    $event = Event::firstOrCreate([
                        'title' => 'Falta generada por el sistema - ' . $title,
                        'start' => $date,
                        'end' => $date,
                        'url' => '#',
                        'js' => '',
                        'blocked' => 0,
                        'status' => 'pending',
                        'type' => 'user',
                        'region_id' => $user->employee->region_id,
                        'user_id' => $user->id,
                        'benefit_id' => $benefit,
                    ]);

                    $event->eventDays()->firstOrCreate([
                        'date' => $event->getStartDate('Y-m-d'),
                        'processed' => 0,
                        'balance_id' => $balance->id,
                    ]);

                    $incident = $event->incident()->firstOrCreate([
                        'user_id' => $boss_id,
                        'from_id' => $user->id,
                        'benefit_id' => $benefit,
                        'week' => date('w', $date),
                        'time' => $date,
                        'amount' => 1,
                        'info' => 'Falta generada por el sistema - ' . $title,
                        'status' => 'pending',
                        'value' => 0,
                        'manager' => 0,
                        'incapacity_id' => 0,
                    ]);
                    DB::commit();
                } catch (\Throwable $th) {
                    DB::rollback();
                    $this->errors[$user->id] = $th->getMessage();
                    dd($this->errors, $boss_id, $user->id);
                }
            }else{
                $this->errors[$user->id] = 'No se encontro su jefe';
            }
        }
    }

    private function createLogs($user_id, $type, $times){
        if(isset($times[$user_id])){
            try {
                $log = ClockLog::create([
                    'date' => $this->yesterday . ' ' . $times[$user_id][$type] . ':00',
                    'user_id' => $user_id,
                    'from' => 'system',
                    'compare_time' => $times[$user_id][$type],
                    'arrive_status' => 'on-time',
                    'type' => $type,
                    'status' => 'pending',
                ]);
            } catch (\Throwable $th) {
                $this->errors[$user_id][$type] = $th->getMessage();
            }
        }
    }

    private function getUsers($users){
        $data = [];
        $users = User::whereIn('id', $users)
        ->with(['employee.region.workshiftFixedDay' => function($q){
            $q->where('day', date('D', strtotime($this->yesterday)))
            ->where('labor', 1)
            ->with(['schedule' => function($q1){
                $q1->where('require_check', 1);
            }]);
        }, 'employee.boss.user'])
        ->whereHas('employee.region.workshiftFixedDay', function($q){
            $q->where('day', date('D', strtotime($this->yesterday)))
            ->where('labor', 1)
            ->whereHas('schedule', function($q1){
                $q1->where('require_check', 1);
            });
        })
        ->get();
        $incidents = Incident::query();
        $incidents = $this->clocklogManager->incidentQuery($incidents, $this->yesterday, $this->yesterday)->get()->pluck('id', 'from_id')->toArray();
        $events = Event::query();
        $events = $this->clocklogManager->eventQuery($events, $this->yesterday, $this->yesterday)->get()->pluck('id', 'region_id')->toArray();

        foreach($users as $user){
            if(!isset($incidents[$user->id]) && !isset($events[$user->employee->region_id]))
                $data[$user->id] = $user;
        }
        return $data;
    }

    private function getCompareTimes($users){
        $times = [];
        $users = User::whereIn('id', $users)
        ->with(['employee.region.workshiftFixedDay' => function($q){
            $q->where('day', date('D', strtotime($this->yesterday)))
            ->where('labor', 1)
            ->with('schedule');
        }])
        ->whereHas('employee.region.workshiftFixedDay', function($q){
            $q->where('day', date('D', strtotime($this->yesterday)))
            ->where('labor', 1)
            ->has('schedule');
        })
        ->get();

        foreach($users as $user){
            $times[$user->id] = [
                'out' => $user->employee->region->workshiftFixedDay->schedule->out,
                'in' => $user->employee->region->workshiftFixedDay->schedule->in,
            ];
        }

        return $times;
    }


    public function generateCurrentBalanceLine(BenefitManager $benefitManager){
        $today = strtotime(date('Y-m-d'));
        $vacBenefit = $benefitManager->getVacationBenefit();
        if(!$vacBenefit) return;

        $users = User::select('id', 'employee_id')
        ->with(['employee' => function($q){
            $q->select('id', 'ingreso');
        }])
        ->has('employee')
        ->whereDoesntHave('balances', function ($query) use($vacBenefit){
            $query->where('benefit_id', $vacBenefit->id);
        })
        ->get();

        foreach($users as $user){
            $date = Carbon::createFromFormat('Y-m-d', $user->employee->ingreso);
            $day = $date->format('d');
            $month = $date->format('m');
            $year = $user->getMyNextYear() - 1;
            $todayIngreso = "{$year}-{$month}-{$day}";
            $passDate = Carbon::create($year, $month, $day, 0 , 0 , 0);
            $until = $passDate->copy()->addYear();
            if($todayIngreso === date('Y-m-d')){
                $strDate = strtotime($passDate->subYear()->format('Y-m-d'));
                $year--;
            }else{
                $strDate = strtotime($passDate->format('Y-m-d'));
            }
            $balance = $user->getMyVacations($strDate);
            if($user->getYears() <= 0){
                Balances::updateOrCreate([
                    'year' => $year + 1,
                    'benefit_id' => $vacBenefit->id,
                    'user_id' => $user->id,
                    'amount' => 0,
                ],[
                    'pending' => 0,
                    'until' => null,
                ]);
            }else{
                Balances::updateOrCreate([
                    'year' => $year + 1,
                    'benefit_id' => $vacBenefit->id,
                    'user_id' => $user->id,
                    'amount' => 0,
                ],[
                    'pending' => $balance,
                    'until' => $until,
                ]);

                Balances::updateOrCreate([
                    'year' => $year + 2,
                    'benefit_id' => $vacBenefit->id,
                    'user_id' => $user->id,
                    'amount' => 0,
                ],[
                    'pending' => 0,
                    'until' => null,
                ]);
            }
        }
    }

    public function matchClocklogsWithTimeIncidents(){
        $employees = DB::select("select cl1.user_id as 'id' from clock_logs as cl1 join clock_logs as cl2 on date(cl1.date) = date(cl2.date) and cl1.user_id = cl2.user_id where cl1.type = 'in' and cl2.type = 'out' and date(cl1.date) = ?", [$this->yesterday]);
        $employees = collect($employees)->pluck('id')->toArray();
        $events = $this->clocklogManager->timeEventsQuery($this->yesterday, $this->yesterday)
        ->whereIn('user_id', $employees)
        ->get();
        $_clocklogs = DB::select("select cl1.id as 'id_in', cl1.date as 'date_in', cl1.compare_time as 'compare_time_in', cl1.user_id as 'user_id_in', cl1.type as 'type_in', cl2.id as 'id_out', cl2.date as 'date_out', cl2.compare_time as 'compare_time_out', cl2.user_id as 'user_id_out', cl2.type as 'type_out' from clock_logs as cl1 join clock_logs as cl2 on date(cl1.date) = date(cl2.date) and cl1.user_id = cl2.user_id where cl1.type = 'in' and cl2.type = 'out' and date(cl1.date) = ?", [$this->yesterday]);
        $clocklogs = [];
        foreach($_clocklogs as $clocklog){
            $clocklogs[$clocklog->user_id_in] = [
                'id_in' => $clocklog->id_in,
                'id_out' => $clocklog->id_out,
                //Si el valor es negativo entro tarde, si es positivo llego temprano
                'start_diff' => $this->clocklogManager->getTimeDifference($clocklog->date_in, $clocklog->compare_time_in),
                //Si el valor es negativo salio temprano, si es positivo salio más tarde
                'end_diff' => $this->clocklogManager->getTimeDifference($clocklog->date_out, $clocklog->compare_time_out) * -1,
                'work_diff' => $this->clocklogManager->getTimeDifference($clocklog->date_in, $clocklog->date_out),
                'labor_diff' => $this->clocklogManager->getTimeDifference($clocklog->compare_time_in, $clocklog->compare_time_out),
            ];
        }

        foreach($events as $event){
            $event_time_minutes = $event->time * 60;
            if($event_time_minutes + $clocklogs[$event->user_id]['work_diff'] >= $clocklogs[$event->user_id]['labor_diff']){
                try {
                    $logs = ClockLog::whereIn('id', [$clocklogs[$event->user_id]['id_in'], $clocklogs[$event->user_id]['id_out']])
                    ->update(['status' => 'accepted']);
                } catch (\Throwable $th) {
                    $this->errors[$event->user_id] = [
                        'event' => $event,
                        'times' => $clocklogs[$event->user_id],
                        'exception' => $th,
                    ];
                }
            }
        }
        CronRunLog::create([
            'date' => $this->today,
            'cron_type' => 'clocklogs_with_time_incidents',
            'frequence' => 'daily',
        ]);
        dd($this->errors);
    }

    public function compressed_week($date = null){
        $date = $date ? $date : $this->today;
        $dateNumber = Date('N', strtotime($date));
        if ($dateNumber !== "7"){
            throw new Exception('Solo se puede ejecutar el cron en Domingos');
        }
        $searchDay = 'Friday';
        $today = new Carbon($date);
        $friday = Carbon::createFromTimeStamp(strtotime("last $searchDay", $today->timestamp))->format('Y-m-d');
        $monday = $today->copy()->startOfWeek()->format('Y-m-d');

        $incidents = Incident::query();
        $incidents = $this->clocklogManager->incidentWeekCalendarQuery($incidents, $monday, $friday)->get();

        $benefit = $this->clocklogManager->getCompressedWeekBenefit();
        if(!$benefit) dd('falta la incidencia');
        foreach($incidents as $incident){
            $user = $incident->event->user;
            $workedMinutes = 0;
            foreach($incident->event->eventDays as $eventDay){
                try {
                    $workedMinutes += $this->clocklogManager->getWorkedMinutesInDay($user, $eventDay->date);
                } catch (\Throwable $th) {
                    $this->errors[$user->id][$eventDay->date] = $th->getMessage();
                }
            }
            $totalMinutes = $user->getWeekWorkMinutes() - 60;
            /** No trabajo los suficientes minutos que debe en la semana **/
            if( $workedMinutes < $totalMinutes ){
                $this->createAbsent($benefit->id, $user, 'compressed_week', strtotime($incident->event->eventDay->date), 'No acumulo las horas correspondientes en la semana comprimida');
            }
        }
        CronRunLog::create([
            'date' => $date,
            'cron_type' => 'compressed_week',
            'frequence' => 'daily',
        ]);
        dd($this->errors);
    }
}
