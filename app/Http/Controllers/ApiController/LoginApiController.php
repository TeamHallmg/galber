<?php

namespace App\Http\Controllers\ApiController;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Profile\Profile;

class LoginApiController extends Controller
{
    /**
     * checklogin
     *
     * @param  string $email
     * @param  string $pass
     *
     * @return json
     * 
     * Función para revisar que el usuario exista
     * y acceda a la aplicación
     */
    public function checklogin($email=null, $pass=null) {
        if(!empty($email) && !is_null($email)) {
            if(!empty($pass) && !is_null($pass)) {
                $user = User::where('email', $email)->first();
                if(!is_null($user)) {
                    $checkPass = \Hash::check($pass, $user->password);
                    if ($checkPass) {
                        $flag['existe'] = $checkPass;
                        $flag['id_user'] = $user->id;
                    } else {
                        $flag =  false; 
                    }
                } else {
                    $flag =  false;
                }
            } else {
                $flag = false;
            }
        } else {
            $flag = false;
        }
        return \Response::json(['success' => $flag], 200, ['Content-Type' => 'application/json;charset=utf8'], JSON_UNESCAPED_UNICODE);
    }

    public function img_path($id){
        $flag = false;
        $path = 'No existe la imagen';
        $pathExist = false;
        $userExists = false;
        $employeeExists = false;
        $nombre = '';
        $puesto = '';
        $sucursal = '';

        $user = User::findOrfail($id);
        
        if($user) {
            $userExists = true;
            $flag = true;

            $employee = $user->employee;
            if ($employee) {
                $employeeExists = true;
                $nombre = $employee->fullName;
                if(!is_null($employee->jobPosition))
                    $puesto = $employee->jobPosition->name;

                $sucursal = $employee->getSucursal();
            }

            $profile = Profile::where('user_id', $user->id)->first();
            if($profile) {
                $fileExists = file_exists(getcwd() . '/uploads/profile/'.$profile->image);
                if($fileExists && !is_null($profile->image)) {
                    $pathExist = true;
                    $path = asset('/uploads/profile/'.$profile->image);
                }
            }
        }

        if ($userExists && $flag && ($pathExist || !$pathExist) && $employeeExists)
            return response()->json([
                'success' => $flag,
                'path' => $path,
                'pathExists' => $pathExist,
                'nombre' => $nombre,
                'puesto' => $puesto,
                'sucursal' => $sucursal
            ], 200);
        else
            return response()->json([
                'success' => $flag,
                'path' => $path,
                'pathExists' => $pathExist,
                'nombre' => $nombre,
                'puesto' => $puesto,
                'sucursal' => $sucursal
            ], 404);
    }

}
