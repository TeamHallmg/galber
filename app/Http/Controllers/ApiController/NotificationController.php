<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use App\Models\NotificationForUser;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function mark_notification(Request $request, $id){
        $notification = NotificationForUser::findOrFail($id);
        try {
            $notification->seen = true;
            $notification->save();
        } catch (\Throwable $th) {
            return response()->json(['success' => false], 200);
        }
        return response()->json(['success' => true], 200);
    }
}
