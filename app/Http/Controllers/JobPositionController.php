<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Area;
use App\Models\JobPosition;
use App\Models\Scale\Course;
use Illuminate\Http\Request;
use App\Models\Scale\ModelData;
use App\Models\Bienes\TipoBienes;
use App\Models\Bienes\jobPositionsBienes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class JobPositionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        // ==== Admin Expedientes ====
        $this->middleware('permission:profile_admin')->only(['index', 'create', 'store', 'show', 'edit', 'update', 'destroy', 'activate', 'beneficios']);
        // ==== /Admin Expedientes ====
        // $this->path = getcwd() . '/uploads/';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = JobPosition::with('area.department.direction')->get();

        return view('jobPosition.index', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $jobs = JobPosition::get();
        $TipoBienes = TipoBienes::get();
        $areas = Area::get();
        return view('jobPosition.create', compact('jobs', 'TipoBienes', 'areas'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = ['name'=>'required', 'area_id' => 'required'];
        $messages = ['name.required' => 'El nombre del puesto es necesario', 'area_id.required' => 'Es necesario seleccionar la estructura del puesto'];

        if($request->hasFile('file')){
            $file = $request->file('file');
            $rules = array_merge($rules, ['file'=>'file|mimes:pdf,png,jpeg,jpg,docx|max:204800']);
            $messages = ['file.mimes' => 'Extensión invalida'];
        }

        // if($request->hasFile('benefits')){
        //     $benefits = $request->file('benefits');
        //     $rules = array_merge($rules, ['benefits'=>'file|mimes:png,jpeg,jpg|max:204800']);
        //     $messages = ['benefits.mimes' => 'Sólo imagenes'];
        // }

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            // dd('skere');
            $data = $request->all();
            $allowedfileExtension=['pdf','png','jpeg','jpg','docx'];

            if (!empty($file)) {
                $filename = uniqid() . '-' .$file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                
                if ($check) {
                    $request->file('file')->move('puestos/', $filename);
                    $data['file'] = $filename;
                }
            }

            // $allowedfileImgExtension=['png','jpeg','jpg'];

            // if (!empty($benefits)) {
            //     $filename = uniqid() . '-' .$benefits->getClientOriginalName();
            //     $extension = $benefits->getClientOriginalExtension();
            //     $check = in_array($extension, $allowedfileImgExtension);
                
            //     if ($check) {
            //         $request->file('benefits')->move('puestos/', $filename);
            //         $data['benefits'] = $filename;
            //     }
            // }

            try {
                DB::beginTransaction();
                    if(isset($data['courses_length'])) {
                        unset($data['courses_length']);
                    }
                    JobPosition::create($data);
                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();

                return redirect()->back()->with('alert-danger', '
                ¡UPS!... NO SE ACTUALIZÓ EL PUESTO <br/>
                VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
                ' . $e->getMessage());
            }

            return redirect()->back()->with('alert-success', 'El puesto se ha ingresado correctamente');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $job = JobPosition::findOrfail($id);
        $jobs = JobPosition::orderBy('name')->get();

        $TipoBienes = TipoBienes::with([
            'jobpositionsbienes' => function($q) use ($id) {
                $q->where('id_job_positions','=', $id);
            }
        ])
        ->whereHas('jobpositionsbienes', function($q) use ($id) {
                $q->where('id_job_positions','=', $id);
        })
        ->get();

        $areas = Area::get();

        $job_courses = JobPosition::getCourses($job->id);

        return view('jobPosition.show', compact('job', 'jobs', 'TipoBienes', 'areas', 'job_courses'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = JobPosition::findOrfail($id);
        $jobs = JobPosition::orderBy('name')->get();

        $TipoBienes = TipoBienes::with([
            'jobpositionsbienes' => function($q) use ($id) {
                $q->where('id_job_positions','=', $id);
            }
        ]) 
        ->get();

        $job_courses = JobPosition::getCourses($job->id);
        // dd($job_courses);
        $job_courses_ids = JobPosition::getCourses($job->id, true);
        $courses = Course::whereNotIn('id', $job_courses_ids)->get();
        $areas = Area::get();

        return view('jobPosition.edit', compact('job', 'jobs', 'TipoBienes', 'courses', 'job_courses', 'areas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = ['name'=>'required', 'area_id' => 'required'];
        $messages = ['name.required' => 'El nombre del puesto es necesario', 'area_id.required' => 'Es necesario seleccionar la estructura del puesto'];
        $data = $request->except('_method', '_token');
        
        if($request->hasFile('file')){
            $file = $request->file('file');
            $rules = array_merge($rules, ['file'=>'file|mimes:pdf,png,jpeg,jpg,docx|max:204800']);
            $messages = ['file.mimes' => 'Extensión invalida'];
        }

        // if($request->hasFile('benefits')){
        //     $benefits = $request->file('benefits');
        //     $rules = array_merge($rules, ['benefits'=>'file|mimes:png,jpeg,jpg|max:204800']);
        //     $messages = ['benefits.mimes' => 'Sólo imagenes'];
        // }

        $validator = Validator::make($request->all(), $rules, $messages);
        $dataJobPosition = $request->except('TipoBiene','_method', '_token');
        if(isset($dataJobPosition['courses_length'])) {
            unset($dataJobPosition['courses_length']);
        }

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            
            $allowedfileExtension=['pdf','png','jpeg','jpg','docx'];

            if (!empty($file)) {
                $filename = uniqid() . '-' .$file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                
                if ($check) {
                    $request->file('file')->move('puestos/', $filename);
                    $data['file'] = $filename;
                }
            }

            // $allowedfileImgExtension=['pdf','png','jpeg','jpg','docx'];

            // if (!empty($benefits)) {
            //     $filename = uniqid() . '-' .$benefits->getClientOriginalName();
            //     $extension = $benefits->getClientOriginalExtension();
            //     $check = in_array($extension, $allowedfileImgExtension);
                
            //     if ($check) {
            //         $request->file('benefits')->move('puestos/', $filename);
            //         $data['benefits'] = $filename;
            //     }
            // }

            try {
                DB::beginTransaction();

                    JobPosition::where('id',$id)->update($dataJobPosition);


                    //seccion asociar bienes a un puesto
                    //seccion asociar bienes a un puesto
                    //seccion asociar bienes a un puesto
                    //seccion asociar bienes a un puesto
                    //seccion asociar bienes a un puesto
                    //seccion asociar bienes a un puesto 
                    $biens_asociado = array();
                    $bienes_asociados = jobPositionsBienes::where('id_job_positions', $id)->where('estatus',1)->get();
                    foreach ($bienes_asociados as $key => $bienes_asociado) {
                            $biens_asociado[] = $bienes_asociado->id_tipo_bien;
                    }
                    $bienes_asociados_count = $bienes_asociados->count();

                    $bienes_asociados_array = $bienes_asociados->all();

                    //si no recibimos la variable TipoBiene borramos todos sus bienes
                    if(!isset($data['TipoBiene'])){
                        $this->borrar_todos_bien($id);
                    } else {
                        $tipo_bienes_recibidos = $data['TipoBiene'];
                        $bienes_recibidos_count = count( $tipo_bienes_recibidos );

                        //si el puesto no tiene bienes asociados
                        if($bienes_asociados_count == 0){

                            foreach ($tipo_bienes_recibidos as $bienes_recibidos) {

                                if( $this->exite_bien($bienes_recibidos, $id) ){

                                        $this->actualizar_bien($bienes_recibidos, $id, 1);

                                }else{

                                        $this->crear_bien($bienes_recibidos, $id);
                                }
                                   

                            }
                        //si lo recibido es igual a asociado, validamos si cambian los registros
                        } else if($bienes_recibidos_count == $bienes_asociados_count ){
                            
                            $tmp = [];
                            $tmp2 = [];
                            $añadir = [];
                            $mantener = [];
                            $eliminar = [];
                            
                            foreach ($tipo_bienes_recibidos as $bienes_recibidos) {

                                foreach ($bienes_asociados as $bien_asociado) {

                                    if ($bien_asociado->id_tipo_bien == $bienes_recibidos && !in_array($bienes_recibidos, $tmp)) {

                                        $mantener[$bien_asociado->id] = $bien_asociado->id_tipo_bien;

                                        array_push($tmp, $bienes_recibidos);

                                    }

                                }

                            }

                                
                            $añadir = array_diff($tipo_bienes_recibidos,$mantener);
                            $eliminar = array_diff($biens_asociado,$mantener);
                                
                            if(isset($añadir)){
                                foreach ($añadir as $key) {

                                    if( $this->exite_bien($key, $id) ){

                                        $this->actualizar_bien($key, $id, 1);

                                    }else{

                                        $this->crear_bien($key, $id);
                                    }

                                }
                            }

                            if(isset($eliminar)){

                                foreach ($eliminar as $key) {

                                    $this->actualizar_bien($key, $id,'0');

                                }
                            }
                            
                        //si lo recibido es menor a asociado, validamos si cambian los registros
                        } else if($bienes_recibidos_count < $bienes_asociados_count ){
                            
                            $tmp = [];
                            $tmp2 = [];
                            $añadir = [];
                            $mantener = [];
                            $eliminar = [];

                            
                            foreach ($tipo_bienes_recibidos as $bienes_recibidos) {

                                foreach ($bienes_asociados as $bien_asociado) {

                                    if ($bien_asociado->id_tipo_bien == $bienes_recibidos && !in_array($bienes_recibidos, $tmp)) {

                                        $mantener[$bien_asociado->id] = $bien_asociado->id_tipo_bien;

                                        array_push($tmp, $bienes_recibidos);

                                    }

                                }

                            }

                            $añadir = array_diff($tipo_bienes_recibidos,$mantener);
                            $eliminar = array_diff($biens_asociado,$mantener);

                            if(isset($añadir)){
                                foreach ($añadir as $key) {

                                    if( $this->exite_bien($key, $id) ){

                                        $this->actualizar_bien($key, $id, 1);

                                    }else{

                                        $this->crear_bien($key, $id);
                                    }

                                }
                            }

                            if(isset($eliminar)){

                                foreach ($eliminar as $key) {

                                    $this->actualizar_bien($key, $id,'0');

                                }
                            }
                        } 

                        //si lo recibido es mayor a asociado, validamos si cambian los registros
                        else if($bienes_recibidos_count > $bienes_asociados_count ){


                            $tmp = [];
                            $tmp2 = [];
                            $añadir = [];
                            $mantener = [];
                            $eliminar = [];

                            
                            foreach ($tipo_bienes_recibidos as $bienes_recibidos) {

                                foreach ($bienes_asociados as $bien_asociado) {

                                    if ($bien_asociado->id_tipo_bien == $bienes_recibidos && !in_array($bienes_recibidos, $tmp)) {

                                        $mantener[$bien_asociado->id] = $bien_asociado->id_tipo_bien;

                                        array_push($tmp, $bienes_recibidos);

                                    } 

                                }

                            }
                                
                            $añadir = array_diff($tipo_bienes_recibidos,$mantener);
                            $eliminar = array_diff($biens_asociado,$mantener);

                            if(isset($añadir)){
                                foreach ($añadir as $key) {

                                    if( $this->exite_bien($key, $id) ){

                                        $this->actualizar_bien($key, $id, 1);

                                    }else{

                                        $this->crear_bien($key, $id);
                                    }

                                }
                            }

                            if(isset($eliminar)){

                                foreach ($eliminar as $key) {

                                    $this->actualizar_bien($key, $id,'0');

                                }
                            }
    
                        }
                  
              }

            //fin seccion asociar bienes a un puesto
            //fin seccion asociar bienes a un puesto
            //fin seccion asociar bienes a un puesto
            //fin seccion asociar bienes a un puesto
            //fin seccion asociar bienes a un puesto
            //fin seccion asociar bienes a un puesto


                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();

                return redirect()->back()->with('alert-danger', '
                ¡UPS!... NO SE ACTUALIZÓ EL PUESTO <br/>
                VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
                ' . $e->getMessage());
            }

            return redirect()->back()->with('alert-success', 'El puesto se ha actualizado correctamente');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job = JobPosition::findOrfail($id);

        try {
            DB::beginTransaction();
                $job->delete();
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();

            return redirect()->back()->with('alert-danger', '
            ¡UPS!... NO SE ELIMINÓ EL PUESTO <br/>
            VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
            ' . $e->getMessage());
        }
        
        return redirect()->back()->with('alert-success', 'El puesto se ha eliminado correctamente');
    }

    public function beneficios() {
        $id = Auth::id();
        $user = User::findOrfail($id);
        $beneficio = null;

        if (!is_null($user->employee)) {
            $idJob = $user->employee->job_position_id;
            $jobBenefit = JobPosition::findOrfail($idJob);

            if(!is_null($jobBenefit->benefits)) {
                $beneficio = $jobBenefit->benefits;
            }
        }

        return view('jobPosition.beneficio', compact('beneficio'));
    }

    public function getJobPositionData($id = null) {
        return JobPosition::find($id);
    }

    public function getJobsFromDepartment(Request $request){
        if($request->ajax()){
            if(isset($request->department)){
                $id = $request->department;
                return JobPosition::select('id', 'name')
                ->whereHas('area.department', function($q) use($id){
                    $q->where('id', $id);
                })
                ->get();
            }
            return response()->json(['error' => 'param Department is required'], 400);
        }
        return response()->json(['error' => 'Error'], 404);
    }





    public function exite_bien($id_bien, $job_positions)
    {

        $consulta = jobPositionsBienes::where('id_tipo_bien', $id_bien)
        ->where('id_job_positions',$job_positions)
        ->exists();

        return $consulta;

    }


    public function actualizar_bien($id_bien, $job_positions, $estatus)
    {

        $consulta = jobPositionsBienes::where('id_tipo_bien', $id_bien)
        ->where('id_job_positions',$job_positions)
        ->update(['estatus' => $estatus]);
        
        return $consulta;

    }

    public function borrar_todos_bien($job_positions)
    {

        $consulta = jobPositionsBienes::where('id_job_positions',$job_positions)
        ->update(['estatus' => 0]);
        
        return $consulta;

    }

    public function actualizar_bien_by_id($id_bien, $estatus)
    {

        $consulta = jobPositionsBienes::where('id', $id_bien)
        ->update(['estatus' => $estatus]);
        
        return $consulta;

    }

    public function crear_bien($id_bien, $job_positions)
    {

        $consulta = jobPositionsBienes::create([
                    'id_tipo_bien' => $id_bien,
                    'id_job_positions' => $job_positions,
                    'estatus' => 1
                   ]);

        return $consulta;

    }
}
