<?php

namespace App\Http\Controllers;

use App\Models\DeletedReason;
use Illuminate\Http\Request;
use App\Models\Direction;
use App\Models\Department;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::select('id','name','description','direction_id')->orderBy('id', 'DESC')->get();
        foreach($departments as $department){
            $direction = Direction::select('id','name')->where('id',isset($department->direction_id)?$department->direction_id:'')->first();
            $department->direction_name = $direction->name;
        }
        //dd($departments);
        return view('departments.index', compact(['departments']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $directions = Direction::select('id','name')->orderBy('name', 'DESC')->get();
        return view('departments.create', compact(['directions']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'description' => 'max:500',
        );

        $error = Validator::make($request->all(), $rules);
        if($error->fails()){
            return redirect()->back()->withErrors(['errors' => $error->errors()->all()]);
        }

        try {
            $form_data = array(
                'direction_id' => $request->direction,
                'name'  =>  '',
                'description'  =>  $request->description,
            );

            if($request->name == -1)
                $form_data['name'] = $request->name_add;
            else {
                Department::withTrashed()->find($request->name)->restore();
                $form_data['name'] = $request->department_name;
            }

            $department = Department::createCascade($request->direction,  $form_data['name']);
            $department->description = $form_data['description'];
            $department->save();
        } catch (\Throwable $th) {
            dd($th);
        }

        return redirect()->route('departamentos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::where('id',$id)->first();
        $directions = Direction::select('id','name')->orderBy('name', 'DESC')->get();
        return view('departments.edit', compact(['department','directions']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name' => 'required',
            'description' => 'max:500',
        );

        $error = Validator::make($request->all(), $rules);
        if($error->fails()){
            return redirect()->back()->withErrors(['errors' => $error->errors()->all()]);
        }

        $form_data = array(
            'direction_id' => $request->direction,
            'name'  =>  $request->name,
            'description'  =>  $request->description,
        );

        Department::where('id',$id)->update($form_data);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $department = Department::find($id);

            foreach($department->areas as $key => $area) {
                foreach($area->jobs as $key2 => $job) {
                    foreach($job->employees as $key3 => $employee) {
                        $employee->job_position_id = null;
                        $employee->save();
                    }
                    $job->delete();
                }
                $area->delete();
            }

            $data = new DeletedReason();
            $data->user_id = Auth::user()->id;
            $data->reason = $request->del_reason;
            $department->deleted_reason()->save($data);

            $department->delete();
            DB::commit();
        } catch(\Throwable $th) {
            DB::rollback();
            dd($th->getMessage());
        }

        return redirect()->route('departamentos.index');
    }

    public function getTrashed($id){
        return Department::select('id','name','description')->where('direction_id',$id)->orderBy('id', 'ASC')->onlyTrashed()->get();
    }
}
