<?php

namespace App\Http\Controllers\NueveBox\Admin;

use DB;
use Session;
use App\User;
use App\Employee;
use Illuminate\Http\Request;
use App\Models\NineBox\Logros;
use App\Models\NineBox\Objetivos;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\NineBox\NineBoxPeriods;
use App\Models\NineBox\ResultadoDesempeno;

class PlanAccionController extends Controller{

public function index(Request $request) {
	$id_periodo_9b = $request->id_periodo_9b;
	$user_id = $request->id_user;
	// dd($user_id);

	$user = User::where('id', $user_id)->first();
	if (!is_null($user)) {
		$empleado = Employee::where('id', $user->employee_id)->first();
		if(!is_null($empleado)) {
			$empleado_id = $empleado->id;
		} else {
			$user = [];
			$request->session()->flash('message', 'Sin información a mostrar');
			return view('9box.avance-colaboradores', compact(
				'user'
			));
		}
	} else {
		$user = [];
		$request->session()->flash('message', 'Sin información a mostrar');
		return view('9box.avance-colaboradores', compact(
			'user'
		));
	}

	/* if (!is_null($user) && !is_null($user->employee)) {
		$empleado_id = $user->employee->id;
	} else {
		$user = [];
		$request->session()->flash('message', 'Sin información a mostrar');
		return view('9box.avance-colaboradores', compact(
			'user'
		));
	} */

	$period9B = NineBoxPeriods::where('id', $id_periodo_9b)->first();
	$plan_id = $period9B->planes->id;
	$plan_start_month = $period9B->planes->periodo;
	
	$objetivos = Objetivos::where('id_empleado', $empleado_id)->where('id_plan', $plan_id)->orderBy('id_objetivo_corporativo')->get();
	$objetivos_id = $objetivos->toArray();

	$logros = Logros::where('id_plan', $plan_id)->where('id_empleado', $empleado_id)->where('revisado', 1)/*->whereIn('periodo', [1,2,3,4])*/->OrderBy('objetivo_id', 'ASC')->OrderBy('periodo', 'ASC')->get();
	
	if($logros->count() > 0) {
		$lastMonth = $logros->last()->periodo;
	} else {
		$user = [];
		$request->session()->flash('message', 'Sin información a mostrar');
		return view('9box.avance-colaboradores', compact(
			'user'
		));
	}

	$valor = $veces = 0;
	foreach ($objetivos as $objetivo) {
		foreach ($logros as $logro) {
			if ($logro->objetivo_id == $objetivo->id) {
				$sum[] = $valor + $logro->logro;
				$vez[] = $veces++;
			} else {
				$sum[] = 0;
			}
		}
		/* echo "<pre>";
		print_r(array_sum($sum));
		echo "</pre>"; */
		if ($veces > 0) {
			$promedio[] = round(array_sum($sum) / $veces, 2);
		} else {
			$promedio[] = 0;
		}

		$sum = [];
		$veces = 0;

		$minimo[] = $objetivo->valor_rojo;
		$meta[] = $objetivo->objetivo;
		$peso[] = $objetivo->peso;
	}

	// dd($promedio);

	foreach ($promedio as $i => $prom) {
		if($prom < ($minimo[$i] - 10)) {
			//$ponderante[] = $prom * $peso[$i] / 100;
			$ponderante[] = ($prom / $meta[$i]) * $peso[$i];
		} else {
			if($meta[$i] > 0 && $meta[$i] != '') {
				if ($prom < $minimo[$i]){
					//$ponderante[] = $prom * $peso[$i] / 100;
					$ponderante[] = ($prom / $meta[$i]) * $peso[$i];
					/*if ($prom >= ($minimo[$i] - 5)){
						//$ponderante[] = $prom*.75*($peso[$i] / 100);
						//$ponderante[] = $prom / $peso[$i]*.75;
						$ponderante[] = ($prom / $meta[$i]) * $peso[$i] * .75;
					}
					else{
						//$ponderante[] = $prom*.5*($peso[$i] / 100);
						//$ponderante[] = $prom / $peso[$i]*.5;
						$ponderante[] = ($prom / $meta[$i]) * $peso[$i] * .5;
					}*/
				}
				else{
					//$val1 = $prom / $meta[$i];
					//$ponderante[] = $val1*$peso[$i];
					//$ponderante[] = ($prom / $meta[$i]) * $peso[$i];
					//$ponderante[] = $prom * $peso[$i] / 100;
					$ponderante[] = ($prom / $meta[$i]) * $peso[$i];
				}
			} else {
				$ponderante[] = 0;
			}
			
			// $ponderante[] = round((((int)$prom / (int)$meta[$i])*(int)$peso[$i]), 2);
		}
		$val1=0;
	}

	$sumaPeso = array_sum($peso);
	$sumaPeso = round($sumaPeso, 2);
	$sumaPonderantes = array_sum($ponderante);
	$sumaPonderantes = round($sumaPonderantes, 2);

	$resultadoDesempeno = ResultadoDesempeno::where('user_id', $user_id)->where('period_id', $period9B->period_id)->first();
	if(!is_null($resultadoDesempeno)) {
		$resultadoDesempeno = ($resultadoDesempeno->total * 15) / 100;
		$resultadoDesempeno = round($resultadoDesempeno, 2);
	} else {
		$resultadoDesempeno = 0;
	}
	$cualitativa = '15';
	
	$sumaTotal = $cualitativa + $sumaPeso;
	$sumaTotal = round($sumaTotal, 2);
	$granTotal = $sumaPonderantes + $resultadoDesempeno;
	$granTotal = round($granTotal, 2);

	

	/* REPORTE DESEMPEÑO INDIVIDUAL */
	$id_evaluado = $request->id_user;
	$id_jefe = auth()->user()->id;
	$competencias = array();
	$evaluacion_jefe = false;
	/*$user = DB::select("SELECT first_name, last_name, areas.name, job_positions.name AS puesto, jefe FROM users INNER JOIN employees ON (employees.id = users.employee_id) INNER JOIN WHERE id = ?", [$id_evaluado]);*/
	$user_evaluado = User::withTrashed()->find($id_evaluado);
	$employee = $user_evaluado->employee_wt;
	$etiquetas = DB::table('etiquetas_desempeno')->orderBy('valor', 'DESC')->get();

	$modalidad = 1;
	$modalidad_comparar = 1;
	$id_periodo = $period9B->period_id;
	if (!empty($id_periodo)){
		$periodos = DB::select('SELECT id_modalidad FROM periodos WHERE id = ?', [$id_periodo]);
		$modalidad = $periodos[0]->id_modalidad;
	} else {
		$periodos = DB::select('SELECT id, id_modalidad FROM periodos WHERE status = ? ORDER BY id DESC', ['Cerrado']);
		if (count($periodos) > 0){
			$id_periodo = $periodos[0]->id;
			$modalidad = $periodos[0]->id_modalidad;
		}
	}

	$evaluaciones_terminadas = false;
	$evaluaciones = DB::select("SELECT id FROM evaluador_evaluado WHERE id_evaluado = ? AND id_periodo = ?", [$id_evaluado, $id_periodo]);
	$terminadas = DB::select("SELECT id, id_evaluador FROM evaluador_evaluado WHERE id_evaluado = ? AND id_periodo = ? AND status = ?", [$id_evaluado, $id_periodo, 'Terminada']);

	if (count($evaluaciones) > 0 && count($evaluaciones) == count($terminadas)){
		$evaluaciones_terminadas = true;
	}

	if (!auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin') && auth()->user()->role != 'supervisor'){
		foreach ($terminadas as $key => $value){
			if ($value->id_evaluador == $id_jefe){
				$evaluacion_jefe = true;
				break;
			}
		}
	} else {
		$evaluacion_jefe = true;
	}

	$id_nivel_puesto = 0;
	$puesto_nivel_puesto = DB::table('puesto_nivel_puesto')->where('id_puesto', $user_evaluado->employee_wt->job_position_id)->select('id_nivel_puesto')->first();
	$niveles_esperados = array();
	$factores_con_nivel_de_puesto = array();
	$pesos_factores = array();

	if (!empty($puesto_nivel_puesto)){
		$id_nivel_puesto = $puesto_nivel_puesto->id_nivel_puesto;
		$niveles_esperados = DB::table('factores_niveles_puestos')->where('id_nivel_puesto', $puesto_nivel_puesto->id_nivel_puesto)->select('nivel_esperado', 'id_factor', 'peso')->get();
		foreach ($niveles_esperados as $key => $value){
			$factores_con_nivel_de_puesto[] = $value->id_factor;
			$pesos_factores[$value->id_factor] = $value->peso;
		}
	}

	$resultados = DB::table('resultados')->join('users', 'users.id', '=', 'resultados.id_evaluador')->join('employees', 'employees.id', '=', 'users.employee_id')->join('factores', 'factores.id', '=', 'resultados.id_factor')->join('evaluador_evaluado', function($join){$join->on('evaluador_evaluado.id_evaluado', '=', 'resultados.id_evaluado');$join->on('evaluador_evaluado.id_evaluador', '=', 'resultados.id_evaluador');$join->on('evaluador_evaluado.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('tipos_evaluadores', 'tipos_evaluadores.id', '=', 'evaluador_evaluado.tipo_evaluacion')->where('resultados.id_evaluado', $id_evaluado)->where('resultados.id_periodo', $id_periodo)->select(array(DB::raw('AVG(nivel_desempeno) AS nivel_desempeno'), 'id_factor', 'comentario', 'tipos_evaluadores.nombre AS nombre_evaluacion', 'resultados.id_evaluador', 'factores.nombre', 'jefe', 'idempleado'))->groupBy('id_factor', 'resultados.id_evaluador')->orderBy('id_factor')->get();

	$comentario = DB::select("SELECT comentario FROM comentarios_desempeno_jefe WHERE id_evaluado = ? AND id_periodo = ?", [$id_evaluado, $id_periodo]);
	$objetivos_entregables_desempeno = DB::select("SELECT objetivo, accion, entregable FROM objetivos_entregables_desempeno WHERE id_evaluado = ? AND id_periodo = ?", [$id_evaluado, $id_periodo]);
	$meses = array(0,'ene','feb','mar','abr','may','jun','jul','ago','sep','oct','nov','dic');
	$periodos = array();
	$resultados_a_comparar = array();

	// El usuario ya tiene registros con comentarios y objetivos para el periodo actual y ademas su jefe es el que esta viendo el reporte 
	//if (count($comentario) > 0 || count($objetivos_entregables_desempeno) > 0 && $user[0]->boss_id == auth()->user()->id){
	if (!empty(auth()->user()->employee) && $user_evaluado->employee->jefe == auth()->user()->employee->idempleado){
		// Obtiene los periodos donde el usuario tiene registrados objetivos, acciones y entregables (excepto el periodo actual)
		$periodos_con_objetivos_entregables = DB::select("SELECT id_periodo FROM objetivos_entregables_desempeno WHERE id_evaluado = ? AND id_periodo != ? GROUP BY id_periodo", [$id_evaluado, $id_periodo]);

		// Existen periodos diferentes al actual con registros de la evaluacion de desempeño
		if (count($periodos_con_objetivos_entregables) > 0){
			// Se guardan los ids de los periodos
			foreach ($periodos_con_objetivos_entregables as $key => $value){
				$periodos[] = $value->id_periodo;
			}

			$periodos = implode(',', $periodos);
			// Se obtienen los datos de los periodos
			$periodos = DB::select("SELECT id, descripcion, id_modalidad FROM periodos WHERE id IN ($periodos) ORDER BY id DESC");

			// Se seleccionó un periodo a comparar
			if (!empty($_POST['periodo_a_comparar'])){
				$periodo_a_comparar = $_POST['periodo_a_comparar'];
				foreach ($periodos as $key => $value){
					if ($value->id == $periodo_a_comparar){
						$modalidad_comparar = $value->id_modalidad;
						break;
					}
				}
			
				// Se obtienen los datos de la evaluación del periodo a comparar
				$resultados_a_comparar = DB::table('resultados')->join('users', 'users.id', '=', 'resultados.id_evaluador')->join('employees', 'employees.id', '=', 'users.employee_id')->join('factores', 'factores.id', '=', 'resultados.id_factor')->join('evaluador_evaluado', function($join){$join->on('evaluador_evaluado.id_evaluado', '=', 'resultados.id_evaluado');$join->on('evaluador_evaluado.id_evaluador', '=', 'resultados.id_evaluador');$join->on('evaluador_evaluado.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('tipos_evaluadores', 'tipos_evaluadores.id', '=', 'evaluador_evaluado.tipo_evaluacion')->where('resultados.id_evaluado', $id_evaluado)->where('resultados.id_periodo', $periodo_a_comparar)->select(array(DB::raw('AVG(nivel_desempeno) AS nivel_desempeno'), 'id_factor', 'comentario', 'tipos_evaluadores.nombre AS nombre_evaluacion', 'resultados.id_evaluador', 'factores.nombre', 'jefe', 'idempleado'))->orderBy('id_factor')->get();
			}
		}
	}

	$competencias = DB::table('periodo_factor')->join('factores', 'periodo_factor.id_factor', '=', 'factores.id')->where('id_periodo', $id_periodo)->select('nombre')->get();
	$tipos_evaluadores = DB::table('tipos_evaluadores')->pluck('descripcion_empresa', 'id')->toArray();
	$ponderaciones = DB::table('ponderaciones')->where('id_periodo', $id_periodo)->where('id_nivel_puesto', $id_nivel_puesto)->select('jefe', 'autoevaluacion', 'colaborador', 'par', 'psicometricos')->first();

	// No hay ponderaciones registradas para el usuario de este periodo
	if (empty($ponderaciones)){
		$last = DB::table('ponderaciones')->select('id_periodo')->orderBy('created_at', 'DESC')->first();
		if (!empty($last->id_periodo)){
			// Se obtienen las ultimas ponderaciones registradas
			$ponderaciones = DB::table('ponderaciones')->where('id_periodo', $last->id_periodo)->where('id_nivel_puesto', $id_nivel_puesto)->select('jefe', 'autoevaluacion', 'colaborador', 'par', 'psicometricos')->first();
		}
	}
	
	$b9_periodo = DB::table('9box_periods')->where('plan_id', $plan_id)->where('period_id', $id_periodo)->first();
		$objetivoss = Objetivos::where('id_empleado', $empleado_id)->where('id_plan', $plan_id)->groupBy('id_objetivo_corporativo')->pluck('id_objetivo_corporativo')->toArray();
		$objetivos_corporativos = DB::table('objetivos_corporativos')->whereIn('id', $objetivoss)->groupBy('id_tipo')->pluck('id_tipo')->toArray();
		$tipos_objetivos_corporativos = DB::table('tipos_objetivos_corporativos')->join('objetivos_corporativos', 'objetivos_corporativos.id_tipo', '=', 'tipos_objetivos_corporativos.id')->whereIn('tipos_objetivos_corporativos.id', $objetivos_corporativos)->pluck('tipos_objetivos_corporativos.name', 'objetivos_corporativos.id')->toArray();
		$total_logros_by_objetive = DB::table('logros')->where('id_plan', $plan_id)->where('revisado', 1)->select(DB::raw('COUNT(logro) AS total_logros'))->groupBy('objetivo_id')->orderBy('total_logros', 'DESC')->first();
		$total_logros = 0;

		if (!empty($total_logros_by_objetive)){

			$total_logros = $total_logros_by_objetive->total_logros;
		}

		$catalogo_objetivos = DB::table('catalogo_objetivos')->pluck('codigo', 'id')->toArray();

	return view('9box.planes-accion.plan', compact(
		'objetivos',
		'logros',
		'user',
		'lastMonth',
		'promedio',
		'ponderante',
		'sumaPeso',
		'sumaPonderantes',
		'resultadoDesempeno',
		'cualitativa',
		'sumaTotal',
		'granTotal',
		'id_periodo',
		'user_evaluado',
		'employee',
		'resultados',
		'meses',
		'modalidad',
		'comentario',
		'objetivos_entregables_desempeno',
		'evaluaciones_terminadas',
		'resultados_a_comparar',
		'competencias',
		'b9_periodo',
		'plan_start_month',
		'tipos_objetivos_corporativos',
		'total_logros',
		'catalogo_objetivos'
	));
}


public function listado_colaboradores(Request $request) {
	$users = User::pluck('id');
	$id_periodo_9b = $request->period_id;

	$periodos = NineBoxPeriods::get();

	foreach ($users as $user) {
		if (!is_null($user)) {
			$user = User::where('id', $user)->first();
			$empleado = Employee::where('id', $user->employee_id)->first();
			if(!is_null($empleado)) {
				$empleado_id = $empleado->id;
			} else {
				continue;
			}
		}
		
		$period9B = NineBoxPeriods::where('id', $id_periodo_9b)->first();
		$plan_id = $period9B->planes->id;
		
		$objetivos = Objetivos::where('id_empleado', $empleado_id)->where('id_plan', $plan_id)->get();
		$objetivos_id = $objetivos->toArray();

		$logros = Logros::where('id_plan', $plan_id)->where('id_empleado', $empleado_id)->where('revisado', 1)/*->whereIn('periodo', [1,2,3,4])*/->OrderBy('objetivo_id', 'ASC')->OrderBy('periodo', 'ASC')->get();
		
		if($logros->count() > 0) {
			$lastMonth = $logros->last()->periodo;
		} /*else {
			continue;
			/* $user = [];
			$request->session()->flash('message', 'Sin información a mostrar');
			return view('9box.avance-colaboradores', compact(
				'user'
			));
		} */ 

		$valor = $veces = 0;
		$promedio = [];
		$minimo = [];
		$meta = [];
		$peso = [];
		foreach ($objetivos as $objetivo) {
			foreach ($logros as $logro) {
				if ($logro->objetivo_id == $objetivo->id) {
					$sum[] = $valor + $logro->logro;
					$vez[] = $veces++;
				} else {
					$sum[] = 0;
				}
			}
			if ($veces > 0) {
				//$promedio[] = round(array_sum($sum), 2) / $veces;
				$promedio[] = round(array_sum($sum) / $veces, 2);
			} else {
				$promedio[] = 0;
			}

			$sum = [];
			$veces = 0;

			$minimo[] = $objetivo->valor_rojo;
			$meta[] = $objetivo->objetivo;
			$peso[] = $objetivo->peso;
		}
		$ponderante = [];
		if(!empty($promedio)) {
			foreach ($promedio as $i => $prom) {
				if($prom < ($minimo[$i] - 10)) {
					//$ponderante[] = 0;
					//$temp = $prom * $peso[$i] / 100;
					$temp = ($prom / $meta[$i]) * $peso[$i];
					$tope = $peso[$i] * 1.2;
								if ($tope >= $temp){
									$ponderante[] = $temp;
								}
								else{
									$ponderante[] = $tope;
								}
				}
				else {
					if($meta[$i] > 0  && $meta[$i] != '') {
						if ($prom < $minimo[$i]){
							//$temp = $prom * $peso[$i] / 100;
							$temp = ($prom / $meta[$i]) * $peso[$i];
							//if ($prom >= ($minimo[$i] - 5)){
								//$ponderante[] = $prom*.75*($peso[$i] / 100);
								//$ponderante[] = $prom / $peso[$i]*.75;
								//$temp = ($prom / $meta[$i]) * $peso[$i] * .75;
								$tope = $peso[$i] * 1.2;
								if ($tope >= $temp){
									$ponderante[] = $temp;
								}
								else{
									$ponderante[] = $tope;
								}
							//}
							//else{
								//$ponderante[] = $prom*.5*($peso[$i] / 100);
								//$ponderante[] = $prom / $peso[$i]*.5;
								/*$temp = ($prom / $meta[$i]) * $peso[$i] * .5;
								$tope = $peso[$i] * 1.2;
								if ($tope >= $temp){
									$ponderante[] = $temp;
								}
								else{
									$ponderante[] = $tope;
								}
							}*/
						}
						else{
							//$val1 = $prom / $meta[$i];
							//$temp = $val1*$peso[$i];
							//$temp = ($prom / $meta[$i]) * $peso[$i];
							//$temp = $prom * $peso[$i] / 100;
							$temp = ($prom / $meta[$i]) * $peso[$i];
							$tope = $peso[$i] * 1.2;
							if ($tope >= $temp){
								$ponderante[] = $temp;
							// $ponderante[] = round((((int)$prom / (int)$meta[$i])*(int)$peso[$i]), 2);
							}
							else{
								$ponderante[] = $tope;
							}
						}
					} 
					else {
						$ponderante[] = 0;
					}
				}
				$val1=0;
			}
			// dd($minimo, $meta, $peso);
		}
		
		$sumaPonderantes = array_sum($ponderante);
		$sumaPonderantes = round($sumaPonderantes, 2);
		$resultados[$user->id]['cuantitativa'] = $sumaPonderantes;

		$sumaPeso = array_sum($peso);
		$sumaPeso = round($sumaPeso, 2);

		/************************************ */
		$resultadoDesempeno = ResultadoDesempeno::where('user_id', $user->id)->where('period_id', $period9B->period_id)->first();
		// $resultadoDesempeno = ResultadoDesempeno::where('user_id', $user->id)->first();
		if(!is_null($resultadoDesempeno)) {
			$resultadoDesempeno = ($resultadoDesempeno->total * 15) / 100;
			$resultadoDesempeno = round($resultadoDesempeno, 2);
		} /* else {
			$resultadoDesempeno = 0;
		} */
		$cualitativa = '15';
		
		$sumaTotal = $cualitativa + $sumaPeso;
		$sumaTotal = round($sumaTotal, 2);
		$granTotal = $sumaPonderantes + $resultadoDesempeno;
		$granTotal = round($granTotal, 2);

		$resultados[$user->id]['cualitativa'] = $resultadoDesempeno;
		$resultados[$user->id]['total'] = $granTotal;


		$resultados[$user->id]['nombre'] = $user->last_name. ' ' .$user->first_name; //$user->Fullname;
		$resultados[$user->id]['direccion'] = $user->employee->direccion;
		$resultados[$user->id]['puesto'] = $user->employee->getPuestoName();
		$resultados[$user->id]['sub-area'] = $user->employee->getDepartmentName();
	}
	// dd($resultados);
	
	return view ('9box.reportes.listado-colaboradores', compact('resultados', 'periodos', 'id_periodo_9b'));

}


public function indexAccion(){

	$period_id = 0;
	$id_empleado = 0;
	$ponderacion_personal = 0;
	$desempeno = 0;
	$objetivos = array();
	$canEdit = true;
	$plan_status = 0;
	$aprobador = false;

	if (!empty($_POST['id_empleado'])){

	$id_empleado = $_POST['id_empleado'];
	}

	else{

	$id_empleado = Session::get('user_id');
	}
	
	
	$user = DB::table('users')->where('id', $id_empleado)->select('id', 'first_name', 'last_name', 'division', 'subdivision', 'boss_id')->first();
	
	
	if ($user->boss_id != auth()->user()->id){
		$canEdit = false;
	}

	$periods = DB::table('9box_periods')->where('status', 'Abierto')->get();
	$planes_accion = $comentarios = $users = array();
	$factors = $totales_factores = array();

	if (count($periods) > 0){

	if (!empty($_POST['period_id']) || Session::has('period_id')){

		if (!empty($_POST['period_id'])){

		$period_id = $_POST['period_id'];
		Session::put('period_id', $period_id);
		}

		else{

		$period_id = Session::get('period_id');
		}
	}

	else{

		$period_id = $periods[0]->id;
	}

	$period = DB::table('9box_periods')->where('id', $period_id)->select('plan_id', 'period_id')->first();
	$planes_accion = DB::table('9box_plan_accion')->where('user_id', $id_empleado)->where('period_id', $period_id)->orderBy('id', 'DESC')->get();
	$comentarios = DB::table('9box_comentarios_generales')->where('user_id', $id_empleado)->where('period_id', $period_id)->orderBy('id', 'DESC')->get();

	// Obtiene todos los usuarios
	$users = DB::table('users')->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->pluck('name', 'id');

	$ponderaciones = DB::table('ponderaciones_objetivos')->where('id_user', $id_empleado)->where('id_plan', $period->plan_id)->select('ponderacion')->first();
	$ponderacion_personal = $ponderaciones->ponderacion;
	
	$objetivos = DB::table('objetivos')->leftJoin('logros', 'logros.objetivo_id', '=', 'objetivos.id')->where('objetivos.id_plan', $period->plan_id)->where('objetivos.id_empleado', $id_empleado)->where('viejo', '<', 2)->select('objetivos.id', 'nombre', 'id_objetivo_corporativo', 'peso', 'tipo', 'objetivos.objetivo', 'objetivos.frecuencia_captura', 'objetivos.valor_rojo', 'objetivos.valor_verde', 'logro', 'logros.periodo AS periodo_logro', 'revisado')->orderBy('objetivos.id')->orderBy('periodo_logro', 'DESC')->get();
	
	// Se obtienen las ponderaciones para el periodo
	$ponderaciones = DB::table('ponderaciones')->where('id_periodo', $period->period_id)->first();

	// No hay ponderaciones registradas para el periodo
	if (empty($ponderaciones)){
		// Se obtienen las ultimas ponderaciones registradas antes del periodo
		$ponderaciones = DB::table('ponderaciones')->where('id_periodo', '<', $period->period_id)->orderBy('id_periodo', 'DESC')->first();
	}

	$resultados = DB::table('resultados')->join('users', 'users.id', '=', 'resultados.id_evaluador')->join('factores', 'factores.id', '=', 'resultados.id_factor')->join('evaluador_evaluado', function($join){$join->on('evaluador_evaluado.id_evaluado', '=', 'resultados.id_evaluado');$join->on('evaluador_evaluado.id_evaluador', '=', 'resultados.id_evaluador');$join->on('evaluador_evaluado.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('tipos_evaluadores', 'tipos_evaluadores.id', '=', 'evaluador_evaluado.tipo_evaluacion')->where('resultados.id_evaluado', $id_empleado)->where('resultados.id_periodo', $period->period_id)->select('nivel_desempeno', 'id_factor', 'tipos_evaluadores.nombre AS nombre_evaluacion', 'resultados.id_evaluador', 'factores.nombre', 'boss_id')->orderBy('id_factor')->get();
	
	//$desempeno = DB::table('resultados')->where('id_periodo', $period->period_id)->where('id_evaluado', $id_empleado)->select(array(DB::raw('SUM(nivel_desempeno) AS total'), DB::raw('COUNT(id) AS cantidad')))->groupBy('id_evaluado')->first();
		
	$user = DB::table('users')->leftJoin('puestos', 'puestos.puesto', '=', 'users.subdivision')->leftJoin('puesto_nivel_puesto', 'puesto_nivel_puesto.id_puesto', '=', 'puestos.id')->where('users.id', $id_empleado)->select('users.id', 'first_name', 'last_name', 'division', 'subdivision', 'boss_id', 'employee_number', 'id_nivel_puesto')->first();
	
	$actual_factor = 0;
	$contador = 0;
	$contador_factores = 0;
	$total = 0;
	$total_factor = 0;
	$promedio = 0;
	$autoevaluaciones = $jefazos = $colaboradores = $pares = array();
	$autoevaluacion = $jefes = $colaborador = $par = 0;
	$contador_autoevaluacion = $contador_jefe = $contador_colaborador = $contador_par = 0;

	foreach ($resultados as $key => $value){
	
		if ($actual_factor != $value->id_factor){
		
		$factors[] = $value->nombre;

		if ($actual_factor != 0){

			$contador_factores++;
			$promedio_jefe = $promedio_colaborador = $promedio_par = 0;

			if ($contador_jefe > 0){
			
			$promedio_jefe = $jefes / $contador_jefe;
			}

			if ($contador_colaborador > 0){
			
			$promedio_colaborador = $colaborador / $contador_colaborador;
			}

			if ($contador_par > 0){
			
			$promedio_par = $par / $contador_par;
			}

			$ponderar = true;

			if ($user->id_nivel_puesto == 2){

			if (($ponderaciones->jefe_director != 0 && $contador_jefe == 0) || ($ponderaciones->par_director != 0 && $contador_par == 0) || ($ponderaciones->colaborador_director != 0 && $contador_colaborador == 0) || ($ponderaciones->autoevaluacion_director != 0 && $autoevaluacion == 0)){

				$ponderar = false;
			}
			}

			else{

			if ($user->id_nivel_puesto == 4){

				if (($ponderaciones->jefe_supervisores != 0 && $contador_jefe == 0) || ($ponderaciones->par_supervisores != 0 && $contador_par == 0) || ($ponderaciones->colaborador_supervisores != 0 && $contador_colaborador == 0) || ($ponderaciones->autoevaluacion_supervisores != 0 && $autoevaluacion == 0)){

				$ponderar = false;
				}
			}

			else{

				if ($user->id_nivel_puesto == 5){

				if (($ponderaciones->jefe_catorcenal != 0 && $contador_jefe == 0) || ($ponderaciones->par_catorcenal != 0 && $contador_par == 0) || ($ponderaciones->colaborador_catorcenal != 0 && $contador_colaborador == 0) || ($ponderaciones->autoevaluacion_catorcenal != 0 && $autoevaluacion == 0)){

					$ponderar = false;
				}
				}

				else{

				if ($user->id_nivel_puesto == 3){

					if (($ponderaciones->jefe_jefaturas != 0 && $contador_jefe == 0) || ($ponderaciones->par_jefaturas != 0 && $contador_par == 0) || ($ponderaciones->colaborador_jefaturas != 0 && $contador_colaborador == 0) || ($ponderaciones->autoevaluacion_jefaturas != 0 && $autoevaluacion == 0)){

					$ponderar = false;
					}
				}
				}
			} 
			}

			if ($ponderar){

			if ($user->id_nivel_puesto == 2){

				$promedio = $promedio_jefe * $ponderaciones->jefe_director / 100 + $promedio_par * $ponderaciones->par_director / 100 + $promedio_colaborador * $ponderaciones->colaborador_director / 100 + $autoevaluacion * $ponderaciones->autoevaluacion_director / 100;
				$autoevaluaciones[] = $autoevaluacion * $ponderaciones->autoevaluacion_director / 100;
				$jefazos[] = $promedio_jefe * $ponderaciones->jefe_director / 100;
				$colaboradores[] = $promedio_colaborador * $ponderaciones->colaborador_director / 100;
				$pares[] = $promedio_par * $ponderaciones->par_director / 100;
			}

			else{

				if ($user->id_nivel_puesto == 4){

				$promedio = $promedio_jefe * $ponderaciones->jefe_supervisores / 100 + $promedio_par * $ponderaciones->par_supervisores / 100 + $promedio_colaborador * $ponderaciones->colaborador_supervisores / 100 + $autoevaluacion * $ponderaciones->autoevaluacion_supervisores / 100;
				$autoevaluaciones[] = $autoevaluacion * $ponderaciones->autoevaluacion_supervisores / 100;
				$jefazos[] = $promedio_jefe * $ponderaciones->jefe_supervisores / 100;
				$colaboradores[] = $promedio_colaborador * $ponderaciones->colaborador_supervisores / 100;
				$pares[] = $promedio_par * $ponderaciones->par_supervisores / 100;
				}

				else{

				if ($user->id_nivel_puesto == 5){

					$promedio = $promedio_jefe * $ponderaciones->jefe_catorcenal / 100 + $promedio_par * $ponderaciones->par_catorcenal / 100 + $promedio_colaborador * $ponderaciones->colaborador_catorcenal / 100 + $autoevaluacion * $ponderaciones->autoevaluacion_catorcenal / 100;
					$autoevaluaciones[] = $autoevaluacion * $ponderaciones->autoevaluacion_catorcenal / 100;
					$jefazos[] = $promedio_jefe * $ponderaciones->jefe_catorcenal / 100;
					$colaboradores[] = $promedio_colaborador * $ponderaciones->colaborador_catorcenal / 100;
					$pares[] = $promedio_par * $ponderaciones->par_catorcenal / 100;
				}

				else{

					if ($user->id_nivel_puesto == 3){

					$promedio = $promedio_jefe * $ponderaciones->jefe_jefaturas / 100 + $promedio_par * $ponderaciones->par_jefaturas / 100 + $promedio_colaborador * $ponderaciones->colaborador_jefaturas / 100 + $autoevaluacion * $ponderaciones->autoevaluacion_jefaturas / 100;
					$autoevaluaciones[] = $autoevaluacion * $ponderaciones->autoevaluacion_jefaturas / 100;
					$jefazos[] = $promedio_jefe * $ponderaciones->jefe_jefaturas / 100;
					$colaboradores[] = $promedio_colaborador * $ponderaciones->colaborador_jefaturas / 100;
					$pares[] = $promedio_par * $ponderaciones->par_jefaturas / 100;
					}

					else{

					if ($contador == 0){

						$contador = 1;
					}

					$promedio = $total_factor / $contador;
					}
				}
				}
			}     
			}

			else{

			if ($contador == 0){

				$contador = 1;
			}

			$promedio = $total_factor / $contador;
			$autoevaluaciones[] = $autoevaluacion;
			$jefazos[] = $promedio_jefe;
			$colaboradores[] = $promedio_colaborador;
			$pares[] = $promedio_par;
			}

			$total += $promedio;
			$totales_factores[] = $promedio;
		}

		$contador = $autoevaluacion = $jefes = $colaborador = $par = 0;
		$contador_autoevaluacion = $contador_jefe = $contador_colaborador = $contador_par = 0;
		$actual_factor = $value->id_factor;
		$total_factor = 0;
		}

		$total_factor += $value->nivel_desempeno;

		if ($value->id_evaluador == $user->id){

		$autoevaluacion = $value->nivel_desempeno;
		$contador_autoevaluacion++;
		}

		else{

		if ((!empty($value->nombre_evaluacion) && $value->nombre_evaluacion == 'JEFE') || ($user->boss_id == $value->id_evaluador)){

			$jefes += $value->nivel_desempeno;
			$contador_jefe++;
		}

		else{
			//Comentado Fco
			// if ((!empty($value->nombre_evaluacion) && $value->nombre_evaluacion == 'SUBORDINADO') || ($user->id == $value->boss_id)){
			if ((!empty($value->nombre_evaluacion) && $value->nombre_evaluacion == 'SUBORDINADO') || ($user->id == $jefe)){

			$colaborador += $value->nivel_desempeno;
			$contador_colaborador++;
			}

			else{

			$par += $value->nivel_desempeno;
			$contador_par++;
			} 
		}
		}

		$contador++;
	}

	if ($actual_factor != 0){

		$contador_factores++;
		$promedio_jefe = $promedio_colaborador = $promedio_par = 0;

		if ($contador_jefe > 0){
			
		$promedio_jefe = $jefes / $contador_jefe;
		}

		if ($contador_colaborador > 0){
			
		$promedio_colaborador = $colaborador / $contador_colaborador;
		}

		if ($contador_par > 0){
			
		$promedio_par = $par / $contador_par;
		}

		$ponderar = true;

		if ($user->id_nivel_puesto == 2){

		if (($ponderaciones->jefe_director != 0 && $contador_jefe == 0) || ($ponderaciones->par_director != 0 && $contador_par == 0) || ($ponderaciones->colaborador_director != 0 && $contador_colaborador == 0) || ($ponderaciones->autoevaluacion_director != 0 && $autoevaluacion == 0)){

			$ponderar = false;
		}
		}

		else{

		if ($user->id_nivel_puesto == 4){

			if (($ponderaciones->jefe_supervisores != 0 && $contador_jefe == 0) || ($ponderaciones->par_supervisores != 0 && $contador_par == 0) || ($ponderaciones->colaborador_supervisores != 0 && $contador_colaborador == 0) || ($ponderaciones->autoevaluacion_supervisores != 0 && $autoevaluacion == 0)){

			$ponderar = false;
			}
		}

		else{

			if ($user->id_nivel_puesto == 5){

			if (($ponderaciones->jefe_catorcenal != 0 && $contador_jefe == 0) || ($ponderaciones->par_catorcenal != 0 && $contador_par == 0) || ($ponderaciones->colaborador_catorcenal != 0 && $contador_colaborador == 0) || ($ponderaciones->autoevaluacion_catorcenal != 0 && $autoevaluacion == 0)){

				$ponderar = false;
			}
			}

			else{

			if ($user->id_nivel_puesto == 3){

				if (($ponderaciones->jefe_jefaturas != 0 && $contador_jefe == 0) || ($ponderaciones->par_jefaturas != 0 && $contador_par == 0) || ($ponderaciones->colaborador_jefaturas != 0 && $contador_colaborador == 0) || ($ponderaciones->autoevaluacion_jefaturas != 0 && $autoevaluacion == 0)){

				$ponderar = false;
				}
			}
			}
		} 
		}

		if ($ponderar){

		if ($user->id_nivel_puesto == 2){

			$promedio = $promedio_jefe * $ponderaciones->jefe_director / 100 + $promedio_par * $ponderaciones->par_director / 100 + $promedio_colaborador * $ponderaciones->colaborador_director / 100 + $autoevaluacion * $ponderaciones->autoevaluacion_director / 100;
			$autoevaluaciones[] = $autoevaluacion * $ponderaciones->autoevaluacion_director / 100;
			$jefazos[] = $promedio_jefe * $ponderaciones->jefe_director / 100;
			$colaboradores[] = $promedio_colaborador * $ponderaciones->colaborador_director / 100;
			$pares[] = $promedio_par * $ponderaciones->par_director / 100;
		}

		else{

			if ($user->id_nivel_puesto == 4){

			$promedio = $promedio_jefe * $ponderaciones->jefe_supervisores / 100 + $promedio_par * $ponderaciones->par_supervisores / 100 + $promedio_colaborador * $ponderaciones->colaborador_supervisores / 100 + $autoevaluacion * $ponderaciones->autoevaluacion_supervisores / 100;
			$autoevaluaciones[] = $autoevaluacion * $ponderaciones->autoevaluacion_supervisores / 100;
			$jefazos[] = $promedio_jefe * $ponderaciones->jefe_supervisores / 100;
			$colaboradores[] = $promedio_colaborador * $ponderaciones->colaborador_supervisores / 100;
			$pares[] = $promedio_par * $ponderaciones->par_supervisores / 100;
			}

			else{

			if ($user->id_nivel_puesto == 5){

				$promedio = $promedio_jefe * $ponderaciones->jefe_catorcenal / 100 + $promedio_par * $ponderaciones->par_catorcenal / 100 + $promedio_colaborador * $ponderaciones->colaborador_catorcenal / 100 + $autoevaluacion * $ponderaciones->autoevaluacion_catorcenal / 100;
				$autoevaluaciones[] = $autoevaluacion * $ponderaciones->autoevaluacion_catorcenal / 100;
				$jefazos[] = $promedio_jefe * $ponderaciones->jefe_catorcenal / 100;
				$colaboradores[] = $promedio_colaborador * $ponderaciones->colaborador_catorcenal / 100;
				$pares[] = $promedio_par * $ponderaciones->par_catorcenal / 100;
			}

			else{

				if ($user->id_nivel_puesto == 3){

				$promedio = $promedio_jefe * $ponderaciones->jefe_jefaturas / 100 + $promedio_par * $ponderaciones->par_jefaturas / 100 + $promedio_colaborador * $ponderaciones->colaborador_jefaturas / 100 + $autoevaluacion * $ponderaciones->autoevaluacion_jefaturas / 100;
				$autoevaluaciones[] = $autoevaluacion * $ponderaciones->autoevaluacion_jefaturas / 100;
				$jefazos[] = $promedio_jefe * $ponderaciones->jefe_jefaturas / 100;
				$colaboradores[] = $promedio_colaborador * $ponderaciones->colaborador_jefaturas / 100;
				$pares[] = $promedio_par * $ponderaciones->par_jefaturas / 100;
				}

				else{

				if ($contador == 0){

					$contador = 1;
				}

				$promedio = $total_factor / $contador;
				}
			}
			}
		}     
		}

		else{

		if ($contador == 0){

			$contador = 1;
		}

		$promedio = $total_factor / $contador;
		$autoevaluaciones[] = $autoevaluacion;
		$jefazos[] = $promedio_jefe;
		$colaboradores[] = $promedio_colaborador;
		$pares[] = $promedio_par;
		}
		
		$total += $promedio;
		$totales_factores[] = $promedio;
	}

	if ($contador_factores == 0){

		$contador_factores = 1;
	}

	$desempeno = $total / $contador_factores;
	$status_plan = DB::table('9box_autorizaciones_plan_accion')->where('period_id', $period_id)->where('user_id', $id_empleado)->select('status')->orderBy('id', 'DESC')->first();

	if (!empty($status_plan)){

		$plan_status = $status_plan->status;
		$aprobadores = DB::table('9box_aprobadores_planes')->where('period_id', $period_id)->where('user_id', auth()->user()->id)->first();

		if (!empty($aprobadores)){

		$aprobador = true;
		} 
	}
	}

	$objetivos_corporativos = DB::table('objetivos_corporativos')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'objetivos_corporativos.id_tipo')->select('objetivos_corporativos.id', 'id_tipo', 'letter')->orderBy('id_tipo')->orderBy('objetivos_corporativos.id')->get();
	$textos_resultados = DB::table('9box_textos_resultados')->get();
	Session::put('user_id', $id_empleado);
	Session::put('period_id', $period_id);
	return view('9box/planes-accion/index', compact('planes_accion', 'periods', 'period_id', 'users', 'comentarios', 'user', 'ponderacion_personal', 'objetivos', 'desempeno', 'canEdit', 'plan_status', 'aprobador', 'textos_resultados', 'factors', 'totales_factores', 'objetivos_corporativos'));
}

public function createAccion($period_id = 0){

	return view('9box/planes-accion/create', compact('period_id'));
}

public function storeAccion(){

	$plan_accion = array();
	$plan_accion['created_by'] = auth()->user()->id;
	$plan_accion['period_id'] = $_POST['period_id'];
	$plan_accion['user_id'] = Session::get('user_id');
	$plan_accion['accion'] = $_POST['accion'];
	$plan_accion['descripcion'] = $_POST['descripcion'];
	$plan_accion['status'] = $_POST['status'];
	DB::table('9box_plan_accion')->insert($plan_accion);
	$plan_accion = DB::table('9box_plan_accion')->where('period_id', $_POST['period_id'])->where('user_id', Session::get('user_id'))->select('id')->orderBy('id', 'DESC')->first();
	$plan_id = $plan_accion->id;

	$cambios_status = array();
	$cambios_status['id_plan'] = $plan_id;
	$cambios_status['status'] = $_POST['status'];
	$cambios_status['created_by'] = auth()->user()->id;
	DB::table('9box_cambios_status_plan_accion')->insert($cambios_status);
		
	if (!empty($_POST['avances'])){

	$bitacora = array();
	$bitacora['avances'] = $_POST['avances'];
	$bitacora['id_plan'] = $plan_id;
	$bitacora['created_by'] = auth()->user()->id;
	$bitacora['evidencia'] = '';

	if (!empty($_FILES['file'])){

		$bitacora['evidencia'] = $_FILES['file']['name'];
	}

	DB::table('9box_bitacora_plan_accion')->insert($bitacora);
	$avance = DB::table('9box_bitacora_plan_accion')->where('id_plan', $plan_id)->select('id')->orderBy('id', 'DESC')->first();
	$avance_id = $avance->id;

	if (!empty($bitacora['evidencia'])){

		if (!file_exists(getcwd() . '/documents/9box/plan_accion/' . $plan_id . '/' . $avance_id)){

		mkdir(getcwd() . '/documents/9box/plan_accion/' . $plan_id . '/' . $avance_id, 0755, true);
		}

		move_uploaded_file($_FILES['file']['tmp_name'],  getcwd() . '/documents/9box/plan_accion/' . $plan_id . '/' . $avance_id . '/' . $bitacora['evidencia']);
	}
	}

	flash('La acción fue creada');
	Session::put('period_id', $_POST['period_id']);
	return redirect()->to('9box/plan-accion');
}

public function editAccion($id){
		
	$canEdit = true;
	$id_empleado = Session::get('user_id');

	$user = DB::table('users')->where('id', $id_empleado)->select('boss_id')->first();

	if ($user->boss_id != auth()->user()->id){

	$canEdit = false;
	}

	$plan = DB::table('9box_plan_accion')->where('id', $id)->first();
	$bitacora = DB::table('9box_bitacora_plan_accion')->where('id_plan', $id)->get();

	// Obtiene todos los usuarios
	$users = DB::table('users')->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->lists('name', 'id');
	
	return view('9box/planes-accion/edit', compact('plan', 'bitacora', 'users', 'canEdit'));
}

public function updateAccion($id){
	
	$plan_accion = DB::table('9box_cambios_status_plan_accion')->where('id_plan', $id)->orderBy('id', 'DESC')->first();

	if (!empty($plan_accion)){

	if ($_POST['status'] != $plan_accion->status){

		$cambios_status = array();
		$cambios_status['id_plan'] = $id;
		$cambios_status['status'] = $_POST['status'];
		$cambios_status['created_by'] = auth()->user()->id;
		DB::table('9box_cambios_status_plan_accion')->insert($cambios_status);
	}
	}

	$plan_accion = array();
	$plan_accion['status'] = $_POST['status'];
	DB::table('9box_plan_accion')->where('id', $id)->update($plan_accion);
	
	if (!empty($_POST['avances'])){

	$bitacora = array();
	$bitacora['avances'] = $_POST['avances'];
	$bitacora['id_plan'] = $id;
	$bitacora['created_by'] = auth()->user()->id;
	$bitacora['evidencia'] = '';

	if (!empty($_FILES['file'])){

		$bitacora['evidencia'] = $_FILES['file']['name'];
	}

	DB::table('9box_bitacora_plan_accion')->insert($bitacora);
	$avance = DB::table('9box_bitacora_plan_accion')->where('id_plan', $id)->select('id')->orderBy('id', 'DESC')->first();
	$avance_id = $avance->id;

	if (!empty($bitacora['evidencia'])){

		if (!file_exists(getcwd() . '/documents/9box/plan_accion/' . $id . '/' . $avance_id)){

		mkdir(getcwd() . '/documents/9box/plan_accion/' . $id . '/' . $avance_id, 0755, true);
		}

		move_uploaded_file($_FILES['file']['tmp_name'],  getcwd() . '/documents/9box/plan_accion/' . $id . '/' . $avance_id . '/' . $bitacora['evidencia']);
	}
	}

	flash('La acción fue guardada');
	return redirect()->to('9box/plan-accion');
}

public function destroyAccion($id){
		
	DB::table('9box_bitacora_plan_accion')->where('id_plan', $id)->delete();
	DB::table('9box_cambios_status_plan_accion')->where('id_plan', $id)->delete();
	DB::table('9box_plan_accion')->where('id', $id)->delete();
	flash('La medida de acción fue borrada');
	return redirect()->to('9box/plan-accion');
}

/**
 * Agrega un comentario al plan de acción de un usuario
*
* @return void
*/
public function agregar_comentario(){
		
	$comentario = array();
	$comentario['period_id'] = $_POST['period_id'];
	$comentario['user_id'] = $_POST['id_empleado'];
	$comentario['comentario'] = $_POST['mensaje'];
	$comentario['created_by'] = auth()->user()->id;
	DB::table('9box_comentarios_generales')->insert($comentario);
	$user = DB::table('users')->where('id', auth()->user()->id)->select('first_name', 'last_name')->first();
	echo $user->first_name . ' ' . $user->last_name;
}

/**
 * Solicita la aprobación del plan de acción de un usuario
*
* @return void
*/
public function solicitar_aprobacion(){
		
	$solicitud_aprobacion = array();
	$solicitud_aprobacion['period_id'] = $_POST['period_id'];
	$solicitud_aprobacion['user_id'] = $_POST['user_id'];
	$solicitud_aprobacion['status'] = 'En Aprobacion';
	$solicitud_aprobacion['created_by'] = auth()->user()->id;
	DB::table('9box_autorizaciones_plan_accion')->insert($solicitud_aprobacion);
	$comentario = array();
	$comentario['period_id'] = $_POST['period_id'];
	$comentario['user_id'] = $_POST['user_id'];
	$comentario['comentario'] = 'Solicitud de Aprobación';
	$comentario['created_by'] = auth()->user()->id;
	DB::table('9box_comentarios_generales')->insert($comentario);
	flash('La solicitud de aprobación fue creada');
	return redirect()->to('9box/plan-accion');
}

/**
 * Aprueba el plan de acción de un usuario
*
* @return void
*/
public function aprobar_plan(){
		
	$aprobacion = array();
	$aprobacion['period_id'] = $_POST['period_id'];
	$aprobacion['user_id'] = $_POST['user_id'];
	$aprobacion['status'] = 'Aprobado';
	$aprobacion['created_by'] = auth()->user()->id;
	DB::table('9box_autorizaciones_plan_accion')->insert($aprobacion);
	$comentario = array();
	$comentario['period_id'] = $_POST['period_id'];
	$comentario['user_id'] = $_POST['user_id'];
	$comentario['comentario'] = 'Plan Aprobado';
	$comentario['created_by'] = auth()->user()->id;
	DB::table('9box_comentarios_generales')->insert($comentario);
	flash('El plan fue aprobado');
	return redirect()->to('9box/plan-accion');
}

/**
 * Autoriza o no crear un plan de acción para el usuario
*
* @return void
*/
public function autorize_plan_accion(){
		
	if (empty($_POST['checked'])){

	DB::table('9box_usuarios_reportes')->where('user_id', $_POST['user_id'])->where('period_id', $_POST['period_id'])->delete();
	}

	else{

	$usuario_reporte = array();
	$usuario_reporte['period_id'] = $_POST['period_id'];
	$usuario_reporte['user_id'] = $_POST['user_id'];
	$usuario_reporte['created_by'] = auth()->user()->id;
	DB::table('9box_usuarios_reportes')->insert($usuario_reporte);
	}
}

public function reporte_colaboradores(){
	$period_id = 0;
	$ponderacion_personal = 0;
	$desempeno = 0;
	$objetivos = array();
	$plan_status = 0;
	$users = array();

	if (auth()->user()->role == 'admin' || auth()->user()->hasRole('9Box')){
		// $users = DB::table('users')->where('active', 1)->select('id', 'subdivision', 'boss_id')->get();
		$users = User::with('employee')->get();
	} else{
		$users = DB::table('users')->where('division', auth()->user()->division)->where('active', 1)->select('id', 'subdivision', 'boss_id')->get();
	}

	$periods = DB::table('9box_periods')->where('status', 'Abierto')->orWhere('status', 'Cerrado')->get();
	$usuarios = array();
	$totales_desempeno = $totales_objetivos = array();

	if (count($periods) > 0){

	if (!empty($_POST['period_id']) || Session::has('period_id')){

		if (!empty($_POST['period_id'])){

		$period_id = $_POST['period_id'];
		Session::put('period_id', $period_id);
		}

		else{

		$period_id = Session::get('period_id');
		}
	}

	else{

		$period_id = $periods[0]->id;
	}

	$period = DB::table('9box_periods')->where('id', $period_id)->select('plan_id', 'period_id')->first();

	// Se obtienen las ponderaciones para el periodo
	$ponderaciones = DB::table('ponderaciones')->where('id_periodo', $period->period_id)->first();

	// No hay ponderaciones registradas para el periodo
	if (empty($ponderaciones)){

		// Se obtienen las ultimas ponderaciones registradas antes del periodo
		$ponderaciones = DB::table('ponderaciones')->where('id_periodo', '<', $period->period_id)->orderBy('id_periodo', 'DESC')->first();
	}

	// Obtiene todos los usuarios
	$usuarios = DB::table('users')->where('active', 1)->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->pluck('name', 'id');

	foreach ($users as $key => $usuario){

		$ponderaciones_objetivos = DB::table('ponderaciones_objetivos')->where('id_user', $usuario->id)->where('id_plan', $period->plan_id)->select('ponderacion')->first();

		if (empty($ponderaciones_objetivos)){

		continue;
		}

		$ponderacion_personal = $ponderaciones_objetivos->ponderacion;
		$objetivos = DB::table('objetivos')->leftJoin('logros', 'logros.objetivo_id', '=', 'objetivos.id')->where('objetivos.id_plan', $period->plan_id)->where('objetivos.id_empleado', $usuario->id)->where('viejo', '<', 2)->select('objetivos.id', 'peso', 'objetivos.objetivo', 'objetivos.frecuencia_captura', 'objetivos.valor_rojo', 'objetivos.valor_verde', 'logro', 'logros.periodo AS periodo_logro', 'revisado')->orderBy('objetivos.id')->orderBy('periodo_logro', 'DESC')->get();

		$resultados = DB::table('resultados')->join('users', 'users.id', '=', 'resultados.id_evaluador')->join('factores', 'factores.id', '=', 'resultados.id_factor')->join('evaluador_evaluado', function($join){$join->on('evaluador_evaluado.id_evaluado', '=', 'resultados.id_evaluado');$join->on('evaluador_evaluado.id_evaluador', '=', 'resultados.id_evaluador');$join->on('evaluador_evaluado.id_periodo', '=', 'resultados.id_periodo');})->leftJoin('tipos_evaluadores', 'tipos_evaluadores.id', '=', 'evaluador_evaluado.tipo_evaluacion')->where('resultados.id_evaluado', $usuario->id)->where('resultados.id_periodo', $period->period_id)->select('nivel_desempeno', 'id_factor', 'tipos_evaluadores.nombre AS nombre_evaluacion', 'resultados.id_evaluador', 'factores.nombre', 'boss_id')->orderBy('id_factor')->get();
		$user = DB::table('users')->leftJoin('puestos', 'puestos.puesto', '=', 'users.subdivision')->leftJoin('puesto_nivel_puesto', 'puesto_nivel_puesto.id_puesto', '=', 'puestos.id')->where('users.id', $usuario->id)->select('users.id', 'first_name', 'last_name', 'division', 'subdivision', 'boss_id', 'employee_number', 'id_nivel_puesto')->first();

		$actual_factor = 0;
		$contador = 0;
		$contador_factores = 0;
		$total = 0;
		$total_factor = 0;
		$promedio = 0;
		$factors = array();
		$autoevaluaciones = $jefazos = $colaboradores = $pares = array();
		$autoevaluacion = $jefes = $colaborador = $par = 0;
		$contador_autoevaluacion = $contador_jefe = $contador_colaborador = $contador_par = 0;

		foreach ($resultados as $key2 => $value){
	
		if ($actual_factor != $value->id_factor){
		
			$factors[] = $value->nombre;

			if ($actual_factor != 0){

			$contador_factores++;
			$promedio_jefe = $promedio_colaborador = $promedio_par = 0;

			if ($contador_jefe > 0){
			
				$promedio_jefe = $jefes / $contador_jefe;
			}

			if ($contador_colaborador > 0){
			
				$promedio_colaborador = $colaborador / $contador_colaborador;
			}

			if ($contador_par > 0){
			
				$promedio_par = $par / $contador_par;
			}

			$ponderar = true;

			if ($user->id_nivel_puesto == 2){

				if (($ponderaciones->jefe_director != 0 && $contador_jefe == 0) || ($ponderaciones->par_director != 0 && $contador_par == 0) || ($ponderaciones->colaborador_director != 0 && $contador_colaborador == 0) || ($ponderaciones->autoevaluacion_director != 0 && $autoevaluacion == 0)){

				$ponderar = false;
				}
			}

			else{

				if ($user->id_nivel_puesto == 4){

				if (($ponderaciones->jefe_supervisores != 0 && $contador_jefe == 0) || ($ponderaciones->par_supervisores != 0 && $contador_par == 0) || ($ponderaciones->colaborador_supervisores != 0 && $contador_colaborador == 0) || ($ponderaciones->autoevaluacion_supervisores != 0 && $autoevaluacion == 0)){

					$ponderar = false;
				}
				}

				else{

				if ($user->id_nivel_puesto == 5){

					if (($ponderaciones->jefe_catorcenal != 0 && $contador_jefe == 0) || ($ponderaciones->par_catorcenal != 0 && $contador_par == 0) || ($ponderaciones->colaborador_catorcenal != 0 && $contador_colaborador == 0) || ($ponderaciones->autoevaluacion_catorcenal != 0 && $autoevaluacion == 0)){

					$ponderar = false;
					}
				}

				else{

					if ($user->id_nivel_puesto == 3){

					if (($ponderaciones->jefe_jefaturas != 0 && $contador_jefe == 0) || ($ponderaciones->par_jefaturas != 0 && $contador_par == 0) || ($ponderaciones->colaborador_jefaturas != 0 && $contador_colaborador == 0) || ($ponderaciones->autoevaluacion_jefaturas != 0 && $autoevaluacion == 0)){

						$ponderar = false;
					}
					}
				}
				} 
			}

			if ($ponderar){

				if ($user->id_nivel_puesto == 2){

				$promedio = $promedio_jefe * $ponderaciones->jefe_director / 100 + $promedio_par * $ponderaciones->par_director / 100 + $promedio_colaborador * $ponderaciones->colaborador_director / 100 + $autoevaluacion * $ponderaciones->autoevaluacion_director / 100;
				$autoevaluaciones[] = $autoevaluacion * $ponderaciones->autoevaluacion_director / 100;
				$jefazos[] = $promedio_jefe * $ponderaciones->jefe_director / 100;
				$colaboradores[] = $promedio_colaborador * $ponderaciones->colaborador_director / 100;
				$pares[] = $promedio_par * $ponderaciones->par_director / 100;
				}

				else{

				if ($user->id_nivel_puesto == 4){

					$promedio = $promedio_jefe * $ponderaciones->jefe_supervisores / 100 + $promedio_par * $ponderaciones->par_supervisores / 100 + $promedio_colaborador * $ponderaciones->colaborador_supervisores / 100 + $autoevaluacion * $ponderaciones->autoevaluacion_supervisores / 100;
					$autoevaluaciones[] = $autoevaluacion * $ponderaciones->autoevaluacion_supervisores / 100;
					$jefazos[] = $promedio_jefe * $ponderaciones->jefe_supervisores / 100;
					$colaboradores[] = $promedio_colaborador * $ponderaciones->colaborador_supervisores / 100;
					$pares[] = $promedio_par * $ponderaciones->par_supervisores / 100;
				}

				else{

					if ($user->id_nivel_puesto == 5){

					$promedio = $promedio_jefe * $ponderaciones->jefe_catorcenal / 100 + $promedio_par * $ponderaciones->par_catorcenal / 100 + $promedio_colaborador * $ponderaciones->colaborador_catorcenal / 100 + $autoevaluacion * $ponderaciones->autoevaluacion_catorcenal / 100;
					$autoevaluaciones[] = $autoevaluacion * $ponderaciones->autoevaluacion_catorcenal / 100;
					$jefazos[] = $promedio_jefe * $ponderaciones->jefe_catorcenal / 100;
					$colaboradores[] = $promedio_colaborador * $ponderaciones->colaborador_catorcenal / 100;
					$pares[] = $promedio_par * $ponderaciones->par_catorcenal / 100;
					}

					else{

					if ($user->id_nivel_puesto == 3){

						$promedio = $promedio_jefe * $ponderaciones->jefe_jefaturas / 100 + $promedio_par * $ponderaciones->par_jefaturas / 100 + $promedio_colaborador * $ponderaciones->colaborador_jefaturas / 100 + $autoevaluacion * $ponderaciones->autoevaluacion_jefaturas / 100;
						$autoevaluaciones[] = $autoevaluacion * $ponderaciones->autoevaluacion_jefaturas / 100;
						$jefazos[] = $promedio_jefe * $ponderaciones->jefe_jefaturas / 100;
						$colaboradores[] = $promedio_colaborador * $ponderaciones->colaborador_jefaturas / 100;
						$pares[] = $promedio_par * $ponderaciones->par_jefaturas / 100;
					}

					else{

						if ($contador == 0){

						$contador = 1;
						}

						$promedio = $total_factor / $contador;
					}
					}
				}
				}     
			}

			else{

				if ($contador == 0){

				$contador = 1;
				}

				$promedio = $total_factor / $contador;
				$autoevaluaciones[] = $autoevaluacion;
				$jefazos[] = $promedio_jefe;
				$colaboradores[] = $promedio_colaborador;
				$pares[] = $promedio_par;
			}

			$total += $promedio;
			}

			$contador = $autoevaluacion = $jefes = $colaborador = $par = 0;
			$contador_autoevaluacion = $contador_jefe = $contador_colaborador = $contador_par = 0;
			$actual_factor = $value->id_factor;
			$total_factor = 0;
		}

		$total_factor += $value->nivel_desempeno;

		if ($value->id_evaluador == $user->id){

			$autoevaluacion = $value->nivel_desempeno;
			$contador_autoevaluacion++;
		}

		else{

			if ((!empty($value->nombre_evaluacion) && $value->nombre_evaluacion == 'JEFE') || ($user->boss_id == $value->id_evaluador)){

			$jefes += $value->nivel_desempeno;
			$contador_jefe++;
			}

			else{

			if ((!empty($value->nombre_evaluacion) && $value->nombre_evaluacion == 'SUBORDINADO') || ($user->id == $value->boss_id)){

				$colaborador += $value->nivel_desempeno;
				$contador_colaborador++;
			}

			else{

				$par += $value->nivel_desempeno;
				$contador_par++;
			}  
			}
		}

		$contador++;
		}

		if ($actual_factor != 0){

		$contador_factores++;
		$promedio_jefe = $promedio_colaborador = $promedio_par = 0;

		if ($contador_jefe > 0){
			
			$promedio_jefe = $jefes / $contador_jefe;
		}

		if ($contador_colaborador > 0){
			
			$promedio_colaborador = $colaborador / $contador_colaborador;
		}

		if ($contador_par > 0){
			
			$promedio_par = $par / $contador_par;
		}

		$ponderar = true;

		if ($user->id_nivel_puesto == 2){

			if (($ponderaciones->jefe_director != 0 && $contador_jefe == 0) || ($ponderaciones->par_director != 0 && $contador_par == 0) || ($ponderaciones->colaborador_director != 0 && $contador_colaborador == 0) || ($ponderaciones->autoevaluacion_director != 0 && $autoevaluacion == 0)){

			$ponderar = false;
			}
		}

		else{

			if ($user->id_nivel_puesto == 4){

			if (($ponderaciones->jefe_supervisores != 0 && $contador_jefe == 0) || ($ponderaciones->par_supervisores != 0 && $contador_par == 0) || ($ponderaciones->colaborador_supervisores != 0 && $contador_colaborador == 0) || ($ponderaciones->autoevaluacion_supervisores != 0 && $autoevaluacion == 0)){

				$ponderar = false;
			}
			}

			else{

			if ($user->id_nivel_puesto == 5){

				if (($ponderaciones->jefe_catorcenal != 0 && $contador_jefe == 0) || ($ponderaciones->par_catorcenal != 0 && $contador_par == 0) || ($ponderaciones->colaborador_catorcenal != 0 && $contador_colaborador == 0) || ($ponderaciones->autoevaluacion_catorcenal != 0 && $autoevaluacion == 0)){

				$ponderar = false;
				}
			}

			else{

				if ($user->id_nivel_puesto == 3){

				if (($ponderaciones->jefe_jefaturas != 0 && $contador_jefe == 0) || ($ponderaciones->par_jefaturas != 0 && $contador_par == 0) || ($ponderaciones->colaborador_jefaturas != 0 && $contador_colaborador == 0) || ($ponderaciones->autoevaluacion_jefaturas != 0 && $autoevaluacion == 0)){

					$ponderar = false;
				}
				}
			}
			} 
		}

		if ($ponderar){

			if ($user->id_nivel_puesto == 2){

			$promedio = $promedio_jefe * $ponderaciones->jefe_director / 100 + $promedio_par * $ponderaciones->par_director / 100 + $promedio_colaborador * $ponderaciones->colaborador_director / 100 + $autoevaluacion * $ponderaciones->autoevaluacion_director / 100;
			$autoevaluaciones[] = $autoevaluacion * $ponderaciones->autoevaluacion_director / 100;
			$jefazos[] = $promedio_jefe * $ponderaciones->jefe_director / 100;
			$colaboradores[] = $promedio_colaborador * $ponderaciones->colaborador_director / 100;
			$pares[] = $promedio_par * $ponderaciones->par_director / 100;
			}

			else{

			if ($user->id_nivel_puesto == 4){

				$promedio = $promedio_jefe * $ponderaciones->jefe_supervisores / 100 + $promedio_par * $ponderaciones->par_supervisores / 100 + $promedio_colaborador * $ponderaciones->colaborador_supervisores / 100 + $autoevaluacion * $ponderaciones->autoevaluacion_supervisores / 100;
				$autoevaluaciones[] = $autoevaluacion * $ponderaciones->autoevaluacion_supervisores / 100;
				$jefazos[] = $promedio_jefe * $ponderaciones->jefe_supervisores / 100;
				$colaboradores[] = $promedio_colaborador * $ponderaciones->colaborador_supervisores / 100;
				$pares[] = $promedio_par * $ponderaciones->par_supervisores / 100;
			}

			else{

				if ($user->id_nivel_puesto == 5){

				$promedio = $promedio_jefe * $ponderaciones->jefe_catorcenal / 100 + $promedio_par * $ponderaciones->par_catorcenal / 100 + $promedio_colaborador * $ponderaciones->colaborador_catorcenal / 100 + $autoevaluacion * $ponderaciones->autoevaluacion_catorcenal / 100;
				$autoevaluaciones[] = $autoevaluacion * $ponderaciones->autoevaluacion_catorcenal / 100;
				$jefazos[] = $promedio_jefe * $ponderaciones->jefe_catorcenal / 100;
				$colaboradores[] = $promedio_colaborador * $ponderaciones->colaborador_catorcenal / 100;
				$pares[] = $promedio_par * $ponderaciones->par_catorcenal / 100;
				}

				else{

				if ($user->id_nivel_puesto == 3){

					$promedio = $promedio_jefe * $ponderaciones->jefe_jefaturas / 100 + $promedio_par * $ponderaciones->par_jefaturas / 100 + $promedio_colaborador * $ponderaciones->colaborador_jefaturas / 100 + $autoevaluacion * $ponderaciones->autoevaluacion_jefaturas / 100;
					$autoevaluaciones[] = $autoevaluacion * $ponderaciones->autoevaluacion_jefaturas / 100;
					$jefazos[] = $promedio_jefe * $ponderaciones->jefe_jefaturas / 100;
					$colaboradores[] = $promedio_colaborador * $ponderaciones->colaborador_jefaturas / 100;
					$pares[] = $promedio_par * $ponderaciones->par_jefaturas / 100;
				}

				else{

					if ($contador == 0){

					$contador = 1;
					}

					$promedio = $total_factor / $contador;
				}
				}
			}
			}     
		}

		else{

			if ($contador == 0){

			$contador = 1;
			}

			$promedio = $total_factor / $contador;
			$autoevaluaciones[] = $autoevaluacion;
			$jefazos[] = $promedio_jefe;
			$colaboradores[] = $promedio_colaborador;
			$pares[] = $promedio_par;
		}
		
		$total += $promedio;
		}

		if ($contador_factores == 0){

		$contador_factores = 1;
		}

		$desempeno = $total / $contador_factores;
		$actual_objetivo = 0;
		$periodo_anual = 0;
		$periodo = 0;
		$resultado_anual = 0;
		$porcentaje = 0;
		$peso = 0;
		$total_porcentaje = 0;
		$total_peso = 0;
		$total_desempeno = 0;
		$total_objetivos = 0;

		if (!empty($desempeno)){

		$total_desempeno = $desempeno * 100 / 4;
		$desempeno = number_format($total_desempeno, 1);

		if ($desempeno > $total_desempeno){

			$total_desempeno = $desempeno;
		}

		$totales_desempeno[$usuario->id] = $total_desempeno;
		}

		for ($i = 0;$i < count($objetivos);$i++){

		if ($objetivos[$i]->id != $actual_objetivo){

			if ($actual_objetivo != 0){

			if ((!empty($resultado_anual) || $resultado_anual == 0) && $periodo == $periodo_anual){

				$porcentaje_peso = $porcentaje * $peso / 100;
				$total_peso += $porcentaje_peso; 
				$total_porcentaje += $porcentaje;
			}
			}

			$actual_objetivo = $objetivos[$i]->id;
			$resultado_anual = 0;
			$porcentaje = 0;

			if (!empty($objetivos[$i]->revisado)){

			$resultado_anual = $objetivos[$i]->logro;
			$porcentaje = 0;
			$periodo = $objetivos[$i]->periodo_logro;

			if ($objetivos[$i]->valor_verde == 'Si' || $objetivos[$i]->valor_verde == 'No'){

				if ($resultado_anual == $objetivos[$i]->valor_verde){

				$porcentaje = 100;
				}
			}

			else{

				if ($objetivos[$i]->valor_verde >= $objetivos[$i]->valor_rojo){

				if ($resultado_anual >= $objetivos[$i]->valor_verde){

					$porcentaje = 100;
				}

				else{

					$porcentaje = $resultado_anual * 100 / $objetivos[$i]->objetivo;

					if ($porcentaje > 100){

					$porcentaje = 100;
					}
				}
				}

				else{

				if ($resultado_anual <= $objetivos[$i]->valor_verde){

					$porcentaje = 100;
				}
				}
			}
			}

			$frecuencia_captura = $objetivos[$i]->frecuencia_captura;
			$periodo_anual = ($frecuencia_captura == 'Bimestral' ? 7 : ($frecuencia_captura == 'Trimestral' ? 5 : ($frecuencia_captura == 'Anual' ? 2 : 13)));
			$peso = $objetivos[$i]->peso;
		}
		}

		if ($actual_objetivo != 0){

		if ((!empty($resultado_anual) || $resultado_anual == 0) && $periodo == $periodo_anual){

			$porcentaje_peso = $porcentaje * $peso / 100;
			$total_peso += $porcentaje_peso;
			$total_porcentaje += $porcentaje;
		}
		}

		if (!empty($objetivos)){
		
		if ($total_porcentaje != 0){

			$total_objetivos = $total_peso;
			$total_peso = number_format($total_peso, 1);

			if ($total_peso > $total_objetivos){

			$total_objetivos = $total_peso;
			}
		}

		$totales_objetivos[$usuario->id] = $total_objetivos;  
		}
	}
	}
	
	if(isset($_POST['listado'])) {
	return view('9box/reportes/listado-colaboradores', compact('periods', 'period_id', 'users', 'usuarios', 'totales_desempeno', 'totales_objetivos'));  
	}
	return view('9box/reportes/reporte-colaboradores', compact('periods', 'period_id', 'users', 'usuarios', 'totales_desempeno', 'totales_objetivos'));
}
}