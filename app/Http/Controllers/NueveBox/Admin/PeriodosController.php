<?php
namespace App\Http\Controllers\NueveBox\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

use App\NueveBox\Admin\Periodos;
use DB;

class PeriodosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Muestra los periodos
     *
     */
    public function index(){
      // Obtiene todos los periodos
      $periodos = DB::select('select * from 9box_periods');

      $plans = DB::table('planes')->join('objetivos', 'objetivos.id_plan', '=', 'planes.id')->groupBy('planes.id')->pluck('planes.nombre', 'planes.id')->toArray();
      $periods = DB::table('periodos')->pluck('descripcion', 'id')->toArray();
      // dd($plans, $periods);
      return view('9box/Admin/periodos/index', compact('periodos', 'plans', 'periods'));
    }

    /**
     * Crea un nuevo periodo
     *
     */
    public function create(){

      if (!empty($_POST['name'])){

        $periodo = DB::insert('insert into 9box_periods (start_date, end_date, name, plan_id, period_id, status, created_by) values (?, ?, ?, ?, ?, ?, ?)', [$_POST['start_date'], $_POST['end_date'], $_POST['name'], $_POST['plan_id'], $_POST['period_id'], $_POST['status'], auth()->user()->id]);
        
        //if (!empty($periodo)){

          if (!empty($_POST['users']) && count($_POST['users']) > 0){

            $period = DB::table('9box_periods')->orderBy('id', 'DESC')->first();

            if (!empty($period)){

              $aprobadores_plan = array();
              $aprobador_plan = array();
              $aprobador_plan['period_id'] = $period->id;

              foreach ($_POST['users'] as $key => $value){
              
                $aprobador_plan['user_id'] = $value;
                $aprobadores_plan[] = $aprobador_plan;
              }

              DB::table('9box_aprobadores_planes')->insert($aprobadores_plan);
            }
          }

          flash('El periodo fue creado correctamente');
        //}

        return redirect('/desempeno-resultados/periodos');
      }

      // Obtiene los usuarios activos
      $users = DB::select('SELECT id AS data, CONCAT(first_name, " ", last_name) AS value FROM users WHERE active = ?', [1]);

      $planes = DB::table('planes')->join('objetivos', 'objetivos.id_plan', '=', 'planes.id')->where('estado', 2)->orWhere('estado', 3)->groupBy('planes.id')->pluck('planes.id')->toArray();
      $plans = DB::table('planes')->whereIn('id', $planes)->select('id', 'nombre AS name')->orderBy('id', 'DESC')->get();
      $periods = DB::table('periodos')->where('status', '<>', 'Preparatorio')->where('status', '<>', 'Cancelado')->select('id', 'descripcion')->orderBy('id', 'DESC')->get();
		
      return view('9box/Admin/periodos/create', compact('users', 'plans', 'periods'));
    }

    /**
     * Edita o actualiza un periodo
     *
     */
    public function edit($id = 0){

      // No existe el id para la pagina de edicion
	    if (empty($id)){

        DB::update('update 9box_periods set start_date = "' . $_POST['start_date'] . '", end_date = "' . $_POST['end_date'] . '", name = "' . $_POST['name'] . '", plan_id = ' . $_POST['plan_id'] . ', period_id = ' . $_POST['period_id'] . ', status = "' . $_POST['status'] . '", updated_at = "' . date('Y-m-d H:i:s') . '" where id = ?', [$_POST['id']]);
		    DB::delete("DELETE FROM 9box_aprobadores_planes WHERE period_id = ?", [$_POST['id']]);

        if (!empty($_POST['users'])){

          foreach ($_POST['users'] as $key => $value){
              
            DB::insert('INSERT INTO 9box_aprobadores_planes (period_id, user_id) values (?, ?)', [$_POST['id'], $value]);
          }
        }
		  
		    flash('El periodo fue guardado correctamente');
        return redirect('/desempeno-resultados/periodos');
      }

      $period = DB::table('9box_periods')->where('id', $id)->first();
      $approvers = DB::table('9box_aprobadores_planes')->where('period_id', $id)->get();
      
      // Obtiene los usuarios activos
      $users = DB::select('SELECT id AS data, CONCAT(first_name, " ", last_name) AS value FROM users WHERE active = ?', [1]);

      $planes = DB::table('planes')->join('objetivos', 'objetivos.id_plan', '=', 'planes.id')->where('estado', 2)->orWhere('estado', 3)->groupBy('planes.id')->pluck('planes.id')->toArray();
      $plans = DB::table('planes')->whereIn('id', $planes)->select('id', 'nombre AS name')->orderBy('id', 'DESC')->get();
      $periods = DB::table('periodos')->where('status', '<>', 'Preparatorio')->where('status', '<>', 'Cancelado')->select('id', 'descripcion')->orderBy('id', 'DESC')->get();
      
      return view('9box/Admin/periodos/edit', compact('period', 'users', 'plans', 'periods', 'approvers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = 0){
      
      //if (!empty($_POST['id'])){

        $periodo = DB::delete('DELETE FROM 9box_periods WHERE id = ?', [$id]);
        
        if (!empty($periodo)){

          $planes = DB::table('9box_plan_accion')->where('period_id', $id)->pluck('id')->toArray();

          if (!empty($planes)){

            foreach ($planes as $key => $value){
              
              DB::table('9box_bitacora_plan_accion')->where('id_plan', $value)->delete();
              DB::table('9box_cambios_status_plan_accion')->where('id_plan', $value)->delete();
              DB::delete('DELETE FROM 9box_plan_accion WHERE id = ?', [$value]);
            }
          }

          DB::delete('DELETE FROM 9box_comentarios_generales WHERE period_id = ?', [$id]);
          DB::delete('DELETE FROM 9box_aprobadores_planes WHERE period_id = ?', [$id]);

          flash('El periodo fue borrado correctamente');
        }

        else{

          flash('Error al borrar el periodo. Intente de nuevo');
        }

        return redirect('/desempeno-resultados/periodos');
      //}

      //$periodo = DB::select('SELECT id, name FROM 9box_periods WHERE id = ?', [$id]);
      //return view('9box/Admin/periodos/delete', compact('periodo'));
    }
}
