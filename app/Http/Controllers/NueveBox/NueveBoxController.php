<?php

namespace App\Http\Controllers\NueveBox;

use DB;
use Session;
use App\User;
use App\Employee;
use App\Http\Requests;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class NueveBoxController extends Controller{

		/*
	|--------------------------------------------------------------------------
	| 9Box Controller
	|--------------------------------------------------------------------------
	|
	| Add Documentation
	|
	*/

	/**
	 * Create a new 9Box controller instance.
	*
	* @return void
	*/
	public function __construct(){
		$this->middleware('auth');
	}

	/**
	 * Add Documentation
	*
	* @return void
	*/
	public function que_es(){
		return view('9box/que-es');
	}

	/**
	 * Muestra la pagina para ver el avance de los colaboradores
	*
	* @return void
	*/
	public function avance_colaboradores(){
		$id_user = auth()->user()->id;
		// $periods = DB::table('9box_periods')->where('status', 'Abierto')->orderBy('id', 'DESC')->get();
		if(auth()->user()->role == 'admin') {
			$periods = DB::table('9box_periods')->where('status', 'Abierto')->orWhere('status', 'Preparatorio')->orderBy('id', 'DESC')->get();	
		} else {
			$periods = DB::table('9box_periods')->where('status', 'Abierto')->orderBy('id', 'DESC')->get();
		}

		$users = $planes_accion_terminados = $planes_accion = $planes_autorizados = $planes_en_espera = array();
		$period = 0;
		$aprobador = false;
		$usuarios_reporte = array();

		// Hay periodos abiertos
		if (count($periods) > 0){
			// Obtiene el periodo abierto mas reciente
			$period = $periods[0];

			// Se requiere mostrar un periodo por default
			if (!empty($_POST['period_id']) || Session::has('period_id')){
				if (!empty($_POST['period_id'])){
					foreach ($periods as $key => $value){
						if ($_POST['period_id'] == $value->id){
							$period = $value;
						}
					}
				}else{
					$period_id = Session::get('period_id');
					$found = false;

					foreach ($periods as $key => $value){
						if ($value->id == $period_id){
							$found = true;
							$period = $value;
							break;
						}
					}

					if (!$found){
						$period = $periods[0];
						Session::forget('period_id');
					}
				}
			}

			$usuarios_reporte = DB::table('9box_usuarios_reportes')->where('period_id', $period->id)->pluck('user_id')->toArray();
			$aprobadores = DB::table('9box_aprobadores_planes')->where('period_id', $period->id)->pluck('user_id')->toArray();
			if (in_array(auth()->user()->id, $aprobadores)){
				$aprobador = true;
			}

			if (auth()->user()->role == 'admin' || auth()->user()->isAdminOrHasRolePermission('9Box')){
				// $users = DB::table('users')->join('ponderaciones_objetivos', 'ponderaciones_objetivos.id_user', '=', 'users.id')->where('active', 1)->where('email', 'not like', '%soporte%')->where('ponderaciones_objetivos.id_plan', $period->plan_id)->select('users.id', 'first_name', 'last_name', 'subdivision', 'workstation', 'employee_number')->get();
				//$users = DB::table('users')->join('ponderaciones_objetivos', 'ponderaciones_objetivos.id_user', '=', 'users.id')->where('active', 1)->where('email', 'not like', '%soporte%')->where('ponderaciones_objetivos.id_plan', $period->plan_id)->select('users.id', 'first_name', 'last_name', 'employee_id')->get();
				// $users = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->join('comentarios_desempeno_jefe', 'comentarios_desempeno_jefe.id_evaluado', '=', 'users.id')->join('logros', 'logros.id_empleado', '=', 'employees.id')->where('comentarios_desempeno_jefe.id_periodo', $period->period_id)->where('logros.id_plan', $period->plan_id)->select('users.id', 'first_name', 'last_name', 'idempleado', 'job_positions.name AS job_position', 'departments.name AS department')->groupBy('users.id')->get();
				$users = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->join('logros', 'logros.id_empleado', '=', 'employees.id')->where('logros.id_plan', $period->plan_id)->select('users.id', 'first_name', 'last_name', 'idempleado', 'job_positions.name AS job_position', 'departments.name AS department')->groupBy('users.id')->get();
				
				$planes_accion_terminados = DB::table('9box_plan_accion')->where('period_id', $period->id)->where('status', 'Terminado')->select(DB::raw('COUNT(id) AS finished_plans'), 'user_id')->groupBy('user_id')->get();
				$planes_accion = DB::table('9box_plan_accion')->where('period_id', $period->id)->select(DB::raw('COUNT(id) AS action_plans'), 'user_id')->groupBy('user_id')->get();
			}else{
				$usuarios = array();
				
				// if (substr(auth()->user()->subdivision, 0, 8) == 'DIRECTOR'){
				/*if (substr(auth()->user()->employee->direccion, 0, 8) == 'DIRECTOR'){
					$colaboradores = array(auth()->user()->id);
					$users = array();

					do{
						$users = array_merge($colaboradores, $users);
						$colaboradores = DB::table('users')->whereIn('boss_id', $colaboradores)->pluck('id')->toArray();
					}while(!empty($colaboradores));
					$usuarios = DB::table('users')->whereIn('id', $users)->pluck('id')->toArray();
				}else{
					if ($aprobador){
						$usuarios = DB::table('users')->whereIn('id', $usuarios_reporte)->pluck('id')->toArray();
					}else{
						// dd('aqui');
						// $usuarios = DB::table('users')->where('boss_id', $id_user)->whereIn('id', $usuarios_reporte)->pluck('id')->toArray();
						if(!empty($usuarios_reporte)) {
							$usuarios = User::with('employees')->whereIn('id', $usuarios_reporte)->where(function($q) use($id_user) {
								$q->where('employee.jefe', $id_user);
							})->pluck('id')->toSql();
						}
					}
				}*/

				// $users = DB::table('users')->join('ponderaciones_objetivos', 'ponderaciones_objetivos.id_user', '=', 'users.id')->whereIn('users.id', $usuarios)->where('active', 1)->where('email', 'not like', '%soporte%')->where('ponderaciones_objetivos.id_plan', $period->plan_id)->select('users.id', 'first_name', 'last_name', 'subdivision', 'workstation', 'employee_number')->get();
				//$users = DB::table('users')->join('ponderaciones_objetivos', 'ponderaciones_objetivos.id_user', '=', 'users.id')->whereIn('users.id', $usuarios)->where('active', 1)->where('email', 'not like', '%soporte%')->where('ponderaciones_objetivos.id_plan', $period->plan_id)->select('users.id', 'first_name', 'last_name', 'employee_id')->get();
				// $users = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->join('comentarios_desempeno_jefe', 'comentarios_desempeno_jefe.id_evaluado', '=', 'users.id')->join('logros', 'logros.id_empleado', '=', 'users.id')->where('comentarios_desempeno_jefe.id_periodo', $period->period_id)->where('logros.id_plan', $period->plan_id)->where('jefe', auth()->user()->employee->idempleado)->select('users.id', 'first_name', 'last_name', 'employee_id')->groupBy('users.id')->get();
				// $users = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->join('logros', 'logros.id_empleado', '=', 'users.id')->where('logros.id_plan', $period->plan_id)->where('jefe', auth()->user()->employee->idempleado)->select('users.id', 'first_name', 'last_name', 'employee_id')->groupBy('users.id')->get();
				$users = DB::table('users')
					->join('employees', 'employees.id', '=', 'users.employee_id')
					/* ->join('logros', 'logros.id_empleado', '=', 'users.id')
					->where('logros.id_plan', $period->plan_id) */
					->where('jefe', auth()->user()->employee->idempleado)
					->whereNull('employees.deleted_at')
					->select('users.id', 'first_name', 'last_name', 'employee_id')
					->groupBy('users.id')
					->get();
				
				$planes_accion_terminados = DB::table('9box_plan_accion')->where('period_id', $period->id)->where('status', 'Terminado')->whereIn('user_id', $usuarios)->select(DB::raw('COUNT(id) AS finished_plans'), 'user_id')->groupBy('user_id')->get();
				$planes_accion = DB::table('9box_plan_accion')->where('period_id', $period->id)->whereIn('user_id', $usuarios)->select(DB::raw('COUNT(id) AS action_plans'), 'user_id')->groupBy('user_id')->get();
			}

			$temp_planes_accion_terminados = $temp_planes_accion = array();

			foreach ($planes_accion as $key => $value){
				$temp_planes_accion[$value->user_id] = $value->action_plans;
			}

			foreach ($planes_accion_terminados as $key => $value){
				$temp_planes_accion_terminados[$value->user_id] = $value->finished_plans;
			}

			$planes_accion = $temp_planes_accion;
			$planes_accion_terminados = $temp_planes_accion_terminados;
			$planes_autorizados = DB::table('9box_autorizaciones_plan_accion')->where('period_id', $period->id)->where('status', 'Aprobado')->pluck('user_id')->toArray();
			$planes_en_espera = DB::table('9box_autorizaciones_plan_accion')->where('period_id', $period->id)->where('status', 'En Aprobacion')->pluck('user_id')->toArray();
		}
		
		return view('9box/avance-colaboradores', compact('users', 'period', 'periods', 'planes_accion_terminados', 'planes_accion', 'aprobador', 'planes_autorizados', 'planes_en_espera', 'usuarios_reporte'));
	}   
}