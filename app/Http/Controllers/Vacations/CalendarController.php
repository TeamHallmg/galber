<?php

namespace App\Http\Controllers\Vacations;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Vacations\Event;
use App\Models\Vacations\Incident;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Helpers\ClockLogManager;

class CalendarController extends Controller
{
    protected $benefitManager;
    protected $clocklogManager;

    public function __construct(BenefitManager $benefitManager, ClockLogManager $clocklogManager){
        $this->benefitManager = $benefitManager;
        $this->clocklogManager = $clocklogManager;
    }

    public function getCalendarEvents(Request $request, User $user = null){
        $user = $user?$user:Auth::user();
        $user_id = isset($request->user_id) && $request->user_id?$request->user_id:$user->id;
        $onlyActive = isset($request->onlyActive)?$request->onlyActive:false;
        $start = new Carbon($request->start);
        $end = new Carbon($request->end);
        $end->subDay();
        $start = strtotime($start->format('Y-m-d'));
        $end = strtotime($end->format('Y-m-d'));
        $incidents = [];
        if($user->employee && ($vacation = $this->benefitManager->getVacationBenefit())){
            $incidents = Incident::with('event')
            ->where('from_id', $user_id)
            ->whereHas('event', function($q) use($start, $end){
                $q->whereBetween('start', [$start, $end])
                ->orWhereBetween('end', [$start, $end])
                ->orWhere(function($q1) use($start, $end){
                    $q1->where('start', '>=', $start)
                    ->where('end', '<=', $end);
                })
                ->orWhere(function($q1) use($start, $end){
                    $q1->where('start', '<', $start)
                    ->where('end', '>', $end);
                });
            })
            ->when($onlyActive, function($q){
                $q->whereNotIn('status', ['rejected', 'canceled']);
            })
            ->get();
        }
        $events = [];
        foreach($incidents as $incident){
            $class = "vac-bg-status-" . $incident->status;
            $incident->event->end = $incident->event->start === $incident->event->end?$incident->event->end:$incident->event->end + (24 * 60 * 60);
            $events[] = [
                'title' => $incident->event->FullDescription,
                'start' => $incident->event->getStartDate('Y-m-d'),
                'end' => $incident->event->getEndDate('Y-m-d'),
                'allDay' => true,
                'classNames' => [ $class ],
            ];
        }

        if($user->employee){
            $globalEvents = Event::query();
            $globalEvents = $this->clocklogManager->eventQuery($globalEvents, date('Y-m-d', $start), date('Y-m-d', $end), $user)
            ->when($onlyActive, function($q){
                $q->where('blocked', 1);
            })
            ->get();
            foreach($globalEvents as $event){
                $class = "vac-bg-event";
                $event->end = $event->start === $event->end?$event->end:$event->end + (24 * 60 * 60);
                $events[] = [
                    'title' => $event->FullDescription,
                    'start' => $event->getStartDate('Y-m-d'),
                    'end' => $event->getEndDate('Y-m-d'),
                    'allDay' => true,
                    'classNames' => [ $class ],
                ];
            }
        }
        return response()->json($events, 200);
    }
}
