<?php

namespace App\Http\Controllers\Vacations;

use DB;
use Lang;
use App\User;
use App\Mail\PleaCreated;
use Illuminate\Http\Request;
use App\Mail\EmployeeRequest;
use App\Models\Vacations\Event;
use App\Models\Vacations\Balances;
use App\Models\Vacations\Benefits;
use App\Models\Vacations\EventDay;
use App\Models\Vacations\Incident;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\IncidentRequest;
use App\Models\Vacations\EventNonBlock;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\Vacations\PleaManager;

class PleaController extends Controller
{
    protected $now, $tomorrow;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->now = time();
        $this->tomorrow = $this->now + (1 * 24 * 60 * 60);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::where([
            ['type', 'user'],
            ['user_id', Auth::user()->id],
            ['status', 'pending']
        ])
        ->whereHas('benefit', function($q){
            $q->groupBenefit();
        })
        ->orderBy('start', 'ASC')
        ->paginate(20);
        foreach ($events as $event) {
            $event->status = \Lang::get('bd.' . $event->status);
        }

        $query_completed = env('APP_DELETE_APPROVED_VACATIONS', false);

        $approved_events = Event::where([
            ['type', 'user'],
            ['user_id', Auth::user()->id]
        ])
        ->whereHas('benefit', function($q){
            $q->groupBenefit();
        })
        ->whereHas('incident', function($q) {
            $q->whereIn('status', ['complete']);
        })
        ->has('incident')
        ->orderBy('start', 'ASC')
        ->paginate(20);

        foreach ($approved_events as $event) {
            $event->status = \Lang::get('bd.' . $event->status);
        }
        return view('vacations.common.active', ['events' => $events, 'approved_events' => $approved_events]);
    }

    public function getInactive()
    {
        $events = Event::where([
            ['type', 'user'],
            ['user_id', Auth::user()->id]
        ])
        ->where('status', '!=', 'pending')
        ->orderBy('start', 'DESC')
        ->get();

        foreach ($events as $event) {
            $event->status = \Lang::get('bd.' . $event->status);
        }
        return view('vacations.common.inactive', ['events' => $events]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->number === 'admin') {
            return redirect()->to('/');
        }
        $data = EmployeeController::getEmployeeBalance();

        return view('vacations.common.plea', $data);
    }

    public static function checkAvailableUser($user, $start, $end)
    {
        $user = Event::where('user_id', $user->id)
        ->where(function ($query) {
            $query->where('status', '<>', 'canceled')
                ->where('status', '<>', 'rejected');
        })
        ->where('type', 'user')
        ->where(function ($query) use ($start, $end) {
            $query->where('end', strtotime($start))
                ->orWhereBetween('end', [strtotime($start), strtotime($end)])
                ->orWhere('start', strtotime($start))
                ->orWhereBetween('start', [strtotime($start), strtotime($end)])
                ->orWhere('start', strtotime($end));
        })
        ->first();

        $events = EventNonBlock::where('end', $start)
        ->orWhereBetween('end', [$start, $end])
        ->orWhere('start', $start)
        ->orWhereBetween('start', [$start, $end])
        ->orWhere('start', $end)
        ->first();

        return !$user && !$events;
    }

    public static function checkAvailable($user, $start, $end, $blocked = 1)
    {
        $globals = Event::where('type', 'global')
        ->where('blocked', $blocked)
        ->where(function ($query) use ($start, $end) {
            $query->whereBetween('start', [strtotime($start), strtotime($end)])
                ->orWhereBetween('end', [strtotime($start), strtotime($end)])
                ->orWhere('start', strtotime($start))
                ->orWhere(function ($query2) use ($start, $end) {
                    $query2->where('start', '<', strtotime($start))
                        ->where('end', '>', strtotime($end));
                });
        })
        ->where(function ($query) use ($user) {
            $query->where('js', 'like', '%"' . $user->id . '"%')
                ->orWhere([
                    ['region_id', $user->getRegionID()], ['js', null]]);
        })
        ->first();

        return !$globals;
    }

    public static function checkHoliday($user, $start, $end, $blocked = 0)
    {
        $globals = Event::where('type', 'global')
        ->where('blocked', $blocked)
        ->where(function ($query) use ($start, $end) {
            $query->whereBetween('start', [strtotime($start), strtotime($end)])
                ->orWhereBetween('end', [strtotime($start), strtotime($end)])
                ->orWhere('start', strtotime($start))
                ->orWhere(function ($query2) use ($start, $end) {
                    $query2->where('start', '<', strtotime($start))
                        ->where('end', '>', strtotime($end));
                });
        })
        ->where(function ($query) use ($user) {
            $query->where('js', 'like', '%"' . $user->id . '"%')
                ->orWhere([['region_id', $user->region_id], ['js', null]]);
        })
        ->first();

        return !$globals;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IncidentRequest $request)
    {
        $event = $request->validated();
        $event['type'] = 'user';
        $event['status'] = 'pending';
        $event['blocked'] = 0;
        $event['user_id'] = Auth::user()->id;

        try {
            $result = PleaManager::storePlea($event);
        } catch (\Throwable $th) {
            return PleaManager::redirectRouteWithMessage('common.plea.create', $th->getMessage());
        }
        flash($result['message'], $result['class']);
        return redirect('/common/plea/create');
    }

    public static function storeRequest($request, $user, $route, $status = 'pending'){
        $now = strtotime(date('Y-m-d'));
        $tomorrow = $now + (1 * 24 * 60 * 60);
        $event = $request;
        $event['type'] = 'user';
        $event['status'] = $status;
        $event['blocked'] = 0;
        $event['user_id'] = $user->id;

        try {
            if (!($user instanceof User)) throw('Expected param to be User instance');
            $result = PleaManager::storePlea($event, $user);
        } catch (\Throwable $th) {
            return PleaManager::redirectRouteParamWithMessage($route, $user->id, $th->getMessage(), 'warning');
        }
        flash($result['message'], $result['class']);
        return redirect()->route($route, $user->id);
    }

    public static function removeDays($balance, $days)
    {
        $js = ['total' => $days];
        foreach ($balance['rows'] as $b) {
            $c = 0;
            while ($b->diff > 0 && $days > 0) {
                $c++;
                $b->diff--;
                $days--;
            }
            if ($c > 0) {
                $b->amount += $c;
                unset($b->diff);
                unset($b->solicited);
                $b->save();
                $js['balances'][] = ['id' => $b->id, 'amount' => $c];
            }
        }
        return (json_encode($js));
    }

    public static function restoreDays($event, $days = 0)
    {
        if ($days > 0) {
            $js = json_decode($event->js);
            foreach ($js->balances as $b) {
                $balance = Balances::find($b->id);
                if ($balance && $days > 0) {
                    while ($b->amount > 0 && $days > 0) {
                        $b->amount--;
                        $days--;
                        $balance->amount--;
                    }
                    $balance->save();
                }
            }
        } else {
            $js = json_decode($event->js);
            foreach ($js->balances as $b) {
                $balance = Balances::find($b->id);
                if ($balance) {
                    $balance->amount -= $b->amount;
                    $balance->save();
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $data = [];

            $r = Incident::where('event_id', $id)->first();

            if (!$r) {
                $data = ['success' => 0, 'error' => "Warning: No hay evento (no fue una solicitud)."];
                return response()->json($data);
            }

            try {
                $success = PleaManager::destroyPlea($r);
                $data = ['success' => $success];
            } catch (\Throwable $th) {
                $data = ['success' => 0, 'message' => $th->getMessage() ];
            }
            //Esto no se que onda...
            //Esta parte revisa si el evento es pasado o no, pero aun asi lo borra
            // if($r->event->end < $this->now) {
            //     $data = ['success' => 1, 'error' => " de evento pasado"];
            // } else {
            //     $data = ['success' => 1, 'error' => ""];
            // }

            return response()->json($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('/home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect('/home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //It most never happend.
    }
}
