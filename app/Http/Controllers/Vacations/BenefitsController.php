<?php

namespace App\Http\Controllers\Vacations;

use App\User;
use App\Http\Requests;

use Illuminate\Http\Request;
use App\Models\Vacations\Benefits;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Vacations\BenefitTimeDescription;

class BenefitsController extends Controller
{

    public function __construct(){
        $this->middleware('permission:incidents_admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $benefits = Benefits::get();
        return view('vacations.admin.incidents.index',['benefits' => $benefits]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vacations.admin.incidents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $benefit = $request->all();

        $benefitTime = [
            "seniority_type_of_time" => isset($benefit["select_seniority"])?$benefit["select_seniority"]:null,
            "seniority_quantity" => isset($benefit["number_seniority"])?$benefit["number_seniority"]:null,
            "period_use_type_of_time" => isset($benefit["select_period"])?$benefit["select_period"]:null,
            "period_use_quantity" => isset($benefit["number_period"])?$benefit["number_period"]:null,
            "use_max_type_of_time" => isset($benefit["select_max"])?$benefit["select_max"]:null,
            "use_max_quantity_per_time" => isset($benefit["number_max"])?$benefit["number_max"]:null,
        ];
        unset($benefit["select_seniority"]);
        unset($benefit["number_seniority"]);
        unset($benefit["select_period"]);
        unset($benefit["number_period"]);
        unset($benefit["select_max"]);
        unset($benefit["number_max"]);
        try {
            DB::beginTransaction();
            $res = Benefits::create($benefit);
            $res->timeDescription()->create($benefitTime);
            DB::commit();
            flash('Incidencia creada correctamente.','success');
        } catch (\Throwable $th) {
            DB::rollback();
            flash('No se pudo crear la incidencia  |  ' . $th->getMessage(),'danger');
        }
        return redirect('/admin/incidents');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $benefit = Benefits::with('timeDescription')->find($id);
        return view('vacations.admin.incidents.show',['benefit' => $benefit]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $benefit = Benefits::with('timeDescription')->find($id);
        return view('vacations.admin.incidents.edit',['benefit' => $benefit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tmp = $request->all();
        unset($tmp['_method']);
        unset($tmp['_token']);
        $benefitTime = [
            "seniority_type_of_time" => isset($tmp["select_seniority"])?$tmp["select_seniority"]:null,
            "seniority_quantity" => isset($tmp["number_seniority"])?$tmp["number_seniority"]:null,
            "period_use_type_of_time" => isset($tmp["select_period"])?$tmp["select_period"]:null,
            "period_use_quantity" => isset($tmp["number_period"])?$tmp["number_period"]:null,
            "use_max_type_of_time" => isset($tmp["select_max"])?$tmp["select_max"]:null,
            "use_max_quantity_per_time" => isset($tmp["number_max"])?$tmp["number_max"]:null,
        ];
        unset($tmp["select_seniority"]);
        unset($tmp["number_seniority"]);
        unset($tmp["select_period"]);
        unset($tmp["number_period"]);
        unset($tmp["select_max"]);
        unset($tmp["number_max"]);
        try {
            DB::beginTransaction();
            Benefits::where('id', $id)->update($tmp);
            BenefitTimeDescription::updateOrCreate(
                ['benefit_id' => $id],
                $benefitTime);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
        }
        
        flash('Registro actualizado.','warning');
        return redirect('/admin/incidents');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $benefit = Benefits::find($id);
        $benefit->delete();
        flash('Registro eliminado','danger');
        return redirect('/admin/incidents');
    }

    public static function process($frequency, $date = null)
    {
        //En herbalife solo el Comodin entra
        $query = Benefits::where(['cronjob'=>1,'frequency'=>$frequency])
                        ->whereNotIn('id',[5]);
        $benefits = $query->get();

        if($benefits) {
            if(is_null($date)){
                $query->update(['exec' => date('Y-m-d')]);
            }else{
                $query->update(['exec' => date('Y-m-d', $date)]);
            }
        }

        $users     = User::select('id')->get();
        if(is_null($date)){
            $nextyear  = strtotime('+1 year');
            $nextmonth = strtotime('+1 month');
            $nextweek  = strtotime('+1 week');
            $nextday   = strtotime('tomorrow');
            $now       = date('Y-m-d H:m:s');
            $year      = date('Y');
        }else{
            $nextyear  = strtotime('+1 year', $date);
            $nextmonth = strtotime('+1 month', $date);
            $nextweek  = strtotime('+1 week', $date);
            $nextday   = strtotime('tomorrow', $date);
            $now       = date('Y-m-d H:m:s', $date);
            $year      = date('Y', $date);
        }

        foreach($benefits as $benefit) {
            $bash = [];
            if($benefit->remove == 1) {
                switch ($frequency) {
                    case 'yearly' : $timestamp = $nextyear; break;
                    case 'monthly': $timestamp = $nextmonth; break;
                    case 'weekly' : $timestamp = $nextweek; break;
                    case 'daily'  : $timestamp = $nextday; break;
                }
                foreach($users as $user) {
                    $bash[] = [
                        'user_id' => $user->id,
                        'amount' => 0,
                        'pending'=> $benefit->days,
                        'benefit_id' => $benefit->id, //Arbitrario...
                        'year' => $year,
                        'until' => date('Y-m-d',$timestamp),
                        'created_at' => $now,
                        'updated_at' => $now,
                    ];
                }
                Balances::insert($bash);
            } else {
                $balances = Balances::where(['benefit_id' => $benefit->id,'year'=>$year])
                                ->update(['pending' => \DB::raw("pending + $benefit->days")]);
            }
        }
    }
}
