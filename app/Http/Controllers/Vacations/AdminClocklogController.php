<?php

namespace App\Http\Controllers\Vacations;

use App\Models\Department;
use Illuminate\Http\Request;
use App\Models\Vacations\ClockLog;
use App\Http\Controllers\Controller;

class AdminClocklogController extends Controller
{
    public function __construct(){
        $this->middleware('permission:incidents_admin')->only(['history']);
    }

    public function history(){
        $departments = Department::with(['direction' => function($q){
            $q->withTrashed();
        }])->withTrashed()
        ->orderBy('name', 'ASC')
        ->get();
        
        $logs = ClockLog::with('user.employee_wt.boss_wt', 'user.employee_wt.jobPosition_wt.area_wt.department_wt', 'comments')
        ->orderBy('date', 'DESC')
        ->get();
        
        return view('vacations.admin.clocklogs.history', compact('logs', 'departments'));
    }
}
