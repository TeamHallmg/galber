<?php

namespace App\Http\Controllers\Vacations;

use App\User;
use Illuminate\Http\Request;
use App\Models\Vacations\Balances;
use App\Http\Controllers\Controller;
use App\Http\Requests\BalancesUserRequest;
use App\Http\Controllers\Vacations\BenefitManager;

class BalanceUserController extends Controller
{
    protected $benefitManager;

    public function __construct(BenefitManager $benefitManager)
    {
        $this->benefitManager = $benefitManager;
    }
    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $benefit = $this->benefitManager->getVacationBenefit();
        $users = User::has('employee')->get()->pluck('FullName', 'id')->toArray();
        return view('vacations.admin.create_balances', compact('benefit', 'users'));
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BalancesUserRequest $request)
    {
        $request = $request->validated();
        $request['amount'] = 0;
        $request['extraordinary_period'] = true;
        try {
            Balances::create($request);
        } catch (\Throwable $th) {
            return redirect()->route('admin.incidents.balances.create')->withInput()->withErrors($th->getMessage());   
        }
        flash('Saldo extraordinario creado correctamente!!!', 'success');
        return redirect()->route('admin.incidents.balances.create');
    }
    
    /**
     * API function to get the user's next period year
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function getYear($id){
        $user = User::has('employee')->findOrFail($id);
        return response()->json(['success' => true, 'year' => $user->getMyNextYear()], 200);
    }
}
