<?php

namespace App\Http\Controllers\Vacations;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Vacations\Event;
use App\Models\Vacations\EventDay;
use App\Models\Vacations\Incident;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\VacationReportExport;
use App\Exports\IncidentsReportExport;
use App\Http\Requests\ExportIncidentsRequest;
use App\Http\Controllers\Vacations\BenefitManager;

class ExportController extends Controller
{
    protected $benefitManager;

    public function __construct(BenefitManager $benefitManager){
        $this->benefitManager = $benefitManager;
        $this->middleware('permission:incidents_admin');
    }

    
    public function index($type){
        $date = null;
        switch($type){
            case 'vacations':
                $vacation = $this->benefitManager->getVacationBenefit();
                if($vacation){
                    $event = EventDay::whereHas('event', function($q) use($vacation){
                        $q->whereIn('status', ['accepted', 'processing'])
                        ->has('user.employee')
                        ->has('incident')
                        ->where('benefit_id', $vacation->id);
                    })
                    ->where('processed', 0)
                    ->orderBy('date', 'ASC')
                    ->first();
                    $date = $event? $event->date:null;
                }
                $title = "Vacaciones";
                $url = route('admin.exports.vacations');
            break;
            case 'incidents':
                $event = EventDay::whereHas('event', function($q){
                    $q->whereIn('status', ['accepted', 'processing'])
                    ->has('user.employee')
                    ->has('incident')
                    ->whereHas('benefit', function($q1){
                        $q1->where('report', 1)
                        ->where('type', 'day');
                    });
                })
                ->where('processed', 0)
                ->orderBy('date', 'ASC')
                ->first();
                $date = $event? $event->date:null;
                $title = 'Incidencias';
                $url = route('admin.exports.incidents');
            break;
        }
        return view('vacations.admin.exports.index', compact('url', 'date', 'title'));
    }
    
    public function exportVacations(ExportIncidentsRequest $request){
        $endDate = $request->end;
        $process = isset($request->process);
        $events = $this->getVacationsToExport($endDate);
        $data = [];
        DB::beginTransaction();
        foreach($events as $event){
            foreach ($event->eventDays as $day){
                if($process){
                    try {
                        $day->processed = 1;
                        $day->processed_at = Carbon::now();
                        $day->processed_by = Auth::user()->id;
                        $day->save();
                    } catch (\Throwable $th) {
                        DB::rollback();
                        return redirect()->route('admin.exports.index', 'vacations')->withInput()->withErrors(['msg' => 'Algo salio mal al intentar procesar el día | ' . $th->getMessage() ]);
                    }
                }
                $data[] = [
                    $event->user->employee->idempleado,
                    $day->getDate('d/m/Y'),
                    1,
                    0.25,
                    '',
                ];
            }
        }
        try {
            $this->processVacationsToExport($endDate);            
        } catch (\Throwable $th) {
            return redirect()->route('admin.exports.index', 'vacations')->withInput()->withErrors(['msg' => 'Algo salio mal al intentar procesar la información | ' . $th->getMessage() ]);
        }
        DB::commit();
        return Excel::download(new VacationReportExport($data), 'ReporteVacaciones.xlsx');
    }

    public function exportIncidents(ExportIncidentsRequest $request){
        $endDate = $request->end;
        $process = isset($request->process);
        $events = $this->getIncidentsToExport($endDate);
        DB::beginTransaction();
        $data = [];
        foreach($events as $event){
            foreach ($event->eventDays as $day){
                if($process){
                    try {
                        $day->processed = 1;
                        $day->processed_at = Carbon::now();
                        $day->processed_by = Auth::user()->id;
                        $day->save();
                    } catch (\Throwable $th) {
                        DB::rollback();
                        return redirect()->route('admin.exports.index', 'incidents')->withInput()->withErrors(['msg' => 'Algo salio mal al intentar procesar el día | ' . $th->getMessage() ]);
                    }
                }
                $data[] = [
                    $event->user->employee->idempleado,
                    $event->benefit->code,
                    $day->getDate('d/m/Y'),
                    1,
                    '',
                ];
            }
        }
        try {
            $this->processIncidentsToExport($endDate);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->route('admin.exports.index', 'incidents')->withInput()->withErrors(['msg' => 'Algo salio mal al intentar procesar la información | ' . $th->getMessage() ]);    
        }
        
        DB::commit();
        return Excel::download(new IncidentsReportExport($data), 'ReporteIncidencias.xlsx');
    }

    public function getVacationsToExport($endDate){
        $vacation = $this->benefitManager->getVacationBenefit();
        $events = [];
        if($vacation){
            $events = Event::with(['user' => function($q){
                $q->select('id', 'employee_id')
                ->with(['employee' => function($q){
                    $q->select('id', 'idempleado');
                }]);
            }, 'eventDays' => function($q) use($endDate){
                $q->where('processed', 0)
                ->where('date', '<=', $endDate);
            }])
            ->whereIn('status', ['accepted', 'processing'])
            ->where('benefit_id', $vacation->id)
            ->has('user.employee')
            ->has('incident')
            ->whereHas('eventDays', function($q) use($endDate){
                $q->where('processed', 0)
                ->where('date', '<=', $endDate);
            })
            ->get();
        }
        return $events;
    }

    public function getIncidentsToExport($endDate){
        $events = Event::with(['user' => function($q){
            $q->select('id', 'employee_id')
            ->with(['employee' => function($q){
                $q->select('id', 'idempleado');
            }]);
        }, 'eventDays' => function($q) use($endDate){
            $q->where('processed', 0)
            ->where('date', '<=', $endDate);
        }, 'benefit' => function($q){
            $q->select('id', 'code')
            ->where('report', 1)
            ->where('type', 'day');
        }])
        ->whereIn('status', ['accepted', 'processing'])
        ->has('user.employee')
        ->has('incident')
        ->whereHas('eventDays', function($q) use($endDate){
            $q->where('processed', 0)
            ->where('date', '<=', $endDate);
        })
        ->whereHas('benefit', function($q1){
            $q1->where('report', 1)
            ->where('type', 'day');
        })
        ->get();
        return $events;
    }

    public function processVacationsToExport($endDate){
        $vacation = $this->benefitManager->getVacationBenefit();
        if($vacation){
            $events = Event::whereIn('status', ['accepted', 'processing'])
            ->where('benefit_id', $vacation->id)
            ->has('user.employee')
            ->has('incident')
            ->whereHas('eventDays', function($q) use($endDate){
                $q->where('processed', 1)
                ->where('date', '<=', $endDate);
            })
            ->get()->pluck('id')->toArray();

            // Si todos los eventDays del evento aún no están procesados, no se procesa el evento
            foreach($events as $key => $event_id) {
                $unset = false;
                try {
                    $event = Event::findOrFail($event_id);
                    foreach($event->eventDays as $event_day) {
                        if($event_day->processed == 0) {
                            $unset = true;
                        }
                    }
                } catch (\Throwable $th) {
                    throw $th;
                }
                if($unset) {
                    unset($events[$key]);
                }
            }

            try {
                Event::whereIn('id', $events)->update(['status' => 'processed', 'processed_at' => Carbon::now(), 'processed_by' => Auth::user()->id]);
                Incident::whereIn('event_id', $events)->update(['status' => 'processed']);
            } catch (\Throwable $th) {
                throw $th;
            }
        }
    }

    public function processIncidentsToExport($endDate){
        $events = Event::whereIn('status', ['accepted', 'processing'])
        ->has('user.employee')
        ->has('incident')
        ->whereHas('eventDays', function($q) use($endDate){
            $q->where('processed', 1)
            ->where('date', '<=', $endDate);
        })
        ->whereHas('benefit', function($q1){
            $q1->where('report', 1)
            ->where('type', 'day');
        })
        ->get()->pluck('id')->toArray();

        // Si todos los eventDays del evento aún no están procesados, no se procesa el evento
        foreach($events as $key => $event_id) {
            $unset = false;
            try {
                $event = Event::findOrFail($event_id);
                foreach($event->eventDays as $event_day) {
                    if($event_day->processed == 0) {
                        $unset = true;
                    }
                }
            } catch (\Throwable $th) {
                throw $th;
            }
            if($unset) {
                unset($events[$key]);
            }
        }

        try {
            Event::whereIn('id', $events)->update(['status' => 'processed', 'processed_at' => Carbon::now(), 'processed_by' => Auth::user()->id]);
            Incident::whereIn('event_id', $events)->update(['status' => 'processed']);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function unprocessVacations($startDate = null) {
        if(Auth::user()->id != 1) {
            return back();
        }
        if(is_null($startDate)) {
            return back();
        }

        $vacation = $this->benefitManager->getVacationBenefit();
        $events = [];
        if($vacation){
            $events = Event::with(['user' => function($q){
                $q->select('id', 'employee_id')
                ->with(['employee' => function($q){
                    $q->select('id', 'idempleado');
                }]);
            }, 'eventDays' => function($q) use($startDate){
                $q->where('processed', 1)
                ->where('date', '>=', $startDate);
            }])
            ->whereIn('status', ['processed'])
            ->where('benefit_id', $vacation->id)
            ->has('user.employee')
            ->has('incident')
            ->whereHas('eventDays', function($q) use($startDate){
                $q->where('processed', 1)
                ->where('date', '>=', $startDate);
            })
            ->get();
        }

        // Event::where('start', '>=', strtotime($startDate))

        // dd($events);

        foreach($events as $event){
            foreach ($event->eventDays as $day){
                try {
                    DB::beginTransaction();
                    $day->processed = 0;
                    $day->processed_at = null;
                    $day->save();
                    DB::commit();
                } catch (\Throwable $th) {
                    DB::rollback();
                    dd($th);
                    return redirect()->route('admin.exports.index', 'incidents')->withInput()->withErrors(['msg' => 'Algo salio mal al intentar desprocesar el día | ' . $th->getMessage() ]);
                }
            }
        }

        // Desprocesa eventos e incidencias
        $events = Event::whereIn('status', ['processed'])
        ->where('benefit_id', $vacation->id)
        ->has('user.employee')
        ->has('incident')
        ->whereHas('eventDays', function($q) use($startDate){
            $q->where('processed', 0)
            ->where('date', '>=', $startDate);
        })
        ->get()->pluck('id')->toArray();

        // Si todos los eventDays del evento aún no están procesados, no se procesa el evento
        foreach($events as $key => $event_id) {
            $unset = false;
            try {
                $event = Event::findOrFail($event_id);
                foreach($event->eventDays as $event_day) {
                    if($event_day->processed == 1) {
                        $unset = true;
                    }
                }
            } catch (\Throwable $th) {
                throw $th;
            }
            if($unset) {
                unset($events[$key]);
            }
        }


        try {
            DB::beginTransaction();
            Event::whereIn('id', $events)->update(['status' => 'accepted', 'processed_at' => null]);
            Incident::whereIn('event_id', $events)->update(['status' => 'complete']);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            dd($th);
        }
        
        dd('se desprocesaron las vacaciones desde el día ',$startDate);
    }


     /**
      * DE AQUI A ABAJO ES CÓDIGO VIEJO DEL MÓDULO, EL CUAL NO TOMAREMOS EN CUENTA POR EL MOMENTO PARA EL PROYECTO DE UCIN
      */
    
    public function export()
    {
        return view('admin.export');
    }

    public function getcsv(Request $request)
    {
        $start = strtotime($request->start);
        $end = strtotime($request->end);
        // dd($request->all());
        $incidents = Incident::with([
            'event',
            'event.eventDays' => function($q){
                // $q->where('processed', '!=', 1);
            }
        ])
        ->whereHas('event', function($q) use ($start, $end){
            $q->whereBetween('start', [$start, $end])
            ->orWhereBetween('end', [$start, $end])
            ->orWhere(function($q) use($start, $end){
                $q->where('start', '>=', $start)
                ->where('end', '<=', $end);
            })
            ->orWhere(function($q) use($start, $end){
                $q->where('start', '<=', $start)
                ->where('end', '>=', $end);
            });
        })->get();
        $csvIncidents = [];
        foreach ($incidents as $key => $incident) {
            $dates = [];
            $beginDate = '';
            $endDate = '';
            $days = 0;
            foreach ($incident->event->eventDays as $key => $day) {
                // if($incident->id == 11)
                // dd($incident->id, date('Y-m-d', $start), $day->date, $day->date, date('Y-m-d',$end), $day->processed, $start <= strtotime($day->date) && strtotime($day->date) <= $end && $day->processed == 0);
                if($start <= strtotime($day->date) && strtotime($day->date) <= $end && $day->processed == 0){
                    $dates[] = $day->date;
                    if(empty($beginDate)){
                        $beginDate = date('d/m/Y', strtotime($day->date));
                    }
                    $endDate = date('d/m/Y', strtotime($day->date));
                    $days++;
                }
            }
            if($days > 0){
                $csvIncidents[] = [
                    'employee' => $incident->from->number,
                    'period'  => 'V'. date('Y', $start),
                    'days' => $days,
                    'zeros' => '0.00',
                    'start' => $beginDate . ',00:00:00',
                    'end' => $endDate . ',00:00:00',
                ];
                // if($incident->id == 11)
                // dd($csvIncidents);
            }
            if(isset($request->mark)){
                $incident->event->eventDays()
                ->whereIn('date', $dates)
                ->update(['processed' => 1]);
            }
        }
        // $myfile = fopen(, "w") or die("Unable to open file!");
        $data = "";
        foreach ($csvIncidents as $key => $line) {
            $data .= "E".$line['employee']."\n";
            $data .= $line['period'].'|'.$line['days'].'|'.$line['zeros'].'|'.$line['start'].'|'.$line['end'].'||'.$line['end']."\n";            
        }
        \Storage::put(date('Y-m-d')."_vac.txt", $data);
        
        return \Storage::download(date('Y-m-d')."_vac.txt");
        // Thursday, 18 April 2019 0:00:00
        // Saturday, 20 April 2019 0:00:00
    }

    public function exportOvertime()
    {
        return view('admin.exportOvertime')
        ;
    }

    public function getcsvOvertime(Request $request)
    {
        $start = strtotime($request->start);
        $end = strtotime($request->end);

        $incidents = Incident::with([
            'event',
            'event.eventDays' => function($q){
                // $q->where('processed', '!=', 1);
            }
        ])
        ->whereHas('event', function($q) use ($start, $end){
            $q->whereBetween('start', [$start, $end])
            ->orWhereBetween('end', [$start, $end])
            ->orWhere(function($q) use($start, $end){
                $q->where('start', '>=', $start)
                ->where('end', '<=', $end);
            })
            ->orWhere(function($q) use($start, $end){
                $q->where('start', '<=', $start)
                ->where('end', '>=', $end);
            });
        })->get();

        // dd($incidents, $request->start, $request->end);
        $vacations = [];
        foreach ($incidents as $key => $incident) {
            $currentTime = $start;
            while($currentTime <= $end){
                if($currentTime >= $incident->event->start && $currentTime <= $incident->event->end){
                    if(isset($vacations[$incident->from->id])){
                        $vacations[$incident->from->id]++;
                    }else{
                        $vacations[$incident->from->id] = 1;
                    }
                }
                $currentTime += 24*60*60;
            }
        }
        dd($vacations);
    }

    public function exportExtras(){
        return view('admin.exportExtra');
    }

    public function getExtras(Request $request){
        // dd($request->all());
        $timeStart = strtotime($request->start);
        $timeEnd = strtotime($request->end);
        $start = $request->start;
        $end = $request->end;

        /*$users = User::where(function($query) use($start, $end){
            $query->whereHas('incidents.event', function($q) use ($start, $end){
                $q->whereBetween('start', [$start, $end])
                ->orWhereBetween('end', [$start, $end])
                ->orWhere(function($q) use($start, $end){
                    $q->where('start', '>=', $start)
                    ->where('end', '<=', $end);
                })
                ->orWhere(function($q) use($start, $end){
                    $q->where('start', '<=', $start)
                    ->where('end', '>=', $end);
                });
            });
        })->get();*/

        $users = User::with([
            /*'incidents.event' => function($q) use($start, $end){
                $q->with('eventDays')
                ->whereBetween('start', [$start, $end])
                ->orWhereBetween('end', [$start, $end])
                ->orWhere(function($q) use($start, $end){
                    $q->where('start', '>=', $start)
                    ->where('end', '<=', $end);
                })
                ->orWhere(function($q) use($start, $end){
                    $q->where('start', '<=', $start)
                    ->where('end', '>=', $end);
                });
            },*/
            'incidents.event.eventDays' => function($q) use($start, $end){
                $q->whereBetween('date', [$start, $end]);
            },
            'incidents.incidentOvertime' => function($q) use($start, $end){
                $q->whereBetween('date', [$start, $end]);
            },
            // 'exchanges.applicantWorkshift' => function($q) use($start, $end){
            //     $q->whereBetween('date', [$start, $end]);
            // },
            // 'exchanges.requestedWorkshift',
            // 'exchangesWithMe.requestedWorkshift' => function($q) use($start, $end){
            //     $q->whereBetween('date', [$start, $end]);
            // },
            // 'exchangesWithMe.applicantWorkshift',
        ])->orderBy('number', 'ASC')->get();

        
        $extra3hourDiscounts = [
            2 => 1,
            3 => 2,
            4 => 3,
            5 => 4,
            6 => 5,
        ];

        $txt = "Empleado,Prolongacion Dobles, Prolongacion Triples, Extension Dobles, Extension Tiples\n";
        foreach ($users as $key => $user) {
            /* Por Regla de Flexfilm todos los empleados tienen 3 horas extras por cajon */
            $extra3hour = 3;
            $prolongacionJornada = 0;
            $descansoLaborado = 0;
            foreach ($user->incidents as $key => $incident) {
                if($incident->event){
                    if(isset($extra3hourDiscounts[count($incident->event->eventDays)])){
                        /* Estas horas se descuentan conforme una tablita ya definida 
                        $extra3hourDiscounts = [2 => 1,3 => 2,4 => 3,5 => 4,6 => 5,];
                        */
                        $extra3hour -= count($incident->event->eventDays);
                    }
                }
                if($incident->incidentOvertime){
                    if($incident->from->region->isDayWorkable($incident->incidentOvertime->date)){
                        $prolongacionJornada += $incident->amount / 60;
                    }else{
                        $descansoLaborado += $incident->amount / 60;
                    }
                }
            }
            
            if($extra3hour > 0){
                if($prolongacionJornada === $descansoLaborado){
                    $prolongacionJornada += $extra3hour;
                }else{
                    $descansoLaborado += ($prolongacionJornada == 0 && $descansoLaborado >= 0)?$extra3hour:0;
                    $prolongacionJornada += ($descansoLaborado == 0 && $prolongacionJornada >= 0)?$extra3hour:0;
                }
            }

            $prolongacionDobles = ($prolongacionJornada > 9)?9:$prolongacionJornada;
            $prolongacionTriples = ($prolongacionJornada > 9)?$prolongacionJornada-9:0;
            $descansoDobles = ($descansoLaborado > 9)?9:$descansoLaborado;
            $descansoTriples = ($descansoLaborado > 9)?$descansoLaborado-9:0;

            if($user->number == "emplo"){
                // dd($descansoLaborado, $prolongacionJornada);
            }

            $txt .= str_pad($user->number, 5, '0', STR_PAD_LEFT) . "," . $prolongacionDobles . "," . $prolongacionTriples . "," . $descansoDobles . "," . $descansoTriples . "\n";
        }

        $currTimeStamp = strtotime(date('Y-m-d H:i:s'));
        \Storage::put($currTimeStamp . ".csv", $txt);
        
        return \Storage::download($currTimeStamp . ".csv");
    }
}
