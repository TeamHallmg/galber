<?php

namespace App\Http\Controllers\Vacations;

use App\User;
use Exception;
use App\Mail\PleaCreated;
use App\Mail\EmployeeRequest;
use App\Models\Vacations\Event;
use App\Models\Vacations\Benefits;
use App\Models\Vacations\EventDay;
use App\Models\Vacations\Incident;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\EmployeeController;

class PleaManager{
    
    public static function isVacationModuleInstalled(){
        return class_exists(\App\Models\Vacations\Incident::class)
            && class_exists(\App\Models\Vacations\Event::class)
            && class_exists(\App\Models\Vacations\Balances::class)
            && class_exists(\App\Models\Vacations\Benefits::class)
            && class_exists(\App\Models\Vacations\EventDay::class);
    }

    public static $now;
    public static $tomorrow;

    public function __construct(){
        self::$now = strtotime(date('Y-m-d'));
        self::$tomorrow = self::$now + (1 * 24 * 60 * 60);
    }
    
    /**
     * Check if user has region relation (Not having employee relation will return false) 
     *
     * @return bool
     */
    public static function userHasRegion($user = null){
        $val = $user?$user->getRegion():Auth::user()->getRegion();
        return !!$val;
    }
    
    /**
     * Helper function to redirect to an URL with a flash message and type of
     *
     * @param  string $url that can be found in web.php file
     * @param  string $flash message to display
     * @param  string $type will change the color of the target alert
     * @return Illuminate\Http\RedirectResponse
     */
    public static function redirectUrlWithMessage($url, $flash, $type = 'warning'){
        flash($flash, $type)->important();
        return redirect($url)->withInput();
    }
    
    /**
     * Helper function to redirect to a Route (Named Urls) with a flash message and type of
     *
     * @param  string $route that can be found in web.php file only for named urls
     * @param  string $flash message to display
     * @param  string $type will change the color of the target alert
     * @return Illuminate\Http\RedirectResponse
     */
    public static function redirectRouteWithMessage($route, $flash, $type = 'warning'){
        flash($flash, $type)->important();
        return redirect()->route($route)->withInput();
    }

    /**
     * Helper function to redirect to a Route (Named Urls with param) with a flash message and type of
     *
     * @param  string $route that can be found in web.php file only for named urls
     * @param  string $param required by de url
     * @param  string $flash message to display
     * @param  string $type will change the color of the target alert
     * @return Illuminate\Http\RedirectResponse
     */
    public static function redirectRouteParamWithMessage($route, $param, $flash, $type = 'warning'){
        flash($flash, $type)->important();
        return redirect()->route($route, $param)->withInput();
    }
    
    /**
     * Genera un arreglo de días disponibles
     * Es decir, son días laborales y no estan bloqueados por eventos
     *
     * @param  \App\Models\Vacations\Event $e
     * @return array 
     */
    public static function getAvailableDays($e, $user = null){
        $schedule = $e->getUserSchedule();
        $daysAvailables = [];
        $holidays = self::getHolidays($e->start, $e->end, $user);
        $holidays += self::getUserEventDays($e->getStartDate('Y-m-d'), $e->getEndDate('Y-m-d'), $user);

        foreach ($schedule as $key => $sday) {
            if ($sday->labor == 1 && !isset($holidays[strtotime($sday->date)])) {
                $daysAvailables[] = $sday->date;
            }
        }
        return $daysAvailables;
    }
    
    /**
     * Get an array with users holidays
     *
     * @param  string $start Y-m-d
     * @param  null $end (optional) string Y-m-d 
     * @param  null (optional) \App\User $user
     * @return array [strtotime({date}) => {date}(Y-m-d)]
     */
    public static function getHolidays($start, $end = null, $user = null)
    {
        $start = strtotime($start);
        $year = date('Y', $start);
        $end = $end ? $end : strtotime(($year + 1) . '-01-01');
        $user = $user ? $user : Auth::user();

        $events = Event::where('type', 'global')
        ->where('blocked', 1)
        ->where(function ($query) use ($start, $end) {
            $query->whereBetween('start', [$start, $end])
            ->orWhereBetween('end', [$start, $end])
            ->orWhere(function($q) use($start, $end){
                $q->where('start', '>=', $start)
                ->where('end', '<=', $end);
            });
        })
        ->where(function ($query) use ($user) {
            $query->where('js', 'like', '%"' . $user->id . '"%')
                ->orWhere([
                    ['region_id', $user->employee->region_id],
                    ['js', null],
                ]);
        })
        ->get();

        $dates = [];
        foreach ($events as $event) {
            $start = $event->start;
            while ($start <= $event->end) {
                $dates[$start] = date('Y-m-d', $start);
                $start += 24 * 60 * 60;
            }
        }
        return $dates;
    }
    
    /**
     * Get an array with users Events Requested
     *
     * @param  string $start Y-m-d
     * @param  string $end Y-m-d
     * @param  \App\User $user
     * @return array [strtotime({date}) => {date}(Y-m-d)]
     */
    public static function getUserEventDays($start, $end, $user = null)
    {
        if ($user && !( $user instanceof User )) throw ('Expected param to be User instance');
        $user = $user ? $user : Auth::user();

        $events = EventDay::whereHas('event', function($q) use($user){
            $q->statusActive()
            ->where('user_id', $user->id);
        })
        ->where('date', '>=', $start)
        ->where('date', '<=', $end)
        ->get();
        $dates = [];
        foreach ($events as $event) {
            $dates[strtotime($event->date)] = $event->date;
        }
        return $dates;
    }
    
    /**
     * Check if employee has the required seniority requested by the benefit
     *
     * @param  int|\App\Models\Vacations\Benefits  $benefit
     * @param  int|\App\User $user
     * @return bool
     */
    public static function checkForRequiredSeniority($benefit, $user){
        $benefit = self::getBenefit($benefit);
        $user = self::getUser($user);
        if($benefit && $user) return $benefit->checkIfUserHasRequiredSeniority($user);
        return false;
    }

        
    /**
     * Check if employee request fits in the max time data
     *
     * @param  int|\App\Models\Vacations\Benefits $benefit
     * @param  int|\App\User $user
     * @param  int $days count in the request
     * @param  null $time (optional) int - Total hours in the request
     * @return bool
     */
    public static function checkForDaysFitsInUseMaxData($benefit, $days, $time = null){
        $benefit = self::getBenefit($benefit);
        if($benefit) return $benefit->checkIfDaysFitsInUseMaxData($days, $time);
        return false;
    }
    
    /**
     * Check if employee request doesn't conflict with max request in the benefit's period
     *
     * @param  int|\App\Models\Vacations\Benefits $benefit
     * @param  int|\App\User $user
     * @param  string $start in Y-m-d format
     * @param  string $end in Y-m-d format
     * @param  int $days count in the request
     * @param  null $time (optional) int - Total hours in the request
     * @return bool
     */
    public static function checkForRequestInTimePeriod($benefit, $user, $start, $end, $days, $time = null){
        $benefit = self::getBenefit($benefit);
        $user = self::getUser($user);
        if($benefit && $user) return $benefit->checkIfUserHasRequestsInTimePeriod($user, $start, $end, $days, $time);
        return false;
    }
    
    /**
     * Combine all benefit validations
     *
     * @param  int|\App\Models\Vacations\Benefits  $benefit
     * @param  int|\App\User $user
     * @param  string $start in Y-m-d format
     * @param  string $end in Y-m-d format
     * @param  int $day count in the request
     * @return bool
     */
    public static function checkForTimeFrequence($benefit, $user, $start, $end, $days){
        $benefit = self::getBenefit($benefit);
        $user = self::getUser($user);
        if($benefit && $user){
            if(self::checkForRequiredSeniority($benefit, $user)){
                return $benefit->checkIfUserHasRequestsInTimePeriod($user, $start, $end, $days)
                    && $benefit->checkIfDaysFitsInUseMaxData($days);
            }
        }
        return false;
    }
    
    /**
     * Helper to get a Benefit object instance if necessary
     *
     * @param  int|\App\Models\Vacations\Benefits  $benefit
     * @return \App\Models\Vacations\Benefits
     */
    public static function getBenefit($benefit){
        return $benefit instanceof Benefits?$benefit:Benefits::with('timeDescription')->find($benefit);
    }
    
    /**
     * Helper to get a User object instance if necessary
     *
     * @param  int|\App\User $user
     * @return \App\User
     */
    public static function getUser($user){
        return $user instanceof User?$user:User::with('employee')->has('employee')->find($user);
    }
    
    /**
     * Generate class and message required for a Flash alert
     *
     * @param  mixed $event
     * @return void
     */
    public static function warning15Days(Event $event){
        $class = 'success';
        $message = 'El evento (' . $event['title'] . ') se ha creado satisfactoriamente.';
        if ($event['start'] < (self::$tomorrow + (15 * 24 * 60 * 60))) {
            $message .= ' Debe procurar realizar sus solicitudes con una anticipación de 15 días.';
            $class = 'warning';
        }
        return ['class' => $class, 'message' => $message];
    }
    
    /**
     * Create a request/incident 
     *
     * @param  array $request all the informaction required to create an Event
     * @param  \App\User (optional)
     * @return array ['message' => {msg}, 'class' => {flash_class}]
     */
    public static function storePlea($request, $user = null){
        $event = $request;
        $incident_status = "pending";

        $user = $user ? $user : Auth::user();
        
        if(isset($request['status'])){
            switch($request['status']){
                case "accepted":
                    $incident_status = "complete";
                break;
                default:
                    $incident_status = $request['status'];
                break;
            }
        }
        if(!self::userHasRegion($user))
            throw new Exception('<strong>Error.</strong> No se te ha asignado una región para obtener tu horario');

        $event['region_id'] = $user->getRegion()->id;
        $startDay = strtotime($event['start']);
        $endDay = strtotime($event['end']);

        /* Revisamos si tiene un jefe por su relacione en la tabla de empleado */
        if (!$user->hasAuthorizator())
            throw new Exception('<strong>Error.</strong> No se te ha asignado a alguien que autorice');

        $js = [];

        $balance = EmployeeController::getBalanceDetail($event['benefit_id'], $user->id);
        $event['start'] = strtotime($event['start']);
        $event['end'] = strtotime($event['end']);

        $e = new Event($event);
        $schedule = $user->getSchedule($e->start);
        if (is_null($schedule))
            throw new Exception('<strong>Error.</strong> No se ha creado tu calendario laboral.');

        if (!$e->isStartDateWorkable())
            throw new Exception('<strong>Error.</strong> La fecha de inicio de la solicitud es un dia no laborable, por favor cambialo');

        if ($e->isStartDateAnEvent())
            throw new Exception('<strong>Error.</strong> La fecha de inicio de la solicitud es un dia bloqueado por un evento, por favor cambialo');

        if ($e->isStartDateClashWithRequest())
            throw new Exception('<strong>Error.</strong> La fecha de inicio de la solicitud choca con una solicitud tuya');

        $e->generateValidDates();
        $msg = self::warning15Days($e);

        if ($balance['benefit']->days != 0) {
            $e->end = $e->continuousDays();
            $days = $balance['benefit']->days;
        } else {
            $days = $e->getDays(true);
            /**
             * Esta parte ira hardcode porque no manejamos este tipo de solicitudes
             * Como no se hara parte del requerimiento dividiremos entre 5 los dias para las solicitudes semanales
             */
            if($e->benefit->week_calendar){
                try {
                    $days /= 5;
                } catch (\Throwable $th) {
                    throw('Fallo con la solicitud semanal');
                }
            }
            /** Fin del Hardcodeo **/
        }

        if ($e->end < $e->start) throw("Start date became greater than the end date");

        /* Revisamos que el empleado tenga la antigüedad suficiente solicitada por el beneficio */
        if(!self::checkForRequiredSeniority($e->benefit_id, $e->user_id))
        throw new Exception('No tienes la antigüedad necesaria para pedir: ' . $e->benefit->name);

        /* Revisamos que los días/horas solicitados no pasen los permitidos por solicitud por el beneficio*/
        if(!self::checkForDaysFitsInUseMaxData($e->benefit_id, $days, $e->time))
            throw new Exception(trans_choice('benefit_times.use_max_type_of_time.' . $e->benefit->timeDescription->use_max_type_of_time, $e->benefit->timeDescription->use_max_quantity_per_time, ['benefit' => $e->benefit->name]));

        /* Revisamos que el usuario no haya sobrepasado el máximo de solicitudes en el periodo de tiempo definido por el beneficio */
        if(!self::checkForRequestInTimePeriod($e->benefit_id, $e->user_id, $e->getStartDate('Y-m-d'), $e->getEndDate('Y-m-d'), $days, $e->time))
            throw new Exception(trans_choice('benefit_times.period_use_type_of_time.' . $e->benefit->timeDescription->period_use_type_of_time, $e->time?$e->time:$days, ['benefit' => $e->benefit->name, 'max' => $e->benefit->timeDescription->period_use_quantity]));

        if ($balance['benefit']->continuous) {
            $e->js = json_encode(['total' => $days]);
            
            $balance['rows'][0]->pending += $days;
            unset($balance['rows'][0]->diff);
            unset($balance['rows'][0]->solicited);

            $daysAvailables = self::getAvailableDays($e, $user);
            DB::beginTransaction();
            try {
                $balance['rows'][0]->save();
            } catch (Exception $e) {
                DB::rollback();
                throw new Exception('<strong>Error.</strong> No se puedo actulizar la linea de balance');
            }

            try {
                $e->save();
            } catch (\Exception $e) {
                DB::rollback();
                throw new Exception('<strong>Error.</strong> No se pudo crear el evento');
            }

            try{
                foreach($daysAvailables as $day){
                    $block = 1;
                    if(isset($event['date'])){
                        $block = $e->benefit->week_calendar && $event['date'] === $day?1:0;
                    }
                    EventDay::create([
                        'date' => $day,
                        'block' => $block,
                        'processed' => 0,
                        'balance_id' => $balance['rows'][0]->id,
                        'event_id' => $e->id,
                    ]);
                }
            } catch (\Exception $e) {
                DB::rollback();
                throw new Exception('<strong>Error.</strong> Fallo al guardar los dias. |' . $e->getMessage());
            }

            try {
                $u = $e->user;
                $b = $user->getAuthorizator();
                $r = Incident::create(['user_id' => $b->id, 'from_id' => $u->id, 'benefit_id' => $e->benefit_id, 'event_id' => $e->id, 'week' => date("W", $e->start), 'time' => time(), 'amount' => $e->benefit->days, 'info' => $e->title, 'comment' => '', 'status' => $incident_status, 'value' => 0, 'incapacity_id' => 0]);
            } catch (\Exception $e) {
                DB::rollback();
                throw new Exception('<strong>Error.</strong> No se pudo crear la incidencia');
            }
            DB::commit();
        } else {
            
            if ($balance['diff'] < $days)
                throw new Exception('<strong>Error.</strong> No puede solicitar más días de los que se le otorgaron.');

            if ($startDay === $endDay) {
                $js = ['total' => $days];
                $i = 0;
                $rowBalances = $balance['rows'];
                //Cuando solo tienes una linea de balance de X beneficio y tienes dias disponibles
                $underDateRange = false;

                foreach ($rowBalances as $key => $rowBalance) {
                    if ($rowBalance->amount < $rowBalance->pending) {
                        $started = "";
                        if (is_null($rowBalance->until)) {
                            $anioVac = $rowBalance->year;
                            $started = $user->employee->ingreso;
                            $started = strtotime(str_replace('-', '/', $started));
                            $started = strtotime(date('d-m-' . $anioVac, $started));
                            $started = strtotime(' +18 month', $started);
                        }
                        if ($e->start < strtotime($rowBalance->until) || $e->start < $started) {
                            $rowBalance->amount += 1;
                            unset($rowBalance->diff);
                            unset($rowBalance->solicited);

                            $js['balances'][] = [
                                'id' => $rowBalances[$i]->id,
                                'date' => $e->getStartDate('Y-m-d'),
                                'amount' => 1,
                            ];

                            $underDateRange = true;
                            $e->js = json_encode($js);

                            DB::beginTransaction();
                            try {
                                $rowBalance->save();
                            } catch (Exception $e) {
                                DB::rollback();
                                throw new Exception('<strong>Error.</strong> No se puedo actualizar los balances');
                            }
                            try {
                                $e->save();
                            } catch (\Exception $e) {
                                DB::rollback();
                                throw new Exception('<strong>Error.</strong> No se puedo crear el evento');
                            }
                            try {
                                $u = $e->user;
                                $b = $user->getAuthorizator();
                                $r = Incident::create(['user_id' => $b->id, 'from_id' => $u->id, 'benefit_id' => $e->benefit_id, 'event_id' => $e->id, 'week' => date("W", $e->start), 'time' => time(), 'amount' => $e->getDays(), 'info' => $e->title, 'comment' => '', 'status' => $incident_status, 'value' => 0, 'incapacity_id' => 0]);
                            } catch (\Throwable $e) {
                                DB::rollback();
                                throw new Exception('<strong>Error.</strong> No se puedo crear la incidencia.  |  ' . $e->getMessage());
                            }

                            foreach ($js['balances'] as $key => $line) {
                                try {
                                    EventDay::create([
                                        'date' => $line['date'],
                                        'processed' => 0,
                                        'balance_id' => $line['id'],
                                        'event_id' => $e->id,
                                    ]);
                                } catch (\Throwable $th) {
                                    throw new Exception('<strong>Error.</strong> Fallo al guardar los dias. |' . $th->getMessage());
                                }
                            }
                            DB::commit();
                            break;
                        } else {
                            throw new Exception('<strong>Error.</strong> No puede solicitar días fuera de la vigencía.');
                        }
                    }
                }
                if (!$underDateRange) {
                    throw new Exception('<strong>Error.</strong> El dia esta fuera del alcance en el que puedes pedir.');
                }
            } else {          
                $daysAvailables = self::getAvailableDays($e, $user);

                if (!$daysAvailables) {
                    throw new Exception('<strong>Error.</strong> Los dias que seleccionastes no laboras.');
                }
                $rowBalances = $balance['rows'];

                $val = false;
                $cont = 0;
                for ($i = 0; $i < count($rowBalances); $i++) {
                    $cont += $rowBalances[$i]->pending - $rowBalances[$i]->amount;
                }
                if ($cont < count($daysAvailables)) {
                    throw new Exception('<strong>Error.</strong> No puede solicitar más días de los que se le otorgaron.');
                }
                $i = 0;
                DB::beginTransaction();
                for ($j = 0; $j < count($daysAvailables); $j++) {
                    $started = null;
                    if ($rowBalances[$i]->amount < $rowBalances[$i]->pending) {
                        if (is_null($rowBalances[$i]->until)) {
                            $anioVac = $rowBalances[$i]->year;
                            $started = $user->employee->ingreso;
                            $started = strtotime(str_replace('-', '/', $started));
                            $started = strtotime(date('d-m-' . $anioVac, $started));
                            $started = strtotime(' +18 month', $started);
                        }

                        if (strtotime($daysAvailables[$j]) < strtotime($rowBalances[$i]->until)
                            || strtotime($daysAvailables[$j]) < $started) {
                            $rowBalances[$i]->amount += 1;
                            unset($rowBalances[$i]->diff);
                            unset($rowBalances[$i]->solicited);
                            try {
                                $rowBalances[$i]->save();
                            } catch (Exception $e) {
                                DB::rollback();
                                throw new Exception('<strong>Error.</strong> No se puedo actualizar una linea de balance |' . $e->getMessage());
                            }

                            $js['balances'][] = [
                                'id' => $rowBalances[$i]->id,
                                'date' => $daysAvailables[$j],
                                'amount' => 1,
                            ];
                            $val = true;

                        } else {
                            throw new Exception('<strong>Error.</strong> No puede solicitar días fuera de la vigencía.');
                        }
                    } else if ($i < count($rowBalances)) {
                        $i++;
                        $j--;
                    }
                }
                if (!$val) {
                    throw new Exception('<strong>Error.</strong> El dia esta fuera del alcance en el que puedes pedir. |' . $e->getMessage());
                } else {
                    $e->js = json_encode($js);
                    try {
                        $e->save();
                    } catch (\Exception $e) {
                        DB::rollback();
                        throw new Exception('<strong>Error.</strong> No se pudo crear el evento |' . $e->getMessage());
                    }

                    foreach ($js['balances'] as $key => $line) {
                        try {
                            EventDay::create([
                                'date' => $line['date'],
                                'processed' => 0,
                                'balance_id' => $line['id'],
                                'event_id' => $e->id,
                            ]);
                        } catch (\Throwable $th) {
                            throw new Exception('<strong>Error.</strong> Fallo al guardar los dias. |' . $th->getMessage());
                        }
                    }

                    try {
                        $u = $e->user;
                        $b = $user->getAuthorizator();
                        $r = Incident::create(['user_id' => $b->id, 'from_id' => $u->id, 'benefit_id' => $e->benefit_id, 'event_id' => $e->id, 'week' => date("W", $e->start), 'time' => time(), 'amount' => count($daysAvailables) /*$e->getDays()*/
                            , 'info' => $e->title, 'comment' => '', 'status' => $incident_status, 'value' => 0, 'incapacity_id' => 0]);
                    } catch (\Exception $e) {
                        DB::rollback();
                        throw new Exception('<strong>Error.</strong> No se pudo crear la incidencia |' . $e->getMessage());
                    }
                }
                DB::commit();
            }
        }
        try {
            Mail::to($b->email)->send(new EmployeeRequest($e));
        } catch (\Exception $e) {
            throw new Exception('Solicitud creada.<strong>Advertencia.</strong> No se puedo enviar correo a tu supervisor. Intenta contactarlo para informar  |' . $e->getMessage());
        }
        try {
            Mail::to($user->email)->send(new PleaCreated($e));
        } catch (\Exception $e) {
            throw new Exception('Solicitud creada.<strong>Advertencia.</strong> No se pudo enviarte correo de confirmación |' . $e->getMessage());
        }
        
        return $msg;
    }

    public static function destroyPlea(Incident $incident, $incident_status = 'canceled'){
        if ($incident->status == 'processed' || $incident->status == 'processing') {
            throw new Exception('Error: Solicitud procesada.');
        } else if ($incident->status == 'canceled') {
            throw new Exception('Error: Solicitud  cancelada.');
        }

        //$days = $incident->event->getDays(true);
        $days = $incident->event->getActiveDayCount();

        $incident->status = $incident_status;
        $incident->event->status = 'canceled';
        DB::beginTransaction();
        try {
            $incident->event->save();
        } catch (Exception $e) {
            DB::rollback();
            throw new Exception('Error al guardar el evento  |  ' . $e->getMessage());
        }
        try {
            $incident->save();
        } catch (Exception $e) {
            DB::rollback();
            throw new Exception('Error al guardar la incidencia  |  ' . $e->getMessage());
        }

        $balance = EmployeeController::getBalanceDetail($incident->event->benefit_id, $incident->event->user_id);
        unset($balance['rows'][0]->diff);
        unset($balance['rows'][0]->solicited);
        if ($balance['benefit']->type == "pending") {
            $balance['rows'][0]->amount -= $days;
            try {
                /** Jalada de código que hicieron por usar json en la base de datos
                 *  PleaController::restoreDays($incident->event);
                 *  Evitar esta función preferentemente
                 */
                $balance['rows'][0]->save();
            } catch (Exception $e) {
                DB::rollback();
                throw new Exception('Error al recuperar los dias.  |  ' . $e->getMessage());
            }

        } else {
            $balance['rows'][0]->pending -= $days;
            try {
                $balance['rows'][0]->save();
            } catch (Exception $e) {
                DB::rollback();
                throw new Exception('Error al recuperar los dias.  |  ' . $e->getMessage());
            }

        }
        DB::commit();
        return true;
    }

    public static function getEmployeeTimeBalance($user, $benefit = null){
        $user = self::getUser($user);

        $benefits = Benefits::whereHas('jobBenefits', function($q) use($user){
            $q->where('id', $user->getJobPositionID());
        })
        ->where('type', 'time')
        ->where('context', 'user')
        ->get();

        foreach($benefits as $benefit){
            $time = Event::where('user_id', $user->id)
            ->where('benefit_id', $benefit->id)
            ->statusActive()
            ->sum('time');

            $benefit->time = $time;
        }

        return $benefits;
    }

}