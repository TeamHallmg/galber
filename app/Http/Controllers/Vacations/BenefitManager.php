<?php

namespace App\Http\Controllers\Vacations;

use App\Models\Vacations\Benefits;

class BenefitManager{

    public function getAbsentBenefit(){
        $benefit = Benefits::where('name', 'Falta No Justificada')
        ->where('group', 'incident')
        ->first();
        return $benefit;
    }

    public function getVacationBenefit(){
        $benefit = Benefits::where('name', 'Vacaciones')
        ->where('group', 'benefit')
        ->first();
        return $benefit;
    }
}