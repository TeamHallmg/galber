<?php

namespace App\Http\Controllers\Vacations;

use App\User;
use Carbon\Carbon;
use App\Models\Schedule;
use Illuminate\Http\Request;
use App\Models\WorkshiftFixed;
use App\Models\Vacations\Event;
use App\Models\Vacations\Balances;
use App\Models\Vacations\Benefits;
use App\Models\Vacations\ClockLog;
use App\Models\Vacations\Incident;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Helpers\ClockLogManager;
use App\Http\Requests\ManageLogsRequest;

class ClockLogController extends Controller
{

    protected $clocklogManager;
    protected $today;

    public function __construct(ClockLogManager $clocklogManager){
        $this->clocklogManager = $clocklogManager;
        $this->today = Carbon::now();
    }

    public function today(){
        $employees = Auth::user()->getSubordinatesAsUsersIDs();
        $regions = Auth::user()->getSubordinatesRegionIDs();

        $today = $this->today->format('Y-m-d');
        $users = User::whereIn('id', $employees)
        ->with(['clocklogs' => function($q) use($today){
            $q->whereDate('date', $today);
        },'employee.region'])
        ->get();

        $workshiftFixed = WorkshiftFixed::with('schedule')
        ->where('labor', 1)
        ->where('day', date('D'))
        ->has('region')
        ->has('schedule')
        ->get();

        $_incidents = Event::with('benefit')
        ->has('benefit')
        ->whereIn('status', ['accepted', 'processing', 'processed'])
        ->whereIn('user_id', $employees)
        ->where('start', '>=', strtotime($this->today->format('Y-m-d')))
        ->where('end', '<=', strtotime($this->today->format('Y-m-d')))
        ->get();

        $incidents = [];
        foreach($_incidents as $incident){ $incidents[$incident->user_id] = $incident; }

        $_events = Event::where('status', 'global')
        ->whereIn('region_id', $regions)
        ->where('start', '>=', strtotime($this->today->format('Y-m-d')))
        ->where('end', '<=', strtotime($this->today->format('Y-m-d')))
        ->get();

        $events = [];
        foreach($_events as $event){ $events[$event->region_id] = $event; }
        
        $workshifts = [];
        foreach($workshiftFixed as $shift){
            $workshifts[$shift->region_id]['in'] = $shift->schedule->in;
            $workshifts[$shift->region_id]['out'] = $shift->schedule->out;
            $workshifts[$shift->region_id]['require_check'] = $shift->schedule->require_check;
        }

        $clocklogs = [];
        foreach ($users as $user) {
            $work = $user->isDayWorkable($today);
            $user->workable = $work;
            $user->tooltip = false;
            if(!$user->workable) $user->not_require_msg = "El empleado no labora este día";
            if(isset($workshifts[$user->employee->region_id])){
                if(isset($incidents[$user->id])){
                    if($incidents[$user->id]->benefit->type === 'time') {
                        $user->require_check = true;
                        $user->tooltip = true;
                    }
                    $user->not_require_msg = "Hay un solicitud del empleado en este día - " 
                    . $incidents[$user->id]->FullDescription;
                }elseif( isset($events[$user->employee->region_id]) ){
                    $user->workable = false;
                    $user->not_require_msg = "Hay un evento en este día -- " . $events[$user->employee->region_id]->FullDescription;
                }
            }
            $clocklogs[$user->id]['user'] = $user;
            foreach($user->clocklogs as $log){
                $clocklogs[$user->id][$log->type] = $log;
            }
        }
        return view('vacations.supervisor.clocklog.today', compact('clocklogs', 'workshifts'));
    }

    public function review_problems(){
        $employees = Auth::user()->getSubordinatesAsUsersIDs();
        $clocklogs = ClockLog::with('user.employee.region')
        ->whereDate('date', '<', $this->today->format('Y-m-d'))
        ->whereIn('user_id', $employees)
        ->has('user')
        ->where('status', 'pending')
        ->where(function($q){
            $q->noInfo()
            ->orWhere(function ($q1){
                $q1->arriveLate();
            })
            ->orWhere(function ($q1){
                $q1->leaveEarly();
            });
        })
        ->orderByRaw('Date(date) DESC')
        ->get();

        $dates = ClockLog::selectRaw('DATE_FORMAT(Date(date), "%d/%m/%Y") as "date"')
        ->whereDate('date', '<', $this->today->format('Y-m-d'))
        ->whereIn('user_id', $employees)
        ->has('user')
        ->where('status', 'pending')
        ->where(function($q){
            $q->noInfo()
            ->orWhere(function ($q1){
                $q1->arriveLate();
            })
            ->orWhere(function ($q1){
                $q1->leaveEarly();
            });
        })
        ->orderByRaw('DATE_FORMAT(Date(date), "%d/%m/%Y") DESC')
        ->groupBy(DB::raw('DATE_FORMAT(Date(date), "%d/%m/%Y")'))
        ->get()->pluck('date')->toArray();

        $benefits = Benefits::where('group', 'incident')
        ->where('context', 'super')
        ->whereIn('type', ['pending', 'day'])
        ->get();

        return view('vacations.supervisor.clocklog.logs', compact('clocklogs', 'dates', 'benefits'));
    }

    public function approve_clock_incidents(ManageLogsRequest $request){
        if($request->ajax()){
            $log = ClockLog::findOrFail($request->log_id);
            DB::beginTransaction();
            try {
                $log->comments()->create([
                    'comment' => $request->log_reason,
                ]);
                $log->status = 'accepted';
                $log->save();
            } catch (\Throwable $th) {
                DB::rollback();
                return response()->json(['success' => false, 'msg' => $th->getMessage()], 200);
            }
            DB::commit();
            return response()->json(['success' => true, 'msg' => 'Autorizado correctamente!!!'], 200);
        }
    }

    public function create_abscent_incident(ManageLogsRequest $request){
        if($request->ajax()){
            $log = ClockLog::findOrFail($request->log_id);
            $date = date('Y-m-d', strtotime($log->date));
            $logs = ClockLog::whereDate('date', $date)
            ->where('user_id', $log->user_id)
            ->get();
        
            $absentBenefit = null;
            if(isset($request->incident_benefit) && $request->incident_benefit){
                $absentBenefit = Benefits::findOrFail($request->incident_benefit);
            }
            if($absentBenefit || ($absentBenefit = $this->clocklogManager->getAbsentBenefit())){
                DB::beginTransaction();
                $user = User::with('employee.region', 'employee.boss.user')->has('employee.region')->has('employee.boss.user')->find($log->user_id);
                $timeToday = strtotime($log->date);
                $boss = $user->employee->boss->user->id;
                try {
                    $balance = Balances::firstOrCreate([
                        'year' => date('Y') + 1,
                        'benefit_id' => $absentBenefit->id,
                        'user_id' => $user->id,
                    ],[
                        'amount' => 0,
                        'pending' => 0,
                        'until' => null,
                    ]);

                    $balance->increment('pending');

                    $event = Event::create([
                        'title' => $request->log_reason,
                        'start' => $timeToday,
                        'end' => $timeToday,
                        'url' => '#',
                        'js' => '',
                        'blocked' => 0,
                        'status' => 'accepted',
                        'type' => 'user',
                        'region_id' => $user->employee->region_id,
                        'user_id' => $user->id,
                        'benefit_id' => $absentBenefit->id,
                    ]);

                    $event->eventDays()->create([
                        'date' => $event->getStartDate('Y-m-d'),
                        'processed' => 0,
                        'balance_id' => $balance->id,
                    ]);
                    
                    $incident = $event->incident()->create([
                        'user_id' => $boss,
                        'from_id' => $user->id,
                        'benefit_id' => $absentBenefit->id,
                        'week' => date('w', $timeToday),
                        'time' => $timeToday,
                        'amount' => 1,
                        'info' => $request->log_reason,
                        'status' => 'accepted',
                        'value' => 0,
                        'manager' => 0,
                        'incapacity_id' => 0,
                    ]);
                } catch (\Throwable $th) {
                    DB::rollback();
                    return response()->json(['success' => false, 'msg' => $th->getMessage()], 200);
                }

                foreach($logs as $log){
                    $log->status = 'rejected';
                    try {
                        $log->comments()->create([
                            'comment' => $request->log_reason,
                        ]);
                        $log->save();
                    } catch (\Throwable $th) {
                        DB::rollback();
                        return response()->json(['success' => false, 'msg' => $th->getMessage()], 200);
                    }
                }
                DB::commit();
                return response()->json(['success' => true, 'msg' => 'Autorizado correctamente!!!'], 200);
            }else{
                return response()->json(['success' => false, 'msg' => 'No se puede crear la falta!!!'], 200);
            }
        }
    }

    public function missing_logs(){
        $employees = Auth::user()->getSubordinatesAsUsersIDs();

        $clocklogs = [];
        $dates = [];
        $incidents = [];
        $benefits = [];
        if($absentBenefit = $this->clocklogManager->getAbsentBenefit()){
            $incidents = Incident::with('from')
            ->where('user_id', Auth::user()->id)
            ->where('benefit_id', $absentBenefit->id)
            ->where('status', 'pending')
            ->has('from')
            ->get();

            $dates  = Incident::selectRaw('FROM_UNIXTIME(time, "%d/%m/%Y") as date')
            ->where('user_id', Auth::user()->id)
            ->where('benefit_id', $absentBenefit->id)
            ->where('status', 'pending')
            ->has('from')
            ->groupBy(\DB::raw('FROM_UNIXTIME(time, "%d/%m/%Y")'))
            ->get()->pluck('date')->toArray();

            $benefits = Benefits::where('group', 'incident')
            ->where('context', 'super')
            ->whereIn('type', ['pending', 'day'])
            ->get();
        }
        return view('vacations.supervisor.clocklog.missing', compact('incidents', 'dates', 'absentBenefit', 'benefits'));
    }

    public function manage_absents(Request $request){
        if($request->ajax()){
            $incident = Incident::with('event', 'from')->has('event')->has('from')->find($request->incident_id);
            //Acepto la falta del empleado en el día seleccionado
            if($request->incident_status === 'accepted'){
                DB::beginTransaction();
                try {
                    $incident->status = "accepted";
                    $incident->event->status = "accepted";
                    if(isset($request->incident_benefit) && $request->incident_benefit){
                        $incident->benefit_id = $request->incident_benefit;
                        $incident->event->benefit_id = $request->incident_benefit;
                    }

                    $incident->event->save();
                    $incident->save();

                    DB::commit();
                } catch (\Throwable $th) {
                    DB::rollback();
                    return response()->json(['success' => false, 'msg' => $th->getMessage()], 200);
                }
                // Cancelo las checadas generadas
                $logs = ClockLog::where('user_id', $incident->from->id)
                ->whereDate('date', $incident->getTimeOnFormat())
                ->get();
                try {
                    foreach ($logs as $log) {
                        $log->status = 'canceled';
                        $log->save();
                        $log->comments()->create([
                            'comment' => $request->incident_reason,
                        ]);
                    }
                } catch (\Throwable $th) {
                    DB::rollback();
                    return response()->json(['success' => false, 'msg' => 'No se pudieron cancelar los registros de checadas | ' . $th->getMessage()], 200);
                }
            // Cancelo la falta y le genero las checadas en tiempo y forma al empleado
            }else{
                // Consigo los "types" de las checadas faltantes "in", "out" en un arreglo (Puede ser cualquiera de sus combinaciones);
                $missing = $this->clocklogManager->getMissingLogsType($incident->getTimeOnFormat(), $incident->from_id);
                $dayDate = date('D', strtotime($incident->time));
                $user = User::with(['employee.region.workshiftFixedDay' => function($q) use($dayDate){
                    $q->where('day', $dayDate)
                    ->with('schedule');
                }])
                ->whereHas('employee.region.workshiftFixedDay', function($q) use($dayDate){
                    $q->where('day', $dayDate)
                    ->has('schedule');
                })->find($incident->from_id);

                $schedule = $user->employee->region->workshiftFixedDay->schedule;

                DB::beginTransaction();
                try {
                    $incident->comment = $request->incident_reason;
                    $incident->status = "canceled";
                    $incident->event->status = "canceled";

                    $incident->event->save();
                    $incident->save();
                } catch (\Throwable $th) {
                    DB::rollback();
                    return response()->json(['success' => false, 'msg' => 'No se pudo cancelar la falta' . $th->getMessage()], 200);
                }

                foreach ($missing as $type) {
                    try {
                        $log = ClockLog::create([
                            'date' => $incident->getTimeOnFormat() . ' ' . $schedule->$type . ':00',
                            'user_id' => $user->id,
                            'from' => 'system',
                            'compare_time' => $schedule->$type,
                            'arrive_status' => 'on-time',
                            'type' => $type,
                            'status' => 'accepted',
                        ]);

                        $log->comments()->create([
                            'comment' => $request->incident_reason,
                        ]);
                    } catch (\Throwable $th) {
                        DB::rollback();
                        return response()->json(['success' => false, 'msg' => 'No se pudo generar la checada' . $th->getMessage()], 200);
                    }
                }
            }
            DB::commit();
            return response()->json(['success' => true, 'msg' => 'Actualizado correctamente!!!'], 200);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clocklogs = [];
        $workshift = [];
        $schedules = [];
        $logs = Auth::user()->clocklogs()->orderBy('date', 'ASC')->get();
        $employee = Auth::user()->employee;
        
        if($employee){
            if($employee->region->workshift === "5x2"){
                $workshift = WorkshiftFixed::with('schedule')
                ->where('region_id', $employee->region->id)
                ->where('labor', 1)
                ->get();

                $schedules = Schedule::with('workshiftsFixed')->whereHas('workshiftsFixed', function($q) use($employee){
                    $q->region_id = $employee->region->id;
                })->get();
            }
        }
        
        $clocklogs = Auth::user()->clocklogs()->orderBy('date')->get();

        return view('vacations.clocklog.index', compact('clocklogs', 'workshift', 'schedules'));
    }

    public function supervisor()
    {
        $employees = Auth::user()->getSubordinatesAsUsersIDs();
        $logs = ClockLog::with('user')
        ->whereIn('user_id', $employees)
        ->where('status', 'pending')
        ->orderBy('date', 'DESC')
        ->get();

        // $absentLogs = $this->clocklogManager->generateAbsentsRecords($employees);

        return view('vacations.supervisor.clocklog.index', compact('logs'));
    }

    public function history()
    {
        $employees = Auth::user()->getSubordinatesAsUsersIDs();
        $logs = ClockLog::with('user', 'comments')
        ->whereIn('user_id', $employees)
        ->orderBy('date', 'DESC')
        ->get();

        return view('vacations.supervisor.clocklog.history', compact('logs'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
