<?php

namespace App\Http\Controllers\Vacations;

use App\User;
use Illuminate\Http\Request;
use App\Models\Vacations\Incident;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\IncidentRequest;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\Vacations\PleaController;

class IncidentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Auth::user()->getSubordinatesAsUsersIDs();
        $users = User::with('employee')
        ->has('employee')
        ->whereIn('id', $employees)
        ->get();

        return view('vacations.supervisor.incidents.index', compact('users'));
    }

    public function show($id){
        $employee_number = Auth::user()->employee?Auth::user()->employee->idempleado:null;

        $user = User::whereHas('employee', function($q) use($employee_number){
            $q->where('jefe', $employee_number);
        })
        ->where('id', $id)
        ->first();

        if(!$user) return redirect()->route('super.user.incidents.index');

        $data = EmployeeController::getEmployeeForSuperBalances($user->id);
        $data['user'] = $user;
        return view('vacations.supervisor.incidents.show', $data);
    }

    public function store(IncidentRequest $request, $id){
        $user = User::findOrFail($id);
        return PleaController::storeRequest($request->validated(), $user, 'super.user.incidents.show', 'accepted');
    }

    public function review($id){
        $employee_number = Auth::user()->employee?Auth::user()->employee->idempleado:null;

        $user = User::whereHas('employee', function($q) use($employee_number){
            $q->where('jefe', $employee_number);
        })
        ->where('id', $id)
        ->first();

        if(!$user) return redirect()->route('super.user.incidents.index');

        $incidents = Incident::whereHas('benefit', function($q){
            $q->whereIn('context', ['super', 'admin'])
            ->whereIn('group', ['incident', 'incapacity']);
        })
        ->whereIn('status', ['pending', 'complete'])
        ->where('from_id', $user->id)
        ->get();

        return view('vacations.supervisor.incidents.review', compact('incidents', 'user'));
    }

    public function update(Request $request, $id){
        $data   = $request->all();
        $_incidents = isset($request->incidents)?$request->incidents:[];
        $id_incidents = array_keys($_incidents);
        $incidents = Incident::whereIn('id', $id_incidents)
        ->where('from_id', $id)
        ->get();

        foreach($incidents as $incident){
            if ( !isset($_incidents[$incident->id]) ) continue;
            switch($_incidents[$incident->id]){
                case -1:
                    //Ignore
                break;
                case 0:
                    //reject
                    \DB::beginTransaction();

                    $incident->event->status = 'canceled';
                    $incident->status = 'rejected';
                    try {
                        $incident->event->save();
                        $incident->save();
                    } catch (\Throwable $th) {
                        DB::rollback();
                        continue;
                    }
                    try {
                        $days = $incident->event->getDays();
                        $balance = EmployeeController::getBalanceDetail($incident->event->benefit_id, $incident->event->user_id);
                        if($balance['benefit']->type == "pending") {
                            $js = PleaController::restoreDays($incident->event);
                        } else {
                            $balance['rows'][0]->pending -= $days;
                            unset($balance['rows'][0]->diff);
                            unset($balance['rows'][0]->solicited);
                            $balance['rows'][0]->save();
                        }
                    } catch (\Throwable $th) {
                        DB::rollback();
                    }
                    DB::commit();
                break;
                case 1:
                    //accept
                    $incident->value = 1;
                    $incident->event->status = 'accepted';
                    $incident->status = 'complete';
                    try {
                        DB::beginTransaction();
                        $incident->event->save();
                        $incident->save();   
                        DB::commit();
                    } catch (Exception $e) {
                        DB::rollback();
                    }
                break;
            }
        }

        flash('Las peticiones han sido guardadas.','success');
        return redirect()->route('super.user.incidents.review', $id);
    }
}
