<?php

namespace App\Http\Controllers\Vacations;

use App\Models\JobPosition;
use Illuminate\Http\Request;
use App\Models\Vacations\Benefits;
use App\Http\Controllers\Controller;
use App\Models\Vacations\BenefitJob;
use App\Http\Requests\BenefitJobRequest;

class BenefitJobController extends Controller
{
    public function __construct(){
        $this->middleware('permission:incidents_admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = JobPosition::with('jobBenefits', 'area.department.direction')
        ->has('jobBenefits')
        ->get();

        return view('vacations.admin.benefitjob.index', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobs = JobPosition::with('area.department.direction')
        ->whereDoesntHave('jobBenefits')
        ->orderBy('name', 'ASC')
        ->get();

        $benefits = Benefits::where('context', 'user')
        ->where('group', 'benefit')
        ->orderBy('code', 'ASC')
        ->get();

        return view('vacations.admin.benefitjob.create', compact('jobs', 'benefits'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BenefitJobRequest $request)
    {
        $job = JobPosition::find($request->job_position_id);
        try {
            $job->jobBenefits()->sync($request->benefits);
        } catch (\Throwable $th) {
            return redirect()->route('admin.benefitjobs.create')->withInput()->withErrors(['msg' => 'No se pudo asginar los beneficios | ' . $th->getMessage()]);
        }
        return redirect()->route('admin.benefitjobs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = JobPosition::with('jobBenefits', 'area.department.direction')
        ->has('jobBenefits')
        ->where('id', $id)
        ->first();

        $job->jobBenefits = $job->jobBenefits->pluck('name', 'id')->toArray();

        $benefits = Benefits::where('context', 'user')
        ->orderBy('code', 'ASC')
        ->get();

        return view('vacations.admin.benefitjob.edit', compact('job', 'benefits'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BenefitJobRequest $request, $id)
    {
        $job = JobPosition::find($request->job_position_id);
        try {
            $job->jobBenefits()->sync($request->benefits);
        } catch (\Throwable $th) {
            dd($th);
        }
        return redirect()->route('admin.benefitjobs.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job = JobPosition::findOrFail($id);
        try {
            $job->jobBenefits()->detach();
            return response()->json(['success' => true], 200);
        } catch (\Throwable $th) {
            return response()->json(['success' => false, 'error' => $th->getMessage()], 200);
        }
    }
}
