<?php

namespace App\Http\Controllers;
use App\Models\Direction;
use App\Models\DeletedReason;
use App\Models\JobPosition;
use App\Models\Department;
use App\Models\Area;

use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

use Illuminate\Http\Request;

class DirectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $directions = Direction::select('id','name','description')->orderBy('id', 'DESC')->get();

        return view('directions.index', compact(['directions']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $trashed = Direction::select('id','name','description')->orderBy('id', 'ASC')->onlyTrashed()->get();
        return view('directions.create', compact('trashed'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required|unique:directions',
            'description' => 'max:500',
        );

        $error = Validator::make($request->all(), $rules);
        if($error->fails()){
            return redirect()->back()->withErrors(['errors' => $error->errors()->all()]);
        }

        $form_data = array(
            'name'  =>  '',
            'description'  =>  $request->description,
        );

        if($request->name == -1) {
            $form_data['name'] = $request->name_add;
            Direction::create($form_data);
        } else {
            Direction::withTrashed()->find($request->name)->restore();
            $restored = Direction::find($request->name);
            $restored->description = $form_data['description'];
            $restored->save();
        }

        return redirect()->route('direcciones.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    
        $direction = Direction::where('id',$id)->first();

        return view('directions.edit', compact(['direction']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = array(
            'name' => 'required',
            'description' => 'max:500',
        );

        $error = Validator::make($request->all(), $rules);
        if($error->fails()){
            return redirect()->back()->withErrors(['errors' => $error->errors()->all()]);
        }

        try {
            
            $form_data = array(
                'name'  =>  $request->name,
                'description'  =>  $request->description,
            );
            
            if($request->direction == null){

                $direction = Direction::createCascade($request->name);

            }

        } catch (\Throwable $th) {
            //dd($th);
        }


        $form_data = array(
            'name'  =>  $request->name,
            'description'  =>  $request->description,
        );

        Direction::where('id',$id)->update($form_data);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $direction = Direction::find($id);

            foreach($direction->department as $key => $department ) {
                foreach($department->areas as $key2 => $area) {
                    foreach($area->jobs as $key3 => $job) {
                        foreach($job->employees as $key4 => $employee) {
                            $employee->job_position_id = null;
                            $employee->save();
                        }
                        $job->delete();
                    }
                    $area->delete();
                }
                $department->delete();
            }

            $data = new DeletedReason();
            $data->user_id = Auth::user()->id;
            $data->reason = $request->del_reason;
            $direction->deleted_reason()->save($data);

            $direction->delete();
            DB::commit();
        } catch(\Throwable $th) {
            DB::rollback();
            dd($th->getMessage());
        }

        return redirect()->route('direcciones.index');
    }

    public function tree(){
        //return Direction::where('id',$id)->orderBy('id', 'ASC')->with('department.areas.jobs.employees')->get();
        return Direction::orderBy('id', 'ASC')->has('department.areas.jobs.employees')->get();
    }
}
