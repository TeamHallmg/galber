<?php

namespace App\Http\Controllers\Fixes;

use App\Employee;
use App\Models\Region;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FixingCronDataController extends Controller
{
    /**
     * Cambiar la información del campo "region", insertarla si es necesario en la base de datos
     * y actualizar el dato en "region_id"
     *
     * @return void
     */
    public function turnRegionIntoForeign(){
        $employees = Employee::all();
        $regions = [];
        foreach ($employees as $employee) {
            if(is_null($employee->region_id)){
                if(!isset($regions[$employee->region])){
                    try {
                        $region = Region::firstOrCreate([
                            'name' => $employee->region,
                        ]);
                        $regions[$employee->region] = $region->id;
                    } catch (\Throwable $th) {
                        dd($th->getMessage());
                    }
                }
                $employee->region_id = $regions[$employee->region];
				$employee->save();
            }
        }
    }
}
