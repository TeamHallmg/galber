<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Responselog;
use App\Models\Cronlog;

use App\Http\Requests;

class CronController extends Controller
{
    private function always()
    {
        //Rutines
        //Now testing
        /*$clock = new ClockController();
        $clock->process();
        EmployeeController::setPartials();
        EmployeeController::processVacations();
        EmployeeController::processComodin();
        EmployeeController::removeBenefits();*/
		/*$clock = new ClockController();
        if(config('config.fileclock')) {
            $clock->autoprocess();
        }
        $clock->process();*/
    }

    private function daily()
    {
      

                $currDay = strtotime(date('Y-m-d H:m:s'));
                $lastDayRun = Cronlog::where('type','daily')->orderBy('date','DESC')->first();
                
                if(!$lastDayRun){
                    $lastDayRun = $currDay;
                }else{                   
                    $lastDayRun = strtotime($lastDayRun->date);
                    $lastDayRun = strtotime('+1 day',$lastDayRun);
                }
                $notifications = [];
                while($lastDayRun <= $currDay)
                {
                    // EmployeeController::processTxt(); 
                    // Si el usuario no tiene balances del tipo vacaciones con la fecha 'until' en null, se crean registros nuevos con su proximo año vacacional y dias de vacaciones con fecha until en 'null', de lo contrario solo se actualizan las vacaciones de esos dias
                    // 
                    // Este le va actualizando las vacaciones proporcionalmente a los usuarios
                    // se lleva todo el getMyNextYear() del usuario, modificando y agregandoles dias, cuando el usuario cumple un año en escencia su unica linea con fecha 'until' en 'null', se le agregara fecha de caducacion, y al siguiente dia que se ejecute el cron se le creara una nueva linea de balances
                    // 
                    $mensajes = [];
                    $mensajes[] = EmployeeController::setPartials($lastDayRun);
                    // Actualiza el balance de todos los registros con fecha 'until' en null que cumplan un año trabajando
                    $mensajes[] = EmployeeController::processVacations($lastDayRun);//Actualiza los benefits que no tiene "Until"
                    EmployeeController::removeBenefits($lastDayRun);//Si la fecha "until" ya paso se borran
                    $clock = new ClockController();
                    //Se comenta el if ya que en el config para herba es en false (se comenta pero seria lo mismo) ya que los datos se traen de la DB y en avnet son de un archivo, el autoprocess es para insertar los datos a la tabla y luego procesarlos.
                    /*if(config('config.fileclock')) {
                      $clock->autoprocess();
                    }*/
                    $clock->process();
                    Cronlog::create(['date' => date('Y-m-d', $lastDayRun),'type' => 'daily']);

                    $notifications[] = [
                        'date' => date('Y-m-d', $lastDayRun),
                        'msj' => $mensajes,
                    ];
                    $lastDayRun = strtotime("+1 day",$lastDayRun);
                }
                if(!empty($notifications)){
                    // dd($notifications, $notifications[0]['msj']);
                    // try {
                    //     \Mail::send('emails.cronErrors', ['notifications' => $notifications], function ($message) {
                    //         $message->subject("Reporte de Errores en el Cron");
                    //         $message->to('victor.gonzalez@hallmg.com');
                    //     });
                    // } catch (\Exception $th) {
                    //     dd($th->getMessage());
                    // }
                }

                return "ok";
        
    }
    private function weekly($date)
    {
        //Rutines
        switch (config('config.name')) {
          /*case 'herbalife':
            $user = new UsersController();
            $user->process();
          break;

          case 'avnet':
            BenefitsController::process('weekly');
          break;*/
          default : 
                BenefitsController::process('weekly', $date);
                Cronlog::create(['date' => date('Y-m-d', $date),'type' => 'weekly']);
          break;
        }
    }
    private function monthly($date)
    {
        //Rutines
        //Quizas otras prestaciones
        BenefitsController::process('monthly',$date);
        Cronlog::create(['date' => date('Y-m-d', $date),'type' => 'monthly']);
        CronController::removelog();
    }
    private function yearly($date)
    {
        //Rutines
        switch (config('config.name')) {
          case 'herbalife':
            EmployeeController::fixVacations();
            EmployeeController::processComodin($date);
            Cronlog::insert(['date' => date('Y-m-d', $date), 'type' => 'yearly']);
          break;

          case 'avnet':
            BenefitsController::process('yearly');
          break;
        }
    }

    public function wasProcess($date,$type,$margin = null)
    {

                $cron = Cronlog::where('date',$date)
                ->where('type','daily')
                ->first();

                if(is_null($cron)){
                    $this->daily();
                }
            
        }

    public function index()
    {
        $now  = time();
        $time = date('H:i',$now);
        $today = date('Y-m-d');
        $time = '01:00';
        // dd($time);

            $this->wasProcess($today,'daily');
            // $this->wasProcess($today,'weekly',4);
            // $this->wasProcess($today,'monthly',7);
            // $this->wasProcess($today,'yearly',31);
            /*$day = date('D',$now);
            //$day = "Tue";
            if($day == "Tue") {
                $this->weekly();
            }

            $day = date('d',$now);
            //$day = 01;
            if($day == '01'){
                // $this->monthly();
            }

            // $day = date('z',$now);
            $day = 0;
            if($day == '0'){
                $this->yearly();
            }
            // $this->daily();*/
        // $this->always();
    }

    public static function removelog()
    {
        $time = date('Y-m-d H:i:s', strtotime('-3 months'));
        // Responselog::where('created_at','<',$time)->delete();
        // Errorlog::where('created_at','<',$time)->delete();
    }
}
