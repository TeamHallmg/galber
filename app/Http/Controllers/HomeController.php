<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Employee;
use App\EmployeeMovement;
use App\User;
use App\Models\Profile\Profile;
use App\Models\Announcement\Announcement;
use App\Models\Announcement\AnnouncementType;
use App\Models\Announcement\AnnouncementCategory;
use App\Models\Announcement\View;
use App\Models\JobPosition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Direction;
use App\Models\Department;
use App\Models\Area;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$view = View::where('name', 'home')->first();
        $carrousel = Announcement::getAnnouncementsToDisplay($view->id, 'carrousel');
        $mosaico = Announcement::getAnnouncementsToDisplay($view->id, 'mosaico');
        $slick = Announcement::getAnnouncementsToDisplay($view->id, 'slick');
        $display_announcements = compact('carrousel', 'mosaico', 'slick');
        return view('home', compact('display_announcements'));*/
        if (!empty(auth()->user()->external)){

            Session::put('primer_login', true);
            return redirect('/cambiar-contrasena/' . auth()->user()->id);
          }

        else{

          Session::forget('primer_login');
        }

        return view('evaluacion-desempeno/que-es-evaluacion-desempeno');
    }

    public function filosofia()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);

        $view = View::where('name',$url)->first();
        $panels = Announcement::getAnnouncementsToDisplay($view->id, 'panels');
        // $display_announcements = compact('banner','cards','banner_panel','panels','carrousel','carrousel_secondary','mosaico','tabla_anuncios','cuatro_columnas','por_categorias');
        $display_announcements = compact('panels');
        return view('filosofia', compact('display_announcements'));
    }

    public function recursividad(&$data, $users, &$list, $nivel){
        
        $nivelcolor=['#002C49','#0064A6','#ECC100','#4FB9FF','#FCF3CA'];

        $cont = [];
        $employees = [];
        foreach ($users as $u){
            $employees[$u->puesto] = (isset($employees[$u->puesto]))? $employees[$u->puesto] . $u->FullName . '<hr>': $u->FullName . '<hr>';
            $cont[] = $u->puesto;
        }
        $cont =  array_count_values($cont);
        $list .= '<ul>';
        foreach ($users as $user) {
            if(!isset($data[$user->puesto])){
                $data[$user->puesto] = [];
                $list .='<li id="open" class="encont"><a tabindex="0" class="btn btn-primary" style="background:'.$nivelcolor[$nivel].' !important;color:white; font-size:1rem; margin-bottom:10px;" data-toggle="popover" data-trigger="focus" title="<strong>Personal</strong>" data-content="'. $employees[$user->puesto] .'">' . $user->puesto . '  |  Plantilla: ' . $cont[$user->puesto] . '</a>';
            }
            if($user->employees->count() > 0){
                $this->recursividad($data[$user->puesto], $user->employees, $list, $nivel + 1);
            }
            if(!isset($data[$user->puesto])){
                $list .= '</li>';
            }
        }
        $list .= '</ul>';
    }

    public function organigramaPlantilla(){

        $totalPuestos = Employee::select('puesto')->groupBy('puesto')->get();
        $totalPuestos = $totalPuestos->count();

        $personas = [];
        $puestos = [];
        $areas = [];
        $totalPersonal = Employee::when(count($personas) > 0 , function($q){
            $q->whereNotIn('id', $personas);
        })
        ->when(count($puestos) > 0 , function($q){
            $q->whereNotIn('puestos', $puestos);
        })
        ->when(count($areas) > 0 , function($q){
            $q->whereNotIn('areas', $areas);
        })
        ->get();
        $totalPersonal = $totalPersonal->count();

        $topUsers = Employee::where('jefe', '')->orWhere('jefe', '0')->orWhere('jefe', null)->orderBy('nombre','DESC')
        ->with(['employees' => function($q){
            $q->orderBy('division')->orderBy('nombre','ASC');
        }])
        ->get();        

        $data = [];

        $list = '<ul class="treeview">';

        $cont = [];
        $employees = [];
        foreach ($topUsers as $u){
            $employees[$u->puesto] = (isset($employees[$u->puesto]))? $employees[$u->puesto] . $u->FullName . '<hr>': $u->FullName . '<hr>';
            $cont[] = $u->puesto;
        }
        $cont =  array_count_values($cont);
        foreach ($topUsers as $user) {
            if(!isset($data[$user->puesto])){// 
                $list .='<li id="open" class="encont"><a tabindex="1" class="btn btn-primary" style="background:#007AAB !important;color:white; font-size:1rem; margin-bottom:10px;" data-toggle="popover" data-trigger="focus" title="<strong>Personal</strong>" data-content="'. $employees[$user->puesto] .'">' . $user->puesto . '  |  Plantilla: ' . $cont[$user->puesto] . '</a>';
                $data[$user->puesto] = [];
            }            
            
            if($user->employees->count() > 0){
                $this->recursividad($data[$user->puesto], $user->employees, $list, 0);
            }
            if(!isset($data[$user->puesto])){
                $list .= '</li>';
            }
        }
        $list .= '</ul>';
        return view('/organigrama-plantilla',compact('list', 'totalPuestos', 'totalPersonal'));
    }

    public function organigrama(){
        $logUserComp = Auth::user()->empresa;
        $logUser = Auth::user()->email;
        $list = '';
        $image = '';
        //variable para definir el nivel de org, (para definir el color del items del menu)
        $nivel = 0;

        $boss_first = Employee::where('jefe', '')->orWhere('jefe', '0')->orderBy('nombre','DESC')->get();
        if(count($boss_first) > 0){
            
            $list = '<ul class="treeview">';
            foreach($boss_first as $boss){
                $image = 'img/vacantes/sinimagen.png';
                $user = User::where('employee_id', $boss->id)->first();
                $hobbies = 'N/A';
                // $department = $boss->fixed_department?$boss->fixed_department->name:'N/A';
                $department = (isset($boss->jobPosition->area->department)?$boss->jobPosition->area->department->name:'N/A');
                if($user){
                    $profile = Profile::where('user_id', $user->id)->first();
                    if($profile != null && !is_null($profile->image)){
                        $image = 'uploads/profile/'.$profile->image;
                        // $image = "'".$image."'";
                    }
                }
                
                // $list .='<li id="open" class="encont"><a tabindex="1" class="btn btn-light" style="background:#373435 !important;color:white;  font-size:1rem; margin-bottom:10px;" data-toggle="popover" data-trigger="focus" title="<img class=img-org src='.$image.'>" data-content="<strong>Nombre: </strong><br>'.$boss->nombre.' '.$boss->paterno.'<br><strong>Puesto:</strong><br>'.$boss->jobPosition->name.'<br><strong>Área:</strong><br>'.$boss->jobPosition->area->name.'<br><strong>Ubicación:</strong><br>N/A<br><strong>Teléfono: </strong><br>'.$boss->telefono.'<br><strong>e-mail: </strong><br>'.$boss->correoempresa.'">'.$boss->nombre.' '.$boss->paterno.' - '.(isset($boss->jobPosition->name)?$boss->jobPosition->name:'N/A').' - '.(isset($boss->jobPosition->area->department->name)?$boss->jobPosition->area->department->name:'N/A').'</a>';
                $list .='<li id="open" class="encont">
                <a tabindex="1" class="btn btn-primary" style="background:#007AAB !important;color:white;  font-size:1rem; margin-bottom:10px;" 
                data-toggle="popover" 
                data-content="
                    <img class=\'img-orgChart\' src=\''.asset($image).'\'>
                    <strong><div class=bg-yellow>'.$boss->getFullNameAttribute().'</div></strong>
                    <div>'.(isset($boss->jobPosition)?$boss->jobPosition->name:'N/A').'</div>
                    <div>'.$boss->sucursal.'</div>
                    <div>'.$boss->correoempresa.'</div>
                    <strong>Teléfono: </strong>'.($boss->telefono?$boss->telefono:'N/A').'<br>
                    <strong>Celular: </strong>'.($boss->celular?$boss->celular:'N/A').'<br>
                    <br>">'.$boss->getFullNameAttribute().' - '.(isset($boss->jobPosition)?$boss->jobPosition->name:'N/A').' - '.$department.'</a>';
                    // (isset($boss->jobPosition->area->department)?$boss->jobPosition->area->department->name:'N/A')
                $list .= $this->subBoss($boss->idempleado,$nivel); 
                $list .= '</li>';
                // (isset($boss->jobPosition->area)?$boss->jobPosition->area->name:'N/A')
            }
            $list .= '</ul>';
        }

        $titulo = 'ORGANIGRAMA';
        return view('organigrama',compact('list', 'titulo'));
    }

    private function subBoss($boss,$nivel){
        $list2 = '';
        $subBos = Employee::where('jefe', $boss)->orderBy('division')->orderBy('nombre','ASC')->get();
        //$nivelcolor=['#57B5E0','#8E0855','#E86EAD','#808285','#B8B8B8','#E86EAD'];
        // $nivelcolor=['#002C49','#0064A6','#ECC100','#4FB9FF','#FCF3CA','#002C49','#0064A6','#ECC100','#4FB9FF','#FCF3CA'];
        // $nivelcolor=['#ED3237', '#ACACAC', '#FEFEFE', '#F1696B'];
        $nivelcolor=['#002C49','#0064A6','#ECC100','#4FB9FF','#228ED1','#002C49','#0064A6','#ECC100','#4FB9FF','#228ED1'];
        //morado
        //$nivelcolor=['#280059','#3f0f7a','#500da3','#762cd1','#965ae0','#ad7ee6','#cba5fa','#ddc2ff'];
        //NARANJA
        //$nivelcolor=['#592e00','#854502','#9e5303','#bd670b','#d47917','#d98d3c','#eba65b','#fabf7f'];
        
        if(count($subBos) > 0){        
            $list2 .= '<ul>';
            foreach($subBos as $sub){
                $image = 'img/vacantes/sinimagen.png';
                $user = User::where('employee_id', $sub->id)->first();
                $hobbies = 'N/A';
                // $department = $sub->fixed_department?$sub->fixed_department->name:'N/A';
                $department = (isset($sub->jobPosition->area->department)?$sub->jobPosition->area->department->name:'N/A');
                if($user){
                    $profile = Profile::where('user_id', $user->id)->first();
                    if($profile != null && !is_null($profile->image)){
                        $image = 'uploads/profile/'.$profile->image;
                        // $image = "'".$image."'";
                    }
                }
                
                // $list2 .='<li class="encont"><a tabindex="0" class="label label-default badge" style="background:'.$nivelcolor[$nivel].' !important;color:white; font-size:1rem; margin-bottom:10px;" data-toggle="popover" data-trigger="focus" title="<img class=img-org src='.$image.'>" data-content="<strong>Sucursal: </strong>'.$sub->sucursal.'<hr>'.$sub->nombre.' '.$sub->paterno.'<hr><strong>Teléfono: </strong>'.$sub->telefono.'<hr><strong>Celular: </strong>'.$sub->celular.'<hr><strong>e-mail: </strong>'.$sub->correoempresa.'">'.$sub->nombre.' '.$sub->paterno.' - '.(isset($sub->jobPosition->name)?$sub->jobPosition->name:'N/A').' - '.(isset($sub->jobPosition->area->department->name)?$sub->jobPosition->area->department->name:'N/A').'</a>';
                    $list2 .='<li class="encont">
                <a tabindex="0" class="label label-default badge" style="background:'.$nivelcolor[$nivel].' !important;color:white; font-size:1rem; margin-bottom:10px;"
                    data-toggle="popover" 
                    data-content="<img class=\'img-orgChart\' src=\''.asset($image).'\'>
                    <strong><div class=bg-yellow>'.$sub->getFullNameAttribute().'</div></strong>
                    <div>'.(isset($sub->jobPosition)?$sub->jobPosition->name:'N/A').'</div>
                    <div>'.$sub->sucursal.'</div>
                    <div>'.$sub->correoempresa.'</div>
                    <strong>Teléfono: </strong>'.($sub->telefono?$sub->telefono:'N/A').'<br>
                    <strong>Celular: </strong>'.($sub->celular?$sub->celular:'N/A').'<br><br>
                ">'
                .$sub->getFullNameAttribute()
                .' - '. (isset($sub->jobPosition)?$sub->jobPosition->name:'N/A')
                // .' - '. (isset($sub->jobPosition->area)?$sub->jobPosition->area->name:'N/A')
                .' - '. $department .'</a>';
                // (isset($sub->jobPosition->area->department)?$sub->jobPosition->area->department->name:'N/A')
                $list2 .= $this->subBoss($sub->idempleado,$nivel+1);
                $list2 .= '</li>';
            }
            $list2 .= '</ul>';
        }
        return $list2;
    }

    private function subJobs($boss,$nivel){
        $list2 = '';
        $nivelcolor=['#002C49','#0064A6', '#007DCC', '#6699C2', '#D4D6D9', '#FFCC29','#E5AD36','#FF9900'];
        
        if(count($boss->jobs) > 0){
            $list2 .= '<ul>';
            foreach($boss->jobs as $job){
                $padding = strval(($job->level->hierarchy * 4 * ($job->level->hierarchy - $nivel)) + 10);
                $lvl = strval($job->level->hierarchy - $nivel);
                $list2 .='<li class="encont c'.$lvl.'" style="padding-left:' . $padding . 'px"><a tabindex="0" class="label label-default badge" style="background:'.$nivelcolor[$job->level->hierarchy - 1].' !important;color:white; font-size:1rem; margin-bottom:10px;" data-toggle="popover" data-trigger="focus" title="<strong>'.$job->name.'" data-content="<strong>Nombre: </strong>'.$job->name.'<hr>">'.$job->name.' | Level: ' . $job->level->hierarchy .'</a>';
                $list2 .= $this->subJobs($job,$nivel+1);
                $list2 .= '</li>';
            }
            $list2 .= '</ul>';
        }
        return $list2;
    }

    public function organigramaPuestos(){
        $jobs = JobPosition::with('jobs', 'level')->bossTop()->get();
        $nivelcolor=['#002C49','#0064A6', '#007DCC', '#6699C2', '#D4D6D9', '#FFCC29','#E5AD36','#FF9900'];
        // $nivelcolor=['#592e00','#854502','#9e5303','#bd670b','#d47917','#d98d3c','#eba65b','#fabf7f'];
        $list = '<ul class="treeview">';
        $nivel = 0;
        foreach ($jobs as $key => $job) {
            if(!isset($job->level->hierarchy)) {
                $padding = strval(1 * 10 + 2);
                $list .='<li class="encont" style="margin-left: '. $padding . 'px"><a tabindex="0" class="label label-default badge" style="background:'.$nivelcolor[1 - 1].' !important;color:white; font-size:1rem; margin-bottom:10px;" data-toggle="popover" data-trigger="focus" title="<strong>'.$job->name.'" data-content="<strong>Nombre: </strong>'.$job->name.'<hr>">'.$job->name.'</a>';
                $list .= $this->subJobs($job,$nivel+1);
                $list .= '</li>';
            } else {
                $padding = strval($job->level->hierarchy * 10 + 2);
                $list .='<li class="encont" style="margin-left: '. $padding . 'px"><a tabindex="0" class="label label-default badge" style="background:'.$nivelcolor[$job->level->hierarchy - 1].' !important;color:white; font-size:1rem; margin-bottom:10px;" data-toggle="popover" data-trigger="focus" title="<strong>'.$job->name.'" data-content="<strong>Nombre: </strong>'.$job->name.'<hr>">'.$job->name.' | Level: ' . $job->level->hierarchy .'</a>';
                $list .= $this->subJobs($job,$nivel+1);
                $list .= '</li>';
            }
        }
        $list .= '</ul>';
        $titulo = 'PLANTILLA DE PUESTOS';
        return view('/organigrama',compact('list', 'titulo'));
    }

    public function employeesMovement() {
        $current_year = Carbon::now()->year;
        $movements = EmployeeMovement::where('type', 'movement')->orderBy('created_at', 'desc')->whereHas('user', function($q) {
                $q->whereNull('deleted_at');
            })->whereYear('created_at', $current_year)->take(6)->get();
        $promotions = EmployeeMovement::where('type', 'promotion')->orderBy('created_at', 'desc')->whereHas('user', function($q) {
                $q->whereNull('deleted_at');
            })->whereYear('created_at', $current_year)->take(6)->get();

        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);
        $view = View::where('name',$url)->first();
        //Traemos todos los anuncios activos y que esten vigentes en cuanto a la fecha de inicio y fin     
        $banner = Announcement::getAnnouncementsToDisplay($view->id, 'banner');

        $display_announcements = compact('banner');

        return view('movimientos-personal', compact('display_announcements', 'movements', 'promotions'));
    }

    public function events(){
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);
        $view = View::where('name',$url)->first();
        //Traemos todos los anuncios activos y que esten vigentes en cuanto a la fecha de inicio y fin        
        $gallery = Announcement::getAnnouncementsToDisplay($view->id, 'gallery');
        $banner = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
        $display_announcements = compact('gallery', 'banner');
        return view('eventos', compact('display_announcements'));
    }

    public function offices() {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);

        $view = View::where('name',$url)->first();
        $mosaico_space = Announcement::getAnnouncementsToDisplay($view->id, 'mosaico_space');
        $banner = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
        $columns = 4;
        $display_announcements = compact('mosaico_space', 'banner','columns');
        // dd($display_announcements);
        return view('oficinas', compact('display_announcements'));
    }

    public function newEmployees() {
        setLocale(LC_TIME, 'Spanish');
        Carbon::setUtf8(true);
        
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);
        $view = View::where('name',$url)->first();
        //Traemos todos los anuncios activos y que esten vigentes en cuanto a la fecha de inicio y fin     
        $banner = Announcement::getAnnouncementsToDisplay($view->id, 'banner');

        $display_announcements = compact('banner');
        
        $users = Employee::whereNull('deleted_at')->whereHas('jobPosition')->whereHas('user')->orderBy('ingreso', 'desc')->take(6)->get();
		foreach ($users as $key => $employee) {
            $job = JobPosition::where('id', $employee->job_position_id)->first();
            if($job != null) {
                $employee->job_position_id = $job->name;
                if(isset($job->area->department)) {
                    $employee->department_name = $job->area->department->name;
                }
            }
            $employee->photo = $employee->user->getProfileImagen();
        }

        return view('nuevos-ingresos', compact('display_announcements', 'users'));
    }

    public function calendario(){
        setLocale(LC_TIME, 'Spanish');
		Carbon::setUtf8(true);
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);
        $view = View::where('name',$url)->first();
        // $directions = Direction::select('id','name','description')->orderBy('name', 'ASC')->get();
        //Traemos todos los anuncios activos y que esten vigentes en cuanto a la fecha de inicio y fin        
        $banner = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
        $calendario = Announcement::getAnnouncementsToDisplay($view->id, 'calendario');
        $display_announcements = compact('banner','calendario');
        return view('calendario', compact('display_announcements'/* ,'directions'*/));
    }

    public function showCalendarEvent($announcement_id) {
        try {
            $view = View::where('name', 'calendario')->first();
            $banner = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
            $display_announcements = compact('banner');

            $announcement = Announcement::findOrFail($announcement_id);
            if($announcement->announcementType->name != 'calendario') {
                flash('Anuncio no es del tipo calendario...');
                return back();
            }
            
            $form_data = $announcement->formData();
            return view('calendario', compact('form_data', 'display_announcements', 'announcement'));
        } catch(\Throwable $th) {
            // dd($th);
            return back();
        }
    }

    public function benefits(){
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);
        $view = View::where('name',$url)->first();
        $banner = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
        $blog = Announcement::getAnnouncementsToDisplay($view->id, 'blog');
        $display_announcements = compact('banner','blog');
        return view('mis_beneficios', compact('display_announcements'));
    }

    public function convenios() {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);

        $view = View::where('name',$url)->first();
        $banner = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
        $convenios = Announcement::getAnnouncementsToDisplay($view->id, 'convenios', Auth::user()->id);

        foreach($convenios['convenios'] as $convenio) {
            $convenio->like = !$convenio->likes->isEmpty();
            $convenio->favorite = !$convenio->favorites->isEmpty();
        }
        $total = $convenios['convenios']->count();

        $categories = AnnouncementCategory::whereHas('announcement_type', function ($q2) {
            $q2->where('name', 'convenios');
        })->get();

        $display_announcements = compact('banner','convenios');
        
        return view('convenios', compact('display_announcements', 'categories', 'total'));
    }

    public function faq(){
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);
        $view = View::where('name',$url)->first();
        //Traemos todos los anuncios activos y que esten vigentes en cuanto a la fecha de inicio y fin        
        $banner = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
        // dd($banner, isset($banner[0]->data['link']));
        $simple_list = Announcement::getAnnouncementsToDisplay($view->id, 'simple_list');
        $display_announcements = compact('banner', 'simple_list');
        return view('preguntas-frecuentes', compact('display_announcements'));
    }

    public function qualityObjectives() {
        return view('objetivos-calidad');
    }

    public function committees() {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);
        $view = View::where('name',$url)->first();
        $banner = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
        // $committees = Announcement::getAnnouncementsToDisplay($view->id, 'committees');
        $images = Announcement::getAnnouncementsToDisplay($view->id, 'images');
        $display_announcements = compact('banner'/*, 'committees'*/, 'images');
        return view('comites', compact('display_announcements'));
    }

    public function commissions() {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);
        $view = View::where('name',$url)->first();
        $banner = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
        // $committees = Announcement::getAnnouncementsToDisplay($view->id, 'committees');
        $images = Announcement::getAnnouncementsToDisplay($view->id, 'images');
        $display_announcements = compact('banner'/*, 'committees'*/, 'images');
        return view('comisiones', compact('display_announcements'));
    }
}
