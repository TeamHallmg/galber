<?php

namespace App\Http\Controllers\Bienes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use DB;
use App\Models\Bienes\TipoBienes;
use App\Models\Bienes\TipoBienesDetalle;
use App\Models\Bienes\Variable;
use App\Models\Bienes\VariableOpcion;
use App\Models\Bienes\VariableValor;

class TipoBienesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $bienes = TipoBienes::get();

        return View('bienes.tipos.index', compact('bienes'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $bienesVariables = Variable::get();

        return View('bienes.tipos.create', compact('bienesVariables'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        // dd($data['variables']);
        try {
            DB::beginTransaction();
                $TipoBienes = TipoBienes::create($data);

                foreach($data['variables'] as $variable) {
                    $TipoBienesDetalle = new TipoBienesDetalle;
                    $TipoBienesDetalle->id_tipo_bien = $TipoBienes->id;
                    $TipoBienesDetalle->id_variable = $variable;
                    $TipoBienesDetalle->save();
                }

            DB::commit();
        }
        catch (\Throwable $e) {
            // dd($e->getMessage());
            DB::rollback();
            //echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
            return redirect()->back()->with('flag', '
                VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
                DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. '.$e->getMessage());
        }

        $request->session()->flash('alert-success', 'Los datos se han agregado correctamente!!!');
        return redirect()->to('Tipobienes');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function show($id) {

        $TipoBienes = TipoBienes::where('id', $id)->first();

        return View('bienes.tipos.show', compact('TipoBienes'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $TipoBienes = TipoBienes::where('id', $id)->first();

        $bienesVariables = Variable::
        with([
            'tipobienesdetalles' => function($q) use ($id) {
                    $q->where('id_tipo_bien','=', $id);
                    $q->where('estatus',1);
            }
        ])
        ->get();    

        // dd($bienesVariables);

        return View('bienes.tipos.edit', compact('TipoBienes','bienesVariables'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

            $data = $request->all();
            try {

                DB::beginTransaction();


                    $form_data = array(
                        'codigo'  =>  $data['codigo'],
                        'nombre'  =>  $data['nombre'],
                        'descripcion'  =>  $data['descripcion'],
                        'estatus'  =>  $data['estatus']
                    );

                    $d = TipoBienes::where('id',$id)->update($form_data);

                    // dd($id,$form_data,$d);
                    
                    $lenguajes = TipoBienesDetalle::where('id_tipo_bien', $id)->where('estatus',1)->get();
                    $lenguajestoArray = TipoBienesDetalle::where('id_tipo_bien', $id)->where('estatus',1)->get()->all();
                    $canActual = TipoBienesDetalle::where('id_tipo_bien', $id)->where('estatus',1)->count();
                    
                    $canRecibida = count($data['variables']);

                        
                        // dd($data['variables']);
                        if($canRecibida < $canActual) {

                            $tmp = [];
                            $existen = [];

                            foreach ($data['variables'] as $idioma) {

                                foreach ($lenguajes as $lenguaje) {

                                    if ($lenguaje->id_variable == $idioma && !in_array($idioma, $tmp)) {

                                        $existen[$lenguaje->id] = $lenguaje->id_variable;

                                        array_push($tmp, $idioma);

                                    } else {

                                        $borrar[$lenguaje->id] = $lenguaje->id_variable;

                                    }

                                }

                            }

                            $eliminar = array_diff_assoc($borrar, $existen);

                            
                            foreach ($eliminar as $key => $elimina) {

                                TipoBienesDetalle::where('id', $key)->update(['estatus' => 0]);

                            }

                        

                        } else if ($canRecibida == $canActual) {
                            
                            $tmp = [];
                            $tmp2 = [];
                            $new = [];
                            $borrar = [];

                            foreach ($data['variables'] as $idioma) {

                                foreach ($lenguajes as $lenguaje) {

                                    if ($lenguaje->id_variable != $idioma && !in_array($idioma, $tmp) && !in_array($lenguaje->id_variable, $tmp2)) {

                                        $new[] = $idioma;
                                        $borrar[$lenguaje->id] = $lenguaje->id_variable;

                                        array_push($tmp, $idioma);
                                        array_push($tmp2, $lenguaje->id_variable);

                                    } 

                                }

                            }
                           
                            foreach ($new as $key) {


                                $exists = TipoBienesDetalle::
                                where('id_variable', $key)
                                ->where('id_tipo_bien',$id)
                                ->exists();

                                if($exists){

                                    TipoBienesDetalle::where('id_variable', $key)
                                                        ->where('id_tipo_bien',$id)
                                                        ->update(['estatus' => 1]);

                                }else{

                                    TipoBienesDetalle::create([
                                        'id_variable' => $key,
                                        'id_tipo_bien' => $id,
                                        'estatus' => 1
                                    ]);

                                }


                            }

                            foreach ($borrar as $key => $elimina) {

                                TipoBienesDetalle::where('id', $key)->update(['estatus' => 0]);

                            }

                        } 
                        else if ($canRecibida > $canActual) {
                      
                            $tmp = [];
                            $tmp2 = [];
                            $añadir = [];
                            $eliminar = [];
                            $mantener = [];
    

                            foreach ($data['variables'] as $idioma) {

                                $idioma = intval($idioma);

                                foreach ($lenguajes as $lenguaje) {

                                    if ($lenguaje->id_variable == $idioma && !in_array($idioma, $tmp)) {

                                        $mantener[$lenguaje->id] = $lenguaje->id_variable;

                                        array_push($tmp, $idioma);

                                    } 

                                }

                            }


                            if($mantener==''){

                                $añadir = array_diff($data['variables'], $mantener);

                                $eliminar = array_diff($lenguajestoArray,$mantener);


                                foreach ($eliminar as $key => $value ) {
                                    
                                    TipoBienesDetalle::where('id', $key)->update(['estatus' => 0]);


                                }    

                            }else{



                                $eliminar = array_diff($lenguajestoArray,$data['variables']);
                                $añadir = array_diff($data['variables'],$lenguajestoArray);
                                
                                foreach ($eliminar as $key ) {
                                    
                                    TipoBienesDetalle::where('id', $key->id)->update(['estatus' => 0]);

                                }

                            }
                                                      
                            foreach ($añadir as $key) {

                                $exists = TipoBienesDetalle::
                                where('id_variable', $key)
                                ->where('id_tipo_bien',$id)
                                ->exists();

                                if($exists){

                                    TipoBienesDetalle::where('id_variable', $key)
                                                        ->where('id_tipo_bien',$id)
                                                        ->update(['estatus' => 1]);

                                }else{

                                    TipoBienesDetalle::create([
                                        'id_variable' => $key,
                                        'id_tipo_bien' => $id,
                                        'estatus' => 1
                                    ]);
                                    
                                }

                              
                            }


                        } 

                    // }


                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();

                return redirect()->back()->with('alert-danger', '
                ¡UPS!... NO SE ACTUALIZÓ EL PUESTO <br/>
                VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
                ' . $e->getMessage());



            }

            
        $request->session()->flash('alert-success', 'Los datos se han actualizado correctamente!!!');
        return redirect()->to('Tipobienes');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
