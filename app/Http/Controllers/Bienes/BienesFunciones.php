<?php

namespace App\Http\Controllers\Bienes\BienesFunciones;


use App\Models\JobPosition;
use App\Models\Bienes\TipoBienes;
use App\Models\Bienes\jobPositionsBienes;

trait BienesTrait
 {
    //

    public function exite_bien($id_bien, $job_positions)
    {

        $consulta = jobPositionsBienes::where('id_tipo_bien', $id_bien)
        ->where('id_job_positions',$job_positions)
        ->exists();

        return $consulta;

    }


    public function actualizar_bien($id_bien, $job_positions, $estatus)
    {

        $consulta = jobPositionsBienes::where('id_tipo_bien', $id_bien)
        ->where('id_job_positions',$job_positions)
        ->update(['estatus' => $estatus]);
        
        return $consulta;

    }

    public function actualizar_bien_by_id($id_bien, $estatus)
    {

        $consulta = jobPositionsBienes::where('id', $id_bien)
        ->update(['estatus' => $estatus]);
        
        return $consulta;

    }

    public function crear_bien($id_bien, $job_positions)
    {


            $consulta = jobPositionsBienes::create([
                        'id_tipo_bien' => $id_bien,
                        'id_job_positions' => $job_positions,
                        'estatus' => 1
	                   ]);

        return $consulta;

    }

}
