<?php

namespace App\Http\Controllers\Bienes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use App\Models\Bienes\TipoBienes;
use App\Models\Bienes\TipoBienesDetalle;
use App\Models\Bienes\Variable;
use App\Models\Bienes\VariableOpcion;
use App\Models\Bienes\VariableValor;

class VariablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $Variable = Variable::get();

        return View('bienes.tiposDetalle.index', compact('Variable'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return View('bienes.tiposDetalle.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        
        if (isset( $data['requerido'] ))
        {
           $data['requerido'] = 1;
        }else{
            $data['requerido'] = 0;
        }


        if (isset( $data['multiple'] ))
        {
           $data['multiple'] = 1;
        }else{
            $data['multiple'] = 0;
        }


        if (isset( $data['editable'] ))
        {
           $data['editable'] = 1;
        }else{
            $data['editable'] = 0;
        }


        if (isset( $data['seleccionable'] ))
        {
           $data['seleccionable'] = 1;
        }else{
            $data['seleccionable'] = 0;
        }




        
        try {
            DB::beginTransaction();

                $reemplazar1 = " ";
                $reemplazar_por1 = "_";
                $nombre = str_replace($reemplazar1, $reemplazar_por1, $data['etiqueta']);
                $data['nombre'] = $nombre;

                Variable::create($data);

            DB::commit();
        }
        catch (\Throwable $e) {
            // dd($e->getMessage());
            DB::rollback();
            //echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
            return redirect()->back()->with('flag', '
                VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
                DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. '.$e->getMessage());
        }

        $request->session()->flash('alert-success', 'Los datos se han agregado correctamente!!!');
        return redirect()->to('TipoBienesDetalles');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $TipoBienesDetalle = Variable::where('id', $id)->first();

        return View('bienes.tiposDetalle.show', compact('TipoBienesDetalle'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $TipoBienesDetalle = Variable::where('id', $id)->first();

        return View('bienes.tiposDetalle.edit', compact('TipoBienesDetalle'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        if (isset( $data['requerido'] ))
        {
           $data['requerido'] = 1;
        }else{
            $data['requerido'] = 0;
        }


        if (isset( $data['multiple'] ))
        {
           $data['multiple'] = 1;
        }else{
            $data['multiple'] = 0;
        }


        if (isset( $data['editable'] ))
        {
           $data['editable'] = 1;
        }else{
            $data['editable'] = 0;
        }


        if (isset( $data['seleccionable'] ))
        {
           $data['seleccionable'] = 1;
        }else{
            $data['seleccionable'] = 0;
        }

        $reemplazar1 = " ";
        $reemplazar_por1 = "_";
        $nombre = str_replace($reemplazar1, $reemplazar_por1, $data['etiqueta']);
        $data['nombre'] = $nombre;

        
        try {
            
            DB::beginTransaction();

            $form_data = array(
                'etiqueta'  =>  $data['etiqueta'],
                'nombre'  =>  $data['nombre'],
                'descripcion'  =>  $data['descripcion'],
                'tipo'  =>  $data['tipo'],
                'requerido'  =>  $data['requerido'],
                'multiple'  =>  $data['multiple'],
                'editable'  =>  $data['editable'],
                'seleccionable'  =>  $data['seleccionable'],
                'estado'  =>  $data['estado'],
                'longitud'  =>  $data['longitud']
            );

            Variable::where('id',$id)->update($form_data);

                
            DB::commit();
        }
        catch (\Throwable $e) {
            // dd($e->getMessage());
            DB::rollback();
            //echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
            return redirect()->back()->with('flag', '
                VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
                DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. '.$e->getMessage());
        }

        $request->session()->flash('alert-success', 'Los datos se han actualizados correctamente!!!');
        return redirect()->to('TipoBienesDetalles');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
