<?php

namespace App\Http\Controllers\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Profile\ProfileBienes;
use App\Models\Profile\Profile;
use App\Models\Bienes\VariableValor;
use PDF;
use DB;


class AdminBienesController extends Controller
{


    public function AsignarBienes(Request $request) {
        $data = $request->all();
        // dd($data['bienesDatos'],$data['tipoBien']);

        //dd($data);

        try {
            DB::beginTransaction();

                $ProfileBienes = new ProfileBienes;
                $ProfileBienes->id_tipo_bien = $data['tipoBien'];
                $ProfileBienes->profile_id = $data['profile_id'];
                $ProfileBienes->codigo = $data['codigo'];
                $ProfileBienes->fecha_entrega = $data['fecha_entrega'];
                $ProfileBienes->estatus = 1;
                $ProfileBienes->save();

                foreach ($data['bienesDatos'] as $n => $tipo) {

                    if($n == $data['tipoBien'])
                        foreach ($tipo as $a => $tipoa) {
                            $VariableValor = new VariableValor;
                            $VariableValor->id_referencia = $ProfileBienes->id;
                            $VariableValor->id_variable = $a;
                            $VariableValor->valor = $tipoa[0];
                            // $VariableValor->descripcion = $data["reading"][$n];
                            // $VariableValor->indice = $data["writing"][$n];
                            $VariableValor->save();
                        }
                }

            DB::commit();
        }
        // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
        catch (\Throwable $e) {
            DB::rollback();
             echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
            die(); 

            // return redirect()->back()->with('alert-danger', '
            // ¡UPS!... NO SE ACTUALIZÓ EL PERFIL <br/>
            // VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
            // ' . $e->getMessage());
        }
        
        return redirect()->back()->with('alert-success', 'El Bien se ha Asignado correctamente!!!');
    }


    public function entregaBienes($id_profile, $id_bien) {


        $profile = Profile::with([
            'bienes' => function($q) use ($id_bien) {
                    $q->where('id','=', $id_bien);
            }
        ])->where('id', $id_profile)->first();


        $bien= ProfileBienes::where('id', $id_bien)->first();

        if($bien->estatus!=4){
            $bien->update(['estatus'=>2]);
        }

        $bienesasociados = [];

        if(isset($profile->bienes)){

            foreach ($profile->bienes as $key => $value) {

                $id = $value->id;

                $bienesasociados[$key] = [

                    'id'=>$id,
                    'codigo'=>$value->codigo,
                    'estatus'=>$value->estatus,
                    'fecha_entrega'=>$value->fecha_entrega,
                    'nombre_bien'=>$value->tipobien->nombre
                ];


                foreach ($value->tipobien->tipobiendetalles as $key_2 => $value_2) {

                   $valores = VariableValor::where('id_referencia',$id)->where('id_variable',$value_2->id_variable)->first();

                   $valor = isset($valores->valor) ? $valores->valor : null;
                    $bienesasociados[$key]['variables'][] = [
                        'variable'=>$value_2->variable->etiqueta,
                        'variable_valor'=> $valor 
                    ];
                }


            }


        }


        $nombre = $profile->name.'_'.$profile->surname_father.'.pdf';

            // return view('profile.pdf.entregaBienes', compact('profile'));

        $pdf = PDF::loadView('profile.pdf.entregaBienes', compact('profile','bienesasociados','nombre'));

        $output = $pdf->output();


        $url_document = storage_path('bienes').'/'.$nombre;
        
        $pdf->save($url_document);

        return response()->file($url_document);
            
    }

    public function adjuntar_comprobante(Request $request)
    {

        $documento = ProfileBienes::where('id',$request->documento_id)->first();
        $profile = Profile::where('id',$request->profile_id)->first();

        $image = $request->file('select_file');

        $new_name = 'Comprobante ('.$documento->codigo. ') - ' . $image->getClientOriginalName();
            
        $url_comprobant = storage_path('bienes').'/'.$new_name;

        $documento->update(['estatus'=>3, 'url_comprobante'=>$url_comprobant]);
          
        $image->move(storage_path('bienes'), $new_name);
        
        return redirect('profile/'.$profile->user->id)->with('alert-success', 'Documento Adjuntado Correctamente!!!');

    }

    public function desincorporar_bien(Request $request)
    {

        $documento = ProfileBienes::where('id',$request->documento_id)->first();

        $profile = Profile::where('id',$request->profile_id)->first();

        $documento->update(['estatus'=>4, 
        	'motivo_desincorporacion'=>$request->motivo_desincorporacion,
        	'fecha_desincorporacion'=>$request->fecha_desincorporacion,
        	'detalle_desincorporacion'=>$request->detalle_desincorporacion
        ]);
          
        return redirect('profile/'.$profile->user->id)->with('alert-success', 'Bien Desincorporado Correctamente!!!');

    }

    public function descargar_documento($id){

        $consulta = ProfileBienes::where('id', $id)->first();
        
        $pdf = $consulta->url_comprobante;
        
        return response()->file($pdf);

    }

}
