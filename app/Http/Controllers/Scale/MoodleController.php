<?php

namespace App\Http\Controllers\Scale;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\User;
use App\Employee;
use App\Models\Area;
use App\Models\Department;
use App\Models\JobPosition;
use App\Models\Scale\Course;
use App\Models\Profile\Profile;
use App\Models\Moodle\User as UserMdl;
use App\Models\Moodle\Course as MCourse;
use App\Models\Moodle\RoleAssigment;
use App\Models\Moodle\CourseCategory;

class MoodleController extends Controller
{

    public function __construct() {
        $this->moodleDB = env('DB_DATABASE_SECOND', 'moodle_db');
        $this->laravelDB = env('DB_DATABASE', 'laravel_db');
        $this->minApproveGrade = 80;
        $this->count = 0;
        $this->employeeControl = [];
        $this->customFilters = [];
        $this->canAccessToUserHistoryBool = false;
        $this->scale_admin = 'scale_admin';

        // ==== Admin Escalafón ====
        $this->middleware('permission:scale_admin')->only([
            'index', 'create', 'store', 'show', 'edit', 'update', 'destroy', 'getUsersEnrolledByCourse'
        ]);
        // ==== Admin Escalafón ====
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customFilters = [
            'courses' => Input::get('courses'), // Listo
            'categories' => Input::get('categories'), // Listo
            'users' => Input::get('users'), // Listo
            'branch_offices' => Input::get('branch_offices'),
            'jobs' => Input::get('jobs'),
            'departments' => Input::get('departments')
        ];
        $filtered_data = false;

        foreach($customFilters as &$filter) {
            if($filter) {
                $filtered_data = true;
            } else {
                $filter = [];
            }
        }

        // dd($customFilters);

        $filters = [
            'categories' => CourseCategory::select('id', 'name')->get()->toArray(),
            'users' => User::whereHas('employee')->get(),
            'branch_offices' => Employee::select('sucursal')->whereNotNull('sucursal')->distinct()->get()->pluck('sucursal')->toArray(),
            'departments' => Department::select('name')->orderBy('name')->distinct('name')->pluck('name'),
            'jobs' => JobPosition::select('name')->orderBy('name')->distinct('name')->pluck('name')
        ];

        $courses = [
            'all' => [],
            'visible' => 0,
            'nonvisible' => 0
        ];

        if($customFilters['courses']) {
            $courses_data = MCourse::whereIn('id', $customFilters['courses'])
                            ->when($customFilters['categories'], function($q) use($customFilters) {
                                $q->whereIn('category', $customFilters['categories']);
                            })
                            ->get();
        } else {
            $courses_data = MCourse::
                            // whereIn('id', [16, 17, 18])->
                            when($customFilters['categories'], function($q) use($customFilters) {
                                $q->whereIn('category', $customFilters['categories']);
                            })
                            ->get();
        }
        foreach($courses_data as $data) {
            $laravel_course = Course::where('moodle_id',$data->id)->first();
            $modality = $trainer_name = $trainer_rfc = $duration = $type = null;
            $hasTrainer = false;
            if($laravel_course) {
                $type  = $laravel_course->type;
                $modality = $laravel_course->getModalityName();
                $trainer_data = $laravel_course->getTrainerData();
                $trainer_name = $trainer_data['name'];
                $trainer_rfc = $trainer_data['rfc'];
                $hasTrainer = $trainer_data['hasTrainer'];
                $duration = $laravel_course == 'E'?$laravel_course->duration:null;
            }

            $courses['all'][$data->id] = [
                'id' => $data->id,
                'category' => $data->m_category->name ?? 'S/C',
                'fullname' => $data->fullname,
                'shortname' => $data->shortname,
                'type' => $type,
                'modality' => $modality?$modality:'N/A',
                'duration' => $duration,
                'hasTrainer' => $hasTrainer,
                'trainer_name' => $trainer_name,
                'trainer_rfc' => $trainer_rfc,
                'enrolled' => []
            ];
            $course = &$courses['all'][$data->id];
            $enrolled = $this->getEnrollData("AND c.id = $data->id");
            foreach($enrolled as $row) {
                $user = User::where('id', $row->laravel_id)->whereHas('employee')
                        ->when($customFilters['users'], function($q) use($customFilters) {
                            $q->whereIn('id', $customFilters['users']);
                        })
                        ->when($customFilters['branch_offices'], function($q) use($customFilters) {
                            $q->whereHas('employee', function($employee) use($customFilters) {
                                $employee->whereIn('sucursal', $customFilters['branch_offices']);
                            });
                        })
                        ->when($customFilters['jobs'], function($q) use($customFilters) {
                            $q->whereHas('employee', function($employee) use($customFilters) {
                                $employee->whereHas('jobposition', function($jobposition) use($customFilters) {
                                    $jobposition->whereIn('name', $customFilters['jobs']);
                                });
                            });
                        })
                        ->when($customFilters['departments'], function($q) use($customFilters) {
                            $q->whereHas('employee', function($employee) use($customFilters) {
                                $employee->whereHas('jobposition', function($jobposition) use($customFilters) {
                                    $jobposition->whereHas('area', function($area) use($customFilters) {
                                        $area->whereHas('department', function($department) use($customFilters) {
                                            $department->whereIn('name', $customFilters['departments']);
                                        });
                                    });
                                });
                            });
                        })
                        ->first();
                if($user) {
                    $user->course_priority = $user->min_grade = null;
                    if($user->employee->jobposition) {
                        $job_course_data = $user->employee->jobposition->getJobCourseData($user->employee->jobposition->id, $data->id);
                        if(!empty($job_course_data)) {
                            $user->course_priority = $job_course_data['priority'];
                            $user->min_grade = $job_course_data['min_grade'];
                        }
                    }
                    $user->course = $this->getUserCourseStatus($user, $data->id);
                    $course['enrolled'][$user->id] = $user;
                }
            }
            $data->visible?$courses['visible']++:$courses['nonvisible']++;
        }

        return view('scale.moodle.index', compact('courses', 'filters', 'filtered_data', 'customFilters'));
    }

    private function getUserCourseStatus($user, $course_id) {
        // Perdoname diosito por lo que voy a hacer
        try {
            $data = $this->getUserCourseDBData(2, $user->id, $course_id);
            $data = $this->formatMoodleDBDataByUser($data);
            // if(empty($data)) {
            //     return [
            //         'status' => 0,
            //         'grade' => null
            //     ];
            // }
            $data = $data[$user->id];
            if(count($data['courses'][$course_id]['items']) > 0) {
                if($data['courses'][$course_id]['completed']) {
                    $status = 2;
                } else {
                    $status = 1;
                }
            } else {
                $status = 0;
            }
            $grade = number_format($data['score'], 2);
            return [
                'status' => $status,
                'grade' => $grade
            ];
        } catch(\Throwable $th) {
            return [
                'status' => 0,
                'grade' => null
            ];
        }
        // $grade = $user->getUserCourseGrade($course_id, $user->id);
        // if(!empty($grade)) {
        //     $grade = $grade[0];
        //     $user->getUserCourseGrade($course_id, $user->id);
        // }
        // return [
        //     'status' => $grade?1:0,
        //     'grade' => $grade?$grade->grade:null
        // ];
    } 

    public function indexBKP()
    {
        $data = $this->getUserCourseDBData();
        $datav2 = $this->getUserCourseDBDataV2();
        $datav3 = $this->getUserCourseDBDataV3();
        $user_courses = $this->getCompletionData($datav3);

        $courses = [
            'visible' => MCourse::visible()->get(),
            'nonvisible' => MCourse::visible(false)->get()
        ];

        return view('scale.moodle.index', compact('data', 'datav2', 'user_courses', 'courses'));
    }

    public function userHistory($employee_id = null) {
        try {
            $user = User::whereHas('employee', function($q) use ($employee_id) {
                $q->where('id', $employee_id);
            })->firstOrFail();

            // Si no es administrador tenemos que validar que la persona
            // se encuentra en su árbol organizacional.
            if(!Auth::user()->isAdminOrHasRolePermission($this->scale_admin)) {
                if(Auth::user()->id == $user->id) {
                    $this->canAccessToUserHistoryBool = true;
                } else {
                    $this->canAccessToUserHistory(Auth::user(), $user);
                }
                if(!$this->canAccessToUserHistoryBool) {
                    flash('No tienes permitido ingresar a este historial de usuario...')->warning();
                    return back();
                }
            }

            $data = $this->getUserMoodleHistory($user);
            // dd($data);
        } catch(\Throwable $th) {
            // dd($th);
            flash('Usuario no encontrado...')->error();
            return back();
        }

        return view('scale.moodle.history', compact('user', 'data'));
    }

    private function canAccessToUserHistory($user, $look_for_user) {
        foreach($user->getSubordinates() as $sub_employee) {
            if($sub_employee->user) {
                if($sub_employee->user->id == $look_for_user->id) {
                    $this->canAccessToUserHistoryBool = true;
                    break;
                } else {
                    $this->canAccessToUserHistory($sub_employee->user, $look_for_user);
                }
            }
        }
    }

    private function getUserMoodleHistory($user) {
        $tmp_data = $this->getUserCourseDBData(2, $user->id);
        $enrolled_courses = $this->getEnrollData("AND u.icq = $user->id");
        // dd($enrolled_courses);
        // dd($tmp_data);
        $data = $this->formatMoodleDBData($tmp_data, $enrolled_courses);
        // dd($data);

        return $data;
    }

    private function formatMoodleDBData($tmp_data, $enrolled_courses) {
        $data = [];
        foreach($enrolled_courses as $row) {
            $data[$row->course_id] = [
                'course_id' => $row->course_id,
                'course' => $row->course,
                'category' => $row->category,
                'modality' => 'virtual',
                'items' => [],
                'with_grade' => 0,
                'completed' => false,
                'total_grades' => 0,
                'score' => 0,
                'max_score' => 0
            ];
            $laravel_course = Course::where('moodle_id', $row->course_id)->first();
            if($laravel_course) {
                $data[$row->course_id]['modality'] = $laravel_course->modality;
            }
            $data[$row->course_id]['modality'] = $this->getModalityName($data[$row->course_id]['modality']);
        }
        foreach($tmp_data as $row) {
            // if(!array_key_exists($course_id = $row->course_id, $data)) {
            //     $data[$course_id] = [
            //         'course_id' => $course_id,
            //         'course' => $row->course,
            //         'category' => $row->category,
            //         'items' => [],
            //         'with_grade' => 0,
            //         'completed' => false,
            //         'total_grades' => 0,
            //         'score' => 0,
            //         'max_score' => 0
            //     ];
            // }
            $course_id = $row->course_id;
            $data[$course_id]['items'][] = [
                'item_type' => $row->item_type,
                'item_module' => $row->item_module,
                'grade' => $row->percentage,
                'approved' => $row->status
            ];
            if($row->score == 'SC') {
                $data[$course_id]['score'] += 0;
            } else {
                $data[$course_id]['score'] += $row->score;
            }
            $data[$course_id]['max_score'] += $row->max_grade;
            if($row->percentage) {
                $data[$course_id]['with_grade'] += 1;
                $data[$course_id]['total_grades'] += $row->percentage;
            }
            if($data[$course_id]['with_grade']/count($data[$course_id]['items']) == 1) {
                $data[$course_id]['completed'] = true;
            } else {
                $data[$course_id]['completed'] = false;
            }
        }

        $new_data['user']['global_completion'] = 
        $new_data['course'] = [];

        $new_data = [
            'user' => [
                'global_completion' => 0,
                'global_average' => 0,
                'completed' => 0

            ],
            'courses' => [],
            'courses_for_global_average' => []
        ];

        // dd($data);

        foreach($data as &$course) {
            if(count($course['items']) > 0) {
                $course['completion'] = number_format(($course['with_grade']/count($course['items']))*100, 2);
            } else {
                $course['completion'] = 0;
            }
            $new_data['user']['global_completion'] += $course['completion'];
            if($course['completed']) {
                $new_data['user']['global_average'] += number_format(($course['score']/$course['max_score'])*100, 2);
                $new_data['user']['completed'] += 1;
            }
            $new_data['courses'][$course['course_id']] = $course;
            if(!empty($course['items'])) {
                $new_data['courses_for_global_average'][$course['course_id']] = $course;
            }
        }
        if(count($new_data['courses_for_global_average']) > 0) {
            $new_data['user']['global_completion'] = number_format($new_data['user']['global_completion']/count($new_data['courses_for_global_average']), 2);
        }
        if($new_data['user']['completed'] > 0) {
            $new_data['user']['global_average'] = number_format($new_data['user']['global_average']/$new_data['user']['completed'], 2);
        }

        return $new_data;
    }

    private function formatMoodleDBDataByUser($tmp_data) {
        $data = [];
        $pivotKey = 'laravel_id';
        foreach($tmp_data as $row) {
            if(!array_key_exists($laravel_id = $row->laravel_id, $data)) {
                $data[$laravel_id] = [
                    'laravel_id' => $laravel_id,
                    'courses' => []
                ];
            }
            if(!array_key_exists($course_id = $row->course_id, $data[$laravel_id]['courses'])) {
                $data[$laravel_id]['courses'][$course_id] = [
                    'course_id' => $course_id,
                    'course' => $row->course,
                    'category' => $row->category,
                    'items' => [],
                    'with_grade' => 0,
                    'completed' => false,
                    'total_grades' => 0,
                    'score' => 0,
                    'max_score' => 0
                ];
            }
            $course = &$data[$laravel_id]['courses'][$course_id];
            $course['items'][] = [
                'item_type' => $row->item_type,
                'item_module' => $row->item_module,
                'grade' => $row->percentage,
                'approved' => $row->status
            ];
            if($row->score == 'SC') {
                $course['score'] += 0;
            } else {
                $course['score'] += $row->score;
            }
            $course['max_score'] += $row->max_grade;
            if($row->percentage) {
                $course['with_grade'] += 1;
                $course['total_grades'] += $row->percentage;
            }
        }

        foreach($data as &$row) {
            $row['completed'] = 0;
            $row['score'] = 0;
            foreach($row['courses'] as &$course) {
                if($course['with_grade']/count($course['items']) == 1) {
                    $course['completed'] = true;
                    $row['completed'] += 1;
                    $row['score'] += ($course['score']/$course['max_score'])*100;
                } else {
                    $course['completed'] = false;
                }
            }
            $row['total_courses'] = count($row['courses']);
        }

        return $data;
    }

    private function getModalityName($modality) {
        switch($modality) {
            case 'face-to-face':
                return 'Presencial';
            break;
            case 'virtual':
                return 'E-Learning';
            break;
            case 'blended':
                return 'Blended Learning';
            break;
            default:
                return 'Desconocido';
            break;
        }
    }

    public function overview() {
        $this->customFilters = [
            'department' => Input::get('department'),
            'branch_office' => Input::get('branch_office')
        ];
        $custom_filters = $this->customFilters;
        $data = $this->getOrganizationTable();
        $filters = [
            'departments' => Department::select('name')->orderBy('name')->distinct('name')->pluck('name'),
            'branch_offices' => Employee::select('sucursal')->orderBy('sucursal')->whereNotNull('sucursal')->distinct('sucursal')->pluck('sucursal')
        ];
        return view('scale.moodle.overview', compact('data', 'filters', 'custom_filters'));
    }

    public function getOrganizationTable(){
        $data = '';
        $nivel = 1;
        $black_listed_mails = ['amolina@ucin.com'];
        $custom_filters = $this->customFilters;
        try {
            if(Auth::user()->isAdminOrHasRolePermission($this->scale_admin)) {
                $top_employees = Employee::where(function ($q){
                                    $q->where('jefe', '')
                                    ->orWhere('jefe', '0');
                                })->whereNotIn('correoempresa', $black_listed_mails)
                                ->when(!is_null($custom_filters['department']), function($employee) use($custom_filters) {
                                    $employee->whereHas('jobPosition', function($jobPosition) use($custom_filters) {
                                        $jobPosition->whereHas('area', function($area) use($custom_filters) {
                                            $area->whereHas('department', function($department) use($custom_filters) {
                                                $department->where('name', $custom_filters['department']);
                                            });
                                        });
                                    });
                                })
                                ->when(!is_null($custom_filters['branch_office']), function($q) use($custom_filters) {
                                    $q->where('sucursal', $custom_filters['branch_office']);
                                })
                                ->orderBy('nombre','DESC')->get();
            } else {
                $top_employees = Employee::where('id', Auth::user()->employee_id)
                                ->when(!is_null($custom_filters['department']), function($employee) use($custom_filters) {
                                    $employee->whereHas('jobPosition', function($jobPosition) use($custom_filters) {
                                        $jobPosition->whereHas('area', function($area) use($custom_filters) {
                                            $area->whereHas('department', function($department) use($custom_filters) {
                                                $department->where('name', $custom_filters['department']);
                                            });
                                        });
                                    });
                                })
                                ->when(!is_null($custom_filters['branch_office']), function($q) use($custom_filters) {
                                    $q->where('sucursal', $custom_filters['branch_office']);
                                })
                                ->orderBy('nombre','DESC')->get();
            }
        } catch(\Throwable $th) {
            // dd($th);
            $top_employees = [];
        }

        $moodle_data = $this->getUserCourseDBData(2);
        $moodle_data = $this->formatMoodleDBDataByUser($moodle_data);
        // dd($moodle_data[4074]);
        // $moodle_ids = array_keys($moodle_data);
        // dd($moodle_ids);
        // $employees = Employee::with(['user' => function($q) {
        //     $q->withTrashed();
        // }])->withTrashed()->get();
        // foreach($employees as $employee) {
        //     $user_id = $employee->user->id;
        //     if(!UserMdl::where('icq', $user_id)->exists()) {
        //         $indx_id = array_rand($moodle_ids);
        //         UserMdl::where('id', $moodle_ids[$indx_id])->update(['icq' => $user_id]);
        //         unset($moodle_ids[$indx_id]);
        //     }
            // else {
            //     dd(UserMdl::where('icq', $user_id)->first());
            // }
        // }
        // dd('la prohibida');
    
        foreach($top_employees as $employee) {
            $anchor = '<a class="text-primary ml-1" href="'.url('escalafon/overview/history/'.$employee->id).'"><i class="far fa-user"></i></a>';
            if(!in_array($employee->id, $this->employeeControl)) {
                $this->employeeControl[] = $employee->id;
                $id = count($this->employeeControl);
            }
            $user_id = $employee->user->id;
            $data .= '<tr data-id="'.$id.'" data-parent="0" data-level="'.$nivel.'">';
            $data .=    '<td data-column="name" class="encont text-left">'.$employee->getFullNameAttribute().' '.$anchor.'</td>';
            $data .=    '<td>'.$employee->getDepartmentName(true).'</td>';
            $data .=    '<td>'.$employee->getPuestoName(true).'</td>';
            $data .=    '<td>'.$employee->getSucursal().'</td>';
            
            if(isset($moodle_data[$user_id])) {
                if($moodle_data[$user_id]['score'] > 0 && $moodle_data[$user_id]['completed']) {
                    $average = number_format($moodle_data[$user_id]['score']/$moodle_data[$user_id]['completed'], 2);
                } else {
                    $average = number_format($moodle_data[$user_id]['score'], 2);
                }
                $completed = $moodle_data[$user_id]['completed'];
                $total_courses = $moodle_data[$user_id]['total_courses'];
                $percentage = number_format(($completed/$total_courses)*100, 0);

                $data .=
                '<td>
                    <div class="progress position-relative" style="height: 24px;">
                        <div class="progress-bar progress-bar-striped bg-primary text-dark" role="progressbar" style="width: '.$percentage.'%" aria-valuenow="'.$percentage.'" aria-valuemin="0" aria-valuemax="100"></div>
                        <span class="justify-content-center d-flex position-absolute w-100 mt-1">
                            <span class="badge badge-pill badge-dark" style="font-size: 11px;">'.$percentage.' %</span>
                        </span>
                    </div>
                </td>';
                $data .= '<td>'.$completed.'/'.$total_courses.'</td>';
                $data .= '<td>'.($average == 0?'S/P':$average).'</td>';
            } else {
                // $data .= '<td><span class="text-muted">S/P</span></td>';
                // $data .= '<td><span class="text-muted">S/C</span></td>';
                $data .=    '<td class="px-5">
                                <small>
                                    <p class="font-weight-bold m-0 rounded-pill bg-transparent border border-muted text-muted">
                                        S/P
                                    </p>
                                </small>
                            </td>';
                $data .=    '<td class="px-5">
                                <small>
                                    <p class="font-weight-bold m-0 rounded-pill bg-transparent border border-muted text-muted">
                                        S/C
                                    </p>
                                </small>
                            </td>';
                $data .= '<td><span class="text-muted">0</span></td>';
            }
            // $data .=    '<td>'.($employee->trashed()?'<span class="text-danger">Inactivo</span>':'<span class="text-success">Activo</span>').'</td>';
            $data .= '</tr>';
            $data .= $this->getSubBoss($employee, $nivel+1, $id, $moodle_data);
        }

        return $data;
    }

    private function getSubBoss($boss, $nivel, $parent, $moodle_data){
        $data = '';
        $custom_filters = $this->customFilters;
        $emloyees = Employee::where('jefe', $boss->idempleado)
                    ->when(!is_null($custom_filters['department']), function($employee) use($custom_filters) {
                        $employee->whereHas('jobPosition', function($jobPosition) use($custom_filters) {
                            $jobPosition->whereHas('area', function($area) use($custom_filters) {
                                $area->whereHas('department', function($department) use($custom_filters) {
                                    $department->where('name', $custom_filters['department']);
                                });
                            });
                        });
                    })
                    ->when(!is_null($custom_filters['branch_office']), function($q) use($custom_filters) {
                        $q->where('sucursal', $custom_filters['branch_office']);
                    })
                    ->orderBy('nombre','ASC')->get();
        
        foreach($emloyees as $key => $employee) {
            $anchor = '<a class="text-primary ml-1" href="'.url('escalafon/overview/history/'.$employee->id).'"><i class="far fa-user"></i></a>';
            if(!in_array($employee->id, $this->employeeControl)) {
                $this->employeeControl[] = $employee->id;
                $id = count($this->employeeControl);
            }
            $user_id = $employee->user->id;
            $data .= '<tr data-id="'.$id.'" data-parent="'.$parent.'" data-level="'.$nivel.'" '.($key == 0?'class="inner-shadow"':'').'>';
            $data .=    '<td data-column="name" class="encont text-left">'.$employee->getFullNameAttribute().' '.$anchor.'</td>';
            $data .=    '<td>'.$employee->getDepartmentName().'</td>';
            $data .=    '<td>'.$employee->getPuestoName().'</td>';
            $data .=    '<td>'.$employee->getSucursal().'</td>';
            
            if(isset($moodle_data[$user_id])) {
                if($moodle_data[$user_id]['score'] > 0 && $moodle_data[$user_id]['completed']) {
                    $average = number_format($moodle_data[$user_id]['score']/$moodle_data[$user_id]['completed'], 2);
                } else {
                    $average = number_format($moodle_data[$user_id]['score'], 2);
                }
                $completed = $moodle_data[$user_id]['completed'];
                $total_courses = $moodle_data[$user_id]['total_courses'];
                $percentage = number_format(($completed/$total_courses)*100, 0);
                // if($id == 19) {
                //     dd($moodle_data[$user_id]);
                // }
                $data .=
                '<td>
                    <div class="progress position-relative" style="height: 24px;">
                        <div class="progress-bar progress-bar-striped bg-primary text-dark" role="progressbar" style="width: '.$percentage.'%" aria-valuenow="'.$percentage.'" aria-valuemin="0" aria-valuemax="100"></div>
                        <span class="justify-content-center d-flex position-absolute w-100 mt-1">
                            <span class="badge badge-pill badge-dark" style="font-size: 11px;">'.$percentage.' %</span>
                        </span>
                    </div>
                </td>';
                $data .= '<td>'.$completed.'/'.$total_courses.'</td>';
                $data .= '<td>'.($average == 0?'S/P':$average).'</td>';
            } else {
                // $data .= '<td><span class="text-muted">S/P</span></td>';
                // $data .= '<td><span class="text-muted">S/C</span></td>';
                $data .=    '<td class="px-5">
                                <small>
                                    <p class="font-weight-bold m-0 rounded-pill bg-transparent border border-muted text-muted">
                                        S/P
                                    </p>
                                </small>
                            </td>';
                $data .=    '<td class="px-5">
                                <small>
                                    <p class="font-weight-bold m-0 rounded-pill bg-transparent border border-muted text-muted">
                                        S/C
                                    </p>
                                </small>
                            </td>';
                $data .= '<td><span class="text-muted">0</span></td>';
            }
            // $data .=    '<td>'.($employee->trashed()?'<span class="text-danger">Inactivo</span>':'<span class="text-success">Activo</span>').'</td>';
            $data .= '</tr>';
            $data .= $this->getSubBoss($employee, $nivel+1, $id, $moodle_data);
        }
           
        return $data;
    }

    private function getCompletionData($data) {
        // $pivotKey = 'user_moodle_id';
        $pivotKey = 'user_id';
        $tmp_data = [];

        foreach($data as $row) {
            if(!array_key_exists($row->$pivotKey, $tmp_data)) {
                $tmp_data[$row->$pivotKey] = [
                    'user_id' => $row->user_id, // laravel user_id
                    'user_moodle_id' => $row->user_moodle_id, // moodle user_id
                    'first_name' => $row->first_name,
                    'last_name' => $row->last_name,
                    'm_first_name' => $row->m_first_name,
                    'm_last_name' => $row->m_last_name,
                    'courses' => [],
                    'percentage' => 0,
                    'percentage_bg' => 'bg-white',
                    'approved_courses' => 0,
                    'not_approved_courses' => 0,
                    'not_initiated_courses' => 0
                ];
            }
            $tmp_data[$row->$pivotKey]['courses'][] = [
                'category_id' => $row->category_id,
                'category' => $row->category,
                'course_id' => $row->course_id,
                'course' => $row->course,
                'course_id' => $row->course_id,
                'grade' => $row->grade,
                'pass' => $row->pass
            ];

            if(!is_null($row->grade)) {
                if($row->pass) {
                    $tmp_data[$row->$pivotKey]['approved_courses'] += 1;
                } else {
                    $tmp_data[$row->$pivotKey]['not_approved_courses'] += 1;
                }
            } else {
                $tmp_data[$row->$pivotKey]['not_initiated_courses'] += 1;
            }

            try {
                if($tmp_data[$row->$pivotKey]['approved_courses'] > 0) {
                    $tmp_data[$row->$pivotKey]['percentage'] = ($tmp_data[$row->$pivotKey]['approved_courses'] / count($tmp_data[$row->$pivotKey]['courses']))*100;
                    if($tmp_data[$row->$pivotKey]['percentage'] <= 40) {
                        $tmp_data[$row->$pivotKey]['percentage_bg'] = 'bg-warning';
                    } elseif($tmp_data[$row->$pivotKey]['percentage'] <= 80) {
                        $tmp_data[$row->$pivotKey]['percentage_bg'] = 'bg-info';
                    } elseif($tmp_data[$row->$pivotKey]['percentage'] <= 100) {
                        $tmp_data[$row->$pivotKey]['percentage_bg'] = 'bg-success';
                    }
                }
            } catch(\Throwable $th) {
                dd($th, count($tmp_data[$row->$pivotKey]['courses']), $tmp_data[$row->$pivotKey]['approved_courses']);
            }

            // if(count($tmp_data[$row->$pivotKey]['courses']) == 2 && false) {
            //     dd(count($tmp_data[$row->$pivotKey]['courses']), $tmp_data[$row->$pivotKey]['approved_courses']);
            // }
        }

        return $tmp_data;
    }

    // $moodle_ids = array_keys($moodle_data);
    // $employees = Employee::with(['user' => function($q) {
    //     $q->withTrashed();
    // }])->withTrashed()->get();
    // foreach($employees as $employee) {
    //     $user_id = $employee->user->id;
    //     if(!UserMdl::where('icq', $user_id)->exists()) {
    //         $indx_id = array_rand($moodle_ids);
    //         UserMdl::where('id', $moodle_ids[$indx_id])->update(['icq' => $user_id]);
    //         unset($moodle_ids[$indx_id]);
    //     }
    // }
    // dd('la prohibida');

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // SELECT u.username,
    //     c.shortname,        
    //     cc.timecompleted
    // FROM mdl_course_completions cc
    // JOIN mdl_course c ON c.id = cc.course
    // JOIN mdl_user u ON u.id = cc.userid

    private function getUserCourseDBDataV4() { // Sirve para tareas específicas
        $query = "
            SELECT
               u.id AS 'user_moodle_id', u.username,
                c.fullname AS 'course', c.id AS 'course_id',
                m.name AS 'activity_type',
                CASE
                    WHEN cmc.completionstate = 0 THEN 'In Progress'
                    WHEN cmc.completionstate = 1 THEN 'Completed'
                    WHEN cmc.completionstate = 2 THEN 'Completed with Pass'
                    WHEN cmc.completionstate = 3 THEN 'Completed with Fail'
                    ELSE 'Unknown'
                END AS 'progress'
                
            FROM
                $this->moodleDB.mdl_course_modules_completion cmc
            JOIN
                $this->moodleDB.mdl_course_modules cm ON cmc.coursemoduleid = cm.id
            JOIN
                $this->moodleDB.mdl_user u ON cmc.userid = u.id
            JOIN
                $this->moodleDB.mdl_course c ON cm.course = c.id
            JOIN
                $this->moodleDB.mdl_modules m ON cm.module = m.id
        ";

        return DB::connection('moodle_mysql')->select($query);
    }

//     SELECT u.username,
//        c.shortname,        
//        cc.timecompleted
// FROM mdl_course_completions cc
// JOIN mdl_course c ON c.id = cc.course
// JOIN mdl_user u ON u.id = cc.userid

    private function getUserCourseDBDataV3() {
        $minApproveGrade = $this->minApproveGrade-1;
        $query = "
            SELECT
                lu.id AS 'user_id', u.id AS 'user_moodle_id', lu.first_name AS 'first_name' , lu.last_name AS 'last_name', u.firstname AS 'm_first_name', u.lastname AS 'm_last_name',
                c.fullname AS 'course', c.id AS 'course_id',
                cc.name AS 'category', cc.id AS 'category_id',
                -- ROUND(gg.finalgrade, 2) AS 'grade',
                ROUND(gg.finalgrade / gg.rawgrademax * 100 ,2) AS 'grade',
                IF (ROUND(gg.finalgrade / gg.rawgrademax * 100 ,2) > $minApproveGrade, true , false) AS 'pass'
                
            FROM
                $this->moodleDB.mdl_course AS c
            JOIN
                $this->moodleDB.mdl_context AS ctx ON c.id = ctx.instanceid
            JOIN
                $this->moodleDB.mdl_role_assignments AS ra ON ra.contextid = ctx.id
            JOIN
                $this->moodleDB.mdl_user AS u ON u.id = ra.userid
            JOIN
                $this->moodleDB.mdl_grade_grades AS gg ON gg.userid = u.id
            JOIN
                $this->moodleDB.mdl_grade_items AS gi ON gi.id = gg.itemid
            JOIN
                $this->moodleDB.mdl_course_categories AS cc ON cc.id = c.category
            LEFT JOIN
                $this->laravelDB.users AS lu ON u.icq = lu.id
                
            WHERE
                gi.courseid = c.id AND gi.itemtype = 'course'
                
            ORDER BY
                first_name
        ";
        // 251

        return DB::connection('moodle_mysql')->select($query);
    }

    private function getUserCourseDBDataV2() {
        $query = "
            SELECT
                u.firstname AS 'first_name',
                u.lastname AS 'last_name',
                c.fullname AS 'course',
                FROM_UNIXTIME(ue.timecreated, '%m/%d/%Y') AS 'enroll_date',
                IFNULL((SELECT DATE_FORMAT(MIN(FROM_UNIXTIME(log.time)),'%d/%m/%Y')
                    FROM $this->moodleDB.mdl_log log
                    WHERE log.course=c.id
                    AND log.userid=u.id), null
                ) AS 'first_access',
                (SELECT
                    IF
                        (ue.status=0, false, true)
                ) AS 'withdrawn',
                IFNULL(
                    (
                        SELECT
                            DATE_FORMAT(FROM_UNIXTIME(la.timeaccess), '%d/%m/%Y')
                        FROM
                            $this->moodleDB.mdl_user_lastaccess la
                        WHERE
                            la.userid=u.id AND la.courseid=c.id
                    ), null
                ) AS 'last_access',
            
            -- A count of the number of distinct days a student has entered a course
            IFNULL(
                (
                    SELECT 
                        COUNT(DISTINCT FROM_UNIXTIME(log.time, '%d/%m/%Y'))
                    FROM
                        $this->moodleDB.mdl_log log
                    WHERE
                        log.course=c.id AND log.userid=u.id AND log.action='view' AND log.module='course'
                    GROUP BY
                        u.id
                ), 0
            ) AS 'days_accessed', -- # Days Accessed
            
            IFNULL(
                (
                    SELECT
                        COUNT(gg.finalgrade) 
                    FROM
                        $this->moodleDB.mdl_grade_grades AS gg 
                    JOIN
                        $this->moodleDB.mdl_grade_items AS gi ON gg.itemid=gi.id
                    WHERE
                        gi.courseid=c.id AND gg.userid=u.id AND gi.itemtype='mod'
                    GROUP BY u.id, c.id
                ), 0
            ) AS 'activities_completed', -- Activities Completed
            
            IFNULL(
                (
                    SELECT
                        COUNT(gi.itemname) 
                    FROM
                        $this->moodleDB.mdl_grade_items AS gi 
                    WHERE
                        gi.courseid = c.id AND gi.itemtype='mod'
                ), 0
            ) AS 'activities_assigned', -- Activities Assigned
            
            
            -- If Activities completed = activities assigned, show date of last log entry. Otherwise, show percentage complete. If Activities Assigned = 0, show 'n/a'.
            (
                SELECT
                    IF(`activities_assigned`!= 0, (
                        SELECT 
                            IF((`activities_completed`) = (`activities_assigned`),
                                100,
                                -- Percent completed
                                (
                                    SELECT
                                        IFNULL(
                                            ROUND(
                                                (`activities_completed`)/(`activities_assigned`)*100, 0
                                            ), 0
                                        )
                                )
                    )
                ), null)
            ) AS 'percentage_completed', -- % of Course Completed
            
            IFNULL(
                CONCAT(
                    ROUND(
                      (
                        SELECT (
                            IFNULL(
                                (
                                SELECT
                                    SUM(gg.finalgrade)
                                FROM
                                    $this->moodleDB.mdl_grade_grades AS gg 
                                JOIN
                                    $this->moodleDB.mdl_grade_items AS gi ON gi.id = gg.itemid
                                WHERE
                                    gg.itemid = gi.id AND gi.courseid = c.id AND gi.itemtype = 'mod' AND gg.userid = u.id
                                GROUP BY
                                    u.id,c.id
                                ), 0
                            ) / (
                                SELECT
                                    SUM(gi.grademax)
                                FROM
                                    $this->moodleDB.mdl_grade_items AS gi
                                JOIN
                                    $this->moodleDB.mdl_grade_grades AS gg ON gi.id=gg.itemid
                                WHERE
                                    gg.itemid=gi.id AND gi.courseid=c.id AND gi.itemtype='mod' AND gg.userid=u.id AND gg.finalgrade IS NOT NULL
                                GROUP BY
                                    u.id,c.id
                            )
                        ) * 100
                      ), 0
                    ), '%'
                ), 'N/A'
            ) AS 'quality', -- Quality of Work to Date
            
            (
            SELECT
                IF(`activities_assigned`!='0',
                CONCAT(
                    IFNULL(
                    ROUND(
                        (
                        (
                            SELECT
                                gg.finalgrade/gi.grademax
                            FROM
                                $this->moodleDB.mdl_grade_items AS gi
                            JOIN
                                $this->moodleDB.mdl_grade_grades AS gg ON gg.itemid=gi.id
                            WHERE
                                gi.courseid=c.id
                            AND
                                gg.userid=u.id
                            AND
                                gi.itemtype='course'
                            GROUP BY
                                'gi.courseid'
                        ) * 100
                        ), 0
                    ), '0'
                    ), '%'
                ), 'N/A'
                )
            ) AS 'final_score' -- Final Score (incl xtra credit)
            
            
            FROM
                $this->moodleDB.mdl_user u
            JOIN
                $this->moodleDB.mdl_user_enrolments ue ON ue.userid=u.id
            JOIN
                $this->moodleDB.mdl_enrol e ON e.id=ue.enrolid
            JOIN
                $this->moodleDB.mdl_course c ON c.id = e.courseid
            JOIN
                $this->moodleDB.mdl_context AS ctx ON ctx.instanceid = c.id
            JOIN
                $this->moodleDB.mdl_role_assignments AS ra ON ra.contextid = ctx.id
            JOIN
                $this->moodleDB.mdl_role AS r ON r.id = e.roleid
            
            WHERE
                ra.userid=u.id AND ctx.instanceid=c.id
                -- AND ue.status='0' -- 0 FOR active, 1 FOR suspended. Leave commented OUT TO include BOTH.
                AND ra.roleid='5' -- 5 = student
                -- AND c.visible='1' -- 1 FOR course visible, 0 FOR hidden
            GROUP BY
                u.id, c.id
            ORDER BY
                u.lastname, u.firstname, c.fullname
        ";

        return DB::connection('moodle_mysql')->select($query);
    }

    private function getUserCourseDBData($item_type = 0, $user = false, $course = null) {
        switch($item_type) {
            case 0: // all items
                $item_type = "";
            break;
            case 1: // only course totals
                $item_type = "AND gi.itemtype = 'course'";
            break;
            case 2: // all items except course totals
                $item_type = "AND gi.itemtype != 'course'";
            break;
        }

        if($user) {
            $user = "AND u.icq = $user";
        } else {
            $user = "";
        }

        if($course) {
            $course = "AND c.id = $course";
        } else {
            $course = "";
        }

        $weightoverride = " AND gi.weightoverride != 1";

        $query = "
            SELECT 
                u.id AS 'moodle_id', u.icq AS 'laravel_id', u.firstname AS 'name' , u.lastname AS 'lastname', c.id AS 'course_id', c.fullname AS 'course', cc.name AS 'category', gi.itemtype AS 'item_type', gi.itemmodule as 'item_module',
                CASE
                    WHEN gi.itemtype = 'Course'    
                    THEN CONCAT(c.fullname, '  - Total de Curso')
                    ELSE gi.itemname 
                END AS 'item_name',
                IF(gg.finalgrade IS NULL, 'SC', ROUND(gg.finalgrade,2)) AS score,
                ROUND(gg.rawgrademax,2) AS max_grade,
                ROUND(gg.finalgrade / gg.rawgrademax * 100 ,2) AS percentage,
                IF (ROUND(gg.finalgrade / gg.rawgrademax * 100 ,2) >= $this->minApproveGrade, 1 , 0) AS status
                
            FROM
                $this->moodleDB.mdl_course AS c 
            JOIN
                $this->moodleDB.mdl_context AS ctx ON c.id = ctx.instanceid 
            JOIN
                $this->moodleDB.mdl_role_assignments AS ra ON ra.contextid = ctx.id 
            JOIN
                $this->moodleDB.mdl_user AS u ON u.id = ra.userid 
            JOIN
                $this->moodleDB.mdl_grade_grades AS gg ON gg.userid = u.id 
            JOIN
                $this->moodleDB.mdl_grade_items AS gi ON gi.id = gg.itemid 
            JOIN
                $this->moodleDB.mdl_course_categories AS cc ON cc.id = c.category 
            WHERE
                gi.courseid = c.id AND gi.itemname != 'Attendance' AND u.icq != '' $item_type $user $weightoverride $course
            ORDER BY
                `Name` ASC
        ";

        return DB::connection('moodle_mysql')->select($query);
    }

    public function getUsersEnrolledByCourse($course_id) {
        $users = [];
        $enrolled_to_course = RoleAssigment::where('roleid' , 5)
                            ->whereHas('user')
                            ->whereHas('context', function($context) use($course_id) {
                                $context->where('contextlevel', 50)
                                ->whereHas('instance', function($instance) use($course_id) {
                                    $instance->where('id', $course_id);
                                });
                            })
                            ->get();
        
        foreach($enrolled_to_course as $enroll){
            if($enroll->user->icq && !array_key_exists($enroll->user->icq, $users)) {
                $users[$enroll->user->icq] = $enroll->user;
            }
        }

        return $users;
    }

    private function getCourseFinalGrade($course_id, $user_id) {
        $query = "
        SELECT gg.id, gi.courseid, gg.userid, IF(gg.finalgrade IS NULL, 'SC', ROUND(gg.finalgrade,2)) AS 'finalgrade', gg.timemodified as 'tentative_date'
        FROM ".$this->moodleDB.".mdl_grade_grades gg
        INNER JOIN ".$this->moodleDB.".mdl_user u ON u.id = gg.userid
        INNER JOIN ".$this->moodleDB.".mdl_grade_items gi ON gi.id = gg.itemid
        INNER JOIN ".$this->moodleDB.".mdl_course c ON c.id = gi.courseid
            WHERE gi.itemtype = 'course'
            AND gi.courseid = $course_id
            AND u.icq = $user_id
        GROUP BY gi.courseid, gg.userid
        ";

        $data = DB::connection('moodle_mysql')->select($query);

        return $data;
    }

    private function getEnrollData($optional_where = '') {
        $query = "
            SELECT
                c.id AS course_id, u.icq AS 'laravel_id', c.fullname AS 'course', cc.name AS 'category'
            FROM 
                $this->moodleDB.mdl_role_assignments ra 
            JOIN
                $this->moodleDB.mdl_user u ON u.id = ra.userid
            JOIN
                $this->moodleDB.mdl_role r ON r.id = ra.roleid
            JOIN
                $this->moodleDB.mdl_context cxt ON cxt.id = ra.contextid
            JOIN
                $this->moodleDB.mdl_course c ON c.id = cxt.instanceid
            JOIN
                $this->moodleDB.mdl_course_categories AS cc ON cc.id = c.category
            WHERE
                ra.userid = u.id AND ra.contextid = cxt.id AND cxt.contextlevel = 50 AND cxt.instanceid = c.id AND  roleid = 5 $optional_where
            ORDER BY
                c.fullname
        ";

        return DB::connection('moodle_mysql')->select($query);

        // $is_enrolled = $this->getEnrollData("AND c.id = $course->courseid AND u.icq = $user->id");
        //  $enrolled_courses = $this->getEnrollData("AND u.icq = $user->id");
        // dd($enrolled_courses);
        // foreach($enrolled_courses as $enroll) {
        //     $final_grade_data = $this->getCourseFinalGrade($course->courseid, $row->moodle_id);
        //     if(!empty($final_grade_data)) { // Tiene calificación
        //         $tmp = $final_grade_data[0];
        //         $data[$laravel_id]['courses'][$course->courseid] = [
        //             'curso' => $course->curso,
        //             'calificacion' => $tmp->finalgrade
        //         ];
        //     } else { // No tiene calificación
        //         $data[$laravel_id]['courses'][$course->courseid] = [
        //             'curso' => $course->curso,
        //             'calificacion' => 'SC'
        //         ];
        //     }
        // }
    }
}