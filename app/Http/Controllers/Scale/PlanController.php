<?php

namespace App\Http\Controllers\Scale;

use DB;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DeletedReason;
use App\Models\Scale\Plan;
use App\Models\Scale\Course;
use App\Models\Scale\ModelData;
use App\Models\Moodle\Course as MCourse;
use Illuminate\Support\Facades\Auth;

class PlanController extends Controller
{
    public function __construct() {
        // ==== Admin Escalafón ====
        $this->middleware('permission:scale_admin')->only([
            'index', 'create', 'store', 'show', 'edit', 'update', 'destroy',
            'getMoodleCoursesByPlanAndJob', 'getCoursesByPlan', 'getMoodleCoursesByPlan'
        ]);
        // ==== Admin Escalafón ====
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plans = Plan::get();

        return view('scale.plan.index', compact(['plans']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::orderBy('name')->get();

        return view('scale.plan.create', compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        try {
            DB::beginTransaction();
            $data = $request->all();
            $data['budget'] = $data['budget']?$this->removeFormatCurrency($data['budget']):null;
            $plan = Plan::create($data);
            if(isset($data['courses'])) {
                foreach($data['courses'] as $course_id) {
                    $primary_keys = [
                        'model_name' => 'Course',
                        'model_id' => $course_id,
                        'scale_data_type' => 'plan',
                        'scale_data_id' => $plan->id
                    ];
                    $is_deleted = ModelData::where($primary_keys)->withTrashed()->first();
                    if($is_deleted && $is_deleted->trashed()) {
                        $is_deleted->restore();
                    }
                    ModelData::updateOrCreate($primary_keys);   
                }
            }
            DB::commit();
            flash('Se creó registro exitosamente!')->success();
            return redirect('escalafon/planes');
        } catch(\Throwable $th) {
            // dd($th->getMessage());
            DB::rollback();
            flash('Error al crear registro...'.$th)->error();
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $plan = Plan::findOrFail($id);
            $courses = Course::orderBy('name')->get();
            // dd($plan->courseData, $plan->getCoursesIDs());

            return view('scale.plan.edit', compact(['plan', 'courses']));
        } catch(\Throwable $th) {
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        try {
            DB::beginTransaction();
            $data = $request->all();
            $data['budget'] = $data['budget']?$this->removeFormatCurrency($data['budget']):null;
            $plan = Plan::findOrFail($id);
            $plan->update($data);
            if(!isset($data['courses'])) {
                $data['courses'] = [];
            }
            $primary_keys = [
                'model_name' => 'Course',
                'scale_data_type' => 'plan',
                'scale_data_id' => $id
            ];
            ModelData::where($primary_keys)->delete();
            // dd($data);
            foreach($data['courses'] as $course_id) {
                $primary_keys['model_id'] = $course_id;
                $is_deleted = ModelData::where($primary_keys)->withTrashed()->first();
                if($is_deleted && $is_deleted->trashed()) {
                    $is_deleted->restore();
                }
                ModelData::updateOrCreate($primary_keys);   
            }
            DB::commit();
            flash('Se actualizó plan exitosamente!')->success();
            return redirect('escalafon/planes');
        } catch(\Throwable $th) {
            // dd($th->getMessage());
            DB::rollback();
            flash('Error al actualizar plan')->error();
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $plan = Plan::findOrFail($id);

            $data = new DeletedReason;
            $data->user_id = Auth::user()->id;
            $data->reason = $request->del_reason;
            $plan->deleted_reason()->save($data);

            $plan->delete();
            DB::commit();
            flash('Se eliminó plan exitosamente!')->success();
        } catch(\Throwable $th) {
            DB::rollback();
            flash('Error al intentar borrar nivel')->error();
        }
        return redirect('escalafon/planes');
    }

    private function removeFormatCurrency($str) {
        return preg_replace('/[^0-9.]/', '', $str);
    }

    public function getMoodleCoursesByPlanAndJob($plan_id, $job_id, $user_id = null) {
        $courses = [];
        try {
            $plan = Plan::findOrFail($plan_id);
            // dd($plan->getMoodleCoursesIDs());
            foreach($plan->getMoodleCoursesIDs() as $course_id) {
                $course = Course::find($course_id);
                if($course && in_array($job_id, $course->getJobPositionIDs())) {
                    $moodle_course = MCourse::select('id', 'fullname')->where('id', $course->moodle_id)->first();
                    if($moodle_course) {
                        if($user_id) {
                            $user = User::findOrFail($user_id);
                            $courses[$moodle_course->id] = [
                                'name' => $moodle_course->fullname,
                                'enrolled' => in_array($course->moodle_id, array_keys($user->getMoodleEnrolledCourses()))?true:false
                            ];
                        } else {
                            $courses[$moodle_course->id] = [
                                'name' => $moodle_course->fullname
                            ];
                        }
                    }
                }
            }
        } catch(\Throwable $th) {
            return [];
        }
        return $courses;
    }

    public function getMoodleCoursesByPlan($plan_id) {
        try {
            $plan = Plan::findOrFail($plan_id);

            return Course::select('id', 'name', 'type')->whereIn('id', $plan->getMoodleCoursesIDs())->orderBy('name', 'ASC')->get();
        } catch(\Throwable $th) {
            return [];
        }
    }

    public function getCoursesByPlan($plan_id) {
        try {
            $plan = Plan::findOrFail($plan_id);

            return Course::select('id', 'name', 'moodle_id', 'type')->whereIn('id', $plan->getCoursesIDs())->orderBy('name', 'ASC')->get();
        } catch(\Throwable $th) {
            return [];
        }
    }
}
