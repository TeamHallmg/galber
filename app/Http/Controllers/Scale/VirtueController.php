<?php

namespace App\Http\Controllers\Scale;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DeletedReason;
use App\Models\Scale\Virtue;
use App\Models\Scale\Level;
use App\Models\Scale\VirtueScore;
use Illuminate\Support\Facades\Auth;

class VirtueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $virtues = Virtue::all();
        // $levels = Level::orderBy('level')->get();

        return view('scale.virtue.index', compact(['virtues']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $levels = Level::orderBy('level')->get();

        return view('scale.virtue.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        try {
            DB::beginTransaction();
            $virtue = new Virtue;
            $virtue->name = $request->name;
            $virtue->description = $request->description;
            $virtue->save();
            // foreach($request->level_id as $key => $id) {
            //     $virtue_score = new VirtueScore;
            //     $virtue_score->level_id = $id;
            //     $virtue_score->virtue_id = $virtue->id;
            //     $virtue_score->min_score = $request->min_score[$id];
            //     $virtue_score->max_score = $request->max_score[$id];
            //     $virtue_score->save();
            // }
            DB::commit();
            flash('Se creó valor exitosamente!')->success();
            return redirect('escalafon/virtudes');
        } catch(\Throwable $th) {
            // dd($th->getMessage());
            DB::rollback();
            flash('Error al crear valor')->error();
            return redirect('escalafon/virtudes/create')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $virtue = Virtue::findOrFail($id);
        // $levels = Level::orderBy('level')->get();

        return view('scale.virtue.edit', compact('virtue'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $virtue = Virtue::find($id);
            $virtue->name = $request->name;
            $virtue->description = $request->description;
            $virtue->save();
            // foreach($request->level_id as $key => $id) {
            //     $virtue_score = VirtueScore::find($request->virtue_score_id[$id]);
            //     $virtue_score->level_id = $id;
            //     $virtue_score->virtue_id = $virtue->id;
            //     $virtue_score->min_score = $request->min_score[$id];
            //     $virtue_score->max_score = $request->max_score[$id];
            //     $virtue_score->save();
            // }
            DB::commit();
            flash('Se actualizó valor exitosamente!')->success();
            return redirect('escalafon/virtudes');
        } catch(\Throwable $th) {
            // dd($th->getMessage());
            DB::rollback();
            flash('Error al actualizar valor')->error();
            return redirect('escalafon/virtudes/'.$id.'/edit')->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $virtue = Virtue::find($id);

            $data = new DeletedReason;
            $data->user_id = Auth::user()->id;
            $data->reason = $request->del_reason;
            $virtue->deleted_reason()->save($data);

            $virtue->delete();
            DB::commit();
            flash('Se eliminó virtud exitosamente!')->success();
            return redirect('escalafon/virtudes');
        } catch(\Throwable $th) {
            DB::rollback();
            // dd($th->getMessage());
            flash('Error al intentar borrar virtud')->error();
            return redirect('escalafon/virtudes');
        }
    }
}
