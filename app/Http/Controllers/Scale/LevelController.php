<?php

namespace App\Http\Controllers\Scale;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DeletedReason;
use App\Models\Moodle\Course as MCourse;
use App\Models\Scale\Course;
use App\Models\Scale\Level;
use App\Models\Scale\LevelCourse;
use Illuminate\Support\Facades\Auth;
use stdClass;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $levels = Level::all();

        return view('scale.level.index', compact(['levels']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $moodle_courses = MCourse::select('id', 'fullname')->get();
        // $courses = Course::all();

        return view('scale.level.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        try {
            DB::beginTransaction();
            $level = new Level();
            $level->name = $request->name;
            $level->virtues_score_from = $request->virtues_score_from;
            $level->virtues_score_to = $request->virtues_score_to;
            $level->courses_score_from = $request->courses_score_from;
            $level->courses_score_to = $request->courses_score_to;
            $level->commission_from = $this->removeFormatCurrency($request->commission_from);
            $level->commission_to = $this->removeFormatCurrency($request->commission_to);
            $level->save();

            DB::commit();
            flash('Se creó nivel exitosamente!')->success();
            return redirect('escalafon/niveles');
        } catch(\Throwable $th) {
            // dd($th->getMessage());
            DB::rollback();
            flash('Error al crear nivel')->error();
            return redirect('escalafon/niveles/create')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $level = Level::findOrFail($id);

        return view('scale.level.edit', compact(['level']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        try {
            DB::beginTransaction();
            $level = Level::find($id);
            $level->name = $request->name;
            $level->virtues_score_from = $request->virtues_score_from;
            $level->virtues_score_to = $request->virtues_score_to;
            $level->courses_score_from = $request->courses_score_from;
            $level->courses_score_to = $request->courses_score_to;
            $level->commission_from = $this->removeFormatCurrency($request->commission_from);
            $level->commission_to = $this->removeFormatCurrency($request->commission_to);
            $level->save();
            DB::commit();
            flash('Se actualizó nivel exitosamente!')->success();
            return redirect('escalafon/niveles');
        } catch(\Throwable $th) {
            // dd($th->getMessage());
            DB::rollback();
            flash('Error al actualizar nivel')->error();
            return redirect('escalafon/niveles/create')->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $level = Level::find($id);

            $data = new DeletedReason;
            $data->user_id = Auth::user()->id;
            $data->reason = $request->del_reason;
            $level->deleted_reason()->save($data);

            $level->delete();
            DB::commit();
            flash('Se eliminó nivel exitosamente!')->success();
            return redirect('escalafon/niveles');
        } catch(\Throwable $th) {
            DB::rollback();
            // dd($th->getMessage());
            flash('Error al intentar borrar nivel')->error();
            return redirect('escalafon/niveles');
        }
    }

    private function removeFormatCurrency($str) {
        return preg_replace('/[^0-9.]/', '', $str);
    }
}
