<?php

namespace App\Http\Controllers\Scale;

use DB;
use App\Employee;
use App\Models\JobPosition;
use App\Models\Scale\Course;
use App\Models\Scale\CourseData;
use Illuminate\Http\Request;
use App\Models\DeletedReason;
use App\Models\Scale\ModelData;
use App\Models\Scale\LevelCourse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Scale\CourseJobposition;
use App\Models\Moodle\Course as MCourse;
use App\Models\Moodle\CourseCategory;
use App\Http\Controllers\MoodleManager;
use App\Models\EvaluacionDesempeno\Admin\Factores;

class CourseController extends Controller
{
    protected $moodleManager;

    public function __construct(MoodleManager $moodleManager){
        $this->look_for = ['Cursos Escalafón'];
        $this->extra_data = [
            'trainer' => 'type_id', 'competence' => 'type_id', 'extern_rfc' => 'data'
        ];
        $this->moodleManager = $moodleManager;

        // ==== Admin Escalafón ====
        $this->middleware('permission:scale_admin')->only([
            'index', 'create', 'getMoodleCoursesByCategory', 'store', 'show', 'edit', 'update', 'destroy', 'adminCourseJobPositions',
            'storeCourseJobPosition', 'updateCourseJobPositions', 'deleteCourseJobPositions', 'enrollUserToCourse'
        ]);
        // ==== Admin Escalafón ====
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::get();
        
        return view('scale.course.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $array = Course::where('moodle_id', '!=', null)->pluck('moodle_id')->toArray();
        // dd($array);

        $look_for = $this->look_for;

        // $moodle_courses = MCourse::select('id', 'fullname')->whereNotIn('id', $array)->get();
        // ->whereHas('m_category', function($mc) use ($look_for) {
        //     $mc->whereIn('name', $look_for);
        // })

        $moodle_categories = CourseCategory::get();
        
        $employees = Employee::get();
        $factors = Factores::orderBy('orden', 'ASC')->get();

        return view('scale.course.create', compact('moodle_categories', 'employees', 'factors'));
    }

    public function getMoodleCoursesByCategory($category_id, $course_id = null) {
        $array = Course::where('moodle_id', '!=', null)->pluck('moodle_id')->toArray();
        if(!is_null($course_id) && (($key = array_search($course_id, $array)) !== false)) {
            unset($array[$key]);
        }
        return MCourse::select('id', 'fullname')->whereNotIn('id', $array)
                ->whereHas('m_category', function ($mc) use ($category_id) {
                    $mc->where('id', $category_id);
                })->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            // dd($data, $this->removeFormatCurrency($data['budget']));
            $category_id = $category = $moodle_id = $name = null;
            // $course_data = [];

            if($data['course'] == 'M') {
                $moodle_category = CourseCategory::find($request->moodle_category);
                $category_id = $data['moodle_category'];
                $category = $moodle_category->name;
                $moodle_course = MCourse::find($request->moodle_course);
                $moodle_id = $data['moodle_course'];
                $name = $moodle_course->fullname;
            } elseif($data['course'] == 'N') {
                $category = $data['category'];
                $name = $data['name'];
            }

            $course = Course::create([
                'category_id' => $category_id,
                'category' => $category,
                'code' => $data['code'],
                'moodle_id' => $moodle_id,
                'name' => $name,
                'type' => $data['type'],
                'modality' => $data['modality'],
                'min_grade' => $data['min_grade'] ?? 0,
                'priority' => $data['priority'] ?? 0,
                'institution' => $data['institution'],
                // 'instructor_name' => $data['instructor_name'],
                'validity' => $data['validity'],
                'duration' => $data['duration'],
                'expire_at' => $data['expire_at'],
                // 'budget' => $data['budget']?$this->removeFormatCurrency($data['budget']):null,
                'observations' => $data['observations']
            ]);

            foreach($this->extra_data as $type => $column) {
                if($request->has($type)) {
                    foreach($data[$type] as $key => $row) {
                        if(is_null($row)) {
                            continue;
                        }
                        $course_data = CourseData::create([
                            'scale_course_id' => $course->id,
                            'type' => $type,
                            $column => $row
                        ]);

                        if($type == 'extern_rfc') {
                            $course_data->update([
                                'extra3' => $data['instructor_name'][$key]
                            ]);
                        }
                    }
                }
            }
            // dd('commit');
            DB::commit();
            flash('Se creó curso exitosamente!')->success();
            return redirect('escalafon/cursos');
        } catch(\Throwable $th) {
            // dd($th->getMessage(), $th);
            DB::rollback();
            flash('Error al crear curso')->error();
            return redirect('escalafon/cursos/create')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $course = Course::findOrFail($id);

            $moodle_categories = CourseCategory::get();
            
            $employees = Employee::get();
            $factors = Factores::orderBy('orden', 'ASC')->get();

            return view('scale.course.edit', compact(['course', 'moodle_categories', 'employees', 'factors']));
        } catch(\Throwable $th) {
            // dd($th);
            flash('Curso no encontrado...')->error();
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        try {
            DB::beginTransaction();
            $data = $request->all();
            $category_id = $category = $moodle_id = $name = null;
            // $course_data = [];

            if($data['course'] == 'M') {
                $moodle_category = CourseCategory::find($request->moodle_category);
                $category_id = $data['moodle_category'];
                $category = $moodle_category->name;
                $moodle_course = MCourse::find($request->moodle_course);
                $moodle_id = $data['moodle_course'];
                $name = $moodle_course->fullname;
            } elseif($data['course'] == 'N') {
                $category = $data['category'];
                $name = $data['name'];
            }

            $course = Course::findOrFail($id);
            $last_priority = $course->priority;
            $new_priority = $data['priority'] ?? 0;
            $last_min_grade = $course->priority;
            $new_min_grade = $data['min_grade'] ?? 0;
            $course->update([
                'category_id' => $category_id,
                'category' => $category,
                'code' => $data['code'],
                'moodle_id' => $moodle_id,
                'name' => $name,
                'type' => $data['type'],
                'modality' => $data['modality'],
                'min_grade' => $data['min_grade'] ?? 0,
                'priority' => $new_priority,
                'institution' => $data['institution'],
                // 'instructor_name' => $data['instructor_name'],
                'validity' => $data['validity'],
                'duration' => $data['duration'],
                'expire_at' => $data['expire_at'],
                // 'budget' => $data['budget']?$this->removeFormatCurrency($data['budget']):null,
                'observations' => $data['observations']
            ]);

            CourseData::where('scale_course_id', $id)->delete();
            foreach($this->extra_data as $type => $column) {
                if($request->has($type)) {
                    foreach($data[$type] as $key => $row) {
                        if(is_null($row)) {
                            continue;
                        }
                        $course_data = CourseData::create([
                            'scale_course_id' => $id,
                            'type' => $type,
                            $column => $row
                        ]);
                        if($type == 'extern_rfc') {
                            $course_data->update([
                                'extra3' => $data['instructor_name'][$key]
                            ]);
                        }
                    }
                }
            }

            foreach($course->jobPositionData as $model_data) {
                if($model_data->priority == $last_priority) {
                    $model_data->priority = $new_priority;
                    $model_data->save();
                }
                if($model_data->score == $last_min_grade) {
                    $model_data->score = $new_min_grade;
                    $model_data->save();
                }
            }

            DB::commit();
            flash('Se actualizó curso exitosamente!')->success();
            return redirect('escalafon/cursos');
        } catch(\Throwable $th) {
            // dd($th->getMessage());
            DB::rollback();
            flash('Error al actualizar curso')->error();
            return redirect('escalafon/cursos/'.$id.'/edit')->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $course = Course::find($id);

            $data = new DeletedReason;
            $data->user_id = Auth::user()->id;
            $data->reason = $request->del_reason;
            $course->deleted_reason()->save($data);

            $course->delete();
            DB::commit();
            flash('Se eliminó curso exitosamente!')->success();
            return redirect('escalafon/cursos');
        } catch(\Throwable $th) {
            DB::rollback();
            // dd($th->getMessage());
            flash('Error al intentar borrar curso')->error();
            return redirect('escalafon/cursos');
        }
    }

    public function adminCourseJobPositions($id)
    {
        try {
            $course = Course::findOrFail($id);
            // dd($course->job_positions);
            // dd($course->getJobIDs());
            // dd($course->getJobNames());
            $jobpositions = JobPosition::whereNotIn('name', $course->getJobNames())->orderBy('name', 'ASC')->pluck('name', 'id')->unique();

            return view('scale.course.jobposition', compact('course', 'jobpositions'));
        } catch(\Throwable $th) {
            // dd($th);
            flash('Error al buscar la información...')->error();
            return back();
        }
    }

    public function storeCourseJobPosition(Request $request) {
        $data = $request->all();
        \DB::beginTransaction();
        try {
            $curso = Course::findOrfail($data['course_id']);
            $primary_keys = [
                'model_name' => 'JobPosition',
                'model_id' => $data['job_position_id'],
                'scale_data_type' => 'course',
                'scale_data_id' => $curso->id
            ];
            $is_deleted = ModelData::where($primary_keys)->withTrashed()->first();
            if($is_deleted && $is_deleted->trashed()) {
                $is_deleted->restore();
            }
            ModelData::updateOrCreate($primary_keys, [
                'score' => $data['calificacion'],
                'priority' => $data['prioridad']
            ]);
            flash('Se agregó el curso al puesto correctamente')->success();
            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollback();
            // dd($th);
            flash('Error al gestionar curso al puesto')->error();
        }
        return redirect()->back();
    }

    public function updateCourseJobPositions(Request $request, $course_id){
        $data = $request->all();

        \DB::beginTransaction();
        try {
            $curso = Course::findOrfail($course_id);

            foreach($data['jobposition_id'] as $job_position_id) {
                $primary_keys = [
                    'model_name' => 'JobPosition',
                    'model_id' => $job_position_id,
                    'scale_data_type' => 'course',
                    'scale_data_id' => $course_id
                ];
                $is_deleted = ModelData::where($primary_keys)->withTrashed()->first();
                if($is_deleted && $is_deleted->trashed()) {
                    $is_deleted->restore();
                }
                ModelData::updateOrCreate($primary_keys, [
                    'score' => $data['calificacion'],
                    'priority' => $data['prioridad']
                ]);   
            }
            flash('Se agregaron los puestos al curso correctamente')->success();
            // $insertData = [];
            // for($i = 0; $i < count($data['puesto_id']); $i++)
            //     $insertData[$data['puesto_id'][$i]] = ['calificacion' => $data['calificacion'][$i]];
            // dd($insertData);
            // $curso->puestos()->syncWithoutDetaching($insertData);
            // $curso->puestos()->sync($insertData);
            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollback();
            // dd($th);
            flash('Error al gestionar el puesto al curso')->error();
        }
        return redirect()->back();
    }

    public function deleteCourseJobPositions($course_id, $job_position_id){
        \DB::beginTransaction();
        try {
            $curso = Course::findOrFail($course_id);

            $job_position = JobPosition::findOrFail($job_position_id);
            $job_position_ids = JobPosition::where('name', $job_position->name)->get()->pluck('id');
            // dd($job_position_ids);
            ModelData::where([
                'model_name' => 'JobPosition',
                'scale_data_type' => 'course',
                'scale_data_id' => $course_id,
            ])->whereIn('model_id', $job_position_ids)->delete();
            flash('Se eliminó el puesto del curso correctamente')->success();
            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollback();
            // dd($th);
            flash('Error al gestionar el puesto al curso')->error();
        }
        return redirect()->back();
    }

    public function enrollUserToCourse(Request $request) {
        try {
            $data = $request->all();
            // dd($data);
            $tmp_course = Course::where('moodle_id', $data['course_id'])->first();
            $courses[$tmp_course->moodle_id] = $tmp_course->name;
            $myCourses = $this->moodleManager->getCourses($data['user_id']);
            // dd($data['user_id'], $myCourses, $courses);
            $data['price'] = $data['price']?$this->removeFormatCurrency($data['price']):0;
            $log = $this->moodleManager->attachCourses($data['user_id'], $myCourses, $courses, $data['plan_id'], $data['price']);
            // dd($log);
            flash('Matriculación realizada con éxito')->success();
        } catch(\Throwable $th) {
            // dd($th);
            $log = 'error';
            flash('Error al matricular... '.$th->getMessage)->error();
        }
        return back();
        
    }

    private function removeFormatCurrency($str) {
        return preg_replace('/[^0-9.]/', '', $str);
    }

}
