<?php

namespace App\Http\Controllers\Scale;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Scale\Course;
use App\Models\Scale\Virtue;
use App\Models\Scale\Level;
use App\Models\Scale\EmployeeData;
use App\Models\JobPosition;
use App\Models\Direction;
use App\User;
use App\Employee;
use DB;
use Exception;
use Carbon\Carbon;

class ScaleController extends Controller
{
    public function __construct()
    {
        $this->level_colors = [
            1 => 'bg-training',
            2 => 'bg-junior',
            3 => 'bg-senior',
            4 => 'bg-master'
        ];
    }

    public function jobPositions() {
        $job_positions = JobPosition::get();

        return view('scale.jobposition.index', compact('job_positions'));
    }

    // Funciones del escalafón de Guíarvis de aquí hacía abajo

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::whereHas('employee', function($employee) {
                        $employee->whereHas('jobPosition', function($jobPosition) {
                                $jobPosition->where('name', 'Asesor');
                            });
                    })->orderBy('id', 'DESC')->get();

        $virtues = Virtue::get();
        $courses = Course::get();

        foreach ($usuarios as $key => $usuario) {
            $direction = Direction::select('id','name')->where('id',$usuario->employee->direccion)->first();
            $job = JobPosition::select('id','name')->where('id',$usuario->employee->job_position_id)->first();
            if(!is_null($direction)) {
                $usuario->employee->direccion = $direction->name;
            }
            if(!is_null($job)) {
                $usuario->employee->job_position_id = $job->name;
            }

            $data = [
                'virtues' => $this->getUserVirtuesData($usuario->employee->id, $virtues),
                'courses' => $this->getUserCoursesData($usuario->employee->id, $courses)
            ];

            try {
                $usuario = $this->getScaleLevel($usuario, $data);
            } catch(\Throwable $th) {
                flash('Error inesperado al asignar un nivel de escalafón un usuario, favor de reportar el problema a soporte...')->error();
                return back();
            }
        }

        return view('scale.employee.index', compact(['usuarios']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function userScale() {
        try {
            $user = User::findOrFail(Auth::user()->id);

            if(is_null($user->employee_id) || !Auth::user()->isUserAdviser()) {
                throw new Exception('Error en el proceso...'); // Si no eres empleado o asesor
            }     
            
            $virtues = Virtue::get();
            $courses = Course::get();
            // $data = ['virtues' => ['total_score' => 0, 'count' => 0]];
            $data = [
                'virtues' => $this->getUserVirtuesData($user->employee->id, $virtues),
                'courses' => $this->getUserCoursesData($user->employee->id, $courses)
            ];

            $user = $this->getScaleLevel($user, $data);
            $user = $this->getUserData($user);

            // dd($user, $data);
        
            return view('scale.employee.show', compact('user', 'virtues', 'courses'));
        } catch(\Throwable $th) {
            // dd($th);
            flash('Error en el proceso...')->error();
            return redirect('tablero');
        }
    }

    public function myEmployeesScale() {
        try {
            if(!Auth::user()->doesUserHaveAdvisersInHisCharge()) {
                flash('No tienes asesores a tu cargo...')->error();
                return redirect('tablero');
            }
            $user = Auth::user();
            $virtues = Virtue::get();
            $my_employees_users = User::whereHas('employee', function($employee) use ($user) {
                $employee->where('jefe', $user->employee->rfc)->whereHas('jobPosition', function($jobPosition) {
                    $jobPosition->where('name', 'Asesor');
                });
            })->get();
            $user = $this->getUserData($user);

            return view('scale.employee.group', compact('user', 'my_employees_users', 'virtues'));
        } catch(\Throwable $th) {
            flash('Error al ingresar al escalafón de tus asesores...'.$th->getMessage())->error();
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function editEmployee($id) {
        try {
            $user = User::where('employee_id',$id)->with(['employee' => function($q) {
                $q->withTrashed();
            }])
            ->whereHas('employee', function($q){
                $q->withTrashed();
            })
            ->orderBy('id', 'DESC')->withTrashed()->firstOrFail();

            $virtues = Virtue::get();
            $courses = Course::get();
            $data['virtues'] = $this->getUserVirtuesData($id, $virtues);
            $data['courses'] = $this->getUserCoursesData($id, $courses);

            // dd($virtues);
            $user = $this->getScaleLevel($user, $data);
            $user = $this->getUserData($user);

            // dd($user, $virtues, $courses, $data['virtues']);

            return view('scale.employee.edit', compact('user', 'virtues', 'courses'));
        } catch(\Throwable $th) {
            flash('Error al ingresar al escalafón del usuario...'.$th->getMessage())->error();
            return back();
        }
    }

    private function getScaleLevel($user, $data) {
        $now = Carbon::now();
        $scale_levels = Level::get();
        $virtues_score = round(($data['virtues']['total_score']*10)/$data['virtues']['count']);
        $courses_score = 0;

        try {
            $base_level = Level::findOrFail(1);
        } catch(\Throwable $th) {
            flash('Error al ingresar a la administración del escalafón de usuarios debido al nivel base, favor de reportar el problema a soporte...')->error();
            return back();
        }
        
        // $user->virtues_scale = ['name' => $base_level->name, 'level' => 1, 'score' => 0];
        // $user->courses_scale = ['name' => $base_level->name, 'level' => 1, 'score' => 0];
        $global_level = ['name' => null, 'level' => null];

        foreach($data['courses'] as $employee_course) {
            $course = Course::findOrFail($employee_course['scale_data_id']);
            $date_start = Carbon::parse($employee_course['date_start']);

            $diff = $date_start->diffInMonths($now);

            if($diff < $course->validity) {
                $courses_score += $course->value;
            }
        }

        // $user->virtues_scale['color'] = $this->getLevelColor($user->virtues_scale['level']);
        // $user->virtues_scale['code'] = $user->virtues_scale['name'][0];
        // $user->courses_scale['color'] = $this->getLevelColor($user->courses_scale['level']);
        // $user->courses_scale['code'] = $user->courses_scale['name'][0];

        // dd($virtues_score, $scale_levels);
        $debug_levels = [];
        foreach($scale_levels as $level) {
            if($virtues_score >= $level->virtues_score_from && $virtues_score <= $level->virtues_score_to) {
                // dd('cumple en este nivel: ', $level);
                $user->virtues_scale = [
                    'name' => $level->name,
                    'level' => $level->id,
                    'score' => $virtues_score,
                    'color' => $this->level_colors[$level->id],
                    'code' => $level->name[0]
                ];
                if($level->id <= $global_level['level'] || $global_level['level'] == null) {
                    $global_level = ['name' => $level->name, 'level' => $level->id];
                }
            }
            // $debug_levels[] = [
            //     'nivel' => $level->name,
            //     'mayor a' => ($virtues_score >= $level->virtues_score_from),
            //     'es mayor a' => $level->virtues_score_from,
            //     'menor a' => ($virtues_score <= $level->virtues_score_to),
            //     'es menor a' => $level->virtues_score_to
            // ];

            if($courses_score >= $level->courses_score_from && $courses_score <= $level->courses_score_to) {
                $user->courses_scale = [
                    'name' => $level->name,
                    'level' => $level->id,
                    'score' => $courses_score,
                    'color' => $this->level_colors[$level->id],
                    'code' => $level->name[0]
                ];
                if($level->id <= $global_level['level'] || $global_level['level'] == null) {
                    $global_level = ['name' => $level->name, 'level' => $level->id];
                }
            }
        }
        // dd($virtues_score, $debug_levels, 'en ninguno');

        if(is_null($global_level['level'])) {
            $global_level = ['name' => $base_level->name, 'level' => $base_level->id];
        }

        $global_level['color'] = $this->level_colors[$global_level['level']];
        $global_level['code'] = $global_level['name'][0];

        $user->global_level = $global_level;

        return $user;
    }

    private function getUserVirtuesData($id, $virtues) {
        $employee_virtues = EmployeeData::select('scale_data_id', 'score')->where([
            'employee_id' => $id,
            'scale_data_type' => 'virtue'
        ])->whereHas('virtue')->get()->toArray();

        $data = ['total_score' => 0, 'count' => 0];

        foreach($virtues as $virtue) {
            $array_column = array_column($employee_virtues, 'scale_data_id');
            if(in_array($virtue->id, $array_column)) {
                $key = array_search($virtue->id, $array_column);
                $virtue->score = $employee_virtues[$key]['score'];
                $data['total_score'] += $virtue->score;
            } else {
                $virtue->score = 0.00;
                $data['total_score'] += 0;
            }
        }
        $data['count'] += $virtues->count();

        return $data;
    }

    private function getUserCoursesData($id, $courses) {
        $employee_courses = EmployeeData::select('scale_data_id', 'date_start')->where([
            'employee_id' => $id,
            'scale_data_type' => 'course'
        ])->whereHas('course')->get()->toArray();
        foreach($courses as $course) {
            $array_column = array_column($employee_courses, 'scale_data_id');
            if(in_array($course->id, $array_column)) {
                $key = array_search($course->id, $array_column);
                $course->date_start = $employee_courses[$key]['date_start'];
            } else {
                $course->date_start = '';
            }
        }

        return $employee_courses;
    }

    private function getUserData($user) {
        if(isset($user->employee->jobPosition->area->name))
            $user->nombre_area = $user->employee->jobPosition->area->name;
        else 
            $user->nombre_area = 'N/A';
        // ====================
        if(isset($user->employee->jobPosition->area->department->name))
            $user->nombre_departamento = $user->employee->jobPosition->area->department->name;
        else 
            $user->nombre_departamento = 'N/A';
        // ====================
        if(isset($user->employee->jobPosition->area->department->direction->name))
            $user->nombre_direccion = $user->employee->jobPosition->area->department->direction->name;
        else 
            $user->nombre_direccion = 'N/A';

        return $user;
    }

    public function updateEmployee(Request $request, $id) {
        $data = $request->all();
        try {
            DB::beginTransaction();

            $employee = Employee::findOrFail($id);
            if(isset($data['virtue'])) {
                foreach ($data['virtue'] as $virtue_id => $score) {
                    EmployeeData::updateOrCreate([
                        'employee_id' => $id,
                        'scale_data_type' => 'virtue',
                        'scale_data_id' => $virtue_id
                    ], [
                        'score' => !is_null($score)?$score:0.00
                    ]);
                }
            }

            if(!isset($data['course'])) {
                $data['course'] = [];
            } else { 
                $data['course'] = array_filter($data['course']);
            }

            $old_courses = EmployeeData::where([
                        'employee_id' => $id,
                        'scale_data_type' => 'course'
                        ])->whereNotIn('scale_data_id', array_keys($data['course']))->delete();

            foreach($data['course'] as $course_id => $coursed_date) {
                EmployeeData::withTrashed()->updateOrCreate([
                    'employee_id' => $id,
                    'scale_data_type' => 'course',
                    'scale_data_id' => $course_id,
                ], [
                    'date_start' => $coursed_date,
                    'deleted_at' => null
                ]);
            }

            DB::commit();
            flash('Se actualizaron los datos del escalafón exitosamente!')->success();
            return redirect('escalafon/editar/'.$id);
        } catch(\Throwable $th) {
            DB::rollback();
            // dd($th);
            flash('Error al actualiar datos del escalfón')->error();
            return back();
        }
    }

    public function updateEmployees(Request $request) {
        $data = $request->all();
        try {
            DB::beginTransaction();

            // dd($data);
            if(isset($data['virtue'])) {
                foreach ($data['virtue'] as $employee_id => $row) {
                    foreach($row as $virtue_id => $score) {
                        EmployeeData::updateOrCreate([
                            'employee_id' => $employee_id,
                            'scale_data_type' => 'virtue',
                            'scale_data_id' => $virtue_id
                        ], [
                            'score' => !is_null($score)?$score:0.00
                        ]);
                    }
                }
            }

            DB::commit();
            flash('Se actualizaron los datos del escalafón exitosamente!')->success();
            return redirect('escalafon/mis_asesores');
        } catch(\Throwable $th) {
            DB::rollback();
            // dd($th);
            flash('Error al actualiar datos del escalfón')->error();
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
