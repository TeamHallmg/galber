<?php

namespace App\Http\Controllers\Scale;

use DB;
use App\User;
use App\Employee;
use App\Models\JobPosition;
use App\Models\DeletedReason;
use App\Models\Scale\Plan;
use App\Models\Scale\Course;
use App\Models\Scale\CourseData;
use App\Models\Scale\ModelData;
use App\Models\Scale\LevelCourse;
use App\Models\Scale\CourseJobposition;
use App\Models\Moodle\User as MoodleUser;
use App\Models\Moodle\Enrol;
use App\Models\Moodle\Course as MCourse;
use App\Models\Moodle\RoleAssigment;
use App\Models\Moodle\UserEnrolment;
use App\Models\Moodle\CourseCategory;
use App\Models\EvaluacionDesempeno\Admin\Factores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\Http\Controllers\MoodleManager;

class EnrollController extends Controller
{
    protected $moodleManager;

    public function __construct(MoodleManager $moodleManager){
        $this->look_for = ['Cursos Escalafón'];
        $this->extra_data = [
            'trainer' => 'type_id', 'competence' => 'type_id', 'extern_rfc' => 'data'
        ];
        $this->moodleManager = $moodleManager;

        // ==== Admin Escalafón ====
        $this->middleware('permission:scale_admin')->only([
            'index', 'create', 'store', 'show', 'edit', 'update', 'destroy', 'summary'
        ]);
        // ==== Admin Escalafón ====
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plans = Plan::trainingPlans()->orderBy('name', 'ASC')->get();
        $programs = Plan::trainingPrograms()->orderBy('name', 'ASC')->get();
        $courses = [
            'moodle' => Course::internOnly()->orderBy('name', 'ASC')->get(),
            'extern' => Course::externOnly()->orderBy('name', 'ASC')->get()
        ];
        $employees = Employee::whereHas('user')->get();
        
        return view('scale.enroll.index', compact('plans', 'programs', 'courses', 'employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    public function summary(Request $request) {
        try {
            $data = $request->all();
            $data['plan_name'] = null;

            if($data['employees'] == '') {
                $employees = array();
            } else {
                $employees = explode(',', $data['employees']);
            }

            if($data['use_plan']) {
                $plan = Plan::findOrFail($data['plan']);
                $data['plan_name'] = $plan->name;
            }

            $program = Plan::findOrFail($data['program']);
            $data['program_name'] = $program->name;

            // dd($data);

            $course = Course::findOrFail($data['course']);
            $data['course_name'] = $course->name;
            $data['moodle_course'] = $course->isMoodleCourse();

            if($data['price']) {
                $data['formated_price'] = $data['price'];
                $data['price'] = $this->removeFormatCurrency($data['price']);
                $data['price'] = doubleval($data['price']);
            } else {
                $data['formated_price'] = null;
                $data['price'] = 0;
            }

            $employees = Employee::whereIn('id', $employees)->whereHas('user')->get();
            foreach($employees as $employee) {
                // dd($employee->user->getMoodleEnrolledCourses());
                $bool = array_key_exists($course->moodle_id, $employee->user->getMoodleEnrolledCourses());
                if($bool) {
                    $employee->enrolled = true;
                } else {
                    $employee->enrolled = false;
                }
            }

            return view('scale.enroll.resume', compact('data', 'employees'));
        } catch(\Throwable $th) {
            flash('Error al intentar visualizar el resumen de matriculación')->error();
            // dd($th, $data);
            return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $course_cant_enroll = false;
        $message = '';
        try {
            $data = $request->all();
            if($data['employees_id'] == '') {
                $employees = array();
            } else {
                $employees = explode(',', $data['employees_id']);
            }
            $employees = Employee::whereIn('id', $employees)->whereHas('user')->get();
            if($data['use_plan']) {
                $plan = Plan::findOrFail($data['plan_id']);
                $plan_id = $plan->id;
            } else {
                $plan_id = null;
            }
            $program = Plan::findOrFail($data['program_id']);
            $program_id = $program->id;
            $course = Course::findOrFail($data['course_id']);
            $courses[$course->moodle_id] = $course->name;
        } catch(\Throwable $th) {
            // dd($th);
            flash('Error al intentar matricular...'.$th->getMessage())->error();
            return back();
        }
        
        foreach($employees as $key => $employee) {
            try {
                DB::beginTransaction();
                $user = $employee->user;
                $mdl_user = MoodleUser::where('icq', $user->id)->first();
                $price = $data['price'];
                $moodle_enroll = $this->enrollToCourse($mdl_user->id, $course->moodle_id, $user->id, $plan_id, $price, $program_id);

                switch($moodle_enroll) {
                    case 0: // Curso no permite matricular
                        $course_cant_enroll = true;
                    break;
                    case 1: // Usuario ya matriculado
                        $message .= ($key+1).'.- Usuario '.$employee->getFullNameAttribute().' previamente matriculado.<br>';
                    break;
                    case 2: // Usuario matriculado exitosamente
                        $message .= ($key+1).'.- Usuario '.$employee->getFullNameAttribute().' matriculado correctamente.<br>';
                    break;
                }
                DB::commit();
            } catch(\Throwable $th) {
                DB::rollback();
                // dd($th, $mdl_user, $course, $user, $plan_id, $price, $program_id);
                $message .= ($key+1).'.- El usuario '.$employee->getFullNameAttribute().' no pudo ser matriculado... ('.$th->getMessage().')<br>';
            }
        }

        if($course_cant_enroll) {
            flash('No se pudieron matricular a los usuarios debido a que el curso no permite esta acción...')->warning();
        } else {
            flash($message)->important();
        }

        return redirect('escalafon/matriculacion');
        
    }

    private function enrollToCourse($userid, $courseid, $laravel_id = null, $plan_id = null, $price = 0, $program_id = null){
        $course = MCourse::find($courseid);
        $context = $course->context(50)->id;

        $model_data = [
            'model_name' => 'User',
            'model_id' => $laravel_id,
            'scale_data_type' => 'moodle_course',
            'scale_data_id' => $courseid,
            'extra1' => $plan_id,
            'extra2' => $price,
            'extra3' => $program_id
        ];
        // dd($model_data);
        
        $enrol = Enrol::where('enrol', 'manual')
        ->where('courseid', $courseid)
        ->first();
            
        if($enrol) {
            $enrolled = RoleAssigment::where('roleid', 5)
            ->where('contextid', $context)
            ->where('userid', $userid)
            ->first();

            if(!$enrolled) {
                RoleAssigment::updateOrCreate([
                    'roleid' => 5,
                    'contextid' => $context,
                    'userid' => $userid
                ], [
                    'timemodified' => strtotime('now')
                ]);
                UserEnrolment::create([
                    'enrolid' => $enrol->id,
                    'userid' => $userid,
                    'timestart' => strtotime('now'),
                    'timeend' => 0
                ]);
                ModelData::create($model_data);
                return 2;
            } else {
                return 1;
            }
        } else {
            return 0;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // 
    }

    private function removeFormatCurrency($str) {
        return preg_replace('/[^0-9.]/', '', $str);
    }

    // public function getUsersStatusByCourse($course_id = null) {
    //     $data = [
    //         'data' => []
    //     ];
    //     $employees = Employee::get();
    //     // where('id', 50)->
    //     foreach ($employees as $key => $employee) {
    //         $data['data'][] = [
    //             'idemp' => $employee->idempleado,
    //             'name' => $employee->getFullNameAttribute(),
    //             'enrolled' => 'Simón'
    //         ];
    //     }
    //     return json_encode($data);
    // }
}
