<?php

namespace App\Http\Controllers;

use App\User;
use App\Employee;

use App\Models\Region;
use App\Models\Schedule;
use App\Models\Workshift;
use Illuminate\Http\Request;
use App\Models\WorkshiftFixed;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ScheduleRequest;

class ScheduleController extends Controller
{
    protected $daysOfWeek;

    public function __construct(){
        $this->middleware('permission:admin');
        $this->daysOfWeek = [
            'Mon' => 'Lunes',
            'Tue' => 'Martes',
            'Wed' => 'Miercoles',
            'Thu' => 'Jueves',
            'Fri' => 'Viernes',
            'Sat' => 'Sabado',
            'Sun' => 'Domingo',
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regions = Region::where('workshift','5x2')->get();
        return view('vacations.admin.schedule.index', compact('regions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $days = $this->daysOfWeek;
        $schedules = Schedule::all();
        return view('vacations.admin.schedule.create', compact('days', 'schedules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ScheduleRequest $request)
    {
        $request = $request->validated();
        try {
            DB::beginTransaction();
            $region = Region::create([
                'name' => $request['name'],
                'rules' => null,
                'workshift' => $request['workshift'],
            ]);

            foreach($this->daysOfWeek as $key => $day){
                WorkshiftFixed::create([
                    'day' => $key,
                    'labor' => isset($request['labor'][$key]),
                    'region_id' => $region->id,
                    'schedule_id' => isset($request['schedule'][$key])?$request['schedule'][$key]:null,
                ]);
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->route('admin.schedule.create')->withErrors(['errors' => $th->getMessage()]);
        }
        flash('Los cambios se guardaron satisfactoriamente.','success');
        return redirect()->route('admin.schedule.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $region = Region::where('id',$id)->firstOrFail();
        $currMonth = date('m');
        $totalDays = date('t');
        $date = mktime(0,0,0, $currMonth, 1, date('y'));
        $dayNumber = date('N', $date);
        // dd($currMonth, $totalDays, $dayNumber, $date);
        if($region->workshift == '5x2'){
            return view('admin.schedule.show5x2', compact('region', 'currMonth', 'totalDays', 'dayNumber'));
        }else{
            return view('admin.schedule.show6x2', compact('region'));
        }

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $region = Region::with('workshiftFixed.schedule')->findOrFail($id);
        $region->loadWeekSchedule();
        $days = $this->daysOfWeek;
        $schedules = Schedule::all();
        return view('vacations.admin.schedule.edit', compact('region', 'days', 'schedules'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ScheduleRequest $request, $id)
    {
        $region = Region::findOrFail($id);
        $request = $request->validated();
        try {
            DB::beginTransaction();
            
            $newRegion = Region::create([
                'name' => $request['name'],
                'rules' => null,
                'workshift' => $request['workshift'],
            ]);

            foreach($this->daysOfWeek as $key => $day){
                WorkshiftFixed::create([
                    'day' => $key,
                    'labor' => isset($request['labor'][$key]),
                    'region_id' => $newRegion->id,
                    'schedule_id' => isset($request['schedule'][$key])?$request['schedule'][$key]:null,
                ]);
            }

            Employee::where('region_id', $region->id)
            ->update(['region_id' => $newRegion->id]);

            $region->delete();

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->route('admin.schedule.edit', $id)->withErrors(['errors' => $th->getMessage()]);
        }
        flash('Los cambios se guardaron satisfactoriamente.','success');
        return redirect()->route('admin.schedule.edit', $newRegion->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('region_id',$id)->first();
        if($user) {
            flash('Para borrar la región debe mover a la gente que pertenece a este horario a otro distinto.','warning');
            return redirect('admin/schedule');
        }
        flash('La región fue borrada correctamente','success');
        $region = Region::where('id',$id)->first();
        $region->delete();
        return redirect('admin/schedule');
    }

}
