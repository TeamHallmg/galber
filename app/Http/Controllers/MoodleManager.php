<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Scale\ModelData;
use App\Models\Moodle\User;
use App\Models\Moodle\Enrol;
use App\Models\Moodle\Course;
use App\Models\PuestoCurso;
use App\Models\Moodle\RoleAssigment;
use App\Models\Moodle\UserEnrolment;
use App\Models\Moodle\CourseCategory;

class MoodleManager {

    public function getCoursesFromCategory($category){
        $courses = [];
        $c_category = CourseCategory::with([
            'courses' => function($q){
                $q->where('visible', 1);
            }, 
            'categories' => function($q){
                $q->where('visible', 1)
                ->with(['courses' => function ($q1){
                    $q1->where('visible', 1);
                }]);
            }
        ])
        ->where('name', $category)
        ->where('visible', 1)
        ->first();
        if($c_category){
            foreach($c_category->courses as $course){
                $courses[$course->id] = $course->fullname;
            }
            foreach($c_category->categories as $category){
                foreach($category->courses as $course){
                    $courses[$course->id] = $course->fullname;
                }
            }
        }
        return $courses;
    }

    public function getCourses($userid){
        $mdl_user = User::where('icq', $userid)->first();
        $role_assigment_course = [];
        if($mdl_user){
            $user_enrolment = UserEnrolment::with('enrol.course')
            ->where('userid', $mdl_user->id)
            ->whereHas('enrol', function($q){
                $q->where('enrol', 'manual');
            })
            ->get();

            $role_assigment_course = [];
            $role_assigment = RoleAssigment::with('context.instance')
            ->where('roleid' , 5)
            ->where('userid', $mdl_user->id)
            ->whereHas('context', function($q){
                $q->where('contextlevel', 50);
                // ->whereIn('instances', $courses)
            })
            ->get();

            // foreach($user_enrolment as $enrolment){
            //     $user_enrolment_course[$enrolment->enrol->course->id] = $enrolment->enrol->course->fullname;
            // }

            foreach($role_assigment as $assigment){
                $role_assigment_course[$assigment->context->instance->id] = $assigment->context->instance->fullname;
            }
        }
        return $role_assigment_course;
    }

    public function attachCourses($userid, $userCourses, $attachCourses, $plan_id = null, $price = 0){
        $mdl_user = User::where('icq', $userid)->first();
        // dd($mdl_user);
        $log = [];
        if($mdl_user){
            // dd($userid, $userCourses, $attachCourses);
            foreach($userCourses as $moodle_id => $name){
                // if(isset($attachCourses[$moodle_id]) && $attachCourses[$moodle_id] === $name){
                //     unset($attachCourses[$moodle_id]);
                // }
                if(in_array($moodle_id, array_keys($attachCourses))) {
                    unset($attachCourses[$moodle_id]);
                }
            }
            if(empty($attachCourses)) {
                return $log;
            }
            // dd($attachCourses);
            foreach($attachCourses as $key => $course){
                $log[$mdl_user->id . '-' . $key] = $this->enrollToCourse($mdl_user->id, $key, $userid, $plan_id, $price);
            }
        }
        return $log;
    }

    public function enrollToCourse($userid, $courseid, $laravel_id = null, $plan_id = null, $price = 0){
        try {
            $course = Course::find($courseid);
            $context = $course->context(50)->id;
        
            // dd($userid, $course, $context, $course->context(50));
            
            $enrol = Enrol::where('enrol', 'manual')
            ->where('courseid', $courseid)
            ->first();
                
            if($enrol){	
                $enrolled = RoleAssigment::where('roleid', 5)
                ->where('contextid', $context)
                ->where('userid', $userid)
                ->first();

                if(!$enrolled){
                    \DB::beginTransaction();
                    try {
                        // dd('matriculo', $userid);
                        RoleAssigment::updateOrCreate([
                            'roleid' => 5,
                            'contextid' => $context,
                            'userid' => $userid
                        ], [
                            'timemodified' => strtotime('now')
                        ]);
                        UserEnrolment::create([
                            'enrolid' => $enrol->id,
                            'userid' => $userid,
                            'timestart' => strtotime('now'),
                            'timeend' => 0
                        ]);
                        ModelData::create([
                            'model_name' => 'User',
                            'model_id' => $laravel_id,
                            'scale_data_type' => 'moodle_course',
                            'scale_data_id' => $courseid,
                            'extra1' => $plan_id,
                            'extra2' => $price
                        ]);
                        \DB::commit();
                        return true;
                    } catch (Exception $e) {
                        $errors['query_errors'][] = $e->getMessage();
                        \DB::rollback();
                    }
                    
                }
            }
            return false;
        } catch(\Throwable $th) {
            return false;
        }
    }

    public function getCoursesFromJob($job){
        $job_courses = PuestoCurso::where('puesto', $job)->get()->pluck('curso_id')->toArray();
        $m_courses = Course::whereIn('id', $job_courses)->get();
        $courses = [];
        foreach($m_courses as $course){
            $courses[$course->id] = $course->fullname;
        }
        return $courses;
    }

    public function createUser($user){
        $pass = '$2y$10$k.Fs1HbxgjQLcspipM3FhOqAUZz4.xZT0PsRM8P4ll6Fu0blu.Zc6';
        $m_user = User::where('icq', $user->id)->first();
        if(!$m_user){
            User::create([
                'confirmed' => 1,
                'mnethostid' => 1,
                'username' => $user->email,
                'password' => $pass,
                'icq' => $user->id,
                'idnumber' => $user->employee_number,
                'firstname' => $user->first_name,
                'lastname' => $user->last_name,
                'email' => $user->email,
                'lang' => 'es',
                'institution' => $user->company?$user->company:'',
                'department' => $user->division?$user->division:'',
                'city' => $user->workstation?$user->workstation:'',
                'msn' => $user->subdivision?$user->subdivision:'',
                'phone1' => $user->telephone?$user->telephone:'',
                'skype' => $user->pers_group_id?$user->pers_group_id:'',
                'aim' => $user->boss_id?$user->boss_id:'',
            ]);
        }
    }
}