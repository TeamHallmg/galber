<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Role;
use App\Models\Permission;
use App\Models\PermissionRole;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function initialize() {
        try {
            DB::beginTransaction();

            $roles_permissions = [
                [
                    'name' => 'announcement_admin',
                    'description' => 'Administrador de Anuncios',
                    'permissions' => [
                        [
                            [
                                'action' => 'see_announcements'
                            ], [
                                'name' => 'Índice Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite ingresar al index del controlador de anuncios'
                            ]
                        ], [
                            [
                                'action' => 'create_announcements'
                            ], [
                                'name' => 'Crear Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite crear anuncios'
                            ]
                        ], [
                            [
                                'action' => 'edit_announcements'
                            ], [
                                'name' => 'Editar Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite editar anuncios'
                            ]
                        ], [
                            [
                                'action' => 'delete_announcements'
                            ], [
                                'name' => 'Eliminar Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite eliminar anuncios'
                            ]
                        ], [
                            [
                                'action' => 'activate_announcements'
                            ], [
                                'name' => 'Actiar/Desactivar Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite activar/desactivar anuncios'
                            ]
                        ], [
                            [
                                'action' => 'announcement_categories'
                            ], [
                                'name' => 'Admin Categorías de Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite CRUD Categorías de Anuncios'
                            ]
                        ], [
                            [
                                'action' => 'announcement_banner'
                            ], [
                                'name' => 'Administra Banner',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Banner'
                            ]
                        ], [
                            [
                                'action' => 'announcement_carrousel'
                            ], [
                                'name' => 'Administra Carrousel',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Carrousel'
                            ]
                        ], [
                            [
                                'action' => 'announcement_mosaic'
                            ], [
                                'name' => 'Administra Mosaico',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Mosaico'
                            ]
                        ], [
                            [
                                'action' => 'announcement_columns'
                            ], [
                                'name' => 'Administra Columna Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Columna Anuncios'
                            ]
                        ], [
                            [
                                'action' => 'announcement_gallery'
                            ], [
                                'name' => 'Administra Galería',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Galería'
                            ]
                        ], [
                            [
                                'action' => 'announcement_mosaic_space'
                            ], [
                                'name' => 'Administra Mosaico Espaciado',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Mosaico Espaciado'
                            ]
                        ], [
                            [
                                'action' => 'announcement_calendar'
                            ], [
                                'name' => 'Administra Calendario',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Calendario'
                            ]
                        ], [
                            [
                                'action' => 'announcement_blog'
                            ], [
                                'name' => 'Administra Blog',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Blog'
                            ]
                        ], [
                            [
                                'action' => 'announcement_convenios'
                            ], [
                                'name' => 'Administra Convenios',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Convenio'
                            ]
                        ], [
                            [
                                'action' => 'announcement_simple_list'
                            ], [
                                'name' => 'Administra Lista Simple',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Lista Simple'
                            ]
                        ], [
                            [
                                'action' => 'announcement_committees'
                            ], [
                                'name' => 'Administra Comités',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Comité'
                            ]
                        ], [
                            [
                                'action' => 'announcement_images'
                            ], [
                                'name' => 'Administra Imágenes',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Imágenes'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'user_admin',
                    'description' => 'Administrador de Usuarios',
                    'permissions' => [
                        [
                            [
                                'action' => 'see_users'
                            ], [
                                'name' => 'Ver Usuarios',
                                'module' => 'General',
                                'description' => 'Permite ingresar al index del controlador de usuarios'
                            ]
                        ], [
                            [
                                'action' => 'user_admin'
                            ], [
                                'name' => 'Administrador de Usuarios',
                                'module' => 'General',
                                'description' => 'Administra usuarios de la plataforma'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'profile_admin',
                    'description' => 'Administrador de Expedientes',
                    'permissions' => [
                        [
                            [
                                'action' => 'see_users'
                            ], [
                                'name' => 'Ver Usuarios',
                                'module' => 'General',
                                'description' => 'Permite ingresar al index del controlador de usuarios'
                            ]
                        ], [
                            [
                                'action' => 'profile_admin'
                            ], [
                                'name' => 'Administrador de Expedientes',
                                'module' => 'Expediente',
                                'description' => 'Administra expediente de usuarios de la plataforma'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'incidents_admin',
                    'description' => 'Administrador de Asistencias/Incidencias',
                    'permissions' => [
                        [
                            [
                                'action' => 'incidents_admin'
                            ], [
                                'name' => 'Administrador de Asistencias/Incidencias',
                                'module' => 'Gestión de Asistencias/Incidencias',
                                'description' => 'Administrador del módulo de Asistencias/Incidencias'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'performance_evaluation_admin',
                    'description' => 'Administrador de Evaluación de Desempeño (DNC)',
                    'permissions' => [
                        [
                            [
                                'action' => 'performance_evaluation_admin'
                            ], [
                                'name' => 'Administrador de Evaluación de Desempeño',
                                'module' => 'Evaluación de Desempeño',
                                'description' => 'Administrador del módulo de Evaluación de Desempeño'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'evaluation_of_results_admin',
                    'description' => 'Administrador de Evaluación de Resultados',
                    'permissions' => [
                        [
                            [
                                'action' => 'evaluation_of_results_admin'
                            ], [
                                'name' => 'Administrador de Evaluación de Resultados',
                                'module' => 'Evaluación de Resultados',
                                'description' => 'Administrador del módulo de Evaluación de Resultados'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'scale_admin',
                    'description' => 'Administrador de Escalafón',
                    'permissions' => [
                        [
                            [
                                'action' => 'scale_admin'
                            ], [
                                'name' => 'Administrador de Escalafón',
                                'module' => 'Capacitación',
                                'description' => 'Administrador del módulo de Capacitación'
                            ]
                        ]
                    ]
                ]
            ];
            // evaluation of results
            foreach($roles_permissions as $role_data) {
                $role = Role::updateOrCreate(
                    [
                        'name' => $role_data['name']
                    ], [
                       'description' => $role_data['description']
                    ]
                );
                foreach ($role_data['permissions'] as $permission_data) {
                    $fixed_data = $permission_data[0];
                    $update_data = $permission_data[1];
                    $permission = Permission::updateOrCreate($fixed_data, $update_data);
                    // dd($permission_data, $fixed_data, $update_data);
                    PermissionRole::firstOrCreate([
                        'role_id' => $role->id,
                        'permission_id' => $permission->id
                    ]);
                }
            }

            DB::commit();
        } catch(\Throwable $th) {
            DB::rollback();
            dd($th, $role_data, $permission_data);
        }
        dd('roles y permisos creados correctamente');
    }
}
