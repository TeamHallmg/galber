<?php

namespace App\Http\Controllers\EvaluacionResultados;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use App\Models\EvaluacionResultados\Admin\UsersGroup;
use App\Models\EvaluacionResultados\Admin\GroupElement;
use DB;

class UsersGroupsController extends Controller{
/*
|--------------------------------------------------------------------------
| Users Groups Controller
|--------------------------------------------------------------------------
|
*/

  /**
  * Middleware que permite el acceso a quien este logueado (auth) y que sea admin
  *
  * @return void
  */
  public function __construct(){

    $this->middleware('auth');
    //$this->middleware('admin');
  }

  /**
  * Muestra el listado de los grupos de usuarios
  *
  * @return void
  */
  public function index(){

    if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

      Flash::error('No tiene permiso para ver la sección');
      return view('/que-es-evaluacion-resultados');
    }
    
    $users_groups = UsersGroup::all();
    $users = DB::table('employees')->whereNull('deleted_at')->select('id', 'nombre', 'paterno', 'materno')->get();
    $temp_users = array();

    foreach ($users as $key => $value){
      
      $temp_users[$value->id * 1] = $value->nombre . ' ' . $value->paterno . ' ' . $value->materno;
    }

    $users = $temp_users;
    $departments = DB::table('departments')->pluck('name', 'id');
    $others_users_groups = DB::table('grupos_usuarios')->pluck('nombre', 'id');
    return view('evaluacion-resultados/users_groups/index', compact('users_groups', 'others_users_groups', 'departments', 'users'));
  }

  /**
   * Muestra la pagina para crear un nuevo grupo de usuarios
   */
  public function create(){
  
    if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

      Flash::error('No tiene permiso para ver la sección');
      return view('/que-es-evaluacion-resultados');
    }

    //$users = User::where('active', '=', 1)->select('id', 'first_name', 'last_name')->get();
    $users = DB::select("SELECT id AS data, CONCAT(nombre, ' ', paterno, ' ', materno) AS value FROM employees");
    $departments = DB::select("SELECT id AS data, name AS value FROM departments");
    $users_groups = DB::select("SELECT id AS data, nombre AS value FROM grupos_usuarios");

    $temp_users_groups = array();

    foreach ($users_groups as $key => $value){
      
      $results = DB::select("SELECT id_grupo FROM elementos_grupo WHERE id_grupo = ? AND tipo_elemento = ?", [$value->data, 'Grupo']);

      if (empty($results)){

        $temp_users_groups[] = $value;
      }
    }

    $users_groups = $temp_users_groups;
    return view('evaluacion-resultados/users_groups/add', compact('users', 'departments', 'users_groups'));
  }

  /**
   * Guarda un nuevo grupo de usuarios
   */
  public function store(Request $request){

    if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

      Flash::error('No tiene permiso para ver la sección');
      return view('/que-es-evaluacion-resultados');
    }

    $users_group = new UsersGroup;
    $users_group->nombre = $request->nombre;
    $users_group->save();
    $last_users_group = UsersGroup::all()->last();

    if (!empty($request->users)){

      foreach($request->users as $key => $user){
      
        $group_element = new GroupElement;
        $group_element->id_grupo = $last_users_group->id;
        $group_element->id_elemento = $user;
        $group_element->tipo_elemento = 'Persona';
        $group_element->save();
      }
    }

    if (!empty($request->departments)){

      foreach($request->departments as $key => $department){
      
        $group_element = new GroupElement;
        $group_element->id_grupo = $last_users_group->id;
        $group_element->id_elemento = $department;
        $group_element->tipo_elemento = 'Departamento';
        $group_element->save();
      }
    }

    if (!empty($request->grupos)){

      foreach($request->grupos as $key => $grupo){
      
        $group_element = new GroupElement;
        $group_element->id_grupo = $last_users_group->id;
        $group_element->id_elemento = $grupo;
        $group_element->tipo_elemento = 'Grupo';
        $group_element->save();
      }
    }

    Flash::success('El grupo de usuarios fue guardado correctamente.');
    return redirect('/evaluacion-resultados/users_groups');
  }

  /**
   * Muestra la página para editar un grupo de usuarios
   */
  public function edit($id){

    if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

      Flash::error('No tiene permiso para ver la sección');
      return view('/que-es-evaluacion-resultados');
    }

    $users_group = UsersGroup::where('id', '=', $id)->first();
    $users = DB::select("SELECT id AS data, CONCAT(nombre, ' ', paterno, ' ', materno) AS value FROM employees");
    $departments = DB::select("SELECT id AS data, name AS value FROM departments");
    $users_groups = DB::select("SELECT id AS data, nombre AS value FROM grupos_usuarios WHERE id != ?", [$id]);
    $temp_users_groups = array();

    foreach ($users_groups as $key => $value){
      
      $results = DB::select("SELECT id_grupo FROM elementos_grupo WHERE id_grupo = ? AND tipo_elemento = ?", [$value->data, 'Grupo']);

      if (empty($results)){

        $temp_users_groups[] = $value;
      }
    }

    $users_groups = $temp_users_groups;
    return view('evaluacion-resultados/users_groups/edit', compact('users_group', 'users_groups', 'users', 'departments'));
  }

  /**
   * Actualiza un grupo de usuarios
   */
  public function update(Request $request, $id){

    if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

      Flash::error('No tiene permiso para ver la sección');
      return view('/que-es-evaluacion-resultados');
    }

    $users_group = UsersGroup::find($id);
    $users_group->nombre = $request->nombre;
    $users_group->updated_at = date('Y-m-d H:i:s');
    $users_group->save();
    GroupElement::where('id_grupo', '=', $id)->delete();

    if (!empty($request->users)){

      foreach($request->users as $key => $user){
      
        $group_element = new GroupElement;
        $group_element->id_grupo = $id;
        $group_element->id_elemento = $user;
        $group_element->tipo_elemento = 'Persona';
        $group_element->save();
      }
    }

    if (!empty($request->departments)){

      foreach($request->departments as $key => $department){
      
        $group_element = new GroupElement;
        $group_element->id_grupo = $id;
        $group_element->id_elemento = $department;
        $group_element->tipo_elemento = 'Departamento';
        $group_element->save();
      }
    }

    if (!empty($request->grupos)){

      foreach($request->grupos as $key => $grupo){
      
        $group_element = new GroupElement;
        $group_element->id_grupo = $id;
        $group_element->id_elemento = $grupo;
        $group_element->tipo_elemento = 'Grupo';
        $group_element->save();
      }
    }

    Flash::success('El grupo de usuarios fue guardado correctamente.');
    return redirect('/evaluacion-resultados/users_groups');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }    
}

