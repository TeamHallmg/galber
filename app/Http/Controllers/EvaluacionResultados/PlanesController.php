<?php

namespace App\Http\Controllers\EvaluacionResultados;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Session;

class PlanesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('admin');
    }

    /**
     * Muestra todos los planes.
     *
     */
    public function index(){
      
      if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){
			
		    flash("No tiene permiso para ver la sección administrativa del módulo de Evaluación de Resultados");
		    return redirect('/');	
	   }
		
	  // Obtiene todos los planes
      $planes = DB::table('planes')->get();

      // Carga la vista para mostrar los planes
      return view('evaluacion-resultados/planes/index', compact('planes'));
    }

    /**
     * Crea un nuevo plan
     *
     */
    public function create(){
		
	  if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){
			
		  flash("No tiene permiso para ver la sección administrativa del módulo de Evaluación de Resultados");
		  return redirect('/');	
	  }
      // Muestra la pagina para la creacion de un plan
      return view('evaluacion-resultados/planes/create');
    }

    /**
     * Edita un plan
     *
     */
    public function edit($id = 0){

      if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){
			
		    flash("No tiene permiso para ver la sección administrativa del módulo de Evaluación de Resultados");
		    return redirect('/');	
	    } 

      // Obtiene el plan a editar
      $plan = DB::table('planes')->where('id', $id)->first();

      // Muestra la pagina de edicion de planes
      return view('evaluacion-resultados/planes/edit', compact('plan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
      
      if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){
      
        flash("No tiene permiso para ver la sección administrativa del módulo de Evaluación de Resultados");
        return redirect('/'); 
      }

      // Se ha asignado un nombre al nuevo plan
      if (!empty($_POST['nombre'])){

        $plan = array();
        $plan['nombre'] = $_POST['nombre'];
        $plan['anio'] = $_POST['anio'];
        $plan['estado'] = $_POST['estado'];
        $plan['alimentar_propuestas'] = $_POST['alimentar_propuestas'];
        $plan['autorizar_propuestas'] = $_POST['autorizar_propuestas'];
        $plan['registrar_logros'] = $_POST['registrar_logros'];
        $plan['autorizar_logros'] = $_POST['autorizar_logros'];
        $plan['registrar_responsabilidades'] = 2;
        $plan['periodo'] = $_POST['periodo'];

        if (!empty($_POST['total_peso_objetivos'])){

          $plan['total_peso_objetivos'] = $_POST['total_peso_objetivos'];
        }

        // Agrega el nuevo plan a la base de datos
        DB::table('planes')->insert($plan);

        flash('El plan fue creado correctamente');
      }

      // Error al intentar crear el plan
      else{

        flash('Error al crear el plan. Intente de nuevo');
      }

      // Redirecciona a la pagina de listado de planes
      return redirect('/evaluacion-resultados/planes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

      if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){
      
        flash("No tiene permiso para ver la sección administrativa del módulo de Evaluación de Resultados");
        return redirect('/'); 
      }

      $fecha = date('Y-m-d H:i:s');
      $plan = array();
      $plan['nombre'] = $_POST['nombre'];
      $plan['anio'] = $_POST['anio'];
      $plan['estado'] = $_POST['estado'];
      $plan['alimentar_propuestas'] = $_POST['alimentar_propuestas'];
      $plan['autorizar_propuestas'] = $_POST['autorizar_propuestas'];
      $plan['registrar_logros'] = $_POST['registrar_logros'];
      $plan['autorizar_logros'] = $_POST['autorizar_logros'];
      $plan['registrar_responsabilidades'] = 2;
      $plan['periodo'] = $_POST['periodo'];
      $plan['updated_at'] = $fecha;
      $plan['total_peso_objetivos'] = '';

      if (!empty($_POST['total_peso_objetivos'])){

        $plan['total_peso_objetivos'] = $_POST['total_peso_objetivos'];
      }

      // Se actualizan los datos del plan
      DB::table('planes')->where('id', $_POST['id'])->update($plan);

      flash('El plan fue guardado correctamente');

      // Redirecciona al listado de planes
      return redirect('/evaluacion-resultados/planes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = 0){
      
      if (!empty($_POST['id'])){

        $plan = DB::delete('DELETE FROM planes WHERE id = ?', [$_POST['id']]);
        
        if (!empty($plan)){

          $objetivos = DB::select('SELECT id FROM objetivos WHERE id_plan = ?', [$_POST['id']]);

          foreach ($objetivos as $key => $objetivo){
            
            DB::delete('DELETE FROM valores_objetivo WHERE id_objetivo = ?', [$objetivo->id]);
            DB::delete('DELETE FROM objetivos WHERE id = ?', [$objetivo->id]);
          }

          DB::delete('DELETE FROM logros WHERE id_plan = ?', [$_POST['id']]);
          DB::delete('DELETE FROM mensajes WHERE id_plan = ?', [$_POST['id']]);
          DB::delete('DELETE FROM resultados_objetivos WHERE id_plan = ?', [$_POST['id']]);
          DB::delete('DELETE FROM status_plan_empleado WHERE id_plan = ?', [$_POST['id']]);
          flash('El plan fue borrado correctamente');
        }

        else{

          flash('Error al borrar el plan. Intente de nuevo');
        }

        return redirect('/evaluacion-resultados/planes');
      }

      $plan = DB::select('SELECT id, nombre FROM planes WHERE id = ?', [$id]);
      return view('evaluacion-resultados/planes/delete', compact('plan'));
    }
}
