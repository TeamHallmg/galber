<?php

namespace App\Http\Controllers\EvaluacionResultados;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;
use DB;
use App\Employee;
use Session;

class BSCController extends Controller{

	/*
  |--------------------------------------------------------------------------
  | BSC Controller (Mídete)
  |--------------------------------------------------------------------------
  |
  | Add Documentation
  |
  */

  /**
  * Create a new BSC controller instance.
  *
  * @return void
  */
  public function __construct(){

    $this->middleware('auth');
  }

  /**
  * Muestra la pagina de los tipos de objetivos corporativos
  *
  * @return void
  */
  public function tipos_objetivos_corporativos(){

    if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

      Flash::error('No tiene permiso para ver la sección');
      return view('evaluacion-resultados/que-es');
    }

    $id_plan = 0;
    $tipos_objetivos_corporativos = $users = array();

    // Obtiene todos los planes no cancelados
    $planes = DB::table('planes')->where('estado', '!=', 4)->orderBy('id', 'DESC')->get();

    // Se requiere ver un plan en particular
    if (!empty($_POST['id_plan']) || Session::has('id_plan')){

      if (!empty($_POST['id_plan'])){

        $id_plan = $_POST['id_plan'];
        Session::put('id_plan', $id_plan);
      }

      else{

        $id_plan = Session::get('id_plan');
        $found = false;

        foreach ($planes as $key => $value){
          
          if ($value->id == $id_plan){

            $found = true;
            break;
          }
        }

        if (!$found){

          // Existen planes no cancelados
          if (count($planes) > 0){

            // Se elige el id del plan mas reciente no cancelado
            $id_plan = $planes[0]->id;
          }

          else{

            $id_plan = 0;
          }

          Session::forget('id_plan');   
        }
      }
    }

    // No se requiere ver un plan en particular
    else{

      // Existen planes no cancelados
      if (count($planes) > 0){

        // Se elige el id del plan mas reciente no cancelado
        $id_plan = $planes[0]->id;
      }
    }

    // Existe un plan no cancelado
    if ($id_plan != 0){

      $tipos_objetivos_corporativos = DB::table('tipos_objetivos_corporativos')->where('id_plan', $id_plan)->select('id', 'name', 'description', 'letter', 'color', 'id_responsable')->orderBy('id')->get();
      $users = DB::table('employees')->select(array('id AS data', DB::raw('CONCAT(nombre, " ", paterno, " ", materno) AS value')))->get();
    }

    return view('evaluacion-resultados/objetivos-corporativos/tipos-objetivos-corporativos', compact('tipos_objetivos_corporativos', 'users', 'planes', 'id_plan'));
  }

  /**
  * Muestra la página de objetivos corporativos
  *
  * @return void
  */
  public function objetivos_corporativos(){

    if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

      Flash::error('No tiene permiso para ver la sección');
      return view('evaluacion-resultados/que-es');
    }

    $id_plan = 0;
    $objetivos_corporativos = $planes_para_copiar = array();
    $planes = DB::table('planes')->where('estado', '!=', 4)->orderBy('id', 'DESC')->get();

    // Se requiere ver un plan en particular
    if (!empty($_POST['id_plan']) || Session::has('id_plan')){

      if (!empty($_POST['id_plan'])){

        $id_plan = $_POST['id_plan'];
        Session::put('id_plan', $id_plan);
      }

      else{

        $id_plan = Session::get('id_plan');
        $found = false;

        foreach ($planes as $key => $value){
          
          if ($value->id == $id_plan){

            $found = true;
            break;
          }
        }

        if (!$found){

          // Existen planes no cancelados
          if (count($planes) > 0){

            // Se elige el id del plan mas reciente no cancelado
            $id_plan = $planes[0]->id;
          }

          else{

            $id_plan = 0;
          }

          Session::forget('id_plan');   
        }
      }
    }

    // No se requiere ver un plan en particular
    else{

      // Existen planes no cancelados
      if (count($planes) > 0){

        // Se elige el id del plan mas reciente no cancelado
        $id_plan = $planes[0]->id;
      }
    }

    // Existe un plan no cancelado
    if ($id_plan != 0){

      // Se obtienen todos los objetivos corporativos relacionados con tipos de objetivos corporativos relacionados con el plan
      $objetivos_corporativos = DB::table('tipos_objetivos_corporativos')->leftJoin('objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'objetivos_corporativos.id_tipo')->where('id_plan', $id_plan)->select('tipos_objetivos_corporativos.id', 'tipos_objetivos_corporativos.name', 'description', 'letter', 'color', 'objetivos_corporativos.name AS nombre_objetivo_corporativo', 'objetivos_corporativos.id AS id_objetivo_corporativo')->orderBy('tipos_objetivos_corporativos.id')->orderBy('objetivos_corporativos.id')->get();

      $planes_para_copiar = DB::table('tipos_objetivos_corporativos')->join('objetivos_corporativos', 'objetivos_corporativos.id_tipo', '=', 'tipos_objetivos_corporativos.id')->join('planes', 'tipos_objetivos_corporativos.id_plan', '=', 'planes.id')->where('planes.id', '!=', $id_plan)->where('estado', 2)->orWhere('estado', 3)->select('planes.id', 'planes.nombre')->groupBy('planes.id', 'planes.nombre')->get();
    }

    return view('evaluacion-resultados/objetivos-corporativos/objetivos-corporativos', compact('objetivos_corporativos', 'planes_para_copiar', 'planes', 'id_plan'));
  }
	
  /**
  * Muestra la página de responsabilidades y compromisos (se capturan las responsabilidades corporativas)
  *
  * @return void
  */
  public function responsabilidades_y_compromisos(){

    // Id del usuario logueado
    $id_user = auth()->user()->employee_id;

    $id_plan = 0;
    $registrar_responsabilidades = '';
    $responsabilities = $users = $users_groups = $autocomplete_users = $notas = $objetivos_corporativos = $planes_para_copiar = array();
    $canEdit = false;

    // Obtiene todos los planes abiertos
    $planes = DB::select("SELECT * FROM planes WHERE estado = ? ORDER BY id DESC", [2]);

    // Se requiere ver un plan en particular
    if (!empty($_POST['id_plan']) || Session::has('id_plan')){

      if (!empty($_POST['id_plan'])){

        $id_plan = $_POST['id_plan'];
        Session::put('id_plan', $id_plan);
      }

      else{

        $id_plan = Session::get('id_plan');
        $found = false;

        foreach ($planes as $key => $value){
          
          if ($value->id == $id_plan){

            $found = true;
            break;
          }
        }

        if (!$found){

          $id_plan = $planes[0]->id;
          Session::forget('id_plan');
        }
      }
    }

    // No se requiere ver un plan en particular
    else{

      // Existen planes aviertos
      if (!empty($planes)){

        // Se elige el id del plan mas reciente
        $id_plan = $planes[0]->id;
      }
    }

    // Existe un plan abierto
    if ($id_plan != 0){

      foreach ($planes as $key => $value){
        
        if ($value->id == $id_plan){

          $registrar_responsabilidades = $value->registrar_responsabilidades;
          break;
        }
      }

      $notas = DB::select("SELECT id_user, mensaje, notas.created_at, nombre, paterno, materno FROM notas INNER JOIN employees ON (employees.id = notas.id_user) WHERE id_plan = ? ORDER BY notas.created_at DESC", [$id_plan]);
      $users_groups = DB::select("SELECT id AS data, nombre AS value FROM grupos_usuarios");
      $users = DB::select("SELECT id, nombre, paterno, materno FROM employees ORDER BY nombre");

      if ($registrar_responsabilidades == 'Responsable'){

        if (!empty($id_user)){

          $objetivos_corporativos = DB::select("SELECT objetivos_corporativos.id, objetivos_corporativos.name, id_tipo, tipos_objetivos_corporativos.name AS nombre_tipo, letter, color FROM objetivos_corporativos INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE id_plan = ? AND id_responsable = ? ORDER BY id_tipo, objetivos_corporativos.id", [$id_plan, $id_user]);

          if (!empty($objetivos_corporativos)){

            $canEdit = true;
            $autocomplete_users = DB::select("SELECT id AS data, CONCAT(first_name, last_name) AS value FROM users");
            $responsabilities = DB::select("SELECT responsabilidades_compromisos.*, id_tipo FROM responsabilidades_compromisos INNER JOIN objetivos_corporativos ON (responsabilidades_compromisos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE responsabilidades_compromisos.id_plan = ? AND tipos_objetivos_corporativos.id_responsable = ? ORDER BY tipos_objetivos_corporativos.id, id_objetivo_corporativo, tipo_indicador", [$id_plan, $id_user]);
            $planes_para_copiar = DB::table('responsabilidades_compromisos')->join('objetivos_corporativos', 'objetivos_corporativos.id', '=', 'responsabilidades_compromisos.id_objetivo_corporativo')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'objetivos_corporativos.id_tipo')->join('planes', 'responsabilidades_compromisos.id_plan', '=', 'planes.id')->where('tipos_objetivos_corporativos.id_responsable', $id_user)->where('planes.id', '!=', $id_plan)->where('estado', 2)->orWhere('estado', 3)->select('planes.id', 'nombre')->groupBy('planes.id', 'nombre')->get();
          }
        }
      }

      if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

        if (!$canEdit){

          $responsabilities = DB::select("SELECT responsabilidades_compromisos.*, id_tipo FROM responsabilidades_compromisos INNER JOIN objetivos_corporativos ON (responsabilidades_compromisos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE responsabilidades_compromisos.id_plan = ? ORDER BY tipos_objetivos_corporativos.id, id_objetivo_corporativo, tipo_indicador", [$id_plan]);
          $objetivos_corporativos = DB::select("SELECT objetivos_corporativos.id, objetivos_corporativos.name, id_tipo, tipos_objetivos_corporativos.name AS nombre_tipo, letter, color FROM objetivos_corporativos INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE id_plan = ? AND id_responsable != ? ORDER BY id_tipo, objetivos_corporativos.id", [$id_plan, 0]);
        }

        if ($registrar_responsabilidades == 'Administrador'){

          $canEdit = true;
          $autocomplete_users = DB::select("SELECT id AS data, CONCAT(nombre, ' ', paterno, ' ', materno) AS value FROM employees");
          $planes_para_copiar = DB::table('responsabilidades_compromisos')->join('planes', 'responsabilidades_compromisos.id_plan', '=', 'planes.id')->where('planes.id', '!=', $id_plan)->where('estado', 2)->orWhere('estado', 3)->select('planes.id', 'nombre')->groupBy('planes.id', 'nombre')->get();
        }
      }

      $temp_responsabilities = array();

      foreach ($responsabilities as $key => $value){

        $corresponsables = array();
        $grupos_corresponsables = array();
        $results = DB::select("SELECT id_corresponsable, tipo FROM responsabilidades_corresponsables WHERE id_responsabilidad = ?", [$value->id]);

        foreach ($results as $key2 => $value2){
            
          // El tipo de corresponsables es individual (no es grupo)
          if ($value2->tipo == 'persona'){

            // Se agrega a la lista de corresponsables
            $corresponsables[] = $value2->id_corresponsable;
          }

          // El tipo de corresponsables es un grupo (varias personas)
          else{

            // Se agrega a la lista de corresponsables de tipo grupo
            $grupos_corresponsables[] = $value2->id_corresponsable;
          }
        }

        $value->corresponsables = $corresponsables;
        $value->grupos_corresponsables = $grupos_corresponsables;
        $temp_responsabilities[] = $value;
      }

      $responsabilities = $temp_responsabilities;
    }

    return view('evaluacion-resultados/responsabilidades-y-compromisos', compact('users', 'responsabilities', 'notas', 'objetivos_corporativos', 'users_groups', 'autocomplete_users', 'planes', 'id_plan', 'id_user', 'canEdit', 'planes_para_copiar'));
  }

  /**
  * Muestra la página del catálogo de objetivos
  */
  public function catalogo_objetivos(){

    if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

      Flash::error('No tiene permiso para ver la sección');
      return view('/que-es-evaluacion-resultados');
    }

    $planes = DB::table('planes')->pluck('nombre', 'id')->toArray();
    $objetivos_usados = DB::table('objetivos')->where('id_catalogo', '!=', 0)->groupBy('id_catalogo')->pluck('id_catalogo')->toArray();
    $results = DB::table('catalogo_objetivos')->orderBy('unique_id')->get();
    $objetivos_corporativos = DB::table('objetivos_corporativos')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'objetivos_corporativos.id_tipo')->select('objetivos_corporativos.id', 'id_tipo', 'letter', 'id_plan')->orderBy('id_plan')->orderBy('id_tipo')->orderBy('objetivos_corporativos.id')->get();
    return view('evaluacion-resultados/catalogo-objetivos', compact('results', 'objetivos_corporativos', 'objetivos_usados', 'planes'));
  }

  /**
  * Exporta plantilla para importación de catálogo de objetivos
  */
  public function exportar_plantilla_catalogo_objetivos(){

    require_once 'PHPExcel.php';
    require_once 'PHPExcel/IOFactory.php';

    // Create new PHPExcel object
    $objPHPExcel = new \PHPExcel();

    $phpColor = new \PHPExcel_Style_Color();
    $phpColor->setRGB('FFFFFF');

    // Crea una hoja con las columnas necesarias para importar los datos de los objetivos para el catálogo
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('0078AD');
    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFont()->setColor($phpColor);
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Objetivo (Redacción)');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Descripción del Objetivo');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Código Objetivo (cualquier combinación de letras y numeros, si ya existe se actualizan los datos, si no se crea nuevo objetivo)');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Dirección (Si no se deja vacío sólo podrá ser asignado a personas con dicha dirección)');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Objetivo Corporativo (Ejemplo: A1, E2, etc)');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Unidad de medida (%, $, #, Si/No)');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Meta (Si, No, valores numéricos: 23, 5.4, etc');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Peso (valores de 1 a 100)');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Frecuencia (Mensual, Bimestral, Trimestral, Cuatrimestral, Semestral, Anual)');
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Semáforo rojo (Valor de referencia cuando el objetivo no se cumple');
    $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Semáforo verde (Valor de referencia cuando el objetivo se cumple');
    $catalogo_objetivos = DB::table('catalogo_objetivos')->orderBy('codigo')->get();

    if (count($catalogo_objetivos) > 0){
    
      $row = 2;
      $objetivos_corporativos = DB::table('objetivos_corporativos')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'objetivos_corporativos.id_tipo')->select('objetivos_corporativos.id', 'id_tipo', 'letter')->orderBy('id_tipo')->orderBy('objetivos_corporativos.id')->get();

      foreach ($catalogo_objetivos as $key => $value){
      
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $value->objetivo);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $value->descripcion);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $value->codigo);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $value->direccion);
        $current_type = 0;
        $counter = 0;

        foreach ($objetivos_corporativos as $key2 => $value2){
          
          if ($current_type != $value2->id_tipo){

            $counter = 1;
            $current_type = $value2->id_tipo;
          }

          if ($value2->id == $value->id_objetivo_corporativo){

            $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $value2->letter . $counter);
            break;
          }

          $counter++;
        }

        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $value->unidad);
        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $value->meta);
        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $value->peso);
        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $value->frecuencia);
        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $value->valor_rojo);
        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $value->valor_verde);
        $row++;
      }
    }
      
    // Rename sheet
    $objPHPExcel->getActiveSheet()->setTitle('Plantilla Catálogo de Objetivos');

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Plantilla Catálogo de Objetivos.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
  }
  
  /**
  * Importa el catálogo de objetivos
  */
  public function importar_catalogo_objetivos(){

      if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

        Flash::error('No tiene permiso para ver la sección');
        return view('/que-es-evaluacion-resultados');
      }

      $path = Storage::disk('local')
                            ->getDriver()
                            ->getAdapter()
                            ->getPathPrefix();
      $folder = 'Interface/';
      require_once "PHPExcel.php";
      $extension = '';

      if (!empty($_FILES['file'])){

        $array = explode('.', $_FILES['file']['name']);
        $extension = end($array);
      
        if ($extension == 'xls' || $extension == 'xlsx'){

          move_uploaded_file($_FILES['file']['tmp_name'], $path . $folder . 'catalogo_objetivos.xls');
        }
      }

      $inputFileName = $path . $folder . 'catalogo_objetivos.xls';
      $excelReader = \PHPExcel_IOFactory::createReaderForFile($inputFileName);
      $objPHPExcel = $excelReader->load($inputFileName);

      // Get worksheet dimensions
      $sheet = $objPHPExcel->getSheet(0); 
      $highestRow = $sheet->getHighestRow();

      $results = array();
      $actualizar = 0;
      $registrar = 0;
      $ignorar = 0;
      $objetivo_corporativo = 0;

      if (empty($_POST['importar'])){

        $objetivos_corporativos = DB::table('objetivos_corporativos')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'objetivos_corporativos.id_tipo')->select('objetivos_corporativos.id', 'id_tipo', 'letter')->orderBy('id_tipo')->orderBy('objetivos_corporativos.id')->get();

        // Loop through each row of the worksheet in turn
        for ($row = 2;$row <= 1000;$row++){
          
          $result = new \stdClass();
          $result->objetivo = $sheet->getCell('A'.$row)->getValue();
          $result->descripcion = $sheet->getCell('B'.$row)->getValue();
          $result->codigo = $sheet->getCell('C'.$row)->getValue();
          $result->direccion = $sheet->getCell('D'.$row)->getValue();
          $result->objetivo_corporativo = $sheet->getCell('E'.$row)->getValue();
          $result->unidad = $sheet->getCell('F'.$row)->getValue();
          $result->meta = $sheet->getCell('G'.$row)->getValue();
          $result->peso = $sheet->getCell('H'.$row)->getValue();
          $result->frecuencia = $sheet->getCell('I'.$row)->getValue();
          $result->valor_rojo = $sheet->getCell('J'.$row)->getValue();
          $result->valor_verde = $sheet->getCell('K'.$row)->getValue();
          $error = false;

          if (empty($result->objetivo) && empty($result->descripcion) && empty($result->codigo) && empty($result->direccion) && empty($result->objetivo_corporativo) && empty($result->unidad) && empty($result->meta) && empty($result->peso) && empty($result->frecuencia) && empty($result->valor_rojo) && empty($result->valor_verde)){

            break;
          }

          if (empty($result->objetivo)){

            $error = true;
            $result->objetivo = 'ERROR: Campo vacío';
          }

          if (empty($result->descripcion)){

            if (!$error){

              $result->descripcion = $result->objetivo;
            }

            else{

              $result->descripcion = 'ERROR: Campo vacío';
            }
          }

          if (empty($result->codigo)){

            $error = true;
            $result->codigo = 'ERROR: Campo vacío';
          }

          if (empty($result->objetivo_corporativo)){

            $error = true;
            $result->objetivo_corporativo = 'ERROR: Campo vacío';
          }

          else{

            $counter = 0;
            $current_type = 0;
            $found = false;

            foreach ($objetivos_corporativos as $key => $value){
              
              if ($current_type != $value->id_tipo){

                $counter = 1;
                $current_type = $value->id_tipo;
              }

              $objetivo_corporativo = $value->letter . $counter;

              if ($objetivo_corporativo == $result->objetivo_corporativo){

                $found = true;
                $objetivo_corporativo = $value->id;
                break;
              }

              $counter++;
            }

            if (!$found){

              $error = true;
              $result->objetivo_corporativo = 'ERROR: Valor inválido';
            } 
          }

          if (!empty($result->unidad)){

            if ($result->unidad != '%' && $result->unidad != '$' && $result->unidad != '#' && $result->unidad != 'Si/No'){

              $error = true;
              $result->unidad = 'ERROR: Valor inválido';
            }
          }

          if (!empty($result->frecuencia)){

            if ($result->frecuencia != 'Mensual' && $result->frecuencia != 'Bimestral' && $result->frecuencia != 'Trimestral' && $result->frecuencia != 'Cuatrimestral' && $result->frecuencia != 'Semestral' && $result->frecuencia != 'Anual'){

              $error = true;
              $result->frecuencia = 'ERROR: Valor inválido';
            }
          }

          if (!$error){

            $temp_code = $result->codigo . '';

            while(strlen($temp_code) < 5){

              $temp_code = '0' . $temp_code;
            }

            $result->codigo = $temp_code;
            $objetivo = DB::table('catalogo_objetivos')->where('codigo', $result->codigo)->first();

            if (!empty($objetivo)){

              if ($objetivo->objetivo != $result->objetivo || $objetivo->descripcion != $result->descripcion || $objetivo->direccion != $result->direccion || $objetivo_corporativo != $objetivo->id_objetivo_corporativo || $objetivo->unidad != $result->unidad || $objetivo->meta != $result->meta || $objetivo->peso != $result->peso || $objetivo->frecuencia != $result->frecuencia || $objetivo->valor_rojo != $result->valor_rojo || $objetivo->valor_verde != $result->valor_verde){

                $result->accion = 'Actualizar';
                $actualizar++;
              }

              else{

                $result->accion = 'Ninguna';
              }
            }

            else{

              $result->accion = 'Insertar';
              $registrar++;
            }
          }

          else{

            $result->accion = 'Ignorar';
            $ignorar++;
          }

          $results[] = $result;
        }

        return view('evaluacion-resultados/importar-catalogo-objetivos', compact('results', 'actualizar', 'registrar', 'ignorar'));
      }

      else{

        $objetivos_corporativos = DB::table('objetivos_corporativos')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'objetivos_corporativos.id_tipo')->select('objetivos_corporativos.id', 'id_tipo', 'letter')->orderBy('id_tipo')->orderBy('objetivos_corporativos.id')->get();
        $objetivo_corporativo2 = 0;

        // Loop through each row of the worksheet in turn
        for ($row = 2; $row <= $highestRow; $row++){
          
          $result = array();
          $result['objetivo'] = $sheet->getCell('A'.$row)->getValue();
          $result['descripcion'] = $sheet->getCell('B'.$row)->getValue();
          $result['codigo'] = $sheet->getCell('C'.$row)->getValue();
          $result['direccion'] = $sheet->getCell('D'.$row)->getValue();
          $result['id_objetivo_corporativo'] = $sheet->getCell('E'.$row)->getValue();
          $objetivo_corporativo2 = $result['id_objetivo_corporativo'];
          $result['unidad'] = $sheet->getCell('F'.$row)->getFormattedValue();
          $result['meta'] = $sheet->getCell('G'.$row)->getValue();
          $result['peso'] = $sheet->getCell('H'.$row)->getValue();
          $result['frecuencia'] = $sheet->getCell('I'.$row)->getValue();
          $result['valor_rojo'] = $sheet->getCell('J'.$row)->getValue();
          $result['valor_verde'] = $sheet->getCell('K'.$row)->getValue();

          if (empty($result['objetivo'])){

            $ignorar++;
            continue;
          }

          if (empty($result['descripcion'])){

            $result['descripcion'] = $result['objetivo'];
          }

          if (empty($result['codigo'])){

            $ignorar++;
            continue;
          }

          else{

            $codigo = $result['codigo'] . '';
            while (strlen($codigo) < 5){
              
              $codigo = '0' . $codigo;
            }

            $result['codigo'] = $codigo;
          }

          if (empty($result['id_objetivo_corporativo'])){

            $ignorar++;
            continue;
          }

          else{

            $counter = 0;
            $current_type = 0;
            $found = false;

            foreach ($objetivos_corporativos as $key => $value){
              
              if ($current_type != $value->id_tipo){

                $counter = 1;
                $current_type = $value->id_tipo;
              }

              $objetivo_corporativo = $value->letter . $counter;

              if ($objetivo_corporativo == $result['id_objetivo_corporativo']){

                $result['id_objetivo_corporativo'] = $value->id;
                $found = true;
                break;
              }

              $counter++;
            }

            if (!$found){

              $ignorar++;
              continue;
            }
          }

          if (!empty($result['unidad'])){

            if ($result['unidad'] != '%' && $result['unidad'] != '$' && $result['unidad'] != '#' && $result['unidad'] != 'Si/No'){

              $ignorar++;
              continue;
            }
          }

          if (!empty($result['frecuencia'])){

            if ($result['frecuencia'] != 'Mensual' && $result['frecuencia'] != 'Bimestral' && $result['frecuencia'] != 'Trimestral' && $result['frecuencia'] != 'Cuatrimestral' && $result['frecuencia'] != 'Semestral' && $result['frecuencia'] != 'Anual'){

              $ignorar++;
              continue;
            }
          }

          $result['meta'] = str_replace('$', '', str_replace('%', '', str_replace(',', '', $result['meta'])));
          $result['valor_rojo'] = str_replace('$', '', str_replace('%', '', str_replace(',', '', $result['valor_rojo'])));
          $result['valor_verde'] = str_replace('$', '', str_replace('%', '', str_replace(',', '', $result['valor_verde'])));
          $objetivo = DB::table('catalogo_objetivos')->where('codigo', $result['codigo'])->first();

          if (!empty($objetivo)){

            if ($objetivo->objetivo != $result['objetivo'] || $objetivo->descripcion != $result['descripcion'] || $objetivo->direccion != $result['direccion'] || $objetivo_corporativo != $objetivo_corporativo2 || $objetivo->unidad != $result['unidad'] || $objetivo->meta != $result['meta'] || $objetivo->peso != $result['peso'] || $objetivo->frecuencia != $result['frecuencia'] || $objetivo->valor_rojo != $result['valor_rojo'] || $objetivo->valor_verde != $result['valor_verde']){

              $actualizar++;
            }

            DB::table('catalogo_objetivos')->where('codigo', $result['codigo'])->update($result);
          }

          else{

            DB::table('catalogo_objetivos')->insert($result);
            $registrar++;
          }
        }

        Flash::success('La importación fue exitosa<br>Nuevos Registros: ' . $registrar . '<br>Registros Actualizados: ' . $actualizar . '<br>Registros Ignorados: ' . $ignorar);
        return redirect('/evaluacion-resultados/catalogo-objetivos');
      }     
  }

  /**
     * Exporta Plantilla para llenado de objetivos por Puesto
     */
    public function exportar_plantilla_objetivos_puestos(){

      require_once 'PHPExcel.php';
      require_once 'PHPExcel/IOFactory.php';

      $id_plan = $_POST['id_plan'];

      // Create new PHPExcel object
      $objPHPExcel = new \PHPExcel();

      // Create a first sheet, representing sales data
      $objPHPExcel->setActiveSheetIndex(0);

      $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Código Puesto');
      $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Puesto');
      $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Código Objetivo de Catálogo');
      $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Peso');
      $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Código Plan');
      $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Meta Enero');
      $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Semáforo rojo Enero');
      $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Semáforo verde Enero');
      $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Meta Febrero');
      $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Semáforo rojo Febrero');
      $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Semáforo verde Febrero');
      $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Meta Marzo');
      $objPHPExcel->getActiveSheet()->setCellValue('M1', 'Semáforo rojo Marzo');
      $objPHPExcel->getActiveSheet()->setCellValue('N1', 'Semáforo verde Marzo');
      $objPHPExcel->getActiveSheet()->setCellValue('O1', 'Meta Abril');
      $objPHPExcel->getActiveSheet()->setCellValue('P1', 'Semáforo rojo Abril');
      $objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Semáforo verde Abril');
      $objPHPExcel->getActiveSheet()->setCellValue('R1', 'Meta Mayo');
      $objPHPExcel->getActiveSheet()->setCellValue('S1', 'Semáforo rojo Mayo');
      $objPHPExcel->getActiveSheet()->setCellValue('T1', 'Semáforo verde Mayo');
      $objPHPExcel->getActiveSheet()->setCellValue('U1', 'Meta Junio');
      $objPHPExcel->getActiveSheet()->setCellValue('V1', 'Semáforo rojo Junio');
      $objPHPExcel->getActiveSheet()->setCellValue('W1', 'Semáforo verde Junio');
      $objPHPExcel->getActiveSheet()->setCellValue('X1', 'Meta Julio');
      $objPHPExcel->getActiveSheet()->setCellValue('Y1', 'Semáforo rojo Julio');
      $objPHPExcel->getActiveSheet()->setCellValue('Z1', 'Semáforo verde Julio');
      $objPHPExcel->getActiveSheet()->setCellValue('AA1', 'Meta Agosto');
      $objPHPExcel->getActiveSheet()->setCellValue('AB1', 'Semáforo rojo Agosto');
      $objPHPExcel->getActiveSheet()->setCellValue('AC1', 'Semáforo verde Agosto');
      $objPHPExcel->getActiveSheet()->setCellValue('AD1', 'Meta Septiembre');
      $objPHPExcel->getActiveSheet()->setCellValue('AE1', 'Semáforo rojo Septiembre');
      $objPHPExcel->getActiveSheet()->setCellValue('AF1', 'Semáforo verde Septiembre');
      $objPHPExcel->getActiveSheet()->setCellValue('AG1', 'Meta Octubre');
      $objPHPExcel->getActiveSheet()->setCellValue('AH1', 'Semáforo rojo Octubre');
      $objPHPExcel->getActiveSheet()->setCellValue('AI1', 'Semáforo verde Octubre');
      $objPHPExcel->getActiveSheet()->setCellValue('AJ1', 'Meta Noviembre');
      $objPHPExcel->getActiveSheet()->setCellValue('AK1', 'Semáforo rojo Noviembre');
      $objPHPExcel->getActiveSheet()->setCellValue('AL1', 'Semáforo verde Noviembre');
      $objPHPExcel->getActiveSheet()->setCellValue('AM1', 'Meta Diciembre');
      $objPHPExcel->getActiveSheet()->setCellValue('AN1', 'Semáforo rojo Diciembre');
      $objPHPExcel->getActiveSheet()->setCellValue('AO1', 'Semáforo verde Diciembre');
      $objPHPExcel->getActiveSheet()->setCellValue('AP1', 'Meta Mes 13');
      $objPHPExcel->getActiveSheet()->setCellValue('AQ1', 'Semáforo rojo Mes 13');
      $objPHPExcel->getActiveSheet()->setCellValue('AR1', 'Semáforo verde Mes 13');
      $objPHPExcel->getActiveSheet()->setCellValue('AS1', 'Nivel de Logro');
      $objPHPExcel->getActiveSheet()->setCellValue('AT1', 'Porcentaje de Bono');
      $objPHPExcel->getActiveSheet()->setCellValue('AU1', 'Nivel de Logro');
      $objPHPExcel->getActiveSheet()->setCellValue('AV1', 'Porcentaje de Bono');
      $objPHPExcel->getActiveSheet()->setCellValue('AW1', 'Nivel de Logro');
      $objPHPExcel->getActiveSheet()->setCellValue('AX1', 'Porcentaje de Bono');
      $objPHPExcel->getActiveSheet()->setCellValue('AY1', 'Tipo de Bono');
      $objetivos = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('objetivos', 'objetivos.id_empleado', '=', 'employees.id')->join('catalogo_objetivos', 'catalogo_objetivos.id', '=', 'objetivos.id_catalogo')->where('id_plan', $_POST['id_plan'])->select('job_positions.id', 'job_positions.name', 'objetivos.id AS id_objetivo', 'objetivos.peso', 'objetivos.frecuencia', 'objetivos.objetivo', 'objetivos.valor_rojo', 'objetivos.valor_verde', 'tipo_bono', 'codigo', 'meta', 'catalogo_objetivos.peso AS peso_catalogo', 'catalogo_objetivos.valor_rojo AS valor_rojo_catalogo', 'catalogo_objetivos.valor_verde AS valor_verde_catalogo')->groupBy('catalogo_objetivos.id', 'job_positions.id')->orderBy('job_positions.name')->get();

      if (count($objetivos) > 0){

        $row = 2;
        $frecuencias = array();
        $frecuencias['Mensual'] = 1;
        $frecuencias['Bimestral'] = 2;
        $frecuencias['Trimestral'] = 3;
        $frecuencias['Cuatrimestral'] = 4;
        $frecuencias['Semestral'] = 6;
        $frecuencias['Anual'] = 12;
        $columnas_metas = array('F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR');
        $columnas_bono_objetivo = array('AS','AT','AU','AV','AW','AX');

        foreach ($objetivos as $key => $value){

          $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $value->id);
          $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $value->name);
          $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $value->codigo);
          $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $value->peso);
          $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $id_plan);

          $valores_objetivo = DB::table('valores_objetivo')->where('id_objetivo', $value->id_objetivo)->get();

          if (count($valores_objetivo) == 0){

            $index = ($frecuencias[$value->frecuencia] - 1) * 3;
            $column = $columnas_metas[$index];
            $objPHPExcel->getActiveSheet()->setCellValue($column . $row, $value->objetivo);
            $column = $columnas_metas[++$index];
            $objPHPExcel->getActiveSheet()->setCellValue($column . $row, $value->valor_rojo);
            $column = $columnas_metas[++$index];
            $objPHPExcel->getActiveSheet()->setCellValue($column . $row, $value->valor_verde);
          }

          else{

            foreach ($valores_objetivo as $key2 => $valor_objetivo){

              $index = ($frecuencias[$value->frecuencia] * $valor_objetivo->periodo - 1) * 3;

              $column = $columnas_metas[$index];
              $objPHPExcel->getActiveSheet()->setCellValue($column . $row, $valor_objetivo->meta);
              $column = $columnas_metas[++$index];
              $objPHPExcel->getActiveSheet()->setCellValue($column . $row, $valor_objetivo->valor_rojo);
              $column = $columnas_metas[++$index];
              $objPHPExcel->getActiveSheet()->setCellValue($column . $row, $valor_objetivo->valor_verde);
            }
          }

          $bonos_objetivo = DB::table('bonos_objetivos')->where('id_objetivo', $value->id_objetivo)->get();

          if (count($bonos_objetivo) > 0){

            $index = 0;

            foreach ($bonos_objetivo as $key2 => $bono_objetivo){
              
              $objPHPExcel->getActiveSheet()->setCellValue($columnas_bono_objetivo[$index] . $row, $bono_objetivo->nivel_logro);
              $index++;
              $objPHPExcel->getActiveSheet()->setCellValue($columnas_bono_objetivo[$index] . $row, $bono_objetivo->porcentaje_bono);
              $index++;
            }
          }

          $objPHPExcel->getActiveSheet()->setCellValue('AY' . $row, $value->tipo_bono);

          $row++;
        }
      }

      else{

        $objPHPExcel->getActiveSheet()->setCellValue('E2', $id_plan);
      }

      // Rename sheet
      $objPHPExcel->getActiveSheet()->setTitle('Plantilla Objetivos Puestos');

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="Plantilla Objetivos Puestos.xls"');
      header('Cache-Control: max-age=0');
      $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      $objWriter->save('php://output');
    }

    /**
     * Importa Plantilla de Resultados
     */
    public function importar_plantilla_objetivos_puestos(){

      if (!empty($_FILES['file']['name'])){

        $array = explode('.', $_FILES['file']['name']);
        $extension = end($array);
      
        if ($extension == 'xls' || $extension == 'xlsx'){

          move_uploaded_file($_FILES['file']['tmp_name'], '/var/www/vhosts/grupogalber.com/httpdocs/storage/app/Interface/importar_objetivos_catalogo.' . $extension);
          $path = Storage::disk('local')
                            ->getDriver()
                            ->getAdapter()
                            ->getPathPrefix();
          
          $folder = 'Interface/';
          require_once "PHPExcel.php";

          $inputFileName = $path . $folder . 'importar_objetivos_catalogo.' . $extension;
          $excelReader = \PHPExcel_IOFactory::createReaderForFile($inputFileName);
          $objPHPExcel = $excelReader->load($inputFileName);

          // Get worksheet dimensions
          $sheet = $objPHPExcel->getSheet(0); 
          $highestRow = $sheet->getHighestRow();
          $users = array();
          $num_errores = 0;
          $errores = '';
          $id_empleado = 0;
          $registers = array();
          $users_to_register = array();
          $frecuencias = array();
          $frecuencias['Mensual'] = 1;
          $frecuencias['Bimestral'] = 2;
          $frecuencias['Trimestral'] = 3;
          $frecuencias['Cuatrimestral'] = 4;
          $frecuencias['Semestral'] = 6;
          $frecuencias['Anual'] = 12;
          $columnas_metas = array('F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR');
          $columnas_bonos_objetivo = array('AS','AT','AU','AV','AW','AX');

          //  Loop through each row of the worksheet in turn
          for ($row = 2; $row <= $highestRow; $row++){
          
            $registers = array();
            $codigo = $sheet->getCell('C'.$row)->getValue();
            $id_plan = $sheet->getCell('E'.$row)->getValue();
            $job_position_id = $sheet->getCell('A'.$row)->getValue();
            $peso = $sheet->getCell('D'.$row)->getValue();
            $tipo_bono = $sheet->getCell('AY'.$row)->getValue();

            if (!empty($codigo) && !empty($id_plan) && !empty($tipo_bono)){
            //if (!empty($id_empleado) && !empty($id_plan)){

              $users = DB::table('employees')->whereNull('deleted_at')->pluck('id')->toArray();

              if (!empty($job_position_id)){

                $users = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->whereIn('employees.id', $users)->where('job_positions.id', $job_position_id)->pluck('employees.id')->toArray();
              }

              $objetivo = DB::table('catalogo_objetivos')->where('codigo', $codigo)->first();
              $plan = DB::table('planes')->where('id', $id_plan)->select('periodo', 'estado')->first();

              if (!empty($objetivo) && !empty($plan) && !empty($users) && $plan->estado == 1){

                //$ids_empleados = DB::table('objetivos')->where('id_plan', $id_plan)->where('id_catalogo', $objetivo->id)->pluck('id_empleado')->toArray();
                $objetivos = DB::table('objetivos')->where('id_plan', $id_plan)->where('id_catalogo', $objetivo->id)->whereIn('id_empleado', $users)->pluck('id')->toArray();
                DB::table('objetivos')->whereIn('id', $objetivos)->delete();
                DB::table('valores_objetivo')->whereIn('id_objetivo', $objetivos)->delete();
                DB::table('bonos_objetivos')->whereIn('id_objetivo', $objetivos)->delete();
                DB::table('status_plan_empleado')->where('id_plan', $id_plan)->whereIn('id_empleado', $users)->delete();
                $fecha = date('Y-m-d');
                $objetivo_a_insertar = array();
                $objetivo_a_insertar['nombre'] = $objetivo->objetivo;
                $objetivo_a_insertar['id_plan'] = $id_plan;
                $objetivo_a_insertar['id_catalogo'] = $objetivo->id;
                $objetivo_a_insertar['id_objetivo_corporativo'] = $objetivo->id_objetivo_corporativo;
                $objetivo_a_insertar['tipo'] = $objetivo->unidad;
                $objetivo_a_insertar['frecuencia'] = $objetivo->frecuencia;
                $objetivo_a_insertar['periodo'] = $plan->periodo;
                $objetivo_a_insertar['peso'] = $peso;
                $objetivo_a_insertar['tipo_bono'] = $tipo_bono;

                if (empty($peso)){

                  if (empty($objetivo->peso)){

                    $num_errores++;
                    $errores .= $num_errores . '. La fila ' . $row . ' no contiene un peso ni el objetivo de catálogo<br>';
                    continue;
                  }

                  $objetivo_a_insertar['peso'] = $objetivo->peso;
                }

                $frecuencia = $frecuencias[$objetivo->frecuencia];
                $month = $frecuencia;

                while ($month < $plan->periodo){
                  
                  $month = $month + $frecuencia; 
                }

                $index = ($month - 1) * 3;
                $metas = $valores_rojos = $valores_verdes = $periodos = array();
                $niveles_logros = $porcentajes_bonos = array();

                for ($i = $index;$i < count($columnas_metas);$i = $i + $frecuencia * 3){

                  $j = $i;
                  $meta = $sheet->getCell($columnas_metas[$j].$row)->getValue();
                  $j++;
                  $valor_rojo = $sheet->getCell($columnas_metas[$j].$row)->getValue();
                  $j++;
                  $valor_verde = $sheet->getCell($columnas_metas[$j].$row)->getValue();

                  if ((empty($meta) && $meta != '0') || (empty($valor_rojo) && $valor_rojo != '0') || (empty($valor_verde) && $valor_verde != '0')){

                    /*if ($i == $index){
                      
                      break;
                    }*/
                    continue;
                  }

                  else{

                    $metas[] = $meta;
                    $valores_rojos[] = $valor_rojo;
                    $valores_verdes[] = $valor_verde;
                    $periodo = intval($i / 3);

                    if ($i % 3 != 0){

                      $periodo++;
                    }

                    $periodo++;
                    $periodo = $periodo / $frecuencia;
                    $periodos[] = $periodo;
                  }
                }

                if ($tipo_bono != 'Comision'){

                  for ($i = 0;$i < count($columnas_bonos_objetivo);$i = $i + 2){

                    $j = $i;
                    $nivel_logro = $sheet->getCell($columnas_bonos_objetivo[$j].$row)->getValue();
                    $j++;
                    $porcentaje_bono = $sheet->getCell($columnas_bonos_objetivo[$j].$row)->getValue();

                    if (empty($nivel_logro) || empty($porcentaje_bono)){
                      
                      break;
                    }

                    else{

                      $niveles_logros[] = $nivel_logro;
                      $porcentajes_bonos[] = $porcentaje_bono;
                    }
                  }
                }

                if (empty($metas) || count($metas) == 1){

                  if (count($metas) == 1){

                    $objetivo_a_insertar['objetivo'] = $metas[0];
                    $objetivo_a_insertar['valor_rojo'] = $valores_rojos[0];
                    $objetivo_a_insertar['valor_verde'] = $valores_verdes[0];
                  }

                  else{

                    if ((empty($objetivo->meta) && $objetivo->meta != '0' && $tipo_bono != 'Comision') || (empty($objetivo->valor_rojo) && $tipo_bono != 'Comision') || (empty($objetivo->valor_verde) && $tipo_bono != 'Comision')){

                      $num_errores++;
                      $errores .= $num_errores . '. La fila ' . $row . ' no contiene valores para el semáforo<br>';
                      continue;
                    }

                    $objetivo_a_insertar['objetivo'] = $objetivo->meta;
                    $objetivo_a_insertar['valor_rojo'] = $objetivo->valor_rojo;
                    $objetivo_a_insertar['valor_verde'] = $objetivo->valor_verde;
                  }
                }

                $objetivo_a_insertar['periodo'] = $plan->periodo;
                $uniques_ids = array();

                foreach ($users as $key => $value){
                
                  $objetivo_a_insertar['id_empleado'] = $value;
                  $objetivo_a_insertar['unique_id'] = uniqid(rand());
                  $uniques_ids[] = $objetivo_a_insertar['unique_id'];
                  $registers[] = $objetivo_a_insertar;

                  if (!in_array($value, $users_to_register)){

                    $users_to_register[] = $value;
                  }
                }

                DB::table('objetivos')->insert($registers);
                $registers = array();
                /*$status_plan_empleado = array();
                $status_plan_empleado['status'] = 3;
                $status_plan_empleado['fecha_solicitado'] = $fecha;
                $status_plan_empleado['fecha_autorizado'] = $fecha;
                $status_plan_empleado['periodo'] = $plan->periodo;
                $status_plan_empleado['id_plan'] = $id_plan;

                foreach ($users_to_register as $key => $value){
                  
                  $status_plan_empleado['id_empleado'] = $value;
                  $registers[] = $status_plan_empleado;
                }
                    
                DB::table('status_plan_empleado')->insert($registers);*/

                if ((!empty($metas) && count($metas) > 1) || (!empty($niveles_logros))){

                  $objetivos = DB::table('objetivos')->whereIn('unique_id', $uniques_ids)->pluck('id')->toArray();

                  if (!empty($metas) && count($metas) > 1){

                    $valores_objetivos = array();
                    $valores_objetivo = array();

                    foreach ($objetivos as $key => $value){
                    
                      $valor_objetivo['id_objetivo'] = $value;

                      foreach ($metas as $key2 => $value2){
                      
                        $valor_objetivo['periodo'] = $periodos[$key2];
                        $valor_objetivo['meta'] = $value2;
                        $valor_objetivo['valor_rojo'] = $valores_rojos[$key2];
                        $valor_objetivo['valor_verde'] = $valores_verdes[$key2];
                        $valores_objetivos[] = $valor_objetivo;
                      }
                    }

                    DB::table('valores_objetivo')->insert($valores_objetivos);
                  }

                  if (!empty($niveles_logros)){

                    $bonos_objetivos = array();
                    $bono_objetivo = array();

                    foreach ($objetivos as $key => $value){
                    
                      $bono_objetivo['id_objetivo'] = $value;

                      foreach ($niveles_logros as $key2 => $value2){
                      
                        $bono_objetivo['nivel_logro'] = str_replace('%', '', $value2);
                        $bono_objetivo['porcentaje_bono'] = str_replace('%', '', $porcentajes_bonos[$key2]);
                        $bonos_objetivos[] = $bono_objetivo;
                      }
                    }

                    DB::table('bonos_objetivos')->insert($bonos_objetivos);
                  }
                }
              }

              else{

                $num_errores++;

                if (empty($objetivo)){

                  $errores .= $num_errores . '. La fila ' . $row . ' no contiene un código de objetivo válido<br>';
                }

                else{

                  if (empty($plan)){

                    $errores .= $num_errores . '. La fila ' . $row . ' no contiene un id de plan válido<br>';
                  }

                  else{

                    if (empty($users)){

                      $errores .= $num_errores . '. La fila ' . $row . ' no contiene usuarios a quien asignarle objetivos<br>';
                    }

                    else{

                      $errores .= $num_errores . '. La fila ' . $row . ' no contiene un id de plan con estado preparatorio <br>';
                    }
                  }
                }
              }
            }

            else{

              $num_errores++;

              if (empty($codigo)){

                $errores .= $num_errores . '. La fila ' . $row . ' no contiene un código de objetivo<br>';
              }

              else{

                if (empty($id_plan)){

                  $errores .= $num_errores . '. La fila ' . $row . ' no contiene un plan<br>';
                }

                else{

                  $errores .= $num_errores . '. La fila ' . $row . ' no contiene tipo de bono<br>';
                }
              }
            }
          }

          if ($num_errores == 0){
      
            flash('Los objetivos fueron guardados');
          }

          else{

            flash($errores);
          }
        }

        else{
      
          flash("El archivo de importacion de objetivos no es un archivo excel");
        }
      }

      return redirect('/evaluacion-resultados/planes/' . $_POST['id_plan'] . '/edit');
    }

    /**
   * Exporta Plantilla de Objetivos por RFC
   */
  public function exportar_plantilla_objetivos_rfc(){

    require_once 'PHPExcel.php';
    require_once 'PHPExcel/IOFactory.php';

    $id_plan = $_POST['id_plan'];

    // Create new PHPExcel object
    $objPHPExcel = new \PHPExcel();

    // Create a first sheet, representing sales data
    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Consecutivo');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'RFC');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Colaborador');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Código Objetivo de Catálogo');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Peso');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Código Plan');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Meta Enero');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Semáforo rojo Enero');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Semáforo verde Enero');
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Meta Febrero');
    $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Semáforo rojo Febrero');
    $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Semáforo verde Febrero');
    $objPHPExcel->getActiveSheet()->setCellValue('M1', 'Meta Marzo');
    $objPHPExcel->getActiveSheet()->setCellValue('N1', 'Semáforo rojo Marzo');
    $objPHPExcel->getActiveSheet()->setCellValue('O1', 'Semáforo verde Marzo');
    $objPHPExcel->getActiveSheet()->setCellValue('P1', 'Meta Abril');
    $objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Semáforo rojo Abril');
    $objPHPExcel->getActiveSheet()->setCellValue('R1', 'Semáforo verde Abril');
    $objPHPExcel->getActiveSheet()->setCellValue('S1', 'Meta Mayo');
    $objPHPExcel->getActiveSheet()->setCellValue('T1', 'Semáforo rojo Mayo');
    $objPHPExcel->getActiveSheet()->setCellValue('U1', 'Semáforo verde Mayo');
    $objPHPExcel->getActiveSheet()->setCellValue('V1', 'Meta Junio');
    $objPHPExcel->getActiveSheet()->setCellValue('W1', 'Semáforo rojo Junio');
    $objPHPExcel->getActiveSheet()->setCellValue('X1', 'Semáforo verde Junio');
    $objPHPExcel->getActiveSheet()->setCellValue('Y1', 'Meta Julio');
    $objPHPExcel->getActiveSheet()->setCellValue('Z1', 'Semáforo rojo Julio');
    $objPHPExcel->getActiveSheet()->setCellValue('AA1', 'Semáforo verde Julio');
    $objPHPExcel->getActiveSheet()->setCellValue('AB1', 'Meta Agosto');
    $objPHPExcel->getActiveSheet()->setCellValue('AC1', 'Semáforo rojo Agosto');
    $objPHPExcel->getActiveSheet()->setCellValue('AD1', 'Semáforo verde Agosto');
    $objPHPExcel->getActiveSheet()->setCellValue('AE1', 'Meta Septiembre');
    $objPHPExcel->getActiveSheet()->setCellValue('AF1', 'Semáforo rojo Septiembre');
    $objPHPExcel->getActiveSheet()->setCellValue('AG1', 'Semáforo verde Septiembre');
    $objPHPExcel->getActiveSheet()->setCellValue('AH1', 'Meta Octubre');
    $objPHPExcel->getActiveSheet()->setCellValue('AI1', 'Semáforo rojo Octubre');
    $objPHPExcel->getActiveSheet()->setCellValue('AJ1', 'Semáforo verde Octubre');
    $objPHPExcel->getActiveSheet()->setCellValue('AK1', 'Meta Noviembre');
    $objPHPExcel->getActiveSheet()->setCellValue('AL1', 'Semáforo rojo Noviembre');
    $objPHPExcel->getActiveSheet()->setCellValue('AM1', 'Semáforo verde Noviembre');
    $objPHPExcel->getActiveSheet()->setCellValue('AN1', 'Meta Diciembre');
    $objPHPExcel->getActiveSheet()->setCellValue('AO1', 'Semáforo rojo Diciembre');
    $objPHPExcel->getActiveSheet()->setCellValue('AP1', 'Semáforo verde Diciembre');
    /*$objPHPExcel->getActiveSheet()->setCellValue('AQ1', 'Meta Mes 13');
    $objPHPExcel->getActiveSheet()->setCellValue('AR1', 'Semáforo rojo Mes 13');
    $objPHPExcel->getActiveSheet()->setCellValue('AS1', 'Semáforo verde Mes 13');
    $objPHPExcel->getActiveSheet()->setCellValue('AT1', 'Nivel de Logro');
    $objPHPExcel->getActiveSheet()->setCellValue('AU1', 'Porcentaje de Bono');
    $objPHPExcel->getActiveSheet()->setCellValue('AV1', 'Nivel de Logro');
    $objPHPExcel->getActiveSheet()->setCellValue('AW1', 'Porcentaje de Bono');
    $objPHPExcel->getActiveSheet()->setCellValue('AX1', 'Nivel de Logro');
    $objPHPExcel->getActiveSheet()->setCellValue('AY1', 'Porcentaje de Bono');
    $objPHPExcel->getActiveSheet()->setCellValue('AZ1', 'Tipo de Bono');*/
    $objetivos = DB::table('employees')->join('objetivos', 'objetivos.id_empleado', '=', 'employees.id')->join('catalogo_objetivos', 'catalogo_objetivos.id', '=', 'objetivos.id_catalogo')->where('id_plan', $_POST['id_plan'])->select('employees.nombre', 'paterno', 'materno', 'objetivos.id AS id_objetivo', 'objetivos.peso', 'objetivos.frecuencia', 'objetivos.objetivo', 'objetivos.valor_rojo', 'objetivos.valor_verde', 'codigo', 'meta', 'catalogo_objetivos.peso AS peso_catalogo', 'catalogo_objetivos.valor_rojo AS valor_rojo_catalogo', 'catalogo_objetivos.valor_verde AS valor_verde_catalogo', 'rfc')->orderBy('employees.id')->get();

    if (count($objetivos) > 0){

      $row = 2;
      $frecuencias = array();
      $frecuencias['Mensual'] = 1;
      $frecuencias['Bimestral'] = 2;
      $frecuencias['Trimestral'] = 3;
      $frecuencias['Cuatrimestral'] = 4;
      $frecuencias['Semestral'] = 6;
      $frecuencias['Anual'] = 12;
      $columnas_metas = array('G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP');
      //$columnas_bono_objetivo = array('AT','AU','AV','AW','AX','AY');

      foreach ($objetivos as $key => $value){

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $row - 1);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $value->rfc);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $value->nombre . ' ' . $value->paterno . ' ' . $value->materno);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $value->codigo);
        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $value->peso);
        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $id_plan);

        //if ($value->tipo_bono != 'Comision'){

          $valores_objetivo = DB::table('valores_objetivo')->where('id_objetivo', $value->id_objetivo)->get();

          if (count($valores_objetivo) == 0){

            $index = ($frecuencias[$value->frecuencia] - 1) * 3;
            $column = $columnas_metas[$index];
            $objPHPExcel->getActiveSheet()->setCellValue($column . $row, $value->objetivo);
            $column = $columnas_metas[++$index];
            $objPHPExcel->getActiveSheet()->setCellValue($column . $row, $value->valor_rojo);
            $column = $columnas_metas[++$index];
            $objPHPExcel->getActiveSheet()->setCellValue($column . $row, $value->valor_verde);
          }

          else{

            foreach ($valores_objetivo as $key2 => $valor_objetivo){

              $index = ($frecuencias[$value->frecuencia] * $valor_objetivo->periodo - 1) * 3;
              $column = $columnas_metas[$index];
              $objPHPExcel->getActiveSheet()->setCellValue($column . $row, $valor_objetivo->meta);
              $column = $columnas_metas[++$index];
              $objPHPExcel->getActiveSheet()->setCellValue($column . $row, $valor_objetivo->valor_rojo);
              $column = $columnas_metas[++$index];
              $objPHPExcel->getActiveSheet()->setCellValue($column . $row, $valor_objetivo->valor_verde);
            }
          }

          /*$bonos_objetivo = DB::table('bonos_objetivos')->where('id_objetivo', $value->id_objetivo)->get();

          if (count($bonos_objetivo) > 0){

            $index = 0;

            foreach ($bonos_objetivo as $key2 => $bono_objetivo){
      
              $objPHPExcel->getActiveSheet()->setCellValue($columnas_bono_objetivo[$index] . $row, $bono_objetivo->nivel_logro);
              $index++;
              $objPHPExcel->getActiveSheet()->setCellValue($columnas_bono_objetivo[$index] . $row, $bono_objetivo->porcentaje_bono);
              $index++;
            }
          }*/
        //}

        //$objPHPExcel->getActiveSheet()->setCellValue('AZ' . $row, $value->tipo_bono);
        $row++;
      }
    }

    else{

      $objPHPExcel->getActiveSheet()->setCellValue('E2', $id_plan);
    }

    // Rename sheet
    $objPHPExcel->getActiveSheet()->setTitle('Plantilla Objetivos por RFC');

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Plantilla Objetivos por RFC.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
  }

/**
 * Importa Plantilla de Resultados por rfc
 */
public function importar_plantilla_objetivos_rfc(){
  if (!empty($_FILES['file']['name'])){
    if(isset($_POST['elcheck'])) {
      $check = true;
    } else {
      $check = false;
    }
    $array = explode('.', $_FILES['file']['name']);
    $extension = end($array);
    
    if ($extension == 'xls' || $extension == 'xlsx'){
      $path_storage = storage_path(). '/app/Interface/importar_objetivos_catalogo_rfc.'. $extension;
      // move_uploaded_file($_FILES['file']['tmp_name'], '/var/www/vhosts/soysepanka.com/test.somosucin.soysepanka.com/storage/app/Interface/importar_objetivos_catalogo_rfc.' . $extension);
      move_uploaded_file($_FILES['file']['tmp_name'], $path_storage);
      $path = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
      
      $folder = 'Interface/';
      require_once "PHPExcel.php";
      // dd($path_storage);
      // $inputFileName = $path . $folder . 'importar_objetivos_catalogo_rfc.' . $extension;
      $inputFileName = $path_storage;
      $excelReader = \PHPExcel_IOFactory::createReaderForFile($inputFileName);
      $objPHPExcel = $excelReader->load($inputFileName);

      // Get worksheet dimensions
      $sheet = $objPHPExcel->getSheet(0); 
      $highestRow = $sheet->getHighestRow();
      $users = array();
      $num_errores = 0;
      $errores = '';
      $id_empleado = 0;
      $registers = array();
      $users_to_register = array();
      $frecuencias = array();
      $frecuencias['Mensual'] = 1;
      $frecuencias['Bimestral'] = 2;
      $frecuencias['Trimestral'] = 3;
      $frecuencias['Cuatrimestral'] = 3;
      $frecuencias['Semestral'] = 6;
      $frecuencias['Anual'] = 12;
      $columnas_metas = array('G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS');
      //$columnas_bonos_objetivo = array('AT','AU','AV','AW','AX','AY');
  
      //  Loop through each row of the worksheet in turn
      for ($row = 2; $row <= $highestRow; $row++){      
        $registers = array();
        $codigo = $sheet->getCell('D'.$row)->getValue();
        $id_plan = $sheet->getCell('F'.$row)->getValue();
        // $job_position_id = $sheet->getCell('A'.$row)->getValue();
        $rfc = $sheet->getCell('B'.$row)->getValue();
        $peso = $sheet->getCell('E'.$row)->getValue();
        //$tipo_bono = $sheet->getCell('AZ'.$row)->getValue();
          
        if (!empty($codigo) && !empty($id_plan)/* && !empty($tipo_bono)*/){
          // $users = DB::table('employees')->whereNull('deleted_at')->pluck('id')->toArray();
          if (!empty($rfc)){
            $users = DB::table('employees')->where('rfc', $rfc)->whereNull('deleted_at')->pluck('id')->toArray();
            // $users = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->whereIn('employees.id', $users)->where('job_positions.id', $job_position_id)->pluck('employees.id')->toArray();
          }
          $objetivo = DB::table('catalogo_objetivos')->where('codigo', $codigo)->first();
          $plan = DB::table('planes')->where('id', $id_plan)->select('periodo', 'estado')->first();

          if (!empty($objetivo) && !empty($plan) && !empty($users) && $plan->estado == 1){
            $objetivos = DB::table('objetivos')->where('id_plan', $id_plan)->where('id_catalogo', $objetivo->id)->whereIn('id_empleado', $users)->pluck('id')->toArray();
            if($check) {
              DB::table('objetivos')->whereIn('id', $objetivos)->delete();
              DB::table('valores_objetivo')->whereIn('id_objetivo', $objetivos)->delete();
              //DB::table('bonos_objetivos')->whereIn('id_objetivo', $objetivos)->delete();
              DB::table('status_plan_empleado')->where('id_plan', $id_plan)->whereIn('id_empleado', $users)->delete();
            } 
            $fecha = date('Y-m-d');
            $objetivo_a_insertar = array();
            $objetivo_a_insertar['nombre'] = $objetivo->objetivo;
            $objetivo_a_insertar['id_plan'] = $id_plan;
            $objetivo_a_insertar['id_catalogo'] = $objetivo->id;
            $objetivo_a_insertar['id_objetivo_corporativo'] = $objetivo->id_objetivo_corporativo;
            $objetivo_a_insertar['tipo'] = $objetivo->unidad;
            $objetivo_a_insertar['frecuencia'] = $objetivo->frecuencia;
            $objetivo_a_insertar['periodo'] = $plan->periodo;
            $objetivo_a_insertar['peso'] = $peso;
            //$objetivo_a_insertar['tipo_bono'] = $tipo_bono;

            if (empty($peso) && $peso != '0'){
              if (empty($objetivo->peso) && $objetivo->peso != '0'){
                $num_errores++;
                $errores .= $num_errores . '. La fila ' . $row . ' no contiene un peso ni el objetivo de catálogo<br>';
                continue;
              }
              $objetivo_a_insertar['peso'] = $objetivo->peso;
            }
            $frecuencia = $frecuencias[$objetivo->frecuencia];
            $month = $frecuencia;
            while ($month < $plan->periodo){
              $month = $month + $frecuencia; 
            }

            $index = ($month - 1) * 3;
            $metas = $valores_rojos = $valores_verdes = $periodos = array();
            $niveles_logros = $porcentajes_bonos = array();

            for ($i = $index;$i < count($columnas_metas);$i = $i + $frecuencia * 3){
              $j = $i;
              $meta = $sheet->getCell($columnas_metas[$j].$row)->getValue();
              $j++;
              $valor_rojo = $sheet->getCell($columnas_metas[$j].$row)->getValue();
              $j++;
              $valor_verde = $sheet->getCell($columnas_metas[$j].$row)->getValue();
              if ((empty($meta) && $meta != '0') || (empty($valor_rojo) && $valor_rojo != '0') || (empty($valor_verde) && $valor_verde != '0')){
                continue;
              } else {
                $metas[] = $meta;
                $valores_rojos[] = $valor_rojo;
                $valores_verdes[] = $valor_verde;
                $periodo = intval($i / 3);

                if ($i % 3 != 0){
                  $periodo++;
                }
                $periodo++;
                $periodo = $periodo / $frecuencia;
                $periodos[] = $periodo;
              }
            }
            /*if ($tipo_bono != 'Comision'){
              for ($i = 0;$i < count($columnas_bonos_objetivo);$i = $i + 2){
                $j = $i;
                $nivel_logro = $sheet->getCell($columnas_bonos_objetivo[$j].$row)->getValue();
                $j++;
                $porcentaje_bono = $sheet->getCell($columnas_bonos_objetivo[$j].$row)->getValue();

                if (empty($nivel_logro) || empty($porcentaje_bono)){
                  break;
                } else{
                  $niveles_logros[] = $nivel_logro;
                  $porcentajes_bonos[] = $porcentaje_bono;
                }
              }
            }*/

            //if (empty($metas) || count($metas) == 1){
              if (count($metas) > 0){
                //if ($tipo_bono != 'Comision'){
                  $objetivo_a_insertar['objetivo'] = $metas[0];
                  $objetivo_a_insertar['valor_rojo'] = $valores_rojos[0];
                  $objetivo_a_insertar['valor_verde'] = $valores_verdes[0];
                /*}
                else{
                  $objetivo_a_insertar['objetivo'] = '';
                  $objetivo_a_insertar['valor_rojo'] = '';
                  $objetivo_a_insertar['valor_verde'] = '';
                }*/
              }else{
                //if ((empty($objetivo->meta) && $objetivo->meta != '0' && $tipo_bono != 'Comision') || (empty($objetivo->valor_rojo) && $tipo_bono != 'Comision') || (empty($objetivo->valor_verde) && $tipo_bono != 'Comision')){
                  $num_errores++;
                  $errores .= $num_errores . '. La fila ' . $row . ' no contiene valores para el semáforo<br>';
                  continue;
                //}

                /*$objetivo_a_insertar['objetivo'] = $objetivo->meta;
                $objetivo_a_insertar['valor_rojo'] = $objetivo->valor_rojo;
                $objetivo_a_insertar['valor_verde'] = $objetivo->valor_verde;*/
              }
            //}

            $objetivo_a_insertar['periodo'] = $plan->periodo;
            $uniques_ids = array();

            foreach ($users as $key => $value){
              $objetivo_a_insertar['id_empleado'] = $value;
              $objetivo_a_insertar['unique_id'] = uniqid(rand());
              $uniques_ids[] = $objetivo_a_insertar['unique_id'];
              $registers[] = $objetivo_a_insertar;

              if (!isset($users_to_register[$id_plan])){

                $users_to_register[$id_plan] = array();
              }

              if (!in_array($value, $users_to_register[$id_plan])){

                $users_to_register[$id_plan][] = $value;
              }
            }
            
            if($check) {
              DB::table('objetivos')->insert($registers);
            } else {
              if(!empty($objetivo_a_insertar['objetivo'])) {
                // $objetivo_codigo = $objetivo->codigo;
                $objetivo_codigo = $objetivo->id;
                $objetivo_corp = $objetivo->id_objetivo_corporativo;
                $id_empleado = $users[0];

                // dd($registers[0]['id_objetivo_corporativo']);
                $existe = DB::table('objetivos')->where('id_plan', $id_plan)
                ->where('id_empleado', $id_empleado)
                ->where('id_catalogo', $objetivo_codigo)
                ->first();

                if($existe) {
                  DB::table('objetivos')
                  ->where('id_plan', $id_plan)
                  ->where('id_empleado', $id_empleado)
                  ->where('id_catalogo', $objetivo_codigo)
                  // ->where('id_objetivo_corporativo', $objetivo_corp)
                  ->update(['objetivo'=>$registers[0]['objetivo'],'valor_rojo'=>$registers[0]['valor_rojo'], 'valor_verde'=>$registers[0]['valor_verde']]);
                } else {
                  DB::table('objetivos')->insert($registers);
                }
              }
            }
            $registers = array();

            if (!empty($_POST['autorizados']) && $check){

              $status_plan_empleado = array();
              $status_plan_empleado['status'] = 3;
              $status_plan_empleado['fecha_solicitado'] = $fecha;
              $status_plan_empleado['fecha_autorizado'] = $fecha;
              $status_plan_empleado['periodo'] = $plan->periodo;
              $status_plan_empleado['id_plan'] = $id_plan;

              if (!empty($users_to_register[$id_plan])){

                foreach ($users_to_register[$id_plan] as $key => $value){
        
                  $status_plan_empleado['id_empleado'] = $value;
                  $registers[] = $status_plan_empleado;
                }
          
                DB::table('status_plan_empleado')->insert($registers);
              }
            }

            /*if ($tipo_bono != 'Comision' && ((!empty($metas) && count($metas) > 1) || (!empty($niveles_logros)))){
              $objetivos = DB::table('objetivos')->whereIn('unique_id', $uniques_ids)->pluck('id')->toArray();
              if (!empty($metas) && count($metas) > 1){
                $valores_objetivos = array();
                $valores_objetivo = array();
                foreach ($objetivos as $key => $value){
                  $valor_objetivo['id_objetivo'] = $value;
                  foreach ($metas as $key2 => $value2){
                    $valor_objetivo['periodo'] = $periodos[$key2];
                    $valor_objetivo['meta'] = $value2;
                    $valor_objetivo['valor_rojo'] = $valores_rojos[$key2];
                    $valor_objetivo['valor_verde'] = $valores_verdes[$key2];
                    $valores_objetivos[] = $valor_objetivo;
                  }
                }
                if($check) {
                  DB::table('valores_objetivo')->insert($valores_objetivos);
                }
              }
              
              if (!empty($niveles_logros)){
                $bonos_objetivos = array();
                $bono_objetivo = array();
                foreach ($objetivos as $key => $value){
                  $bono_objetivo['id_objetivo'] = $value;
                  foreach ($niveles_logros as $key2 => $value2){
                    $bono_objetivo['nivel_logro'] = str_replace('%', '', $value2);
                    $bono_objetivo['porcentaje_bono'] = str_replace('%', '', $porcentajes_bonos[$key2]);
                    $bonos_objetivos[] = $bono_objetivo;
                  }
                }
                if($check) {
                  DB::table('bonos_objetivos')->insert($bonos_objetivos);
                }
                
              }
            }*/
          }else{
            $num_errores++;
            if (empty($objetivo)){
              $errores .= $num_errores . '. La fila ' . $row . ' no contiene un código de objetivo válido<br>';
            }else{  
              if (empty($plan)){
                $errores .= $num_errores . '. La fila ' . $row . ' no contiene un id de plan válido<br>';
              }else{
                if (empty($users)){
                  $errores .= $num_errores . '. La fila ' . $row . ' no contiene usuarios a quien asignarle objetivos<br>';
                }else{
                  $errores .= $num_errores . '. La fila ' . $row . ' no contiene un id de plan con estado preparatorio <br>';
                }
              }
            }
          }
        }else{
          $num_errores++; 
          if (empty($codigo)){
            $errores .= $num_errores . '. La fila ' . $row . ' no contiene un código de objetivo<br>';
          }else{
            if (empty($id_plan)){
              $errores .= $num_errores . '. La fila ' . $row . ' no contiene un plan<br>';
            }/*else{
              $errores .= $num_errores . '. La fila ' . $row . ' no contiene tipo de bono<br>';
            }*/
          }
        }
      }

      if ($num_errores == 0){
        flash('Los objetivos fueron guardados');
      }else{
        flash($errores);
      }
    }else{
      flash("El archivo de importacion de objetivos por rfc no es un archivo excel");
    }
  }

  return redirect('/evaluacion-resultados/planes/' . $_POST['id_plan'] . '/edit');
}

    /**
     * Exporta Plantilla para llenado de bonos
     */
    public function exportar_plantilla_bonos(){

      require_once 'PHPExcel.php';
      require_once 'PHPExcel/IOFactory.php';

      $id_plan = $_POST['id_plan'];

      // Create new PHPExcel object
      $objPHPExcel = new \PHPExcel();

      // Create a first sheet, representing sales data
      $objPHPExcel->setActiveSheetIndex(0);

      $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Número de empleado');
      $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Nombre');
      $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Plan');
      $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Bono');
      $users = DB::table('employees')->join('objetivos', 'objetivos.id_empleado', '=', 'employees.id')->where('id_plan', $_POST['id_plan'])->groupBy('id_empleado')->pluck('id_empleado')->toArray();
      $bonos = DB::table('bonos_planes')->where('id_plan', $_POST['id_plan'])->pluck('total_bono', 'id_user')->toArray();
      $empleados = DB::table('employees')->whereIn('id', $users)->select('id', 'nombre', 'paterno', 'materno', 'idempleado')->get();
      $row = 2;

      foreach ($empleados as $key => $value){

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $value->idempleado);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $value->nombre . ' ' . $value->paterno . ' ' . $value->materno);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $_POST['id_plan']);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, (isset($bonos[$value->id]) ? $bonos[$value->id] : ''));
        $row++;
      }

      // Rename sheet
      $objPHPExcel->getActiveSheet()->setTitle('Plantilla Bonos');

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="Plantilla Bonos.xls"');
      header('Cache-Control: max-age=0');
      $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      $objWriter->save('php://output');
    }

    /**
     * Importa Plantilla de Bonos
     */
    public function importar_plantilla_bonos(){

      if (!empty($_FILES['file']['name'])){

        $array = explode('.', $_FILES['file']['name']);
        $extension = end($array);
      
        if ($extension == 'xls' || $extension == 'xlsx'){

          move_uploaded_file($_FILES['file']['tmp_name'], '/var/www/vhosts/grupogalber.com/httpdocs/storage/app/Interface/importar_objetivos_catalogo.' . $extension);
          $path = Storage::disk('local')
                            ->getDriver()
                            ->getAdapter()
                            ->getPathPrefix();
          
          $folder = 'Interface/';
          require_once "PHPExcel.php";

          $inputFileName = $path . $folder . 'importar_objetivos_catalogo.' . $extension;
          $excelReader = \PHPExcel_IOFactory::createReaderForFile($inputFileName);
          $objPHPExcel = $excelReader->load($inputFileName);

          // Get worksheet dimensions
          $sheet = $objPHPExcel->getSheet(0); 
          $highestRow = $sheet->getHighestRow();
          $users = array();
          $id_empleado = 0;
          $bono_plan = array();
          $bonos_planes = array();

          //  Loop through each row of the worksheet in turn
          for ($row = 2; $row <= $highestRow; $row++){
          
            $bono = $sheet->getCell('D'.$row)->getValue();
            $bono = str_replace('$', '', str_replace(',', '', $bono));
            $id_plan = $sheet->getCell('C'.$row)->getValue();
            $idempleado = $sheet->getCell('A'.$row)->getValue();

            if (!empty($bono) && !empty($id_plan) && !empty($idempleado)){
            //if (!empty($id_empleado) && !empty($id_plan)){

              $bono_plan['id_plan'] = $id_plan;
              $user = DB::table('employees')->where('idempleado', $idempleado)->select('id')->first();

              if (!empty($user)){

                DB::table('bonos_planes')->where('id_plan', $_POST['id_plan'])->where('id_user', $user->id)->delete();
                $bono_plan['id_user'] = $user->id;
                $bono_plan['total_bono'] = $bono;
                $bonos_planes[] = $bono_plan;
              }
            }
          }

          DB::table('bonos_planes')->insert($bonos_planes);
          flash('Los objetivos fueron guardados');
        }

        else{
      
          flash("El archivo de importacion de bonos no es un archivo excel");
        }
      }

      return redirect('/evaluacion-resultados/planes/' . $_POST['id_plan'] . '/edit');
    }
	
  /**
  * Carga la pagina de cumplimiento de compromisos
  *
  * @return void
  */
  public function cumplimiento_de_compromisos(){
        
    // Id del usuario logueado
    $id_user = auth()->user()->employee_id;

    $id_plan = 0;
    $start_month = 1;
    $year = 0;
    $responsabilities = $notas = $plan = array();
    $objetivos_corporativos = array();
    $canEdit = true;

    // Obtiene todos los planes abiertos
    $planes = DB::select("SELECT * FROM planes WHERE estado = ? ORDER BY id DESC", [2]);

    // Se requiere ver un plan en particular
    if (!empty($_POST['id_plan']) || Session::has('id_plan')){

      if (!empty($_POST['id_plan'])){

        $id_plan = $_POST['id_plan'];
        Session::put('id_plan', $id_plan);
      }

      else{

        $id_plan = Session::get('id_plan');
        $found = false;

        foreach ($planes as $key => $value){
          
          if ($value->id == $id_plan){

            $found = true;
            break;
          }
        }

        if (!$found){

          $id_plan = $planes[0]->id;
          Session::forget('id_plan');
        }
      }
    }

    // No se requiere ver un plan en particular
    else{

      // Existen planes aviertos
      if (!empty($planes)){

        // Se elige el id del plan mas reciente
        $id_plan = $planes[0]->id;
      }
    }

    // Existe un plan abierto
    if ($id_plan != 0){

      foreach ($planes as $key => $value){
        
        if ($value->id == $id_plan){

          $plan = $value;
          $start_month = $plan->periodo;
          $year = $plan->anio;
          break;
        }
      }

      if (!empty($id_user)){

        // Se obtienen todas las responsabilidades relacionadas con el plan y sus cumplimientos (si es que ya hay registrados) y que el usuario logueado sea el capturista
        $responsabilities = DB::select("SELECT responsabilidades_compromisos.id, responsabilidades_compromisos.description, responsabilidades_compromisos.frecuencia, responsabilidades_compromisos.frecuencia_captura, responsabilidades_compromisos.meta, responsabilidades_compromisos.unidad, responsabilidades_compromisos.valor_rojo, responsabilidades_compromisos.valor_verde, responsabilidades_compromisos.id_objetivo_corporativo, cumplimientos_compromisos.id AS id_cumplimiento, cumplimientos_compromisos.logro, cumplimientos_compromisos.periodo, cumplimientos_compromisos.revisado FROM responsabilidades_compromisos INNER JOIN objetivos_corporativos ON (objetivos_corporativos.id = responsabilidades_compromisos.id_objetivo_corporativo) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) LEFT JOIN cumplimientos_compromisos ON (responsabilidades_compromisos.id = cumplimientos_compromisos.id_responsabilidad) WHERE responsabilidades_compromisos.id_plan = ? AND id_capturista = ? ORDER BY tipos_objetivos_corporativos.id, responsabilidades_compromisos.id_objetivo_corporativo, responsabilidades_compromisos.id", [$id_plan, $id_user]);
      }

      if (empty($responsabilities)){

        // El usuario logueado es administrador
        if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

          $canEdit = false;

          // Se obtienen todas las responsabilidades relacionadas con el plan y sus cumplimientos (si es que ya hay registrados)
          $responsabilities = DB::select("SELECT responsabilidades_compromisos.id, responsabilidades_compromisos.description, responsabilidades_compromisos.frecuencia, responsabilidades_compromisos.frecuencia_captura, responsabilidades_compromisos.meta, responsabilidades_compromisos.unidad, responsabilidades_compromisos.valor_rojo, responsabilidades_compromisos.valor_verde, responsabilidades_compromisos.id_objetivo_corporativo, cumplimientos_compromisos.id AS id_cumplimiento, cumplimientos_compromisos.logro, cumplimientos_compromisos.periodo, cumplimientos_compromisos.revisado FROM responsabilidades_compromisos INNER JOIN objetivos_corporativos ON (objetivos_corporativos.id = responsabilidades_compromisos.id_objetivo_corporativo) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) LEFT JOIN cumplimientos_compromisos ON (responsabilidades_compromisos.id = cumplimientos_compromisos.id_responsabilidad) WHERE responsabilidades_compromisos.id_plan = ? ORDER BY tipos_objetivos_corporativos.id, responsabilidades_compromisos.id_objetivo_corporativo, responsabilidades_compromisos.id", [$id_plan]);
        }
      }

      $objetivos_corporativos = DB::select("SELECT objetivos_corporativos.id, id_tipo, letter FROM objetivos_corporativos INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) ORDER BY objetivos_corporativos.id_tipo, objetivos_corporativos.id");
      $notas = DB::select("SELECT mensaje, notas.created_at, nombre, paterno, materno FROM notas INNER JOIN employees ON (employees.id = notas.id_user) WHERE id_plan = ? ORDER BY notas.created_at DESC", [$id_plan]);
    }

    return view('evaluacion-resultados/cumplimiento-de-compromisos', compact('planes', 'id_plan', 'start_month', 'year', 'responsabilities', 'notas', 'objetivos_corporativos', 'canEdit'));
  }
	
  /**
  * Muestra todas las iniciativas
  *
  * @return void
  */
  public function iniciativas(){

    // Id del usuario logueado
    $id_user = auth()->user()->employee_id;

    $id_plan = 0;
    $iniciatives = $users = $users_groups = $responsabilities = array();
    $objetivos_corporativos = $notas = array();
    $canEdit = true;

    // Obtiene todos los planes abiertos
    //$planes = DB::select("SELECT * FROM planes WHERE status = ? ORDER BY id DESC", ['Abierto']);
    $planes = DB::table('planes')->where('estado', 2)->orderBy('id', 'DESC')->get();

    // Se requiere ver un plan en particular
    if (!empty($_POST['id_plan']) || Session::has('id_plan')){

      if (!empty($_POST['id_plan'])){

        $id_plan = $_POST['id_plan'];
        Session::put('id_plan', $id_plan);
      }

      else{

        $id_plan = Session::get('id_plan');
        $found = false;

        foreach ($planes as $key => $value){
          
          if ($value->id == $id_plan){

            $found = true;
            break;
          }
        }

        if (!$found){

          // Existen planes abiertos
          if (!empty($planes) && !$planes->isEmpty()){

            // Se elige el id del plan mas reciente
            $id_plan = $planes[0]->id;
          }

          else{

            $id_plan = 0;
          }

          Session::forget('id_plan');   
        }
      }
    }

    // No se requiere ver un plan en particular
    else{

      // Existen planes abiertos
      if (!empty($planes) && !$planes->isEmpty()){

        // Se elige el id del plan mas reciente
        $id_plan = $planes[0]->id;
      }
    }

    // Existe un plan abierto
    if ($id_plan != 0){

      $users_groups = DB::select("SELECT id AS data, nombre as value FROM grupos_usuarios");
      $users = DB::select("SELECT id AS data, CONCAT(nombre, ' ', paterno, ' ', materno) AS value FROM employees");
      $notas = DB::select("SELECT id_user, mensaje, notas.created_at, nombre, paterno, materno FROM notas INNER JOIN employees ON (employees.id = notas.id_user) WHERE id_plan = ? ORDER BY notas.created_at DESC", [$id_plan]);
      $objetivos_corporativos = DB::table('objetivos_corporativos')->join('tipos_objetivos_corporativos', 'objetivos_corporativos.id_tipo', '=', 'tipos_objetivos_corporativos.id')->select('objetivos_corporativos.id', 'id_tipo', 'tipos_objetivos_corporativos.letter')->orderBy('tipos_objetivos_corporativos.id', 'ASC')->orderBy('objetivos_corporativos.id', 'ASC')->get();

      if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

        $iniciatives = DB::select("SELECT iniciativas.id, iniciativas.description, id_responsabilidad, fecha_inicio, fecha_termino, estado, id_capturista, formula, objetivos_corporativos.id AS id_objetivo_corporativo FROM iniciativas INNER JOIN responsabilidades_compromisos ON (responsabilidades_compromisos.id = iniciativas.id_responsabilidad) INNER JOIN objetivos_corporativos ON (responsabilidades_compromisos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE responsabilidades_compromisos.id_plan = ? ORDER BY responsabilidades_compromisos.id_objetivo_corporativo, responsabilidades_compromisos.tipo_indicador", [$id_plan]);
        //$responsabilities = DB::table('responsabilidades_compromisos')->join('objetivos_corporativos', 'responsabilidades_compromisos.id_objetivo_corporativo', '=', 'objetivos_corporativos.id')->join('tipos_objetivos_corporativos', 'objetivos_corporativos.id_tipo', '=', 'tipos_objetivos_corporativos.id')->where('id_plan', $id_plan)->select('responsabilidades_compromisos.id', 'responsabilidades_compromisos.description', 'id_objetivo_corporativo')->orderBy('responsabilidades_compromisos.id')->get();
        $responsabilities = DB::select("SELECT responsabilidades_compromisos.id, responsabilidades_compromisos.description, formula, responsabilidades_compromisos.id_objetivo_corporativo FROM responsabilidades_compromisos INNER JOIN objetivos_corporativos ON (objetivos_corporativos.id = responsabilidades_compromisos.id_objetivo_corporativo) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE responsabilidades_compromisos.id_plan = ? ORDER BY responsabilidades_compromisos.id", [$id_plan]);
      }

      else{

        $canEdit = false;

        if (!empty($id_user)){

          $iniciatives = DB::select("SELECT iniciativas.id, iniciativas.description, fecha_inicio, fecha_termino, estado, responsabilidades_compromisos.description AS descripcion_responsabilidad, id_capturista, formula, objetivos_corporativos.id AS id_objetivo_corporativo FROM iniciativas INNER JOIN responsabilidades_compromisos ON (responsabilidades_compromisos.id = iniciativas.id_responsabilidad) INNER JOIN objetivos_corporativos ON (responsabilidades_compromisos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE responsabilidades_compromisos.id_plan = ? AND id_capturista = ? ORDER BY responsabilidades_compromisos.id_objetivo_corporativo, responsabilidades_compromisos.tipo_indicador", [$id_plan, $id_user]);
        }
      }

      if (empty($iniciatives)){

        $ids_iniciatives = array();
        $iniciativas = DB::select("SELECT iniciativas.id FROM iniciativas INNER JOIN responsabilidades_compromisos ON (responsabilidades_compromisos.id = iniciativas.id_responsabilidad) INNER JOIN objetivos_corporativos ON (responsabilidades_compromisos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE responsabilidades_compromisos.id_plan = ?", [$id_plan]);

        foreach ($iniciativas as $key => $iniciative){
          
          $responsables = DB::select("SELECT id_responsable, tipo FROM iniciativas_responsables WHERE id_iniciativa = ?", [$iniciative->id]);

          foreach ($responsables as $key => $responsable){
            
            if ($responsable->tipo == 'persona'){

              if ($responsable->id_responsable == $id_user){

                $ids_iniciatives[] = $iniciative->id;
                break;
              }
            }

            else{

              $usuarios = DB::table('elementos_grupo')->where('id_grupo', $responsable->id_responsable)->where('tipo_elemento', 'Persona')->pluck('id_elemento');
              $usuarios = (array) $usuarios;

              if (in_array($id_user, $usuarios)){

                $ids_iniciatives[] = $iniciative->id;
                break;
              }
            }
          }
        }

        if (!empty($ids_iniciatives)){

          $ids_iniciatives = implode(',', $ids_iniciatives);
          $iniciatives = DB::select("SELECT iniciativas.id, iniciativas.description, fecha_inicio, fecha_termino, estado, responsabilidades_compromisos.description AS descripcion_responsabilidad, id_capturista, responsabilidades_compromisos.id_objetivo_corporativo, formula FROM iniciativas INNER JOIN responsabilidades_compromisos ON (responsabilidades_compromisos.id = iniciativas.id_responsabilidad) WHERE iniciativas.id IN ($ids_iniciatives) ORDER BY responsabilidades_compromisos.id_objetivo_corporativo, responsabilidades_compromisos.tipo_indicador");
        }
      }

      $temp_iniciatives = array();

      foreach ($iniciatives as $key => $value){

        $responsables = array();
        $grupos_responsables = array();
        $results = DB::select("SELECT id_responsable, tipo FROM iniciativas_responsables WHERE id_iniciativa = ?", [$value->id]);

        foreach ($results as $key2 => $value2){
            
          // El tipo de responsables es individual (no es grupo)
          if ($value2->tipo == 'persona'){

            // Se agrega a la lista de responsables
            $responsables[] = $value2->id_responsable;
          }

          // El tipo de responsables es un grupo (varias personas)
          else{

            // Se agrega a la lista de responsables de tipo grupo
            $grupos_responsables[] = $value2->id_responsable;
          }
        }

        $value->responsables = $responsables;
        $value->grupos_responsables = $grupos_responsables;
        $temp_iniciatives[] = $value;
      }

      $iniciatives = $temp_iniciatives;
    }

    return view('/evaluacion-resultados/iniciativas', compact('users', 'iniciatives', 'responsabilities', 'users_groups', 'planes', 'id_plan', 'canEdit', 'notas', 'objetivos_corporativos'));
  }
	
  /**
  * Reporte semaforizado
  *
  * @return void
  */
  public function reporte_semaforizado($id_plan = 0){
        
    if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

      Flash::error('No cuenta con permiso para ver la sección');
      return redirect('/que-es-evaluacion-resultados');
    }

    $responsabilities = array();

    // Obtiene todos los planes abiertos o cerrados
    //$planes = DB::select("SELECT * FROM planes WHERE estado = ? OR estado = ? ORDER BY id DESC", [2, 3]);
    $planes = DB::table('planes')->where('estado', 2)->orWhere('estado', 3)->orderBy('id', 'DESC')->get();

    // Se requiere ver un plan en particular
    if (!empty($id_plan) || Session::has('id_plan')){

      if (!empty($id_plan)){

        //$id_plan = $_POST['id_plan'];
        Session::put('id_plan', $id_plan);
      }

      else{

        $id_plan = Session::get('id_plan');
        $found = false;
        
        foreach ($planes as $key => $value) {
          
          if ($value->id == $id_plan){

            $found = true;
            break;
          }
        }

        if (!$found){

          if (!empty($planes) && !$planes->isEmpty()){

            $id_plan = $planes[0]->id;
          }

          else{

            $id_plan = 0;
          }

          Session::forget('id_plan');
        }
      }
    }

    // No se requiere ver un plan en particular
    else{

      // Existen planes aviertos
      if (!empty($planes) && !$planes->isEmpty()){

        // Se elige el id del plan mas reciente
        $id_plan = $planes[0]->id;
      }
    }

    // Existe un plan abierto
    if ($id_plan != 0){

      // Se obtienen todas las responsabilidades relacionadas con el plan
      //$responsabilities = DB::select("SELECT responsabilidades_compromisos.id, description, meta, unidad, valor_verde, valor_rojo, id_objetivo_corporativo, objetivos_corporativos.name, id_tipo, tipos_objetivos_corporativos.name AS tipo_objetivo_corporativo, logro FROM responsabilidades_compromisos INNER JOIN objetivos_corporativos ON (objetivos_corporativos.id = responsabilidades_compromisos.id_objetivo_corporativo) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) INNER JOIN cumplimientos_compromisos ON (responsabilidades_compromisos.id = cumplimientos_compromisos.id_responsabilidad) WHERE id_plan = ? AND tipo_indicador = ? AND revisado = ? ORDER BY id_tipo, id_objetivo_corporativo, responsabilidades_compromisos.id, periodo DESC", [$id_plan, 'De Resultado', 1]);
      $responsabilities = DB::table('responsabilidades_compromisos')->join('objetivos_corporativos', 'id_objetivo_corporativo', '=', 'objetivos_corporativos.id')->join('tipos_objetivos_corporativos', 'id_tipo', '=', 'tipos_objetivos_corporativos.id')->join('cumplimientos_compromisos', 'responsabilidades_compromisos.id', '=', 'id_responsabilidad')->where('responsabilidades_compromisos.id_plan', $id_plan)->where('tipo_indicador', 'De Resultado')->where('revisado', 1)->select('responsabilidades_compromisos.id', 'responsabilidades_compromisos.description', 'meta', 'unidad', 'valor_rojo', 'valor_verde', 'id_objetivo_corporativo', 'objetivos_corporativos.name', 'id_tipo', 'tipos_objetivos_corporativos.name AS tipo_objetivo_corporativo', 'logro')->orderBy('id_tipo')->orderBy('id_objetivo_corporativo')->orderBy('responsabilidades_compromisos.id')->orderBy('periodo', 'DESC')->get();
    }

    return view('evaluacion-resultados/reportes/reporte-semaforizado', compact('planes', 'id_plan', 'responsabilities'));
  }

  /**
  * Reporte de un objetivo corporativo
  *
  * @return void
  */
  public function reporte_objetivo_corporativo($id_plan, $id_objetivo_corporativo){
        
    if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

      Flash::error('No cuenta con permiso para ver la sección');
      return redirect('/evaluacion-resultados/que-es-evaluacion-resultados');
    }

    //$id_plan = 0;
    $results_responsabilities = $tendence_responsabilities = $iniciatives = $objetivos_corporativos = array();
    $iniciatives = $users = $users_groups = array();
    //$id_objetivo_estrategico = $_POST['id_objetivo_estrategico'];

    // Obtiene todos los planes abiertos o cerrados
    //$planes = DB::select("SELECT * FROM planes WHERE status = ? OR status = ? ORDER BY id DESC", ['Abierto', 'Cerrado']);
    $planes = DB::table('planes')->where('estado', 2)->orWhere('estado', 3)->orderBy('id', 'DESC')->get();

    // Se requiere ver un plan en particular
    if (!empty($id_plan) || Session::has('id_plan')){

      if (!empty($id_plan)){

        //$id_plan = $_POST['id_plan'];
        Session::put('id_plan', $id_plan);
      }

      else{

        $id_plan = Session::get('id_plan');
        $found = false;
        
        foreach ($planes as $key => $value) {
          
          if ($value->id == $id_plan){

            $found = true;
            break;
          }
        }

        if (!$found){

          if (!empty($planes) && !$planes->isEmpty()){

            $id_plan = $planes[0]->id;
          }

          else{

            $id_plan = 0;
          }

          Session::forget('id_plan');
        }
      }
    }

    // No se requiere ver un plan en particular
    else{

      // Existen planes aviertos
      if (!empty($planes) && !$planes->isEmpty()){

        // Se elige el id del plan mas reciente
        $id_plan = $planes[0]->id;
      }
    }

    // Existe un plan abierto
    if ($id_plan != 0){

      //$users_groups = DB::select("SELECT id, name FROM users_groups");
      $users_groups = DB::table('grupos_usuarios')->select('id', 'nombre');
      //$users = DB::select("SELECT id, first_name, last_name FROM users WHERE id > ? ORDER BY first_name", [7]);
      $users = DB::table('employees')->select('id', 'nombre', 'paterno', 'materno')->get();

      // Se obtienen todas las responsabilidades relacionadas con el plan
      //$objetivos_estrategicos = DB::select("SELECT id_objetivo_estrategico, name, id_tipo FROM responsabilidades_compromisos INNER JOIN objetivos_estrategicos ON (objetivos_estrategicos.id = responsabilidades_compromisos.id_objetivo_estrategico) INNER JOIN cumplimientos_compromisos ON (responsabilidades_compromisos.id = cumplimientos_compromisos.id_responsabilidad) WHERE id_plan = ? AND revisado = ? GROUP BY id_objetivo_estrategico ORDER BY id_objetivo_estrategico", [$id_plan, 1]);
      $objetivos_corporativos = DB::table('responsabilidades_compromisos')->join('objetivos_corporativos', 'objetivos_corporativos.id', '=', 'id_objetivo_corporativo')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'id_tipo')->join('cumplimientos_compromisos', 'responsabilidades_compromisos.id', '=', 'id_responsabilidad')->where('responsabilidades_compromisos.id_plan', $id_plan)->where('revisado', 1)->select('id_objetivo_corporativo', 'objetivos_corporativos.name', 'id_tipo')->groupBy('id_objetivo_corporativo')->orderBy('id_objetivo_corporativo')->get();

      // Se obtienen todas las responsabilidades de resultados relacionadas con el plan y el objetivo estratégico
      //$results_responsabilities = DB::select("SELECT responsabilidades_compromisos.id, id_responsable, description, meta, unidad, valor_rojo, valor_verde, logro FROM responsabilidades_compromisos INNER JOIN objetivos_estrategicos ON (responsabilidades_compromisos.id_objetivo_estrategico = objetivos_estrategicos.id) INNER JOIN cumplimientos_compromisos ON (responsabilidades_compromisos.id = cumplimientos_compromisos.id_responsabilidad) WHERE id_plan = ? AND id_objetivo_estrategico = ? AND tipo_indicador = ? AND revisado = ? ORDER BY responsabilidades_compromisos.id, periodo DESC", [$id_plan, $id_objetivo_estrategico, 'De Resultado', 1]);
      $results_responsabilities = DB::table('responsabilidades_compromisos')->join('objetivos_corporativos', 'id_objetivo_corporativo', '=', 'objetivos_corporativos.id')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'id_tipo')->join('cumplimientos_compromisos', 'id_responsabilidad', 'responsabilidades_compromisos.id')->where('responsabilidades_compromisos.id_plan', $id_plan)->where('id_objetivo_corporativo', $id_objetivo_corporativo)->where('tipo_indicador', 'De Resultado')->where('revisado', 1)->select('responsabilidades_compromisos.id', 'responsabilidades_compromisos.id_responsable', 'responsabilidades_compromisos.description', 'unidad', 'meta', 'valor_verde', 'valor_rojo', 'tipos_objetivos_corporativos.name', 'logro')->orderBy('responsabilidades_compromisos.id')->orderBy('periodo', 'DESC')->get();

      // Se obtienen todas las responsabilidades de tendencia relacionadas con el plan y el objetivo estratégico
      //$tendence_responsabilities = DB::select("SELECT responsabilidades_compromisos.id, id_responsable, description, meta, unidad, valor_rojo, valor_verde, logro FROM responsabilidades_compromisos INNER JOIN objetivos_estrategicos ON (objetivos_estrategicos.id = responsabilidades_compromisos.id_objetivo_estrategico) INNER JOIN cumplimientos_compromisos ON (responsabilidades_compromisos.id = cumplimientos_compromisos.id_responsabilidad) WHERE id_plan = ? AND id_objetivo_estrategico = ? AND tipo_indicador = ? AND revisado = ? ORDER BY responsabilidades_compromisos.id, periodo DESC", [$id_plan, $id_objetivo_estrategico, 'De Tendencia', 1]);
      $tendence_responsabilities = DB::table('responsabilidades_compromisos')->join('objetivos_corporativos', 'id_objetivo_corporativo', '=', 'objetivos_corporativos.id')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'id_tipo')->join('cumplimientos_compromisos', 'id_responsabilidad', 'responsabilidades_compromisos.id')->where('responsabilidades_compromisos.id_plan', $id_plan)->where('id_objetivo_corporativo', $id_objetivo_corporativo)->where('tipo_indicador', 'De Tendencia')->where('revisado', 1)->select('responsabilidades_compromisos.id', 'responsabilidades_compromisos.id_responsable', 'responsabilidades_compromisos.description', 'unidad', 'meta', 'valor_verde', 'valor_rojo', 'logro')->orderBy('responsabilidades_compromisos.id')->orderBy('periodo', 'DESC')->get();

      // Se obtienen las iniciativas relacionadas con el plan y el objetivo estratégico
      //$iniciatives = DB::select("SELECT iniciativas.id, iniciativas.description, fecha_inicio, fecha_termino, estado FROM iniciativas INNER JOIN responsabilidades_compromisos ON (responsabilidades_compromisos.id = iniciativas.id_responsabilidad) INNER JOIN objetivos_estrategicos ON (objetivos_estrategicos.id = responsabilidades_compromisos.id_objetivo_estrategico) WHERE id_plan = ? AND id_objetivo_estrategico = ?", [$id_plan, $id_objetivo_estrategico]);
      $iniciatives = DB::table('iniciativas')->join('responsabilidades_compromisos', 'id_responsabilidad', '=', 'responsabilidades_compromisos.id')->join('objetivos_corporativos', 'objetivos_corporativos.id', '=', 'id_objetivo_corporativo')->join('tipos_objetivos_corporativos', 'id_tipo', 'tipos_objetivos_corporativos.id')->where('responsabilidades_compromisos.id_plan', $id_plan)->where('id_objetivo_corporativo', $id_objetivo_corporativo)->select('iniciativas.id', 'iniciativas.description', 'fecha_inicio', 'fecha_termino', 'estado')->get();

      $temp_iniciatives = array();

      foreach ($iniciatives as $key => $value){

        $responsables = array();
        $grupos_responsables = array();
        //$results = DB::select("SELECT id_responsable, tipo FROM iniciativas_responsables WHERE id_iniciativa = ?", [$value->id]);
        $results = DB::table('iniciativas_responsables')->where('id_iniciativa', $value->id)->select('id_responsable', 'tipo')->get();

        foreach ($results as $key2 => $value2){
            
          // El tipo de responsables es individual (no es grupo)
          if ($value2->tipo == 'persona'){

            // Se agrega a la lista de responsables
            $responsables[] = $value2->id_responsable;
          }

          // El tipo de responsables es un grupo (varias personas)
          else{

            // Se agrega a la lista de responsables de tipo grupo
            $grupos_responsables[] = $value2->id_responsable;
          }
        }

        $value->responsables = $responsables;
        $value->grupos_responsables = $grupos_responsables;
        $temp_iniciatives[] = $value;
      }

      $iniciatives = $temp_iniciatives;
    }

    return view('evaluacion-resultados/reportes/reporte-objetivo-corporativo', compact('planes', 'id_plan', 'id_objetivo_corporativo', 'results_responsabilities', 'tendence_responsabilities', 'iniciatives', 'users', 'users_groups', 'objetivos_corporativos'));
  }

  /**
  * Reporte de una responsabilidad
  *
  * @return void
  */
  public function reporte_objetivo_responsabilidad($id_plan, $id_responsabilidad){
        
    if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

      Flash::error('No cuenta con permiso para ver la sección');
      return redirect('/evaluacion-resultados/que-es-evaluacion-resultados');
    }

    $responsability = $responsabilities = array();
    $year = 0;
    $start_month = 0;

    // Obtiene todos los planes abiertos o cerrados
    //$planes = DB::select("SELECT * FROM planes WHERE status = ? OR status = ? ORDER BY id DESC", ['Abierto', 'Cerrado']);
    $planes = DB::table('planes')->where('estado', 2)->orWhere('estado', 3)->orderBy('id', 'DESC')->get();

    // Se requiere ver un plan en particular
    if (!empty($id_plan) || Session::has('id_plan')){

      if (!empty($id_plan)){

        //$id_plan = $_POST['id_plan'];
        Session::put('id_plan', $id_plan);
      }

      else{

        $id_plan = Session::get('id_plan');
        $found = false;
        
        foreach ($planes as $key => $value) {
          
          if ($value->id == $id_plan){

            $found = true;
            break;
          }
        }

        if (!$found){

          if (!empty($planes) && !$planes->isEmpty()){

            $id_plan = $planes[0]->id;
          }

          else{

            $id_plan = 0;
          }

          Session::forget('id_plan');
        }
      }
    }

    // No se requiere ver un plan en particular
    else{

      // Existen planes aviertos
      if (!empty($planes) && !$planes->isEmpty()){

        // Se elige el id del plan mas reciente
        $id_plan = $planes[0]->id;
      }
    }

    // Existe un plan abierto
    if ($id_plan != 0){

      foreach ($planes as $key => $plan){
        
        if ($plan->id == $id_plan){

          $year = $plan->anio;
          $start_month = $plan->periodo;
          break;
        }
      }

      // Se obtienen todas las responsabilidades relacionadas con el plan y que tengan cumplimientos registrados
      //$responsabilities = DB::select("SELECT responsabilidades_compromisos.id, description FROM responsabilidades_compromisos INNER JOIN objetivos_estrategicos ON (responsabilidades_compromisos.id_objetivo_estrategico = objetivos_estrategicos.id) INNER JOIN cumplimientos_compromisos ON (responsabilidades_compromisos.id = cumplimientos_compromisos.id_responsabilidad) WHERE id_plan = ? AND revisado = ? GROUP BY responsabilidades_compromisos.id ORDER BY responsabilidades_compromisos.id", [$id_plan, 1]);
      $responsabilities = DB::table('responsabilidades_compromisos')->join('objetivos_corporativos', 'id_objetivo_corporativo', '=', 'objetivos_corporativos.id')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'id_tipo')->join('cumplimientos_compromisos', 'id_responsabilidad', 'responsabilidades_compromisos.id')->where('responsabilidades_compromisos.id_plan', $id_plan)->where('revisado', 1)->select('responsabilidades_compromisos.id', 'responsabilidades_compromisos.description')->groupBy('responsabilidades_compromisos.id')->orderBy('responsabilidades_compromisos.id')->get();

      // Se obtienen los datos de la responsabilidad
      //$responsability = DB::select("SELECT description, id_objetivo_estrategico, meta, unidad, frecuencia_captura, valor_rojo, valor_verde, logro, periodo FROM responsabilidades_compromisos INNER JOIN objetivos_estrategicos ON (objetivos_estrategicos.id = responsabilidades_compromisos.id_objetivo_estrategico) INNER JOIN cumplimientos_compromisos ON (responsabilidades_compromisos.id = cumplimientos_compromisos.id_responsabilidad) WHERE id_plan = ? AND responsabilidades_compromisos.id = ? AND revisado = ? ORDER BY periodo", [$id_plan, $id_responsabilidad, 1]);
      $responsability = DB::table('responsabilidades_compromisos')->join('objetivos_corporativos', 'id_objetivo_corporativo', '=', 'objetivos_corporativos.id')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'id_tipo')->join('cumplimientos_compromisos', 'id_responsabilidad', 'responsabilidades_compromisos.id')->where('responsabilidades_compromisos.id_plan', $id_plan)->where('responsabilidades_compromisos.id', $id_responsabilidad)->where('revisado', 1)->select('frecuencia_captura', 'periodo', 'responsabilidades_compromisos.description', 'id_objetivo_corporativo', 'unidad', 'meta', 'valor_verde', 'valor_rojo', 'logro')->orderBy('periodo')->get();

      if (!$responsability->isEmpty()){

        // Se obtienen los datos de las responsabilidades de resultados para cierto objetivo corporativo
        //$results_responsabilities = DB::select("SELECT responsabilidades_compromisos.id, description, meta, unidad, valor_rojo, valor_verde, logro, name, id_tipo FROM responsabilidades_compromisos INNER JOIN objetivos_estrategicos ON (responsabilidades_compromisos.id_objetivo_estrategico = objetivos_estrategicos.id) INNER JOIN cumplimientos_compromisos ON (responsabilidades_compromisos.id = cumplimientos_compromisos.id_responsabilidad) WHERE id_objetivo_estrategico = ? AND tipo_indicador = ? AND revisado = ? ORDER BY responsabilidades_compromisos.id, periodo DESC", [$responsability[0]->id_objetivo_estrategico, 'De Resultado', 1]);
        $results_responsabilities = DB::table('responsabilidades_compromisos')->join('objetivos_corporativos', 'id_objetivo_corporativo', '=', 'objetivos_corporativos.id')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'id_tipo')->join('cumplimientos_compromisos', 'id_responsabilidad', 'responsabilidades_compromisos.id')->where('id_objetivo_corporativo', $responsability[0]->id_objetivo_corporativo)->where('tipo_indicador', 'De Resultado')->where('revisado', 1)->select('responsabilidades_compromisos.id', 'responsabilidades_compromisos.description', 'unidad', 'meta', 'valor_verde', 'valor_rojo', 'objetivos_corporativos.name', 'tipos_objetivos_corporativos.name AS tipo_objetivo_corporativo', 'logro')->orderBy('responsabilidades_compromisos.id')->orderBy('periodo', 'DESC')->get();
      }
    }

    return view('evaluacion-resultados/reportes/reporte-objetivo-responsabilidad', compact('planes', 'id_plan', 'year', 'start_month', 'id_responsabilidad', 'responsabilities', 'responsability', 'results_responsabilities'));
  }

  /**
  * Guarda los tipos de objetivos corporativos
  *
  * @return void
  */
  public function save_tipos_objetivos_corporativos(){
      
    foreach ($_POST['name'] as $key => $value){
        
      $tipo_objetivo_corporativo = array();
      $tipo_objetivo_corporativo['name'] = $value;
      $tipo_objetivo_corporativo['description'] = $_POST['description'][$key];
      $tipo_objetivo_corporativo['letter'] = $_POST['letter'][$key];
      $tipo_objetivo_corporativo['color'] = $_POST['color'][$key];
      $tipo_objetivo_corporativo['id_responsable'] = (!empty($_POST['id_responsable'][$key]) ? $_POST['id_responsable'][$key] : 0);
      $id = 0;

      if (!empty($_POST['id_tipo_objetivo_corporativo'][$key])){

        $id = $_POST['id_tipo_objetivo_corporativo'][$key];
        $tipo_objetivo_corporativo['updated_at'] = date('Y-m-d H:i:s');
        DB::table('tipos_objetivos_corporativos')->where('id', $id)->update($tipo_objetivo_corporativo);
      }

      else{

        $tipo_objetivo_corporativo['id_plan'] = $_POST['id_plan'];
        $tipo_objetivo_corporativo['unique_id'] = uniqid(rand());
        DB::table('tipos_objetivos_corporativos')->insert($tipo_objetivo_corporativo);
      }
    }

    Flash::success('Se guardaron los tipos de objetivos corporativos');
    return redirect('/evaluacion-resultados/tipos-objetivos-corporativos');
  }

  /**
  * Guarda los objetivos corporativos
  *
  * @return void
  */
  public function save_objetivos_corporativos(){
        
    $id_tipo_objetivo_corporativo = $_POST['id_tipo_objetivo_corporativo'];
      
    foreach ($_POST['names'] as $key => $value){
        
      $objetivo_corporativo = array();
      $objetivo_corporativo['name'] = $value;
      $objetivo_corporativo['id_tipo'] = $id_tipo_objetivo_corporativo;
      $id = 0;

      if (!empty($_POST['ids_objetivos'][$key])){

        $id = $_POST['ids_objetivos'][$key];
        $objetivo_corporativo['updated_at'] = date('Y-m-d H:i:s');
        DB::table('objetivos_corporativos')->where('id', $id)->update($objetivo_corporativo);
      }

      else{

        $objetivo_corporativo['unique_id'] = uniqid(rand());
        DB::table('objetivos_corporativos')->insert($objetivo_corporativo);
      }
    }

    Flash::success('Se guardaron los objetivos corporativos');
    return redirect('/evaluacion-resultados/objetivos-corporativos');
  }
	
  /**
     * Guarda las responsabilidades y compromisos junto con sus posibles iniciativas
     *
     * @return void
     */
    public function save_responsabilities(){

      if (!empty($_POST['id_objetivo_corporativo'])){

        $id_user = auth()->user()->employee_id;
        $id_responsability = 0;

        foreach ($_POST['id_objetivo_corporativo'] as $key => $value){
          
          $responsability = array();
          $responsability['id_objetivo_corporativo'] = $value;
          $responsability['id_responsable'] = $_POST['id_responsable'][$key];
          $responsability['id_capturista'] = $_POST['id_capturista'][$key];
          $responsability['description'] = $_POST['description'][$key];
          $responsability['tipo_indicador'] = $_POST['tipo_indicador'][$key];
          $responsability['formula'] = $_POST['formula'][$key];
          $responsability['frecuencia'] = $_POST['frecuencia'][$key];
          $responsability['frecuencia_captura'] = $_POST['frecuencia_captura'][$key];
          $responsability['fuente'] = $_POST['fuente'][$key];
          $responsability['id_status'] = $_POST['id_status'][$key];
          $responsability['meta'] = $_POST['meta'][$key];
          $responsability['unidad'] = $_POST['unidad'][$key];
          $responsability['valor_rojo'] = $_POST['valor_rojo'][$key];
          $responsability['valor_verde'] = $_POST['valor_verde'][$key];

          if (!empty($_POST['id_responsabilidad'][$key])){

            $id_responsability = $_POST['id_responsabilidad'][$key];
            DB::table('responsabilidades_compromisos')->where('id', $id_responsability)->update($responsability);
            DB::delete("DELETE FROM responsabilidades_corresponsables WHERE id_responsabilidad = ?", [$id_responsability]);
          }

          else{

            $responsability['id_plan'] = $_POST['id_plan'];
            $responsability['created_by'] = $id_user;
            $responsability['unique_id'] = uniqid(rand());
            DB::table('responsabilidades_compromisos')->insert($responsability);
            $results = DB::select("SELECT id FROM responsabilidades_compromisos ORDER BY id DESC LIMIT 1");
            $id_responsability = $results[0]->id;
          }

          if (!empty($_POST['corresponsables'][$key])){

            foreach ($_POST['corresponsables'][$key] as $key2 => $value2){
              
              $corresponsable = array();
              $corresponsable['id_responsabilidad'] = $id_responsability;
              $corresponsable['id_corresponsable'] = $value2;
              $corresponsable['tipo'] = 'persona';
              DB::table('responsabilidades_corresponsables')->insert($corresponsable);
            }
          }

          if (!empty($_POST['grupos_corresponsables'][$key])){

            foreach ($_POST['grupos_corresponsables'][$key] as $key2 => $value2){
              
              $corresponsable = array();
              $corresponsable['id_responsabilidad'] = $id_responsability;
              $corresponsable['id_corresponsable'] = $value2;
              $corresponsable['tipo'] = 'grupo';
              DB::table('responsabilidades_corresponsables')->insert($corresponsable);
            }
          }
        }
      }

      Flash::success('Se guardaron las responsabilidades y compromisos');
      return redirect('/evaluacion-resultados/responsabilidades-y-compromisos');
    }

  /**
  * Copia objetivos corporativos de un plan a otro
  *
  * @return void
  */
  public function copiar_objetivos_corporativos(){

    $plan_para_copiar = $_POST['plan_para_copiar'];
    $plan_a_copiar = $_POST['plan_a_copiar'];
    $objetivos_corporativos = $old_objetivos_corporativos = array();
    $id_user = auth()->user()->id;
    $old_objetivos_corporativos = DB::table('objetivos_corporativos')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'objetivos_corporativos.id_tipo')->where('id_plan', $plan_a_copiar)->select('objetivos_corporativos.id')->get();

    // Obtiene los objetivos corporativos
    $objetivos_corporativos = DB::table('objetivos_corporativos')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'objetivos_corporativos.id_tipo')->where('id_plan', $plan_para_copiar)->select('objetivos_corporativos.id', 'id_tipo', 'objetivos_corporativos.name', 'objetivos_corporativos.unique_id')->get();

    foreach ($old_objetivos_corporativos as $key => $objetivo_corporativo){
      
      $responsabilities = DB::table('responsabilidades_compromisos')->where('id_objetivo_corporativo', $objetivo_corporativo->id)->select('id', 'id_capturista')->get();

      foreach ($responsabilities as $key => $value){
        
        DB::table('responsabilidades_corresponsables')->where('id_responsabilidad', $value->id)->delete();
        DB::table('valores_responsabilidad')->where('id_responsabilidad', $value->id)->delete();
        DB::table('cumplimientos_compromisos')->where('id_responsabilidad', $value->id)->delete();
        DB::table('status_plan')->where('id_capturista', $value->id_capturista)->where('id_plan', $plan_a_copiar)->delete();

        $iniciativas = DB::table('iniciativas')->where('id_responsabilidad', $value->id)->select('id')->get();

        foreach ($iniciativas as $key => $iniciativa){
        
          DB::table('iniciativas_responsables')->where('id_iniciativa', $iniciativa->id)->delete();
          DB::table('iniciativas')->where('id', $iniciativa->id)->delete();
        }

        DB::table('responsabilidades_compromisos')->where('id', $value->id)->delete();
      }

      DB::table('objetivos_corporativos')->where('id', $objetivo_corporativo->id)->delete();
    }

    $new = array();
    $tipos_objetivos_corporativos = array();
    $new_tipos_objetivos_corporativos = array();
    $id_tipo_objetivo_corporativo = 0;

    foreach ($objetivos_corporativos as $key => $objetivo_corporativo){
        
      $new['name'] = $objetivo_corporativo->name;
      $new['unique_id'] = $objetivo_corporativo->unique_id;

      if (in_array($objetivo_corporativo->id_tipo, $tipos_objetivos_corporativos)){

        $id_tipo_objetivo_corporativo = $new_tipos_objetivos_corporativos[$objetivo_corporativo->id_tipo];
      }

      else{

        $tipos_objetivos_corporativos[] = $objetivo_corporativo->id_tipo;
        $tipo_objetivo_corporativo = DB::table('tipos_objetivos_corporativos')->where('id', $objetivo_corporativo->id_tipo)->select('name', 'description', 'letter', 'color', 'id_responsable', 'unique_id')->first();
        $new_tipo_objetivo_corporativo = DB::table('tipos_objetivos_corporativos')->where('unique_id', $tipo_objetivo_corporativo->unique_id)->where('id_plan', $plan_a_copiar)->select('id')->first();

        if (!empty($new_tipo_objetivo_corporativo)){

          $id_tipo_objetivo_corporativo = $new_tipo_objetivo_corporativo->id;
        }

        else{

          $new_tipo_objetivo_corporativo = array();
          $new_tipo_objetivo_corporativo['name'] = $tipo_objetivo_corporativo->name;
          $new_tipo_objetivo_corporativo['description'] = $tipo_objetivo_corporativo->description;
          $new_tipo_objetivo_corporativo['letter'] = $tipo_objetivo_corporativo->letter;
          $new_tipo_objetivo_corporativo['color'] = $tipo_objetivo_corporativo->color;
          $new_tipo_objetivo_corporativo['id_responsable'] = $tipo_objetivo_corporativo->id_responsable;
          $new_tipo_objetivo_corporativo['unique_id'] = $tipo_objetivo_corporativo->unique_id;
          $new_tipo_objetivo_corporativo['id_plan'] = $plan_a_copiar;
          DB::table('tipos_objetivos_corporativos')->insert($new_tipo_objetivo_corporativo);  
          $new_tipo_objetivo_corporativo = DB::table('tipos_objetivos_corporativos')->select('id')->orderBy('id', 'DESC')->first();
          $id_tipo_objetivo_corporativo = $new_tipo_objetivo_corporativo->id;
        }

        $new_tipos_objetivos_corporativos[$objetivo_corporativo->id_tipo] = $id_tipo_objetivo_corporativo;
      }

      $new['id_tipo'] = $id_tipo_objetivo_corporativo;
      DB::table('objetivos_corporativos')->insert($new);
    }

    Session::put('id_plan', $plan_a_copiar);
    Flash::success('Los objetivos corporativos fueron copiados correctamente');
    return redirect('/evaluacion-resultados/objetivos-corporativos');
  }

  /* Copia responsabilidades de un plan a otro
  *
  * @return void
  */
  public function copiar_responsabilidades(){

    $plan_para_copiar = $_POST['plan_para_copiar'];
    $plan_a_copiar = $_POST['plan_a_copiar'];
    $responsabilidades = $responsabilities = array();
    $id_user = auth()->user()->employee_id;
      
    if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

      $responsabilities = DB::table('responsabilidades_compromisos')->where('id_plan', $plan_a_copiar)->select('id')->get();

      // Obtiene las responsabilidades
      $responsabilidades = DB::table('responsabilidades_compromisos')->where('id_plan', $plan_para_copiar)->get();
    }

    else{

      $responsabilities = DB::table('responsabilidades_compromisos')->join('objetivos_corporativos', 'objetivos_corporativos.id', '=', 'responsabilidades_compromisos.id_objetivo_corporativo')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'objetivos_corporativos.id_tipo')->where('id_plan', $plan_a_copiar)->where('tipos_objetivos_corporativos.id_responsable', $id_user)->select('responsabilidades_compromisos.id')->get();

      // Obtiene las responsabilidades
      $responsabilidades = DB::table('responsabilidades_compromisos')->join('objetivos_corporativos', 'objetivos_corporativos.id', '=', 'responsabilidades_compromisos.id_objetivo_corporativo')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'objetivos_corporativos.id_tipo')->where('id_plan', $plan_para_copiar)->where('tipos_objetivos_corporativos.id_responsable', $id_user)->select('responsabilidades_compromisos.*')->get();
    }

    foreach ($responsabilities as $key => $value){
        
      DB::table('responsabilidades_corresponsables')->where('id_responsabilidad', $value->id)->delete();
      DB::table('valores_responsabilidad')->where('id_responsabilidad', $value->id)->delete();
      DB::table('responsabilidades_compromisos')->where('id', $value->id)->delete();
    }

    $new = array();
    $new['created_by'] = $id_user;
    $corresponsable = array();

    foreach ($responsabilidades as $key => $responsabilidad){
        
      $new['description'] = $responsabilidad->description;
      $new['id_plan'] = $plan_a_copiar;
      $new['id_objetivo_corporativo'] = $responsabilidad->id_objetivo_corporativo;
      $new['id_responsable'] = $responsabilidad->id_responsable;
      $new['id_capturista'] = $responsabilidad->id_capturista;
      $new['tipo_indicador'] = $responsabilidad->tipo_indicador;
      $new['formula'] = $responsabilidad->formula;
      $new['frecuencia'] = $responsabilidad->frecuencia;
      $new['frecuencia_captura'] = $responsabilidad->frecuencia_captura;
      $new['fuente'] = $responsabilidad->fuente;
      $new['id_status'] = $responsabilidad->id_status;
      $new['meta'] = $responsabilidad->meta;
      $new['unidad'] = $responsabilidad->unidad;
      $new['valor_rojo'] = $responsabilidad->valor_rojo;
      $new['valor_verde'] = $responsabilidad->valor_verde;
      $new['unique_id'] = $responsabilidad->unique_id;
      DB::table('responsabilidades_compromisos')->insert($new);
      $responsability = DB::table('responsabilidades_compromisos')->select('id')->orderBy('id', 'DESC')->first();
      $corresponsables = DB::table('responsabilidades_corresponsables')->where('id_responsabilidad', $responsabilidad->id)->get();

      foreach ($corresponsables as $key => $value){
        
        $corresponsable['id_responsabilidad'] = $responsability->id;
        $corresponsable['id_corresponsable'] = $value->id_corresponsable;
        $corresponsable['tipo'] = $value->tipo;
        DB::table('responsabilidades_corresponsables')->insert($corresponsable);
      }
    }

    Session::put('id_plan', $plan_a_copiar);
    Flash::success('Las responsabilidades fueron copiadas correctamente');
    return redirect('/evaluacion-resultados/responsabilidades-y-compromisos');
  }
	
	/**
     * Guarda un cumplimiento
     *
     * @return void
     */
    public function save_cumplimiento(){

      if (!empty($_POST['id_plan'])){

        $id_user = auth()->user()->employee_id;
        $cumplimiento = array();
        $cumplimiento['id_responsabilidad'] = $_POST['id_responsability'];
        $cumplimiento['periodo'] = $_POST['periodo'];
        $cumplimiento['logro'] = $_POST['cumplimiento'];
        DB::table('cumplimientos_compromisos')->insert($cumplimiento);
        $results = DB::select("SELECT status FROM status_plan WHERE id_plan = ? AND id_capturista = ? AND status = ? AND periodo = ?", [$_POST['id_plan'], $id_user, 3, $_POST['periodo']]);

        if (empty($results)){

          $status_plan = array();
		  $status_plan['id_plan'] = $_POST['id_plan'];
          $status_plan['id_capturista'] = $id_user;
          $status_plan['status'] = 3;
          $status_plan['periodo'] = $_POST['periodo'];
          DB::table('status_plan')->insert($status_plan);
        }
      }

      else{

        $cumplimiento = array();
        $cumplimiento['logro'] = $_POST['cumplimiento'];
        $cumplimiento['updated_at'] = date('Y-m-d H:i:s');
        DB::table('cumplimientos_compromisos')->where('id', $_POST['id'])->update($cumplimiento);
      } 
    }
	
	/**
     * Autorizar cumplimientos
     *
     * @return void
     */
    public function autorize_cumplimientos(){

      if (!empty($_POST['id_plan'])){

        $id_user = auth()->user()->employee_id;
        //$plan = DB::select("SELECT registrar_logros FROM planes WHERE id = ?", [$_POST['id_plan']]);
        $results = array();

        /*if ($plan[0]->registrar_logros == 'Responsable'){

          $results = DB::select("SELECT cumplimientos_compromisos.id FROM cumplimientos_compromisos INNER JOIN responsabilidades_compromisos ON (responsabilidades_compromisos.id = cumplimientos_compromisos.id_responsabilidad) INNER JOIN objetivos_estrategicos ON (objetivos_estrategicos.id = responsabilidades_compromisos.id_objetivo_estrategico) WHERE responsabilidades_compromisos.id_responsable = ? AND cumplimientos_compromisos.revisado = ? AND objetivos_estrategicos.id_plan = ?", [$id_user, 0, $_POST['id_plan']]);
        }

        else{

          if ($plan[0]->registrar_logros == 'Capturista'){*/

            $results = DB::select("SELECT cumplimientos_compromisos.id FROM cumplimientos_compromisos INNER JOIN responsabilidades_compromisos ON (responsabilidades_compromisos.id = cumplimientos_compromisos.id_responsabilidad) INNER JOIN objetivos_corporativos ON (objetivos_corporativos.id = responsabilidades_compromisos.id_objetivo_corporativo) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE responsabilidades_compromisos.id_capturista = ? AND cumplimientos_compromisos.revisado = ? AND responsabilidades_compromisos.id_plan = ?", [$id_user, 0, $_POST['id_plan']]);
          //}

          /*else{

            $results = DB::select("SELECT cumplimientos_compromisos.id FROM cumplimientos_compromisos INNER JOIN responsabilidades_compromisos ON (responsabilidades_compromisos.id = cumplimientos_compromisos.id_responsabilidad) INNER JOIN objetivos_estrategicos ON (objetivos_estrategicos.id = responsabilidades_compromisos.id_objetivo_estrategico) WHERE cumplimientos_compromisos.revisado = ? AND objetivos_estrategicos.id_plan = ?", [0, $_POST['id_plan']]);
          }
        }*/

        foreach ($results as $key => $result){
          
          DB::update("UPDATE cumplimientos_compromisos SET revisado = 1 WHERE id = ?", [$result->id]);
        }

        DB::update("UPDATE status_plan SET status = 5, fecha_revisado = '" . date('Y-m-d') . "' WHERE id_plan = ? AND id_capturista = ? AND status = ?", [$_POST['id_plan'], $id_user, 3]);
        $nota = array();
        $nota['id_user'] = $id_user;
        $nota['mensaje'] = 'Los cumplimientos fueron autorizados';
        $nota['id_plan'] = $_POST['id_plan'];
        $nota['created_at'] = date('Y-m-d');
        DB::table('notas')->insert($nota);
        Flash::success('Los cumplimientos fueron autorizados');
        return redirect('/evaluacion-resultados/cumplimiento-de-compromisos');
      }
    }
	
	/**
     * Guarda el estado de una iniciativa
     *
     * @return void
     */
    public function save_iniciative_state()
    {
        
      $iniciative = array();
      $iniciative['estado'] = $_POST['estado'];
      $iniciative['updated_at'] = date('Y-m-d H:i:s');

      // Se actualiza el estado
      DB::table('iniciativas')->where('id', $_POST['id'])->update($iniciative);

      flash('Se guardó el nuevo estado de la actividad');
      return redirect('/evaluacion-resultados/iniciativas');
    }

    /**
     * Guarda una iniciativa
     *
     * @return void
     */
    public function save_iniciative(){
        
      $iniciative = array();
      $iniciative['description'] = $_POST['description'];
      $iniciative['id_responsable'] = 1;
      $iniciative['id_responsabilidad'] = $_POST['id_responsabilidad'];
      $iniciative['fecha_inicio'] = $_POST['start_date'];
      $iniciative['fecha_termino'] = $_POST['end_date'];
      $id_iniciativa = 0;

      if (!empty($_POST['id'])){

        $iniciative['updated_at'] = date('Y-m-d H:i:s');
        $id_iniciativa = $_POST['id'];

        // Se actualiza la iniciativa
        DB::table('iniciativas')->where('id', $id_iniciativa)->update($iniciative);

        DB::table('iniciativas_responsables')->where('id_iniciativa', $id_iniciativa)->delete();
      }

      else{

        // Se crea la iniciativa
        DB::table('iniciativas')->insert($iniciative);

        $results = DB::table('iniciativas')->select('id')->orderBy('id', 'DESC')->limit(1)->first();
        $id_iniciativa = $results->id;
      }

      if (!empty($_POST['corresponsables'])){

        foreach ($_POST['corresponsables'] as $key => $value){
              
          $responsable = array();
          $responsable['id_iniciativa'] = $id_iniciativa;
          $responsable['id_responsable'] = $value;
          $responsable['tipo'] = 'persona';
          DB::table('iniciativas_responsables')->insert($responsable);
        }
      }

      if (!empty($_POST['grupos_corresponsables'])){

        foreach ($_POST['grupos_corresponsables'] as $key => $value){
              
          $responsable = array();
          $responsable['id_iniciativa'] = $id_iniciativa;
          $responsable['id_responsable'] = $value;
          $responsable['tipo'] = 'grupo';
          DB::table('iniciativas_responsables')->insert($responsable);
        }
      }

      Flash::success('Se guardó la actividad');
      return redirect('/evaluacion-resultados/iniciativas');
    }
	
	/**
     * Crea una nueva nota
     */
    public function save_note(){

      $id_user = auth()->user()->employee_id;
      $nota = array();
      $nota['mensaje'] = $_POST['mensaje'];
      $nota['id_plan'] = $_POST['id_plan'];

      if (!empty($_POST['id_empleado'])){

        $id_empleado = $_POST['id_empleado'];
        $nota['id_de'] = $id_user;

        if ($id_empleado != $id_user){

          $nota['id_para'] = $id_empleado;
        }

        else{

          $personal = Employee::find($id_user);
          $nota['id_para'] = $personal->boss->id;
        }

        DB::table('mensajes')->insert($nota);
      }

      else{

        $nota['id_user'] = $id_user;
        DB::table('notas')->insert($nota);
      }
    }

  /**
  * Borra un tipo de objetivo corporativo
  */
  public function delete_tipo_objetivo_corporativo(){

    $tipos_objetivos_corporativos = DB::select("SELECT id_responsable FROM tipos_objetivos_corporativos WHERE id = ?", [$_POST['id']]);
    $objetivos_corporativos = DB::select("SELECT id FROM objetivos_corporativos WHERE id_tipo = ?", [$_POST['id']]);

    if ($tipos_objetivos_corporativos[0]->id_responsable != 0){

      foreach ($objetivos_corporativos as $key => $objetivo_corporativo){
        
        $responsabilities = DB::select("SELECT id FROM responsabilidades_compromisos WHERE id_objetivo_corporativo = ?", [$objetivo_corporativo->id]);

        foreach ($responsabilities as $key => $responsability){
          
          DB::delete("DELETE FROM cumplimientos_compromisos WHERE id_responsabilidad = ?", [$responsability->id]);
          DB::delete("DELETE FROM responsabilidades_corresponsables WHERE id_responsabilidad = ?", [$responsability->id]);
          DB::delete("DELETE FROM responsabilidades_compromisos WHERE id = ?", [$responsability->id]);
        }

        DB::delete("DELETE FROM objetivos_corporativos WHERE id = ?", [$objetivo_corporativo->id]);
      }
    }

    else{

      foreach ($objetivos_corporativos as $key => $objetivo_corporativo){
        
        $objetivos = DB::select("SELECT id FROM objetivos WHERE id_objetivo_corporativo = ?", [$objetivo_corporativo->id]);

        foreach ($objetivos as $key => $objetivo){
          
          DB::delete("DELETE FROM logros WHERE objetivo_id = ?", [$objetivo->id]);
          DB::delete("DELETE FROM objetivos WHERE id = ?", [$objetivo->id]);
        }

        DB::delete("DELETE FROM objetivos_corporativos WHERE id = ?", [$objetivo_corporativo->id]);
      }
    }

    DB::delete("DELETE FROM tipos_objetivos_corporativos WHERE id = ?", [$_POST['id']]);
  }

  /**
  * Borra un objetivo corporativo
  */
  public function delete_objetivo_corporativo(){

    $responsabilities = DB::select("SELECT id FROM responsabilidades_compromisos WHERE id_objetivo_corporativo = ?", [$_POST['id']]);

    foreach ($responsabilities as $key => $responsability){
          
      DB::delete("DELETE FROM cumplimientos_compromisos WHERE id_responsabilidad = ?", [$responsability->id]);
      DB::delete("DELETE FROM responsabilidades_corresponsables WHERE id_responsabilidad = ?", [$responsability->id]);
      DB::delete("DELETE FROM responsabilidades_compromisos WHERE id = ?", [$responsability->id]);
    }

    $objetivos = DB::select("SELECT id FROM objetivos WHERE id_objetivo_corporativo = ?", [$_POST['id']]);

    foreach ($objetivos as $key => $objetivo){
          
      DB::delete("DELETE FROM logros WHERE objetivo_id = ?", [$objetivo->id]);
      DB::delete("DELETE FROM objetivos WHERE id = ?", [$objetivo->id]);
    }

    DB::delete("DELETE FROM objetivos_corporativos WHERE id = ?", [$_POST['id']]);
  }
	
  /**
     * Borra una responsabilidad
     */
    public function delete_responsability(){

      DB::delete("DELETE FROM responsabilidades_corresponsables WHERE id_responsabilidad = ?", [$_POST['id']]);
      DB::delete("DELETE FROM cumplimientos_compromisos WHERE id_responsabilidad = ?", [$_POST['id']]);
      DB::delete("DELETE FROM responsabilidades_compromisos WHERE id = ?", [$_POST['id']]);
    }
	
	/**
     * Borra una iniciativa
     */
    public function delete_iniciative(){

      DB::delete("DELETE FROM iniciativas_responsables WHERE id_iniciativa = ?", [$_POST['id']]);
      DB::delete("DELETE FROM iniciativas WHERE id = ?", [$_POST['id']]);
    }

  /**
  * Crea un nuevo objetivo de catalogo
  *
  */
  public function crear_objetivo_catalogo(){
 
    $planes =DB::table('tipos_objetivos_corporativos')->join('objetivos_corporativos', 'objetivos_corporativos.id_tipo', '=', 'tipos_objetivos_corporativos.id')->groupBy('id_plan')->pluck('id_plan')->toArray();
    $planes = DB::table('planes')->whereIn('id', $planes)->orderBy('id', 'DESC')->pluck('nombre', 'id')->toArray();
    $objetivos_corporativos = DB::select("SELECT objetivos_corporativos.id, id_tipo, letter, id_plan FROM objetivos_corporativos INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) ORDER BY id_plan DESC, objetivos_corporativos.id_tipo, objetivos_corporativos.id");
    $catalogo_objetivos = DB::table('catalogo_objetivos')->pluck('codigo')->toArray();

    // Muestra la pagina para la creacion de un objetivo de catalogo
    return view('evaluacion-resultados/catalogo-objetivos/create', compact('objetivos_corporativos', 'catalogo_objetivos', 'planes'));
  }

  /**
  * Edita un objetivo de catálogo
  *
  */
  public function editar_objetivo_catalogo($id = 0){

    // Obtiene el plan a editar
    $objetivo_catalogo = DB::table('catalogo_objetivos')->join('objetivos_corporativos', 'objetivos_corporativos.id', '=', 'catalogo_objetivos.id_objetivo_corporativo')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'objetivos_corporativos.id_tipo')->select('catalogo_objetivos.*', 'id_plan')->where('catalogo_objetivos.id', $id)->first();

    $id_plan = $objetivo_catalogo->id_plan;
    $objetivos_corporativos = DB::select("SELECT objetivos_corporativos.id, id_tipo, letter, id_plan FROM objetivos_corporativos INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) ORDER BY id_plan DESC, objetivos_corporativos.id_tipo, objetivos_corporativos.id");

    $planes =DB::table('tipos_objetivos_corporativos')->join('objetivos_corporativos', 'objetivos_corporativos.id_tipo', '=', 'tipos_objetivos_corporativos.id')->groupBy('id_plan')->pluck('id_plan')->toArray();
    $planes = DB::table('planes')->whereIn('id', $planes)->orderBy('id', 'DESC')->pluck('nombre', 'id')->toArray();
    $catalogo_objetivos = DB::table('catalogo_objetivos')->pluck('codigo')->toArray();

    // Muestra la pagina de edicion de objetivos de catálogo
    return view('evaluacion-resultados/catalogo-objetivos/edit', compact('objetivo_catalogo', 'objetivos_corporativos', 'catalogo_objetivos', 'planes', 'id_plan'));
  }

  /**
  * Guarda un nuevo objetivo de catálogo
  */
  public function guardar_objetivo_catalogo(){

    $objetivo_catalogo = array();
    $objetivo_catalogo['objetivo'] = $_POST['objetivo'];
    $objetivo_catalogo['descripcion'] = $_POST['objetivo'];

    if (!empty($_POST['descripcion'])){

      $objetivo_catalogo['descripcion'] = $_POST['descripcion'];
    }

    $objetivo_catalogo['codigo'] = $_POST['codigo'];
    $objetivo_catalogo['id_objetivo_corporativo'] = $_POST['id_objetivo_corporativo'];

    if (!empty($_POST['unidad'])){

      $objetivo_catalogo['unidad'] = $_POST['unidad'];
    }

    if (!empty($_POST['meta'])){

      $objetivo_catalogo['meta'] = $_POST['meta'];
    }

    if (!empty($_POST['peso'])){

      $objetivo_catalogo['peso'] = $_POST['peso'];
    }

    $objetivo_catalogo['frecuencia'] = $_POST['frecuencia'];

    if (!empty($_POST['valor_rojo'])){

      $objetivo_catalogo['valor_rojo'] = $_POST['valor_rojo'];
    }

    if (!empty($_POST['valor_verde'])){

      $objetivo_catalogo['valor_verde'] = $_POST['valor_verde'];
    }

    $objetivo_catalogo['unique_id'] = uniqid(rand());

    // Agrega el nuevo objetivo de catalogo a la base de datos
    DB::table('catalogo_objetivos')->insert($objetivo_catalogo);

    flash('El objetivo de catálogo fue creado correctamente');

    // Redirecciona a la pagina de catálogo de objetivos
    return redirect('/evaluacion-resultados/catalogo-objetivos');
  }

  /**
  * Actualiza un nuevo objetivo de catálogo
  */
  public function actualizar_objetivo_catalogo(){

    $objetivo_catalogo = array();
    $objetivo_catalogo['objetivo'] = $_POST['objetivo'];
    $objetivo_catalogo['descripcion'] = $_POST['objetivo'];

    if (!empty($_POST['descripcion'])){

      $objetivo_catalogo['descripcion'] = $_POST['descripcion'];
    }

    $objetivo_catalogo['codigo'] = $_POST['codigo'];
    $objetivo_catalogo['id_objetivo_corporativo'] = $_POST['id_objetivo_corporativo'];
    $objetivo_catalogo['unidad'] = '';

    if (!empty($_POST['unidad'])){

      $objetivo_catalogo['unidad'] = $_POST['unidad'];
    }

    $objetivo_catalogo['meta'] = '';

    if (!empty($_POST['meta'])){

      $objetivo_catalogo['meta'] = $_POST['meta'];
    }

    $objetivo_catalogo['peso'] = '';

    if (!empty($_POST['peso'])){

      $objetivo_catalogo['peso'] = $_POST['peso'];
    }

    $objetivo_catalogo['frecuencia'] = $_POST['frecuencia'];
    $objetivo_catalogo['valor_rojo'] = '';

    if (!empty($_POST['valor_rojo'])){

      $objetivo_catalogo['valor_rojo'] = $_POST['valor_rojo'];
    }

    $objetivo_catalogo['valor_verde'] = '';

    if (!empty($_POST['valor_verde'])){

      $objetivo_catalogo['valor_verde'] = $_POST['valor_verde'];
    }

    // Actualiza el objetivo de catalogo
    DB::table('catalogo_objetivos')->where('id', $_POST['id'])->update($objetivo_catalogo);

    flash('El objetivo de catálogo fue guardado correctamente');

    // Redirecciona a la pagina de catálogo de objetivos
    return redirect('/evaluacion-resultados/catalogo-objetivos');
  }

  /**
  * Borra un objetivo de catálogo
  *
  */
  public function borrar_objetivo_catalogo($id = 0){

    DB::table('catalogo_objetivos')->where('id', $id)->delete();

    flash('El objetivo de catálogo fue borrado');

    // Redirecciona a la pagina de catálogo de objetivos
    return redirect('/evaluacion-resultados/catalogo-objetivos');
  }
}