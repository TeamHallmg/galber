<?php
namespace App\Http\Controllers\EvaluacionResultados;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use DB;
use Session;
use Mail;
use App\Employee;

class LogrosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Crea o actualiza un nuevo logro para alguna propuesta
     */
    public function create(){

        // Se obtiene el id del empleado
        $id_empleado = $_POST['id_empleado'];
        $logro = 0;

		    $status_plan = DB::select("SELECT id_plan FROM status_plan_empleado WHERE id_empleado = ? AND id_plan = ? AND periodo = ?", [$id_empleado, $_POST['id_plan'], $_POST['periodo']]);

        if (count($status_plan) == 0){

          $status_plan = DB::select("SELECT fecha_solicitado, fecha_autorizado FROM status_plan_empleado WHERE id_empleado = ? AND id_plan = ? ORDER BY periodo DESC LIMIT 1", [$id_empleado, $_POST['id_plan']]);
          DB::insert('INSERT INTO status_plan_empleado (id_plan, id_empleado, status, fecha_solicitado, fecha_autorizado, periodo) values (?,?,?,?,?,?)', [$_POST['id_plan'], $id_empleado, 3, $status_plan[0]->fecha_solicitado, $status_plan[0]->fecha_autorizado, $_POST['periodo']]); 
        }

        else{

          $status_plan_empleado = array();
          $status_plan_empleado['status'] = 3;
          DB::table('status_plan_empleado')->where('id_empleado', $id_empleado)->where('id_plan', $_POST['id_plan'])->where('periodo', $_POST['periodo'])->update($status_plan_empleado);
        }

        $_POST['logro'] = str_replace('$', '', str_replace('%', '', str_replace(',', '', $_POST['logro'])));

        if (!empty($_POST['exist'])){

          $logro = DB::update("UPDATE logros SET logro = '" . $_POST['logro'] . "', revisado = 0 WHERE id_empleado = ? AND id_plan = ? AND periodo = ? AND objetivo_id = ?", [$id_empleado, $_POST['id_plan'], $_POST['periodo'], $_POST['id_metrico']]);

          if (!empty($_POST['comision'])){

            $valor_objetivo = array();
            $valor_objetivo['meta'] = str_replace('$', '', str_replace(',', '', $_POST['comision']));
            DB::table('valores_objetivo')->where('id_objetivo', $_POST['id_metrico'])->where('periodo', $_POST['periodo'])->update($valor_objetivo);
          }
        }

        else{   

          $logro = DB::insert('insert into logros (id_plan, id_empleado, objetivo_id, logro, periodo) values (?,?,?,?,?)', [$_POST['id_plan'], $id_empleado, $_POST['id_metrico'], $_POST['logro'], $_POST['periodo']]);

          if (!empty($_POST['comision'])){

            $valor_objetivo = array();
            $valor_objetivo['meta'] = str_replace('$', '', str_replace(',', '', $_POST['comision']));
            $valor_objetivo['periodo'] = $_POST['periodo'];
            $valor_objetivo['id_objetivo'] = $_POST['id_metrico'];
            DB::table('valores_objetivo')->insert($valor_objetivo);
          }
        }

        echo $logro;
    }

    /**
     * Crea una nueva nota
     */
    public function agregar_nota(){

      $id_empleado = auth()->user()->employee_id;

      if (!empty($_POST['id_empleado'])){

        $id_empleado = $_POST['id_empleado'];
        $id_jefe = auth()->user()->employee_id;
        $nota = DB::insert('insert into mensajes (id_de, id_para, mensaje, fecha) values (?,?,?,?)', [$id_jefe, $id_empleado, $_POST['mensaje'], date('Y-m-d')]);
        $jefe = DB::select('select nombre, paterno, materno from users WHERE id = ?', [$id_jefe]);
        echo $jefe[0]->nombre . ' ' . $jefe[0]->paterno . ' ' . $jefe[0]->materno;
      }

      else{

        //$empleado = DB::select('select boss_id, first_name, last_name from users WHERE id = ?', [$id_empleado]);
        $empleado = Employee::find($id_empleado);

        if (!empty($empleado->boss)){

          $nota = DB::insert('insert into mensajes (id_de, id_para, mensaje, fecha) values (?,?,?,?)', [$id_empleado, $empleado->boss->id, $_POST['mensaje'], date('Y-m-d')]);
        }

        else{

          $nota = DB::insert('insert into mensajes (id_de, id_para, mensaje, fecha) values (?,?,?,?)', [$id_empleado, $id_empleado, $_POST['mensaje'], date('Y-m-d')]);
        }
        echo $empleado->nombre . ' ' . $empleado->paterno . ' ' . $empleado->materno;
      }
    }

    /**
     * Solicita revision para los logros obtenidos
     */
    public function solicitar_revision(){

      // Id del empleado
      $id_empleado = $_POST['id_empleado'];
      $id_plan = $_POST['id_plan'];
      $periodo = $_POST['periodo'];
      $fecha = date('Y-m-d');
      $planes = DB::select("SELECT registrar_logros, autorizar_logros FROM planes WHERE id = ?", [$id_plan]);
      $registrar_logros = $planes[0]->registrar_logros;
      $autorizar_logros = $planes[0]->autorizar_logros;
		
	    if ($registrar_logros != $autorizar_logros){
		
      	DB::update('update status_plan_empleado set status = 4, fecha_registrado = "' . $fecha . '" WHERE id_plan = ? AND id_empleado = ? AND status = ?', [$id_plan, $id_empleado, 3]);
        $empleado = Employee::find($id_empleado);
      	//$jefe = DB::select('select first_name, last_name, email from users WHERE id = ?', [$empleado[0]->boss_id]);
        $nota = DB::insert('insert into mensajes (id_plan, id_de, id_para, mensaje, fecha) values (?,?,?,?,?)', [$id_plan, $id_empleado, $empleado->boss->id, 'Solicitud para Revision Enviada', date('Y-m-d')]);
		    flash('Solicitud para revision enviada');
      }

      else{
		  
		    $results = DB::select("SELECT periodo FROM logros WHERE id_plan = ? AND id_empleado = ? AND revisado = ?", [$id_plan, $id_empleado, 0]);
      
        if (count($results) > 0){
      
          $periodos = array();
      
          foreach ($results as $key => $value){
            
            if (!in_array($value->periodo, $periodos)){

              $periodos[] = $value->periodo;
              DB::update('UPDATE status_plan_empleado SET status = 5, fecha_registrado = "' . $fecha . '", fecha_revisado = "' . $fecha . '" WHERE id_plan = ? AND id_empleado = ? AND periodo = ?', [$id_plan, $id_empleado, $value->periodo]);
            }
          }
        }

        if (!empty(auth()->user()->employee_id)){

          $nota = DB::insert('INSERT INTO mensajes (id_plan, id_de, id_para, mensaje, fecha) values (?,?,?,?,?)', [$id_plan, auth()->user()->employee_id, $id_empleado, 'Revision Aprobada', date('Y-m-d')]);
        }
		      
        DB::update('UPDATE logros SET revisado = 1 WHERE id_plan = ? AND id_empleado = ?', [$id_plan, $id_empleado]);
        flash('Revision Aprobada');
      }

      /*Mail::send('emails.evaluacion-resultados.mensajes', ['mensaje' => 'Tu colaborador ' . $empleado[0]->first_name . ' ' . $empleado[0]->last_name . ' solicita la revisión de sus resultados'], function ($message) use ($empleado, $jefe){
    
        $message->from('evaluacion@abcleasing.com.mx', 'Abc');
        $message->to($jefe[0]->email, $jefe[0]->first_name . ' ' . $jefe[0]->last_name);
		$message->cc($empleado[0]->email);
        $message->subject('Abc - Evaluacion de Resultados');
      });*/
      Session::put('id_plan', $id_plan);

      if ($registrar_logros > 1){

        Session::put('id_empleado', $id_empleado);
      }
      
      return redirect('registro-logros');
    }

    /**
     * Acepta los logros como revisados o los rechaza
     */
    public function revisar(){

      $id_empleado = $_POST['id_empleado'];
      $id_plan = $_POST['id_plan'];
      $periodo = $_POST['periodo'];
      $status = $_POST['status'];
      $mensaje = '';
	    $body = '';
      //$status_plan = DB::select('select status from status_plan_empleado where id_plan = ? AND id_empleado = ? AND periodo = ?', [$id_plan, $id_empleado, $periodo]);
      
      //if (!empty($status_plan)){

        if ($status == 3){

          $nota = 'Revision Rechazada';
		      $body = 'rechazo';
          DB::update('update status_plan_empleado set status = 3 WHERE id_plan = ? AND id_empleado = ? AND status = ?', [$id_plan, $id_empleado, 4]);
        }

        else{

          $fecha = date('Y-m-d');
          $nota = 'Revision Aprobada';
		      $body = 'ha autorizado';
          DB::update('update status_plan_empleado set status = ' . $status . ', fecha_revisado = "' . $fecha . '" WHERE id_plan = ? AND id_empleado = ? AND status = ?', [$id_plan, $id_empleado, 4]);
		      DB::update('UPDATE logros SET revisado = 1 WHERE id_plan = ? AND id_empleado = ?', [$id_plan, $id_empleado]);
        }
      //}

      //else{

        //DB::insert('insert into status_plan_empleado (id_plan, id_empleado, status, fecha_autorizado) values (?,?,?,?)', [$id_plan, $id_empleado, 2, date('Y-m-d')]);
      //}

      //$empleado = DB::select('select boss_id, first_name, last_name, email from users WHERE id = ?', [$id_empleado]);
      $empleado = Employee::find($id_empleado);
      //$jefe = DB::select('select first_name, last_name, email from users WHERE id = ?', [$empleado[0]->boss_id]);
      DB::insert('insert into mensajes (id_plan, id_de, id_para, mensaje, fecha) values (?,?,?,?,?)', [$id_plan, $empleado->boss->id, $id_empleado, $nota, date('Y-m-d')]);

      /*Mail::send('emails.evaluacion-resultados.mensajes', ['mensaje' => $jefe[0]->first_name . ' ' . $jefe[0]->last_name . ' ' . $body . ' tus resultados'], function ($message) use ($jefe, $empleado){
    
        $message->from('evaluacion@abcleasing.com.mx', 'Abc');
        $message->to($empleado[0]->email, $empleado[0]->first_name . ' ' . $empleado[0]->last_name);
		$message->cc($jefe[0]->email);
        $message->subject('Abc - Evaluacion de Resultados');
      });*/

      flash($nota);
      Session::put('id_plan', $id_plan);
      Session::put('id_empleado', $id_empleado);
      return redirect('revision-logros');
    }

  /**
  * Se aceptan los resultados de todo el plan por el Director
  */
    public function revision_director(){

      $id_empleado = $_POST['id_empleado'];
      $id_plan = $_POST['id_plan'];
      $mensaje = '';
      $body = '';
      $nota = 'Revision Aprobada por el Director';
      $empleado = Employee::find($id_empleado);

      //$jefe = DB::select('select first_name, last_name, email from users WHERE id = ?', [$empleado[0]->boss_id]);
      DB::insert('insert into mensajes (id_plan, id_de, id_para, mensaje, fecha) values (?,?,?,?,?)', [$id_plan, auth()->user()->employee->id, $id_empleado, $nota, date('Y-m-d')]);
      $revisado_director = array();
      $revisado_director['plan_id'] = $id_plan;
      $revisado_director['user_id'] = $empleado->user->id;
      $revisado_director['created_by'] = auth()->user()->id;
      DB::table('revision_resultados_director')->insert($revisado_director);
      /*Mail::send('emails.evaluacion-resultados.mensajes', ['mensaje' => $jefe[0]->first_name . ' ' . $jefe[0]->last_name . ' ' . $body . ' tus resultados'], function ($message) use ($jefe, $empleado){
    
        $message->from('evaluacion@abcleasing.com.mx', 'Abc');
        $message->to($empleado[0]->email, $empleado[0]->first_name . ' ' . $empleado[0]->last_name);
    $message->cc($jefe[0]->email);
        $message->subject('Abc - Evaluacion de Resultados');
      });*/

      flash($nota);
      Session::put('id_plan', $id_plan);
      Session::put('id_empleado', $id_empleado);
      return redirect('registro-logros');
    }

  /**
  * Se rechazan los resultados de todo el plan por el Director
  */
  public function rechazo_director(){

    $id_empleado = $_POST['id_empleado'];
    $id_plan = $_POST['id_plan'];
    $nota = 'Revision Rechazada por el Director';
    DB::insert('insert into mensajes (id_plan, id_de, id_para, mensaje, fecha) values (?,?,?,?,?)', [$id_plan, auth()->user()->employee->id, $id_empleado, $nota, date('Y-m-d')]);
    DB::table('logros')->where('id_plan', $id_plan)->where('id_empleado', $id_empleado)->delete();
    DB::table('status_plan_empleado')->where('id_plan', $id_plan)->where('id_empleado', $id_empleado)->where('periodo', '>', 1)->delete();
    flash($nota);
    Session::put('id_plan', $id_plan);
    Session::put('id_empleado', $id_empleado);
    return redirect('registro-logros');
  }
	
	/**
     * Exporta Plantilla para llemado de resultados
     */
    public function exportar_plantilla_resultados(){

      require_once 'PHPExcel.php';
      require_once 'PHPExcel/IOFactory.php';

      $id_plan = $_POST['id_plan'];
      $plan = DB::select("SELECT nombre FROM planes WHERE id = ?", [$id_plan]);
	    $results = array();

      // Create new PHPExcel object
      $objPHPExcel = new \PHPExcel();

      // Create a first sheet, representing sales data
      $objPHPExcel->setActiveSheetIndex(0);

      $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Objetivo');
      $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Descripción del Objetivo');
      $objPHPExcel->getActiveSheet()->setCellValue('C1', 'RFC');
      $objPHPExcel->getActiveSheet()->setCellValue('D1', 'ID Empleado');
      $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Empleado');
      $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Código Plan');
      //$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Plan');
      $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Ene');
      $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Feb');
      $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Mar');
      $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Abr');
      $objPHPExcel->getActiveSheet()->setCellValue('K1', 'May');
      $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Jun');
      $objPHPExcel->getActiveSheet()->setCellValue('M1', 'Jul');
      $objPHPExcel->getActiveSheet()->setCellValue('N1', 'Ago');
      $objPHPExcel->getActiveSheet()->setCellValue('O1', 'Sep');
      $objPHPExcel->getActiveSheet()->setCellValue('P1', 'Oct');
      $objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Nov');
      $objPHPExcel->getActiveSheet()->setCellValue('R1', 'Dic');
      $users_can_register_results = DB::table('status_plan_empleado')->where('id_plan', $_POST['id_plan'])->where('status', '>', 2)->groupBy('id_empleado')->pluck('id_empleado')->toArray();
      $results = DB::table('catalogo_objetivos')->join('objetivos', 'objetivos.id_catalogo', '=', 'catalogo_objetivos.id')->join('employees', 'employees.id', '=', 'objetivos.id_empleado')->leftJoin('logros', 'logros.objetivo_id', '=', 'objetivos.id')->where('objetivos.id_plan', $id_plan)->whereIn('objetivos.id_empleado', $users_can_register_results)->select('codigo', 'catalogo_objetivos.objetivo', 'objetivos.id AS objetivo_id', 'objetivos.tipo', 'objetivos.frecuencia', 'employees.id', 'employees.nombre', 'paterno', 'materno', 'rfc', 'idempleado', 'logro', 'logros.periodo', 'revisado')->orderBy('codigo')->orderBy('employees.id')->orderBy('logros.periodo')->get();

      $i = 1;
      $current_code = '';
      $current_employee_id = $current_objetive = 0;
      $cells = array('XXX','G','H','I','J','K','L','M','N','O','P','Q','R');
      $current_period = 0;
      $frecuencia = 1;

      foreach ($results as $key => $value){

        if ($current_code != $value->codigo || $current_employee_id != $value->id){

          $i++;
          $current_code = $value->codigo;
          $current_employee_id = $value->id;
          $current_period = 0;
          $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $current_code);
          $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $value->objetivo);
          $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $value->rfc);
          $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $value->idempleado);
          $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $value->nombre . ' ' . $value->paterno . ' ' . $value->materno);
          $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $id_plan);
          $frecuencia = 1;

          if ($value->frecuencia == 'Bimestral'){

            $frecuencia = 2;
          }

          else{

            if ($value->frecuencia == 'Trimestral'){

              $frecuencia = 3;
            }

            else{

              if ($value->frecuencia == 'Cuatrimestral'){

                $frecuencia = 4;
              }

              else{

                if ($value->frecuencia == 'Semestral'){

                  $frecuencia = 6;
                }

                else{

                  if ($value->frecuencia == 'Anual'){

                    $frecuencia = 12;
                  }
                }
              }
            } 
          }
        }

        if (!empty($value->revisado)){

          if ($current_period != $value->periodo){

            $current_period = $value->periodo;
            $mes = $current_period * $frecuencia;
            $objPHPExcel->getActiveSheet()->setCellValue($cells[$mes] . $i, $value->logro);
          }
        }
      }

      // Rename sheet
      $objPHPExcel->getActiveSheet()->setTitle('Plantilla');

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="Plantilla Evaluación de Resultados.xls"');
      header('Cache-Control: max-age=0');
      $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      $objWriter->save('php://output');
    }
	
	/**
     * Importa Plantilla de Resultados
     */
    public function importar_plantilla_resultados(){

      if (!empty($_FILES['file']['name'])){

        $array = explode('.', $_FILES['file']['name']);
        $extension = end($array);
      
        if ($extension == 'xls' || $extension == 'xlsx'){

          $path = Storage::disk('local')
                            ->getDriver()
                            ->getAdapter()
                            ->getPathPrefix();
          
          $folder = 'Interface/';
          move_uploaded_file($_FILES['file']['tmp_name'], $path . $folder . 'importar_logros.' . $extension);
          require_once "PHPExcel.php";

          $inputFileName = $path . $folder . 'importar_logros.' . $extension;
          $excelReader = \PHPExcel_IOFactory::createReaderForFile($inputFileName);
          $objPHPExcel = $excelReader->load($inputFileName);

          // Get worksheet dimensions
          $sheet = $objPHPExcel->getSheet(0); 
          $highestRow = $sheet->getHighestRow();
          $users = array();
          $id_empleado = 0;
          $resultado_a_insertar = array();
          $resultado_a_insertar['objetivo_id'] = 0;
          $resultado_a_insertar['logro'] = 0;
          $resultado_a_insertar['periodo'] = 0;
          $resultado_a_insertar['id_plan'] = 0;
          $resultado_a_insertar['id_empleado'] = 0;
          $resultado_a_insertar['revisado'] = 1;
          $resultado_a_actualizar = array();
          $resultado_a_actualizar['revisado'] = 1;
		      $columnas_meses = array('G','H','I','J','K','L','M','N','O','P','Q','R');
          $users = array();
          $fecha = date('Y-m-d');
          $statuses_planes = array();
          $num_errores = 0;
          $errores = '';

          //  Loop through each row of the worksheet in turn
          for ($row = 2; $row <= $highestRow; $row++){
          
            $codigo = $sheet->getCell('A'.$row)->getValue();
            $rfc = $sheet->getCell('C'.$row)->getValue();
            //$nombre_objetivo = $sheet->getCell('B'.$row)->getValue();
            $id_plan = $sheet->getCell('F'.$row)->getValue();
            //$value = $sheet->getCell('M'.$row)->getValue();

            //if ($current_plan == 0 || $current_plan != $id_plan){

            //$users = DB::table('status_plan_empleado')->where('id_plan', $id_plan)->where('status', '>', 2)->groupBy('id_empleado')->pluck('id_empleado')->toArray();
            //}

            //$current_plan = $id_plan;

            if (!empty($codigo) && !empty($id_plan) && !empty($rfc)){
            //if (!empty($id_empleado) && !empty($id_plan)){

              $plan = DB::table('planes')->where('id', $id_plan)->first();

              if (empty($plan)){

                $num_errores++;
                $errores .= $num_errores . '. La fila ' . $row . ' no contiene un plan válido<br>';
                continue;
              }

              $user = DB::table('employees')->where('rfc', $rfc)->select('id')->first();

              if (!empty($user)){

                if (!in_array($user->id, $statuses_planes)){

                  $status_plan = DB::table('status_plan_empleado')->where('id_plan', $id_plan)->where('status', '>', 2)->where('id_empleado', $user->id)->first();

                  if (!empty($status_plan)){

                    $statuses_planes[] = $user->id;
                  }
                }

                if (in_array($user->id, $statuses_planes)){

                  $objetivo = DB::table('catalogo_objetivos')->join('objetivos', 'objetivos.id_catalogo', '=', 'catalogo_objetivos.id')->where('codigo', $codigo)->where('id_plan', $id_plan)->where('id_empleado', $user->id)->select('objetivos.id', 'objetivos.frecuencia')->first();
                  $num_meses = 1;

                  if (empty($objetivo)){

                    $num_errores++;
                    $errores .= $num_errores . '. La fila ' . $row . ' no contiene un objetivo válido<br>';
                    continue;
                  }

                  if ($objetivo->frecuencia == 'Bimestral'){

                    $num_meses = 2;
                  }

                  else{

                    if ($objetivo->frecuencia == 'Trimestral'){

                      $num_meses = 3;
                    }

                    else{

                      if ($objetivo->frecuencia == 'Cuatrimestral'){

                        $num_meses = 4;
                      }

                      else{

                        if ($objetivo->frecuencia == 'Semestral'){

                          $num_meses = 6;
                        }

                        else{

                          if ($objetivo->frecuencia == 'Anual'){

                            $num_meses = 12;
                          }
                        }
                      }
                    } 
                  }

                  foreach ($columnas_meses as $key => $columna){
              
                    $value = $sheet->getCell($columna.$row)->getFormattedValue();
                    $value = str_replace(',', '', str_replace('$', '', str_replace('%', '', $value)));
                    $value = trim($value);

                    if (!empty($value) || $value == '0'){

                      if ($value != 'Si' && $value != 'No' && !is_numeric($value)){

                        $value = $sheet->getCell($columna.$row)->getOldCalculatedValue();

                        if (!is_numeric($value)){

                          $value = $sheet->getCell($columna.$row)->getValue();
                        }
                      }

                      $periodo = $key + 1;

                      if ($periodo < $plan->periodo){

                        $num_errores++;
                        $errores .= $num_errores . '. La fila ' . $row . ' contiene un resultado en un mes anterior al mes de inicio del plan<br>';

                        continue;
                      }

                      if ($periodo % $num_meses != 0){

                        do{

                          $periodo++;

                        }while($periodo % $num_meses != 0);
                      }

                      $periodo = $periodo / $num_meses;
                      $logro = DB::table('logros')->where('objetivo_id', $objetivo->id)->where('periodo', $periodo)->first();

                      if (empty($logro)){

                        $resultado_a_insertar['logro'] = $value;
                        $resultado_a_insertar['objetivo_id'] = $objetivo->id;
                        $resultado_a_insertar['id_empleado'] = $user->id;
                        $resultado_a_insertar['periodo'] = $periodo;
                        $resultado_a_insertar['id_plan'] = $id_plan;
                        DB::table('logros')->insert($resultado_a_insertar);
                      }

                      else{

                        $resultado_a_actualizar['logro'] = $value;
                        DB::table('logros')->where('objetivo_id', $objetivo->id)->where('periodo', $periodo)->update($resultado_a_actualizar);
                      }

                      $status_plan_empleado = array();
                      $status_plan_empleado['status'] = 5;
                      $status_plan_empleado['fecha_registrado'] = $fecha;
                      $status_plan_empleado['fecha_revisado'] = $fecha;
                      $status_plan = DB::table('status_plan_empleado')->where('id_plan', $id_plan)->where('id_empleado', $user->id)->where('periodo', $periodo)->first();

                      if (!empty($status_plan)){

                        DB::table('status_plan_empleado')->where('id_empleado', $user->id)->where('id_plan', $id_plan)->where('periodo', $periodo)->update($status_plan_empleado);
                      }

                      else{

                        $status_plan_empleado['id_plan'] = $id_plan;
                        $status_plan_empleado['id_empleado'] = $user->id;
                        $status_plan_empleado['periodo'] = $periodo;
                        $status_plan_empleado['fecha_solicitado'] = $fecha;
                        $status_plan_empleado['fecha_autorizado'] = $fecha;
                        DB::table('status_plan_empleado')->insert($status_plan_empleado);
                      }
                    }
                  }
                }
              }
            }
          }

          if ($num_errores == 0){
      
            flash('Los resultados fueron guardados');
          }

          else{

            flash($errores);
          }
        }

        else{
      
          flash("El archivo de importacion de resultados no es un archivo excel");
        }
      }

      Session::put('id_plan', $id_plan);
      return redirect('/estadistica-avance');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = 0){
      
      if (!empty($_POST['id'])){

        $eje_estrategico = DB::delete('delete from ejes_estrategicos where id = ?', [$_POST['id']]);
        
        if (!empty($eje_estrategico)){

          flash('El eje estratégico fue borrado correctamente');
        }

        else{

          flash('Error al borrar el eje estratégico. Intente de nuevo');
        }

        return redirect('ejes-estrategicos');
      }

      $eje_estrategico = DB::select('select id, nombre from ejes_estrategicos where id = ?', [$id]);
      return view('evaluacion-resultados/ejes-estrategicos/delete', compact('eje_estrategico'));
    }

  /**
     * Borrar resultados
     */
    public function resetear_logros(){

      // Id del empleado
      $id_empleado = $_POST['id_empleado'];
      $id_plan = $_POST['id_plan'];

      $user = DB::table('users')->where('employee_id', $id_empleado)->select('id')->first();

      DB::table('logros')->where('id_plan', $id_plan)->where('id_empleado', $id_empleado)->delete();

      DB::table('status_plan_empleado')->where('id_plan', $id_plan)->where('id_empleado', $id_empleado)->where('periodo', '>', 1)->delete();
      DB::table('revision_resultados_director')->where('plan_id', $id_plan)->where('user_id', $user->id)->delete();
      Session::put('id_plan', $id_plan);
      Session::put('id_empleado', $id_empleado);
      flash('Los logros fueron borrados');
      return redirect('registro-logros');
    }
}
