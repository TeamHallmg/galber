<?php

namespace App\Http\Controllers\EvaluacionResultados;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\EvaluacionResultados\Admin\StatusPlanEmpleado;
use DB;
use Session;
use Mail;
use App\Employee;

class PropuestasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Guarda una propuesta
     *
     * @return int
     */
    public function create(){
		
		// Se obtiene el id del empleado
        $id_empleado = $_POST['id_empleado'];
        $propuesta = 0;
        $retorno = 0;
        $id = 0;
        $plan = DB::table('planes')->where('id', $_POST['id_plan'])->select('periodo')->first();
        $_POST['periodo'] = $plan->periodo;

        if ($_POST['frecuencia'] == 'Bimestral'){

          if ($plan->periodo > 5){

            $_POST['periodo'] = 5;
          }
        }

        else{

          if ($_POST['frecuencia'] == 'Trimestral'){

            if ($plan->periodo > 4){

              $_POST['periodo'] = 4;
            }
          }

          else{

            if ($_POST['frecuencia'] == 'Cuatrimestral'){

              if ($plan->periodo > 3){

                $_POST['periodo'] = 3;
              }
            }

            else{

              if ($_POST['frecuencia'] == 'Semestral'){

                if ($plan->periodo > 2){

                  $_POST['periodo'] = 2;
                }
              }

              else{

                if ($_POST['frecuencia'] == 'Anual'){

                  $_POST['periodo'] = 1;
                }
              }
            }
          } 
        }

        DB::table('bonos_planes')->where('id_user', $id_empleado)->where('id_plan', $_POST['id_plan'])->delete();

        // Existe el id de la propuesta (es una actualizacion)
        if (!empty($_POST['id'])){

          $id = $_POST['id'];
          DB::table('objetivos_corresponsables')->where('id_objetivo', $id)->delete();
          DB::table('bonos_objetivos')->where('id_objetivo', $id)->delete();

          //if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

            $bono_plan = array();
            $bono_plan['id_plan'] = $_POST['id_plan'];
            $bono_plan['id_user'] = $id_empleado;
            $bono_plan['total_bono'] = str_replace('$', '', str_replace(',', '', $_POST['total_bono']));
            DB::table('bonos_planes')->insert($bono_plan);
          //}

          /*$objetivos = DB::select("SELECT periodo FROM objetivos WHERE id = ?", [$_POST['id']]);

          if ($objetivos[0]->periodo != $_POST['periodo']){

            DB::insert("INSERT INTO valores_objetivo VALUES (?,?,?,?)", [$_POST['id'],$_POST['valor_rojo'],$_POST['valor_verde'],$_POST['periodo']]);
          }

          else{*/
			
			if (empty($_POST['id_objetivo_corporativo'])){
				
				$_POST['id_objetivo_corporativo'] = 0;
			}

          if (!empty($_POST['objetivo'])){

            $objetivo = explode(',', $_POST['objetivo']);
            $valor_rojo = explode(',', $_POST['valor_rojo']);
            $valor_verde = explode(',', $_POST['valor_verde']);

            if (count($objetivo) > 1){

              $periodo_inicial = 12 - count($objetivo) + 1;

              if ($_POST['frecuencia'] == 'Bimestral'){

                $periodo_inicial = 6 - count($objetivo) + 1;
              }
      
              else{

                if ($_POST['frecuencia'] == 'Trimestral'){

                  $periodo_inicial = 4 - count($objetivo) + 1;
                }

                else{

                  if ($_POST['frecuencia'] == 'Cuatrimestral'){

                    $periodo_inicial = 3 - count($objetivo) + 1;
                  }

                  else{

                    if ($_POST['frecuencia'] == 'Semestral'){

                      $periodo_inicial = 2 - count($objetivo) + 1;
                    }

                    else{

                      if ($_POST['frecuencia'] == 'Anual'){

                        $periodo_inicial = 1;
                      }
                    }
                  } 
                }
              }

              DB::table('valores_objetivo')->where('id_objetivo', $id)->delete();
              $valor_objetivo = array();
              $valor_objetivo['id_objetivo'] = $id;
              $valores_objetivo = array();

              foreach ($objetivo as $key => $value){
                
                $valor_objetivo['meta'] = $value;
                $valor_objetivo['valor_rojo'] = $valor_rojo[$key];
                $valor_objetivo['valor_verde'] = $valor_verde[$key];
                $valor_objetivo['periodo'] = $periodo_inicial;
                $valores_objetivo[] = $valor_objetivo;
                $periodo_inicial++;
              }

              DB::table('valores_objetivo')->insert($valores_objetivo);
              $propuesta = array();
              $propuesta['nombre'] = $_POST['nombre'];
              $propuesta['formula'] = $_POST['formula'];
              $propuesta['periodo'] = $_POST['periodo'];
              $propuesta['id_objetivo_corporativo'] = $_POST['id_objetivo_corporativo'];
              $propuesta['tipo'] = $_POST['tipo'];
              $propuesta['peso'] = $_POST['peso'];
              $propuesta['objetivo'] = $_POST['objetivo'];
              $propuesta['frecuencia'] = $_POST['frecuencia'];
              $propuesta['valor_rojo'] = '';
              $propuesta['valor_verde'] = '';
              DB::table('objetivos')->where('id', $id)->update($propuesta);

              // Actualiza la propuesta
              //$propuesta = DB::update('UPDATE objetivos SET nombre = "' . $_POST['nombre'] . '", formula = "' . $_POST['formula'] . '", periodo = ' . $_POST['periodo'] . ', id_objetivo_corporativo = ' . $_POST['id_objetivo_corporativo'] . ', tipo = "' . $_POST['tipo'] . '", peso = ' . $_POST['peso'] . ', objetivo = "", frecuencia = "' . $_POST['frecuencia'] . '",  valor_rojo = "", valor_verde = "" WHERE id = ?', [$id]);
            }

            else{

              $propuesta = array();
              $propuesta['nombre'] = $_POST['nombre'];
              $propuesta['formula'] = $_POST['formula'];
              $propuesta['periodo'] = $_POST['periodo'];
              $propuesta['id_objetivo_corporativo'] = $_POST['id_objetivo_corporativo'];
              $propuesta['tipo'] = $_POST['tipo'];
              $propuesta['peso'] = $_POST['peso'];
              $propuesta['objetivo'] = $_POST['objetivo'];
              $propuesta['frecuencia'] = $_POST['frecuencia'];
              $propuesta['valor_rojo'] = $_POST['valor_rojo'];
              $propuesta['valor_verde'] = $_POST['valor_verde'];
              DB::table('objetivos')->where('id', $id)->update($propuesta);

              // Actualiza la propuesta
              //$propuesta = DB::update('UPDATE objetivos SET nombre = "' . $_POST['nombre'] . '", formula = "' . $_POST['formula'] . '", periodo = ' . $_POST['periodo'] . ', id_objetivo_corporativo = ' . $_POST['id_objetivo_corporativo'] . ', tipo = "' . $_POST['tipo'] . '", peso = ' . $_POST['peso'] . ', objetivo = "' . $_POST['objetivo'] . '", frecuencia = "' . $_POST['frecuencia'] . '",  valor_rojo = "' . $_POST['valor_rojo'] . '", valor_verde = "' . $_POST['valor_verde'] . '" WHERE id = ?', [$id]);
            }
          }
          //}
			
	      // Cambia el status del periodo actual
            //DB::update('UPDATE status_plan_empleado SET status = 1 WHERE id_plan = ? AND id_empleado = ? AND periodo = ?', [$_POST['id_plan'], $id_empleado, $_POST['periodo']]);
        }

        // Se va a guardar una nueva propuesta
        else{   

          $fecha = date('Y-m-d H:i:s');
			
		  // Cambia el status del periodo actual
          //DB::update('UPDATE status_plan_empleado SET status = 1 WHERE id_plan = ? AND id_empleado = ? AND periodo = ?', [$_POST['id_plan'], $id_empleado, $_POST['periodo']]);
			
		  if (empty($_POST['id_objetivo_corporativo'])){
				
			$_POST['id_objetivo_corporativo'] = 0;
		  }

      if (empty($_POST['id_catalogo'])){
        
      $_POST['id_catalogo'] = 0;
      }

      if (empty($_POST['formula'])){
        
      $_POST['formula'] = '';
      }

          // Se guarda la nueva propuesta
          $propuesta = DB::insert('INSERT INTO objetivos (nombre, id_plan, id_objetivo_corporativo, id_catalogo, id_empleado, formula, tipo, peso, objetivo, frecuencia, periodo, valor_rojo, valor_verde, created_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [$_POST['nombre'], $_POST['id_plan'], $_POST['id_objetivo_corporativo'], $_POST['id_catalogo'], $id_empleado, $_POST['formula'], $_POST['tipo'], $_POST['peso'], $_POST['objetivo'], $_POST['frecuencia'], $_POST['periodo'], $_POST['valor_rojo'], $_POST['valor_verde'], $fecha]);
          
          // Se regresa el id de la nueva propuesta
          $retorno = DB::getPdo()->lastInsertId();
          $id = $retorno;

          if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

            $bono_plan = array();
            $bono_plan['id_plan'] = $_POST['id_plan'];
            $bono_plan['id_user'] = $id_empleado;
            $bono_plan['total_bono'] = str_replace('$', '', str_replace(',', '', $_POST['total_bono']));
            DB::table('bonos_planes')->insert($bono_plan);
          }
        }

        if (!empty($_POST['grupos_corresponsables'])){

          $grupos_corresponsables = explode(',', $_POST['grupos_corresponsables']);
          $grupo_corresponsable = array();
          $grupo_corresponsable['id_objetivo'] = $id;
          $grupo_corresponsable['tipo'] = 'grupo';

          foreach ($grupos_corresponsables as $key => $value){
                
            $grupo_corresponsable['id_corresponsable'] = $value;
            DB::table('objetivos_corresponsables')->insert($grupo_corresponsable);
          }
        }

        if (!empty($_POST['corresponsables'])){

          $corresponsables = explode(',', $_POST['corresponsables']);
          $corresponsable = array();
          $corresponsable['id_objetivo'] = $id;
          $corresponsable['tipo'] = 'persona';

          foreach ($corresponsables as $key => $value){
                
            $corresponsable['id_corresponsable'] = $value;
            DB::table('objetivos_corresponsables')->insert($corresponsable);
          }
        }

        if (!empty($_POST['niveles_logro'])){

          $niveles_logro = explode(',', $_POST['niveles_logro']);
          $porcentajes_bono = explode(',', $_POST['porcentajes_bono']);
          $bono_objetivo = array();
          $bono_objetivo['id_objetivo'] = $id;

          foreach ($niveles_logro as $key => $value){

            if (!empty($value) && $value != '%'){
                
              $bono_objetivo['nivel_logro'] = str_replace('%', '', $value);
              $bono_objetivo['porcentaje_bono'] = str_replace('%', '', $porcentajes_bono[$key]);
              DB::table('bonos_objetivos')->insert($bono_objetivo);
            }
          }
        }

        echo $retorno;
    }
	
	/**
     * Guarda nuevos valores para una propuesta
     *
     * @return int
     */
    public function guardar_valores(){

        $respuesta = 0;
		$id_empleado = $_POST['id_empleado'];

        // Existe el id de la propuesta
        if (!empty($_POST['id'])){

          $logros = DB::select("SELECT periodo FROM logros WHERE objetivo_id = ? ORDER BY periodo DESC LIMIT 1", [$_POST['id']]);
		  $_POST['periodo'] = $logros[0]->periodo + 1;
		  $valores = DB::select("SELECT valor_rojo, valor_verde, periodo FROM valores_objetivo WHERE id_objetivo = ? ORDER BY periodo DESC LIMIT 1", [$_POST['id']]);

          if (count($valores) > 0){

            if ($valores[0]->valor_rojo != $_POST['valor_rojo'] || $valores[0]->valor_verde != $_POST['valor_verde']){

              if ($_POST['periodo'] == $valores[0]->periodo){

                // Cambia los valores del objetivo
                DB::update("UPDATE valores_objetivo SET valor_rojo = " . $_POST['valor_rojo'] . ", valor_verde = " . $_POST['valor_verde'] . " WHERE id_objetivo = ? AND periodo = ?", [$_POST['id'], $_POST['periodo']]);
              }

              else{

                DB::insert("INSERT INTO valores_objetivo VALUES (?,?,?,?)", [$_POST['id'],$_POST['valor_rojo'],$_POST['valor_verde'],$_POST['periodo']]);

                // Cambia el status del periodo actual
                DB::update('UPDATE status_plan_empleado SET status = 1 WHERE id_plan = ? AND id_empleado = ? AND periodo = ?', [$_POST['id_plan'], $id_empleado, $_POST['periodo']]);
                
                $respuesta = 1;
              }
            }
          }

          else{

            $objetivos = DB::select("SELECT valor_rojo, valor_verde FROM objetivos WHERE id = ?", [$_POST['id']]);

            if ($objetivos[0]->valor_rojo != $_POST['valor_rojo'] || $objetivos[0]->valor_verde != $_POST['valor_verde']){

              DB::insert("INSERT INTO valores_objetivo VALUES (?,?,?,?)", [$_POST['id'],$_POST['valor_rojo'],$_POST['valor_verde'],$_POST['periodo']]);

              // Cambia el status del periodo actual
              DB::update('UPDATE status_plan_empleado SET status = 1 WHERE id_plan = ? AND id_empleado = ? AND periodo = ?', [$_POST['id_plan'], $id_empleado, $_POST['periodo']]);

              $respuesta = 1;
            }
          }
        }

        echo $respuesta;
    }
	
	/**
     * Importa objetivos desde un archivo excel
     */
    public function importar_objetivos(){

      $puede_importar_objetivos = false;

      // El usuario logueado es admin
      if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){
      
        $puede_importar_objetivos = true;
      }

      if ($puede_importar_objetivos){

        if (!empty($_FILES['file'])){

          $array = explode('.', $_FILES['file']['name']);
          $extension = end($array);
      
          if ($extension == 'xls' || $extension == 'xlsx'){

            move_uploaded_file($_FILES['file']['tmp_name'], '/var/www/vhosts/soysepanka.com/somosucin.soysepanka.com/storage/app/Interface/importar_objetivos.' . $extension);
            $path = Storage::disk('local')
                            ->getDriver()
                            ->getAdapter()
                            ->getPathPrefix();
            $folder = 'Interface/';

            // Include PHPExcel_IOFactory
            //include 'PHPExcel/IOFactory.php';
			      require_once "PHPExcel.php";

            $inputFileName = $path . $folder . 'importar_objetivos.' . $extension;
			      $excelReader = \PHPExcel_IOFactory::createReaderForFile($inputFileName);
            //$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            //$objReader = PHPExcel_IOFactory::createReaderForFile($inputFileType);
            $objPHPExcel = $excelReader->load($inputFileName);

            // Get worksheet dimensions
            $sheet = $objPHPExcel->getSheet(0); 
            $highestRow = $sheet->getHighestRow();
            //$highestColumn = $sheet->getHighestColumn();

            //$fecha = date('Y-m-d');
            $objetivo = array();
            $objetivo['id_plan'] = 1;
            //$objetivo['frecuencia'] = 'Mensual';
            //$bono_plan = array();
            //$bono_plan['id_plan'] = 1;
            $bono_objetivo = array();
            //$bonos_guardados[] = array();
            /*$status_plan_empleado = array();
            $status_plan_empleado['id_plan'] = 1;
            $status_plan_empleado['status'] = 3;
            $status_plan_empleado['fecha_solicitado'] = $fecha;
            $status_plan_empleado['fecha_autorizado'] = $fecha;
            $status_plan_empleado['periodo'] = 9;*/

            $objetivos_corporativos = DB::table('objetivos_corporativos')->join('tipos_objetivos_corporativos', 'tipos_objetivos_corporativos.id', '=', 'objetivos_corporativos.id_tipo')->select('letter', 'id_tipo', 'objetivos_corporativos.id')->orderBy('id_tipo')->orderBy('objetivos_corporativos.id')->get();

            //  Loop through each row of the worksheet in turn
            for ($row = 2; $row <= $highestRow; $row++){

              $niveles_logro = array();
              $porcentajes_bono = array();
              $id_empleado = $sheet->getCell('A'.$row)->getValue();
              $objetivo['nombre'] = $sheet->getCell('B'.$row)->getValue();
              $id_objetivo_corporativo = $sheet->getCell('C'.$row)->getValue();
              $objetivo['tipo'] = $sheet->getCell('D'.$row)->getValue();
              $objetivo['objetivo'] = $sheet->getCell('E'.$row)->getValue();
              $objetivo['peso'] = $sheet->getCell('F'.$row)->getValue();
              $objetivo['frecuencia'] = $sheet->getCell('G'.$row)->getValue();
              $objetivo['valor_rojo'] = $sheet->getCell('H'.$row)->getValue();
              $objetivo['valor_verde'] = $sheet->getCell('I'.$row)->getValue();
              //$bono_plan['total_bono'] = $sheet->getCell('H'.$row)->getValue();
              $niveles_logro[] = $sheet->getCell('K'.$row)->getValue();
              $porcentajes_bono[] = $sheet->getCell('L'.$row)->getValue();
              $niveles_logro[] = $sheet->getCell('M'.$row)->getValue();
              $porcentajes_bono[] = $sheet->getCell('N'.$row)->getValue();
              $niveles_logro[] = $sheet->getCell('O'.$row)->getValue();
              $porcentajes_bono[] = $sheet->getCell('P'.$row)->getValue();

              if (!empty($id_empleado)){

                $personal = DB::table('employees')->where('idempleado', $id_empleado)->select('id')->first();
                //$objetivo_catalogo = DB::table('catalogo_objetivos')->where('codigo', $codigo_objetivo_catalogo)->select('id', 'objetivo', 'id_objetivo_corporativo')->first();
                $objetivo['id_empleado'] = $personal->id;
                //$bono_plan['id_user'] = $personal->id;
                //$status_plan_empleado['id_empleado'] = $personal->id;
                //$objetivo['id_catalogo'] = $objetivo_catalogo->id;
                //$objetivo['nombre'] = $objetivo_catalogo->objetivo;
                $counter = 0;
                $current_type = 0;

                foreach ($objetivos_corporativos as $key => $value){
                  
                  if ($current_type != $value->id_tipo){

                    $current_type = $value->id_tipo;
                    $counter = 1;
                  }

                  $objetivo_corporativo = $value->letter . $counter;

                  if ($objetivo_corporativo == $id_objetivo_corporativo){

                    $objetivo['id_objetivo_corporativo'] = $value->id;
                    break;
                  }

                  $counter++;
                }

                $objetivo['objetivo'] = str_replace('$', '', str_replace('%', '', str_replace(',', '', $objetivo['objetivo'])));
                $objetivo['valor_rojo'] = str_replace('$', '', str_replace('%', '', str_replace(',', '', $objetivo['valor_rojo'])));
                $objetivo['valor_verde'] = str_replace('$', '', str_replace('%', '', str_replace(',', '', $objetivo['valor_verde'])));

                //$objetivo['id_objetivo_corporativo'] = $objetivo_catalogo->id_objetivo_corporativo;
                DB::table('objetivos')->insert($objetivo);

                /*if (!in_array($personal->id, $bonos_guardados)){

                  DB::table('bonos_planes')->insert($bono_plan);
                  DB::table('status_plan_empleado')->insert($status_plan_empleado);
                  $bonos_guardados[] = $personal->id;
                }*/

                $nuevo_objetivo = DB::table('objetivos')->select('id')->orderBy('id', 'DESC')->first();
                $bono_objetivo['id_objetivo'] = $nuevo_objetivo->id;

                foreach ($niveles_logro as $key => $value){
                  
                  if (!empty($value)){

                    $bono_objetivo['nivel_logro'] = str_replace('%', '', $value);

                    if (!empty($porcentajes_bono[$key])){

                      $bono_objetivo['porcentaje_bono'] = str_replace('%', '', $porcentajes_bono[$key]);
                    }

                    else{

                      $bono_objetivo['porcentaje_bono'] = 100;
                    }

                    DB::table('bonos_objetivos')->insert($bono_objetivo);
                  }
                }
              }
            }

            flash("Los objetivos fueron importados correctamente");
          }

          else{

            flash("El archivo no es un archivo excel");
          }
        }
      }

      else{

        flash("No tiene permiso para ver esta sección");
        return redirect()->to('que-es-evaluacion-resultados');
      }
    
      return redirect()->to('estadistica-avance');
    }

    /**
     * Se agrega una nueva nota para el chat entre empleado y jefe
     *
     * @return string
     */
    public function agregar_nota(){

      // Id del usuario logueado
      $id_user = auth()->user()->employee_id;

      $id_plan = $_POST['id_plan'];
      $id_empleado = $_POST['id_empleado'];

      // Obtiene los datos del usuario logueado
      $user = Employee::find($id_user);

      // El usuario logueado no es el empleado a evaluar
      if ($id_user != $id_empleado){
        
        // Agrega una nueva nota
        $nota = DB::insert('insert into mensajes (id_plan, id_de, id_para, mensaje, fecha) values (?,?,?,?,?)', [$id_plan, $id_user, $id_empleado, $_POST['mensaje'], date('Y-m-d')]);
      }

      // El que agrega la nota es el usuario logueado
      else{
        
        // Agrega una nueva nota
        $nota = DB::insert('insert into mensajes (id_plan, id_de, id_para, mensaje, fecha) values (?,?,?,?,?)', [$id_plan, $id_user, $user->boss->id, $_POST['mensaje'], date('Y-m-d')]);
      }

      echo $user->nombre . ' ' . $user->paterno . ' ' . $user->materno;
    }

    /**
     * Solicita autorizacion para las propuestas
     *
     * @return void
     */
    public function solicitar_autorizacion(){

      // Id del empleado
      $id_empleado = $_POST['id_empleado'];
      $id_plan = $_POST['id_plan'];
      $periodo = $_POST['periodo'];
		
      $planes = DB::select("SELECT alimentar_propuestas, autorizar_propuestas FROM planes WHERE id = ?", [$id_plan]);
      $alimentar_propuestas = $planes[0]->alimentar_propuestas;
      $autorizar_propuestas = $planes[0]->autorizar_propuestas;
      /*$bono_plan = DB::table('bonos_planes')->where('id_user', $id_empleado)->where('id_plan', $id_plan)->first();

      if (empty($bono_plan)){

        // Guarda en sesion el plan actual
        Session::put('id_plan', $id_plan);

        if ($alimentar_propuestas == 2 || $alimentar_propuestas == 3){

          Session::put('id_empleado', $id_empleado);
        }

        // Mensaje para avisar
        flash('No se ha guardado el bono');
      
        // Redirecciona a la pagina de las propuestas
        return redirect('propuesta');        
      }*/
		
	  /*$objetivos = DB::select("SELECT SUM(peso) AS peso_total FROM objetivos WHERE id_plan = ? AND id_empleado = ? GROUP BY periodo", [$id_plan, $id_empleado]);
		
	  if (count($objetivos) > 0){
		  
		  $peso_valido = true;
		  
		  foreach ($objetivos AS $key => $value){
			  
			  if ($value->peso_total != 100){
				  
				  $peso_valido = false;
				  break;
			  }
		  }
		  
		  if (!$peso_valido){
			  
			  // Mensaje para avisar
      		  flash('La suma de los pesos de los objetivos por periodo debe ser 100');
      
      		  // Guarda en sesion el plan actual
      		  Session::put('id_plan', $id_plan);

      		  if ($alimentar_propuestas == 2){

        	  	Session::put('id_empleado', $id_empleado);
      		  }
      
      		  // Redirecciona a la pagina de las propuestas
      		  return redirect('propuesta');
		  }
	  }
		
	  else{
		  
		  if ($periodo == 1){
			  
			  // Mensaje para avisar
      		  flash('La suma de los pesos de los objetivos por periodo debe ser 100');
			  
			  // Guarda en sesion el plan actual
      		  Session::put('id_plan', $id_plan);

      		  if ($alimentar_propuestas == 2){

        	  	Session::put('id_empleado', $id_empleado);
      		  }
      
      		  // Redirecciona a la pagina de las propuestas
      		  return redirect('propuesta');
		  }
	  }*/
      
      $status = 2;

      if ($alimentar_propuestas == $autorizar_propuestas){

        $status = 3;
      }

      // Obtiene los datos del periodo y plan actuales del empleado
      $status_plan = DB::select('select status from status_plan_empleado where id_plan = ? AND id_empleado = ? AND periodo = ?', [$id_plan, $id_empleado, $periodo]);
      
      // Existe ya algun periodo de un plan
      if (!empty($status_plan)){

        // Fecha actual
        $fecha = date('Y-m-d');

        if ($status == 2){

          // Actualiza el status del periodo
          DB::update('update status_plan_empleado set status = 2, fecha_solicitado = "' . $fecha . '" WHERE id_plan = ? AND id_empleado = ? AND periodo = ?', [$id_plan, $id_empleado, $periodo]);
        }

        else{

          // Actualiza el status del periodo
          DB::update('update status_plan_empleado set status = 3, fecha_autorizado = "' . $fecha . '" WHERE id_plan = ? AND id_empleado = ? AND periodo = ?', [$id_plan, $id_empleado, $periodo]);
        }
      }

      // No hay periodos
      else{

        if ($status == 2){

          // Inserta un nuevo periodo
          DB::insert('insert into status_plan_empleado (id_plan, id_empleado, status, fecha_solicitado, periodo) values (?,?,?,?,?)', [$id_plan, $id_empleado, 2, date('Y-m-d'), $periodo]);
        }

        else{

          // Inserta un nuevo periodo
          DB::insert('insert into status_plan_empleado (id_plan, id_empleado, status, fecha_solicitado, fecha_autorizado, periodo) values (?,?,?,?,?,?)', [$id_plan, $id_empleado, 3, date('Y-m-d'), date('Y-m-d'), $periodo]);
        }
      }

      $configuraciones = DB::select("SELECT alimentar_propuestas FROM planes WHERE id = ?", [$id_plan]);
      $alimentar_propuestas = $configuraciones[0]->alimentar_propuestas;

      // Obtiene el id del jefe
      //$empleado = DB::select('select boss_id, first_name, last_name, email from users WHERE id = ?', [$id_empleado]);
      $empleado = Employee::find($id_empleado);

      //$jefe = DB::select('select first_name, last_name, email from users WHERE id = ?', [$empleado[0]->boss_id]);

      if ($alimentar_propuestas != $autorizar_propuestas){

        if ($autorizar_propuestas == 2 && !empty($empleado->boss)){
      
          // Agrega una nueva nota
          $nota = DB::insert('insert into mensajes (id_plan, id_de, id_para, mensaje, fecha) values (?,?,?,?,?)', [$id_plan, $id_empleado, $empleado->boss->id, 'Solicitud Enviada', date('Y-m-d')]);
        }

        else{

          // Agrega una nueva nota
          $nota = DB::insert('insert into mensajes (id_plan, id_de, id_para, mensaje, fecha) values (?,?,?,?,?)', [$id_plan, $id_empleado, $id_empleado, 'Solicitud Enviada', date('Y-m-d')]);
        }
      }

      /*Mail::send('emails.evaluacion-resultados.mensajes', ['mensaje' => 'Tu colaborador ' . $empleado[0]->first_name . ' ' . $empleado[0]->last_name . ' solicita autorización de su propuesta'], function ($message) use ($empleado, $jefe){
    
        $message->from('evaluacion@abcleasing.com.mx', 'Abc');
        $message->to($jefe[0]->email, $jefe[0]->first_name . ' ' . $jefe[0]->last_name);
		    $message->cc($empleado[0]->email);  
        $message->subject('Abc - Evaluacion de Resultados');
      });*/

      if ($status == 2){

        // Mensaje para avisar
        flash('Solicitud enviada');
      }

      else{

        // Mensaje para avisar
        flash('Objetivos autorizados');
      }
      
      // Guarda en sesion el plan actual
      Session::put('id_plan', $id_plan);

      if ($alimentar_propuestas != 1){

        Session::put('id_empleado', $id_empleado);
      }
      
      // Redirecciona a la pagina de las propuestas
      return redirect('propuesta');
    }

    /**
     * Autoriza o rechaza las propuestas
     *
     * @return void
     */
    public function autorizar(){

      // Id del usuario logueado
      $id_user = auth()->user()->employee_id;

      // Obtiene los datos necesarios
      $id_empleado = $_POST['id_empleado'];
      $id_plan = $_POST['id_plan'];
      $periodo = $_POST['periodo'];
      $status = $_POST['status'];
      $mensaje = '';
	  $body = '';
      
      // Obtiene los datos del perido y plan actual del empleado
      $status_plan = DB::select('select status from status_plan_empleado where id_plan = ? AND id_empleado = ? AND periodo = ?', [$id_plan, $id_empleado, $periodo]);
      
      // Existe algun periodo
      if (!empty($status_plan)){

        // Solicitud Rechazada
        if ($status == 1){

          $nota = 'Solicitud Rechazada';
		  $body = 'rechazo';
          
          // Borra los estatus
          StatusPlanEmpleado::where('id_plan', $id_plan)->where('id_empleado', $id_empleado)->delete();
        }
        
        // Solicitud aprobada
        else{

          // Fecha actual
          $fecha = date('Y-m-d');
          $nota = 'Solicitud Aprobada';
		  $body = 'ha autorizado';
          
          // Actualiza el status y fecha del periodo
          DB::update('update status_plan_empleado set status = ' . $status . ', fecha_autorizado = "' . $fecha . '" WHERE id_plan = ? AND id_empleado = ? AND periodo = ?', [$id_plan, $id_empleado, $periodo]);
        }
      }

      // No existe registro de algun periodo
      else{

        // Crea un nuevo periodo
        DB::insert('insert into status_plan_empleado (id_plan, id_empleado, status, fecha_autorizado) values (?,?,?,?)', [$id_plan, $id_empleado, 2, date('Y-m-d')]);
      }

      // Obtiene el id del jefe
      //$empleado = DB::select('select boss_id, first_name, last_name, email from users WHERE id = ?', [$id_empleado]);
      $empleado = Employee::find($id_empleado);
      //$jefe = DB::select('select first_name, last_name, email from users WHERE id = ?', [$empleado[0]->boss_id]);
      
      // Agrega una nueva nota
      DB::insert('insert into mensajes (id_plan, id_de, id_para, mensaje, fecha) values (?,?,?,?,?)', [$id_plan, $id_user, $id_empleado, $nota, date('Y-m-d')]);
		
	  /*$exception = false;

      if ($status != 1){
		  
		try{

        	Mail::send('emails.evaluacion-resultados.autorizacion-objetivos', ['colaborador' => $empleado->nombre . ' ' . $empleado->paterno . ' ' . $empleado->materno], function ($message) use ($empleado){
    
          		$message->from('soporte@miplataformafinvivir.com');
          		$message->to($empleado->boss->correoempresa, $empleado->boss->nombre . ' ' . $empleado->boss->paterno . ' ' . $empleado->boss->materno);
          		$message->subject('Mídete - Objetivos Autorizados | Plataforma Finvivir');
        	});
		}
		catch(\Exception $e){
    		$exception = true;
		}
      }

      else{
		  
		try{

        	Mail::send('emails.evaluacion-resultados.rechazo-objetivos', ['colaborador' => $empleado->nombre . ' ' . $empleado->paterno . ' ' . $empleado->materno], function ($message) use ($empleado){
    
          		$message->from('soporte@miplataformafinvivir.com');
          		$message->to($empleado->boss->correoempresa, $empleado->boss->nombre . ' ' . $empleado->boss->paterno . ' ' . $empleado->boss->materno);
          		$message->subject('Mídete - Objetivos Rechazados | Plataforma Finvivir');
        	});
		}
		catch(\Exception $e){
    		$exception = true;
		}
      }
		
	  if ($exception){
		  
		  $nota = $nota . '. Pero el correo no fue enviado al jefe ya que por el momento se ha excedido el numero de correos permitidos por el servidor. Espere a la siguiente hora para intentar enviar de nuevo';
	  }*/

      // Muestra el mensaje
      flash($nota);
      
      // Guarda en sesion plan y empleado
      Session::put('id_plan', $id_plan);
      Session::put('id_empleado', $id_empleado);
      
      // Redirecciona a la pagina de autorizacion
      return redirect('autorizacion');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Borra una propuesta
     *
     * @return void
     */
    public function delete(){

        // Existe el id de la propuesta
        if (!empty($_POST['id_propuesta'])){

          // Borra la propuesta
          DB::delete('delete from objetivos WHERE id = ?', [$_POST['id_propuesta']]);

          // Borra los corresponsables
          DB::table('objetivos_corresponsables')->where('id_objetivo', $_POST['id_propuesta'])->delete();

          // Obtiene los nuevos objetivos del periodo actual
          $objetivos = DB::select('SELECT id FROM objetivos where id_plan = ? AND id_empleado = ? AND periodo = ?', [$_POST['id_plan'], $_POST['id_empleado'], $_POST['periodo']]);

          // No hay nuevos objetivos en el periodo actual
          if (count($objetivos) == 0){

            // Cambia el status del periodo actual
            DB::update('UPDATE status_plan_empleado SET status = 3 WHERE id_plan = ? AND id_empleado = ? AND periodo = ?', [$_POST['id_plan'], $_POST['id_empleado'], $_POST['periodo']]);
          }
        }
    }

  /**
     * Remueve la autorización de los objetivos y borra resultados (si es que los hubiera)
     */
    public function resetear_autorizacion(){

      // Id del empleado
      $id_empleado = $_POST['id_empleado'];
      $id_plan = $_POST['id_plan'];

      $user = DB::table('users')->where('employee_id', $id_empleado)->select('id')->first();

      DB::table('logros')->where('id_plan', $id_plan)->where('id_empleado', $id_empleado)->delete();

      DB::table('status_plan_empleado')->where('id_plan', $id_plan)->where('id_empleado', $id_empleado)->delete();
      DB::table('revision_resultados_director')->where('plan_id', $id_plan)->where('user_id', $user->id)->delete();
      Session::put('id_plan', $id_plan);
      Session::put('id_empleado', $id_empleado);
      flash('La autorización de los objetivos fue removida');
      return redirect('propuesta');
    }
}
