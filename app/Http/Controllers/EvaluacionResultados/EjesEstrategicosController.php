<?php

namespace App\Http\Controllers\EvaluacionResultados;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Session;

class EjesEstrategicosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      
      if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){
			
		flash("No tiene permiso para ver la sección administrativa del módulo de Evaluación de Resultados");
		return redirect('/');	
	  }
		
	  $ejes_estrategicos = DB::select('select * from ejes_estrategicos');
      return view('evaluacion-resultados/ejes-estrategicos/index', compact('ejes_estrategicos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
		
	  if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){
			
		flash("No tiene permiso para ver la sección administrativa del módulo de Evaluación de Resultados");
		return redirect('/');	
	  }

      if (!empty($_POST['nombre'])){

        $eje_estrategico = DB::insert('insert into ejes_estrategicos (nombre) values (?)', [$_POST['nombre']]);
        
        if (!empty($eje_estrategico)){

          flash('El eje estratégico fue creado correctamente');
        }

        else{

          flash('Error al crear el eje estratégico. Intente de nuevo');
        }

        return redirect('ejes-estrategicos');
      }

      return view('evaluacion-resultados/ejes-estrategicos/create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id = 0){
		
	  if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){
			
		flash("No tiene permiso para ver la sección administrativa del módulo de Evaluación de Resultados");
		return redirect('/');	
	  }

      if (empty($id)){

        $eje_estrategico = DB::update('update ejes_estrategicos set nombre = "' . $_POST['nombre'] . '" where id = ?', [$_POST['id']]);
        
        //if (!empty($eje_estrategico)){

          flash('El eje estratégico fue guardado correctamente');
        //}

        //else{

          //flash('Error al guardar el eje estratégico. Intente de nuevo');
        //}

        return redirect('ejes-estrategicos');
      }

      $eje_estrategico = DB::select('select * from ejes_estrategicos where id = ?', [$id]);
      return view('evaluacion-resultados/ejes-estrategicos/edit', compact('eje_estrategico'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = 0){
		
	  if (!auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){
			
		flash("No tiene permiso para ver la sección administrativa del módulo de Evaluación de Resultados");
		return redirect('/');	
	  }
      
      if (!empty($_POST['id'])){

        $eje_estrategico = DB::delete('delete from ejes_estrategicos where id = ?', [$_POST['id']]);
        
        if (!empty($eje_estrategico)){

          flash('El eje estratégico fue borrado correctamente');
        }

        else{

          flash('Error al borrar el eje estratégico. Intente de nuevo');
        }

        return redirect('ejes-estrategicos');
      }

      $eje_estrategico = DB::select('select id, nombre from ejes_estrategicos where id = ?', [$id]);
      return view('evaluacion-resultados/ejes-estrategicos/delete', compact('eje_estrategico'));
    }
}
