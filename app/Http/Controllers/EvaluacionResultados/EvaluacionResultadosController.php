<?php

namespace App\Http\Controllers\EvaluacionResultados;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Session;
use App\Employee;

class EvaluacionResultadosController extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Evaluacion Resultados Controller
    |--------------------------------------------------------------------------
    |
    | Add Documentation
    |
    */

    /**
     * Create a new Evaluacion Resultados controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Muestra la pagina de "¿Que es?"
     *
     * @return void
     */
    public function que_es(){

      return view('evaluacion-resultados/que-es-evaluacion-resultados');
    }

    /**
     * Muestra la pagina de Informe Consolidado
     *
     * @return void
     */
    public function informe_consolidado(){

      $users = array();
      $id_jefe = auth()->user()->id;
	  $planes = DB::select("SELECT id, nombre FROM planes WHERE estado = 3 ORDER BY id DESC");
      //$periodos = DB::select("SELECT id, descripcion FROM periodos WHERE status = 'Cerrado' ORDER BY id DESC");

      if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

        // Obtiene los datos de todos los usuarios
        $users = DB::select('SELECT first_name, last_name, id, division, subdivision FROM users ORDER BY id');
      }

      else{

        // Obtiene los datos de todos los subordinados del usuario logueado
        $users = DB::select('SELECT first_name, last_name, id, division, subdivision FROM users WHERE boss_id = ? AND id <> ? ORDER BY users.id', [$id_jefe, $id_jefe]);
      }

      return view('evaluacion-resultados/informe-consolidado', compact('users', 'planes'));
    }

    /**
     * Muestra la pagina de Informe Consolidado
     *
     * @return void
     */
    public function informe_consolidado_individual($id_empleado = 0){

      if (!empty($id_empleado)){

        $id_jefe = auth()->user()->id;
		  
		if (!empty($_POST['id_plan'])){

          Session::put('plan_informe', $_POST['id_plan']);
        }

        /*if (!empty($_POST['id_periodo'])){

          Session::put('periodo_informe', $_POST['id_periodo']);
        }*/
        
        // Obtiene los datos del empleado
        $empleado = DB::select('SELECT id, first_name, last_name, boss_id FROM users WHERE id = ?', [$id_empleado]);

        if (!empty($empleado)){

          $empleado = $empleado[0];
		  $resultados = array();

          if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin') || $empleado->boss_id == $id_jefe){

            if (!empty($_POST['id_plan'])){
				
            	$resultados = DB::select('select resultados_objetivos.resultado, resultados_objetivos.maxima_cal, planes.id, planes.nombre from resultados_objetivos inner join planes on (planes.id = resultados_objetivos.id_plan) WHERE id_empleado = ? AND id_plan = ? ORDER BY id_plan DESC LIMIT 1', [$id_empleado, $_POST['id_plan']]);
            }

            else{
			  
				$resultados = DB::select('select resultados_objetivos.resultado, resultados_objetivos.maxima_cal, planes.id, planes.nombre from resultados_objetivos inner join planes on (planes.id = resultados_objetivos.id_plan) WHERE id_empleado = ? ORDER BY id_plan DESC LIMIT 1', [$id_empleado]);
			}

            if (!empty($resultados)){
				
			  /*$id_periodo = 0;

              if (!empty($_POST['id_periodo'])){

                $id_periodo = $_POST['id_periodo'];
              }

              else{

                flash('No existe registro en la evaluacion de desempeño');
                return redirect()->to('informe-consolidado');
              }
                
              $calificaciones = DB::select('select id_familia_factor, ponderacion, valor_esperado, valor_obtenido from reporte_consolidado_desempeno WHERE id_evaluado = ? AND id_evaluador != ? AND id_periodo = ? ORDER BY id_familia_factor', [$id_empleado, $id_empleado, $id_periodo]);

              if (!empty($calificaciones)){

                $periodo = DB::select('select descripcion from periodos WHERE id = ?', [$id_periodo]);
                $nombre_periodo = $periodo[0]->descripcion;*/
				$notas = DB::select('select recomendacion, created_at from recomendaciones_informe WHERE id_empleado = ? AND boss_id = ? AND id_plan = ? ORDER BY created_at DESC', [$id_empleado, $id_jefe, $resultados[0]->id]);
              /*}

              else{

                flash('No existe registro en la evaluacion de desempeño');
                return redirect()->to('informe-consolidado');
              }*/ 
            }

            else{

              flash(utf8_encode('No existe registro en la evaluacion de resultados'));
              return redirect()->to('informe-consolidado');
            }
          }

          else{

            flash(utf8_encode('El empleado no es subordinado suyo'));
            return redirect()->to('informe-consolidado');
          }
        }

        else{

          flash(utf8_encode('El empleado no existe'));
          return redirect()->to('informe-consolidado');
        }
      }

      else{

        flash(utf8_encode('El id del empleado es incorrecto'));
        return redirect()->to('informe-consolidado');
      }

      return view('evaluacion-resultados/informe-consolidado-individual', compact('empleado', 'resultados', 'notas'));
    }

    /**
     * Muestra la pagina de la propuesta
     *
     * @return void
     */
		public function propuesta(){

      $planes = array();

      if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

        // Obtiene todos los planes preparatorios y abiertos ordenados por los mas recientes
        $planes = DB::select('SELECT id, nombre, anio, tipo, alimentar_propuestas, autorizar_propuestas, periodo, total_peso_objetivos FROM planes WHERE estado = ? OR estado = ? ORDER BY id DESC', [1, 2]);
      }

      else{
      
        // Obtiene todos los planes abiertos ordenados por los mas recientes
        $planes = DB::select('SELECT id, nombre, anio, tipo, alimentar_propuestas, autorizar_propuestas, periodo, total_peso_objetivos FROM planes WHERE estado = ? ORDER BY id DESC', [2]);
      }

      $user = $objetivos = $notas = array();
      $alimentar_propuestas = 1;
	    $id_user = 0;
	    $periodo = '';
	    $peso_periodo = 0;
      $id_plan = 0;
		  $status = 1;
		  $editar_objetivos = false;
		  $mostrar_boton_aceptar = false;
      $users = $users_groups = $objetivos_corporativos = $objetivos_corporativos_sin_responsable = array();
      $status_plan = $mensajes = $responsabilities = $objetivos_corresponsables = $user = array();
      $ejes_estrategicos = $catalogo_objetivos = $bono_plan = $plan = $planes_para_copiar = $users_with_objetives = $users_plans = array();

      // Hay planes abiertos
      if (!empty($planes)){

        // Obtiene el id del usuario logueado
        $id_user = auth()->user()->employee_id;

        if (!empty($_POST['id_empleado']) || Session::has('id_empleado')){

          if (!empty($_POST['id_empleado'])){

            $id_user = $_POST['id_empleado'];
          }

          else{

            $id_user = Session::get('id_empleado');
            Session::forget('id_empleado');
          }
        }
        
        // Obtiene el plan abierto mas reciente
        $plan = $planes[0];

        // Se requiere mostrar un plan por default
        if (!empty($_POST['id_plan']) || Session::has('id_plan')){

          if (!empty($_POST['id_plan'])){

            $id_plan = $_POST['id_plan'];
            Session::put('id_plan', $id_plan);
          }

          else{

            $id_plan = Session::get('id_plan');
            Session::forget('id_plan');
          }

          foreach ($planes as $key => $value){
            
            if ($id_plan == $value->id){

              $plan = $value;
            }
          }
        }

        $notas = DB::select("SELECT id_user, mensaje, notas.created_at, nombre, paterno, materno FROM notas INNER JOIN employees ON (employees.id = notas.id_user) WHERE id_plan = ? ORDER BY notas.created_at DESC", [$plan->id]);
        $users_groups = DB::select("SELECT id AS data, nombre AS value FROM grupos_usuarios");
        //$users = DB::select("SELECT id AS data, CONCAT(first_name, last_name) AS value FROM users");
        $users = Employee::select('id AS data', DB::raw('CONCAT(nombre, " ", paterno, " ", materno) AS value'))->get();
        $ids_responsabilities = array();
        $responsabilidades = DB::select("SELECT responsabilidades_compromisos.id, responsabilidades_compromisos.id_responsable, responsabilidades_compromisos.id_capturista, responsabilidades_compromisos.id_objetivo_corporativo, objetivos_corporativos.id_tipo FROM responsabilidades_compromisos INNER JOIN objetivos_corporativos ON (responsabilidades_compromisos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE responsabilidades_compromisos.id_plan = ?", [$plan->id]);

        if (!empty($id_user)){

          foreach ($responsabilidades as $key => $responsability){

            $found = false;
          
            if ($responsability->id_responsable == $id_user || $responsability->id_capturista == $id_user){

              $ids_responsabilities[] = $responsability->id;
              continue;
            }

            $corresponsables = DB::select("SELECT id_corresponsable, tipo FROM responsabilidades_corresponsables WHERE id_responsabilidad = ?", [$responsability->id]);

            foreach ($corresponsables as $key2 => $corresponsable){
            
              if ($corresponsable->tipo == 'persona'){

                if ($corresponsable->id_corresponsable == $id_user){

                  $ids_responsabilities[] = $responsability->id;
                  break;
                }
              }

              else{

                $elementos = DB::select("SELECT id_elemento, tipo_elemento FROM elementos_grupo WHERE id_grupo = ?", [$corresponsable->id_corresponsable]);

                foreach ($elementos as $key3 => $element){
                
                  if ($element->tipo_elemento == 'Persona'){

                    if ($element->id_elemento == $id_user){

                      $ids_responsabilities[] = $responsability->id;
                      $found = true;
                      break;
                    }
                  }

                  else{

                    if ($element->tipo_elemento == 'Departamento'){

                      //$area = DB::select("SELECT nombre FROM areas WHERE id = ?", [$element->id_elemento]);
                      //$usuarios = DB::table('users')->where('division', $area[0]->nombre)->pluck('id');
                      //$usuarios = Personal::where('departamento', $area[0]->nombre)->pluck('id');
                      $usuarios = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->where('departments.id', $element->id_elemento)->pluck('employees.id')->toArray();
                      //$usuarios = (array) $usuarios;

                      if (in_array($id_user, $usuarios)){

                        $ids_responsabilities[] = $responsability->id;
                        break;
                      }
                    }

                    else{

                      $elementos_internos = DB::select("SELECT id_elemento, tipo_elemento FROM elementos_grupo WHERE id_grupo = ?", [$element->id_elemento]);

                      foreach ($elementos_internos as $key4 => $element_interno){
                
                        if ($element_interno->tipo_elemento == 'Persona'){

                          if ($element_interno->id_elemento == $id_user){

                            $ids_responsabilities[] = $responsability->id;
                            break;
                          }
                        }

                        else{

                          if ($element_interno->tipo_elemento == 'Departamento'){

                            //$area = DB::select("SELECT nombre FROM areas WHERE id = ?", [$element_interno->id_elemento]);
                            //$usuarios = Personal::where('departamento', $area[0]->nombre)->pluck('id');
                            $usuarios = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->where('departments.id', $element_interno->id_elemento)->pluck('employees.id')->toArray();
                            //$usuarios = (array) $usuarios;

                            if (in_array($id_user, $usuarios)){

                              $ids_responsabilities[] = $responsability->id;
                              break;
                            }
                          }
                        }
                      }

                      if ($found){

                        break;
                      }
                    }
                  }
                }

                if ($found){

                  break;
                }
              }
            }
          }

          if (!empty($ids_responsabilities)){

            $ids_responsabilities = implode(',', $ids_responsabilities);
            $responsabilities = DB::select("SELECT responsabilidades_compromisos.*, objetivos_corporativos.id_tipo FROM responsabilidades_compromisos INNER JOIN objetivos_corporativos ON (responsabilidades_compromisos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE responsabilidades_compromisos.id IN ($ids_responsabilities) ORDER BY tipos_objetivos_corporativos.id, responsabilidades_compromisos.id_objetivo_corporativo, responsabilidades_compromisos.tipo_indicador");
          }

          $alimentar_propuestas = $plan->alimentar_propuestas;

          // Obtiene los datos del usuario
          //$user = DB::select('SELECT first_name, last_name, division, subdivision, boss_id FROM users WHERE id = ?', [$id_user]);
          $user = Employee::find($id_user);
		  
		      if ($plan->tipo == 2){
			
			     $cambiar_valores_periodo = DB::select("SELECT estado FROM cambiar_valores_periodos WHERE id_empleado = ? AND id_plan = ?", [$id_user, $plan->id]);
			
			      if (count($cambiar_valores_periodo) > 0){
				  
				      $user->cambiar_valores = $cambiar_valores_periodo[0]->estado;
			      }
		      }
		  
		      $periodos_status_plan = DB::select('SELECT status, fecha_solicitado, fecha_autorizado, fecha_registrado, fecha_revisado, periodo FROM status_plan_empleado where id_plan = ? AND id_empleado = ? ORDER BY periodo', [$plan->id, $id_user]);
		      $periodo = 1;
		  
		      // Hay periodos en el plan del usuario
          if (!empty($periodos_status_plan)){
			
			      $periodo = $periodos_status_plan[count($periodos_status_plan) - 1]->periodo;
		      }
			
		      // Obtiene los objetivos o propuestas del usuario
          //$objetivos = DB::select('SELECT * FROM objetivos WHERE id_plan = ? AND id_empleado = ? AND viejo != ?', [$plan->id, $id_user, 1]);
          $objetivos = DB::select("SELECT objetivos.*, objetivos_corporativos.id_tipo, catalogo_objetivos.objetivo AS nombre_catalogo, catalogo_objetivos.peso AS peso_catalogo, catalogo_objetivos.id_objetivo_corporativo AS objetivo_catalogo, catalogo_objetivos.frecuencia AS frecuencia_catalogo, catalogo_objetivos.unidad AS tipo_catalogo, catalogo_objetivos.meta AS meta_catalogo, catalogo_objetivos.valor_rojo AS valor_rojo_catalogo, catalogo_objetivos.valor_verde As valor_verde_catalogo FROM objetivos INNER JOIN objetivos_corporativos ON (objetivos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) LEFT JOIN catalogo_objetivos ON (catalogo_objetivos.id = objetivos.id_catalogo) WHERE objetivos.id_plan = ? AND id_empleado = ?  AND viejo != ?", [$plan->id, $id_user, 1]);
		  
		      $periodos_status_plan = DB::select('SELECT status, fecha_solicitado, fecha_autorizado, fecha_registrado, fecha_revisado, periodo FROM status_plan_empleado where id_plan = ? AND id_empleado = ? ORDER BY periodo', [$plan->id, $id_user]);
        
          // Hay periodos en el plan del usuario
          if (!empty($periodos_status_plan)){

            // Obtiene el status del ultimo periodo del plan del empleado
            $status_plan = $periodos_status_plan[count($periodos_status_plan) - 1];

            // Si el ultimo periodo ya tiene fecha de revision de logros
            if (!empty($status_plan->fecha_revisado) && $status_plan->fecha_revisado != '0000-00-00'){

              // El ultimo periodo no es el ultimo del año
              if ($status_plan->periodo < 12){

                $band = false;

                foreach ($objetivos as $key => $value){

                  if ($value->frecuencia == 'Mensual'){

                    $band = true;
                    break;
                  }

                  else{

                    if ($value->frecuencia == 'Semestral' && ($status_plan->periodo * 6) < 12){

                      $band = true;
                      break;
                    }

                    else{

                      if ($value->frecuencia == 'Trimestral' && ($status_plan->periodo * 3) < 12){

                        $band = true;
                        break;
                      }

                      else{

                        if ($value->frecuencia == 'Cuatrimestral' && ($status_plan->periodo * 4) < 12){

                          $band = true;
                          break;
                        }
                      }
                    } 
                  }
                }

                if ($band){

                  // Verificar si existen cambios de puesto pendientes
                  $cambios_puesto = DB::select('SELECT id FROM cambios_de_puesto WHERE id_empleado = ?', [$id_user]);

                  if (count($cambios_puesto) > 0){

                    $id = $cambios_puesto[0]->id;

                    // Se inserta un registro para un nuevo periodo con status para agregar nuevos objetivos
                    DB::insert('INSERT INTO status_plan_empleado (id_plan, id_empleado, status, periodo) VALUES (?,?,?,?)', [$plan->id, $id_user, 1, $status_plan->periodo + 1]);

                    // Se vuelven viejos los objetivos actuales
                    DB::update('UPDATE objetivos SET viejo = 1 WHERE id_plan = ? AND id_empleado = ?', [$plan->id, $id_user]);

                    // Borra el registro de cambio de puesto
                    DB::delete('DELETE FROM cambios_de_puesto WHERE id = ?', [$id]);
                  }

                  else{

                    // Se inserta un registro para un nuevo periodo
                    DB::insert('INSERT INTO status_plan_empleado (id_plan, id_empleado, status, fecha_solicitado, fecha_autorizado, periodo) VALUES (?,?,?,?,?,?)', [$plan->id, $id_user, 3, $status_plan->fecha_solicitado, $status_plan->fecha_autorizado, $status_plan->periodo + 1]);
					
                  }
				  
				          $periodo = $status_plan->periodo + 1;
				  
				          // Obtiene los objetivos o propuestas del usuario
                  //$objetivos = DB::select('SELECT * FROM objetivos WHERE id_plan = ? AND id_empleado = ? AND viejo != ?', [$plan->id, $id_user, 1]);
                  $objetivos = DB::select("SELECT objetivos.*, objetivos_corporativos.id_tipo, catalogo_objetivos.objetivo AS nombre_catalogo, catalogo_objetivos.peso AS peso_catalogo, catalogo_objetivos.id_objetivo_corporativo AS objetivo_catalogo, catalogo_objetivos.frecuencia AS frecuencia_catalogo, catalogo_objetivos.unidad AS tipo_catalogo, catalogo_objetivos.meta AS meta_catalogo, catalogo_objetivos.valor_rojo AS valor_rojo_catalogo, catalogo_objetivos.valor_verde As valor_verde_catalogo FROM objetivos INNER JOIN objetivos_corporativos ON (objetivos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) LEFT JOIN catalogo_objetivos ON (catalogo_objetivos.id = objetivos.id_catalogo) WHERE objetivos.id_plan = ? AND id_empleado = ?  AND viejo != ?", [$plan->id, $id_user, 1]);
                }
			        }
            }
          }  
		  
		      // Obtiene los datos de status en que se encuentra el plan del usuario
          $status_plan = DB::select('SELECT status, fecha_solicitado, fecha_autorizado, fecha_revisado, periodo, status FROM status_plan_empleado WHERE id_plan = ? AND id_empleado = ? ORDER BY periodo DESC LIMIT 1', [$plan->id, $id_user]);

          if (empty($status_plan) || $status_plan[0]->status < 3){

            // Borra el registro de cambio de puesto
            DB::delete('DELETE FROM cambios_de_puesto WHERE id_empleado = ?', [$id_user]);
          }
		  
		      if (!empty($status_plan)){

            $periodo = $status_plan[0]->periodo;
		  		  $status = $status_plan[0]->status;
				
				    if ($alimentar_propuestas == 2 && $id_user == auth()->user()->employee_id && $status > 2){

              $user_accept = DB::select("SELECT fecha FROM user_accepts WHERE id_plan = ? AND id_empleado = ? AND tipo = ?", [$plan->id,$id_user,'Objetivos']);

              if (count($user_accept) == 0){

                $fecha = $status_plan[0]->fecha_autorizado;
                $fecha_actual = date('Y-m-d');
                $days = (strtotime($fecha_actual) - strtotime($fecha)) / (60 * 60 * 24);

                if ($days < 8){

                  $mostrar_boton_aceptar = true;
                }
              }
            }
				  }

          $planes_para_copiar = array();

          if ((empty($status_plan) || $status_plan[0]->status == 1) && ($alimentar_propuestas == 1 || ($alimentar_propuestas == 2 && $id_user != auth()->user()->employee_id) || ($alimentar_propuestas == 3 && auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')))){

            $planes_para_copiar = DB::table('objetivos')->join('planes', 'planes.id', '=', 'objetivos.id_plan')->where('planes.id', '<>', $plan->id)->where('estado', '<>', 1)->where('estado', '<>', 5)->select('planes.id')->groupBy('planes.id')->pluck('planes.id')->toArray();
            $users_with_objetives = DB::table('objetivos')->whereIn('id_plan', $planes_para_copiar)->select('id_empleado')->groupBy('id_empleado')->pluck('id_empleado')->toArray();
            $users_with_objetives = Employee::whereIn('id', $users_with_objetives)->select('id AS data', DB::raw('CONCAT(nombre, " ", paterno, " ", materno) AS value'))->get();
            $users_plans = DB::table('objetivos')->whereIn('id_plan', $planes_para_copiar)->select('id_empleado', 'id_plan')->groupBy('id_empleado', 'id_plan')->get();
            $planes_para_copiar = DB::table('planes')->whereIn('id', $planes_para_copiar)->select('id', 'nombre')->get();
          }

          if (count($objetivos) == 0 && empty($status_plan)){

            $editar_objetivos = true;
          }

          $nuevos_objetivos = array();
          $planes_para_copiar = array();
		  
		      foreach ($objetivos as $key => $objetivo){

            if ($objetivo->periodo >= $periodo){

              $peso_periodo += $objetivo->peso;
            }
			
		        $logros = DB::select("SELECT revisado FROM logros WHERE objetivo_id = ? ORDER BY periodo DESC LIMIT 1", [$objetivo->id]);
		        /*$valores_objetivo = DB::select("SELECT periodo, valor_rojo, valor_verde, meta FROM valores_objetivo WHERE id_objetivo = ? ORDER BY periodo", [$objetivo->id]);
		  
		        if (count($valores_objetivo) > 0){

			        $objetivo->valores = $valores_objetivo;
            }*/
				
		        if (empty($status_plan) || (count($logros) == 0 && ($status_plan[0]->status == 1 || ($status_plan[0]->status == 3 && !empty($user->cambiar_valores) && $user->cambiar_valores == 1)))){
			  
			        $objetivo->editar = true;
			        $editar_objetivos = true;
		        }
				
		        else{

          	  if (!empty($status_plan) && ($status_plan[0]->status == 1 || $status_plan[0]->status == 3) && $plan->tipo == 2 && !empty($user->cambiar_valores) && $user->cambiar_valores == 1 && $objetivo->tipo != 'Si/No' && count($logros) > 0 && $logros[0]->revisado == 1){

				        $objetivo->cambiar_valores = true;
          	  }
		        }

            $nuevos_objetivos[] = $objetivo;
          }

          $objetivos = $nuevos_objetivos;

          $ids_objetivos = array();
          $objetivos_corresponsable = DB::select("SELECT objetivos.id FROM objetivos INNER JOIN objetivos_corporativos ON (objetivos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE objetivos.id_plan = ? AND id_responsable = ? ORDER BY objetivos.id", [$plan->id, 0]);

          foreach ($objetivos_corresponsable as $key => $objetivo){

            $found = false;
            $corresponsables = DB::select("SELECT id_corresponsable, tipo FROM objetivos_corresponsables WHERE id_objetivo = ?", [$objetivo->id]);

            foreach ($corresponsables as $key2 => $corresponsable){
            
              if ($corresponsable->tipo == 'persona'){

                if ($corresponsable->id_corresponsable == $id_user){

                  $ids_objetivos[] = $objetivo->id;
                  break;
                }
              }

              else{

                $elementos = DB::select("SELECT id_elemento, tipo_elemento FROM elementos_grupo WHERE id_grupo = ?", [$corresponsable->id_corresponsable]);

                foreach ($elementos as $key3 => $element){
                
                  if ($element->tipo_elemento == 'Persona'){

                    if ($element->id_elemento == $id_user){

                      $ids_objetivos[] = $objetivo->id;
                      $found = true;
                      break;
                    }
                  }

                  else{

                    if ($element->tipo_elemento == 'Departamento'){

                      //$area = DB::select("SELECT nombre FROM areas WHERE id = ?", [$element->id_elemento]);
                      $usuarios = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->where('departments.id', $element->id_elemento)->pluck('employees.id')->toArray();
                      //$usuarios = Personal::where('departamento', $area[0]->nombre)->pluck('id');
                      //$usuarios = (array) $usuarios;

                      if (in_array($id_user, $usuarios)){

                        $ids_objetivos[] = $objetivo->id;
                        break;
                      }
                    }

                    else{

                      $elementos_internos = DB::select("SELECT id_elemento, tipo_elemento FROM elementos_grupo WHERE id_grupo = ?", [$element->id_elemento]);

                      foreach ($elementos_internos as $key4 => $element_interno){
                
                        if ($element_interno->tipo_elemento == 'Persona'){

                          if ($element_interno->id_elemento == $id_user){

                            $ids_objetivos[] = $objetivo->id;
                            break;
                          }
                        }

                        else{

                          if ($element_interno->tipo_elemento == 'Departamento'){

                            //$area = DB::select("SELECT nombre FROM areas WHERE id = ?", [$element_interno->id_elemento]);
                            $usuarios = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->where('departments.id', $element_interno->id_elemento)->pluck('employees.id')->toArray();
                            //$usuarios = Personal::where('departamento', $area[0]->nombre)->pluck('id');
                            //$usuarios = (array) $usuarios;

                            if (in_array($id_user, $usuarios)){

                              $ids_objetivos[] = $objetivo->id;
                              break;
                            } 
                          }
                        }
                      }

                      if ($found){

                        break;
                      }
                    }
                  }
                }

                if ($found){

                  break;
                }
              }
            }
          }

          if (!empty($ids_objetivos)){

            $ids_objetivos = implode(',', $ids_objetivos);
            $objetivos_corresponsables = DB::select("SELECT objetivos.*, objetivos_corporativos.id_tipo FROM objetivos INNER JOIN objetivos_corporativos ON (objetivos.id_objetivo_corporativo = objetivos_corporativos.id) WHERE objetivos.id IN ($ids_objetivos) ORDER BY objetivos.id");
          }

          $temp_responsabilities = array();

          foreach ($responsabilities as $key => $value){

            $corresponsables = array();
            $grupos_corresponsables = array();
            $results = DB::select("SELECT id_corresponsable, tipo FROM responsabilidades_corresponsables WHERE id_responsabilidad = ?", [$value->id]);

            foreach ($results as $key2 => $value2){
            
              // El tipo de corresponsables es individual (no es grupo)
              if ($value2->tipo == 'persona'){

                // Se agrega a la lista de corresponsables
                $corresponsables[] = $value2->id_corresponsable;
              }

              // El tipo de corresponsables es un grupo (varias personas)
              else{

                // Se agrega a la lista de corresponsables de tipo grupo
                $grupos_corresponsables[] = $value2->id_corresponsable;
              }
            }

            $value->corresponsables = $corresponsables;
            $value->grupos_corresponsables = $grupos_corresponsables;
            $temp_responsabilities[] = $value;
          }

          $responsabilities = $temp_responsabilities;
          $temp_objetives = array();
          $ids_objetivos = array();

          foreach ($objetivos as $key => $value){

            $ids_objetivos[] = $value->id;
            $corresponsables = array();
            $grupos_corresponsables = array();
            $results = DB::select("SELECT id_corresponsable, tipo FROM objetivos_corresponsables WHERE id_objetivo = ?", [$value->id]);

            foreach ($results as $key2 => $value2){
            
              // El tipo de corresponsables es individual (no es grupo)
              if ($value2->tipo == 'persona'){

                // Se agrega a la lista de corresponsables
                $corresponsables[] = $value2->id_corresponsable;
              }

              // El tipo de corresponsables es un grupo (varias personas)
              else{

                // Se agrega a la lista de corresponsables de tipo grupo
                $grupos_corresponsables[] = $value2->id_corresponsable;
              }
            }

            $bonos = DB::table('bonos_objetivos')->where('id_objetivo', $value->id)->get();
            $value->corresponsables = $corresponsables;
            $value->grupos_corresponsables = $grupos_corresponsables;
            $value->bonos = $bonos;
            $temp_objetives[] = $value;
          }

          $objetivos = $temp_objetives;
          $temp_objetives = array();

          foreach ($objetivos_corresponsables as $key => $value){

            if (in_array($value->id, $ids_objetivos)){

              continue;
            }

            $corresponsables = array();
            $grupos_corresponsables = array();
            $results = DB::select("SELECT id_corresponsable, tipo FROM objetivos_corresponsables WHERE id_objetivo = ?", [$value->id]);

            foreach ($results as $key2 => $value2){
            
              // El tipo de corresponsables es individual (no es grupo)
              if ($value2->tipo == 'persona'){

                // Se agrega a la lista de corresponsables
                $corresponsables[] = $value2->id_corresponsable;
              }

              // El tipo de corresponsables es un grupo (varias personas)
              else{

                // Se agrega a la lista de corresponsables de tipo grupo
                $grupos_corresponsables[] = $value2->id_corresponsable;
              }
            }

            $value->corresponsables = $corresponsables;
            $value->grupos_corresponsables = $grupos_corresponsables;
            $temp_objetives[] = $value;
          }

          $objetivos_corresponsables = $temp_objetives;
        
          // Obtiene los diferentes ejes estrategicos en los que puede clasificarse una propuesta
          //$ejes_estrategicos = DB::select('select * from ejes_estrategicos');

            // Obtiene los mensajes o notas del usuario
            //if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

              $mensajes = DB::select('SELECT nombre, paterno, materno, mensaje, mensajes.fecha, mensajes.id FROM mensajes INNER JOIN employees ON (employees.id = mensajes.id_de) WHERE id_plan = ? AND (mensajes.id_de = ? OR mensajes.id_para = ?) ORDER BY mensajes.id DESC', [$plan->id, $id_user, $id_user]);
            /*}

            else{

              $mensajes = DB::select('SELECT nombre, paterno, materno, mensaje, mensajes.fecha FROM mensajes INNER JOIN employees ON (employees.id = mensajes.id_de) where id_plan = ? AND ((mensajes.id_de = ? AND mensajes.id_para = ?) OR (mensajes.id_para = ? AND mensajes.id_de = ?)) ORDER BY mensajes.id DESC', [$plan->id, $id_user, auth()->user()->employee->id, $id_user, auth()->user()->employee->id]);
            }*/

          $objetivos_corporativos = DB::select("SELECT objetivos_corporativos.id, objetivos_corporativos.name, objetivos_corporativos.id_tipo, tipos_objetivos_corporativos.name AS nombre_tipo, tipos_objetivos_corporativos.letter, tipos_objetivos_corporativos.color FROM objetivos_corporativos INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE id_plan = ? ORDER BY objetivos_corporativos.id_tipo, objetivos_corporativos.id", [$plan->id]);

          if (count($objetivos_corporativos) == 0){

            $objetivos_corporativos = DB::select("SELECT objetivos_corporativos.id, objetivos_corporativos.name, objetivos_corporativos.id_tipo, tipos_objetivos_corporativos.name AS nombre_tipo, tipos_objetivos_corporativos.letter, tipos_objetivos_corporativos.color FROM objetivos_corporativos INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) ORDER BY objetivos_corporativos.id_tipo, objetivos_corporativos.id");
          }

          $objetivos_corporativos_sin_responsable = DB::select("SELECT objetivos_corporativos.id, id_tipo, letter FROM objetivos_corporativos INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) ORDER BY objetivos_corporativos.id_tipo, objetivos_corporativos.id");
        }

        else{

          $user = auth()->user();
          $user->nombre = $user->first_name;
          $user->paterno = $user->last_name;
          $user->materno = '';
        }

        //if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

          $catalogo_objetivos = DB::table('catalogo_objetivos')->where('direccion', '')->orWhere(function($q) use($user){$q->whereNull('direccion')->orWhere('direccion', $user->direccion);})->orderBy('objetivo')->get();
        //}

        $bono_plan = DB::table('bonos_planes')->where('id_plan', $plan->id)->where('id_user', $id_user)->first();  
      }

      $responsabilities = $objetivos_corresponsables = array();
      return view('evaluacion-resultados/propuesta', compact('plan', 'planes', 'id_user', 'user', 'objetivos', 'objetivos_corporativos', 'objetivos_corporativos_sin_responsable', 'objetivos_corresponsables', 'mensajes', 'users', 'users_groups', 'responsabilities', 'notas', 'status_plan', 'alimentar_propuestas', 'periodo', 'peso_periodo', 'editar_objetivos', 'planes_para_copiar', 'mostrar_boton_aceptar', 'catalogo_objetivos', 'bono_plan', 'users_with_objetives', 'users_plans'));
    }

		/**
     * Muestra la pagina de autorizacion o rechace de propuestas
     *
     * @return void
     */
    public function autorizacion(){
    	
      // Obtiene el id del usuario logueado
      $id_user = auth()->user()->employee_id;

      $user_logged = Employee::find($id_user);
      $users_groups = $usuarios = $bono_plan = $planes = $notas = array();

      // Se obtiene el id del usuario a autorizarle las propuestas
      if (!empty($_POST['id_empleado']) || !empty(Session::get('id_empleado'))){

        if (!empty($_POST['id_empleado'])){

          $id_user = $_POST['id_empleado'];
        }

        else{

          $id_user = Session::get('id_empleado');
          Session::forget('id_empleado');
        }
      }

      if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

        // Obtiene todos los planes preparatorios y abiertos ordenados por los mas recientes
        $planes = DB::select('SELECT id, nombre, anio, tipo, alimentar_propuestas, autorizar_propuestas FROM planes WHERE estado = ? OR estado = ? ORDER BY id DESC', [1, 2]);
      }

      else{

        // Obtiene todos los planes abiertos ordenados por los mas recientes
        $planes = DB::select('SELECT id, nombre, anio, tipo, alimentar_propuestas, autorizar_propuestas FROM planes WHERE estado = ? ORDER BY id DESC', [2]);
      }

      $user = $objetivos = $objetivos_corporativos_sin_responsable = array();
      $alimentar_propuestas = 1;
	    $periodo = '';

      // Hay planes abiertos
      if (!empty($planes)){

        // Obtiene el plan abierto mas reciente
        $plan = $planes[0];

        // Se requiere mostrar un plan por default
        if (!empty($_POST['id_plan']) || !empty(Session::get('id_plan'))){

          if (!empty($_POST['id_plan'])){

            $id_plan = $_POST['id_plan'];
            Session::put('id_plan', $id_plan);
          }

          else{

            $id_plan = Session::get('id_plan');
            Session::forget('id_plan');
          }

          foreach ($planes as $key => $value){
            
            if ($id_plan == $value->id){

              $plan = $value;
            }
          }
        }

        $alimentar_propuestas = $plan->alimentar_propuestas;

        if (!empty($id_user)){

          // Obtiene los datos del usuario
          //$user = DB::select('SELECT first_name, last_name, boss_id, division, subdivision FROM users WHERE id = ?', [$id_user]);
          $user = Employee::find($id_user);
		  
		      // Obtiene los datos de status en que se encuentra el plan del usuario
          //$status_plan = DB::select('SELECT status, fecha_autorizado, fecha_registrado, periodo FROM status_plan_empleado WHERE id_plan = ? AND id_empleado = ? ORDER BY periodo DESC LIMIT 1', [$plan->id, $id_user]);
          $status_plan = DB::table('status_plan_empleado')->where('id_plan', $plan->id)->where('id_empleado', $id_user)->select('status', 'fecha_autorizado', 'fecha_registrado', 'periodo')->orderBy('periodo', 'DESC')->limit(1)->get();

          if (count($status_plan) == 0 || $status_plan[0]->status < 3){

            // Borra el registro de cambio de puesto
            DB::delete('DELETE FROM cambios_de_puesto WHERE id_empleado = ?', [$id_user]);
          }
		  
		      $periodo = 1;
		  
		      if (count($status_plan) > 0){
			
			      $periodo = $status_plan[0]->periodo;
		      }
        
          // Obtiene los objetivos o propuestas del usuario
          //$objetivos = DB::select('SELECT * FROM objetivos WHERE id_plan = ? AND id_empleado = ? AND viejo != ?', [$plan->id, $id_user, 1]);
          $objetivos = DB::select("SELECT objetivos.*, objetivos_corporativos.id_tipo FROM objetivos INNER JOIN objetivos_corporativos ON (objetivos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE objetivos.id_plan = ? AND id_empleado = ? AND viejo != ?", [$plan->id, $id_user, 1]);
		  
		      //if ($plan->tipo == 2){
		  
			      /*$nuevos_objetivos = array();
		  
			      foreach ($objetivos as $key => $objetivo){

        		  if ($objetivo->tipo != 'Si/No'){
				
					      $valores_objetivo = DB::select("SELECT meta, valor_rojo, valor_verde, periodo FROM valores_objetivo WHERE id_objetivo = ? ORDER BY periodo", [$objetivo->id]);

                if (count($valores_objetivo) > 0){

              	  $objetivo->valores = $valores_objetivo;
                }
				      }

          	  $nuevos_objetivos[] = $objetivo;
        	  }

        	  $objetivos = $nuevos_objetivos;*/
		      //}

          $temp_objetives = array();

          foreach ($objetivos as $key => $value){

            $corresponsables = array();
            $grupos_corresponsables = array();
            $results = DB::select("SELECT id_corresponsable, tipo FROM objetivos_corresponsables WHERE id_objetivo = ?", [$value->id]);

            foreach ($results as $key2 => $value2){
            
              // El tipo de corresponsables es individual (no es grupo)
              if ($value2->tipo == 'persona'){

                // Se agrega a la lista de corresponsables
                $corresponsables[] = $value2->id_corresponsable;
              }

              // El tipo de corresponsables es un grupo (varias personas)
              else{

                // Se agrega a la lista de corresponsables de tipo grupo
                $grupos_corresponsables[] = $value2->id_corresponsable;
              }
            }

            $bonos = DB::table('bonos_objetivos')->where('id_objetivo', $value->id)->get();
            $value->corresponsables = $corresponsables;
            $value->grupos_corresponsables = $grupos_corresponsables;
            $value->bonos = $bonos;
            $temp_objetives[] = $value;
          }

          $objetivos = $temp_objetives;
        
          // Obtiene los diferentes ejes estrategicos en los que puede clasificarse una propuesta
          //$ejes_estrategicos = DB::select('select * from ejes_estrategicos');
          $objetivos_corporativos_sin_responsable = DB::select("SELECT objetivos_corporativos.id, id_tipo, letter FROM objetivos_corporativos INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) ORDER BY objetivos_corporativos.id_tipo, objetivos_corporativos.id");
          $users_groups = DB::select("SELECT id AS data, name AS value FROM grupos_usuarios");
          //$usuarios = DB::select("SELECT id AS data, CONCAT(first_name, last_name) AS value FROM users");
          $usuarios = Employee::select('id AS data', DB::raw('CONCAT(nombre, " ", paterno, " ", materno) AS value'))->get();

          // Obtiene los mensajes o notas del usuario
            //if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

              $notas = DB::select('SELECT nombre, paterno, materno, mensaje, mensajes.fecha, mensajes.id FROM mensajes INNER JOIN employees ON (employees.id = mensajes.id_de) WHERE id_plan = ? AND (mensajes.id_de = ? OR mensajes.id_para = ?) ORDER BY mensajes.id DESC', [$plan->id, $id_user, $id_user]);
            /*}

            else{

              $notas = DB::select('SELECT nombre, paterno, materno, mensaje, mensajes.fecha, mensajes.id FROM mensajes INNER JOIN employees ON (employees.id = mensajes.id_de) where id_plan = ? AND ((mensajes.id_de = ? AND mensajes.id_para = ?) OR (mensajes.id_para = ? AND mensajes.id_de = ?)) ORDER BY mensajes.id DESC', [$plan->id, $id_user, auth()->user()->employee->id, $id_user, auth()->user()->employee->id]);
            }*/
        
          
        }

        $bono_plan = DB::table('bonos_planes')->where('id_plan', $plan->id)->where('id_user', $id_user)->first();  
      }

      return view('evaluacion-resultados/autorizacion', compact('plan', 'planes', 'id_user', 'user', 'objetivos', 'objetivos_corporativos_sin_responsable', 'notas', 'status_plan', 'alimentar_propuestas', 'periodo', 'users_groups', 'usuarios', 'user_logged', 'bono_plan'));
    }

		/**
     * Muestra la pagina para registrar los logros de las propuestas
     *
     * @return void
     */
    public function registroLogros(){

      // Obtiene todos los planes abiertos ordenados por los mas recientes
      $planes = DB::select('SELECT id, nombre, anio, alimentar_propuestas, registrar_logros, autorizar_logros, periodo FROM planes where estado = ? order by id DESC', [2]);
      $user = $objetivos = array();
      $periodos_status_plan = array();
      $status_plan = array();
      $alimentar_propuestas = 1;
	    $id_user = $user_id = 0;
	    $periodo = '';
	    $mostrar_boton_aceptar = false;
	    $cambiar_valores = false;
      $responsabilities = $notas = $plan = array();
      $objetivos_corporativos = $objetivos_corresponsables = array();
      $mensajes = array();
      $mes_a_registrar = 13;
      $fecha_solicitado = null;
      $fecha_autorizado = null;
      $revisado_por_director = array();

      // Hay planes abiertos
      if (!empty($planes)){

        // Obtiene el id del usuario logueado
        $id_user = auth()->user()->employee_id;

        if (!empty($_POST['id_empleado']) || !empty(Session::get('id_empleado'))){

          if (!empty($_POST['id_empleado'])){

            $id_user = $_POST['id_empleado'];
          }

          else{

            $id_user = Session::get('id_empleado');
            Session::forget('id_empleado');
          }
        }

        // Obtiene el plan abierto mas reciente
        $plan = $planes[0];

        // Se requiere mostrar un plan por default
        if (!empty($_POST['id_plan']) || !empty(Session::get('id_plan'))){

          if (!empty($_POST['id_plan'])){

            $id_plan = $_POST['id_plan'];
            Session::put('id_plan', $id_plan);
          }

          else{

            $id_plan = Session::get('id_plan');
            Session::forget('id_plan');
          }

          foreach ($planes as $key => $value){
            
            if ($id_plan == $value->id){

              $plan = $value;
            }
          }
        }

        if (!empty($id_user)){        

          $ids_responsabilities = array();
          $responsabilidades = DB::select("SELECT responsabilidades_compromisos.id, responsabilidades_compromisos.id_responsable FROM responsabilidades_compromisos INNER JOIN objetivos_corporativos ON (responsabilidades_compromisos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE responsabilidades_compromisos.id_plan = ?", [$plan->id]);

          foreach ($responsabilidades as $key => $responsability){

            $found = false;

            if ($responsability->id_responsable == $id_user){

              $ids_responsabilities[] = $responsability->id;
              continue;
            }
          
            $corresponsables = DB::select("SELECT id_corresponsable, tipo FROM responsabilidades_corresponsables WHERE id_responsabilidad = ?", [$responsability->id]);

            foreach ($corresponsables as $key2 => $corresponsable){
            
              if ($corresponsable->tipo == 'persona'){

                if ($corresponsable->id_corresponsable == $id_user){

                  $ids_responsabilities[] = $responsability->id;
                  break;
                }
              }

              else{

                $elementos = DB::select("SELECT id_elemento, tipo_elemento FROM elementos_grupo WHERE id_grupo = ?", [$corresponsable->id_corresponsable]);

                foreach ($elementos as $key3 => $element){
                
                  if ($element->tipo_elemento == 'Persona'){

                    if ($element->id_elemento == $id_user){

                      $ids_responsabilities[] = $responsability->id;
                      $found = true;
                      break;
                    }
                  }

                  else{

                    if ($element->tipo_elemento == 'Departamento'){

                      //$area = DB::select("SELECT nombre FROM areas WHERE id = ?", [$element->id_elemento]);
                      $usuarios = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->where('departments.id', $element->id_elemento)->pluck('employees.id')->toArray();
                      //$usuarios = Personal::where('departamento', $area[0]->nombre)->pluck('id');
                      //$usuarios = (array) $usuarios;

                      if (in_array($id_user, $usuarios)){

                        $ids_responsabilities[] = $responsability->id;
                        break;
                      }
                    }

                    else{

                      $elementos_internos = DB::select("SELECT id_elemento, tipo_elemento FROM elementos_grupo WHERE id_grupo = ?", [$element->id_elemento]);

                      foreach ($elementos_internos as $key4 => $element_interno){
                
                        if ($element_interno->tipo_elemento == 'Persona'){

                          if ($element_interno->id_elemento == $id_user){

                            $ids_responsabilities[] = $responsability->id;
                            break;
                          }
                        }

                        else{

                          if ($element_interno->tipo_elemento == 'Departamento'){

                            $area = DB::select("SELECT nombre FROM areas WHERE id = ?", [$element_interno->id_elemento]);
                            $usuarios = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->where('departments.id', $element_interno->id_elemento)->pluck('employees.id')->toArray();
                            //$usuarios = Personal::where('departamento', $area[0]->nombre)->pluck('id');
                            //$usuarios = (array) $usuarios;

                            if (in_array($id_user, $usuarios)){

                              $ids_responsabilities[] = $responsability->id;
                              break;
                            }
                          }
                        }
                      }

                      if ($found){

                        break;
                      }
                    }
                  }
                }

                if ($found){

                  break;
                }
              }
            }
          }

          if (!empty($ids_responsabilities)){

            $ids_responsabilities = implode(',', $ids_responsabilities);

            // Se obtienen todas las responsabilidades relacionadas con el plan y sus cumplimientos (si es que ya hay registrados) que el usuario pueda ver
            $responsabilities = DB::select("SELECT responsabilidades_compromisos.id, responsabilidades_compromisos.description, responsabilidades_compromisos.frecuencia, responsabilidades_compromisos.frecuencia_captura, responsabilidades_compromisos.meta, responsabilidades_compromisos.unidad, responsabilidades_compromisos.valor_rojo, responsabilidades_compromisos.valor_verde, responsabilidades_compromisos.id_objetivo_corporativo, cumplimientos_compromisos.id AS id_cumplimiento, cumplimientos_compromisos.logro, cumplimientos_compromisos.periodo, cumplimientos_compromisos.revisado FROM responsabilidades_compromisos INNER JOIN objetivos_corporativos ON (objetivos_corporativos.id = responsabilidades_compromisos.id_objetivo_corporativo) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) LEFT JOIN cumplimientos_compromisos ON (responsabilidades_compromisos.id = cumplimientos_compromisos.id_responsabilidad) WHERE responsabilidades_compromisos.id IN ($ids_responsabilities) ORDER BY tipos_objetivos_corporativos.id, responsabilidades_compromisos.id_objetivo_corporativo, responsabilidades_compromisos.id");
          }

          $alimentar_propuestas = $plan->alimentar_propuestas;
		      $results = DB::select("SELECT id_empleado FROM cambiar_valores_periodos WHERE id_plan = ? AND estado = ? AND id_empleado = ?", [$plan->id, 1, $id_user]);
		  
		      //if ($alimentar_propuestas == 2 && $id_user == auth()->user()->id){
		      /*if ($alimentar_propuestas == 2){

            //$status_plan = DB::select("SELECT fecha_revisado FROM status_plan_empleado WHERE id_plan = ? AND id_empleado = ? AND status = ? ORDER BY periodo DESC LIMIT 1", [$plan->id,$id_user,5]);
		        $status_plan = DB::select("SELECT fecha_revisado, status FROM status_plan_empleado WHERE id_plan = ? AND id_empleado = ? AND status = ? ORDER BY periodo DESC LIMIT 1", [$plan->id,$id_user,5]);

            if (count($status_plan) > 0){
			  
		  	      if ($status_plan[0]->status == 5){

            	  $fecha = $status_plan[0]->fecha_revisado;
            	  $fecha_actual = date('Y-m-d');
            	  $user_accept = DB::select("SELECT fecha FROM user_accepts WHERE id_plan = ? AND id_empleado = ? AND tipo = ? ORDER BY fecha DESC LIMIT 1", [$plan->id,$id_user,'Resultados']);

            	  if (count($user_accept) > 0){

              	  $fecha_resultados = substr($user_accept[0], 0, 10);

              	  if ($fecha_resultados < $fecha){
						
						        if (count($results) > 0 && auth()->user()->employee_id != $id_user){

                      $cambiar_valores = true;
                    }

               	 	  $days = (strtotime($fecha_actual) - strtotime($fecha)) / (60 * 60 * 24);

                	  if ($days < 8 && auth()->user()->employee_id == $id_user){

                  	  $mostrar_boton_aceptar = true;
                	  }
					        }
				        }
				
				        else{
					
					        if (count($results) > 0 && auth()->user()->employee_id != $id_user){

                    $cambiar_valores = true;
                  }

              	  $days = (strtotime($fecha_actual) - strtotime($fecha)) / (60 * 60 * 24);

              	  if ($days < 8 && auth()->user()->employee_id == $id_user){

                	  $mostrar_boton_aceptar = true;
              	  }
            	  }
              } 
            }
          }*/  

          // Obtiene los datos del usuario
          //$user = DB::select('select first_name, last_name, boss_id, division, subdivision FROM users WHERE id = ?', [$id_user]);
          $user = Employee::find($id_user);
          //$user = DB::table('employees')->join('users', 'users.employee_id', '=', 'employees.id')->where('employees.id', $id_user)->select('employees.*', 'users.id AS user_id')->first();
          $user_id = $user->user->id;
          $periodos_status_plan = DB::select('SELECT fecha_solicitado, fecha_autorizado FROM status_plan_empleado WHERE id_plan = ? AND id_empleado = ? LIMIT 1', [$plan->id, $id_user]);

          if (count($periodos_status_plan) > 0){

            $fecha_solicitado = $periodos_status_plan[0]->fecha_solicitado;
            $fecha_autorizado = $periodos_status_plan[0]->fecha_autorizado;
          }
		  
		      // Obtiene todos los status de cada periodo del plan del usuario ordenados por el periodo
          $periodos_status_plan = DB::select('SELECT status, fecha_solicitado, fecha_autorizado, fecha_registrado, fecha_revisado, periodo FROM status_plan_empleado where id_plan = ? AND id_empleado = ? ORDER BY periodo', [$plan->id, $id_user]);
		  
		      $periodo = 1;
		  
		      // Hay periodos en el plan del usuario
          if (!empty($periodos_status_plan)){

            // Obtiene el status del ultimo periodo del plan del empleado
            $periodo = $periodos_status_plan[count($periodos_status_plan) - 1]->periodo;
		      }

          // Obtiene los objetivos o propuestas del usuario junto con los logros por perido de cada propuesta
          //$objetivos = DB::select('SELECT objetivos.id, objetivos.nombre, objetivos.peso, objetivos.frecuencia, objetivos.tipo, objetivos.objetivo, objetivos.valor_rojo, objetivos.valor_verde, objetivos.viejo, objetivos.periodo, ejes_estrategicos.nombre AS nombre_eje, logros.logro, logros.periodo AS periodo_logro from objetivos left join ejes_estrategicos on (ejes_estrategicos.id = objetivos.id_eje) left join logros on (logros.objetivo_id = objetivos.id AND logros.id_plan = objetivos.id_plan AND objetivos.id_empleado = logros.id_empleado) WHERE objetivos.id_plan = ? AND objetivos.id_empleado = ? ORDER BY objetivos.id, periodo_logro', [$plan->id, $id_user]);
          $objetivos = DB::select("SELECT objetivos.id, objetivos.nombre, id_catalogo, objetivos.peso, objetivos.frecuencia, objetivos.tipo, objetivos.objetivo, objetivos.valor_rojo, objetivos.valor_verde, objetivos.viejo, objetivos.periodo, objetivos.id_objetivo_corporativo, tipo_bono, logros.logro, logros.periodo AS periodo_logro, revisado FROM objetivos INNER JOIN objetivos_corporativos ON (objetivos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) LEFT JOIN logros ON (logros.objetivo_id = objetivos.id AND logros.id_empleado = objetivos.id_empleado AND logros.id_plan = objetivos.id_plan) WHERE objetivos.id_plan = ? AND objetivos.id_empleado = ? ORDER BY objetivos.id, periodo_logro", [$plan->id, $id_user]);
        
          // Hay periodos en el plan del usuario
          /*if (!empty($periodos_status_plan)){

            // Obtiene el status del ultimo periodo del plan del empleado
            $status_plan = $periodos_status_plan[count($periodos_status_plan) - 1];

            // Si el ultimo periodo ya tiene fecha de revision de logros
            if (!empty($status_plan->fecha_revisado) && $status_plan->fecha_revisado != '0000-00-00'){

              // El ultimo periodo no es el ultimo del año
              if ($status_plan->periodo < 12){
				
			          //$objetivos = DB::select('select frecuencia from objetivos where id_plan = ? AND id_empleado = ?', [$plan->id, $id_user]);

                $band = false;

                foreach ($objetivos as $key => $value){

                  if ($value->frecuencia == 'Mensual'){

                    $band = true;
                    break;
                  }

                  else{

                    if ($value->frecuencia == 'Semestral' && ($status_plan->periodo * 6) < 12){

                      $band = true;
                      break;
                    }

                    else{

                      if ($value->frecuencia == 'Trimestral' && ($status_plan->periodo * 3) < 12){

                        $band = true;
                        break;
                      }
                    }  
                  }
                }

                if ($band){

                  // Verificar si existen cambios de puesto pendientes
                  $cambios_puesto = DB::select('SELECT id FROM cambios_de_puesto WHERE id_empleado = ?', [$id_user]);

                  if (count($cambios_puesto) > 0){

                    $id = $cambios_puesto[0]->id;

                    // Se inserta un registro para un nuevo periodo con status para agregar nuevos objetivos
                    DB::insert('INSERT INTO status_plan_empleado (id_plan, id_empleado, status, periodo) VALUES (?,?,?,?)', [$plan->id, $id_user, 1, $status_plan->periodo + 1]);

                    // Se vuelven viejos los objetivos actuales
                    DB::update('UPDATE objetivos SET viejo = 1 WHERE id_plan = ? AND id_empleado = ?', [$plan->id, $id_user]);

                    // Borra el registro de cambio de puesto
                    DB::delete('DELETE FROM cambios_de_puesto WHERE id = ?', [$id]);
                  }

                  else{

                    // Se inserta un registro para un nuevo periodo
                    DB::insert('INSERT INTO status_plan_empleado (id_plan, id_empleado, status, fecha_solicitado, fecha_autorizado, periodo) VALUES (?,?,?,?,?,?)', [$plan->id, $id_user, 3, $status_plan->fecha_solicitado, $status_plan->fecha_autorizado, $status_plan->periodo + 1]);
                  }
				  
			 	          $periodo = $status_plan->periodo + 1;
				  
				          // Obtiene los objetivos o propuestas del usuario junto con los logros por perido de cada propuesta
                  //$objetivos = DB::select('SELECT objetivos.id, objetivos.nombre, objetivos.peso, objetivos.frecuencia, objetivos.tipo, objetivos.objetivo, objetivos.valor_rojo, objetivos.valor_verde, objetivos.viejo, objetivos.periodo, ejes_estrategicos.nombre AS nombre_eje, logros.logro, logros.periodo AS periodo_logro from objetivos left join ejes_estrategicos on (ejes_estrategicos.id = objetivos.id_eje) left join logros on (logros.objetivo_id = objetivos.id AND logros.id_plan = objetivos.id_plan AND logros.id_empleado = objetivos.id_empleado) WHERE objetivos.id_plan = ? AND objetivos.id_empleado = ? ORDER BY objetivos.id, periodo_logro', [$plan->id, $id_user]);
                  $objetivos = DB::select("SELECT objetivos.id, objetivos.nombre, objetivos.peso, objetivos.frecuencia, objetivos.tipo, objetivos.objetivo, objetivos.valor_rojo, objetivos.valor_verde, objetivos.viejo, objetivos.periodo, objetivos.id_objetivo_corporativo, logros.logro, logros.periodo AS periodo_logro, revisado FROM objetivos INNER JOIN objetivos_corporativos ON (objetivos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) LEFT JOIN logros ON (logros.objetivo_id = objetivos.id AND logros.id_empleado = objetivos.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan = ? AND objetivos.id_empleado = ? ORDER BY objetivos.id, periodo_logro", [$plan->id, $id_user]);
              
                  // Obtiene todos los status de cada periodo del plan del usuario ordenados por el periodo
                  $periodos_status_plan = DB::select('SELECT status, fecha_solicitado, fecha_autorizado, fecha_registrado, fecha_revisado, periodo FROM status_plan_empleado WHERE id_plan = ? AND id_empleado = ? ORDER BY periodo', [$plan->id, $id_user]);
              
                  // Obtiene el status del ultimo periodo del paln del empleado
                  //$status_plan = $periodos_status_plan[count($periodos_status_plan) - 1];
			          }
              }
            }*/

            // Obtiene los objetivos o propuestas del usuario junto con los logros por perido de cada propuesta
            /*$objetivos = DB::select('select objetivos.id, objetivos.nombre, objetivos.frecuencia, objetivos.tipo, objetivos.objetivo, objetivos.valor_rojo, objetivos.valor_verde, objetivos.tipo_semaforo, ejes_estrategicos.nombre AS nombre_eje, logros.logro, logros.periodo from objetivos inner join ejes_estrategicos on (ejes_estrategicos.id = objetivos.id_eje) left join logros on (logros.objetivo_id = objetivos.id AND logros.id_plan = objetivos.id_plan) where objetivos.id_plan = ? AND objetivos.id_empleado = ? ORDER BY objetivos.id', [$plan->id, $id_user]);*/
          //}
		  
		      $nuevos_objetivos = array();
		      $periodos = array();
          $actual_objetivo = 0;
          $valores_objetivo = array();
          $ids_personales = array();
          $mes = 1;
          $frecuencia = 1;
          $periodo_actual = 0;

          foreach ($objetivos as $key => $objetivo){
                
            if ($actual_objetivo != $objetivo->id){

              if ($actual_objetivo != 0){

                $mes = $periodo_actual * $frecuencia;

                if ($mes < $mes_a_registrar){

                  $mes_a_registrar = $mes;
                }
              }

              $actual_objetivo = $objetivo->id;
              $valores_objetivo = DB::select("SELECT valor_rojo, valor_verde, meta, periodo FROM valores_objetivo WHERE id_objetivo = ? ORDER BY periodo", [$actual_objetivo]);

              if ($objetivo->tipo_bono == 'Comision'){

                $objetivo->comisiones = $valores_objetivo;
              }

              $ids_personales[] = $actual_objetivo;
              $frecuencia = 1;
              //$periodo_actual = $objetivo->periodo;

              if ($objetivo->frecuencia == 'Bimestral'){

                $frecuencia = 2;
              }

              else{

                if ($objetivo->frecuencia == 'Trimestral'){

                  $frecuencia = 3;
                }

                else{

                  if ($objetivo->frecuencia == 'Cuatrimestral'){

                    $frecuencia = 4;
                  }

                  else{

                    if ($objetivo->frecuencia == 'Semestral'){

                      $frecuencia = 6;
                    }

                    else{

                      if ($objetivo->frecuencia == 'Anual'){

                        $frecuencia = 12;
                      }
                    }
                  }
                }
              }

              $periodo_actual = intval($plan->periodo / $frecuencia);

              if ($plan->periodo % $frecuencia != 0){

                $periodo_actual++;
              }
            }

            if (!empty($objetivo->logro) || $objetivo->logro == '0'){

              if ($objetivo->revisado == 1){

                $periodo_actual = $objetivo->periodo_logro + 1;
              }

              else{

                $periodo_actual = $objetivo->periodo_logro;
              }

              if (!empty($periodos[$objetivo->periodo_logro])){
				  
				        $objetivo->status = $periodos[$objetivo->periodo_logro];
			        }
				
			        else{
				
			  	      $result = DB::select("SELECT status FROM status_plan_empleado WHERE id_plan = ? AND id_empleado = ? AND periodo = ?", [$plan->id, $id_user, $objetivo->periodo_logro]);

                if (count($result) > 0){

                  $objetivo->status = $result[0]->status;
					        $periodos[$objetivo->periodo_logro] = $result[0]->status;
                }

                else{

                  DB::insert('INSERT INTO status_plan_empleado (id_plan, id_empleado, status, fecha_solicitado, fecha_autorizado, periodo) VALUES (?,?,?,?,?,?)', [$plan->id, $id_user, 3, $fecha_solicitado, $fecha_autorizado, $objetivo->periodo_logro]);
                  $objetivo->status = 3;
                }
			        }

              /*if (!empty($valores_objetivo)){

                $band = false;

                foreach ($valores_objetivo as $key2 => $value2){

                  if ($objetivo->periodo_logro == $value2->periodo){

                    $objetivo->valor_rojo = $value2->valor_rojo;
                    $objetivo->valor_verde = $value2->valor_verde;
                    $objetivo->objetivo = $value2->meta;
                    $band = true;
                    break;
                  }

                  else{

                    if ($value2->periodo > $objetivo->periodo_logro){
					  
					            if ($key2 > 0){

                    	  $objetivo->valor_rojo = $valores_objetivo[$key2 - 1]->valor_rojo;
                    	  $objetivo->valor_verde = $valores_objetivo[$key2 - 1]->valor_verde;
                        $objetivo->objetivo = $valores_objetivo[$key2 - 1]->meta;
					            }
						
					            $band = true;
                      break;
                    } 
                  }
                }

                if (!$band){

                  $objetivo->valor_rojo = $valores_objetivo[count($valores_objetivo) - 1]->valor_rojo;
                  $objetivo->valor_verde = $valores_objetivo[count($valores_objetivo) - 1]->valor_verde;
                  $objetivo->objetivo = $valores_objetivo[count($valores_objetivo) - 1]->meta;
                }
              }*/
            }

            else{

              /*if (!empty($valores_objetivo)){

                $band = false;

                foreach ($valores_objetivo as $key2 => $value2){

                  if ($periodo_actual == $value2->periodo){

                    $objetivo->valor_rojo = $value2->valor_rojo;
                    $objetivo->valor_verde = $value2->valor_verde;
                    $objetivo->objetivo = $value2->meta;
                    $band = true;
                    break;
                  }
                }

                if (!$band){

                  $objetivo->valor_rojo = $valores_objetivo[0]->valor_rojo;
                  $objetivo->valor_verde = $valores_objetivo[0]->valor_verde;
                  $objetivo->objetivo = $valores_objetivo[0]->meta;
                }
              }*/
            }

            $nuevos_objetivos[] = $objetivo;
          }

          if ($actual_objetivo != 0){

            $mes = $periodo_actual * $frecuencia;

            if ($mes < $mes_a_registrar){

              $mes_a_registrar = $mes;
            }
          }

          $objetivos = $nuevos_objetivos;

          $ids_objetivos = array();
          $objetivos_corresponsable = DB::select("SELECT objetivos.id FROM objetivos INNER JOIN objetivos_corporativos ON (objetivos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE objetivos.id_plan = ? AND id_responsable = ?", [$plan->id, 0]);

          foreach ($objetivos_corresponsable as $key => $objetivo){

            if (in_array($objetivo->id, $ids_personales)){

              continue;
            }

            $found = false;
            $corresponsables = DB::select("SELECT id_corresponsable, tipo FROM objetivos_corresponsables WHERE id_objetivo = ?", [$objetivo->id]);

            foreach ($corresponsables as $key2 => $corresponsable){
            
              if ($corresponsable->tipo == 'persona'){

                if ($corresponsable->id_corresponsable == $id_user){

                  $ids_objetivos[] = $objetivo->id;
                  break;
                }
              }

              else{

                $elementos = DB::select("SELECT id_elemento, tipo_elemento FROM elementos_grupo WHERE id_grupo = ?", [$corresponsable->id_corresponsable]);

                foreach ($elementos as $key3 => $element){
                
                  if ($element->tipo_elemento == 'Persona'){

                    if ($element->id_elemento == $id_user){

                      $ids_objetivos[] = $objetivo->id;
                      $found = true;
                      break;
                    }
                  }

                  else{

                    if ($element->tipo_elemento == 'Departamento'){

                      //$area = DB::select("SELECT nombre FROM areas WHERE id = ?", [$element->id_elemento]);
                      $usuarios = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->where('departments.id', $element->id_elemento)->pluck('employees.id')->toArray();
                      //$usuarios = Personal::where('departamento', $area[0]->nombre)->pluck('id');
                      //$usuarios = (array) $usuarios;

                      if (in_array($id_user, $usuarios)){

                        $ids_objetivos[] = $objetivo->id;
                        break;
                      }
                    }

                    else{

                      $elementos_internos = DB::select("SELECT id_elemento, tipo_elemento FROM elementos_grupo WHERE id_grupo = ?", [$element->id_elemento]);

                      foreach ($elementos_internos as $key4 => $element_interno){
                
                        if ($element_interno->tipo_elemento == 'Persona'){

                          if ($element_interno->id_elemento == $id_user){

                            $ids_objetivos[] = $objetivo->id;
                            break;
                          }
                        }

                        else{

                          if ($element_interno->tipo_elemento == 'Departamento'){

                            //$area = DB::select("SELECT nombre FROM areas WHERE id = ?", [$element_interno->id_elemento]);
                            $usuarios = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->where('departments.id', $element_interno->id_elemento)->pluck('employees.id')->toArray();
                            //$usuarios = Personal::where('departamento', $area[0]->nombre)->pluck('id');
                            //$usuarios = (array) $usuarios;

                            if (in_array($id_user, $usuarios)){

                              $ids_objetivos[] = $objetivo->id;
                              break;
                            }  
                          }
                        }
                      }

                      if ($found){

                        break;
                      }
                    }
                  }
                }

                if ($found){

                  break;
                }
              }
            }
          }

          if (!empty($ids_objetivos)){

            $ids_objetivos = implode(',', $ids_objetivos);
            $objetivos_corresponsables = DB::select("SELECT objetivos.id, objetivos.nombre, objetivos.frecuencia, objetivos.objetivo, objetivos.tipo, objetivos.valor_rojo, objetivos.valor_verde, objetivos.id_objetivo_corporativo, logros.id AS id_logro, logros.logro, logros.periodo, logros.revisado FROM objetivos LEFT JOIN logros ON (logros.id_empleado = objetivos.id_empleado AND logros.objetivo_id = objetivos.id AND logros.id_plan = objetivos.id_plan) WHERE objetivos.id IN ($ids_objetivos) ORDER BY objetivos.id, logros.periodo");
          }

          $objetivos_corporativos = DB::select("SELECT objetivos_corporativos.id, id_tipo, letter FROM objetivos_corporativos INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) ORDER BY objetivos_corporativos.id_tipo, objetivos_corporativos.id");
          $notas = DB::select("SELECT mensaje, notas.created_at, nombre, paterno, materno FROM notas INNER JOIN employees ON (employees.id = notas.id_user) WHERE id_plan = ? ORDER BY notas.created_at DESC", [$plan->id]);

          // Obtiene los mensajes o notas del usuario
            //if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

              $mensajes = DB::select('SELECT nombre, paterno, materno, mensaje, mensajes.fecha, mensajes.id FROM mensajes INNER JOIN employees ON (employees.id = mensajes.id_de) WHERE id_plan = ? AND (mensajes.id_de = ? OR mensajes.id_para = ?) ORDER BY mensajes.id DESC', [$plan->id, $id_user, $id_user]);
            /*}

            else{

              $mensajes = DB::select('SELECT nombre, paterno, materno, mensaje, mensajes.fecha, mensajes.id FROM mensajes INNER JOIN employees ON (employees.id = mensajes.id_de) where id_plan = ? AND ((mensajes.id_de = ? AND mensajes.id_para = ?) OR (mensajes.id_para = ? AND mensajes.id_de = ?)) ORDER BY mensajes.id DESC', [$plan->id, $id_user, auth()->user()->employee->id, $id_user, auth()->user()->employee->id]);
            }*/
        }

        else{

          $user = auth()->user();
          $user->nombre = $user->first_name;
          $user->paterno = $user->last_name;
          $user->materno = '';
          $user_id = $user->id;
        }

        $revisado_por_director = DB::table('revision_resultados_director')->where('plan_id', $plan->id)->where('user_id', $user_id)->first();
      }

      return view('evaluacion-resultados/registro-logros', compact('plan', 'planes', 'id_user', 'user', 'objetivos', 'objetivos_corresponsables', 'objetivos_corporativos', 'responsabilities', 'notas', 'mensajes', 'fecha_autorizado', 'periodos_status_plan', 'alimentar_propuestas', 'periodo', 'mostrar_boton_aceptar', 'cambiar_valores', 'mes_a_registrar', 'revisado_por_director'));
    }

		/**
     * Muestra la pagina para revisar los logros de las propuestas
     *
     * @return void
     */
    public function revisionLogros(){

      // Obtiene el id del usuario logueado
      $id_user = auth()->user()->employee_id;

      $bono = 0;

      // Se obtiene el id del usuario a revisarle los logros
      if (!empty($_POST['id_empleado']) || !empty(Session::get('id_empleado'))){

        if (!empty($_POST['id_empleado'])){

          $id_user = $_POST['id_empleado'];
        }

        else{

          $id_user = Session::get('id_empleado');
          Session::forget('id_empleado');
        }
      }

      // Obtiene todos los planes abiertos ordenados por los mas recientes
      $planes = DB::select('SELECT id, nombre, anio, alimentar_propuestas, autorizar_logros FROM planes WHERE estado = ? ORDER BY id DESC', [2]);

      $user = $objetivos = $ejes_estrategicos = array();
      $alimentar_propuestas = 1;
	    $periodo = '';

      // Hay planes abiertos
      if (!empty($planes)){

        // Obtiene el plan abierto mas reciente
        $plan = $planes[0];

        // Se requiere mostrar un plan por default
        if (!empty($_POST['id_plan']) || !empty(Session::get('id_plan'))){

          if (!empty($_POST['id_plan'])){

            $id_plan = $_POST['id_plan'];
            Session::put('id_plan', $id_plan);
          }

          else{

            $id_plan = Session::get('id_plan');
            Session::forget('id_plan');
          }

          foreach ($planes as $key => $value){
            
            if ($id_plan == $value->id){

              $plan = $value;
            }
          }
        }

        $alimentar_propuestas = $plan->alimentar_propuestas;

        if (!empty($id_user)){

          // Obtiene los datos del empleado
          //$user = DB::select('SELECT first_name, last_name, boss_id, division, subdivision FROM users WHERE id = ?', [$id_user]);
          $user = Employee::find($id_user);

          // Obtiene todos los status de cada periodo del plan del usuario ordenados por el periodo
          $periodos_status_plan = DB::select('SELECT status, fecha_autorizado, fecha_registrado, fecha_revisado, periodo FROM status_plan_empleado WHERE id_plan = ? AND id_empleado = ? ORDER BY periodo', [$plan->id, $id_user]);
		  
		      $periodo = 1;
        
          // Hay periodos en el plan del usuario
          if (!empty($periodos_status_plan)){

            // Obtiene el status del ultimo periodo del plan del empleado
            $status_plan = $periodos_status_plan[count($periodos_status_plan) - 1];
			
		        $periodo = $status_plan->periodo;
          }

          // Obtiene los objetivos o propuestas del usuario junto con los logros por perido de cada propuesta
          //$objetivos = DB::select('SELECT objetivos.id, objetivos.nombre, objetivos.peso, objetivos.frecuencia, objetivos.tipo, objetivos.valor_rojo, objetivos.valor_verde, ejes_estrategicos.nombre AS nombre_eje, objetivos.objetivo, objetivos.periodo, logros.logro, logros.revisado, logros.periodo AS periodo_logro FROM objetivos LEFT JOIN ejes_estrategicos on (ejes_estrategicos.id = objetivos.id_eje) LEFT JOIN logros on (logros.objetivo_id = objetivos.id AND logros.id_plan = objetivos.id_plan AND logros.id_empleado = objetivos.id_empleado) WHERE objetivos.id_plan = ? AND objetivos.id_empleado = ? ORDER BY objetivos.id, periodo_logro', [$plan->id, $id_user]);
          $objetivos = DB::select("SELECT objetivos.id, objetivos.nombre, objetivos.peso, objetivos.frecuencia, objetivos.tipo, objetivos.objetivo, objetivos.valor_rojo, objetivos.valor_verde, objetivos.periodo, objetivos.id_objetivo_corporativo, tipo_bono, logros.logro, logros.revisado, logros.periodo AS periodo_logro FROM objetivos INNER JOIN objetivos_corporativos ON (objetivos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) LEFT JOIN logros ON (logros.objetivo_id = objetivos.id AND logros.id_empleado = objetivos.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan = ? AND objetivos.id_empleado = ? ORDER BY objetivos.id, periodo_logro", [$plan->id, $id_user]);

          $nuevos_objetivos = array();
          $actual_objetivo = 0;
          $valores_objetivo = array();

          foreach ($objetivos as $key => $objetivo){
                
            if ($actual_objetivo != $objetivo->id){

              $actual_objetivo = $objetivo->id;
              $valores_objetivo = DB::select("SELECT valor_rojo, valor_verde, meta, periodo FROM valores_objetivo WHERE id_objetivo = ? ORDER BY periodo", [$actual_objetivo]);

              if ($objetivo->tipo_bono == 'Comision'){

                $objetivo->comisiones = $valores_objetivo;
              }
            }

            if (!empty($objetivo->logro) || $objetivo->logro == '0'){

              if (!empty($valores_objetivo)){

                $band = false;

                foreach ($valores_objetivo as $key2 => $value2){

                  if ($objetivo->periodo_logro == $value2->periodo){

                    $objetivo->valor_rojo = $value2->valor_rojo;
                    $objetivo->valor_verde = $value2->valor_verde;
                    $objetivo->objetivo = $value2->meta;
                    $band = true;
                    break;
                  } 

                  else{

                    if ($value2->periodo > $objetivo->periodo_logro){
					  
					            if ($key2 > 0){

                    	  $objetivo->valor_rojo = $valores_objetivo[$key2 - 1]->valor_rojo;
                    	  $objetivo->valor_verde = $valores_objetivo[$key2 - 1]->valor_verde;
                        $objetivo->objetivo = $valores_objetivo[$key2 - 1]->meta;
					            }
					  
                      $band = true;
                      break;
                    } 
                  }
                }

                if (!$band){

                  $objetivo->valor_rojo = $valores_objetivo[count($valores_objetivo) - 1]->valor_rojo;
                  $objetivo->valor_verde = $valores_objetivo[count($valores_objetivo) - 1]->valor_verde;
                  $objetivo->objetivo = $valores_objetivo[count($valores_objetivo) - 1]->meta;
                }
              }
            }

            else{

              if (!empty($valores_objetivo)){

                $band = false;

                foreach ($valores_objetivo as $key2 => $value2){

                  if ($objetivo->periodo == $value2->periodo){

                    $objetivo->valor_rojo = $value2->valor_rojo;
                    $objetivo->valor_verde = $value2->valor_verde;
                    $objetivo->objetivo = $value2->meta;
                    $band = true;
                    break;
                  }
                }

                if (!$band){

                  $objetivo->valor_rojo = $valores_objetivo[0]->valor_rojo;
                  $objetivo->valor_verde = $valores_objetivo[0]->valor_verde;
                  $objetivo->objetivo = $valores_objetivo[0]->meta;
                }
              }
            }

            $nuevos_objetivos[] = $objetivo;
          }

          $objetivos = $nuevos_objetivos;

          // Obtiene el bono si es que ya fue calculado para el plan
          //$bonos = DB::select("SELECT bono_total FROM users_bonos WHERE id_user = ? AND id_plan = ?", [$id_user, $plan->id]);

          // Si hay bono calculado
          /*if (count($bonos) > 0){

            $bono = $bonos[0]->bono_total;
          }*/

          // Obtiene los mensajes o notas del usuario
            //if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

              $notas = DB::select('SELECT nombre, paterno, materno, mensaje, mensajes.fecha, mensajes.id FROM mensajes INNER JOIN employees ON (employees.id = mensajes.id_de) WHERE id_plan = ? AND (mensajes.id_de = ? OR mensajes.id_para = ?) ORDER BY mensajes.id DESC', [$plan->id, $id_user, $id_user]);
            /*}

            else{

              $notas = DB::select('SELECT nombre, paterno, materno, mensaje, mensajes.fecha, mensajes.id FROM mensajes INNER JOIN employees ON (employees.id = mensajes.id_de) where id_plan = ? AND ((mensajes.id_de = ? AND mensajes.id_para = ?) OR (mensajes.id_para = ? AND mensajes.id_de = ?)) ORDER BY mensajes.id DESC', [$plan->id, $id_user, auth()->user()->employee->id, $id_user, auth()->user()->employee->id]);
            }*/

          $bono = DB::table('bonos_planes')->where('id_plan', $plan->id)->where('id_user', $id_user)->first();  
        }
      }

      $objetivos_corporativos_sin_responsable = DB::select("SELECT objetivos_corporativos.id, id_tipo, letter FROM objetivos_corporativos INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) WHERE id_responsable = ? ORDER BY objetivos_corporativos.id_tipo, objetivos_corporativos.id", [0]);
      return view('evaluacion-resultados/revision-logros', compact('plan', 'planes', 'id_user', 'user', 'objetivos', 'objetivos_corporativos_sin_responsable', 'notas', 'status_plan', 'periodos_status_plan', 'alimentar_propuestas', 'periodo', 'bono'));	
    }

		public function matrizObjetivos()
    {
    	return view('evaluacion-resultados/matriz-objetivos');
    }

    /**
     * Muestra la pagina ver el avance de los subordinados por jefe o todos si eres admin
     *
     * @return void
     */
		public function estadisticaAvance(){

      $planes = array();

      if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

        // Obtiene todos los planes preparatorios y abiertos ordenados por los mas recientes
        $planes = DB::select('SELECT id, nombre, tipo, estado, periodo, alimentar_propuestas, autorizar_propuestas, registrar_logros, autorizar_logros, registrar_logros_abierto FROM planes where estado = ? OR estado = ? ORDER BY id DESC', [1, 2]);
      }

      else{

    	  // Obtiene todos los planes abiertos ordenados por los mas recientes
        $planes = DB::select('SELECT id, nombre, tipo, periodo, alimentar_propuestas, autorizar_propuestas, registrar_logros, autorizar_logros, registrar_logros_abierto FROM planes where estado = ? ORDER BY id DESC', [2]);
      }

      $id_jefe = auth()->user()->employee_id;
      $usuarios = array();
      $users = array();
      $alimentar_propuestas = 1;
      $user = Employee::where('id', $id_jefe)->select('idempleado')->first();
      $director_revisions = array();
      $id_plan = 0;

      // Hay planes abiertos
      if (!empty($planes)){

        // Obtiene el plan abierto mas reciente
        $plan = $planes[0];

        // Se requiere mostrar un plan por default
        if (!empty($_POST['id_plan']) || Session::has('id_plan')){

          if (!empty($_POST['id_plan'])){

            $id_plan = $_POST['id_plan'];
            Session::put('id_plan', $id_plan);
          }

          else{

            $id_plan = Session::get('id_plan');
          }

          foreach ($planes as $key => $value){
            
            if ($id_plan == $value->id){

              $plan = $value;
            }
          }
        }

        $alimentar_propuestas = $plan->alimentar_propuestas;

        if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

          // Obtiene los datos de todos los usuarios
          $users = DB::select('SELECT nombre, paterno, materno, idempleado, directions.name AS direccion, employees.id, jefe, departments.name AS department, job_positions.name AS job_position, id_plan, fecha_solicitado, fecha_autorizado, fecha_registrado, fecha_revisado, status, periodo, users.id AS user_id FROM employees JOIN users ON (users.employee_id = employees.id) LEFT JOIN job_positions ON (job_positions.id = employees.job_position_id) LEFT JOIN areas ON (areas.id = job_positions.area_id) LEFT JOIN departments ON (departments.id = areas.department_id) LEFT JOIN directions ON (directions.id = departments.direction_id) LEFT JOIN status_plan_empleado ON (status_plan_empleado.id_empleado = employees.id) WHERE employees.deleted_at IS NULL ORDER BY employees.id, id_plan, periodo DESC');
        }

        else{

          if (!empty($id_jefe)){

            //if (!empty(auth()->user()->employee->grado)){

              $users = array($user->idempleado);
              $ids = array();

              do{

                $ids = array_merge($ids, $users);
                $users = DB::table('employees')->whereIn('jefe', $users)->pluck('idempleado')->toArray();

              }while(count($users) > 0);

              // Obtiene los datos de todos los subordinados del usuario logueado
              //$users = DB::select('SELECT nombre, paterno, materno, idempleado, direccion, employees.id, departments.name AS department, job_positions.name AS job_position, id_plan, fecha_solicitado, fecha_autorizado, fecha_registrado, fecha_revisado, status, periodo FROM employees LEFT JOIN job_positions ON (job_positions.id = employees.job_position_id) LEFT JOIN areas ON (areas.id = job_positions.area_id) LEFT JOIN departments ON (departments.id = areas.department_id) LEFT JOIN status_plan_empleado ON (status_plan_empleado.id_empleado = employees.id) WHERE employees.jefe = ? AND employees.id <> ? AND employees.deleted_at IS NULL ORDER BY employees.id, id_plan, periodo DESC', [$user->idempleado, $id_jefe]);
              $users = DB::table('employees')->join('users', 'users.employee_id', '=', 'employees.id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('directions', 'directions.id', '=', 'departments.direction_id')->leftJoin('status_plan_empleado', 'status_plan_empleado.id_empleado', '=', 'employees.id')->whereIn('jefe', $ids)->whereNull('employees.deleted_at')->select('nombre', 'paterno', 'materno', 'idempleado', 'directions.name AS direccion', 'employees.id', 'departments.name AS department', 'job_positions.name AS job_position', 'id_plan', 'fecha_solicitado', 'fecha_autorizado', 'fecha_registrado', 'fecha_revisado', 'status', 'periodo', 'users.id AS user_id')->orderBy('employees.id')->orderBy('id_plan')->orderBy('periodo', 'DESC')->get();
            /*}

            else{

              $users = DB::table('employees')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('status_plan_empleado', 'status_plan_empleado.id_empleado', '=', 'employees.id')->where('jefe', $user->idempleado)->whereNull('employees.deleted_at')->select('nombre', 'paterno', 'materno', 'idempleado', 'direccion', 'employees.id', 'departments.name AS department', 'job_positions.name AS job_position', 'id_plan', 'fecha_solicitado', 'fecha_autorizado', 'fecha_registrado', 'fecha_revisado', 'status', 'periodo')->orderBy('employees.id')->orderBy('id_plan')->orderBy('periodo', 'DESC')->get();
            }*/
          }
        }

        $users_bonos = DB::table('bonos_planes')->where('id_plan', $plan->id)->pluck('id_user')->toArray();
		    $num_users = DB::select("SELECT COUNT(id) AS total_users FROM employees WHERE deleted_at IS NULL");
        $num_users = $num_users[0]->total_users;
        $objetivos_autorizados = DB::select("SELECT id_empleado FROM status_plan_empleado WHERE status > ? AND id_plan = ? GROUP BY id_empleado", [2, $plan->id]);
        $objetivos_autorizados = count($objetivos_autorizados);
        $logros_revisados = DB::select("SELECT id_empleado FROM logros WHERE revisado = ? AND id_plan = ? AND periodo = ? GROUP BY id_empleado", [1, $plan->id, $plan->periodo]);
        $logros_revisados = count($logros_revisados);
        $ids_empleados = array();
		    $id_empleado = 0;
		    $temp = 0;
		    $cambiar_valores = array();
		  
		    if ($plan->tipo == 2){
			
			    $temporal = array();
			    $cambiar_valores = DB::select("SELECT id_empleado FROM cambiar_valores_periodos WHERE id_plan = ? AND estado = ?", [$plan->id, 1]);
			
			    foreach ($cambiar_valores AS $key => $value){
				
				    $temporal[] = $value->id_empleado;
			    }
			
			    $cambiar_valores = $temporal;
		    }

        $statuses = DB::table('status_plan_empleado')->where('id_plan', $plan->id)->where('status', 4)->pluck('id_empleado')->toArray();

        // Revisa todos los empleados y solo se queda con los que pertenezcan al plan actual
        foreach ($users as $key => $value){
			
		      if ($id_empleado != $value->id){

            if ($id_empleado != 0){

              if (!in_array($id_empleado, $ids_empleados)){
				  
				        if (in_array($id_empleado, $cambiar_valores)){
					  
					        $temp->estado = 1;
				        }

                if (in_array($id_empleado, $users_bonos)){

                  $temp->bono = true;
                }

                $temp->id_plan = '';
                $temp->fecha_registrado = '';
                $temp->fecha_solicitado = '';
                $temp->fecha_autorizado = '';
                $temp->fecha_revisado = '';
				        $temp->periodo = 0;
				        $temp->status = 0;
                $usuarios[] = $temp;
                array_push($ids_empleados, $id_empleado);
              }
            }

            $id_empleado = $value->id;
          }

          $temp = $value;

          if (!empty($value->id_plan)){

            if ($plan->id == $value->id_plan){
              
              if (!in_array($value->id, $ids_empleados)){
				  
				        //if ($value->periodo > 1 && $value->fecha_revisado == '0000-00-00'){
						    if (empty($value->fecha_revisado)){	
						
							    if (!empty($users[$key + 1]) && $users[$key + 1]->id == $value->id && $users[$key + 1]->id_plan == $plan->id){
					
					        	$value->ultima_reunion = $users[$key + 1]->fecha_revisado;
                  	$value->status = $users[$key + 1]->status;
							    }
				        }
				  
						    else{
							
							    $value->ultima_reunion = $value->fecha_revisado;
						    }

                if (in_array($id_empleado, $statuses)){

                  $value->status = 4;
                }
				  
				        if (in_array($id_empleado, $cambiar_valores)){
					  
					        $value->estado = 1;
				        }

                if (in_array($value->id, $users_bonos)){

                  $value->bono = true;
                }

                $usuarios[] = $value;
                array_push($ids_empleados, $value->id);
              }
            }
          }

          else{
			  
			      if (in_array($value->id, $cambiar_valores)){
					  
				      $value->estado = 1;
			      }

            if (in_array($value->id, $users_bonos)){

              $value->bono = true;
            }

            $usuarios[] = $value;
			      array_push($ids_empleados, $value->id);
          }
        }
		  
		    if ($id_empleado != 0){
			
	  		  if (!in_array($id_empleado, $ids_empleados)){
				
				    if (in_array($id_empleado, $cambiar_valores)){
					  
					    $temp->estado = 1;
				    }

            if (in_array($id_empleado, $users_bonos)){

              $temp->bono = true;
            }

            $temp->id_plan = '';
            $temp->fecha_registrado = '';
        		$temp->fecha_solicitado = '';
            $temp->fecha_autorizado = '';
            $temp->fecha_revisado = '';
				    $temp->periodo = 0;
				    $temp->status = 0;
            $usuarios[] = $temp;
            array_push($ids_empleados, $id_empleado);
        	}
	  	  }

      	$users = $usuarios;
        $director_revisions = DB::table('revision_resultados_director')->where('plan_id', $plan->id)->pluck('created_at', 'user_id')->toArray();
      }
			
      return view('evaluacion-resultados/estadistica-avance', compact('users', 'plan', 'planes', 'alimentar_propuestas', 'num_users', 'objetivos_autorizados', 'logros_revisados', 'user', 'director_revisions'));
    }

    /**
     * Muestra la pagina para ver el listado de personas con bono ganado
     *
     * @return void
     */
    public function bonos(){

      // Obtiene todos los planes abiertos ordenados por los mas recientes
      $planes = DB::select('SELECT id, nombre, tipo, alimentar_propuestas, registrar_logros, registrar_logros_abierto FROM planes where estado = ? ORDER BY id DESC', [2]);

      $users = array();
      $user = Employee::where('id', auth()->user()->employee_id)->select('idempleado')->first();

      // Hay planes abiertos
      if (!empty($planes)){

        // Obtiene el plan abierto mas reciente
        $plan = $planes[0];

        // Se requiere mostrar un plan por default
        if (!empty($_POST['id_plan'])){

          foreach ($planes as $key => $value){
            
            if ($_POST['id_plan'] == $value->id){

              $plan = $value;
            }
          }
        }

        if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

          // Obtiene los datos de todos los usuarios con algun logro revisado
          $users = DB::table('objetivos')->join('employees', 'employees.id', '=', 'objetivos.id_empleado')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->join('logros', 'logros.objetivo_id', '=', 'objetivos.id')->where('objetivos.id_plan', $plan->id)->where('revisado', 1)->select('employees.id', 'employees.nombre', 'paterno', 'materno', 'idempleado', 'job_positions.name AS job_position', 'departments.name AS department')->groupBy('employees.id')->get();
        }

        else{

          //if (!empty($id_jefe)){

            // Obtiene los datos de todos los subordinados del usuario logueado
            $users = DB::table('objetivos')->join('employees', 'employees.id', '=', 'objetivos.id_empleado')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->join('logros', 'logros.objetivo_id', '=', 'objetivos.id')->where('objetivos.id_plan', $plan->id)->where('jefe', $user->idempleado)->where('revisado', 1)->select('employees.id', 'employees.nombre', 'paterno', 'materno', 'idempleado', 'job_positions.name AS job_position', 'departments.name AS department')->groupBy('employees.id')->get();
          //}
        }

        $temp_users = array();

        foreach ($users as $key => $value){
          
          $bono = DB::table('bonos_planes')->where('id_plan', $plan->id)->where('id_user', $value->id)->select('total_bono')->first();
          $value->bono = $bono;
          $temp_users[] = $value;
        }

        $users = $temp_users;
      }
      
      return view('evaluacion-resultados/bonos', compact('users', 'plan', 'planes', 'user'));
    }

    /**
     * Muestra la pagina para ver el detalle del bono de un empleado
     *
     * @return void
     */
    public function detalle_bono(){

      // Obtiene el id del usuario logueado
      $id_user = auth()->user()->employee_id;

      $bono = 0;
      $metas = array();

      // Se obtiene el id del usuario a revisarle el detalle del bono
      if (!empty($_POST['id_empleado']) || !empty(Session::get('id_empleado'))){

        if (!empty($_POST['id_empleado'])){

          $id_user = $_POST['id_empleado'];
        }

        else{

          $id_user = Session::get('id_empleado');
          Session::forget('id_empleado');
        }
      }

      // Obtiene todos los planes abiertos ordenados por los mas recientes
      $planes = DB::select('SELECT id, nombre, anio, alimentar_propuestas FROM planes WHERE estado = ? ORDER BY id DESC', [2]);

      $user = $objetivos = array();
      $alimentar_propuestas = 1;

      // Hay planes abiertos
      if (!empty($planes)){

        // Obtiene el plan abierto mas reciente
        $plan = $planes[0];

        // Se requiere mostrar un plan por default
        if (!empty($_POST['id_plan']) || !empty(Session::get('id_plan'))){

          if (!empty($_POST['id_plan'])){

            $id_plan = $_POST['id_plan'];
          }

          else{

            $id_plan = Session::get('id_plan');
            Session::forget('id_plan');
          }

          foreach ($planes as $key => $value){
            
            if ($id_plan == $value->id){

              $plan = $value;
            }
          }
        }

        if (!empty($id_user)){

          $alimentar_propuestas = $plan->alimentar_propuestas;

          // Obtiene los datos del empleado
          $user = Employee::find($id_user);

          // Obtiene los objetivos o propuestas del usuario junto con los logros por perido de cada propuesta
          //$objetivos = DB::select('SELECT objetivos.id, objetivos.nombre, objetivos.peso, objetivos.frecuencia, objetivos.tipo, objetivos.valor_rojo, objetivos.valor_verde, ejes_estrategicos.nombre AS nombre_eje, objetivos.objetivo, objetivos.periodo, logros.logro, logros.revisado, logros.periodo AS periodo_logro FROM objetivos LEFT JOIN ejes_estrategicos on (ejes_estrategicos.id = objetivos.id_eje) LEFT JOIN logros on (logros.objetivo_id = objetivos.id AND logros.id_plan = objetivos.id_plan AND logros.id_empleado = objetivos.id_empleado) WHERE objetivos.id_plan = ? AND objetivos.id_empleado = ? ORDER BY objetivos.id, periodo_logro', [$plan->id, $id_user]);
          $objetivos = DB::select("SELECT objetivos.id, objetivos.nombre, objetivos.peso, objetivos.frecuencia, objetivos.tipo, objetivos.objetivo, objetivos.valor_rojo, objetivos.valor_verde, objetivos.periodo, objetivos.id_objetivo_corporativo, tipo_bono, logros.logro, logros.revisado, logros.periodo AS periodo_logro FROM objetivos INNER JOIN objetivos_corporativos ON (objetivos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) LEFT JOIN logros ON (logros.objetivo_id = objetivos.id AND logros.id_empleado = objetivos.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan = ? AND objetivos.id_empleado = ? ORDER BY objetivos.id, periodo_logro", [$plan->id, $id_user]);

          $nuevos_objetivos = array();
          $actual_objetivo = 0;
          $valores_objetivo = array();
          $contador_metas = 0;

          foreach ($objetivos as $key => $objetivo){
                
            if ($actual_objetivo != $objetivo->id){

              $actual_objetivo = $objetivo->id;
              $bonos_objetivos = DB::table('bonos_objetivos')->where('id_objetivo', $objetivo->id)->get();
              $valores_objetivo = DB::table('valores_objetivo')->where('id_objetivo', $objetivo->id)->orderBy('periodo')->get();
              $objetivo->bonos = $bonos_objetivos;
              $objetivo->valores = $valores_objetivo;
            }

            $index = $key + 1;

            if (empty($objetivos[$index]) || $objetivos[$index]->id != $actual_objetivo){

              if ($objetivo->tipo_bono != 'Comision'){

                if (count($valores_objetivo) > 0){

                  if (empty($objetivo->periodo_logro)){

                    $metas[$contador_metas] = $valores_objetivo[0]->meta;
                  }

                  else{

                    $found = false;

                    foreach ($valores_objetivo as $key2 => $valor_objetivo){
                      
                      if ($valor_objetivo->periodo == $objetivo->periodo_logro){

                        $metas[$contador_metas] = $valor_objetivo->meta;
                        $found = true;
                        break;
                      }

                      else{

                        if ($valor_objetivo->periodo > $objetivo->periodo_logro){

                          if ($key > 0){

                            $index2 = $key - 1;
                            $metas[$contador_metas] = $valores_objetivo[$index2]->meta;
                            $found = true;
                            break;
                          }
                        }
                      }
                    }

                    if (!$found){

                      $metas[$contador_metas] = $valores_objetivo[0]->meta;
                    }
                  }

                  $contador_metas++;
                }
              }
            }

            $nuevos_objetivos[] = $objetivo;
          }

          $objetivos = $nuevos_objetivos;
        
          // Obtiene los mensajes o notas del usuario con su jefe
          if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

            if ($alimentar_propuestas == 3){

              $notas = DB::select('SELECT nombre, paterno, materno, mensaje, mensajes.fecha, mensajes.id FROM mensajes INNER JOIN employees ON (employees.id = mensajes.id_de) WHERE id_plan = ? AND (mensajes.id_de = ? OR mensajes.id_para = ?) ORDER BY mensajes.id DESC', [$plan->id, $id_user, $id_user]);
            }

            else{

              if (!empty($user->boss)){

              $notas = DB::select('SELECT nombre, paterno, materno, mensaje, mensajes.fecha, mensajes.id FROM mensajes INNER JOIN employees ON (employees.id = mensajes.id_de) WHERE id_plan = ? AND ((mensajes.id_de = ? AND mensajes.id_para = ?) OR (mensajes.id_para = ?)) ORDER BY mensajes.id DESC', [$plan->id, $id_user, $user->boss->id, $id_user]);
              }
            }
          }

          else{

            if (!empty($user->boss)){

              $notas = DB::select('SELECT nombre, paterno, materno, mensaje, mensajes.fecha, mensajes.id FROM mensajes INNER JOIN employees ON (employees.id = mensajes.id_de) WHERE id_plan = ? AND ((mensajes.id_de = ? AND mensajes.id_para = ?) OR (mensajes.id_para = ? AND mensajes.id_de = ?)) ORDER BY mensajes.id DESC', [$plan->id, $id_user, $user->boss->id, $id_user, $user->boss->id]);
            }
          }

          $bono = DB::table('bonos_planes')->where('id_plan', $plan->id)->where('id_user', $id_user)->select('total_bono')->first();
        }
      }

      $objetivos_corporativos_sin_responsable = DB::select("SELECT objetivos_corporativos.id, id_tipo, letter FROM objetivos_corporativos INNER JOIN tipos_objetivos_corporativos ON (tipos_objetivos_corporativos.id = objetivos_corporativos.id_tipo) ORDER BY objetivos_corporativos.id_tipo, objetivos_corporativos.id");
      return view('evaluacion-resultados/detalle-bono', compact('plan', 'planes', 'id_user', 'user', 'objetivos', 'objetivos_corporativos_sin_responsable', 'notas', 'bono', 'alimentar_propuestas', 'metas')); 
    }

    /**
     * Muestra la pagina para ver el último cambio sobre el bono
     *
     * @return void
     */
    public function ultimo_bono(){

      // Obtiene el id del usuario logueado
      $id_user = auth()->user()->employee_id;

      $bono = 0;

      // Se obtiene el id del usuario a revisarle el detalle del bono
      if (!empty($_POST['id_empleado']) || !empty(Session::get('id_empleado'))){

        if (!empty($_POST['id_empleado'])){

          $id_user = $_POST['id_empleado'];
        }

        else{

          $id_user = Session::get('id_empleado');
          Session::forget('id_empleado');
        }
      }

      // Obtiene todos los planes abiertos ordenados por los mas recientes
      $planes = DB::select('SELECT id, nombre, anio, alimentar_propuestas FROM planes WHERE estado = ? ORDER BY id DESC', [2]);

      $user = $objetivos = array();
      $alimentar_propuestas = 1;

      // Hay planes abiertos
      if (!empty($planes)){

        // Obtiene el plan abierto mas reciente
        $plan = $planes[0];

        // Se requiere mostrar un plan por default
        if (!empty($_POST['id_plan']) || !empty(Session::get('id_plan'))){

          if (!empty($_POST['id_plan'])){

            $id_plan = $_POST['id_plan'];
          }

          else{

            $id_plan = Session::get('id_plan');
            Session::forget('id_plan');
          }

          foreach ($planes as $key => $value){
            
            if ($id_plan == $value->id){

              $plan = $value;
            }
          }
        }

        if (!empty($id_user)){

          $alimentar_propuestas = $plan->alimentar_propuestas;

          // Obtiene los datos del empleado
          $user = DB::table('employees')->where('id', $id_user)->first();

          // Obtiene los objetivos o propuestas del usuario junto con los logros por perido de cada propuesta
          //$objetivos = DB::select('SELECT objetivos.id, objetivos.nombre, objetivos.peso, objetivos.frecuencia, objetivos.tipo, objetivos.valor_rojo, objetivos.valor_verde, ejes_estrategicos.nombre AS nombre_eje, objetivos.objetivo, objetivos.periodo, logros.logro, logros.revisado, logros.periodo AS periodo_logro FROM objetivos LEFT JOIN ejes_estrategicos on (ejes_estrategicos.id = objetivos.id_eje) LEFT JOIN logros on (logros.objetivo_id = objetivos.id AND logros.id_plan = objetivos.id_plan AND logros.id_empleado = objetivos.id_empleado) WHERE objetivos.id_plan = ? AND objetivos.id_empleado = ? ORDER BY objetivos.id, periodo_logro', [$plan->id, $id_user]);
          $objetivos = DB::select("SELECT objetivos.id, objetivos.nombre, objetivos.peso, objetivos.tipo, objetivos.objetivo, objetivos.valor_rojo, objetivos.valor_verde, objetivos_corporativos.name, tipo_bono, logros.logro, logros.periodo FROM objetivos INNER JOIN objetivos_corporativos ON (objetivos.id_objetivo_corporativo = objetivos_corporativos.id) INNER JOIN logros ON (logros.objetivo_id = objetivos.id) WHERE objetivos.id_plan = ? AND objetivos.id_empleado = ? AND revisado = ? ORDER BY objetivos.id, periodo DESC", [$plan->id, $id_user, 1]);

          $nuevos_objetivos = array();
          $actual_objetivo = 0;

          foreach ($objetivos as $key => $objetivo){
                
            if ($actual_objetivo != $objetivo->id){

              $actual_objetivo = $objetivo->id;
              $bonos_objetivos = DB::table('bonos_objetivos')->where('id_objetivo', $objetivo->id)->get();
              $objetivo->bonos = $bonos_objetivos;
              $valor_objetivo = DB::table('valores_objetivo')->where('id_objetivo', $objetivo->id)->where('periodo', $objetivo->periodo)->first();

              if (!empty($valor_objetivo)){

                if ($objetivo->tipo_bono == 'Comision'){

                  $objetivo->bono = $valor_objetivo->meta;
                }

                else{

                  $objetivo->objetivo = $valor_objetivo->meta;
                  $objetivo->valor_rojo = $valor_objetivo->valor_rojo;
                  $objetivo->valor_verde = $valor_objetivo->valor_verde;
                }
              }
            }

            $nuevos_objetivos[] = $objetivo;
          }

          $objetivos = $nuevos_objetivos;
        
          // Obtiene los mensajes o notas del usuario con su jefe
          if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

            if ($alimentar_propuestas == 3){

              $notas = DB::select('SELECT nombre, paterno, materno, mensaje, mensajes.fecha, mensajes.id FROM mensajes INNER JOIN employees ON (employees.id = mensajes.id_de) WHERE id_plan = ? AND (mensajes.id_de = ? OR mensajes.id_para = ?) ORDER BY mensajes.id DESC', [$plan->id, $id_user, $id_user]);
            }

            else{

              if (!empty($user->boss)){

                $notas = DB::select('SELECT nombre, paterno, materno, mensaje, mensajes.fecha, mensajes.id FROM mensajes INNER JOIN employees ON (employees.id = mensajes.id_de) WHERE id_plan = ? AND ((mensajes.id_de = ? AND mensajes.id_para = ?) OR (mensajes.id_para = ?)) ORDER BY mensajes.id DESC', [$plan->id, $id_user, $user->boss->id, $id_user]);
              }
            }
          }

          else{

            if (!empty($user->boss)){

              $notas = DB::select('SELECT nombre, paterno, materno, mensaje, mensajes.fecha, mensajes.id FROM mensajes INNER JOIN employees ON (employees.id = mensajes.id_de) WHERE id_plan = ? AND ((mensajes.id_de = ? AND mensajes.id_para = ?) OR (mensajes.id_para = ? AND mensajes.id_de = ?)) ORDER BY mensajes.id DESC', [$plan->id, $id_user, $user->boss->id, $id_user, $user->boss->id]);
            }
          }

          $bono = DB::table('bonos_planes')->where('id_plan', $plan->id)->where('id_user', $id_user)->select('total_bono')->first();
        }
      }

      return view('evaluacion-resultados/ultimo-bono', compact('plan', 'planes', 'id_user', 'user', 'objetivos', 'notas', 'bono', 'alimentar_propuestas')); 
    }
	
	/**
     * Se agrega una nueva recomendacion para el informe consolidado
     *
     * @return string
     */
    public function agregar_recomendacion(){

      // Id del usuario logueado
      $id_empleado = auth()->user()->employee_id;

      $id_plan = $_POST['id_plan'];
	    $id_periodo = $_POST['id_periodo'];

      // Si existe el id del empleado
      if (!empty($_POST['id_empleado'])){

        $id_empleado = $_POST['id_empleado'];
        
        // Id del jefe
        $id_jefe = auth()->user()->employee_id;
        
        // Agrega una nueva nota
        $nota = DB::insert('INSERT INTO recomendaciones_informe (id_empleado, boss_id, recomendacion, id_plan, id_periodo) VALUES (?,?,?,?,?)', [$id_empleado, $id_jefe, $_POST['mensaje'], $id_plan, $id_periodo]);
        
        // Obtiene el nombre del jefe
        $jefe = DB::select('SELECT nombre, paterno, materno FROM employees WHERE id = ?', [$id_jefe]);
        echo $jefe[0]->nombre . ' ' . $jefe[0]->paterno . ' ' . $jefe[0]->materno;
      }
    }

    /**
     * Muestra la pagina para configurar quien alimenta las propuestas
     *
     * @return void
     */
    public function configuracion(){

      // El usuario logueado es admin
      if (!empty(Session::get('midete_admin'))){
		  
		// Obtiene todos los planes abiertos ordenados por los mas recientes
      	$planes = DB::select('SELECT id FROM planes WHERE estado = ?', [2]);
		  
		if (count($planes) == 0){

        	$alimentar_propuestas = 1;

        	// Se desea guardar el valor
        	if (!empty($_POST['alimentar_propuestas'])){

          		$alimentar_propuestas = $_POST['alimentar_propuestas'];
          		DB::update("UPDATE configuraciones SET valor = '$alimentar_propuestas' WHERE nombre = ?", ['alimentar_propuestas']);
          		flash('Datos guardados');
        	}

        	else{

          		$configuraciones = DB::select("SELECT valor FROM configuraciones WHERE nombre = ?", ['alimentar_propuestas']);
          		$alimentar_propuestas = $configuraciones[0]->valor;
        	}

        	return view('evaluacion-resultados/configuracion', compact('alimentar_propuestas'));
		}
		  
		else{
			
			flash('Hay planes abiertos');
			return redirect()->to('planes');
		}
      }

      else{

        return redirect()->to('que-es-evaluacion-resultados');
      }
    }
	
	/**
     * Agrega o actualiza registros para decidir si un usuario puede cambiar valores de periodos pasados
     *
     * @return void
     */
    public function cambiar_valores_periodos(){

      // El usuario logueado es admin
      if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){
		  
		    $estado = $_POST['estado'];
		  
		// Obtiene el registro del usuario y el plan si es que existe
      	$cambiar_valores = DB::select('SELECT id_empleado FROM cambiar_valores_periodos WHERE id_empleado = ? AND id_plan = ?', [$_POST['id_empleado'], $_POST['id_plan']]);
		  
		    if (count($cambiar_valores) > 0){

        	DB::update("UPDATE cambiar_valores_periodos SET estado = $estado WHERE id_empleado = ? AND id_plan = ?", [$_POST['id_empleado'], $_POST['id_plan']]);    		
        }
		  
		    else{
			
			    DB::insert("INSERT INTO cambiar_valores_periodos VALUES (?,?,?)", [$_POST['id_empleado'], $_POST['id_plan'], $estado]);
		    }	
      }
    }

    /**
     * Copia objetivos de un plan a otro
     *
     * @return void
     */
    public function copiar_objetivos(){

      $id_empleado = $_POST['id_empleado'];
      $plan_para_copiar = $_POST['plan_para_copiar'];
      $plan_a_copiar = $_POST['plan_a_copiar'];
      DB::delete("DELETE FROM objetivos WHERE id_empleado = ? AND id_plan = ?", [$id_empleado, $plan_a_copiar]);

      if (!empty($_POST['colaborador'])){
      
        // Obtiene los objetivos
        $objetivos = DB::select('SELECT * FROM objetivos WHERE id_empleado = ? AND id_plan = ?', [$_POST['colaborador'], $plan_para_copiar]);

        foreach ($objetivos as $key => $objetivo){
        
          $new = array();
          $new['nombre'] = $objetivo->nombre;
          $new['formula'] = $objetivo->formula;
          $new['id_plan'] = $plan_a_copiar;
          $new['id_catalogo'] = $objetivo->id_catalogo;
          $new['id_objetivo_corporativo'] = $objetivo->id_objetivo_corporativo;
          $new['id_empleado'] = $id_empleado;
          $new['tipo'] = $objetivo->tipo;
          $new['peso'] = $objetivo->peso;
          $new['objetivo'] = $objetivo->objetivo;
          $new['frecuencia'] = $objetivo->frecuencia;
          $new['periodo'] = $objetivo->periodo;
          $new['valor_rojo'] = $objetivo->valor_rojo;
          $new['valor_verde'] = $objetivo->valor_verde;
          $new['viejo'] = $objetivo->viejo;
          $new['unique_id'] = $objetivo->unique_id;
          $new['tipo_bono'] = $objetivo->tipo_bono;
          $new['updated_by'] = auth()->user()->id;
          $new['created_at'] = date('Y-m-d H:i:s');
          DB::table('objetivos')->insert($new);
        }

        flash('Los objetivos fueron copiados correctamente');
      }

      else{

        flash('No se seleccionó colaborador para copiar objetivos');
      }

      Session::put('id_empleado', $id_empleado);
      Session::put('id_plan', $plan_a_copiar);
      return redirect('/propuesta');
    }
	
	/**
     * Guarda la aceptacion del empleado de sus objetivos y resultados
     *
     * @return void
     */
    public function aceptar_objetivos_resultados(){

      $user_accept = array();
      $user_accept['id_empleado'] = $_POST['id_empleado'];
      $user_accept['id_plan'] = $_POST['id_plan'];
      $user_accept['tipo'] = $_POST['tipo'];
      DB::table('user_accepts')->insert($user_accept);
      $user = Employee::find($_POST['id_empleado']);
      $mensaje = array();
      $mensaje['id_de'] = $_POST['id_empleado'];
      $mensaje['id_para'] = $user->boss->id;
      $mensaje['id_plan'] = $_POST['id_plan'];
      $mensaje['mensaje'] = 'Ha aceptado los ' . $_POST['tipo'];
      $mensaje['fecha'] = date('Y-m-d');
      DB::table('mensajes')->insert($mensaje);
      Session::put('id_empleado', $user_accept['id_empleado']);
      Session::put('id_plan', $user_accept['id_plan']);
      flash('Ha aceptado los ' . $user_accept['tipo']);
      return redirect('/propuesta');
    }
}