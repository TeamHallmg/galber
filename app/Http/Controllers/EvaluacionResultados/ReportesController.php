<?php

namespace App\Http\Controllers\EvaluacionResultados;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Employee;
use Session;

class ReportesController extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Reportes Controller
    |--------------------------------------------------------------------------
    |
    | Add Documentation
    |
    */

    /**
     * Create a new Reportes controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Reporte Global
     *
     * @return void
     */
    public function reporte_global(){
    	
      $users = array();
      $departments = array();
      $departments2 = array();
      $id_jefe = auth()->user()->employee_id;
      $jefe = Employee::find($id_jefe);
      $anio_plan = 0;
      $anios = array();
      $ids_planes = '';
      $ids_plans = array();
      $planes = DB::select("SELECT id, anio FROM planes WHERE estado = ? OR estado = ? ORDER BY anio DESC", [2, 3]);

      if (!empty($planes)){

        // Obtiene el año mas reciente
        $anio_plan = $planes[0]->anio;

        // Se requiere mostrar un plan por default
        if (!empty($_POST['anio_plan'])){

          $anio_plan = $_POST['anio_plan'];
        }

        foreach ($planes as $key => $value){
            
          if (!in_array($value->anio, $anios)){

            $anios[] = $value->anio;
          }

          if ($value->anio == $anio_plan){

            $ids_planes .= ',' . $value->id;
          }
        }

        $ids_planes = substr($ids_planes, 1);
        $ids_plans = explode(',', $ids_planes);

        if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

          $departments = DB::select("SELECT COUNT(DISTINCT employees.id) AS total_usuarios, departments.name FROM employees INNER JOIN job_positions ON (job_positions.id = employees.job_position_id) INNER JOIN areas ON (areas.id = job_positions.area_id) INNER JOIN departments ON (departments.id = areas.department_id) INNER JOIN objetivos ON (employees.id = objetivos.id_empleado) INNER JOIN logros ON (objetivos.id = logros.objetivo_id AND objetivos.id_empleado = logros.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan IN ($ids_planes) AND logros.revisado = 1 GROUP BY departments.name ORDER BY departments.name");
			
		      if (!empty($_POST['department'])){

            $department = $_POST['department'];

            $departments2 = DB::select("SELECT COUNT(DISTINCT employees.id) AS total_usuarios, departments.name FROM employees INNER JOIN job_positions ON (job_positions.id = employees.job_position_id) INNER JOIN areas ON (areas.id = job_positions.area_id) INNER JOIN departments ON (departments.id = areas.department_id) INNER JOIN objetivos ON (employees.id = objetivos.id_empleado) INNER JOIN logros ON (objetivos.id = logros.objetivo_id AND objetivos.id_empleado = logros.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan IN ($ids_planes) AND logros.revisado = 1 AND departments.name = '$department' GROUP BY departments.name ORDER BY departments.name");

            // Obtiene los datos de todos los usuarios de la respectiva area
            $users = DB::select("SELECT employees.nombre, paterno, materno, direccion, employees.id, departments.name AS department, job_positions.name AS job_position, frecuencia, peso, valor_rojo, valor_verde, logro, logros.periodo FROM employees INNER JOIN job_positions ON (job_positions.id = employees.job_position_id) INNER JOIN areas ON (areas.id = job_positions.area_id) INNER JOIN departments ON (departments.id = areas.department_id) INNER JOIN objetivos ON (employees.id = objetivos.id_empleado) INNER JOIN logros ON (objetivos.id = logros.objetivo_id AND objetivos.id_empleado = logros.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan IN ($ids_planes) AND revisado = 1 AND departments.name = '$department' ORDER BY departments.name, employees.id, logros.periodo");
          }

          else{

          	// Obtiene los datos de todos los usuarios
          	$users = DB::select("SELECT employees.nombre, paterno, materno, direccion, employees.id, departments.name AS department, job_positions.name AS job_position, frecuencia, peso, valor_rojo, valor_verde, logro, logros.periodo FROM employees INNER JOIN job_positions ON (job_positions.id = employees.job_position_id) INNER JOIN areas ON (areas.id = job_positions.area_id) INNER JOIN departments ON (departments.id = areas.department_id) INNER JOIN objetivos ON (employees.id = objetivos.id_empleado) INNER JOIN logros ON (objetivos.id = logros.objetivo_id AND objetivos.id_empleado = logros.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan IN ($ids_planes) AND revisado = 1 ORDER BY departments.name, employees.id, logros.periodo");
		      }
        }

        else{

          //$users = DB::select("SELECT id, idempleado FROM employees WHERE jefe = ?", [$jefe->idempleado]);
          $users = DB::table('employees')->where('jefe', $jefe->idempleado)->pluck('idempleado')->toArray();

          if (count($users) > 0){

            $final_users = $users;
            /*$counter = 0;

            while (count($users) > 0 && $counter < 7){
              
              $ids = $users;*/

              /*foreach ($users as $key => $user){
                
                $ids[] = $user->idempleado;
              }*/

              //$users_string = implode(',', $ids);
              //$final_users .= ',' . $users_string;
              //$final_users = array_merge($final_users, $ids);
              //$users = DB::select("SELECT id, idempleado FROM employees WHERE jefe IN ($users_string)");
              //$users = DB::table('employees')->whereIn('jefe', $ids)->pluck('idempleado')->toArray();
              //$counter++;
//            }

            //$final_users = substr($final_users, 1);
            //$departments = DB::select("SELECT COUNT(DISTINCT employees.id) AS total_usuarios, departments.name FROM employees INNER JOIN job_positions ON (job_positions.id = employees.job_position_id) INNER JOIN areas ON (areas.id = job_positions.area_id) INNER JOIN departments ON (departments.id = areas.department_id) INNER JOIN objetivos ON (employees.id = objetivos.id_empleado) INNER JOIN logros ON (objetivos.id = logros.objetivo_id AND objetivos.id_empleado = logros.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan IN ($ids_planes) AND idempleado IN ($final_users) AND employees.id <> $jefe->id AND revisado = 1 GROUP BY departments.name ORDER BY departments.name");
            $departments = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->join('objetivos', 'objetivos.id_empleado', '=', 'employees.id')->join('logros', 'logros.objetivo_id', '=', 'objetivos.id')->whereIn('objetivos.id_plan', $ids_plans)->whereIn('idempleado', $final_users)->where('employees.id', '!=', $jefe->id)->where('revisado', 1)->select('departments.name')->groupBy('departments.name')->orderBy('departments.name')->get();
			  
			      if (!empty($_POST['department'])){

              $department = $_POST['department'];

              //$departments2 = DB::select("SELECT COUNT(DISTINCT employees.id) AS total_usuarios, departments.name FROM employees INNER JOIN job_positions ON (job_positions.id = employees.job_position_id) INNER JOIN areas ON (areas.id = job_positions.area_id) INNER JOIN departments ON (departments.id = areas.department_id) INNER JOIN objetivos ON (employees.id = objetivos.id_empleado) INNER JOIN logros ON (objetivos.id = logros.objetivo_id AND objetivos.id_empleado = logros.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan IN ($ids_planes) AND idempleado IN ($final_users) AND employees.id <> $jefe->id AND revisado = 1 AND departments.name = '$department' GROUP BY departments.name ORDER BY departments.name");
              $departments2 = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->join('objetivos', 'objetivos.id_empleado', '=', 'employees.id')->join('logros', 'logros.objetivo_id', '=', 'objetivos.id')->whereIn('objetivos.id_plan', $ids_plans)->whereIn('idempleado', $final_users)->where('employees.id', '!=', $jefe->id)->where('revisado', 1)->where('departments.name', $department)->select('departments.name')->groupBy('departments.name')->orderBy('departments.name')->get();

              // Obtiene los datos de todos los subordinados del usuario logueado en el area
              //$users = DB::select("SELECT employees.nombre, paterno, materno, employees.id, departments.name AS department, job_positions.name AS job_position, frecuencia, peso, valor_rojo, valor_verde, logro, logros.periodo FROM employees INNER JOIN job_positions ON (job_positions.id = employees.job_position_id) INNER JOIN areas ON (areas.id = job_positions.area_id) INNER JOIN departments ON (departments.id = areas.department_id) LEFT JOIN objetivos ON (employees.id = objetivos.id_empleado) INNER JOIN logros ON (objetivos.id = logros.objetivo_id AND objetivos.id_empleado = logros.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan IN ($ids_planes) AND revisado = 1 AND idempleado IN ($final_users) AND employees.id <> $jefe->id AND department = '$department' ORDER BY department, employees.id, logros.periodo");
              $users = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('objetivos', 'objetivos.id_empleado', '=', 'employees.id')->join('logros', 'logros.objetivo_id', '=', 'objetivos.id')->whereIn('objetivos.id_plan', $ids_plans)->whereIn('idempleado', $final_users)->where('employees.id', '!=', $jefe->id)->where('revisado', 1)->where('department', $department)->select('employees.nombre', 'paterno', 'materno', 'direccion', 'employees.id', 'departments.name AS department', 'job_positions.name AS job_position', 'frecuencia', 'peso', 'valor_rojo', 'valor_verde', 'logro', 'logros.periodo')->orderBy('departments.name')->orderBy('employees.id')->orderBy('logros.periodo')->get();
            }

            else{

            	// Obtiene los datos de todos los subordinados del usuario logueado
            	//$users = DB::select("SELECT employees.nombre, paterno, materno, employees.id, departments.name AS department, job_positions.name AS job_position, frecuencia, peso, valor_rojo, valor_verde, logro, logros.periodo FROM employees INNER JOIN job_positions ON (job_positions.id = employees.job_position_id) INNER JOIN areas ON (areas.id = job_positions.area_id) INNER JOIN departments ON (departments.id = areas.department_id) LEFT JOIN objetivos ON (employees.id = objetivos.id_empleado) INNER JOIN logros ON (objetivos.id = logros.objetivo_id AND objetivos.id_empleado = logros.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan IN ($ids_planes) AND revisado = 1 AND idempleado IN ($final_users) AND employees.id <> $jefe->id ORDER BY department, employees.id, logros.periodo");
              $users = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('objetivos', 'objetivos.id_empleado', '=', 'employees.id')->join('logros', 'logros.objetivo_id', '=', 'objetivos.id')->whereIn('objetivos.id_plan', $ids_plans)->whereIn('idempleado', $final_users)->where('employees.id', '!=', $jefe->id)->where('revisado', 1)->select('employees.nombre', 'paterno', 'materno', 'direccion', 'employees.id', 'departments.name AS department', 'job_positions.name AS job_position', 'frecuencia', 'peso', 'valor_rojo', 'valor_verde', 'logro', 'logros.periodo')->orderBy('departments.name')->orderBy('employees.id')->orderBy('logros.periodo')->get();
			      }
          }
        }
      }

      return view('evaluacion-resultados/reportes/reporte-global', compact('users', 'anios', 'anio_plan', 'departments', 'departments2'));
    }

    /**
     * Reporte Individual
     *
     * @return void
     */
    public function reporte_individual(){
      
      $user = array();
      $objetivos = array();
      $anio_plan = 0;
      $anios = array();
      $ids_planes = '';
	    $id = $_POST['id_empleado'];
      $planes = DB::select("SELECT id, anio FROM planes WHERE estado = ? OR estado = ? ORDER BY anio DESC", [2, 3]);

      if (!empty($planes)){

        // Obtiene el año mas reciente
        $anio_plan = $planes[0]->anio;

        // Se requiere mostrar un plan por default
        if (!empty($_POST['anio_plan'])){

          $anio_plan = $_POST['anio_plan'];
        }

        foreach ($planes as $key => $value){
            
          if (!in_array($value->anio, $anios)){

            $anios[] = $value->anio;
          }

          if ($value->anio == $anio_plan){

            $ids_planes .= ',' . $value->id;
          }
        }

        $ids_planes = substr($ids_planes, 1);

        // Obtiene los datos del usuario
        //$user = DB::select("SELECT id, first_name, last_name, division, subdivision FROM users WHERE id = ?", [$id]);
        $user = Employee::find($id);

        // Obtiene los objetivos y logros
        $objetivos = DB::select("SELECT objetivos.id, objetivos.nombre, frecuencia, peso, valor_rojo, valor_verde, logro, logros.periodo, planes.nombre AS nombre_plan FROM objetivos INNER JOIN planes ON (objetivos.id_plan = planes.id) INNER JOIN logros ON (objetivos.id = logros.objetivo_id AND objetivos.id_empleado = logros.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan IN ($ids_planes) AND objetivos.id_empleado = $id AND revisado = 1 ORDER BY objetivos.id, logros.periodo");
      }

      return view('evaluacion-resultados/reportes/reporte-individual', compact('user', 'anio_plan', 'objetivos', 'anios'));
    }
	
	/**
  * Calculo del bono
  *
  * @return void
  */
  public function calculo_bono($id_plan){

    require_once 'PHPExcel.php';
    require_once 'PHPExcel/IOFactory.php';

    // Create new PHPExcel object
    $objPHPExcel = new \PHPExcel();

    // Create a first sheet, representing users data
    $objPHPExcel->setActiveSheetIndex(0);
    
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'CLAVE');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'CONCEPTO');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'CAP1');
	  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'CAP2');
	  $objPHPExcel->getActiveSheet()->setCellValue('E1', 'CAP3');
	  $objPHPExcel->getActiveSheet()->setCellValue('F1', '#');
	  $objPHPExcel->getActiveSheet()->setCellValue('G1', 'CODIGO');
	  $objPHPExcel->getActiveSheet()->setCellValue('H1', '');

    $users = array();

    if (!empty($id_plan)){

      $plan = DB::select("SELECT nombre, anio FROM planes WHERE id = ?", [$id_plan]);

      if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

        // Obtiene los datos de todos los usuarios
        $users = DB::select("SELECT users.first_name, users.last_name, users.id, users.rfc, users.division, users.subdivision, users.started_at, users.employee_number, objetivos.frecuencia, objetivos.peso, objetivos.objetivo, objetivos.periodo, logros.logro, logros.periodo FROM users INNER JOIN objetivos ON (users.id = objetivos.id_empleado) INNER JOIN logros ON (objetivos.id = logros.objetivo_id AND objetivos.id_empleado = logros.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan = ? AND logros.revisado = ? ORDER BY users.rfc, logros.periodo", [$id_plan, 1]);

        if (count($users) > 0){

          $i = 1;
		      $current_user = '';
          $current_objective = '';
          $bono = 0;
          $merece_bono = false;
          $sueldo = 0;
          $puestos = array();
          $departamentos = array();
          $bonos = array();
          $frecuencia = '';
          $puesto = '';
          $departamento = '';
		      $region = 0;
	        $employee_number = '';
          $tipo_bono = '';
          $num_dias = 0;
          $absences = 0;
		      $regions = array();
          $id_user = 0;
          $user_bono['id_plan'] = $id_plan;

          foreach ($users as $key => $user){
                
            if ($current_user != $user->rfc){

              if ($current_user != ''){

                if ($merece_bono){

                  if (!empty($puestos[$puesto]) && !empty($departamentos[$departamento])){

                    $bono = $bonos[$puesto];
                    $tipo_bono = $puestos[$puesto];
					          $region = $regions[$puesto];
                  }

                  else{

                    $results = DB::select("SELECT tipo_bono, porcentaje, monto, region FROM bonos WHERE puesto = ? AND departamento = ? AND frecuencia = ?", [$puesto, $departamento, $frecuencia]);

                    if (count($results) > 0){

					            $temp_puesto = '';
                      $temp_bono = '';
                      $temp_region = '';
						
					            foreach ($results as $key => $result){
                        
                        $temp_puesto .= ',' . $result->tipo_bono;
                        $temp_region .= ',' . $result->region;

                        if ($result->tipo_bono == 'M'){

                          $temp_bono .= ',' . $result->monto;
                        }

                        else{

                          $temp_bono .= ',' . $result->porcentaje;
                        }
                      }
						
					            $puestos[$puesto] = substr($temp_puesto, 1);
                      $tipo_bono = $puestos[$puesto];
                      $bonos[$puesto] = substr($temp_bono, 1);
                      $bono = $bonos[$puesto];
                      $regions[$puesto] = substr($temp_region, 1);
                      $region = $regions[$puesto];
                    }
                  }

                  if (!empty($tipo_bono)){

                    $monto = 0;
					          $regions_array = explode(',', $region);
					  
					          if ($regions_array[0] != 0){

                      $results = DB::select("SELECT region FROM user_regions WHERE rfc = ?", [$current_user]);
                      $tipo_bono = explode(',', $tipo_bono);
                      $bono = explode(',', $bono);

                      if (count($results) > 0){
						
					  	          foreach ($regions_array as $key2 => $value){
                        
                        	if ($value == $results[0]->region){

                          	if ($tipo_bono[$key2] == 'P'){

                            	$salario_nominal_mensual = $sueldo * 7 * 52 / 12;
                            	$monto = $salario_nominal_mensual * 45 * 100; 
                          	}

                          	else{

                            	$monto = $bono[$key2];
                          	}
                        	}
                      	}
					            }
                    }

                    else{

                    	if ($tipo_bono == 'P'){

                      	$salario_nominal_mensual = $sueldo * 7 * 52 / 12;
                      	$monto = $salario_nominal_mensual * 45 / 100;
                    	}

                    	else{

                      	$monto = $bono;
                    	}
					          }
					  
                    $monto_a_pagar = $calificacion_final * $monto / 100;
                    $bono_por_dia = $monto_a_pagar / $num_dias;
                    $bono = $bono_por_dia * ($num_dias - $absences);
                  }
                }

                if ($bono != 0){

                  $i++;
                  $objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $employee_number);
                  $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, ($frecuencia == 'Mensual' ? '26' : ($frecuencia == 'Trimestral' ? '07' : '29')));
                  $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, number_format($bono,2));
				          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, '0');
				          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, '0');
				          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, '#');
				          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $employee_number . ' ' . ($frecuencia == 'Mensual' ? '26' : ($frecuencia == 'Trimestral' ? '07' : '29')) . ' ' . number_format($bono,2) . ' 0 0 #');
				          $objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, '51');

                  // Guarda el calculo del bono
                  DB::insert("INSERT INTO users_bonos SET id_plan = $id_plan, id_user = $id_user, bono_total = $bono ON DUPLICATE KEY UPDATE bono_total = $bono");
                }
              }

			        $periodo = $user->periodo;
              $merece_bono = false;
              $absences = 0;
              $calificacion_final = 0;
              $current_user = $user->rfc;
              $sueldo = 0;
              $bono = 0;
              $frecuencia = $user->frecuencia;
              $puesto = $user->subdivision;
              $departamento = $user->division;
			        $employee_number = $user->employee_number;
              $tipo_bono = '';
              $num_dias = 0;
              $id_user = $user->id;
              $results = DB::select("SELECT * FROM user_absences WHERE rfc = ?", [$user->rfc]);

              if ($user->frecuencia == 'Mensual'){

                $num_dias = 30;

                if (count($results) > 0){

                  $sueldo = $results[0]->sueldo;

                  switch ($periodo){
                    
                    case 1:$absences = $results[0]->enero;break;
                    case 2:$absences = $results[0]->febrero;break;
                    case 3:$absences = $results[0]->marzo;break;
                    case 4:$absences = $results[0]->abril;break;
                    case 5:$absences = $results[0]->mayo;break;
                    case 6:$absences = $results[0]->junio;break;
                    case 7:$absences = $results[0]->julio;break;
                    case 8:$absences = $results[0]->agosto;break;
                    case 9:$absences = $results[0]->septiembre;break;
                    case 10:$absences = $results[0]->octubre;break;
                    case 11:$absences = $results[0]->noviembre;break;
                    case 12:$absences = $results[0]->diciembre;
                  }
                }

                if ($periodo < 10){

                  $periodo = '0' . $periodo;
                }

                $fecha_ingreso = $plan[0]->anio . '-' . $periodo . '-01';

                if ($user->started_at <= $fecha_ingreso){

                  if ($absences < 6){
                  
                    $merece_bono = true;
                  }
                }
              }

              else{

                if ($user->frecuencia == 'Trimestral'){

                  $num_dias = 90;

                  if (count($results) > 0){

                    $sueldo = $results[0]->sueldo;

                    switch ($periodo){
                    
                      case 1:$absences = $results[0]->enero + $results[0]->febrero + $results[0]->marzo;break;
                      case 2:$absences = $results[0]->abril + $results[0]->mayo + $results[0]->junio;break;
                      case 3:$absences = $results[0]->julio + $results[0]->agosto + $results[0]->septiembre;break;
                      case 4:$absences = $results[0]->octubre + $results[0]->noviembre + $results[0]->diciembre;
                    }
                  }

                  $periodo = $periodo * 3 - 2;

                  if ($periodo < 10){

                    $periodo = '0' . $periodo;
                  }

                  $fecha_ingreso = $plan[0]->anio . '-' . $periodo . '-07';

                  if ($user->started_at <= $fecha_ingreso){

                    if ($absences < 16){
                  
                      $merece_bono = true;
                    }
                  }
                }

                else{

                  $num_dias = 365;
                  $fecha_ingreso = $plan[0]->anio . '-07-15';

                  if (count($results) > 0){

                    $sueldo = $results[0]->sueldo;
                    $absences = $results[0]->enero + $results[0]->febrero + $results[0]->marzo + $results[0]->abril + $results[0]->mayo + $results[0]->junio + $results[0]->julio + $results[0]->agosto + $results[0]->septiembre + $results[0]->octubre + $results[0]->noviembre + $results[0]->diciembre;
                  }

                  if ($user->started_at <= $fecha_ingreso){

                    if ($absences < 31){

                      $merece_bono = true;
                    }
                  }
                }
              }
            }

            if ($merece_bono){

              if ($user->logro == 'Si' || $user->logro == 'No'){

                if ($user->logro == $user->objetivo){

                  $calificacion_final += $user->peso;
                }
              }

              else{

                $calificacion = $user->logro * $user->peso / ($user->objetivo != 0 ? $user->objetivo : 1);

                if ($calificacion > $user->peso){

                  $calificacion = $user->peso;
                }

                $calificacion_final += $calificacion;
              }                  
            }

            else{

              continue;
            }
          }
			
		      if ($current_user != ''){

          	if ($merece_bono){

            	if (!empty($puestos[$puesto]) && !empty($departamentos[$departamento])){

                $bono = $bonos[$puesto];
                $tipo_bono = $puestos[$puesto];
              }

              else{

                $results = DB::select("SELECT tipo_bono, porcentaje, monto FROM bonos WHERE puesto = ? AND departamento = ? AND frecuencia = ?", [$puesto, $departamento, $frecuencia]);

                if (count($results) > 0){

                  $puestos[$puesto] = $results[0]->tipo_bono;
                  $tipo_bono = $results[0]->tipo_bono;

                  if ($results[0]->tipo_bono == 'M'){

                    $bonos[$puesto] = $results[0]->monto;
                    $bono = $results[0]->monto;
                  }

                  else{

                    $bonos[$puesto] = $results[0]->porcentaje;
                    $bono = $results[0]->porcentaje;
                  }
                }
              }

              if (!empty($tipo_bono)){

                $monto = 0;

                if ($tipo_bono == 'P'){

                  $salario_nominal_mensual = $sueldo * 7 * 52 / 12;
                  $monto = $salario_nominal_mensual * 45 / 100; 
                }

                else{

                  $monto = $bono;
                }

                $monto_a_pagar = $calificacion_final * $monto / 100;
                $bono_por_dia = $monto_a_pagar / $num_dias;
                $bono = $bono_por_dia * ($num_dias - $absences);
              }
           	}

            if ($bono != 0){

              $i++;
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $employee_number);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, ($frecuencia == 'Mensual' ? '26' : ($frecuencia == 'Trimestral' ? '07' : '29')));
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, number_format($bono,2));
			        $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, '0');
			        $objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, '0');
			        $objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, '#');
			        $objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $employee_number . ' ' . ($frecuencia == 'Mensual' ? '26' : ($frecuencia == 'Trimestral' ? '07' : '29')) . ' ' . number_format($bono,2) . ' 0 0 #');
			        $objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, '51');

              // Guarda el calculo del bono
              DB::insert("INSERT INTO users_bonos SET id_plan = $id_plan, id_user = $id_user, bono_total = $bono ON DUPLICATE KEY UPDATE bono_total = $bono");
            }
          }
        }
      }
    }
	  
	  // Redirect output to a client’s web browser (Excel5)
	  header('Content-Type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment;filename="Bonos - ' . $plan[0]->nombre . '.xls"');
	  header('Cache-Control: max-age=0');
	  $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
  }

  /**
  * Guarda en base de datos el promedio de "Mídete" del usuario
  *
  * @return void
  */
    public function guardar_promedio(){

      $anio_plan = $_POST['anio_plan'];
      
      foreach ($_POST['promedios'] as $key => $value){

        $id_user = $_POST['usuarios'][$key];
        $promedio = $value;
      
        // Guarda el promedio
        DB::insert("INSERT INTO calificaciones_midete SET anio = $anio_plan, id_user = $id_user, calificacion = $promedio ON DUPLICATE KEY UPDATE calificacion = $promedio");
      }
    }
	
	/**
  * Muestra los datos a importar de las regiones para el calculo del bono
  */
  public function mostrar_regiones_empleados(){

    if (!empty($_FILES['file']['name'])){

      $array = explode('.', $_FILES['file']['name']);
      $extension = end($array);
      
      if ($extension == 'xls' || $extension == 'xlsx'){

        $path = Storage::disk('local')
                        ->getDriver()
                        ->getAdapter()
                        ->getPathPrefix();
        $folder = 'Interface/';
        move_uploaded_file($_FILES['file']['tmp_name'], $path . $folder . 'users_regions.xlsx');
        require_once "PHPExcel.php";
        $inputFileName = $path . $folder . 'users_regions.xlsx';
        $excelReader = \PHPExcel_IOFactory::createReaderForFile($inputFileName);
        $objPHPExcel = $excelReader->load($inputFileName);

        // Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow();

        $datos = array();
        $montos_y_porcentajes = false;

        // Obtiene el valor de la columna F
        $celdaF = $sheet->getCell('F2')->getValue();

        // La celda F contine un valor... se trata del archivo para guardar los montos y porcentajes de bonos
        if (!empty($celdaF)){

          $montos_y_porcentajes = true;

          //  Loop through each row of the worksheet in turn
          for ($row = 2; $row <= $highestRow; $row++){

            $fila = new \stdClass();
            $montos_y_porcentajes = true;
            $fila->puesto = $sheet->getCell('A'.$row)->getValue();
            $fila->departamento = $sheet->getCell('B'.$row)->getValue();
            $fila->region = $sheet->getCell('C'.$row)->getValue();
            $fila->frecuencia = $sheet->getCell('D'.$row)->getValue();
            $fila->bono_anual = $sheet->getCell('E'.$row)->getValue() * 100 . '%';
            $fila->tipo_bono = $sheet->getCell('F'.$row)->getValue();
            $fila->porcentaje = $sheet->getCell('G'.$row)->getValue();

            // El tipo de bono es por porcentaje
            if ($fila->tipo_bono == 'P'){

              // Se le da el formato correcto
              $fila->porcentaje = $fila->porcentaje * 100 . '%';
            }

            $fila->monto = $sheet->getCell('H'.$row)->getValue();
            $datos[] = $fila;
          }
        }

        // Se trata del archivo de regiones para cada usuario
        else{

          //  Loop through each row of the worksheet in turn
          for ($row = 2; $row <= $highestRow; $row++){

            $fila = new \stdClass();
            $fila->rfc = $sheet->getCell('A'.$row)->getValue();
            $fila->nombre = $sheet->getCell('B'.$row)->getValue();
            $fila->sucursal = $sheet->getCell('C'.$row)->getValue();
            $fila->puesto = $sheet->getCell('D'.$row)->getValue();
            $fila->region = $sheet->getCell('E'.$row)->getValue();
            $datos[] = $fila;
          }
        }
      }
    }

    return view('evaluacion-resultados/reportes/importar-regiones', compact('datos', 'montos_y_porcentajes'));
  }

  /**
  * Importa las regiones para el calculo del bono
  */
  public function importar_regiones_empleados(){

    $path = Storage::disk('local')
                    ->getDriver()
                    ->getAdapter()
                    ->getPathPrefix();
          
    $folder = 'Interface/';
    require_once "PHPExcel.php";
    $inputFileName = $path . $folder . 'users_regions.xlsx';
    $excelReader = \PHPExcel_IOFactory::createReaderForFile($inputFileName);
    $objPHPExcel = $excelReader->load($inputFileName);

    // Get worksheet dimensions
    $sheet = $objPHPExcel->getSheet(0); 
    $highestRow = $sheet->getHighestRow();

    // Obtiene el valor de la columna F
    $celdaF = $sheet->getCell('F2')->getValue();

    $registro = array();

    // La celda F contine un valor... se trata del archivo para guardar los montos y porcentajes de bonos
    if (!empty($celdaF)){

      // Se borran todos los registros de los bonos
      DB::delete("DELETE FROM bonos");

      //  Loop through each row of the worksheet in turn
      for ($row = 2; $row <= $highestRow; $row++){

        $registro['id'] = $row - 1;
        $registro['puesto'] = $sheet->getCell('A'.$row)->getValue();
        $registro['departamento'] = $sheet->getCell('B'.$row)->getValue();
        $registro['region'] = $sheet->getCell('C'.$row)->getValue();
        $registro['frecuencia'] = $sheet->getCell('D'.$row)->getValue();
        $registro['bono_anual'] = $sheet->getCell('E'.$row)->getValue() * 100;
        $registro['tipo_bono'] = $sheet->getCell('F'.$row)->getValue();
        $registro['porcentaje'] = $sheet->getCell('G'.$row)->getValue();
        $registro['monto'] = $sheet->getCell('H'.$row)->getValue();

        // El tipo de bono es por porcentaje
        if ($registro['tipo_bono'] == 'P'){

          // Se le da el formato correcto
          $registro['porcentaje'] = $registro['porcentaje'] * 100;

          $registro['monto'] = 0;
        }

        // El tipo de bono es por monto
        else{

          $registro['porcentaje'] = 0;
        }

        if (empty($registro['region'])){

          $registro['region'] = 0;
        }

        DB::table('bonos')->insert($registro);
      }
    }

    // Se trata del archivo de regiones para cada usuario
    else{

      // Se borran todos los registros de las regiones de los usuarios
      DB::delete("DELETE FROM user_regions");

      // Loop through each row of the worksheet in turn
      for ($row = 2; $row <= $highestRow; $row++){

        $registro['id'] = $row - 1;
        $registro['rfc'] = $sheet->getCell('A'.$row)->getValue();
        $registro['region'] = $sheet->getCell('E'.$row)->getValue();

        DB::table('user_regions')->insert($registro);
      }
    }

    // Se borra el archivo fisico de la importación
    unlink($path . $folder . 'users_regions.xlsx');

    flash("El archivo fue importado de manera exitosa");
    return redirect('/planes');
  }

  /**
  * Exporta los bonos
  *
  * @return void
  */
  public function reporte_bonos($id_plan = 0){

    require_once 'PHPExcel.php';
    require_once 'PHPExcel/IOFactory.php';

    // Create new PHPExcel object
    $objPHPExcel = new \PHPExcel();

    // Create a first sheet, representing users data
    $objPHPExcel->setActiveSheetIndex(0);
    
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'CLAVE EMPLEADO');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'MONTO');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'CLAVE');

    $plan = DB::table('planes')->where('id', $id_plan)->select('id', 'nombre')->first();
    $employees = DB::table('employees')->pluck('idempleado', 'id')->toArray();

    // Obtiene los objetivos o propuestas del usuario junto con los logros por periodo de cada propuesta
    $objetivos = DB::select("SELECT objetivos.id, objetivos.id_empleado, objetivos.peso, objetivos.tipo, objetivos.objetivo, objetivos.valor_rojo, objetivos.valor_verde, tipo_bono, logros.logro, logros.periodo FROM objetivos INNER JOIN logros ON (logros.objetivo_id = objetivos.id) WHERE objetivos.id_plan = ? AND revisado = ? ORDER BY objetivos.id_empleado, objetivos.id, periodo DESC", [$plan->id, 1]);

    $nuevos_objetivos = array();
    $actual_objetivo = 0;
    $actual_usuario = 0;
    $bono = 0;
    $bono_alcanzado = 0;
    $row = 2;

    foreach ($objetivos as $key => $objetivo){

      if ($actual_usuario != $objetivo->id_empleado){

        if ($actual_usuario != 0){

          $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $employees[$actual_usuario]);
          $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, '$' . number_format($bono_alcanzado, 2));
          $row++;
        }

        $actual_objetivo = 0;
        $actual_usuario = $objetivo->id_empleado;
        $bono_alcanzado = 0;
        $bono = DB::table('bonos_planes')->where('id_user', $actual_usuario)->where('id_plan', $plan->id)->first();

        if (empty($bono)){

          continue;
        }
      }

      if ($actual_objetivo != $objetivo->id){

        $actual_objetivo = $objetivo->id;
        $bonos_objetivos = DB::table('bonos_objetivos')->where('id_objetivo', $objetivo->id)->get();
        $objetivo->bonos = $bonos_objetivos;
        $valor_objetivo = DB::table('valores_objetivo')->where('id_objetivo', $objetivo->id)->where('periodo', $objetivo->periodo)->first();

        if (!empty($valor_objetivo)){

          if ($objetivo->tipo_bono == 'Comision'){

            $objetivo->bono = $valor_objetivo->meta;
          }

          else{

            $objetivo->objetivo = $valor_objetivo->meta;
            $objetivo->valor_rojo = $valor_objetivo->valor_rojo;
            $objetivo->valor_verde = $valor_objetivo->valor_verde;
          }
        }

        if ($objetivo->tipo_bono != 'Comision'){

          $peso = 0;
          $porcentajes_bono = $objetivo->bonos;
          $total_bono = 0;
          $peso = $objetivo->peso;
          $porcentaje_bono = $peso * $bono->total_bono / 100;

          // El objetivo es de tipo Si/No 
          if ($objetivo->valor_verde == 'Si' || $objetivo->valor_verde == 'No'){

            // Se cumplió el objetivo
            if ($objetivo->logro == $objetivo->valor_verde){

              $nivel_logro = 100;

              if (count($porcentajes_bono) > 0){

                foreach ($porcentajes_bono as $key => $value){
                      
                  if ($nivel_logro >= $value->nivel_logro){

                    $porcentaje = $value->porcentaje_bono;

                    if (empty($porcentaje)){

                      $porcentaje = $nivel_logro;
                    }

                    $total_bono = $porcentaje * $porcentaje_bono / 100;
                    break;
                  }
                }
              }

              else{

                $total_bono = $porcentaje_bono;
              }
            }
          }

          // El objetivo es de tipo numérico
          else{

            if ($objetivo->valor_verde >= $objetivo->valor_rojo){

              $objetivo->valor_verde = str_replace(',', '', $objetivo->valor_verde) * 1;
              $objetivo->logro = str_replace(',', '', $objetivo->logro) * 1;

              // Se cumplió el objetivo
              if ($objetivo->logro >= $objetivo->valor_verde){

                $nivel_logro = 100;

                if (count($porcentajes_bono) > 0){

                  foreach ($porcentajes_bono as $key => $value){
                      
                    if ($nivel_logro >= $value->nivel_logro){

                      $porcentaje = $value->porcentaje_bono;

                      if (empty($porcentaje)){

                        $porcentaje = $nivel_logro;
                      }

                      $total_bono = $porcentaje * $porcentaje_bono / 100;
                      break;
                    }
                  }
                }

                else{

                  $total_bono = $porcentaje_bono;
                }
              }

              // Se cumplió a medias o no se cumplió
              else{

                // Se calcula la calificación respecto al peso 
                $valor = $objetivo->logro * 100 / $objetivo->valor_verde;
                $peso = $objetivo->peso * $valor / 100;

                if (count($porcentajes_bono) > 0){

                  foreach ($porcentajes_bono as $key => $value){
                      
                    if ($valor >= $value->nivel_logro){

                      $porcentaje = $value->porcentaje_bono;

                      if (empty($porcentaje)){

                        $porcentaje = $nivel_logro;
                      }

                      $total_bono = $porcentaje * $porcentaje_bono / 100;
                      break;
                    }
                  }
                }

                else{

                  $total_bono = $valor * $porcentaje_bono / 100;
                }
              }
            }

            else{

              $objetivo->valor_verde = str_replace(',', '', $objetivo->valor_verde) * 1;
              $objetivo->logro = str_replace(',', '', $objetivo->logro) * 1;

              // Se cumplió el objetivo
              if ($objetivo->logro <= $objetivo->valor_verde){

                $nivel_logro = 100;

                if (count($porcentajes_bono) > 0){

                  foreach ($porcentajes_bono as $key => $value){
                      
                    if ($nivel_logro >= $value->nivel_logro){

                      $porcentaje = $value->porcentaje_bono;

                      if (empty($porcentaje)){

                        $porcentaje = $nivel_logro;
                      }

                      $total_bono = $porcentaje * $porcentaje_bono / 100;
                      break;
                    }
                  }
                }

                else{

                  $total_bono = $porcentaje_bono;
                }
              }
            }
          }

          $bono_alcanzado += $total_bono;
        }

        else{

          $bono_alcanzado += $objetivo->bono;
        } 
      }
    }

    if ($actual_usuario != 0){

      $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $employees[$actual_usuario]);
      $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, '$' . number_format($bono_alcanzado, 2));
    }

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Bonos - ' . $plan->nombre . '.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
  }

  /**
  * Reporte resultados mensuales
  *
  * @return void
  */
  public function reporte_resultados_mensuales(){
      
      $users = array();
      $departments = array();
      $departments2 = array();
      $id_jefe = auth()->user()->employee_id;
      $jefe = Employee::find($id_jefe);
      $anio_plan = 0;
      $anios = array();
      $ids_planes = '';
      $ids_plans = array();
      $planes = DB::select("SELECT id, anio FROM planes WHERE estado = ? OR estado = ? ORDER BY anio DESC", [2, 3]);

      if (!empty($planes)){

        // Obtiene el año mas reciente
        $anio_plan = $planes[0]->anio;

        // Se requiere mostrar un plan por default
        if (!empty($_POST['anio_plan'])){

          $anio_plan = $_POST['anio_plan'];
        }

        foreach ($planes as $key => $value){
            
          if (!in_array($value->anio, $anios)){

            $anios[] = $value->anio;
          }

          if ($value->anio == $anio_plan){

            $ids_planes .= ',' . $value->id;
          }
        }

        $ids_planes = substr($ids_planes, 1);
        $ids_plans = explode(',', $ids_planes);

        if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){

          $departments = DB::select("SELECT COUNT(DISTINCT employees.id) AS total_usuarios, departments.name FROM employees INNER JOIN job_positions ON (job_positions.id = employees.job_position_id) INNER JOIN areas ON (areas.id = job_positions.area_id) INNER JOIN departments ON (departments.id = areas.department_id) INNER JOIN objetivos ON (employees.id = objetivos.id_empleado) INNER JOIN logros ON (objetivos.id = logros.objetivo_id AND objetivos.id_empleado = logros.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan IN ($ids_planes) AND logros.revisado = 1 GROUP BY departments.name ORDER BY departments.name");
      
          if (!empty($_POST['department'])){

            $department = $_POST['department'];

            $departments2 = DB::select("SELECT COUNT(DISTINCT employees.id) AS total_usuarios, departments.name FROM employees INNER JOIN job_positions ON (job_positions.id = employees.job_position_id) INNER JOIN areas ON (areas.id = job_positions.area_id) INNER JOIN departments ON (departments.id = areas.department_id) INNER JOIN objetivos ON (employees.id = objetivos.id_empleado) INNER JOIN logros ON (objetivos.id = logros.objetivo_id AND objetivos.id_empleado = logros.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan IN ($ids_planes) AND logros.revisado = 1 AND departments.name = '$department' GROUP BY departments.name ORDER BY departments.name");

            // Obtiene los datos de todos los usuarios de la respectiva area
            $users = DB::select("SELECT employees.nombre, paterno, materno, direccion, employees.id, departments.name AS department, job_positions.name AS job_position, frecuencia, peso, valor_rojo, valor_verde, logro, logros.periodo FROM employees INNER JOIN job_positions ON (job_positions.id = employees.job_position_id) INNER JOIN areas ON (areas.id = job_positions.area_id) INNER JOIN departments ON (departments.id = areas.department_id) INNER JOIN objetivos ON (employees.id = objetivos.id_empleado) INNER JOIN logros ON (objetivos.id = logros.objetivo_id AND objetivos.id_empleado = logros.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan IN ($ids_planes) AND revisado = 1 AND departments.name = '$department' ORDER BY department, employees.id, logros.periodo");
          }

          else{

            // Obtiene los datos de todos los usuarios
            $users = DB::select("SELECT employees.nombre, paterno, materno, direccion, employees.id, departments.name AS department, job_positions.name AS job_position, frecuencia, peso, valor_rojo, valor_verde, logro, logros.periodo FROM employees INNER JOIN job_positions ON (job_positions.id = employees.job_position_id) INNER JOIN areas ON (areas.id = job_positions.area_id) INNER JOIN departments ON (departments.id = areas.department_id) INNER JOIN objetivos ON (employees.id = objetivos.id_empleado) INNER JOIN logros ON (objetivos.id = logros.objetivo_id AND objetivos.id_empleado = logros.id_empleado AND objetivos.id_plan = logros.id_plan) WHERE objetivos.id_plan IN ($ids_planes) AND revisado = 1 ORDER BY departments.name, employees.id, logros.periodo");
          }
        }

        else{

          $users = DB::table('employees')->where('jefe', $jefe->idempleado)->pluck('idempleado')->toArray();

          if (count($users) > 0){

            $final_users = $users;
            
            $departments = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->join('objetivos', 'objetivos.id_empleado', '=', 'employees.id')->join('logros', 'logros.objetivo_id', '=', 'objetivos.id')->whereIn('objetivos.id_plan', $ids_plans)->whereIn('idempleado', $final_users)->where('employees.id', '!=', $jefe->id)->where('revisado', 1)->select('departments.name')->groupBy('departments.name')->orderBy('departments.name')->get();
        
            if (!empty($_POST['department'])){

              $department = $_POST['department'];
              $departments2 = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->join('objetivos', 'objetivos.id_empleado', '=', 'employees.id')->join('logros', 'logros.objetivo_id', '=', 'objetivos.id')->whereIn('objetivos.id_plan', $ids_plans)->whereIn('idempleado', $final_users)->where('employees.id', '!=', $jefe->id)->where('revisado', 1)->where('departments.name', $department)->select('departments.name')->groupBy('departments.name')->orderBy('departments.name')->get();

              // Obtiene los datos de todos los subordinados del usuario logueado en el area
              $users = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('objetivos', 'objetivos.id_empleado', '=', 'employees.id')->join('logros', 'logros.objetivo_id', '=', 'objetivos.id')->whereIn('objetivos.id_plan', $ids_plans)->whereIn('idempleado', $final_users)->where('employees.id', '!=', $jefe->id)->where('revisado', 1)->where('department', $department)->select('employees.nombre', 'paterno', 'materno', 'direccion', 'employees.id', 'departments.name AS department', 'job_positions.name AS job_position', 'frecuencia', 'peso', 'valor_rojo', 'valor_verde', 'logro', 'logros.periodo')->orderBy('departments.name')->orderBy('employees.id')->orderBy('logros.periodo')->get();
            }

            else{

              // Obtiene los datos de todos los subordinados del usuario logueado
              $users = DB::table('employees')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('objetivos', 'objetivos.id_empleado', '=', 'employees.id')->join('logros', 'logros.objetivo_id', '=', 'objetivos.id')->whereIn('objetivos.id_plan', $ids_plans)->whereIn('idempleado', $final_users)->where('employees.id', '!=', $jefe->id)->where('revisado', 1)->select('employees.nombre', 'paterno', 'materno', 'direccion', 'employees.id', 'departments.name AS department', 'job_positions.name AS job_position', 'frecuencia', 'peso', 'valor_rojo', 'valor_verde', 'logro', 'logros.periodo')->orderBy('departments.name')->orderBy('employees.id')->orderBy('logros.periodo')->get();
            }
          }
        }
      }

      return view('evaluacion-resultados/reportes/reporte-resultados-mensuales', compact('users', 'anios', 'anio_plan', 'departments', 'departments2'));
  }
}


