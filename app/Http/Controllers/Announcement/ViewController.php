<?php

namespace App\Http\Controllers\Announcement;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Models\Announcement\Announcement;
use App\Models\Announcement\AnnouncementType;
use App\Models\Announcement\AnnouncementFormData;
use App\Models\Announcement\AnnouncementViewSequence;
use App\Models\Announcement\View;

use App\Models\Access\Access;

use Carbon\Carbon;

class ViewController extends Controller
{
    public $url;

    public function __construct()
    {
        $this->url = isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:'';
        $this->url = substr($this->url, 1);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $views = View::all();
        return view('announcement.views.index', compact('views'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function views()
    {
        return view('announcement.view.announcements_with_layout', compact('announcement_types','view'));
    }

    public function viewSequence($id)
    {
        $announcements = Announcement::where('view_id',$id)
        ->where('active',1)
        ->where('starts','<=',Carbon::now())
        ->where('ends','>=',Carbon::now())
        ->groupBy('announcement_type_id')
        ->get();
        if(!empty($announcements)){
            $view = $id;
            $announcement_types = AnnouncementType::whereIn('id',$announcements->pluck('announcement_type_id')->toArray())->get();            
            $orders = AnnouncementViewSequence::where('view_id',$id)
            ->orderBy('sequence','ASC')
            ->get();
            $val = true;
            foreach ($orders as $key => $value) {
                foreach ($announcements as $key => $value2) {
                    if($value->announcement_type_id == $value2->announcement_type_id){
                        $val = false; 
                    }
                }
                if($val){
                    $value->sequence = 0;
                    // $value->delete();
                    $val = true;
                }
            }
            $orders = AnnouncementViewSequence::where('view_id',$id)
            ->where('sequence','!=',0)
            ->orderBy('sequence','ASC')
            ->get();
            $cont = 1;
            foreach ($orders as $key => $value) {
                if($value->sequence != $cont){
                    $value->sequence = $cont;
                    $value->save();
                }
                $cont++;
            }
            // dd($announcements,$announcement_types);
            $val = true;
            $pos=1;
            foreach ($announcements as $key => $announcement) {
                foreach ($announcement_types as $key2 => $type) {
                    if($announcement->announcement_type_id == $type->id){
                        $types[$type->id] = $type->toArray();
                        foreach ($orders as $key3 => $order) {                            
                            if($type->id == $order->announcement_type_id){
                                // $orderView[$type->id] = $order->sequence;
                                // $types[$type->id]['pos'] = $type->id;
                                $types[$type->id]['pos'] = $order->sequence;
                                $val = false;
                                // dd($type->id,$order->sequence,$orderView,$types,$cont);
                            }
                        }
                        if($val){
                            // $orderView[$type->id] = $cont;
                            // $types[$type->id]['pos'] = $pos;
                            $types[$type->id]['pos'] = $cont;
                            $cont++;
                        }
                        $pos++;
                        $val = true;
                    }
                }
            }
            // dd($types);
            return view('announcement.view.sequence', compact('types','view'));
        }
        return redirect()->to('announcements?view=' .$id);


        if(!empty($announcements)){
            $orders = AnnouncementViewSequence::where('view_id','id')->get();
            if(count($orders) == 0){
                $cont = 0;
                foreach ($announcements as $key => $value) {
                    $order[$value->id] = $cont+=1;                    
                }
            }else{
                foreach ($announcements as $key => $value) {                           
                    foreach ($orders as $key2 => $value2) {
                        if($value->id = $value2->announcement_id){
                            $order[$value->id] = $value2->sequence;
                        }
                    }
                    $order[$value->id] = 0;
                }
            }
        }
        return view('announcement.view.sequence', compact('announcements','order'));
    }

    function saveOrder(Request $request){
        $types = $request->types;
        $order = $request->order;
        $view = $request->view;
        for ($i=0; $i <count($types) ; $i++) {
            AnnouncementViewSequence::updateOrCreate(
                ['view_id' => $view, 'announcement_type_id' => $types[$i]],
                ['sequence' => $order[$i]]
            );
        }
        return redirect()->to('ordenar/'.$request->view);
    }
}
