<?php

namespace App\Http\Controllers\Announcement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Announcement\AnnouncementType;

class AnnouncementTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = AnnouncementType::all();
        //dd($types);
        return view('announcement.announcement_types.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('announcement.announcement_types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $announcement_type = AnnouncementType::findOrFail($id);
        return view('announcement.announcement_types.edit', compact('announcement_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        $announcement_type = AnnouncementType::findOrFail($id);
        $announcement_type->show_name = $request->show_name;
        $announcement_type->max_quantity_stored = $request->max_quantity_stored;
        $announcement_type->save();
        */
        
        $form_data = array(
            'name'  =>  $request->name,
            'show_name'  =>  $request->show_name,
            'view_file'  =>  $request->view_file,
            'max_quantity_stored'  =>  $request->max_quantity_stored,
        );
        
        AnnouncementType::where('id',$id)->update($form_data);
        flash('Guardado Correctamente')->success();
        return redirect()->to('announcement_types');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $announcement_type = AnnouncementType::findOrFail($id);
        $announcement_type->delete();
        flash('Borrado exitosamente')->success();
        return redirect()->to('announcement_types');   
    }
}
