<?php

namespace App\Http\Controllers\Announcement;

use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Announcement\AnnouncementForm;
use App\Models\Announcement\AnnouncementType;
use App\Models\Announcement\AnnouncementAttribute;
use App\Models\Announcement\AnnouncementFormData;


class AnnouncementFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $canCreateForm = empty(AnnouncementForm::missingForms())?false:true;
        $formShapes = AnnouncementForm::orderBy('announcement_type_id','ASC')
        ->orderBy('view_order','ASC')
        ->get();
        $forms = [];
        foreach ($formShapes as $key => $formShape) {            
            $forms[$formShape->announcementType->name][$formShape->announcementAttribute->name] = $formShape;
        }

        return view('announcement.announcement_forms.index', compact('forms', 'canCreateForm'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = AnnouncementForm::missingForms()->pluck('name','id')->toArray();
        $attributes = AnnouncementAttribute::all();
        return view('announcement.announcement_forms.create', compact('types','attributes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attr_list = isset($request->attr_list)?$request->attr_list:[];
        $attr_list_req = isset($request->attr_list_req)?$request->attr_list_req:[];
        foreach ($attr_list as $key => $attr) {
            $required = (in_array($attr, $attr_list_req))?1:0;
            $form = [
                'announcement_type_id' => $request->type,
                'announcement_attribute_id' => $attr,
                'required' => $required];
                AnnouncementForm::create($form);
        }
        //dd($form);
        
        return redirect()->route('announcement_forms.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $form = AnnouncementForm::where('announcement_type_id',$id)->orderBy('view_order','ASC')->get();

        return view('announcement.announcement_forms.show', compact('form'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $announcement_type = AnnouncementType::find($id);
        $form = AnnouncementForm::where('announcement_type_id',$announcement_type->id)->get();
        $attributes = AnnouncementAttribute::all();
        //dd($attributes);
        foreach ($attributes as $key => $attribute) {
            $attribute->active = false;
            $attribute->required = false;
            foreach ($form as $key2 => $formAttr) {
                if($attribute->id == $formAttr->announcement_attribute_id){
                    $attribute->active = true;
                    $attribute->required = ($formAttr->required)?true:false; 
                }
            }
        }        
        return view('announcement.announcement_forms.edit', compact('attributes','announcement_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        // dd($request->attr_list, $request->attr_list_req, $id);

        if($request->has('attr_list')) {
            $attr_list = $request->attr_list;
            $attr_list_req = ($request->has('attr_list_req')?$request->attr_list_req:null);
            $type = AnnouncementType::with('attributes')->where('id', $id)->first();

            $array_to_update = array();
            $array_to_delete = array();
            foreach($type->attributes as $key => $attribute) {
                if(in_array($attribute->id, $attr_list)) {
                    // array_push($array_to_update, $attribute->id);
                    // Los que se actualizarán
                    if($attr_list_req != null)
                        $attribute->pivot->required = (in_array($attribute->id, $request->attr_list_req)?1:0);
                    else
                        $attribute->pivot->required = 0;
                    $attribute->pivot->save();
                } else {
                    // array_push($array_to_delete, $attribute->id);
                    // Los que se eliminarán
                    if($attribute->attr_view == 'file' || $attribute->attr_view == 'multi-file') {
                        $file_datas = AnnouncementFormData::where('announcement_form_id', $attribute->pivot->id)->get();
                        $destinationPath = 'img/announcements/';
                        foreach($file_datas as $key => $data) {
                            if($attribute->attr_view == 'file') {
                                File::delete($destinationPath.$data->value);
                            } elseif($attribute->attr_view == 'multi-file') {
                                $file_array = json_decode($data->value);
                                foreach($file_array as $file) {
                                    File::delete($destinationPath.$file);
                                }
                            }
                        }
                    }
                    AnnouncementFormData::where('announcement_form_id', $attribute->pivot->id)->delete();
                    $attribute->pivot->delete();
                }
                $index = array_search($attribute->id, $attr_list);
                unset($attr_list[$index]);
            }
            foreach($attr_list as $key => $attribute) {
                // Los que se agregarán
                if($attr_list_req != null)
                    $required = (in_array($attribute, $request->attr_list_req)?1:0);
                else
                    $required = 0;
                AnnouncementForm::insert(array(
                    'announcement_type_id' => $id,
                    'announcement_attribute_id' => $attribute,
                    'required' => $required,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ));
            }
            // dd($array_to_update, $array_to_delete, $attr_list);
        }
        
        return redirect()->route('announcement_forms.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $form = AnnouncementForm::where('announcement_type_id', $id)->get()
        ->pluck('id')->toArray();
        foreach ($form as $key => $id) {
            AnnouncementForm::find($id)->delete();
        }
        return redirect()->route('announcement_forms.index');
    }
}
