<?php

namespace App\Http\Controllers\Announcement;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Announcement\View;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Models\Announcement\Announcement;
use App\Models\Announcement\AnnouncementForm;
use App\Models\Announcement\AnnouncementType;
use App\Models\Announcement\AnnouncementCategory;
use App\Models\Announcement\AnnouncementFormData;

class AnnouncementCategoryController extends Controller
{
    protected $path;

    public function __construct()
    {
        $this->middleware('permission:announcement_categories')->only(['create', 'store', 'show', 'edit', 'update', 'destroy']);

        $this->path = getcwd() . '/img/announcements/categories/';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = Input::get('type');
        $categories = AnnouncementCategory::when($type, function($q) {
            $q->whereHas('announcement_type', function ($q2) {
                $q2->where('name', 'convenios');
            });
        })->get();
        return view('announcement.announcement_categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = AnnouncementType::where('name', 'convenios')->pluck('name', 'id');
        return view('announcement.announcement_categories.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = $request->except('_token');
        $file = $request['image'];
        $image = time() . '-'. $file->getClientOriginalName();
        $request['image'] = $image;
        \DB::beginTransaction();
        try {
            AnnouncementCategory::create($request);
        } catch (\Throwable $th) {
            \DB::rollback();
            flash("No se logro guardar la categoría");
            return redirect()->route('announcement_categories.create')->withInput();
        }
        
        if(!$file->move($this->path, $image)){
            \DB::rollback();
            flash("No se logro guardar la imagen");
            return redirect()->route('announcement_categories.create')->withInput();
        }

        \DB::commit();
        flash("Categoria creada correctamente!!");
        return redirect()->route('announcement_categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $category = AnnouncementCategory::findOrFail($id);
            $types = AnnouncementType::where('name', 'convenios')->pluck('name', 'id');
        } catch (\Throwable $th) {
            return back();
        }

        return view('announcement.announcement_categories.edit', compact('category', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $announcement = AnnouncementCategory::findOrFail($id);
        $file_old = $this->path.$announcement->image;
        $request = $request->except(['_method', '_token']);
        
        if( isset($request['image']) ){            
            $file = $request['image'];
            $image = time() . '-'. $file->getClientOriginalName();
            $file->move($this->path, $image);
            $request['image'] = $image;
        }

        \DB::beginTransaction();
        try{
            $announcement->update($request);
            \DB::commit();
        } catch (\Throwable $th) {
            if(isset($request['image'])){
                $img = $this->path . $image;
                if(\File::exists($img)) {
                    \File::delete($img);
                }
            }
            \DB::rollback();
            flash("No se logro guardar la categoría");
            return redirect()->route('announcement_categories.edit', $id)->withInput();
        }

        flash('Categoría actualizada correctamente!!');
        return redirect()->route('announcement_categories.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = AnnouncementCategory::with('announcements')->findOrFail($id);
        $empty = $category->announcements->isEmpty();
        if(!$empty){
            flash('No se puede borrar ya que tiene anuncios!!');
            return redirect()->route('announcement_categories.edit', $id);
        }else{
            try {
                $category->delete();
            } catch (\Throwable $th) {
                flash('No se puedo borrar  | ' . $th->getMessage());
                return redirect()->route('announcement_categories.edit', $id);
            }
        }
        flash('Categoría borrada correctamente');
        return redirect()->route('announcement_categories.index');
    }
}
