<?php

namespace App\Http\Controllers\Announcement;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

use Laracasts\Flash\Flash;
use Carbon\Carbon;
use DB;
use App\Http\Controllers\Controller;

use App\Models\Region;
use App\Models\Announcement\AnnouncementCategory;
use App\Models\Announcement\AnnouncementType;
use App\Models\Announcement\AnnouncementTypePerView;
use App\Models\Announcement\AnnouncementUserInteraction;
use App\Models\Announcement\AnnouncementForm;
use App\Models\Announcement\AnnouncementFormData;
use App\Models\Announcement\AnnouncementAttribute;
use App\Models\Announcement\Announcement;
use App\Models\Announcement\View;

use App\Http\Requests\Announcement\MarkAsWatchedRequest;
use App\Http\Requests\Announcement\AnnouncementUserActionRequest;

use App\Models\Access\Access;

class AnnouncementController extends Controller
{
    protected $path;

    public function __construct()
    {
        $this->middleware('auth');

        // ==== Admin Anuncios ====
        $this->middleware('permission:see_announcements')->only(['index']);
        $this->middleware('permission:create_announcements')->only(['create', 'store']);
        $this->middleware('permission:edit_announcements')->only(['edit', 'update']);
        $this->middleware('permission:delete_announcements')->only(['destroy']);
        $this->middleware('permission:activate_announcements')->only(['activate']);
        // ==== /Admin Anuncios ====

        //$this->path = public_path() . '/img/announcements/';
        $this->path = getcwd() . '/img/announcements/';
        $this->use_regions = [];
        $this->use_categories = ['convenios'];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $type = Input::get('type');
        $view = Input::get('view');
        if($view = View::find($view)){
            $announcements = Announcement::where('view_id',$view->id);
            if($announcement_types = AnnouncementType::find($type)){
                if(!AnnouncementTypePerView::check($view->id, $type)){
                    $announcement_types =  $view->announcementsInThisView;
                    return view('announcement.announcement_types.types', compact('announcements','view','announcement_types','type'));
                }
                $this->deactivateAnnouncementsIfMust($view->id, $type);
                $announcements = $announcements->where('announcement_type_id',$announcement_types->id)->get();
                $announcement_types = AnnouncementType::where('id',$type)->first();
                $announcement_types->headers = $announcement_types->getFormHeaders();
                foreach ($announcements as $key => $announcement) {
                    $announcement->data = $announcement->formData();
                }
                return view('announcement.announcements.index', compact('announcements','view','announcement_types','type'));
            }else{
                $announcement_types =  $view->announcementsInThisView;
                return view('announcement.announcement_types.types', compact('announcements','view','announcement_types','type'));
            }
        }
        $views = View::all();
        return view('announcement.announcements.views', compact('views'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = Input::get('type');
        $view = Input::get('view');
        if($view = View::find($view)){
            $view = $view->id;
            if($announcement_type = AnnouncementType::find($type)){
                $announcement_types[$announcement_type->id] = $announcement_type->name;
            }else{
                // $announcement_type = AnnouncementType::all();
                // foreach ($announcement_type as $key => $value) {
                //     $announcement_types[$value->id] = $value->name;
                // }
                return redirect()->to('announcements?view='.$view);
            }

            $parents = [];
            if(!is_null($announcement_type->typeParent)){
                $parents = $announcement_type->typeParent->announcements;
            }
            $annParent = [];
            foreach ($parents as $key => $parent) {
                $parent->data = $parent->formData();
                if(isset($parent->data['title'])){
                    $annParent[$parent->id] = $parent->data['title'];
                }
            }

            if(in_array($announcement_type->name, $this->use_categories)) {
                $categories = AnnouncementCategory::whereHas('announcement_type', function($q) {
                    $q->where('name', 'convenios');
                })->pluck('name', 'id')->toArray();
            } else {
                $categories = false;
            }
            // Debido a que ningún anuncio por el momento usa regiones lo setteamos en false
            // $regions = Region::pluck('name', 'id')->toArray();
            $regions = false;

            $form = AnnouncementForm::getAnnouncementForm($announcement_type->id);
            return view('announcement.announcements.create', compact('view','announcement_types','regions','categories','form','announcement_type', 'annParent'));
        }else{
            flash('La pagina no existe')->warning();
        }
        $views = View::all();
        return view('announcement.announcements.views', compact('views'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $view = View::find($request->view);
        if(!is_null($view)){
            $request = $request->all();
            $form_data = [];

            $now = Carbon::now();
            
            //Sets the Start Date
            $startsHour = empty($request['starts_hour']) ? '00' : $request['starts_hour'];
            $startsMinute = empty($request['starts_minute']) ? '00' : $request['starts_minute'];
            $starts = empty($request['starts']) ? $now : Carbon::parse($request['starts'] . ' '. $startsHour . ':' . $startsMinute);

            //Sets the End Date
            $endsHour = empty($request['ends_hour']) ? '00' : $request['ends_hour'];
            $endsMinute = empty($request['ends_minute']) ? '00' : $request['ends_minute'];
            $ends = empty($request['ends']) ? $now->copy()->addDays(7) : Carbon::parse($request['ends'] . ' '. $endsHour . ':' . $endsMinute);

            $announcement_parent = null;
            if(isset($request['parent'])){
                $announcement_parent = Announcement::find($request['parent'])->id;
            }

            $announcement = Announcement::create([
                'starts' => $starts,
                'ends' => $ends,
                'active' => false,
                'approved_by_user_id' => null,
                'created_by_user_id'  => Auth::user()->id,
                'announcement_type_id' => $request['type'],
                'announcement_category_id' => (isset($request['category']))?$request['category']:null,
                //'announcement_parent_id' => $announcement_parent,
                'region_id' => (isset($region))?$region:null,
                'view_id' => $view->id
            ]);
            //dd($announcement);

            $attrs = AnnouncementForm::getAttr($request['type']);
            //dd($attrs);
            foreach ($attrs as $key => $attr) {
                $campo = '';
                if(isset($request[$attr->form_name])){
                    if($attr->attr_view == 'file'){
                        if (! is_null($request[$attr->form_name])) {
                            $campo = time() . '-'. $request[$attr->form_name]->getClientOriginalName();
                            $request[$attr->form_name]->move($this->path, $campo);
                        }
                    }else if($attr->attr_view == 'multi-file') {
                        if (! is_null($request[$attr->form_name])) {
                            $rutas = [];
                            foreach($request[$attr->form_name] as $file){
                                $campo = time() . '-'. $file->getClientOriginalName();
                                $rutas[] = $campo;
                                $file->move($this->path, $campo);
                            }
                            //dd($rutas);
                            $campo = json_encode($rutas, JSON_UNESCAPED_UNICODE);
                            //dd($campo);
                        }
                    }else{
                        $campo = $request[$attr->form_name];
                    }
                    $formID = AnnouncementForm::where('announcement_type_id',$request['type'])->where('announcement_attribute_id', $attr->id)->first();
                    $form_data = [
                        'value' => $campo,
                        'announcement_form_id' => $formID->id,
                        'announcement_id' => $announcement->id
                    ];
                    //dd($form_data,$request,$campo);
                    AnnouncementFormData::create($form_data);
                }
            }
            // Flash::success('El registro fue agregado');
            return redirect()->to('announcements?view='.$view->id.'&type=' . $announcement->announcement_type_id);
        }else{
            // Flash::success('La pagina no existe');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $announcement = Announcement::findOrFail($id);
        $view = $announcement->view_id;
        $announcement_type = $announcement->announcementType;
        $announcement_types[$announcement->announcement_type_id] = $announcement->announcementType->name;

        $parents = [];
        if(!is_null($announcement_type->typeParent)){
            $parents = $announcement_type->typeParent->announcements;
        }
        $annParent = [];
        foreach ($parents as $key => $parent) {
            $parent->data = $parent->formData();
            if(isset($parent->data['title'])){
                $annParent[$parent->id] = $parent->data['title'];
            }
        }

        if(in_array($announcement_type->name, $this->use_categories)) {
            $categories = AnnouncementCategory::whereHas('announcement_type', function($q) {
                $q->where('name', 'convenios');
            })->pluck('name', 'id')->toArray();
        } else {
            $categories = false;
        }

        // Debido a que ningún anuncio por el momento usa regiones lo setteamos en false
        // $regions = Region::pluck('name', 'id')->toArray();
        $regions = false;

        $dates = [
            'starts' => [
                'date' => date('Y-m-d',strtotime($announcement->starts)),
                'hour' => date('H', strtotime($announcement->starts)),
                'minute' => date('i', strtotime($announcement->starts)),
            ],
            'ends'=> [
                'date' => date('Y-m-d', strtotime($announcement->ends)),
                'hour' => date('H', strtotime($announcement->ends)),
                'minute' => date('i', strtotime($announcement->ends)),
            ]
        ];

        $form = AnnouncementForm::getAnnouncementForm($announcement_type->id);
        foreach ($form as $key => $attr) {
            $attr->data = optional($attr->getData($announcement->id))->value;
        }
        return view('announcement.announcements.edit', compact('view','announcement_types','regions','categories','form','announcement_type', 'announcement', 'dates', 'annParent'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $announcement = Announcement::findOrFail($id);
        $category = (isset($request->category))?AnnouncementCategory::find($request->category)->id:null;
        $region = (isset($request->region))?Region::find($request->region)->id:null;

        //Sets the Start Date
        $startsHour = empty($request['starts_hour']) ? '00' : $request['starts_hour'];
        $startsMinute = empty($request['starts_minute']) ? '00' : $request['starts_minute'];
        $starts = empty($request['starts']) ? $now : Carbon::parse($request['starts'] . ' '. $startsHour . ':' . $startsMinute);

        //Sets the End Date
        $endsHour = empty($request->ends_hour) ? '00' : $request->ends_hour;
        $endsMinute = empty($request->ends_minute) ? '00' : $request->ends_minute;
        $ends = empty($request->ends) ? $now->copy()->addDays(7) : Carbon::parse($request->ends . ' '. $endsHour . ':' . $endsMinute);

        $announcement_parent = null;
        if(isset($request['parent'])){
            $announcement_parent = Announcement::find($request['parent'])->id;
        }

        Announcement::where('id', $id)->update([                
            'starts' => $starts,
            'announcement_category_id' => $category,
            //'announcement_parent_id' => $announcement_parent,
            'region_id' => $region,
            'ends' => $ends
        ]);
        $request = $request->all();
        // Obtenemos todos los datos del formulario a partir de tipo
        $attrs = AnnouncementForm::getAttr($announcement->announcement_type_id);
        $form_data = [];
        foreach ($attrs as $key => $attr) {
            $form_id = AnnouncementForm::findAndGetID($announcement->announcement_type_id, $attr->id);
            $campo = '';
            $form_data = AnnouncementFormData::findItem($form_id, $announcement->id);
                    
            if(isset($request[$attr->form_name]) && !is_null($form_data)){
                $destinationPath = 'img/announcements/';
                $campo = $request[$attr->form_name];
                if($attr->attr_view == 'file'){
                    if (file_exists($this->path . $form_data->value)){
                        unlink($this->path . $form_data->value);
                    }
                    $campo = time() . '-'. $request[$attr->form_name]->getClientOriginalName();
                    $request[$attr->form_name]->move($this->path, $campo);

                    File::delete($destinationPath.$form_data->value);
                } elseif($attr->attr_view == 'multi-file') {
                    if (! is_null($request[$attr->form_name])) {
                        $rutas = [];
                        foreach($request[$attr->form_name] as $file){
                            $campo = time() . '-'. $file->getClientOriginalName();
                            $rutas[] = $campo;
                            $file->move($this->path, $campo);
                        }
                        $campo = json_encode($rutas, JSON_UNESCAPED_UNICODE);

                        $file_array = json_decode($form_data->value); // Datos anteriores
                        foreach($file_array as $file) {
                            File::delete($destinationPath.$file);
                        }
                    }
                }
                $form_data->value = $campo;
                $form_data->save();
            }else{
                if(!isset($request[$attr->form_name]) || is_null($request[$attr->form_name])){
                    continue;
                }
                if($attr->attr_view == 'file'){
                    if (isset($request[$attr->form_name]) && !is_null($request[$attr->form_name])) {
                        $campo = time() . '-'. $request[$attr->form_name]->getClientOriginalName();
                        $request[$attr->form_name]->move($this->path, $campo);
                    }else{
                        continue;
                    }
                } elseif($attr->attr_view == 'multi-file') {
                    if (! is_null($request[$attr->form_name])) {
                        $rutas = [];
                        foreach($request[$attr->form_name] as $file){
                            $campo = time() . '-'. $file->getClientOriginalName();
                            $rutas[] = $campo;
                            $file->move($this->path, $campo);
                        }
                        //dd($rutas);
                        $campo = json_encode($rutas, JSON_UNESCAPED_UNICODE);
                        //dd($campo);
                    }
                } else{
                    $campo = $request[$attr->form_name];
                }
                $form_data = [
                    'value' => $campo,
                    'announcement_form_id' => $form_id,
                    'announcement_id' => $announcement->id
                ];
                AnnouncementFormData::create($form_data);
            }
        }
        // Flash::info('El registro fue editado');
        return redirect()->to('announcements?view=' .$request['view'].'&type=' . $request['type']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $announcement = Announcement::find($id);
        $view = $announcement->view_id;
        $type = $announcement->announcement_type_id;
        $form_data = $announcement->announcementFormData;
        foreach ($form_data as $key => $data) {
            $form_elem = $data->announcementForm;
            $attr = $form_elem->announcementAttribute;
            if($attr->attr_view == 'file'){
                if (file_exists($this->path . $data->value)){
                    unlink($this->path . $data->value);
                }
            }
        }
        AnnouncementFormData::where('announcement_id', $id)->delete();
        $announcement->delete();

        // Flash::success('El anuncio se ha borrado exitosamente');
        return redirect()->to('announcements?view=' .$view. '&type=' .$type);
    }

    /**
     * Update the active status for the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request, $id)
    {
        
        $announcement = Announcement::findOrFail($id);
        $status = ! $announcement->active;
        Announcement::where('id', $id)->update([
            'active' => $status,
            'approved_by_user_id' => Auth::user()->id
        ]);
        // Flash::success('El registro fue editado');
        return redirect()->to('announcements?view=' .$announcement->view_id. '&type=' .$request->type);
    }

    public function deleteFiles($id, $type = null){
        $announcement = Announcement::find($id);
        $files = explode(',', trim($announcement->file,'[]'));
        if((is_null($type) || 'file' === $type) && (!is_null($announcement->file) || !empty($announcement->file))){
            foreach ($files as $key => $file) {
                $file = trim($file,'""');
                if (file_exists($this->path . $file)){
                    unlink($this->path . $file);
                }
            }
        }
        if(is_null($type) || 'document' === $type){
            if (!is_null($announcement->document) && !empty($announcement->document) && file_exists($this->path2 . $announcement->document)){
                unlink($this->path . $announcement->document);
            }
        }
        if(is_null($type) || 'image' === $type){
            if (!is_null($announcement->image) && !empty($announcement->image) && file_exists($this->path . $announcement->image)){
                unlink($this->path . $announcement->image);
            }
        }
    }

    public function deactivateAnnouncementsIfMust($view = null, $type = null) {
        $currDate = date('Y-m-d H:m:s');
        Announcement::when(!is_null($view), function ($q) use ($view){
            $q->where('view_id', $view);
        })
        ->when(!is_null($type), function ($q) use ($type) {
            $q->where('announcement_type_id', $type);
        })
        ->where('ends', '<=', $currDate)
        ->update(['active' => 0]);
    }

    /**
     *  Funciones para convenios
     */
     private function getAnnouncementTypes($a_type = null)
    {
        $selectAnnouncementTypes = array();
        $types = null;

        if (auth()->user()->role == 'admin') {
            if($a_type){
                $types = AnnouncementType::where('id', $a_type)->get();
            }else{
                $types = AnnouncementType::all();
            }
            
        } else {
            $types = AnnouncementType::where('manager', auth()->user()->id)
            ->orWhere('collaboration', true);
            if($a_type){
                $types->where('id', $a_type);
            }
            $types = $types->get();
        }
        
        if (!is_null($types) && ! $types->isEmpty()) {
            $allowToCreate = true;

            foreach ($types as $type) {
                $selectAnnouncementTypes[$type->id] = $type->name;
            }
        }

        return $selectAnnouncementTypes;
    }

    public function setAction(Request $request){
        $announcement = Announcement::find($request->announcement_id);

        $exists = $announcement->announcement_user_actions()
        ->wherePivot('user_id', auth()->user()->id)
        ->wherePivot('action', '=', $request->action)
        ->exists();
        try {
            if($exists){
                $announcement->announcement_user_actions()
                ->wherePivot('action', '=', $request->action)
                ->detach(auth()->user()->id, ['action' => $request->action]);
            }else{
                $announcement->announcement_user_actions()
                ->wherePivot('action', '=', $request->action)
                ->attach(auth()->user()->id, ['action' => $request->action]);
            }

        } catch (\Throwable $th) {
            return response()->json(['success' => false, 'error' => $th->getMessage()], 200);
        }
        return response()->json(['success' => true], 200);
    }

    public function markAsWatched(MarkAsWatchedRequest $request){
        $announcement = Announcement::find($request->announcement_id);
        try {
            // $announcement->announcement_user_actions()
            // ->wherePivot('action', '=', $request->action)
            // ->sync([auth()->user()->id => ['action' => $request->action]]);
            AnnouncementUserInteraction::firstOrCreate([
                'action' => $request->action,
                'user_id' => auth()->user()->id,
                'announcement_id' => $announcement->id
            ]);
        } catch (\Throwable $th) {
            return response()->json(['success' => false, 'error' => $th->getMessage()], 200);
        }
        return response()->json(['success' => true], 200);
    }

    public function announcementInArea(Request $request){
        if(isset($request->area)){
            $id = $request->area;
            $ann = Announcement::whereHas('announcementStates', function($q) use($id){
                $q->where('id', $id);
            })->get()->pluck('id')->toArray();
            return response()->json(['success' => true, 'areas' => $ann], 200);
        }
    }

    public function actionsReport(){
        try {
            $announcement_type = AnnouncementType::where('name', 'convenios')->firstOrFail();
        } catch(\Throwable $th) {
            return back();
        }
        $convenios_type = $announcement_type->id;
        $count['convenios'] = Announcement::where('announcement_type_id', $convenios_type)->where('active', true)->count();
        $count['views'] = AnnouncementUserInteraction::actionPerActiveAnnouncement('watched', $convenios_type)->count();
        $count['likes'] = AnnouncementUserInteraction::actionPerActiveAnnouncement('like', $convenios_type)->count();
        $count['favorites'] = AnnouncementUserInteraction::actionPerActiveAnnouncement('favorite', $convenios_type)->count();

        /** Datos para las graficas */
        $_categories = AnnouncementCategory::with('announcements.favorites')->where('announcement_type_id', $convenios_type)->get();
        $category_names = json_encode($_categories->pluck('name')->toArray());
        $categories = [];
        $categories_favorite = [];
        foreach($_categories as $category){
            $categories[] = [
                $category->name,
                $category->announcements->count(),
            ];
            $cont_favorites = 0;
            foreach($category->announcements as $announcement){
                $cont_favorites += $announcement->favorites->count();
            }
            $categories_favorite[] = [
                'name' => $category->name,
                'data' => [$cont_favorites],
            ];
        }
        $categories = json_encode($categories);
        $categories_favorite = json_encode($categories_favorite);

        /** Formateamos y ordenamos los datos para las tablas */
        $convenios = Announcement::with('category', 'likes', 'favorites', 'watched')
        ->where('announcement_type_id', $convenios_type)
        ->where('active', true)
        ->get();
        $convenios_likes = $convenios->sortByDesc( function($convenio){
            return $convenio->likes->count();
        });
        $convenios_favorites = $convenios->sortByDesc( function($convenio){
            return $convenio->favorites->count();
        });
        $convenios_watched = $convenios->sortByDesc( function($convenio){
            return $convenio->watched->count();
        });
        return view('announcement.report', compact('count', 'convenios_likes', 'convenios_favorites', 'convenios_watched', 'categories', 'category_names', 'categories_favorite'));
    }

    public function home()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);

        $view = View::where('name',$url)->first();
        $banner = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
        $carrousel = Announcement::getAnnouncementsToDisplay($view->id, 'carrousel');
        $carrousel_secondary = Announcement::getAnnouncementsToDisplay($view->id, 'carrousel_secondary');
        $slick = Announcement::getAnnouncementsToDisplay($view->id, 'slick');
        $display_announcements = compact('banner', 'carrousel', 'carrousel_secondary', 'slick');
        return view('home', compact('display_announcements'));
    }

    public function quienes()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);

        $view = View::where('name',$url)->first();
        $panels = Announcement::getAnnouncementsToDisplay($view->id, 'panels');
        // $display_announcements = compact('banner','cards','banner_panel','panels','carrousel','carrousel_secondary','mosaico','tabla_anuncios','cuatro_columnas','por_categorias');
        $display_announcements = compact('panels');
        return view('quienes-somos', compact('display_announcements'));
    }

    public function valores()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);

        $view = View::where('name',$url)->first();
        $columna_anuncios = Announcement::getAnnouncementsToDisplay($view->id, 'columna_anuncios');
        //dd($columna_anuncios);
        return view('proposito-mision-vision', compact('columna_anuncios'));
    }

    public function tablero()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);

        $view = View::where('name',$url)->first();
        $carrousel = Announcement::getAnnouncementsToDisplay($view->id, 'carrousel');
        $mosaico = Announcement::getAnnouncementsToDisplay($view->id, 'mosaico');
        // $display_announcements = compact('banner','cards','banner_panel','panels','carrousel','carrousel_secondary','mosaico','tabla_anuncios','cuatro_columnas','por_categorias');
        $display_announcements = compact('carrousel','mosaico');
        return view('tablero', compact('display_announcements'));
    }

    public function seed() {
        try {
            DB::beginTransaction();
            View::firstOrCreate(['name' => 'cumpleaños']);
            View::firstOrCreate(['name' => 'movimientos-personal']);
            View::firstOrCreate(['name' => 'eventos']);
            View::firstOrCreate(['name' => 'oficinas']);
            View::firstOrCreate(['name' => 'nuevos-ingresos']);
            View::firstOrCreate(['name' => 'calendario']);
            View::firstOrCreate(['name' => 'mis_beneficios']);
            View::firstOrCreate(['name' => 'convenios']);
            View::firstOrCreate(['name' => 'preguntas-frecuentes']);
            View::firstOrCreate(['name' => 'comites']);
            View::firstOrCreate(['name' => 'comisiones']);

            // Comités
            AnnouncementAttribute::updateOrCreate(
                [
                    'form_name' => 'members',
                ], [
                    'name' => 'Integrantes',
                    'attr_view' => 'textarea'
                ]
            );

            AnnouncementAttribute::updateOrCreate(
                [
                    'form_name' => 'responsable',
                ], [
                    'name' => 'Responsable',
                    'attr_view' => 'textarea'
                ]
            );

            // Convenios
            AnnouncementAttribute::updateOrCreate(
                [
                    'form_name' => 'company',
                ], [
                    'name' => 'Empresa',
                    'attr_view' => 'text'
                ]
            );

            // Calendario
            AnnouncementAttribute::updateOrCreate(
                [
                    'form_name' => 'time_start',
                ], [
                    'name' => 'Hora Inicio',
                    'attr_view' => 'time'
                ]
            );

            AnnouncementAttribute::updateOrCreate(
                [
                    'form_name' => 'time_end',
                ], [
                    'name' => 'Hora Fin',
                    'attr_view' => 'time'
                ]
            );

            // Comités y Comisiones V2
            AnnouncementType::updateOrCreate(
                [
                    'name' => 'images'
                ], [
                    'show_name' => 'Imágenes',
                    'view_file' => 'images'
                ]
            );

            // Comités
            AnnouncementType::updateOrCreate(
                [
                    'name' => 'committees'
                ], [
                    'show_name' => 'Comité',
                    'view_file' => 'committees'
                ]
            );

            // Preguntas Frecuentes
            AnnouncementType::updateOrCreate(
                [
                    'name' => 'simple_list'
                ], [
                    'show_name' => 'Lista Simple',
                    'view_file' => 'simple_list'
                ]
            );

            // Convenios
            AnnouncementType::updateOrCreate(
                [
                    'name' => 'convenios'
                ], [
                    'show_name' => 'Convenios',
                    'view_file' => 'convenios'
                ]
            );

            // Beneficios
            AnnouncementType::updateOrCreate(
                [
                    'name' => 'blog'
                ], [
                    'show_name' => 'Blog',
                    'view_file' => 'blog'
                ]
            );

            // Home
            AnnouncementType::updateOrCreate(
                [
                    'name' => 'slick'
                ], [
                    'show_name' => 'Deslizador',
                    'view_file' => 'slick'
                ]
            );

            // Oficinas
            AnnouncementType::updateOrCreate(
                [
                    'name' => 'mosaico_space'
                ], [
                    'show_name' => 'Mosaico Espaciado',
                    'view_file' => 'mosaico_space',
                    'max_quantity_showed' => 9,
                    'max_quantity_stored' => 20
                ]
            );

            // Calendario
            AnnouncementType::updateOrCreate(
                [
                    'name' => 'calendario'
                ], [
                    'show_name' => 'Calendario',
                    'view_file' => 'calendar',
                    'max_quantity_showed' => 9,
                    'max_quantity_stored' => 20
                ]
            );

            $types = AnnouncementType::get()->pluck('id', 'name')->toArray();
            $attributes = AnnouncementAttribute::get()->pluck('id', 'form_name')->toArray();
            // dd($attributes);

            // Comités y Comisiones V2
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['images'],
                'announcement_attribute_id' => $attributes['title'],
            ],[
                'required' => 1,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['images'],
                'announcement_attribute_id' => $attributes['image'],
            ],[
                'required' => 1,
            ]);

            // Convenios
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['committees'],
                'announcement_attribute_id' => $attributes['title'],
            ],[
                'required' => 1,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['committees'],
                'announcement_attribute_id' => $attributes['description'],
            ],[
                'required' => 1,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['committees'],
                'announcement_attribute_id' => $attributes['members'],
            ],[
                'required' => 1,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['committees'],
                'announcement_attribute_id' => $attributes['responsable'],
            ],[
                'required' => 1,
            ]);

            // Convenios
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['simple_list'],
                'announcement_attribute_id' => $attributes['title'],
            ],[
                'required' => 1,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['simple_list'],
                'announcement_attribute_id' => $attributes['description'],
            ],[
                'required' => 1,
            ]);

            // Convenios
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['convenios'],
                'announcement_attribute_id' => $attributes['title'],
            ],[
                'required' => 1,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['convenios'],
                'announcement_attribute_id' => $attributes['description'],
            ],[
                'required' => 1,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['convenios'],
                'announcement_attribute_id' => $attributes['image'],
            ],[
                'required' => 1,
            ]);
            // AnnouncementForm::updateOrCreate([
            //     'announcement_type_id' => $types['convenios'],
            //     'announcement_attribute_id' => $attributes['company'],
            // ],[
            //     'required' => 0,
            // ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['convenios'],
                'announcement_attribute_id' => $attributes['link'],
            ],[
                'required' => 0,
            ]);

            // Slick
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['slick'],
                'announcement_attribute_id' => $attributes['title'],
            ],[
                'required' => 1,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['slick'],
                'announcement_attribute_id' => $attributes['link'],
            ],[
                'required' => 0,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['slick'],
                'announcement_attribute_id' => $attributes['image'],
            ],[
                'required' => 1,
            ]);

            // Blog
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['blog'],
                'announcement_attribute_id' => $attributes['title'],
            ],[
                'required' => 1,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['blog'],
                'announcement_attribute_id' => $attributes['description'],
            ],[
                'required' => 1,
            ]);

            // Calendario
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['calendario'],
                'announcement_attribute_id' => $attributes['title'],
            ],[
                'required' => 1,
            ]);

            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['calendario'],
                'announcement_attribute_id' => $attributes['description'],
            ],[
                'required' => 1,
            ]);

            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['calendario'],
                'announcement_attribute_id' => $attributes['link'],
            ],[
                'required' => 0,
            ]);

            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['calendario'],
                'announcement_attribute_id' => $attributes['image'],
            ],[
                'required' => 1,
            ]);

            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['calendario'],
                'announcement_attribute_id' => $attributes['date'],
            ],[
                'required' => 1,
            ]);

            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['calendario'],
                'announcement_attribute_id' => $attributes['place'],
            ],[
                'required' => 1,
            ]);

            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['calendario'],
                'announcement_attribute_id' => $attributes['time_start'],
            ],[
                'required' => 1,
            ]);

            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['calendario'],
                'announcement_attribute_id' => $attributes['time_end'],
            ],[
                'required' => 1,
            ]);

            // Oficinas
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['mosaico_space'],
                'announcement_attribute_id' => $attributes['title'],
            ],[
                'required' => 1,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['mosaico_space'],
                'announcement_attribute_id' => $attributes['description'],
            ],[
                'required' => 1,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['mosaico_space'],
                'announcement_attribute_id' => $attributes['link'],
            ],[
                'required' => 1,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['mosaico_space'],
                'announcement_attribute_id' => $attributes['image'],
            ],[
                'required' => 1,
            ]);

            // Eventos
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['gallery'],
                'announcement_attribute_id' => $attributes['image'],
            ],[
                'required' => 1,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['gallery'],
                'announcement_attribute_id' => $attributes['gallery'],
            ],[
                'required' => 1,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['gallery'],
                'announcement_attribute_id' => $attributes['title'],
            ],[
                'required' => 1,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['gallery'],
                'announcement_attribute_id' => $attributes['date'],
            ],[
                'required' => 1,
            ]);
            AnnouncementForm::updateOrCreate([
                'announcement_type_id' => $types['gallery'],
                'announcement_attribute_id' => $attributes['place'],
            ],[
                'required' => 1,
            ]);

            $views = View::get()->pluck('id', 'name')->toArray();

            // Comités
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['banner'],
                'view_id' => $views['comites'],
            ]);
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['images'],
                'view_id' => $views['comites'],
            ]);

            // Comisiones
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['banner'],
                'view_id' => $views['comisiones'],
            ]);
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['images'],
                'view_id' => $views['comisiones'],
            ]);

            // Preguntas Frecuentes
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['banner'],
                'view_id' => $views['preguntas-frecuentes'],
            ]);
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['simple_list'],
                'view_id' => $views['preguntas-frecuentes'],
            ]);
            // Convenios
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['banner'],
                'view_id' => $views['convenios'],
            ]);
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['convenios'],
                'view_id' => $views['convenios'],
            ]);
            // Cumpleaños
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['banner'],
                'view_id' => $views['cumpleaños'],
            ]);
            // Movimientos de Personal
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['banner'],
                'view_id' => $views['movimientos-personal'],
            ]);
            // Eventos
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['banner'],
                'view_id' => $views['eventos'],
            ]);
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['gallery'],
                'view_id' => $views['eventos'],
            ]);
            // Oficinas
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['banner'],
                'view_id' => $views['oficinas'],
            ]);
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['mosaico_space'],
                'view_id' => $views['oficinas'],
            ]);
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['banner'],
                'view_id' => $views['nuevos-ingresos'],
            ]);
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['banner'],
                'view_id' => $views['calendario'],
            ]);
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['calendario'],
                'view_id' => $views['calendario'],
            ]);
            // AnnouncementTypePerView::firstOrCreate([
            //     'announcement_type_id' => $types['banner'],
            //     'view_id' => $views['mis_beneficios'],
            // ]);
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['blog'],
                'view_id' => $views['mis_beneficios'],
            ]);
            AnnouncementTypePerView::firstOrCreate([
                'announcement_type_id' => $types['slick'],
                'view_id' => $views['home'],
            ]);
            DB::commit();
        } catch(\Throwable $th) {
            DB::rollback();
            dd($th);
        }
        dd('announcements seeded');
    }
}
