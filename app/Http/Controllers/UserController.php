<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Permission;
use App\Models\PermissionRole;
use App\Employee;
use App\EmployeeForm;
use App\EmployeeMovement;
use App\Handlers\UserHandler;
use App\Http\Controllers\Vacations\PleaManager;
use App\Http\Requests\UserRequest;
use App\Models\Area;
use App\Models\Department;
use App\Models\Direction;
use App\Models\Enterprise;
use App\Models\JobPosition;
use App\Models\Region;
use App\Models\Vacations\Benefits;
use App\User;
use App\Models\Moodle\User as UserMdl;
use DB;
use Carbon\Carbon;
use Exception;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function logActivity($request){
    LogActivity::create([
    'user_id'       =>   Auth::id(),
    'user_email'    =>   Auth::user()->email,
    'tag'           =>  $request->method(),
    'url'           =>  $request->fullUrl(),
    'user_agent'    =>  \Illuminate\Support\Facades\Request::header('User-Agent'),
    'ip_address'    =>  \Illuminate\Support\Facades\Request::ip()
    ]);
    }*/

    public function __construct()
    {
        $this->middleware('auth');

        // ==== Admin Usuarios ====
        $this->middleware('permission:user_admin')->only(['create', 'store', 'show', 'edit', 'update', 'destroy', 'activate']);
        // ==== /Admin Usuarios ====

        // $this->path = getcwd() . '/uploads/';
    }

    public function index(Request $request)
    {
        if(!Auth()->user()->isAdminOrHasRolePermission('see_users')) {
            flash('No tienes acceso a esta acción...')->error();
            return redirect('home');
        }

        $usuarios = User::with(['employee' => function ($q) {
            $q->withTrashed();
        }])
            ->whereHas('employee', function ($q) {
                $q->withTrashed();
            })
            ->orderBy('id', 'ASC')
            ->get();

        foreach ($usuarios as $key => $usuario) {
            $direction = Direction::select('id', 'name')->where('id', $usuario->employee->direccion)->first();
            $job = JobPosition::select('id', 'name')->where('id', $usuario->employee->job_position_id)->first();
            /* if($direction != null) { $usuario->employee->direccion = $direction->name; } */

            if (isset($usuario->employee->jobPosition->area->department->direction->name)) {
                $usuario->employee->direccion = $usuario->employee->jobPosition->area->department->direction->name;
            } else {
                $usuario->employee->direccion = 'N/A';
            }

            if (isset($usuario->employee->jobPosition->area->department->name)) {
                $usuario->employee->departamento = $usuario->employee->jobPosition->area->department->name;
            } else {
                $usuario->employee->departamento = 'N/A';
            }

            if ($job != null) {
                $usuario->employee->job_position_id = $job->name;
            }


            /*$departamentos = Department::orderBy('name', 'ASC')->pluck('name', 'id')->unique();
            foreach ($departamentos as $id => $depto) {
                if($usuario->employee->jobPosition->area->department->name == $depto) {
                    $deptoID = $id;
                }
            }

            $puestos = JobPosition::orderBy('name', 'ASC')->pluck('name', 'id')->unique();
            foreach ($puestos as $id => $puesto) {
                if($usuario->employee->jobPosition->name == $puesto) {
                    $puesID = $id;
                }
            }

            Employee::where('id', $usuario->employee_id)->update([
                'direccion' => $usuario->employee->jobPosition->area->department->direction->id,
                'seccion' => '1',
                'division' => $deptoID,
                'job_position_id' => $puesID,
            ]);*/
        }
        $useVacation = class_exists(PleaManager::class) && PleaManager::isVacationModuleInstalled();
        return view('/userAdmin/index', compact(['usuarios'], 'useVacation'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usuarios = User::with('employee')
            ->whereHas('employee')
            ->orderBy('id', 'DESC')
            ->get();

        //columnas de la tabla para generar inputs en la vista
        $inputs = EmployeeForm::orderBy('id', 'ASC')->where('active', true)->get();

        $directions = Direction::select('id', 'name', 'description')->orderBy('id', 'DESC')->get();
        $enterprises = Enterprise::select('id', 'name')->orderBy('name', 'ASC')->get();
        //dd($enterprises);
        $bosses = JobPosition::orderBy('id', 'ASC')->get();
        //$levels = JobPositionLevel::orderBy('id', 'ASC')->get();

        $regions = Region::select('id', 'name')->orderBy('name', 'ASC')->get();

        foreach ($inputs as $input) {
            $form[] = $input->name;
        }

        $columns = Schema::getColumnListing('employees');

        // dd($form, $columns);

        foreach ($form as $column) {
            $name = EmployeeForm::select('name_show')->where('name', $column)->where('active', true)->first();
            $fields[] = [
                'type' => Schema::getColumnType('employees', $column),
                'column' => $column,
                'nameShow' => $name->name_show,
                //'value' => $usuario->employee->$column
            ];
        }

        // independización de estrucura de dirección->depto->área->puesto
        // $direcciones = Direction::orderBy('name', 'ASC')->pluck('name', 'id');
        // $departamentos = Department::orderBy('name', 'ASC')->pluck('name', 'id')->unique();
        // la consulta se deja con el pluck, pero sin mandar id para que respete el distinct
        // $areas = Area::orderBy('name', 'ASC')->pluck('name', 'id')->unique();
        $puestos = JobPosition::orderBy('name', 'ASC')->get();
        // dd($fields);

        return view('userAdmin.create', compact(['fields', 'directions', 'usuarios', 'enterprises', 'regions'],
            // 'direcciones',
            // 'departamentos',
            // 'areas',
            'puestos'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $rules = array(
            'idempleado' => 'required|unique:employees',
            'nombre' => 'required',
            'paterno' => 'required',
            'materno' => 'required',
            'rfc' => 'required|unique:employees',
            'curp' => 'required',
            'correoempresa' => 'required',
            'password' => 'required'
        );

        $error = Validator::make($request->all(), $rules);
        if ($error->fails()) {
            return redirect()->back()->withInput()->withErrors(['errors' => $error->errors()->first()]);
        }

        $data = $request->all();
        //dd($data);
        $data['password'] = Hash::make($data['password']);

        //creates
        //Guardar en la tabla de empleados
        \DB::beginTransaction();
        try {
            //code...

            // ============  Estructura de puesto  ============
            $job_position = JobPosition::findOrFail($data['job_position_id']);
            $area = $job_position->area ?? null;
            $department = $job_position->area->department ?? null;
            $direction = $job_position->area->department->direction ?? null;
            $data['direccion'] = $direction->id ?? null;
            $data['division'] = $department->id ?? null;
            $data['seccion'] = $area->id ?? null;
            // ============ /Estructura de puesto  ============

            $newEmployee = Employee::create($data);

            //Guardar en la tabla de users
            User::create([
                'employee_id' => $newEmployee->id,
                'first_name' => $newEmployee->nombre,
                'last_name' => $newEmployee->paterno . ' ' . $newEmployee->materno,
                'email' => $newEmployee->correoempresa,
                'password' => $newEmployee->password
                // 'role' => $newEmployee->rol,
            ]);

            // Guardar en la tabla de mdl_user
            $newUser = User::where('employee_id', $newEmployee->id)->first();

            // UserMdl::create([
            //     'username' => $newUser->email,
            //     'password' => $newUser->password,
            //     'firstname' => $newUser->first_name,
            //     'lastname' => $newUser->last_name,
            //     'email' => $newUser->email,
            //     'icq' => $newUser->id,
            //     'skype' => $newEmployee->nacimiento,
            //     'yahoo' => $newEmployee->curp,
            //     'aim' => $newEmployee->sexo,
            //     'confirmed' => 1,
            //     'mnethostid' => 1
            // ]);

            \DB::commit();
            return redirect()->route('admin-de-usuarios.index')->with('success0', $newEmployee->idempleado);
        } catch (\Throwable $th) {
            //throw $th;
            //dd($th->getMessage());
            \DB::rollback();
            return redirect()->route('admin-de-usuarios.index')->withErrors(['Lo sentimos ocurrió un error al crear el usuario, por favor inténtalo más tarde...'. $th->getMessage()]);
        }

        return redirect()->route('admin-de-usuarios.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        //Datos de employees de usuarios
        //$usuario = User::where('employee_id',$id)->with('employee')->first();
        $users = User::with('employee')
            ->whereHas('employee')
            ->orderBy('id', 'DESC')
            ->get();

        $directions = Direction::select('id', 'name', 'description')->orderBy('id', 'DESC')->get();
        $enterprises = Enterprise::select('id', 'name')->orderBy('name', 'ASC')->get();
        $bosses = JobPosition::orderBy('id', 'ASC')->get();

        $usuario = User::where('employee_id', $id)->with(['employee' => function ($q) {
            $q->withTrashed();
        }])
            ->whereHas('employee', function ($q) {
                $q->withTrashed();
            })
            ->orderBy('id', 'DESC')->withTrashed()->first();

        $job = JobPosition::where('id', $usuario->employee->job_position_id)->first();
        //dd($job->area->name);

        $regions = Region::select('id', 'name')->orderBy('name', 'ASC')->get();

        //campos de la tabla de employees_form
        $inputs = EmployeeForm::orderBy('id', 'ASC')->where('active', true)->get();

        foreach ($inputs as $input) {
            $form[] = $input->name;
        }

        $columns = Schema::getColumnListing('employees');

        foreach ($form as $column) {
            $name = EmployeeForm::select('name_show')->where('name', $column)->where('active', true)->first();
            $fields[] = [
                'type' => Schema::getColumnType('employees', $column),
                'column' => $column,
                'nameShow' => $name->name_show,
                'value' => $usuario->employee->$column,
            ];
        }

        // independización de estrucura de dirección->depto->área->puesto
        // $direcciones = Direction::orderBy('name', 'ASC')->pluck('name', 'id');
        // $departamentos = Department::orderBy('name', 'ASC')->pluck('name', 'id')->unique();
        // la consulta se deja con el pluck, pero sin mandar id para que respete el distinct
        // $areas = Area::orderBy('name', 'ASC')->pluck('name', 'id')->unique();
        $puestos = JobPosition::orderBy('name', 'ASC')->get();

        return view('/userAdmin/edit', compact(['usuario', 'fields', 'directions', 'job', 'users', 'enterprises', 'regions'],
            // 'direcciones',
            // 'departamentos',
            // 'areas',
            'puestos'    
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $request = $request->validated();
        $newPassword = $request['password'];
        $oldPassword = $request['oldPassword'];

        $user = User::where('id', $id)->has('employee')->first();
        if ($newPassword) {
            $request['password'] = Hash::make($request['password']);
        } else {
            unset($request['password']);
        }
        unset($request['oldPassword']);

        \DB::beginTransaction();
        try {
            $this->validateEmployeeMovement($user->employee, $request['job_position_id']);
            $old_boss = $user->getAuthorizatorID();
            //UPDATE datos de Personal
            // ============  Estructura de puesto  ============
            $job_position = JobPosition::findOrFail($request['job_position_id']);
            $area = $job_position->area ?? null;
            $department = $job_position->area->department ?? null;
            $direction = $job_position->area->department->direction ?? null;
            $request['direccion'] = $direction->id ?? null;
            $request['division'] = $department->id ?? null;
            $request['seccion'] = $area->id ?? null;
            // ============ /Estructura de puesto  ============
            // dd($request);
            $user->employee->update($request);
            if(!is_null($user->profile)) {
                $user->profile->update([
                    'name' => $request['nombre'],
                    'surname_father' => $request['paterno'],
                    'surname_mother' => $request['materno'],
                    'cellphone' => $request['celular'],
                    'phone' => $request['telefono'],
                    'gender' => $request['sexo'],
                    'date_birth' => $request['nacimiento'],
                    'rfc' => $request['rfc'],
                    'igss' => $request['nss'],
                    'irtra' => $request['extra1']
                ]);
            }

            $user->update([
                'first_name' => $user->employee->nombre,
                'last_name' => $user->employee->LastNames,
                'email' => $user->employee->correoempresa
                // 'role' => $request->input('rol'),
            ]);

            if (isset($request['password'])) {
				$user->update([
					'password' => $request['password']
                ]);     
            }
            
            // UserMdl::where('icq', $id)->update([
            //     'firstname' => $request['nombre'],
            //     'lastname' => $request['paterno'].' '.$request['materno'],
            //     'email' => $user->employee->correoempresa,
            //     'username' => $user->employee->correoempresa,
            //     'password' => $user->password,
            //     'skype' => $request['nacimiento'],
            //     'yahoo' => $request['curp'],
            //     'aim' => $request['sexo']
            // ]);

            if (class_exists(UserHandler::class)) {
                $userHandler = new UserHandler($user);
                $userHandler->updateIncidentData($old_boss);
            }

            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollback();
            // if (isset($e->errorInfo)) {
            //     $errors = $e->errorInfo;
            //     return view('errors.globalError', compact('errors'));
            // } elseif (!isset($e->errorInfo)) {
            //     return redirect()->back()->with('success', 'Ok')->withInput();
            // }
            return redirect()->route('admin-de-usuarios.index')->withErrors(['Lo sentimos ocurrió un error al actualizar el usuario, por favor inténtalo más tarde...'. $th->getMessage()]);
        }

        return redirect()->back()->with('success', 'Ok');
    }

    public function validateEmployeeMovement($employee, $new_job_position_id) {
        $old_job_position = JobPosition::find($employee->job_position_id);
        $new_job_position = JobPosition::find($new_job_position_id);

        // dd($old_job_position->id, $new_job_position->id);
        // Validar si no es la misma
        if($old_job_position != null && $new_job_position != null) { // Validación para usuarios sin puesto
            if($old_job_position->id != $new_job_position->id) {
                // Validar si es promoción o sólo movimiento
                $user = User::where('employee_id', $employee->id)->first();

                $to_del_emp_mov = EmployeeMovement::where('user_id', $user->id)->first();
                if($to_del_emp_mov != null)
                    $to_del_emp_mov->delete();

                $employee_movement = new EmployeeMovement;
                $employee_movement->user_id = $user->id;
                $type = 'none';
                if($new_job_position->job_position_level_id < $old_job_position->job_position_level_id) {
                    $type = 'promotion';
                } else {
                    $type = 'movement';
                }

                $employee_movement->type = $type;
                $employee_movement->old_job_position_id = $old_job_position->id;
                $employee_movement->new_job_position_id = $new_job_position->id;
                $employee_movement->save();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        $user = User::where('employee_id', $id)->with('employee')->first();

        \DB::beginTransaction();
        try {
            // $mdlUser = UserMdl::where('icq', $user->id)->first();
            $userId = $user->employee->idempleado;
            //code...
            // if($mdlUser) {
            //     $mdlUser->update([
            //         'suspended' => 1,
            //     ]);
            // }
            $user->delete();
            $user->employee->delete();
            \DB::commit();
            return redirect()->back()->with('success1', $userId);
        } catch (\Throwable $th) {
            //throw $th;
            \DB::rollback();
            return redirect()->back()->withErrors(['Lo sentimos ocurrió un error al suspender al usuario, por favor inténtalo más tarde.']);
        }

        return redirect()->back()->with('success');
    }

    public function activate(Request $request, $id)
    {
        $user = User::where('employee_id', $id)->with(['employee' => function ($q) {
            $q->withTrashed();
        }])
            ->whereHas('employee', function ($q) {
                $q->withTrashed();
            })
            ->orderBy('id', 'DESC')->withTrashed()->first();

        \DB::beginTransaction();
        try {
            $userId = $user->employee->idempleado;
            // $mdlUser = UserMdl::where('icq', $user->id)->first();
            //code...
            // if($mdlUser) {
            //     $mdlUser->update([
            //         'suspended' => 0,
            //     ]);
            // }
            $user->restore();
            $user->employee->restore();
            \DB::commit();
            return redirect()->back()->with('success2', $userId);
        } catch (\Throwable $th) {
            //throw $th;
            \DB::rollback();
            return redirect()->back()->withErrors(['Lo sentimos ocurrió un error al reactivar al usuario, por favor inténtalo más tarde.']);
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generarContrato($id_employee)
    {
        $employee = Employee::where('id', $id_employee)->first();
        $enterprise = Enterprise::where('id', $employee->enterprise_id)->first();

        $nombre = $employee->nombre . '_' . $employee->paterno . '_' . $employee->materno . '.pdf';
        $job = JobPosition::where('id', $employee->job_position_id)
            ->first();

        $pdf = PDF::loadView('userAdmin.pdf.contrato', compact('employee', 'job', 'enterprise'));

        return $pdf->download('ContratoPersonal-' . $nombre);
    }

    public function balances($id)
    {
        $user = User::find($id);

        if ($user) {
            $data = EmployeeController::getEmployeeBalance($user->id);
            if ($benefit = Benefits::where('name', 'Vacaciones')->first()) {
                $data['vacations'] = EmployeeController::getBalanceDetail($benefit->id, $user->id);
            }
            $data['time'] = $time = PleaManager::getEmployeeTimeBalance($user);
            return view('userAdmin.balances', $data);
        }
        return redirect()->route('admin-de-usuarios.index');
    }

    public function debugUserEmails() {
        if(Auth::user()->id != 1) {
            return back();
        }
        $usuarios = User::with('employee')
            ->whereHas('employee')
            ->orderBy('id', 'DESC')
            ->get();

        $data = [];
        foreach($usuarios as $user) {
            if($user->email != $user->employee->correoempresa) {
                $data[] = [
                    'users' => $user->email,
                    'employees' => $user->employee->correoempresa
                ];
            }
        }

        dd($data, 'usuarios en los que no coincide su correo');
    }

    public function initializeMoodleData() {
        // $users = User::with(['employee' => function ($q) {
        //     $q->withTrashed();
        // }])
        // ->whereHas('employee', function ($q) {
        //     $q->withTrashed();
        // })
        // ->withTrashed()->get();
        $users = User::get();
        $created = [];
        foreach($users as $user) {
            try {
                DB::beginTransaction();
                $mdlUser = UserMdl::where('icq', $user->id)->first();
                if(!$mdlUser) {
                    $mdlUser = UserMdl::create([
                        'username' => $user->email,
                        'password' => ($user->password?$user->password:'-'),
                        'firstname' => ($user->first_name?$user->first_name:'-'),
                        'lastname' => ($user->last_name?$user->last_name:'-'),
                        'email' => $user->email,
                        'icq' => $user->id,
                        'skype' => $user->employee->nacimiento ?? Carbon::now(),
                        'yahoo' => $user->employee->curp ?? '-',
                        'aim' => $user->employee->sexo ?? '-',
                        'confirmed' => 1,
                        'mnethostid' => 1,
                        'suspended' => $user->trashed()?1:0,
                    ]);
                }
                $created[] = $user->email;
                DB::commit();
            } catch(\Throwable $th) {
                DB::rollback();
                dd($th);
            }
        }
        dd('listop, moodle users created successfully!', $created);
    }

    public function setMoodleUsersLanguage() {
        try {
            DB::beginTransaction();
            UserMdl::whereNotNull('icq')->update(['lang' => 'es_mx']);
            DB::commit();
        } catch(\Throwable $th) {
            DB::rollback();
            dd('Error', $th->getMessage());
        }
        dd('Lenguaje actualizado para todas las personas');
    }

    public function initializePermissionsData() {
        try {
            DB::beginTransaction();

            $roles_permissions = [
                [
                    'name' => 'announcement_admin',
                    'description' => 'Administrador de Anuncios',
                    'permissions' => [
                        [
                            [
                                'action' => 'see_announcements'
                            ], [
                                'name' => 'Índice Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite ingresar al index del controlador de anuncios'
                            ]
                        ], [
                            [
                                'action' => 'create_announcements'
                            ], [
                                'name' => 'Crear Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite crear anuncios'
                            ]
                        ], [
                            [
                                'action' => 'edit_announcements'
                            ], [
                                'name' => 'Editar Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite editar anuncios'
                            ]
                        ], [
                            [
                                'action' => 'delete_announcements'
                            ], [
                                'name' => 'Eliminar Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite eliminar anuncios'
                            ]
                        ], [
                            [
                                'action' => 'activate_announcements'
                            ], [
                                'name' => 'Actiar/Desactivar Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite activar/desactivar anuncios'
                            ]
                        ], [
                            [
                                'action' => 'announcement_banner'
                            ], [
                                'name' => 'Administra Banner',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Banner'
                            ]
                        ], [
                            [
                                'action' => 'announcement_carrousel'
                            ], [
                                'name' => 'Administra Carrousel',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Carrousel'
                            ]
                        ], [
                            [
                                'action' => 'announcement_mosaic'
                            ], [
                                'name' => 'Administra Mosaico',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Mosaico'
                            ]
                        ], [
                            [
                                'action' => 'announcement_columns'
                            ], [
                                'name' => 'Administra Columna Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Columna Anuncios'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'user_admin',
                    'description' => 'Administrador de Usuarios',
                    'permissions' => [
                        [
                            [
                                'action' => 'see_users'
                            ], [
                                'name' => 'Ver Usuarios',
                                'module' => 'General',
                                'description' => 'Permite ingresar al index del controlador de usuarios'
                            ]
                        ], [
                            [
                                'action' => 'user_admin'
                            ], [
                                'name' => 'Administrador de Usuarios',
                                'module' => 'General',
                                'description' => 'Administra usuarios de la plataforma'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'profile_admin',
                    'description' => 'Administrador de Expedientes',
                    'permissions' => [
                        [
                            [
                                'action' => 'see_users'
                            ], [
                                'name' => 'Ver Usuarios',
                                'module' => 'General',
                                'description' => 'Permite ingresar al index del controlador de usuarios'
                            ]
                        ], [
                            [
                                'action' => 'profile_admin'
                            ], [
                                'name' => 'Administrador de Expedientes',
                                'module' => 'Expediente',
                                'description' => 'Administra expediente de usuarios de la plataforma'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'incidents_admin',
                    'description' => 'Administrador de Asistencias/Incidencias',
                    'permissions' => [
                        [
                            [
                                'action' => 'incidents_admin'
                            ], [
                                'name' => 'Administrador de Asistencias/Incidencias',
                                'module' => 'Gestión de Asistencias/Incidencias',
                                'description' => 'Administrador del módulo de Asistencias/Incidencias'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'performance_evaluation_admin',
                    'description' => 'Administrador de Evaluación de Desempeño (DNC)',
                    'permissions' => [
                        [
                            [
                                'action' => 'performance_evaluation_admin'
                            ], [
                                'name' => 'Administrador de Evaluación de Desempeño',
                                'module' => 'Evaluación de Desempeño',
                                'description' => 'Administrador del módulo de Evaluación de Desempeño'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'evaluation_of_results_admin',
                    'description' => 'Administrador de Evaluación de Resultados',
                    'permissions' => [
                        [
                            [
                                'action' => 'evaluation_of_results_admin'
                            ], [
                                'name' => 'Administrador de Evaluación de Resultados',
                                'module' => 'Evaluación de Resultados',
                                'description' => 'Administrador del módulo de Evaluación de Resultados'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'scale_admin',
                    'description' => 'Administrador de Escalafón',
                    'permissions' => [
                        [
                            [
                                'action' => 'scale_admin'
                            ], [
                                'name' => 'Administrador de Escalafón',
                                'module' => 'Capacitación',
                                'description' => 'Administrador del módulo de Capacitación'
                            ]
                        ]
                    ]
                ]
            ];
            // evaluation of results
            foreach($roles_permissions as $role_data) {
                $role = Role::updateOrCreate(
                    [
                        'name' => $role_data['name']
                    ], [
                       'description' => $role_data['description']
                    ]
                );
                foreach ($role_data['permissions'] as $permission_data) {
                    $fixed_data = $permission_data[0];
                    $update_data = $permission_data[1];
                    $permission = Permission::updateOrCreate($fixed_data, $update_data);
                    // dd($permission_data, $fixed_data, $update_data);
                    PermissionRole::firstOrCreate([
                        'role_id' => $role->id,
                        'permission_id' => $permission->id
                    ]);
                }
            }

            DB::commit();
        } catch(\Throwable $th) {
            DB::rollback();
            dd($th, $role_data, $permission_data);
        }
        dd('roles y permisos creados correctamente');
    }

    public function authMeWith($user_email) {
        if(!Auth::user()->isSuperAdmin()) {
            return ('/');
        }
        try {
            $user = User::where('email', $user_email)->first();
            if(!$user) {
                throw new Exception("Usuario no encontrado", 1);
            } else {
                Auth::loginUsingId($user->id);
                flash('Sesión iniciada con éxito!')->success();
            }
        } catch(\Throwable $th) {
            flash('Error en el proceso... '.$th->getMessage())->error();
        }
        return redirect('/home');
    }
}
