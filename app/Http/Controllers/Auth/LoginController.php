<?php

namespace App\Http\Controllers\Auth;

use Hash;
use App\User;
use App\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Helpers\ClockLogManager;
use App\Http\Requests\CheckEmployeeRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    protected $clocklogManager;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ClockLogManager $clocklogManager)
    {
        $this->middleware('guest')->except(['logout', 'check_employee', 'confirm_employee', 'success_check']);
        $this->clocklogManager = $clocklogManager;
    }

    public function check_employee(CheckEmployeeRequest $request){
        $user = Employee::where('idempleado', $request->number)->first()->user;
        if($user) {
            return view('employee_login.auth-photo', ['number' => $request->number, 'photo' => $user->getProfileImage()]);
        }else{
            $errorUser = 'El número de empleado no tiene un usuario activo.';
            return redirect()->back()->withErrors(['msg' => $errorUser]);
        }   
    }

    public function confirm_employee(CheckEmployeeRequest $request){
        $employee = Employee::has('region.workshiftFixed.schedule')->where('idempleado', $request->number)->first();
        if($employee){
            $user = $employee->user;
            if($user){
                $date = date('Y-m-d');
                $fullDate = date('Y-m-d H:i:s');
                $checkDay = $checkIncident = $checkEvent = null;
                if(($checkDay = $user->isDayWorkable($date)) && ($checkIncident = !$this->clocklogManager->isDayAnDayIncident($user, $date)) && ($checkEvent = !$this->clocklogManager->isDayBlockByEvent($user, $date))){
                    $clocklog = [
                        'request_ip' => $request->ip(),
                        'date' => $fullDate,
                        'status' => 'pending',
                        'from' => 'web',
                    ];
                    $arrive = $this->clocklogManager->getArriveData(date('H:i'), date('D'), $employee->region_id);
                    if(!$this->clocklogManager->checkExistingLog($user, $date, $arrive['type'])){
                        $clocklog += $arrive;
                        try {
                            $user->clocklogs()->create($clocklog);
                        } catch (\Throwable $th) {
                            return redirect()->route('checklogin')->withErrors(['msg' => 'Algo salio mal al intentar checar']);
                        }
                        return redirect()->route('success_check', ['name' => $user->FullName, 'number' => $request->number, 'photo' => $user->getProfileImage()]);
                    }
                    //El usuario ya tiene una checada para el mismo tipo, ya sea entrada o salida
                    $type = $arrive['type']==='in'?'Entrada':'Salida';
                    return redirect()->route('checklogin')->withErrors(['msg' => 'Ya tienes una checada para tu ' . $type]);
                }
                $msg = $this->clocklogManager->sendFailMail($user, $fullDate, !$checkDay, !$checkIncident, !$checkEvent);
                //El usuario no labora este día
                return redirect()->route('checklogin')->withErrors(['msg' => 'No puedes checar este día, revisa tu horario, permisos o eventos' . $msg]);
            }
            //El empleado no tiene ralación con un usuario o , por lo tanto no goza privilegios
            return redirect()->route('checklogin')->withErrors(['msg' => 'No eres un usuario activo de la plataforma']);
        }
        return redirect()->route('checklogin')->withErrors(['msg' => 'Tu horario no ha sido creado']);
    }

    public function success_check($name, $number){
        $user = Employee::where('idempleado', $number)->first()->user;
        if($user){
            $user->load('notificationMessages');
            $messages = $user->notificationMessages;
            $photo = $user->getProfileImage();
            return view('employee_login.success', compact('name', 'number', 'messages', 'photo'));
        }

        return view('employee_login.login');
    }

    public function mdlLogin($email, $token)
    {
        $user = User::where('email', $email)
                    ->where('session_token', $token)
                    ->first();

        if (! is_null($user) && Auth::loginUsingId($user->id))
        {
            $user->session_token = null;
            $user->save();
            return redirect()->to('/home');
        }
        
        flash('Usuario o contraseña incorrectos');
        return redirect()->to('/login');
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
     public function logout()
    {
        Auth::logout();

        $moodle = config('app.moodle_login'); 

        if($moodle) {
            return redirect()->to('moodle/login/logout.php?loginpage=laravel');
        } else {
            return redirect()->to('/');
        }
    }
}
