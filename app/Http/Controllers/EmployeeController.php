<?php

namespace App\Http\Controllers;

use App\User;
use App\Employee;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Vacations\Event;
use App\Models\Vacations\Balances;
use App\Models\Vacations\Benefits;
use App\Models\Vacations\Incident;
use App\Http\Controllers\Controller;
use App\Models\Vacations\Userfields;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Vacations\PleaManager;

class EmployeeController extends Controller
{
    public function balances()
    {   
        $data = EmployeeController::getEmployeeBalance();
        if($benefit = Benefits::where('name', 'Vacaciones')->first()){
            $data['vacations'] = EmployeeController::getBalanceDetail($benefit->id);
        }
        $data['time'] = $time = PleaManager::getEmployeeTimeBalance(Auth::user());
        return view('vacations.common.balances', $data);
    }

    public function calendar()
    {
        $data = EmployeeController::getEmployeeBalance();
        $data['vacations'] = EmployeeController::getBalanceDetail(5);
        $data['config'] = $this->config;
        return view('common.calendar',$data);
    }

    public static function getVacationsDetail($bid, $uid = null ){
        if($uid == null) $user_id = Auth::user()->id;
        else $user_id = $uid;
        //Se obtienen los balances ordenados, primero los null, y despues de menor a mayor
        $rows = Balances::where([['user_id',$user_id],['benefit_id',$bid]])
                    ->orderBy('until','ASC')
                    ->get();

        $data = [];
        $pending = 0;
        $amount = 0;
        $last = null;
        //Si no se encontraron registros se le crea una del año siguiente pero sin dias para pedir
        if(!$rows->first()) {
            $data['rows'][] = new Balances(['year'=>date('Y') + 1, 'user_id'=>$user_id, 'benefit_id'=>$bid, 'pending' => $pending, 'amount' => $amount, 'js' => '']);
            $data['benefit'] = Benefits::find($bid);
            $data['pending'] = $pending;
            $data['amount'] = $amount;
            $data['solicited'] = 0;
            return($data);
        }        
        foreach($rows as $row) {
            $row->diff = $row->pending - $row->amount;
            $pending += $row->pending + $row->amount;
            // dd($pending, $row);
            $amount += $row->amount;
            $events = Event::where('status','pending')->where('js','like','%"id":'.$row->id.',%')->get();
            // dd($events, $row);
            $row->solicited = 0;
            foreach($events as $event) {
                $js = json_decode($event->js);
                foreach($js->balances as $b) {
                    if($b->id == $row->id)$row->solicited += $b->amount;
                }
            }
            if($row->until == null) {
                $last = $row;
            } else {
                $data['rows'][] = $row;
            }
        }
        $solicited = Incident::where([
            ['from_id',Auth::user()->id],
            ['status','pending'],
            ['event_id','<>','0'],
            ['benefit_id', $bid]
        ])->sum('amount');
        if(!$solicited) $solicited = 0;

        if($last)$data['rows'][] = $last;
            $data['benefit'] = Benefits::find($bid);
            $data['pending'] = $pending;
            $data['amount'] = $amount;
            $data['solicited'] = intval($solicited);
            $data['diff'] = $pending - $amount;
        // dd($data, $pending);
        return ($data);
    }

    public static function getBalanceDetail($bid, $uid = null )
    {
        if($uid == null) $user_id = Auth::user()->id;
        else $user_id = $uid;
        //Se obtienen los balances ordenados, primero los null, y despues de menor a mayor

        $vac = Benefits::where('name', 'Vacaciones')->first();
        $benefit = Benefits::find($bid);
        $rows = Balances::where([
            ['user_id',$user_id],
            ['benefit_id',$bid]
        ])
        ->when($benefit->id != $vac->id, function($q){
            $q->where('year', date('Y') + 1);
        })
        ->orderBy('until','ASC')
        ->get();

        $data = [];
        $pending = 0;
        $amount = 0;
        $last = null;
        //Si no se encontraron registros se le crea una del año siguiente pero sin dias para pedir
        if(!$rows->first()) {
            $data['rows'][] = new Balances(['year'=>date('Y') + 1,'user_id'=>$user_id, 'benefit_id'=>$bid, 'pending' => $pending, 'amount' => $amount]);
            $data['benefit'] = Benefits::find($bid);
            $data['pending'] = $pending;
            $data['amount'] = $amount;
            $data['solicited'] = 0;
            $data['diff'] = 0;
            return($data);
        }        
        foreach($rows as $row) {
            $row->diff = $row->pending - $row->amount;
            $pending += $row->pending;
            // dd($pending, $row);
            $amount += $row->amount;
            $events = Event::where('status','pending')->where('js','like','%"id":'.$row->id.',%')->get();
            // dd($events, $row);
            $row->solicited = 0;
            foreach($events as $event) {
                $js = json_decode($event->js);
                foreach($js->balances as $b) {
                    if($b->id == $row->id)$row->solicited += $b->amount;
                }
            }
            if($row->until == null) {
                $last = $row;
            } else {
                $data['rows'][] = $row;
            }
        }
        $solicited = Incident::where([
            ['from_id',Auth::user()->id],
            ['status','pending'],
            ['event_id','<>','0'],
            ['benefit_id', $bid]
        ])->sum('amount');
        if(!$solicited) $solicited = 0;

        if($last)$data['rows'][] = $last;
            $data['benefit'] = Benefits::find($bid);
            $data['pending'] = $pending;
            $data['amount'] = $amount;
            $data['solicited'] = intval($solicited);
            $data['diff'] = $pending - $amount;
        // dd($data, $pending);
        return ($data);
    }

    public static function getEmployeeForSuperBalances( $id = null )
    {
        if($id == null) {
            $user = Auth::user();
            $user_id = $user->id;
        } else {
            $user_id = $id;
            $user = User::where('id',$id)->first();
        }
        //Get balances
        $benefits = Benefits::whereIn('context', ['super', 'admin'])
        ->whereIn('group', ['incident', 'incapacity'])
        ->where('context', 'super')
        ->get();
        $balances = [];
        /*No suma bien */
        foreach($benefits as $benefit) {
            $balance = Balances::select(\DB::raw('SUM(amount) AS amount, SUM(pending) AS pending, benefit_id, user_id, year'))
            ->where([['user_id',$user_id],['benefit_id',$benefit->id]])
            ->whereHas('benefit',function($query) use ($user){
                $query->where('gender','x')
                    ->orWhere('gender', strtolower($user->getEmployeeAttr('sexo')));
            })
            ->groupBy('year')
            ->groupBy('user_id')
            ->groupBy('benefit_id')
            ->first();

            if($balance) {
                if($benefit->type === "pending"){
                    $balance->diff = $balance->pending - $balance->amount;
                }
                $balances[] = $balance;
            } else {
                if($benefit->gender != 'x' && $benefit->gender != strtolower($user->getEmployeeAttr('sexo'))) continue;
                $b = [
                    'amount' => 0,
                    'pending' => 0,
                    'benefit_id' => $benefit->id,
                    'user_id' => $user_id,
                    'year' => date('Y'),
                ];
                
                $b = new Balances($b);
                $b->load('benefit.jobBenefits');
                if($benefit->type === "pending"){
                    $b->diff = 0;
                }
                $balances[] = $b;
            }
        }
        return (['balances'=>$balances]);
    }

    public static function getEmployeeBalance( $id = null )
    {
        $user = $id?User::where('id',$id)->first():Auth::user();
        $user_id = $user->id;
        //Get balances
        $benefits = Benefits::whereHas('jobBenefits', function($q) use($user){
            $q->where('id', $user->getJobPositionID());
        })
        ->get();
        $balances = [];
        /*No suma bien */
        foreach($benefits as $benefit) {
            $balance = Balances::select(\DB::raw('SUM(amount) AS amount, SUM(pending) AS pending, benefit_id, user_id, year'))
            ->where([['user_id',$user_id],['benefit_id',$benefit->id]])
            ->whereHas('benefit',function($query) use ($user){
                $query->where('gender','x')
                    ->orWhere('gender',strtolower($user->getEmployeeAttr('sexo')));
            })
            ->groupBy('year')
            ->groupBy('user_id')
            ->groupBy('benefit_id')
            ->first();

            if($balance) {
                if($benefit->type === "pending"){
                    $balance->diff = $balance->pending - $balance->amount;
                }
                $balances[] = $balance;
            } else {
                if($benefit->gender != 'x' && $benefit->gender != strtolower($user->getEmployeeAttr('sexo'))) continue;
                $b = [
                    'amount' => 0,
                    'pending' => 0,
                    'benefit_id' => $benefit->id,
                    'user_id' => $user_id,
                    'year' => date('Y'),
                ];
                
                $b = new Balances($b);
                $b->load('benefit.jobBenefits');
                if($benefit->type === "pending"){
                    $b->diff = 0;
                }
                $balances[] = $b;
            }
        }
        return (['balances'=>$balances]);
    }

    public static function processTxt()
    {
        $eight = 8 * 60;
        $balances = Balances::where([['benefit_id',10],['pending','>',$eight]])->get(); //Arbitrario

        foreach($balances as $balance) {
            $d = 0;
            while($balance->pending > $eight) {
                $d++;
                $balance->pending -= $eight;
            }
            $b = EmployeeController::getBalanceDetail(11,$balance->user_id); //Arbitrario
            $b['rows'][0]->pending += $d;
            unset($b['rows'][0]->diff);
            $b['rows'][0]->save();
            $balance->save();
        }
    }

    public static function processComodin($date)
    {
        $year   = date('Y', $date);
        $users  = User::select('id')->get();
        $now    = date('Y-m-d H:m:s',$date);
        $until  = date('Y-m-d',mktime(0,0,0,12,31,$year)); //Arbitrario
        $b = [
            'amount' => 0,
            'pending' => 1,
            'benefit_id' => 6, //Arbitrario...
            'year' => date('Y'),
            'created_at' => $now,
            'updated_at' => $now,
            'until' => $until,
        ];
        $bash = [];
        echo "Generating Bash <br />";
        foreach($users as $user) {
            $b['user_id'] = $user->id;
            $bash[] = $b;
        }
        echo "Inserting Bash in Balances <br />";
        Balances::insert($bash);
    }

    public static function processVacationsCSV($data)
    {
        //Delete * 5 and 6
        $users = User::select(['id','number'])->get();
        $b = [];
        $u = [];
        $date = date('Y-m-d');

        foreach($users as $user) {
            if(isset($data[$user->number])) {
                $tmp = Userfields::where('user_id',$user->id)
                            ->where('fieldname','files')
                            ->where('value','like','%"week":"'.$data[$user->number]['files']['week'].'"%')
                            ->first();
                if($tmp) {
                    Balances::where('user_id',$user->id)->where('benefit_id','5')->update(
                        ['user_id'=>$user->id,'benefit_id'=>'5','amount'=>0,'pending'=>$data[$user->number]['VAC']['pending'],'year'=>$data[$user->number]['VAC']['year']]
                    );
                    Balances::where('user_id',$user->id)->where('benefit_id','6')->update(
                        ['user_id'=>$user->id,'benefit_id'=>'6','amount'=>0,'pending'=>(2 - $data[$user->number]['FD']['pending']),'year'=>$data[$user->number]['FD']['year']]
                    );


                    $tmp->value = json_encode(['from'=>$date,$data[$user->number]['files']]);
                    //if($tmp->id == '987') {dd($data[$user->number],$tmp); die;}
                    $tmp->save();
                    continue;
                }

                Balances::where('user_id',$user->id)->whereIn('benefit_id',['5','6'])->delete();
                //$amount = Incident::where([['benefit_id','5'],['user_id',$user->id],['event_id','<>','0'],['status','complete']])->sum('amount');
                if(isset($data[$user->number]['VAC']))$b[] = ['user_id'=>$user->id,'benefit_id'=>'5','amount'=>0,'pending'=>$data[$user->number]['VAC']['pending'],'year'=>$data[$user->number]['VAC']['year']];
                if(isset($data[$user->number]['FD'])) $b[] = ['user_id'=>$user->id,'benefit_id'=>'6','amount'=>0,'pending'=>(2 - $data[$user->number]['FD']['pending']),'year'=>$data[$user->number]['FD']['year']];
                if($data[$user->number]['files']['pdf']!="") $u[] = ['user_id'=>$user->id,'fieldname'=>'files','value'=>json_encode(['from'=>$date,$data[$user->number]['files']])];
            }
        }
        Balances::insert($b);
        Userfields::insert($u);
    }

    public static function processVacations($date)
    {
        $vacBenefit = Benefits::where('name', 'Vacaciones')->firstOrFail();

        $noMensajes = [];
        $year  = date('Y', $date) + 1;
        $month = date('m', $date);
        $day   = date('d', $date);
        $now   = date('Y-m-d H:m:s', $date);
        $until = date('Y-m-d',mktime(0,0,0,$month,$day,$year)); //Arbitrario
        // dd($year ,$month, $day);
        //Un join entre usuarios y balances, 
        //"until" es nulo, sean vacaciones, tienen que tener ser del mes actual y del dia
        //en resumen son los usuarios que cumplen años

        $users = User::select('id')
        ->with([
            'balances' => function($q) use($year, $vacBenefit){
                $q->whereNull('until')
                ->where('benefit_id', $vacBenefit->id)
                ->where('year', $year);    
            }
        ])
        ->whereHas('balances', function($q) use($year, $vacBenefit){
            $q->whereNull('until')
            ->where('benefit_id', $vacBenefit->id)
            ->where('year', $year);
        })
        ->whereHas('employee', function($q) use($month, $day){
            $q->whereMonth('ingreso', $month)
            ->whereDay('ingreso', $day);
        })
        ->get();
        
        $balance = [
            'benefit_id' => $vacBenefit->id, //Arbitrario...
            'year' => date('Y'),
            'created_at' => $now,
            'updated_at' => $now,
            'until' => $until,
        ];
        $bash = [];
        $in   = [];

        $errors = [];
        echo "Generating Bash <br />";
        foreach($users as $user) {
            \DB::beginTransaction();
            if($user->balances->count() > 0){
                try {
                    $user->balances[0]->until = $until;
                    $user->balances[0]->save();
                } catch (\Exception $th) {
                    $noMensajes[] = $user->id . ' | No pudimos actualizar la fecha until del Balance |  ' . $th->getMessage();
                    \DB::rollback();
                    continue;
                }

                try{
                    Balances::create([
                        'year' => $year + 1,
                        'amount' => 0,
                        'pending' => 0,
                        'benefit_id' => $vacBenefit->id,
                        'user_id' => $user->id,
                        'until' => null,
                    ]);
                } catch (\Exception $th) {
                    $noMensajes[] = $user->id . ' | No pudimos crear la fecha until del Balance |  ' . $th->getMessage();
                    \DB::rollback();
                    continue;
                }
            }else{
                $noMensajes[] = $user->id . ' |  Aqui se supone el usuario no tiene lineas y ps esta mal jajaj';
                //Por alguna razon el usuario ya tiene un año no tiene lineas de balances
            }
            \DB::commit();
            echo "<br>";
        }
        echo "Inserting Bash <br />";
        return $noMensajes;
    }

    public static function removeBenefits($date)
    {
        $now   = date('Y-m-d', $date);
        $balances = Balances::whereDate('until','<',$now)->delete();
    }

    public static function erraseDuplis($id){
        //SELECT * FROM balances WHERE id IN ( SELECT id FROM balances GROUP BY year,until HAVING count(id) >1 ) and user_id = 291
        //
        $array = Balances::select('id')
            ->where('user_id',$id)
            ->groupBy('year','until')
            ->havingRaw('count(id) > 1')
            ->get();
        foreach ($array as  $value) {
            $value->forceDelete();
        }
        // dd($array);
        // $balances = Balances::whereIn($array)
        // ->where('user_id',$id)
        // ->get();
    }

    public static function fixVacations()
    {
        /*$today  = date('Y-m-d');
        $year   = date('Y');
        Balances::where([['benefit_id',5],['year',$year-1],['until','>',$today]])->update(['year'=>$year]);
        Balances::where([['benefit_id',5],['year',$year-1],['until',null]])->update(['year'=>$year]);*/
    }

    public static function setPartials($date)
    {
        $noMensajes = [];

        $vacBenefit = Benefits::where('name', 'Vacaciones')->firstOrFail();

        $users = User::select('id', 'employee_id')
        ->with(['employee' => function($q){
            $q->select('id', 'ingreso');
        }])
        ->has('employee')
        ->whereDoesntHave('balances', function ($query) use($vacBenefit){
            $query->where('benefit_id', $vacBenefit->id);
        })->get();
        foreach ($users as $user) {
            $year = $user->getMyNextYear($date);
            try {
                Balances::create([
                    'year' => $year,
                    'amount' => 0,
                    'pending' => 0,
                    'benefit_id' => $vacBenefit->id,
                    'user_id' => $user->id,
                    'until' => null,
                ]);
            } catch (\Exception $e) {
                //throw $th;
            }
        }
        $bash = [];
        $balance = [];
        //Tomar la lista de usuarios
        $users = User::select('id', 'employee_id')
        ->with(['employee' => function($q){
            $q->select('id', 'ingreso');
        }])
        ->has('employee')
        ->get();
        //Se tienen todos los usuarios

        $balances = Balances::select('id', 'user_id')
        ->where([
            ['until', null],
            ['benefit_id', $vacBenefit->id],
        ])->get();
        //se obtienen todos los balances con fecha until en null y que sean del beneficio 5 "Vacaciones"
        //Se crea un arreglo con [0] = id del registro y [1] = id del usuario
        
        foreach($balances as $balance) {
            $bash[$balance->user_id] = ['id' => $balance->id, 'user_id' => $balance->user_id];
        }
        // $now = date('Y-m-d H:m:s');
        $now = date('Y-m-d H:m:s', $date);
        $balance = [
            'amount' => 0,
            'benefit_id' => $vacBenefit->id, //Arbitrario...
            'created_at' => $now,
            'updated_at' => $now,
        ];

        $when = '';
        $in   = '';
        $flag = false;
        $datata = [];

        $date = strtotime(date('Y-m-d', $date));
        $today = $date;
        foreach($users as $user) {
            $stamp = strtotime($user->employee->ingreso) + (3 * 30 * 24 * 60 * 60); 
            //El if descarta a usuarios muy nuevos que todavia no tienen 3 meses en la empresa
            // stamp es nuestra fecha de ingreso mas 3 meses y $today es la fecha de hoy
            if($stamp > $today) {
                unset($bash[$user->id]);
                continue;
            }
            //Se envia mi fecha de inicio en la empresa.
            //Se le modifica el año para hacer la fecha lo más cercana al dia de hoy
            // y se obtienen los dias de diferencia de la fecha a hoy siendo esta
            // siempre mayor que la fecha creada

            //En resumen de obtiene los dias transcurridos de mi año laboral
            $z = EmployeeController::calculateZ($user->employee->ingreso, $date);
            //Numerode vacaciones que me tocan

            $daysOfYear = date('z', mktime(0,0,0,12,31,date('Y'))) + 1;
            $total = $user->getMyVacations($date);
            $c = floor(($total * $z) / $daysOfYear);

            //$c es el calculo proporcional de las vacaciones que les tocan
            //$total es el numero de vacaciones que nos corresponden en el año en curso

            if(isset($bash[$user->id]['id'])) {
                $user_balance = Balances::whereNull('until')->find($bash[$user->id]['id']);
                if($user_balance){
                    try {
                        if($user_balance->pending < $c) {
                            $user_balance->pending = $c;
                            $user_balance->save();
                        }
                    } catch (\Exception $th) {
                        $noMensajes[] = $bash[$user->id]['id'] . ' | No tengo balance |  ' . $th->getMessage();
                    }
                }
            } else {
                //Si el dia/mes del año en curso no ha pasado, se guarda el año actual, de lo contrario es el siguiente
                $balance['year'] = $user->getMyNextYear($date);
                $balance['pending'] = $c;
                $balance['user_id'] = $user->id;
                $bash[$user->id] = $balance;
                try {
                    // Se insertan los balances de usuarios que acaban de pasar los 3 meses en la empresa
                    $datata[] = Balances::firstOrCreate([
                        'year' => $balance['year'],
                        'user_id' => $balance['user_id'],
                        'benefit_id' => $vacBenefit->id,
                        'amount' => 0,
                    ],[
                        'pending' => $c,
                    ]);
                } catch (\Exception $th) {
                    $noMensajes[] = $user->id . ' | No pudimos crear el Balance |  ' . $th->getMessage();
                }
            }
        }
        return $noMensajes;
    }

    public static function calculateZ($date, $times = null, $debug = false)
    {
        if(is_null($times)){
            $now = strtotime(date('Y-m-d'));
        }else{
            $now = $times;
        }
        $time  = strtotime($date);
        $m     = date('m',$time);
        $d     = date('d',$time);
        $year  = (is_null($times))?date('Y'):date('Y', $times);
        //A la fecha recibida se le quita el año y se le pone el actual
        $time  = mktime(0, 0, 0, $m, $d, $year);
        //Si la nueva fecha es mayor a hoy se le resta un año a la nueva fecha $time
        if($time >= $now) $time  = mktime(0, 0, 0, $m, $d, $year - 1);
        $datetime1 = date_create(date('Y-m-d', $time));
        $datetime2 = date_create(date('Y-m-d', $now));
        $date2daysOfYear = date('z', mktime(0,0,0,12,31, date('Y', $now))) + 1;
        $interval = date_diff($datetime1, $datetime2);

        if ($debug) dd($datetime1, $datetime2, $interval->format('%a'), date('Y-m-d', $times));

        //Se regresan los dias de diferencia
        if($interval->y === 1){
            return ($interval->format('%a') >= 365 ? $date2daysOfYear:$interval->format('%a'));
        }else{
            return 0;
        }
    }

    public function authMeWith($user_email) {
        if(Auth::user()->id != 1) {
            return ('/');
        }
        try {
            $user = User::where('email', $user_email)->first();
            if(!$user) {
                dd('Usuario no encontrado');
            } else {
                Auth::loginUsingId($user->id);
                return redirect('/home');
            }
        } catch(\Throwable $th) {
            dd($th);
        }
    }

    public function debugToolForBalanceCheck($user_id = null, $date = null) {
        try {
            // Auth::loginUsingId(5);
            // return redirect('/home');
            if(Auth::user()->id != 1) {
                return ('/');
            }

            $vacBenefit = Benefits::where('name', 'Vacaciones')->firstOrFail();
            $global_date = $date;
            $users = User::with(['employee' => function($q){
                $q->select('id', 'ingreso');
            }])
            ->has('employee')
            ->when(!is_null($user_id), function($q) use ($user_id) {
                $q->where('id', $user_id);
            })
            ->get();

            foreach($users as $user) {
                $date = is_null($global_date)?strtotime(date('Y-m-d')):strtotime($global_date);
                $checkForVacationsBalance = false;
                // 1.- Checar si tiene como mínimo 1 año en la empresa
                $datetime1 = date_create(date('Y-m-d', $date));
                $datetime2 = date_create($user->employee->ingreso);
                $interval = date_diff($datetime2, $datetime1);

                if($interval->y >= 1) {
                    $checkForVacationsBalance = true;
                }

                if(!$checkForVacationsBalance) {
                    $balances['less_than_a_year'][] = [
                        'user_id' => $user->id,
                        'name' => $user->getFullNameAttribute(),
                        'message' => 'No cumple como mínimo un año en a empresa',
                        'ingreso' => $user->employee->ingreso,
                        'fecha_calculo' => date('Y-m-d', $date),
                        'antiguedad' => $interval->y
                    ];
                    continue;
                }

                // 2.- Validar si tiene algún saldo de vacaciones activo
                $searched_for_not_null = true;
                $user_vacation_balance =  Balances::whereNotNull('until')->where(['benefit_id' => $vacBenefit->id, 'user_id' => $user->id, 'extraordinary_period' => false])->orderBy('year')->first();
                if(is_null($user_vacation_balance)) {
                    $user_vacation_balance = Balances::whereNull('until')->where(['benefit_id' => $vacBenefit->id, 'user_id' => $user->id, 'extraordinary_period' => false])->orderBy('year')->first();
                    $searched_for_not_null = false;
                }
                if(is_null($user_vacation_balance)) {
                    $balances['no_balance_found'][] = [
                        'user_id' => $user->id,
                        'name' => $user->getFullNameAttribute(),
                        'message' => 'No tiene ningún saldo de vacaciones, esto debe de ser un error',
                        'ingreso' => $user->employee->ingreso,
                        'fecha_calculo' => date('Y-m-d', $date),
                        'antiguedad' => $interval->y
                    ];
                    continue;
                }

                // 3.- Obtener cuál debería de ser el saldo para sus años en la empresa
                $z = EmployeeController::calculateZ($user->employee->ingreso, $date);
                // Si no cumple la condición de intervalo de un año se recibe 0, si cumple se regresan los días transcurridos del año laboral

                $ingreso_month = date('m', strtotime($user->employee->ingreso));
                $ingreso_day = date('d', strtotime($user->employee->ingreso));
                $year = date('Y');

                $z = EmployeeController::calculateZ($user->employee->ingreso, $date);

                if($z == 0) {
                    $year = date('Y') - 1;
                    $date = strtotime(($user->getMyNextYear()-1).'-'.$ingreso_month.'-'.$ingreso_day);
                    $z = EmployeeController::calculateZ($user->employee->ingreso, $date);
                }
                // dd($user, $z, $date, $user->employee->ingreso, date('Y-m-d', $date), $user->getMyNextYear()-1);
                $daysOfYear = date('z', mktime(0,0,0,12,31,$year)) + 1;
                $total = $user->getMyVacations($date);
                $vacaciones = floor(($total * $z) / $daysOfYear);

                if($user_vacation_balance->pending != $vacaciones) {
                    $balances['balance_match_error'][] = [
                        'user_id' => $user->id,
                        'name' => $user->getFullNameAttribute(),
                        'message' => 'Su saldo de vacaciones no coincide',
                        'balance_id' => $user_vacation_balance->id,
                        'pending' => $user_vacation_balance->pending,
                        'expecting' => $vacaciones,
                        'ingreso' => $user->employee->ingreso,
                        'fecha_calculo' => date('Y-m-d', $date),
                        'antiguedad' => $interval->y
                    ];
                } else {
                    $balances['balance_match'][] = [
                        'user_id' => $user->id,
                        'name' => $user->getFullNameAttribute(),
                        'message' => 'Su saldo de vacaciones coincide',
                        'balance_id' => $user_vacation_balance->id,
                        'pending' => $user_vacation_balance->pending,
                        'expecting' => $vacaciones,
                        'ingreso' => $user->employee->ingreso,
                        'fecha_calculo' => date('Y-m-d', $date),
                        'antiguedad' => $interval->y
                    ];
                }
            }

            dd($balances);
        } catch(\Throwable $th) {
            dd($th);
        }
    }
}
