<?php

namespace App\Http\Controllers\Denuncias;

use App\User;
use Illuminate\Http\Request;
use App\Models\Denuncias\File;
use App\Models\Announcement\View;
use App\Models\Denuncias\Bitacora;
use Illuminate\Support\Facades\DB;
use App\Models\Denuncias\Denuncias;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Announcement\Announcement;

class DenunciasController extends Controller
{
    public function __construct()
    {
        $this->path = getcwd() . '/uploads';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(Auth::check()) {
            $view = 'layouts.app';

            $url = $_SERVER['REQUEST_URI'];
            $url = substr($url, 1);
            $vista = View::where('name', $url)->first();
            // dd($vista);
            $banner = Announcement::getAnnouncementsToDisplay($vista->id, 'banner');
            $columna_anuncios = Announcement::getAnnouncementsToDisplay($vista->id, 'columna_anuncios');
            $display_announcements = compact('banner');

        } else {
            $view = 'layouts.app_ext';

            $vista = View::where('name', 'denuncias_index')->first();
            // dd($vista);
            $banner = Announcement::getAnnouncementsToDisplay($vista->id, 'banner');
            $columna_anuncios = Announcement::getAnnouncementsToDisplay($vista->id, 'columna_anuncios');
            $display_announcements = compact('banner');
        }
        
        
        return view('denuncias.denuncia', compact('view', 'display_announcements', 'columna_anuncios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function images(Request $request) {
        try {
            DB::beginTransaction();
                $path = $this->path.'/denuncias';
                $file = $request->file('file');
                // $name = uniqid() . '_' . trim($file->getClientOriginalName());
                $nameFile = trim($file->getClientOriginalName());
                $lastDot = strrpos($nameFile, ".");
                $fileName = str_replace(".", "_", substr($nameFile, 0, $lastDot)) . substr($nameFile, $lastDot);
                $name = uniqid() . '_' . $fileName;
                $file->move($path, $name);

                return response()->json([
                    'name'          => $name,
                    'original_name' => $file->getClientOriginalName(),
                ]);
            DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['estatus'] = 'sin_procesar';
        $data['empresa_hechos'] = 'UCIN';
        $data['tipo_usuario'] = 'Empleado';
        $data['lugar_hechos'] = 'En la Empresa';
        // dd($data);
        try {
            DB::beginTransaction();
                $denuncia = Denuncias::create($data);

                if(isset($data['file_images'])) {
                    foreach($data['file_images'] as $key => $file) {                        
                        $fileArray = explode(".", $file);
                        $filename = $fileArray[0].'.'.$fileArray[1];
                        $extension = $fileArray[1];
                        $nameArray = explode("_", $filename);
                        $name = $nameArray[1];

                        $file_data = new File();
                        $file_data->denuncia_id = $denuncia->id;
                        $file_data->type = 'image';
                        $file_data->filename = $filename;
                        $file_data->name = $name;
                        $file_data->extension = $extension;
                        $denuncia->files()->save($file_data);
                    }
                }
                if(isset($data['file_documents'])) {
                    $bool = $this->validateFileExtension($data, ['pdf'], 'file_documents');
                    if(!$bool) {
                        flash::error('Tipo de documento no válido...');
                        return redirect()->back();
                    }
                    foreach($data['file_documents'] as $key => $file) {
                        $pop = strlen($file->getClientOriginalExtension()) + 1;
                        $name = substr($file->getClientOriginalName(), 0, - $pop);
                        $extension = $file->getClientOriginalExtension();
                        $filename = uniqid() . '-' .$file->getClientOriginalName();

                        $file_data = new File();
                        $file_data->denuncia_id = $denuncia->id;
                        $file_data->type = 'document';
                        $file_data->filename = $filename;
                        $file_data->name = $name;
                        $file_data->extension = $extension;
                        $denuncia->files()->save($file_data);
                        $path = $this->path.'/denuncias';

                        $file->move($path, $filename);
                    }
                }

                $mail_data = [
                    'to_email' => $data['email_externo'],
                ];

                Mail::send('emails.denuncia.mail', $mail_data, function ($message) use($mail_data) {
                    $message->from('noreply@ucinmedica.com', 'No reply');
                    $message->to('contaco@ucin.com');
                    if (!empty($mail_data['to_email']))
                        $message->to($mail_data['to_email']);
                    $message->subject('Nueva Mejora reportada');
                });

            DB::commit();
            return view('denuncias.respuesta', compact('denuncia'));
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return redirect()->back()->with('alert-danger', 'No se pudo registrar tu evento, revisa tu información o intenta mas tarde.')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        try {
            DB::beginTransaction();
                $denuncia = Denuncias::findOrFail($id)->update($data);
            DB::commit();
            return $this->adminIndex();
        } catch (\Throwable $th) {
            throw $th;
            DB::rollback();
            return redirect()->back()->with('alert-danger', 'Algo salió mal intenta mas tarde.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function adminIndex() {
        $denuncias = Denuncias::get();

        foreach($denuncias as $denuncia) {
            switch ($denuncia->estatus) {
                case 'sin_procesar':
                    $estado[] = 'danger';
                    $txt_estado[] = 'Sin Procesar';
                    break;
                case 'en_proceso':
                    $estado[] = 'primary';
                    $txt_estado[] = 'En Proceso';
                    break;
                case 'procesada':
                    $estado[] = 'success';
                    $txt_estado[] = 'Procesada';
                    break;
                case 'cerrada':
                    $estado[] = 'warning';
                    $txt_estado[] = 'Cerrada';
                    break;
                default:
                    $estado[] = 'danger';
                    $txt_estado[] = 'Sin Procesar';
                    break;
            }
            switch ($denuncia->importancia) {
                case 'no_asignado':
                    $importancia[] = 'dark';
                    $txt_importancia[] = 'No Asignado';
                    break;
                case 'critico':
                    $importancia[] = 'danger';
                    $txt_importancia[] = 'Crítico';
                    break;
                case 'moderado':
                    $importancia[] = 'warning';
                    $txt_importancia[] = 'Moderado';
                    break;
                case 'bajo':
                    $importancia[] = 'primary';
                    $txt_importancia[] = 'Bajo';
                    break;
                default:
                    $importancia[] = 'dark';
                    $txt_importancia[] = 'No Asignado';
                    break;
            }
        }

        $view = View::where('name', 'denuncias_index')->first();
        $banner = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
        $columna_anuncios = Announcement::getAnnouncementsToDisplay($view->id, 'columna_anuncios');
        $display_announcements = compact('banner');
        
        return view('denuncias.adminIndex', compact('denuncias', 'estado', 'txt_estado', 'importancia', 'txt_importancia', 'display_announcements', 'columna_anuncios'));
    }

    public function gestionar($id) {
        $denuncia = Denuncias::findOrfail($id);
        $users = User::orderBy('first_name', 'asc')->get();

        return view('denuncias.gestion', compact('denuncia', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bitacora(Request $request) {
        $data = $request->all();
        
        $data['denuncia_id'] = $data['id'];
        $data['fecha_hora'] = date('Y-m-d H:i:s');
        $data['user_id'] = auth()->user()->id;
        $user = User::find(auth()->user()->id);
        $user = $user->FullName;

        if(isset($data['tipo'])) {
            $data['comentarios'] = $data['fecha_hora'].' Cambio de clasificación a: ' .$data['accion']. ' Realizado por: '.$user;
        }

        if(isset($data['importancia'])) {
            $data['comentarios'] = $data['fecha_hora'].' Cambio de Importancia a: ' .$data['accion']. ' Realizado por: '.$user;
        } elseif(isset($data['responsable'])) {
            $user_resp = User::find($data['responsable_id']);
            $user_resp = $user_resp->FullName;
            $data['comentarios'] = $data['fecha_hora'].' Cambio de Responsable a: ' .$user_resp. ' Realizado por: '.$user;
        } else {
            $data['comentarios'] = $data['fecha_hora'].' Cambio de Estatus a: ' .$data['accion']. ' Realizado por: '.$user;
        }

        // dd($data);

        try {
            DB::beginTransaction();
                $insert = Bitacora::create($data);
            DB::commit();
            $bitacora = Bitacora::findOrFail($insert->id);
            // return redirect()->back();
            return response()->json($bitacora);
        } catch (\Throwable $th) {
            // throw $th;
            return response()->json(['error' => 'Error msg'], 404);
            DB::rollback();
        }
        
    }

    private function validateFileExtension($data, $array, $name) {
        $bool = true;
        $allowedfileExtension = $array;
        foreach($data[$name] as $key => $file) {
            $extension = $file->getClientOriginalExtension();
            $extension = strtolower($extension);
            if(!in_array($extension, $allowedfileExtension)) {
                $bool = false;
            }
        }
        return $bool;
    }
}
