<?php

if (!function_exists('is_url_of_hidden_menu')) {
 
    /**
     * Checks if a value exists in an array
     *
     * @param string $URL format by laravel
     * The searched value
     *
     * @return bool true if is a key in the array
     * false otherwise
     */
    function is_url_of_hidden_menu($url){
        $HIDDEN_MENU = [
            'admin.clocklog.history' => true,
            'admin.incidents.history' => true,
            'admin.exports.index' => true,
            'super.events.index' => true,
            'super.events.create' => true,
            'super.events.edit' => true,
            'admin.incidents.index' => true,
            'admin.incidents.create' => true,
            'admin.incidents.edit' => true,
            'admin.schedule.index' => true,
            'admin.schedule.create' => true,
            'admin.schedule.edit' => true,
            'admin.benefitjobs.index' => true,
            'admin.benefitjobs.create' => true,
            'admin.benefitjobs.edit' => true,
            'admin.incidents.balances' => true,
            'admin.incidents.balances.create' => true,
        ];

        return isset($HIDDEN_MENU[$url]);
    }
}