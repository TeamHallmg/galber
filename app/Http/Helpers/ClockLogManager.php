<?php

namespace App\Http\Helpers;

use App\User;
use Exception;
use Carbon\Carbon;
use App\Models\WorkshiftFixed;
use App\Models\Vacations\Event;
use App\Models\Vacations\Benefits;
use App\Models\Vacations\ClockLog;
use App\Models\Vacations\Incident;
use App\Events\ClockLogFailCheckIntent;

class ClockLogManager{

    protected $today;
    protected $tolerance;

    public function __construct(){
        $this->today = date('Y-m-d');
        $this->tolerance = 10;
    }

    public function getArriveData($time, $day, $region){
        $shift = WorkshiftFixed::with('schedule')->where('day', $day)
        ->where('region_id', $region)
        ->has('schedule')
        ->first();

        $schedule = $shift->schedule;
        $closest_data = $this->getClosestTimeDataTo($time, ['in' => $schedule->in, 'out' => $schedule->out]);
        $status = $this->getArrivalStatus($closest_data['diff'], $closest_data['type']);
        return ['compare_time' => $closest_data['time'], 'arrive_status' => $status, 'type' => $closest_data['type']];
    }

    public function getClosestTimeDataTo($to, $times){
        $closest = null;
        $closest_diff = 999999;
        $in_out = "";
        $difss = [];
        $bigger = false;
        foreach($times as $type => $time){
            $start = new Carbon($time);
            $end = new Carbon($to);
            $diff = $start->diffInMinutes($end, false);
            $difss[] = $diff;
            if(abs($diff) < $closest_diff){
                $closest_diff = $diff;
                $closest = $time;
                $in_out = $type;
                if($type === 'in'){
                    $bigger = ($diff > 0)?false:true;
                }else{
                    $bigger = ($diff < 0)?false:true;
                }
            }
        }
        return ['type' => $in_out, 'diff' => $closest_diff, 'time' => $closest];
    }

    public function getArrivalStatus($diff, $type){
        if($type === 'in'){
            if($diff === 0){
                return 'on-time';
            }elseif($diff < 0){
                return 'early';
            }elseif($diff <= $this->tolerance){
                return 'tolerance';
            }elseif($diff > $this->tolerance){
                return 'late';
            }else{
                return '';
            }
        }elseif ($type === 'out'){
            if($diff === 0){
                return 'on-time';
            }elseif($diff < 0){
                return 'early';
            }elseif($diff > 0){
                return 'ok';
            }else{
                return '';
            }
        }
    }

    public function generateAbsentsRecords($employees = [], $maxDaysPassed = 2){
        $date = Carbon::now()->subDays($maxDaysPassed)->format('Y-m-d 00:00:00');
        $logs = ClockLog::with('user', 'comments')
        ->when(count($employees), function($q) use($employees){
            $q->whereIn('user_id', $employees);
        })
        ->where('date', '>=', $date)
        ->orderBy('date', 'DESC')
        ->get();

        $users = User::with('employee')->has('employee.region')->whereIn('id', $employees)->get()->groupBy('id');

        $workshifts = WorkshiftFixed::with('schedule')->has('schedule')->get();

        $shifts = [];
        foreach($workshifts as $workshift){
            if($workshift->labor){
                $shifts[$workshift->region_id][$workshift->day]['in'] = $workshift->schedule->in;
                $shifts[$workshift->region_id][$workshift->day]['out'] = $workshift->schedule->out;
            }
        }
        
        $days = [];
        foreach($logs as $record){
            $days[$record->user_id][$this->dateToYmd($record->date)][$record->type] = $record;
        }

        $date = strtotime($date);
        $newRecords = [];
        for ($i = 0; $i < $maxDaysPassed + 1; $i++) {
            foreach($employees as $employee){
                if(isset($days[$employee]) && isset($days[$employee][date('Y-m-d', $date)])){
                    if(!isset($days[$employee][date('Y-m-d', $date)]['in'])){
                        $newRecords[] = new ClockLog([
                            'date' => date('Y-m-d', $date) . ' ' . $shifts[$users[$employee][0]->employee->region_id][date('D', $date)]['in'],
                            'type' => 'in',
                            'status' => 'missing',
                            'from' => 'system',
                            'user_id' => $employee,
                            'compare_time' => $shifts[$users[$employee][0]->employee->region_id][date('D', $date)]['in'],
                        ]);
                    }
                    if(!isset($days[$employee][date('Y-m-d', $date)]['out'])){
                        $newRecords[] = new ClockLog([
                            'date' => date('Y-m-d', $date) . ' ' . $shifts[$users[$employee][0]->employee->region_id][date('D', $date)]['out'],
                            'type' => 'out',
                            'status' => 'missing',
                            'from' => 'system',
                            'user_id' => $employee,
                            'compare_time' => $shifts[$users[$employee][0]->employee->region_id][date('D', $date)]['out'],
                        ]);
                    }
                }else{
                    if(isset($users[$employee])){
                        if(!isset($days[$employee][date('Y-m-d', $date)]['in'])){
                            $newRecords[] = new ClockLog([
                                'date' => date('Y-m-d', $date) . ' ' . $shifts[$users[$employee][0]->employee->region_id][date('D', $date)]['in'],
                                'type' => 'in',
                                'status' => 'missing',
                                'from' => 'system',
                                'user_id' => $employee,
                                'compare_time' => $shifts[$users[$employee][0]->employee->region_id][date('D', $date)]['in'],
                            ]);
                        }
                        if(!isset($days[$employee][date('Y-m-d', $date)]['out'])){
                            $newRecords[] = new ClockLog([
                                'date' => date('Y-m-d', $date) . ' ' . $shifts[$users[$employee][0]->employee->region_id][date('D', $date)]['out'],
                                'type' => 'out',
                                'status' => 'missing',
                                'from' => 'system',
                                'user_id' => $employee,
                                'compare_time' => $shifts[$users[$employee][0]->employee->region_id][date('D', $date)]['out'],
                            ]);
                        }
                    }
                }
            }
            $date += 24 * 60 * 60;
        }
        return $newRecords;
    }
    
    /**
     * Adds the conditions to a query to filter all the active incidents in the date range
     *
     * @param  \Illuminate\Database\Schema\Builder $query
     * @param  string $start Y-m-d
     * @param  string $end Y-m-d
     * @param  \App\User $user (optional)
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function incidentQuery($query, $start, $end, $user = null){
        return $query->whereIn('status', ['accepted', 'processed', 'complete'])
        ->whereHas('benefit', function($q){
            $q->whereIn('context', ['user', 'super'])
            ->whereIn('type', ['pending', 'day']);
        })
        ->whereHas('event', function($query) use ($start, $end, $user){
            $query->when($user, function($q) use($user){
                $q->where('user_id', $user->id);
            })
            ->whereHas('eventDays', function($q) use($start, $end){
                $q->where('date', '>=', $start)
                ->where('date', '<=', $end)
                ->blockDays();
            });
            /*->when(!!$user, function($qwhen) use($user){
                $qwhen->where(function ($query) use ($user) {
                    $query->where('js', 'like', '%"' . $user->id . '"%')
                    ->orWhere([['region_id', $user->employee->region_id], ['js', null]]);
                });
            });*/
        });
    }
    
    /**
     * Adds the conditions to a query to filter all the active incidents working with week_calendar (Semana comprimmida)
     * in the given date range
     *
     * @param  \Illuminate\Database\Schema\Builder $query
     * @param  string $start Y-m-d
     * @param  string $end Y-m-d
     * @param  \App\User $user (optional)
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function incidentWeekCalendarQuery($query, $start, $end, $user = null){
        return $query->with([
            'benefit', 
            'event' => function($q) use ($start, $end, $user){
                $q->when($user, function($q1) use($user){
                    $q1->where('user_id', $user->id);
                })->with([
                    'eventDays' => function($q1) use($start, $end){
                        $q1->where('date', '>=', $start)
                        ->where('date', '<=', $end)
                        ->nonBlockDays();
                    },
                    'eventDay' => function($q1) use($start, $end){
                        $q1->where('date', '>=', $start)
                        ->where('date', '<=', $end)
                        ->blockDays();
                    }
                ]);
            }
        ])
        ->whereIn('status', ['accepted', 'processed', 'complete'])
        ->whereHas('benefit', function($q){
            $q->whereIn('context', ['user', 'super'])
            ->whereIn('type', ['pending', 'day'])
            ->where('week_calendar', 1);
        })
        ->whereHas('event', function($query) use ($start, $end, $user){
            $query->when($user, function($q) use($user){
                $q->where('user_id', $user->id);
            })
            ->whereHas('eventDays', function($q) use($start, $end){
                $q->where('date', '>=', $start)
                ->where('date', '<=', $end);
            });
        });
    }

    public function isDayAnDayIncident($user, $date, $boolOrElement = true){
        $start = $end = $this->dateToYmd($date);
        $blocked = Incident::where('from_id', $user->id);
        $blocked = $this->incidentQuery($blocked, $start, $end, $user);
        return $boolOrElement?$blocked->exists():$blocked->first();
    }

    public function eventQuery($query, $start, $end, $user = null){
        return $query->where('type', 'global')
        ->where('blocked', '1')
        ->where(function ($query) use ($start, $end) {
            $query->whereBetween('start', [strtotime($start), strtotime($end)])
                ->orWhereBetween('end', [strtotime($start), strtotime($end)])
                ->orWhere('start', strtotime($start))
                ->orWhere(function ($query2) use ($start, $end) {
                    $query2->where('start', '<', strtotime($start))
                        ->where('end', '>', strtotime($end));
                });
        })
        ->when(!!$user, function($qwhen) use($user){
            $qwhen->where(function ($query) use ($user) {
                $query->where('js', 'like', '%"' . $user->id . '"%')
                ->orWhere([['region_id', $user->employee->region_id], ['js', null]]);
            });
        });
    }
    
    /**
     * Query base para obtener las incidencias de tiempo realizadas en el rango de fechas indicado
     *
     * @param  string $start in Y-m-d format
     * @param  string $end in Y-m-d format
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function timeEventsQuery($start, $end){
        return Event::with('benefit')
        ->whereHas('benefit', function($q){
            $q->where('type', 'time');
        })
        ->whereIn('status', ['accepted', 'processing', 'processed'])
        ->where('start', '>=', strtotime($start))
        ->where('end', '<=', strtotime($end));
    }

    public function isDayBlockByEvent($user, $date, $boolOrElement = true){
        $start = $end = $this->dateToYmd($date);
        $blocked = Event::query();
        $blocked = $this->eventQuery($blocked, $start, $end, $user);
        return $boolOrElement?$blocked->exists():$blocked->first();
    }

    public function checkExistingLog($user, $date, $type = null){
        $log = ClockLog::where('user_id', $user->id)
        ->whereDate('date', $date)
        ->when($type, function($q) use($type) {
            $q->where('type', $type);
        })
        ->exists();
        
        return $log;
    }

    public function getWorkedMinutesInDay($user, $date){
        $clocklog = ClockLog::where('user_id', $user->id)
        ->whereDate('date', $date)
        ->get()->groupBy('type');

        if(count($clocklog) < 2)
            throw new Exception("Missing clock logs  |  " . $date);

        return $this->getTimeDifference($clocklog['in'][0]->date, $clocklog['out'][0]->date);
    }

    public function dateToYmd($date){
        return date('Y-m-d', strtotime($date));
    }

    public function getAbsentBenefit(){
        $benefit = Benefits::where('name', 'Falta No Justificada')
        ->where('group', 'incident')
        ->first();
        return $benefit;
    }

    public function getCompressedWeekBenefit(){
        $benefit = Benefits::where('name', 'Trabajo comprimido')
        ->where('group', 'benefit')
        ->first();
        return $benefit;
    }

    public function getMissingLogsType($date, $user){
        $logs = Clocklog::select('type')
        ->whereDate('date', $date)
        ->where('user_id', $user)
        ->groupBy('type')
        ->get()->pluck('type')->toArray();
        if(count($logs) === 0){
            return ['in', 'out'];
        }elseif(count($logs) >= 2){
            return [];
        }else{
            $type = [];
            foreach($logs as $log){
                $type[$log] = $log;
            }
            return isset($type['in'])?['out']:['in'];
        }
    }

    public function sendFailMail($user, $date, $checkDay, $checkIncident, $checkEvent){
        if( !config('clocklog.send_fail_clocklog_mail') )
            return '. Si tienes dudas contacta a tu encargado';

        $problem = '';
        if($checkDay){
            $problem = 'no-labor';
        }elseif($checkIncident){
            $problem = 'incident-blocked';
        }elseif($checkEvent){
            $problem = 'event-blocked';
        }
        try {
            event(new ClockLogFailCheckIntent($user, $problem, $date));
        } catch (\Throwable $th) {
            return '. Si tienes dudas contacta a tu encargado';
        }
        return '';
    }


    public function getTimeDifference($start, $end){
        $start = new Carbon($start);
        $end = new Carbon($end);
        $start_startOfDay = new Carbon($start); $start_startOfDay = $start_startOfDay->startOfDay();
        $end_startOfDay = new Carbon($end); $end_startOfDay = $end_startOfDay->startOfDay();
        $start->second = 0;
        $end->second = 0;
        $diffDays = $start_startOfDay->startOfDay()->diffInDays($end_startOfDay->startOfDay());
        $end = $diffDays > 0?$end->subDays($diffDays):$end->addDays($diffDays);
        return $start->diffInMinutes($end, false);
    }
}