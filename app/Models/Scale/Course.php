<?php

namespace App\Models\Scale;

use App\Models\JobPosition;
use App\Models\DeletedReason;
use App\Models\Scale\ModelData;
use App\Models\Scale\CourseData;
use App\Models\Scale\CourseJobposition;
use App\Models\Moodle\Course as MCourse;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use SoftDeletes;
    
    protected $table = 'scale_courses';

    protected $fillable = ['category_id', 'category', 'moodle_id', 'name', 'code', 'type', 'modality', 'min_grade', 'priority', 'institution', 'instructor_name', 'observations', 'validity', 'duration', 'expire_at'];

    public function scopeMoodleOnly($query) {
        return $query->whereNotNull('moodle_id');
    }

    public function scopeNonMoodleOnly($query) {
        return $query->whereNull('moodle_id');
    }

    public function scopeInternOnly($query) {
        return $query->where('type', 'I');
    }

    public function scopeExternOnly($query) {
        return $query->where('type', 'E');
    }

    public function deleted_reason() {
        return $this->morphMany(DeletedReason::class, 'deleted_reason');
    }

    public function m_course()
    {
        return $this->belongsTo(MCourse::class, 'moodle_id', 'id');
    }

    public function getCourseType() {
        return $this->type == 'I'?'Interno':'Externo';
    }
    
    public function isMoodleCourse() {
        return $this->moodle_id?true:false;
    }

    public function getFrom() {
        return is_null($this->moodle_id)?'Nuevo':'Universidad Ucin';
    }

    public function getCategory() {
        return $this->category ?? 'N/A';
    }

    public function job_positions() {
        return $this->hasManyThrough(
            JobPosition::class,
            ModelData::class,
            'scale_data_id', // Foreign key on users table...
            'id', // Foreign key on posts table...
            'id', // Local key on countries table...
            'model_id' // Local key on users table...
        )->where('scale_data_type', 'course')->withTrashed();
    }

    public function scale_model_data($model)
    {
        // return $this->belongsToMany('App\Models\JobPosition', 'scale_jobposition', 'scale_course_id', 'job_position_id', 'calificacion');
        // return $this->belongsToMany('App\Models\JobPosition', 'scale_jobposition', 'scale_course_id')->withPivot('calificacion');
        return $this->hasMany(ModelData::class, 'scale_data_id', 'id')->where(['model_name' => $model, 'scale_data_type' => 'course']);
    }

    public function jobPositionData() {
        return $this->hasMany(ModelData::class, 'scale_data_id', 'id')->where(['model_name' => 'JobPosition', 'scale_data_type' => 'course']);
    }

    public function getJobPositionIDs() {
        $ids = [];
        foreach($this->job_positions as $row) {
            $ids[] = $row->id;
        }
        return $ids;
    }

    public function getJobNames() {
        $names = [];
        foreach($this->jobPositionData as $row) {
            $names[] = $row->getJobName();
        }
        return $names;
    }

    public function data()
    {
        return $this->hasMany(CourseData::class, 'scale_course_id', 'id');
    }

    public function getData($type = null) {
        return $this->data->where('type', $type);
    }

    public function getArrayData($type = null) {
        $column = $this->getColumn($type);
        return $this->getData($type)->pluck($column)->toArray();
    }

    public function getColumn($type = null) {
        if($type == 'competence' || $type == 'trainer') {
            return 'type_id';
        } else {
            return 'data';
        }
    }

    public function getModalityName() {
        switch($this->modality) {
            case 'face-to-face':
                return 'Presencial';
            break;
            case 'virtual':
                return 'E-Learning';
            break;
            case 'blended':
                return 'Blended Learning';
            break;
            default:
                return 'Desconocido';
            break;
        }
    }

    public function getTrainerData() {
        $trainer = $this->getData('extern_rfc');
        if(!$trainer->isEmpty()) {
            // Sólo busco el primer dato encontrado
            foreach($trainer as $row) {
                return [
                    'hasTrainer' => true,
                    'rfc' => $row->data,
                    'name' => $row->extra3
                ];
            }
        } else {
            return [
                'hasTrainer' => false,
                'rfc' => null,
                'name' => null
            ];
        }
    }
}
