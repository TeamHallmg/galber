<?php

namespace App\Models\Scale;

use App\Models\DeletedReason;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Virtue extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'scale_virtues';

    public function deleted_reason() {
        return $this->morphMany(DeletedReason::class, 'deleted_reason');
    }
}
