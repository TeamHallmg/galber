<?php

namespace App\Models\Scale;

use App\Models\DeletedReason;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Level extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'scale_levels';

    public function courses()
    {
        return $this->hasMany(LevelCourse::class, 'level_id', 'id')->where('course_id', '!=', null);
    }

    public function moodle_courses()
    {
        return $this->hasMany(LevelCourse::class, 'level_id', 'id')->where('moodle_id', '!=', null);
    }

    public function deleted_reason() {
        return $this->morphMany(DeletedReason::class, 'deleted_reason');
    }
}