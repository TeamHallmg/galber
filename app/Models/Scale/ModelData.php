<?php

namespace App\Models\Scale;

use App\Models\JobPosition;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModelData extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'scale_model_data';

    protected $fillable = ['model_id', 'model_name', 'scale_data_type', 'scale_data_id', 'score', 'priority', 'date_start', 'extra1', 'extra2', 'extra3', 'extra4'];

    // =======  Registros creados en la matriculación de un usuario  =======
    // IF:
    // 'model_name' => 'User',
    // 'scale_data_type' => 'moodle_course',
    // THEN:
    // extra1 => 'Id de Plan, que es registro del modelo Plan'
    // extra2 => 'Precio del curso de esta persona'
    // extra3 => 'Id del Programa, que es registro del modelo Plan'
    // =======  /Registros creados en la matriculación de un usuario  =======

    public function course() {
        return $this->belongsTo(Course::class, 'scale_data_id', 'id')->withTrashed();
    }

    public function job_position()
    {
        return $this->belongsTo(JobPosition::class, 'model_id', 'id');
    }

    public function getJobName() {
        return $this->job_position->name ?? null;
    }

    // public function virtue() {
    //     return $this->belongsTo(Virtue::class, 'scale_data_id', 'id');
    // }
    
}
