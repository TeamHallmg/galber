<?php

namespace App\Models\Scale;

use App\Models\JobPosition;
use Illuminate\Database\Eloquent\Model;

class CourseJobposition extends Model
{
    public $timestamps = false;

    protected $table = 'scale_jobposition';

    protected $fillable = ['scale_course_id', 'job_position_id', 'calificacion', 'prioridad'];

    public function course()
    {
        return $this->belongsTo(Course::class, 'scale_course_id', 'id');
    }

    public function job_position()
    {
        return $this->belongsTo(JobPosition::class, 'job_position_id', 'id');
    }

    public function getJobIDs() {
        return $this->job_position_id;
    }

    public function getJobName() {
        return $this->job_position->name ?? null;
    }

}
