<?php

namespace App\Models\Scale;

use Illuminate\Database\Eloquent\Model;

class CourseData extends Model
{
    protected $table = 'scale_course_data';

    protected $fillable = ['scale_course_id', 'type', 'type_id', 'data', 'extra1', 'extra2', 'extra3', 'extra4'];
}
