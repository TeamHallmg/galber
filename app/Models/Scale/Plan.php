<?php

namespace App\Models\Scale;

use App\Models\DeletedReason;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends Model
{
    use SoftDeletes;

    protected $table = 'scale_plans';

    protected $fillable = ['type', 'name', 'description', 'status', 'start_date', 'end_date', 'budget'];

    /**
	 * Scope a query to only include training plans
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeTrainingPlans($query)
	{
		return $query->where('type', 'training');
    }
    
    /**
	 * Scope a query to only include training programs
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder $query
	 * @return \Illuminate\Database\Eloquent\Builder
     */
     
	public function scopeTrainingPrograms($query)
	{
		return $query->where('type', 'program');
	}

    public function deleted_reason() {
        return $this->morphMany(DeletedReason::class, 'deleted_reason');
    }

    public function getType() {
        switch($this->type) {
            case 'training':
                return 'Plan Capacitación';
            break;
            case 'program':
                return 'Programa Capacitación';
            break;
            default:
                return 'N/A';
            break;
        }
    }

    public function getStatus() {
        switch($this->status) {
            case 'open':
                return 'Abierto';
            break;
            case 'closed':
                return 'Cerrado';
            break;
            case 'canceled':
                return 'Cancelado';
            break;
            default:
                return 'N/A';
            break;
        }
    }

    public function getFormattedDate($column) {
        return date('d/m/Y', strtotime($this->$column));
    }

    public function getFormattedBudget() {
        return $this->budget?('$ '. number_format($this->budget, 2)):'N/A';
    }

    public function courseData() {
        return $this->hasMany(ModelData::class, 'scale_data_id', 'id')->where(['model_name' => 'Course', 'scale_data_type' => 'plan']);
    }

    public function courses() {
        return $this->hasManyThrough(
            Course::class,
            ModelData::class,
            'scale_data_id', // Foreign key on users table...
            'id', // Foreign key on posts table...
            'id', // Local key on countries table...
            'model_id' // Local key on users table...
        )->withTrashed();
    }

    public function moodle_courses() {
        return $this->hasManyThrough(
            Course::class,
            ModelData::class,
            'scale_data_id', // Foreign key on users table...
            'id', // Foreign key on posts table...
            'id', // Local key on countries table...
            'model_id' // Local key on users table...
        )->whereNotNull('moodle_id')->withTrashed();
    }

    public function getMoodleCoursesIDs() {
        // Return Course ID not Moodle Course ID
        $ids = [];
        foreach($this->moodle_courses as $row) {
            $ids[] = $row->id;
        }
        return $ids;
    }

    public function getCoursesIDs() {
        $ids = [];
        foreach($this->courseData as $row) {
            $ids[] = $row->model_id;
        }
        return $ids;
    }
}
