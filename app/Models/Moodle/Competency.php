<?php

namespace App\Models\Moodle;

use Illuminate\Database\Eloquent\Model;

class Competency extends Model
{
	/**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'moodle_mysql';
	
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mdl_competency';
	
}