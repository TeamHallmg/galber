<?php

namespace App\Models\Moodle;

use Illuminate\Database\Eloquent\Model;

class RoleAssigment extends Model
{
    protected $connection = 'moodle_mysql';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mdl_role_assignments';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['roleid', 'contextid', 'userid', 'timemodified', 'modifierid'];

    public function context()
    {
        return $this->belongsTo(Context::class, 'contextid', 'id');
    }

    public function user() // Zapata
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = context_id, localKey = id)
        return $this->hasOne(User::class, 'id', 'userid');
    }

    public function users() // Zapata
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = context_id, localKey = id)
        return $this->hasMany(User::class, 'userid');
    }
    
}
