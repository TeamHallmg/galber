<?php

namespace App\Models\Moodle;

use Illuminate\Database\Eloquent\Model;

class Enrol extends Model
{
    protected $connection = 'moodle_mysql';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mdl_enrol';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    public function course()
    {
        return $this->belongsTo(Course::class, 'courseid', 'id');
    }
    
}
