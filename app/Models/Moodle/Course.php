<?php

namespace App\Models\Moodle;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
	protected $connection = 'moodle_mysql';
	
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mdl_course';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['fullname', 'shortname','category'];

    /**
     * Scope a query to only include visible courses
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
     public function scopeVisible($query, $bool = true)
    {
        return $query->where('visible', $bool);
    }

    /**
     * Scope a query to only include visible courses
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
     public function scopeNonVisible($query)
    {
        return $query->where('visible', 0);
    }

    /**
     * Course belongs to Category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function m_category()
    {
        // belongsTo(RelatedModel, foreignKey = category_id, keyOnRelatedModel = id)
        return $this->belongsTo(CourseCategory::class, 'category');
    }

    /**
     * CourseCategory has many Contexts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contexts()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = courseCategory_id, localKey = id)
        return $this->hasMany(Context::class, 'instanceid');
    }

    /**
     * Course has one Context.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function context($contextlevel = null)
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = course_id, localKey = id)
        $q = $this->hasOne(Context::class, 'instanceid');
        if(!is_null($contextlevel)){
            $q->where('contextlevel', $contextlevel);
            return $q->first();
        }
        return $q;
    }

    public function getCategoryName(){
        return $this->m_category?$this->m_category->name:'';
    }

}
