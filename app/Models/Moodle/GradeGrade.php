<?php

namespace App\Models\Moodle;

use Illuminate\Database\Eloquent\Model;

class GradeGrade extends Model
{
	/**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'moodle_mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'grade_grades';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    public function grade_items() // Zapata
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = context_id, localKey = id)
        return $this->hasOne(GradeItem::class, 'itemid');
    }
	
}