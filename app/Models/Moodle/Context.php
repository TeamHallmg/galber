<?php

namespace App\Models\Moodle;

use Illuminate\Database\Eloquent\Model;

class Context extends Model
{
    protected $connection = 'moodle_mysql';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mdl_context';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['contextlevel', 'instanceid', 'path', 'depth'];

    /**
     * Context has many RoleAssigments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roleAssigments()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = context_id, localKey = id)
        return $this->hasMany(RoleAssigment::class, 'contextid');
    }

    public function instance()
    {
        return $this->belongsTo(Course::class, 'instanceid', 'id');
    }
}
