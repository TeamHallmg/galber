<?php

namespace App\Models\Moodle;

use Illuminate\Database\Eloquent\Model;

class CourseCategory extends Model
{
    protected $connection = 'moodle_mysql';
	
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mdl_course_categories';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['name','coursecount'];

    /**
     * CourseCategory has many Courses.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courses()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = courseCategory_id, localKey = id)
    	return $this->hasMany(Course::class,'category');
    }

    public function categories()
    {
        return $this->hasMany(CourseCategory::class, 'parent');
    }
    
}
