<?php

namespace App\Models\Moodle;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
 	protected $connection = 'moodle_mysql';

 	/**
 	 * The database table used by the model.
 	 *
 	 * @var string
 	 */
 	protected $table = 'mdl_user';

 	/**
 	 * Indicates if the model should be timestamped.
 	 *
 	 * @var boolean
 	 */
 	public $timestamps = false;

 	/**
 	 * Fields that can be mass assigned.
 	 *
 	 * @var array
 	 */
	protected $fillable = ['confirmed', 'mnethostid', 'suspended', 'username', 'password', 'idnumber', 'firstname', 'lastname', 'email', 'lang', 'department','icq','skype', 'yahoo', 'aim'];

	public function grade_grades() // Zapata
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = context_id, localKey = id)
        return $this->hasMany(User::class, 'userid');
    }
}
