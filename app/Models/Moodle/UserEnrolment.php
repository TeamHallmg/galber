<?php

namespace App\Models\Moodle;

use Illuminate\Database\Eloquent\Model;

class UserEnrolment extends Model
{
    protected $connection = 'moodle_mysql';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mdl_user_enrolments';

    const CREATED_AT = 'timecreated';
    const UPDATED_AT = 'timemodified';

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['enrolid', 'userid', 'timestart'];

    public function timestamp(){
        $this->timecreated = time();
        $this->timemodified = time();
    }

    public function enrol()
    {
        return $this->belongsTo(Enrol::class, 'enrolid', 'id');
    }
}
