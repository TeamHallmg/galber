<?php

namespace App\Models\Moodle;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
	/**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'moodle_mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'quiz';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];
	
}