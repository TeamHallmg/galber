<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class NotificationForUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notification_for_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'title', 'description', 'importance', 'see_since', 'see_until', 'seen'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
