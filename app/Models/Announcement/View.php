<?php

namespace App\Models\Announcement;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    //
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * View has many Announcements.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function announcements()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = view_id, localKey = id)
    	return $this->hasMany(Announcement::class);
    }

    /**
     * AnnouncementType belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function announcementsInThisView()
    {
        // belongsToMany(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
        return $this->belongsToMany(AnnouncementType::class, 'announcement_type_per_view');
    }

    /**
     * View has many Announcement.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function announcementViewSequences()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = view_id, localKey = id)
    	return $this->hasMany(announcementViewSequence::class)
        ->where('sequence','!=',0)
        ->orderBy('sequence','ASC');
    }
}
