<?php

namespace App\Models\Announcement;

use Illuminate\Database\Eloquent\Model;

class AnnouncementAttribute extends Model
{
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['form_name', 'name', 'attr_view', 'form_type', 'display_type', 'view_order'];

    /**
     * AnnouncementAttribute has many .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function forms()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = announcementAttribute_id, localKey = id)
    	return $this->hasMany(AnnouncementForm::class);
    }


}
