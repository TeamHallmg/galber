<?php

namespace App\Models\Announcement;

use Illuminate\Database\Eloquent\Model;
use App\Models\Announcement\Announcement;

class AnnouncementUserInteraction extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'announcement_user_interactions';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['action', 'announcement_id', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function announcement()
    {
        return $this->belongsTo(Announcement::class, 'announcement_id', 'id');
    }
    
    /**
     * Make a query ready with a group of actions per announcement
     *
     * @param  int $convenios_type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function actionPerActiveAnnouncement($action, $convenios_type){
        return AnnouncementUserInteraction::where('action', $action)->has('user')->whereHas('announcement', function($q) use($convenios_type){
            $q->where('announcement_type_id', $convenios_type)->where('active', true);
        });
    }
}
