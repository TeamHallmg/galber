<?php

namespace App\Models\Announcement;

use Illuminate\Database\Eloquent\Model;

class AnnouncementFormData extends Model
{
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['value','announcement_form_id', 'announcement_id'];

    /**
     * AnnouncementFormData belongs to AnnouncementForm.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function announcementForm()
    {
    	// belongsTo(RelatedModel, foreignKey = announcementForm_id, keyOnRelatedModel = id)
    	return $this->belongsTo(AnnouncementForm::class);
    }

    /**
     * AnnouncementFormData belongs to Announcement.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function announcement()
    {
    	// belongsTo(RelatedModel, foreignKey = announcement_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Announcement::class);
    }

    public static function findItem($announcement_form_id, $announcement_id){
        $data = AnnouncementFormData::where('announcement_form_id', $announcement_form_id)
        ->where('announcement_id', $announcement_id)
        ->first();
        return $data;
    }
    
}
