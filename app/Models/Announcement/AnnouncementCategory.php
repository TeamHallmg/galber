<?php

namespace App\Models\Announcement;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnnouncementCategory extends Model
{
	use SoftDeletes;
	//
	/**
	 * Fields that can be mass assigned.
	 *
	 * @var array
	 */
    protected $fillable = ['name', 'description', 'image', 'announcement_type_id'];
	
	/**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * AnnouncementCategory has many Announcements.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function announcements()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = announcementCategory_id, localKey = id)
    	return $this->hasMany(Announcement::class);
    }

    /**
     * AnnouncementCategory belongs to AnnouncementCategory.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentCategory()
    {
        // belongsTo(RelatedModel, foreignKey = announcementCategory_id, keyOnRelatedModel = id)
        return $this->belongsTo(AnnouncementCategory::class, 'announcement_category_id');
    }

    /**
     * AnnouncementCategory has many .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subCategories()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = announcementCategory_id, localKey = id)
        return $this->hasMany(AnnouncementCategory::class);
    }

    public function announcement_type()
    {
        return $this->belongsTo(AnnouncementType::class, 'announcement_type_id', 'id');
    }
}
