<?php

namespace App\Models\Announcement;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class AnnouncementViewSequence extends Model
{
	use SoftDeletes;
    //
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['sequence','view_id','announcement_type_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * AnnouncementViewSequence belongs to View.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function view()
    {
    	// belongsTo(RelatedModel, foreignKey = view_id, keyOnRelatedModel = id)
    	return $this->belongsTo(View::class);
    }

    /**
     * AnnouncementViewSequence belongs to Announcement.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    /**
     * AnnouncementViewSequence belongs to AnnouncementType.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function announcementType()
    {
        // belongsTo(RelatedModel, foreignKey = announcementType_id, keyOnRelatedModel = id)
        return $this->belongsTo(AnnouncementType::class);
    }
    /**
     * AnnouncementViewSequence has many Announcements.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function announcements()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = announcementViewSequence_id, localKey = id)
        return $this->hasMany(Announcement::class,'announcement_type_id','announcement_type_id')
        ->where('active',1)
        ->where('starts','<=', Carbon::now())
        ->where('ends','>=', Carbon::now());
    }

}
