<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CronLog extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cronlog';

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['date','type'];
    
}
