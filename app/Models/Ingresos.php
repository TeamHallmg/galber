<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ingresos extends Model
{
    protected $connection = 'inclock';

    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ingresos';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['person_number', 'timestamp'];
}
