<?php

namespace App\Models\EvaluacionResultados\Admin;

use Illuminate\Database\Eloquent\Model;

class StatusPlanEmpleado extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'status_plan_empleado';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_plan', 'id_empleado', 'status', 'fecha_solicitado', 'fecha_autorizado', 'fecha_registrado', 'fecha_revisado', 'periodo'];
}
