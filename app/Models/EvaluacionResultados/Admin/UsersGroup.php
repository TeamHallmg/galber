<?php
namespace App\Models\EvaluacionResultados\Admin;

use Illuminate\Database\Eloquent\Model;

class UsersGroup extends Model{

  /**
  * The database table used by the model.
  *
  * @var string
  */
  protected $table = 'grupos_usuarios';

  public function elements(){

    return $this->hasMany('App\Models\EvaluacionResultados\Admin\GroupElement', 'id_grupo', 'id');
  }
}