<?php
namespace App\Models\EvaluacionResultados\Admin;

use Illuminate\Database\Eloquent\Model;

class GroupElement extends Model{

  /**
  * The database table used by the model.
  *
  * @var string
  */
  protected $table = 'elementos_grupo';
}
