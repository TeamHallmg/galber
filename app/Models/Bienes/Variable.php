<?php

namespace App\Models\Bienes;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{  
	protected $table = 'variables';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['etiqueta',
     'nombre',
     'descripcion',
     'tipo',
     'requerido',
     'multiple',
     'editable',
     'seleccionable',
     'estado',
     'longitud'
  ];



     public function valores()
     {
         return $this->belongsTo('App\Models\Bienes\VariableValor','id','id_variable');
     }

    public function tipobienesdetalles() {
        return $this->hasMany('App\Models\Bienes\TipoBienesDetalle','id_variable');
    }


}
