<?php

namespace App\Models\Bienes;

use Illuminate\Database\Eloquent\Model;

class TipoBienesDetalle extends Model
{
	protected $table = 'tipo_bienes_detalle';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_tipo_bien','id_variable','estatus'];



     public function variable()
     {
         return $this->belongsTo('App\Models\Bienes\Variable','id_variable');
     }
 
     public function tipobien()
     {
         return $this->belongsTo('App\Models\Bienes\TipoBienes','id_tipo_bien');
     }
 

}
