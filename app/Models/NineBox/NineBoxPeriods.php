<?php

namespace App\Models\NineBox;

use App\Models\NineBox\Planes;
use Illuminate\Database\Eloquent\Model;

class NineBoxPeriods extends Model
{
    protected $table = '9box_periods';

    public function planes()
    {
        return $this->belongsTo(Planes::class, 'plan_id', 'id');
    }
}
