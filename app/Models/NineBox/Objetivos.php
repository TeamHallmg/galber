<?php

namespace App\Models\NineBox;

use App\Employee;
use App\Models\NineBox\Planes;
use Illuminate\Database\Eloquent\Model;

class Objetivos extends Model
{
    protected $table = 'objetivos';

    /* public function planes()
    {
        return $this->belongsTo(Planes::class, 'id_plan', 'id');
    } */

    public function empleado()
    {
        return $this->belongsTo(Employee::class, 'id_empleado', 'id');
    }
}
