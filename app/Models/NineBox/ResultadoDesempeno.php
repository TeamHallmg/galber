<?php

namespace App\Models\NineBox;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ResultadoDesempeno extends Model
{
    protected $table = 'resultados_desempeno';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->withDefault();
    }
}
