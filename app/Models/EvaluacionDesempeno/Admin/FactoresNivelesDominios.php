<?php

namespace App\Models\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class FactoresNivelesDominios extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'factores_niveles_dominios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_factor', 'nivel_dominio', 'calificacion', 'status'];
}
