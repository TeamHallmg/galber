<?php

namespace App\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class TiposEvaluadores extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tipos_evaluadores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre', 'organizacion', 'descripcion_empresa'];
}
