<?php

namespace App\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class ModalidadesPeriodos extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'modalidades_periodos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre'];
}
