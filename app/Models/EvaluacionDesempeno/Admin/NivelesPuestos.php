<?php

namespace App\Models\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class NivelesPuestos extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'niveles_puestos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre', 'mando'];

    public function getNameAttribute(){
        return $this->nombre;
    }

    public function Puestos(){
        return $this->belongsToMany('App\Models\JobPosition', 'id', 'id');
    }

    public function scopeNivelesPuestos($query, $NivelesPuestos){
        return $query->where('id', $NivelesPuestos);
    }
}
