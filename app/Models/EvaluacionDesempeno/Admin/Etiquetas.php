<?php

namespace App\Models\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class Etiquetas extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'etiquetas_desempeno';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre'];
}
