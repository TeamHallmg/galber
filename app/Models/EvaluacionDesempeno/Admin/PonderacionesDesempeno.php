<?php

namespace globalgas\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class PonderacionesDesempeno extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ponderaciones_desempeno';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_grupo_area', 'id_familia_factor', 'mando', 'ponderacion', 'id_periodo'];
}
