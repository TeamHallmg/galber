<?php

namespace App\Models\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class FactoresNivelesPuestos extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'factores_niveles_puestos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_factor', 'id_nivel_puesto', 'nivel_esperado'];

    public function nombreNivel(){
        return $this->hasMany('App\Models\EvaluacionDesempeno\Admin\NivelesPuestos', 'id', 'id_nivel_puesto');
    }
}
