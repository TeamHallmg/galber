<?php

namespace App\Models\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class FamiliasFactores extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'familias_factores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre', 'descripcion'];

    public function getNameAttribute(){
        return $this->nombre;
    }
}
