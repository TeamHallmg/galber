<?php

namespace App\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'areas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre'];

    public function getNameAttribute(){
        return $this->nombre;
    }

    public function areas(){
        return $this->hasMany('App\EvaluacionDesempeno\Admin\AreasGruposAreas', 'id_areas', 'id');
    }

    public function grupoAreas(){
        return $this->belongsToMany('App\EvaluacionDesempeno\Admin\GruposAreas', 'AreasGruposAreas')->withPivot('id_grupos_areas');
    }
}
