<?php

namespace App\Models\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class Puestos extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'puestos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['puesto'];

    public function getNameAttribute(){
        return $this->puesto;
    }

    public function puestos(){
        return $this->hasMany('App\Models\EvaluacionDesempeno\Admin\PuestosNivelesPuestos', 'id_puesto', 'id');
    }

}
