<?php

namespace App\Models\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class DepartmentsGruposAreas extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'departments_grupo_areas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_departments', 'id_grupos_areas'];

    public function GrupoAreas(){
        return $this->hasOne('App\Models\EvaluacionDesempeno\Admin\GruposAreas', 'id', 'id_grupos_areas');
    }

    public function Departments(){
        return $this->hasOne('App\Models\Department', 'id', 'id_departments');
    }

    public function scopeDepartmentGrupoArea($query, $DepartmentsGrupoAreas){
        return $query->where('id_grupos_areas', $DepartmentsGrupoAreas);
    }

    public function scopeDepartmentsGrupo($query, $DepartmentsGrupoAreas){
        return $query->where('id_departments', $DepartmentsGrupoAreas);
    }
}
