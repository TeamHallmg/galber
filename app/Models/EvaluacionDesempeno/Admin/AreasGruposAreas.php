<?php

namespace App\Models\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class AreasGruposAreas extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'areas_grupo_areas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_areas', 'id_grupos_areas'];

    public function GrupoAreas(){
        return $this->hasOne('App\Models\EvaluacionDesempeno\Admin\GruposAreas', 'id', 'id_grupos_areas');
    }

    public function Areas(){
        return $this->hasOne('App\Models\Area', 'id', 'id_areas');
    }

    public function scopeAreaGrupoArea($query, $AreasGrupoAreas){
        return $query->where('id_grupos_areas', $AreasGrupoAreas);
    }

    public function scopeAreasGrupo($query, $AreasGrupoAreas){
        return $query->where('id_areas', $AreasGrupoAreas);
    }
}
