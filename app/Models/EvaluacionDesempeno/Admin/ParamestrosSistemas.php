<?php

namespace App\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class ParamestrosSistemas extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'parametros_sistema';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['organizacion', 'calificacion_inicial', 'calificacion_final'];
}
