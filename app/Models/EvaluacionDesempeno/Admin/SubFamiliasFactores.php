<?php

namespace App\Models\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class SubFamiliasFactores extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sub_familias_factores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre'];

    public function getNameAttribute(){
        return $this->nombre;
    }
}
