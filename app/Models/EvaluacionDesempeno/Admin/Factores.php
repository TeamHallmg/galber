<?php

namespace App\Models\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class Factores extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'factores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre', 'descripcion', 'status', 'id_grupo_area', 'id_familia'];

    public function grupoArea(){
      return $this->hasOne('App\Models\EvaluacionDesempeno\Admin\GruposAreas', 'id', 'id_grupo_area');
    }

    public function factoresFamilia(){
      return $this->hasOne('App\Models\EvaluacionDesempeno\Admin\FamiliasFactores', 'id', 'id_familia');
    }

    public function nivelDeDominio(){
      return $this->hasMany('App\Models\EvaluacionDesempeno\Admin\FactoresNivelesDominios', 'id_factor', 'id');
    }

    public function nPuesto(){
      return $this->hasMany('App\Models\EvaluacionDesempeno\Admin\FactoresNivelesPuestos', 'id_factor', 'id');
    }
}
