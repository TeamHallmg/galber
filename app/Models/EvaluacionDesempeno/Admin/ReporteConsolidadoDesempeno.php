<?php

namespace App\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class ReporteConsolidadoDesempeno extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reporte_consolidado_desempeno';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_factor', 'id_familia_factor', 'id_grupo_area', 'id_evaluador', 'id_evaluado', 'ponderacion', 'total_ponderacion_respuestas' , 'valor_esperado', 'valor_obtenido', 'id_periodo'];
}
