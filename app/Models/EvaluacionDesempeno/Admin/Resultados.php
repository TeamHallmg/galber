<?php

namespace App\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class Resultados extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'resultados';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_periodo', 'id_evaluador', 'id_evaluado', 'id_factor', 'nivel_desempeno'];

    public function nameUser(){
      return $this->hasOne('App\User', 'id', 'id_evaluado');
    }

    public function bossEvaluado(){
      return $this->hasMany('App\User', 'boss_id', 'id_evaluado');
    }

    public function puesto(){
      return $this->hasOne('App\User', 'id', 'id_evaluado');
    }

    public function puestoReporte(){
      return $this->hasOne('App\User', 'id', 'id_evaluado');
    }

    public function area(){
      return $this->hasOne('App\User', 'id', 'id_evaluado');
    }

    public function grupoArea(){
      return $this->hasOne('App\User', 'id', 'id_evaluado');
    }

    public function factores(){
      return $this->hasOne('App\EvaluacionDesempeno\Admin\Factores', 'id', 'id_factor');
    }

    public function factoresNivelDom(){
      return $this->hasMany('App\EvaluacionDesempeno\Admin\FactoresNivelesDominios', 'id_factor', 'id_factor');
    }

    public function nivelPuesto(){
      return $this->hasMany('App\EvaluacionDesempeno\Admin\FactoresNivelesPuestos', 'id_factor', 'id_factor');
    }

    public function nivelPuestoReporte(){
      return $this->hasOne('App\EvaluacionDesempeno\Admin\FactoresNivelesPuestos', 'id_factor', 'id_factor');
    }
}
