<?php

namespace App\Models\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class GruposAreas extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'grupo_areas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre'];

    public function getNameAttribute(){
        return $this->nombre;
    }

    public function departments(){
        return $this->belongsToMany('App\Models\Department', 'id', 'id_department');
    }

    public function scopeGruposAreas($query, $GrupoAreas){
        return $query->where('id', $GrupoAreas);
    }
}
