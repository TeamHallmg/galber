<?php

namespace App\Models\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class PuestosNivelesPuestos extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'puesto_nivel_puesto';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_nivel_puesto', 'id_puesto'];

    public function Puestos(){
        return $this->hasOne('App\Models\JobPosition', 'id', 'id_puesto');
    }

    public function Niveles(){
        return $this->hasMany('App\Models\EvaluacionDesempeno\Admin\NivelesPuestos', 'id', 'id_nivel_puesto');
    }

    public function nivelesReporte(){
        return $this->hasOne('App\Models\EvaluacionDesempeno\Admin\NivelesPuestos', 'id', 'id_nivel_puesto');
    }

    public function deseadoReporte(){
        return $this->hasOne('App\Models\EvaluacionDesempeno\Admin\FactoresNivelesPuestos', 'id_nivel_puesto', 'id_nivel_puesto');
    }

    public function idPuestoUser(){
        return $this->belongsTo('App\Employee', 'job_position_id', 'id_puesto');   
    }

    public function scopePuestoNivelPuesto($query, $PuestosNivelPuestos){
        return $query->where('id_nivel_puesto', $PuestosNivelPuestos);
    }

    public function scopePuestos($query, $Puestos){
        return $query->where('id_puesto', $Puestos);
    }
}
