<?php

namespace App\Models\EvaluacionDesempeno;

use Illuminate\Database\Eloquent\Model;

class TipoEvaluacion extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tipos_evaluadores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre', 'descripcion_empresa'];

    public function getNameAttribute(){
        return $this->nombre;
    }

    public function getDescriptionAttribute(){
        return $this->descripcion_empresa;
    }
}
