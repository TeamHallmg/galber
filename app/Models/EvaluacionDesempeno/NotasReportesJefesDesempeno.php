<?php

namespace globalgas\EvaluacionDesempeno;

use Illuminate\Database\Eloquent\Model;

class NotasReportesJefesDesempeno extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notas_reportes_jefe_desempeno';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_boss', 'id_employee', 'nota'];

    public function nameUser(){
      return $this->hasOne('globalgas\User', 'id', 'id_boss');
    }

    public function famFactores(){
      return $this->hasOne('globalgas\EvaluacionDesempeno\Admin\Factores', 'id', 'id_factor');
    }

}
