<?php namespace App\Models\EvaluacionDesempeno;

use Illuminate\Database\Eloquent\Model;

class EvaluadorEvaluado extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'evaluador_evaluado';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_evaluador', 'id_evaluado', 'status', 'id_periodo', 'tipo_evaluacion'];

    public function nameCompleteUserEvaluador(){
    	return $this->hasOne('App\User', 'id', 'id_evaluador')->withTrashed();
    }

    public function nameEvaluaciones(){
        return $this->hasOne('App\User', 'id', 'id_evaluado');
    }

    public function nameCompleteUserEvaluado(){
    	return $this->hasOne('App\User', 'id', 'id_evaluado')->withTrashed();
    }

    public function puestoUserEvaluado(){
        return $this->hasOne('App\User', 'id', 'id_evaluado');
    }

    public function areaYPuestoUsuario(){
        return $this->hasOne('App\User', 'id', 'id_evaluado');
    }

    public function areaEvaluado(){
        return $this->hasOne('App\User', 'id', 'id_evaluado');
    }

    public function puestoEvaluado(){
        return $this->hasOne('App\User', 'id', 'id_evaluado');
    }

    public function puestoUsuario(){
        return $this->hasOne('App\User', 'id', 'id_evaluado');
    }

    public function tipoEvaluacion(){
        return $this->hasOne('App\Models\EvaluacionDesempeno\TipoEvaluacion', 'id', 'tipo_evaluacion');
    }

    public function periodoName(){
        return $this->hasOne('App\Models\EvaluacionDesempeno\Admin\Periodos', 'id', 'id_periodo');
    }

    public function scopeEvaluador($query){
    	return $query->where('id_evaluador', auth()->user()->id);
    }

    public function scopeEvaluado($query){
        return $query->where('id_evaluado');
    }
}
