<?php

namespace App\Models\EvaluacionDesempeno\Admin;

use Illuminate\Database\Eloquent\Model;

class Periodos extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'periodos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['fecha_inicio', 'fecha_cierre', 'id_modalidad', 'descripcion', 'status', 'organizacion'];

    public function getNameAttribute(){
        return $this->descripcion;
    }
}
