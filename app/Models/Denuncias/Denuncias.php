<?php

namespace App\Models\Denuncias;

use App\User;
use Carbon\Carbon;
use App\Models\Denuncias\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Denuncias extends Model
{
    use SoftDeletes;
    
    protected $table = 'denuncias';

    protected $fillable = [
        'denunciante_id',
        'tipo',
        'empresa_hechos',
        'tipo_usuario',
        'lugar_hechos',
        'fecha_hechos',
        'personas_involucradas',
        'denuncia',
        'nombre_externo',
        'email_externo',
        'ciudad_externo',
        'puesto_externo',
        'clasificacion',
        'area_corresponde',
        'estatus',
        'importancia',
        'comentarios',
        'responsable_id',
        'fecha_seguimiento',
        'fecha_cierre',
        'afectan_a',
        'afectan_a_categoria',
        'debe_ser',
        'como_se_hace',
        'que_sugiere',
        'otros'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'responsable_id', 'id');
    }
    
    public function files() {
        return $this->morphMany(File::class, 'files');
    }

    public function images() {
        return $this->morphMany(File::class, 'files')->where('type', 'image');
    }

    public function images_first() {
        return $this->morphMany(File::class, 'files')->where('type', 'image')->first();
    }

    public function documents() {
        return $this->morphMany(File::class, 'files')->where('type', 'document');
    }

    public function bitacora()
    {
        return $this->hasMany(Bitacora::class, 'denuncia_id', 'id');
    }

    public function formatoFecha($fecha){
        if ($fecha == '0000-00-00')
            return '';

        return Carbon::parse($fecha)->format('d-m-Y');
    }
}
