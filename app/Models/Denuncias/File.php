<?php

namespace App\Models\Denuncias;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'denuncias_files';

    protected $fillable = ['denuncia_id', 'type', 'filename', 'name', 'extension', 'model', 'model_id'];

    public function files() {
        return $this->morphTo();
    }

    public function fullname() {
        return $this->name.'.'.$this->extension;
    }
    public function url() {
        return $this->filename;
    }

}
