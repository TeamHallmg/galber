<?php

namespace App\Models\Denuncias;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bitacora extends Model
{
    use SoftDeletes;

    protected $table = 'denuncias_bitacora';

    protected $fillable = [
        'denuncia_id',
        'user_id',
        'comentarios',
        'fecha_hora',
        'accion'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function denuncias()
    {
        return $this->belongsTo(Denuncias::class, 'denuncia_id', 'id');
    }
}
