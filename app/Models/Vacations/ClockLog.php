<?php

namespace App\Models\Vacations;

use App\User;
use Carbon\Carbon;
use App\Http\Helpers\ClockLogManager;
use Illuminate\Database\Eloquent\Model;

class ClockLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clock_logs';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['date', 'user_id', 'longitude', 'latitude', 'address', 'comment', 'request_ip', 'from', 'compare_time', 'arrive_status', 'type', 'status'];

    public function getDateOnFormat($format = 'Y-m-d'){
        return $this->date?Carbon::createFromFormat('Y-m-d H:i:s', $this->date)->format($format):'';
    }

    /**
     * Scope a query to only include registers without arrive status or type
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNoInfo($query)
    {
        return $query->whereNull('arrive_status')
        ->orWhereNull('type');
    }

    /**
     * Scope a query to only include registers where the users arrive late
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeArriveLate($query)
    {
        return $query->where('arrive_status', 'late')
        ->where('type', 'in');
    }

    /**
     * Scope a query to only include registers where the users leave early
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLeaveEarly($query)
    {
        return $query->where('arrive_status', 'early')
        ->where('type', 'out');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->withTrashed();
    }

    public function comments()
    {
        return $this->hasMany(ClockLogComment::class, 'clock_log_id', 'id');
    }

    public function getArriveStatus(){
        $check_hour = new Carbon(date('H:i', strtotime($this->date)));
        $should_check = new Carbon($this->compare_time);
        $diff = $check_hour->diffInMinutes($should_check, false);
        if($this->type === 'in'){
            if($diff === 0){
                return 'on-time';
            }elseif($diff < 0){
                return 'early';
            }elseif($diff <= $this->tolerance){
                return 'tolerance';
            }elseif($diff > $this->tolerance){
                return 'late';
            }else{
                return '';
            }
        }elseif ($this->type === 'out'){
            if($diff === 0){
                return 'on-time';
            }elseif($diff < 0){
                return 'leave_early';
            }elseif($diff > 0){
                return 'ok';
            }else{
                return '';
            }
        }
        return "none";
    }

    public function getType(){
        if($this->type === 'in'){
            return 'Entrada';
        }else if($this->type === 'out'){
            return 'Salida';
        }else{
            return '-';
        }
    }

    public function getTypeInfo(){
        if($this->type === 'in'){
            switch($this->arrive_status){
                case "on-time":
                    return "Llegada";
                break;
                case "early":
                    return "Llegada Temprano";
                break;
                case "tolerance":
                    return "Tolerancia";
                break;
                case "late":
                    return "Llegada tarde";
                break;
            }
            return 'Entrada';
        }else if($this->type === 'out'){
            switch($this->arrive_status){
                case "on-time":
                    return "Salida";
                break;
                case "early":
                    return "Salida temprano";
                break;
                case "tolerance":
                    return "Salida";
                break;
                case "late":
                    return "Salida";
                break;
            }
            return 'Salida';
        }else{
            return '-';
        }
    }
}
