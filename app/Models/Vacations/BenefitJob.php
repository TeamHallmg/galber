<?php

namespace App\Models\Vacations;

use App\Models\JobPosition;
use Illuminate\Database\Eloquent\Model;

class BenefitJob extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'benefit_job';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['benefit_id', 'job_position_id'];

    public function benefit()
    {
        return $this->belongsTo(Benefits::class, 'benefit_id', 'id');
    }

    public function jobPosition()
    {
        return $this->belongsTo(JobPosition::class, 'job_position_id', 'id');
    }
}
