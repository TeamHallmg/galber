<?php

namespace App\Models\Vacations;

use App\Models\JobPosition;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Benefits extends Model
{
    use SoftDeletes;
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'benefits';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['code','shortname', 'name','context','type','continuous','cronjob','report','remove','relationship','days','frequency','group','retype','exec','gender', 'file_required','blocked'];

	protected $dates = ['deleted_at'];

	/**
	 * Scope a query to only include benefit group
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeGroupBenefit($query)
	{
		return $query->where('group', 'benefit');
	}

	public function events()
	{
		return $this->hasMany(Event::class, 'benefit_id', 'id');
	}

	public function incidents()
	{
		return $this->hasMany(Incident::class, 'benefit_id', 'id');
	}
	
	public function jobBenefits()
    {
        return $this->belongsToMany(JobPosition::class, 'benefit_job', 'benefit_id', 'job_position_id');
	}
	
	public function timeDescription()
	{
		return $this->hasOne(BenefitTimeDescription::class, 'benefit_id', 'id');
	}

	public function getMaxTimePerRequest(){
		return $this->timeDescription && $this->timeDescription->hasUseMaxData()?$this->timeDescription->use_max_quantity_per_time:null;
	}
	
	/**
	 * Check if user has the required seniority requested by $this benefit
	 *
	 * @param  \App\User $user
	 * @return bool
	 */
	public function checkIfUserHasRequiredSeniority($user){
		if(!$this->timeDescription || !$this->timeDescription->hasSeniorityData()) return true;
		return $user->getSeniorityDays() >= $this->timeDescription->getSeniorityDays();
	}
		
	/**
	 * Check if employee request doesn't conflict with max request in the benefit's period
	 *
	 * @param  \App\User $user
     * @param  string $start in Y-m-d format
     * @param  string $end in Y-m-d format
     * @param  int $days count in the request
	 * @param  null $time (optional) int - Total hours in the request
	 * @return bool
	 */
	public function checkIfUserHasRequestsInTimePeriod($user, $start, $end, $days, $time = null){
		if(!$this->timeDescription || !$this->timeDescription->hasPeriodUseData()) return true;
		return $this->timeDescription->checkDateRangePeriodUseData($user, $start, $end, $days, $time);
	}
	
	/**
	 * Check if employee request fits in the max time data
	 *
	 * @param  \App\User $user
	 * @param  int $days count in the request
	 * @param  null $time (optional) int - Total hours in the request
	 * @return bool
	 */
	public function checkIfDaysFitsInUseMaxData($days, $time = nul){
		if(!$this->timeDescription || !$this->timeDescription->hasUseMaxData()) return true;
		return $this->timeDescription->checkIfDaysFitsInUseMaxData($days, $time);
	}

}
