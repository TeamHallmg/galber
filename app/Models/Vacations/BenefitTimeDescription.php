<?php

namespace App\Models\Vacations;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BenefitTimeDescription extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'benefit_time_description';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['seniority_type_of_time', 'seniority_quantity', 'period_use_type_of_time', 'period_use_quantity', 'use_max_type_of_time', 'use_max_quantity_per_time', 'benefit_id'];

    public function benefit()
    {
        return $this->belongsTo(Benefits::class, 'benefit_id', 'id');
    }
    
    /**
     * Check if both fields "seniority_type_of_time" and "seniority_quantity" have data
     *
     * @return bool
     */
    public function hasSeniorityData(){
		return $this->seniority_type_of_time && $this->seniority_quantity;
    }
    
    /**
     * Check if both fields "period_use_type_of_time" and "period_use_quantity" have data
     *
     * @return bool
     */
    public function hasPeriodUseData(){
        return $this->period_use_type_of_time && $this->period_use_quantity;
    }

    /**
     * Check if both fields "use_max_type_of_time" and "use_max_quantity_per_time" have data
     *
     * @return bool
     */
    public function hasUseMaxData(){
        return $this->use_max_type_of_time && $this->use_max_quantity_per_time;
    }
    
    /**
     * Calculate de days of seniority required for this benefit
     *
     * @return int|null null if benefit has seniority data but "seniority_type_of_time" field is not yet define
     */
    public function getSeniorityDays(){
        if($this->hasSeniorityData()){
            switch($this->seniority_type_of_time){
                case 'day':
                    return $this->seniority_quantity;
                break;
                case 'week':
                    return $this->seniority_quantity * 7;
                break;
                case 'month':
                    return $this->seniority_quantity * 30;
                break;
                case 'year':
                    return $this->seniority_quantity * 365;
                break;
            }
        }
        return null;
    }
    
    /**
     * Check if the employee has enough days available to request
     *
     * @param  \App\User $user
     * @param  string $start in Y-m-d format
     * @param  string $end in Y-m-d format
     * @param  int $days count in the request
     * @param  null $time (optional) int - Total hours in the request
     * @return bool|null null if benefit has period use data but "period_use_type_of_time" field is not yet define
     */
    public function checkDateRangePeriodUseData($user, $start, $end, $days, $time = null){
        if(!$this->hasPeriodUseData()) return true;
        $dates = $this->getDateRangePeriodUseData($start, $end);
        switch($this->period_use_type_of_time){
            case 'month':
                return $this->checkMonthDateRangePeriod($user, $dates, $days, $time);
            break;
            case 'year':
                return $this->checkYearDateRangePeriod($user, $dates, $days, $time);
            break;
            case 'year-hour':
                return $this->checkYearHourDateRangePeriod($user, $dates, $days, $time);
            break;
        }
        return null;
    }
    
    /**
     * Calculate the date ranges depending on the period of use for this benefit
     *
     * @param  string $start in Y-m-d format
     * @param  string $end in Y-m-d format
     * @return bool|null null if benefit has period use data but "period_use_type_of_time" field is not yet define
     */
    public function getDateRangePeriodUseData($start, $end){
        if($this->hasPeriodUseData()){
            switch($this->period_use_type_of_time){
                case 'month':
                    return $this->getMonthDateRange($start, $end);
                break;
                case 'year':
                    return $this->getYearDateRange($start, $end);
                break;
                case 'year-hour':
                    return $this->getYearDateRange($start, $end);
                break;
            }
        }
        return null;
    }
    
    /**
     * Check if the event days doesn't surpass the max days in the period of use
     *
     * @param  \App\User $user
     * @param  array $dates Either an Array in this format ['start' => Y-m-d, 'end' => Y-m-d] or Arrays in Array in this format ['start' => ['start' => Y-m-d, 'end' => Y-m-d], 'end' => ['start' => Y-m-d, 'end' => Y-m-d]]
     * @param  mixed $days count in the request
     * @param  null $time (optional) int - Total hours in the request
     * @return bool
     */
    public function checkMonthDateRangePeriod($user, $dates, $days, $time = null){
        if(is_array($dates['start'])){

        }else{
            $events = $this->getEvents($user, $dates);
            if($events->isEmpty()) return $days <= $this->period_use_quantity;
            
            $cont = 0;
            foreach($events as $event){
                $start = $event->start;
                while($start <= $event->end){
                    if( $start >= strtotime($dates['start']) && $start <= strtotime($dates['end']) )
                        $cont++;
                    $start += 24 * 60 * 60;
                }
            }
            return $cont + $days <= $this->period_use_quantity;
        }
    }
    
    /**
     * Check if the event days doesn't surpass the max days in the period of use
     *
     * @param  \App\User $user
     * @param  array $dates Either an Array in this format ['start' => Y-m-d, 'end' => Y-m-d] or Arrays in Array in this format ['start' => ['start' => Y-m-d, 'end' => Y-m-d], 'end' => ['start' => Y-m-d, 'end' => Y-m-d]]
     * @param  mixed $days count in the request
     * @return bool
     */
    public function checkYearDateRangePeriod($user, $dates, $days){
        if(is_array($dates['start'])){

        }else{
            $events = $events = $this->getEvents($user, $dates);
            if($events->isEmpty()) return $days <= $this->period_use_quantity;
            $cont = 0;
            foreach($events as $event){
                if($event->benefit->week_calendar){
                    $cont++;
                    continue;
                }
                $start = $event->start;
                while($start <= $event->end){
                    if( $start >= strtotime($dates['start']) && $start <= strtotime($dates['end']) )
                        $cont++;
                    $start += 24 * 60 * 60;
                }
            }
            return $cont + $days <= $this->period_use_quantity;
        }
    }

    /**
     * Check if the event hours doesn't surpass the max hours in the period of use
     *
     * @param  \App\User $user
     * @param  array $dates Either an Array in this format ['start' => Y-m-d, 'end' => Y-m-d] or Arrays in Array in this format ['start' => ['start' => Y-m-d, 'end' => Y-m-d], 'end' => ['start' => Y-m-d, 'end' => Y-m-d]]
     * @param  mixed $days count in the request
     * @param  null $time (optional) int - Total hours in the request
     * @return bool
     */
    public function checkYearHourDateRangePeriod($user, $dates, $days, $time){
        $events = $events = $this->getEvents($user, $dates);
        if($events->isEmpty()) return $time <= $this->period_use_quantity;

        $cont = 0;
        $cont = $events->reduce(function ($carry, $item) {
            return $carry + $item->time;
        });
        return $cont + $time <= $this->period_use_quantity;
    }
    
    /**
     * Check if requested days fit under the benefit max use per request
     *
     * @param  mixed $days Days or Hours in the request
     * @param  null $time (optional) int - Total hours in the request
     * @return bool|null
     */
    public function checkIfDaysFitsInUseMaxData($days, $time = null){
        if($this->hasUseMaxData()){
            switch ($this->use_max_type_of_time) {
                //Time param is required for hour type
                case 'hour':
                    return $time <= $this->use_max_quantity_per_time;
                case 'day':
                    return $days <= $this->use_max_quantity_per_time;
                break;
            }
        }
        return null;
    }
    
    /**
     * Get the dates for the first day and last day of month for each month depending on the dates given
     *
     * @param  string $start in Y-m-d format
     * @param  string $end in Y-m-d format
     * @param  array $dates Either an Array in this format ['start' => Y-m-d, 'end' => Y-m-d] or Arrays in Array in this format ['start' => ['start' => Y-m-d, 'end' => Y-m-d], 'end' => ['start' => Y-m-d, 'end' => Y-m-d]]
     */
    public function getMonthDateRange($start, $end){
        $startDate = Carbon::createFromFormat('Y-m-d', $start);
        $endDate = Carbon::createFromFormat('Y-m-d', $end);
        if($this->areDatesOfSameMonth($start, $end)){
            return ['start' => $startDate->startOfMonth()->format('Y-m-d'), 'end' => $endDate->endOfMonth()->format('Y-m-d')];
        }else{
            return [
                'start' => [
                    'start' => $startDate->startOfMonth()->format('Y-m-d'), 
                    'end' => $startDate->endOfMonth()->format('Y-m-d'),
                ],
                'end' => [
                    'start' => $endDate->startOfMonth()->format('Y-m-d'),
                    'end' => $endDate->endOfMonth()->format('Y-m-d'),
                ]
            ];
        }
    }
    
    /**
     * Get the dates for the first day and last day of year for each year depending on the dates given
     *
     * @param  string $start in Y-m-d format
     * @param  string $end in Y-m-d format
     * @param  array $dates Either an Array in this format ['start' => Y-m-d, 'end' => Y-m-d] or Arrays in Array in this format ['start' => ['start' => Y-m-d, 'end' => Y-m-d], 'end' => ['start' => Y-m-d, 'end' => Y-m-d]]
     */
    public function getYearDateRange($start, $end){
        $startDate = Carbon::createFromFormat('Y-m-d', $start);
        $endDate = Carbon::createFromFormat('Y-m-d', $end);
        if($this->areDatesOfSameYear($start, $end)){
            return ['start' => $startDate->startOfYear()->format('Y-m-d'), 'end' => $endDate->endOfYear()->format('Y-m-d')];
        }else{
            return [
                'start' => [
                    'start' => $startDate->startOfYear()->format('Y-m-d'), 
                    'end' => $startDate->endOfYear()->format('Y-m-d'),
                ],
                'end' => [
                    'start' => $endDate->startOfYear()->format('Y-m-d'),
                    'end' => $endDate->endOfYear()->format('Y-m-d'),
                ]
            ];
        }
    }
    
    /**
     * Check if dates are from same month
     *
     * @param  string $start in Y-m-d format
     * @param  string $end in Y-m-d format
     * @return bool
     */
    public function areDatesOfSameMonth($start, $end){
        $startDate = Carbon::createFromFormat('Y-m-d', $start)->startOfMonth();
        $endDate = Carbon::createFromFormat('Y-m-d', $end)->endOfMonth();
        return $startDate->format('F') === $endDate->format('F');
    }
    
    /**
     * Check if dates are from same year
     *
     * @param  string $start in Y-m-d format
     * @param  string $end in Y-m-d format
     * @return bool
     */
    public function areDatesOfSameYear($start, $end){
        $startDate = Carbon::createFromFormat('Y-m-d', $start)->startOfYear();
        $endDate = Carbon::createFromFormat('Y-m-d', $end)->endOfYear();
        return $startDate->year === $endDate->year;
    }
    
    /**
     * getEvents
     *
     * @param  \App\User $user
     * @param  array $dates Either an Array in this format ['start' => Y-m-d, 'end' => Y-m-d]
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getEvents($user, $dates){
        return Event::where('user_id', $user->id)
        ->where('benefit_id', $this->benefit_id)
        ->whereIn('status', ['pending', 'accepted', 'processed', 'processing'])
        ->whereHas('eventDays', function($q) use($dates){
            $q->where('date', '>=', $dates['start'])
            ->where('date', '<=', $dates['end']);
        })
        ->get();
    }
}
