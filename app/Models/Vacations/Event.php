<?php

namespace App\Models\Vacations;

use App\Models\Region;
use App\Models\Vacations\EventDay;
use App\Models\Vacations\Incident;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\Vacations\PleaManager;
use App\Http\Controllers\Vacations\PleaController;

class Event extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'start', 'end', 'time', 'type', 'user_id', 'blocked', 'status', 'benefit_id', 'region_id', 'js', 'processed_at', 'processed_by'];

    /**
     * Scope a query to only include active or processed requests
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatusActive($query)
    {
        return $query->whereIn('status', ['pending', 'processing', 'accepted', 'processed']);
    }

    /*
    public static getEventsBetween($user,$start,$end,$type,$results = 'first')
    {
    $result = Event::where([['type',$type],['start','<=',$start],['end','>=',$end]])
    ->where(function($query){
    $query->where('js','like','%"' . $user->id . '"%')
    ->orWhere('region_id',$user->region_id);
    })
    ->first();
    return ($result);
    }
     */

    public function getFullDescriptionAttribute(){
        $msg = $this->title;
        $msg .= $this->benefit->type === "time"?(' por ' . $this->time . ' Horas.'):'';
        $msg .= ' (' . $this->benefit->shortname . ')' ;
        return $msg;
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function benefit()
    {
        return $this->belongsTo('App\Models\Vacations\Benefits');
    }

    public function incident(){
        return $this->hasOne(Incident::class, 'event_id', 'id');
    }

    public function getStartDate($format = 'd-m-Y')
    {
        return date($format, $this->start);
    }

    public function getEndDate($format = 'd-m-Y')
    {
        return date($format, $this->end);
    }

    public function eventDays()
    {
        return $this->hasMany(EventDay::class);
    }

    public function eventDay()
    {
        return $this->hasOne(EventDay::class);
    }

    public function generateValidStartDate()
    {
        if ($this->start !== $this->end) {
            $start = $this->start;
            $end = $this->end;            
            $schedule = $this->getSchedule($start, null, true);
            $holidays = PleaController::getHolidays($this->start, $this->end, $this->user);

            $d = date('D', $start);
            while (isset($holidays[$start]) || $schedule[$d][2] == 0) {
                $start += (1 * 24 * 60 * 60);
                $d = date('D', $start);
            }
            $this->start = $start;
        }
    }

    public function generateValidEndDate()
    {
        if ($this->start !== $this->end) {
            $start = $this->start;
            $end = $this->end;
            $schedule = $this->getSchedule($start, null, true);
            $holidays = PleaManager::getHolidays($this->start, $this->end, $this->user);
            $holidays += PleaManager::getUserEventDays($this->getStartDate('Y-m-d'), $this->getEndDate('Y-m-d'), $this->user);
            $d = date('D', $end);
            while (isset($holidays[$end]) || $schedule[$d][2] == 0) {
                $end -= (1 * 24 * 60 * 60);
                $d = date('D', $end);
            }
            $this->end = $end;
        }
    }
    
    /**
     * If dates are different, the can change if they clash with an event, incident, etc.
     *
     * @return void
     */
    public function generateValidDates()
    {
        if ($this->start !== $this->end) {
            // $this->generateValidStartDate();
            $this->generateValidEndDate();
        }
    }

    public function getDays($onlyLabor = false)
    {
        $start = $this->start;
        $end = $this->end;
        $days = 0;
        $holidays = PleaManager::getHolidays($this->start, $this->end, $this->user);
        if ($start === $end) {
            if (!isset($holidays[$this->start]) && $this->user->getDaysCountInMySchedule($this->start, $this->end, $onlyLabor)) {
                $days = 1;
            }
        } else {
            while ($start < $end) {
                if (isset($holidays[$start])) {
                    $days--;
                }
                $last = date('Y-m-d', $start);
                $start = strtotime($last.' +1 days');
                // $start += 24 * 60 * 60;
            }
            $days += $this->user->getDaysCountInMySchedule($this->start, $this->end, $onlyLabor);
        }

        return ($days);
    }

    public function getUserSchedule($onlyLabor = false)
    {
        $start = $this->start;
        $end = $this->end;
        if ($this->user->getRegion()) {
            return $this->user->employee->region->getDays($start, $end); 
        }
        return [];
    }
    
    /**
     * Check if start date of event is workday in my working schedule
     *
     * @return bool
     */
    public function isStartDateWorkable()
    {
        return $this->user->isDayWorkable($this->getStartDate('Y-m-d'));
    }
    
    /**
     * Check if start date of event clash with an Event Holiday
     *
     * @return void
     */
    public function isStartDateAnEvent()
    {
        $globals = Event::where('type', 'global')
        ->where('blocked', 1)
        ->where(function ($query) {
            $query->where('start', $this->start);
        })
        ->where(function ($query) {
            $query->where('js', 'like', '%"' . $this->user_id . '"%')
                ->orWhere([
                    ['region_id', $this->user->employee->region_id],
                    ['js', null],
                ]);
        })
        ->first();
        return !!$globals;
    }
    
    /**
     * Check if start date of event clash with an Event created by the current user instance
     *
     * @return void
     */
    public function isStartDateClashWithRequest(){
        $globals = Event::has('incident')
        ->statusActive()
        ->where('user_id', $this->user_id)
        ->whereHas('eventDays', function($q){
            $q->where('date', $this->getStartDate('Y-m-d'));
        })
        ->first();
        return !!$globals;
    }

    public function checkIfDaysAreWorkable()
    {
        $start = $this->start;
        $end = $this->end;
        if ($start === $end) {
            $this->user->isDayWorkable($start);
        } else {

        }

    }

    public function getTotalDays()
    {
        $days = 0;
        $start = $this->start;
        $end = $this->end;

        if (($end - $start) == 0) {
            $days = 1;
        } else {
            $start = $this->start;
            $end = $this->end + (1 * 24 * 60 * 60);
            $days = $end - $start;
            $days = $days / (1 * 24 * 60 * 60);
        }
        return ($days);
    }

    public function continuous()
    {
        $days = 1;
        $start = $this->start;
        $total = $this->benefit->days;
        $schedule = $this->user->getMySchedule();

        while ($days < $total) {
            $d = date('D', $start);
            if ($schedule[$d][2] == 0) {
                $days--;
            }

            $start += (1 * 24 * 60 * 60);
            $days++;
        }
        $this->end = $start;
    }
    
    /**
     * For continuous incidents with an specific day range, calculates the end date, skipping holidays and user requests
     *
     * @return void
     */
    public function continuousDays()
    {
        $holidays = PleaManager::getHolidays($this->start, $this->end, $this->user);
        $holidays += PleaManager::getUserEventDays($this->getStartDate('Y-m-d'), $this->getEndDate('Y-m-d'), $this->user);
        $days = 1;
        $newEnd = $this->start;
        $total = $this->benefit->days;
        $schedule = $this->getSchedule($newEnd, null, true);
        $dayReturn = array();
        while ($days < $total) {
            $d = date('D', $newEnd);
            if (!$this->benefit->natural_days && ($schedule[$d][2] == 0 || isset($holidays[$newEnd]))) {
                $days--;
            }
            $newEnd = strtotime('+1 days', $newEnd);
            $days++;
        }
        return $newEnd;
    }
    
    /**
     * Get an array with the employee working schedule
     *
     * @param  string $begin timestamp
     * @param  null $end (optional) string timestamp
     * @param  bool $whole_week
     * @return array [ShortName of Day] => [{schedule_in}, {schedule_out}, {schedule_labor}]
     */
    public function getSchedule($begin, $end = null, $whole_week = false){
        $schedules = $this->user->getSchedule($begin, null, true);
        $schedule = [];
        foreach ($schedules as $day) {
            if($day->schedule){
                $schedule[$day->day][] = $day->schedule->in;
                $schedule[$day->day][] = $day->schedule->out;
                $schedule[$day->day][] = $day->labor;
            }
        }
        return $schedule;
    }

    public function howMDFT($today)
    {
        $days = 0;
        $start = $today - (1 * 24 * 60 * 60);
        $end = $this->end;
        $schedule = $this->user->getMySchedule();
        if (($end - $start) == 0) {
            $days = 0;
        } else {
            $days = $end - $start;
            $days = $days / (1 * 24 * 60 * 60);
            while ($end > $start) {
                $d = date('D', $start);
                if ($schedule[$d][2] == 0) {
                    $days--;
                }

                $start += (1 * 24 * 60 * 60);
            }
        }
        return ($days);
    }

    public function getActiveDayCount(){
        return $this->eventDays()->blockDays()->count();
    }
}
