<?php

namespace App\Models\Vacations;

use App\User;
use Carbon\Carbon;
use App\Models\DeletedReason;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Incident extends Model
{
    use SoftDeletes;

    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'incidents';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id','from_id','benefit_id','event_id','week','time','amount','info','comment','file_path','status','value','manager','incapacity_id'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function user_wt(){
        return $this->belongsTo(User::class, 'user_id', 'id')->withTrashed();
    }
	public function from(){
        return $this->belongsTo(User::class, 'from_id', 'id');
    }
	public function from_wt(){
        return $this->belongsTo(User::class, 'from_id', 'id')->withTrashed();
    }

    public function benefit() {
        return $this->belongsTo('App\Models\Vacations\Benefits');
    }
    public function incapacity(){
        return $this->belongsTo('App\Models\Vacations\Incapacity');
    }
    public function event(){
        return $this->belongsTo('App\Models\Vacations\Event');
    }

    public function incidentOvertime()
    {
        return $this->hasOne('App\Models\Vacations\IncidentOvertime');
    }

    public function getDate(){
        return date('d-m-Y',$this->time);
    }

    public function getTimeOnFormat($format = 'Y-m-d'){
        return $this->time?Carbon::createFromTimestamp($this->time)->format($format):'';
    }

    public function deleted_reason() {
        return $this->morphMany(DeletedReason::class, 'deleted_reason');
    }
}
