<?php

namespace App\Models\Vacations;

use Illuminate\Database\Eloquent\Model;
use App\Models\Vacations\Event;
use App\Models\Vacations\Balances;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class EventDay extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_days';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['date', 'block', 'processed', 'balance_id', 'event_id', 'processed_at', 'processed_by'];

    /**
     * Scope a query to only include non block days
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNonBlockDays($query)
    {
        return $query->where('block', 0);
    }

    /**
     * Scope a query to only include block days
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeBlockDays($query)
    {
        return $query->where('block', 1);
    }

    public function balance()
    {
        return $this->belongsTo(Balances::class);
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function getDate($format = 'Y-m-d'){
        return $this->date?Carbon::createFromFormat('Y-m-d', $this->date)->format($format):'';
    }
}
