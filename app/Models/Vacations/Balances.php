<?php

namespace App\Models\Vacations;

use App\User;
use App\Models\Vacations\Event;
use App\Models\Vacations\Benefits;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Balances extends Model
{
    use SoftDeletes;
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'balances';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['year','amount', 'pending', 'until', 'benefit_id', 'user_id', 'extraordinary_period'];

	protected $dates = ['until', 'deleted_at'];
	
	public function user()
	{
		return $this->belongsTo(User::class);
	}

    public function benefit() {
        return $this->belongsTo(Benefits::class);
	}
	
	public function getMaxTimePerRequest(){
		return $this->benefit?$this->benefit->getMaxTimePerRequest():null;
	}

	public function getBalance(){
		return $this->benefit->type === 'pending'?$this->pending:"-";
	}

	public function getAmount(){
		return $this->benefit->type === 'pending'?$this->amount:$this->pending;
	}

	public function getProcessed() {
		$processed = 0;
		$events = Event::whereIn('status', ['accepted', 'processing', 'processed'])
                        ->where('js','like','%"id":'.$this->id.',%')->get();

		foreach($events as $event) {
			// Sumo los días del evento procesados a la variable de $processed
			foreach($event->eventDays as $event_day) {
				if($event_day->processed == 1) {
					$processed += 1;
				}
			}
		}

		return $processed;
	}
}
