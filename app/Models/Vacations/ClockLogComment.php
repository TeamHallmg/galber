<?php

namespace App\Models\Vacations;

use Illuminate\Database\Eloquent\Model;

class ClockLogComment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clock_log_comments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['clock_log_id', 'comment'];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['comment', 'created_at'];

    public function clocklog()
    {
        return $this->belongsTo(ClockLog::class, 'clock_log_id', 'id')->orderBy('created_at', 'DESC');
    }
}
