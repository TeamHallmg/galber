<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkshiftFixed extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'workshift_fixed';
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['day','labor','region_id','schedule_id'];

    /**
     * Scope a query to only include laborDay
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLaborDay($query)
    {
        return $query->where('labor', 1);
    }

    public function getInTime(){
        return $this->schedule?$this->schedule->in:'-';
    }

    public function getOutTime(){
        return $this->schedule?$this->schedule->out:'-';
    }

    /**
     * WorkshiftFixed belongs to Region.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
    	// belongsTo(RelatedModel, foreignKey = region_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Region::class);
    }

    /**
     * WorkshiftFixed belongs to Schdedule.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function schedule()
    {
    	// belongsTo(RelatedModel, foreignKey = schdedule_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Schedule::class);
    }
}
