<?php

namespace App\Models;

use App\Employee;
use App\Models\Vacations\Benefits;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobPosition extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'job_positions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'file',
        'experience',
        'knowledge',
        'comments',
        'enterprise',
        'benefits',
        'job_position_level_id',
        'job_position_boss_id',
        'area_id'];

    public function scopeBossTop($query){
        return $query->whereNull('job_position_boss_id');
    }

    public function area(){
        return $this->belongsTo(Area::class, 'area_id', 'id');
    }
    
    public function area_wt(){
        return $this->belongsTo(Area::class, 'area_id', 'id')->withTrashed();
    }
    
    public function level()
    {
        return $this->belongsTo(JobPositionLevel::class, 'job_position_level_id', 'id');
    }

    public function jobs()
    {
        return $this->hasMany(JobPosition::class, 'job_position_boss_id', 'id');
    }

    public function employees() {
        return $this->hasMany(Employee::class, 'job_position_id','id');
    }

    public function deleted_reason() {
        return $this->morphMany(DeletedReason::class, 'deleted_reason');
    }

    public function jobBenefits()
    {
        return $this->belongsToMany(Benefits::class, 'benefit_job', 'job_position_id', 'benefit_id');
    }

    public function getStructureNames(){
        $names = "";
        // $names .= $this->getAreaName(); No hay areas por el momento
        $names .= $this->getDepartmentName();
        $names .= " / ";
        $names .= $this->getDirectionName();
        return $names;
    }

    public function getAreaName(){
        return $this->area?$this->area->name:'';
    }

    public function getDepartmentName(){
        return ($this->area && $this->area->department)?$this->area->department->name:'';
    }

    public function getDirectionName(){
        return ($this->area && $this->area->department && $this->area->department->direction)?$this->area->department->direction->name:'';
    }
    public function Jobtipobienes()
    {
        return $this->hasMany('App\Models\Bienes\jobPositionsBienes', 'id_job_positions', 'id')->where('estatus',1);
    }

    public function courses() {
        // return $this->hasMany(ModelData::class, 'model_id', 'id')->where(['model_name' => 'JobPosition', 'scale_data_type' => 'course']);
        return $this->hasManyThrough(
            Course::class,
            ModelData::class,
            'model_id', // Foreign key on users table...
            'id', // Foreign key on posts table...
            'id', // Local key on countries table...
            'scale_data_id' // Local key on users table...
        )->withTrashed();
    }

    public function getIDCoursesArray() {
        return $this->courses->pluck('id')->toArray();
    }

    public static function getCourses($job_position_id, $ids = false) {
        $courses = [];
        try {
            $job_position = JobPosition::where('id', $job_position_id)->withTrashed()->first();
            $model_data = ModelData::where([
                'model_name' => 'JobPosition',
                'scale_data_type' => 'course'
            ])->get();
            // dd($model_data);

            foreach($model_data as $row) {
                if($row->getJobName() == $job_position->name || $row->model_id == $job_position->id) {
                    // dd('here', $row->course, $row->course->moodle_id, $row->course && $row->course->moodle_id);
                    if($row->course) { //  && $row->course->moodle_id
                        if(!$ids) {
                            $row->course['job_priority'] = $row->priority;
                            $row->course['job_min_grade'] = $row->min_grade;
                            // dd($row, $row->course);
                            $courses[] = $row->course;
                        } else {
                            $courses[] = $row->course->id;
                        }
                    }
                }
            }
        } catch(\Throwable $th) {
            // dd($th);
            return $courses;
        }
        return $courses;
    }

    public static function getJobCourseData($job_position_id, $course_id) {
        $data = [];
        try {
            $job_position = JobPosition::where('id', $job_position_id)->withTrashed()->first();
            $course = Course::where('moodle_id', $course_id)->first();
            $model_data = ModelData::where([
                'model_name' => 'JobPosition',
                'scale_data_type' => 'course',
                'scale_data_id' => $course->id
            ])->get();

            foreach($model_data as $row) {
                if($row->getJobName() == $job_position->name || $row->model_id == $job_position->id) {
                    return [
                        'priority' => $row->priority,
                        'min_grade' => $row->min_grade
                    ];
                }
            }
        } catch(\Throwable $th) {
            // dd($th);
            return $data;
        }
        return $data;
    }
}
