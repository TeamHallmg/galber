<?php

namespace App\Models\Cron;

use Illuminate\Database\Eloquent\Model;

class CronRunLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cron_run_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['date', 'cron_type', 'frequence'];
}
