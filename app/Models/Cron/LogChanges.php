<?php

namespace App\Models\Cron;

use Illuminate\Database\Eloquent\Model;

class LogChanges extends Model
{
    const UPDATED_AT = null;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_changes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['personal_id', 'changes'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at'];

}
