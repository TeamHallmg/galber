<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeletedReason extends Model
{
    protected $table = 'deleted_reasons';

    protected $fillable = ['user_id', 'date', 'reason', 'comment', 'deleted_reason_id', 'deleted_reason_type'];

    public function deleted_reason() {
        return $this->morphTo();
    }

    // Ex:
    // $data = new DeletedReason;
    // $data->user_id = Auth::user()->id;
    // $data->reason = $request->del_reason;
    // if($request->has('exit_date')) {
    //     $data->date = $request->exit_date;
    // }
    // if($request->has('exit_comments')) {
    //     $data->comment = $request->exit_comments;
    // }
    // $user->deleted_reason()->save($data);
    // $user->delete();
}
