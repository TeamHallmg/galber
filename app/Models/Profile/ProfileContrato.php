<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;

class ProfileContrato extends Model
{
    protected $table = 'profile_contrato';

    protected $fillable = [
        'profile_id',
        'nacionalidad',
        'profesion',
        'sede_principal',
        'sede_secundaria',
        'tipo_contrato',
        'horario',
        'salario',
        'salario_base',
        'bono'
    ];

}
