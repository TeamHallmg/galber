<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class ProfileBeneficiariesDet extends Model
{
    // use SoftDeletes;

    protected $table = 'profile_beneficiaries_det';

    // protected $dates = ['deleted_at'];

    protected $fillable = [
        'profile_beneficiaries_id',
        'beneficiarie_name',
        'beneficiarie_sex',
        'date_birth_beneficiarie',
        'file_birth_certificate_beneficiarie',
        'type_beneficiarie'
    ];
}
