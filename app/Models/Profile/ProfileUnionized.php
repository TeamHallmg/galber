<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;

class ProfileUnionized extends Model
{

    protected $table = 'profile_unionized';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id',
        'profile_id',
        'unionized_name'
    ];
}
