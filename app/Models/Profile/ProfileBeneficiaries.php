<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;

class ProfileBeneficiaries extends Model
{
    protected $table = 'profile_beneficiaries';

    protected $fillable = [
        'profile_id',
        'spouse_name',
        'date_marriage',
        'file_marriage',
        'father_name',
        'date_birth_father',
        'file_birth_certificate_father',
        'mother_name',
        'date_birth_mother',
        'file_birth_certificate_mother'
    ];

    public function perfilBeneficiariosDet() {
        return $this->hasMany(ProfileBeneficiariesDet::class);
    }

    public function perfilBeneficiariosDetHijo() {
        return $this->hasMany(ProfileBeneficiariesDet::class)->where('type_beneficiarie', 'HIJO');
    }

    public function perfilBeneficiariosDetOtro() {
        return $this->hasOne(ProfileBeneficiariesDet::class)->where('type_beneficiarie','!=', 'HIJO')->withDefault();
    }
}
