<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;

class ProfileRegistry extends Model
{
    protected $table = 'profile_registries';

    protected $fillable = [
        'profile_id',
        'curp_registry',
        'file_curp_registry',
        'infonavit_registry',
        'file_infonavit_registry',
        'rfc_registry',
        'file_rfc_registry',
        'fonacot_registry',
        'file_fonacot_registry',
        'imss_registry',
        'file_imss_registry',
        'clabe_registry',
        'file_clabe_registry',
        'ine_registry',
        'ine_exp_registry',
        'file_ine_registry',
        'other_registry',
        'file_other_registry'
    ];
}
