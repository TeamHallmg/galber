<?php

namespace App\Models;

use App\Models\GeoDepartamento;
use Illuminate\Database\Eloquent\Model;

class GeoMunicipio extends Model
{
    protected $table = 'geo_municipios';

    public function entidad()
    {
        return $this->belongsTo(GeoDepartamento::class, 'id_geo_departamento', 'id')->withDefault();
    }
}
