<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['shift','in','out'];

    public function workshiftsFixed()
    {
        return $this->hasMany(WorkshiftFixed::class);
    }

    public function getTimeOfWork(){
        $start = new Carbon($this->in);
        $end = new Carbon($this->out);
        return $start->diffInMinutes($end, false);
    }
}
