<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\GeoMunicipio;

class GeoDepartamento extends Model
{
    protected $table = 'geo_departamentos';

    public function municipios()
    {
        return $this->hasMany(GeoMunicipio::class, 'id_geo_departamento', 'id')->orderBy('nombre', 'ASC');
    }
}
