<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class VacationReportExport implements FromArray, ShouldAutoSize, WithHeadings
{
    /**
    * @return \Illuminate\Support\FromArray
    */
    protected $incidents;

    public function __construct(array $incidents)
    {
        $this->incidents = $incidents;
    }

    public function array(): array
    {
        return $this->incidents;
    }

    public function headings(): array
    {
        return [
            'EMP_VACAC',
            'FEDE_VACAC',
            'DD_VACAC',
            'PM_VACAC',
            'OBSINC'
        ];
    }
}
