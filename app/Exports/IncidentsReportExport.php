<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class IncidentsReportExport implements FromArray, ShouldAutoSize, WithHeadings
{
    /**
    * @return \Illuminate\Support\FromArray
    */
    protected $incidents;

    public function __construct(array $incidents)
    {
        $this->incidents = $incidents;
    }

    public function array(): array
    {
        return $this->incidents;
    }

    public function headings(): array
    {
        return [
            'CVE_EMPL',
            'CVE_INC',
            'FECHINC',
            'CANTINC',
            'OBSINC'
        ];
    }
}
