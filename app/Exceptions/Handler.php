<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if($exception instanceof MethodNotAllowedHttpException){
            if($route = $this->redirecTo($request->path())){
                return redirect()->route($route);
            }
        }else if($exception instanceof TokenMismatchException){
            if($request->path() == 'confirm_employee' || $request->path() == 'confirm_employee'){
                return redirect()->route('checklogin');
            }
        }
        return parent::render($request, $exception);
    }

    public function redirecTo($urlPath){
        $urls = [
            'confirm_employee' => 'checklogin',
            'check_employee' => 'checklogin',
            'confirm_employee' => 'checklogin',
        ];
        return isset($urls[$urlPath])?$urls[$urlPath]:null;
    }
}
