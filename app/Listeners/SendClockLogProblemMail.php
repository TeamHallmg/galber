<?php

namespace App\Listeners;

use App\Mail\ClockLogCheckIntent;
use Illuminate\Support\Facades\Mail;
use App\Events\ClockLogFailCheckIntent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendClockLogProblemMail
{
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  ClockLogCreated  $event
     * @return void
     */
    public function handle(ClockLogFailCheckIntent $event)
    {
        try {
            Mail::to($event->user->email)->send(new ClockLogCheckIntent($event->reason));   
        } catch (\Throwable $th) {
            throw $th;
            
        }
    }

}
