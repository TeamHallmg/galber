@extends('layout')

@section('content')
<div class="col-md-2 text-right">
	@include('VacantesInternas/partials/sub-menu')
</div>
<div class="col-md-10">
	<!-- if there are creation errors, they will show here -->
	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>

	<a class="btn btn-small btn-medium_blue" href="{{ URL::previous() }}">Regresar</a>
	<h3 class="strong_blue">PERFIL EXTERNO</h3>
	<hr class="strong_blue_hr" style="margin-bottom: 3rem">

	{{--  {!! Form::open(array('url' => 'profiles','enctype' => 'multipart/form-data')) !!}  --}}

	<div class="strong_blue_background" style="padding: .1rem 2rem; margin-bottom: 1rem">
		<h4 style="color: white">DATOS PERSONALES</h4>
	</div>
	<br>
	<div class="row">
		<div class="col-md-8"></div>
		<div class="col-md-4">
			@if(!is_null($profile->cv_file) && !empty($profile->cv_file))
				<div class="pull-right form-group">
					<label for="file">Currículo</label>
					<a href="{{ asset('file/cv/' . $profile->cv_file) }}" target="_blank" class="btn btn-primary form-control">CV</a>
				</div>
			@endif
		</div>
	</div>
	<br>
	<div class="row">
		<div class="form-group">
			<div class="col-md-4">
				{!! Form::hidden('id', 0, []) !!}
				{!! Form::hidden('job_id', 0,[]) !!}
				{!! Form::label('', 'Nombre: ') !!}
				{!! Form::text('first_name', $profile->externo->name, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-4">
				{!! Form::label('', 'R.F.C.: ') !!}
				{!! Form::text('rfc', $profile->externo->rfc, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-3">
				{!! Form::label('', 'Fecha de Nacimiento: ') !!}
				{!! Form::date('date_birth', $profile->externo->date_birth, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="form-group">
			<div class="col-md-4">
				{!! Form::label('', 'C.U.R.P.: ') !!}
				{!! Form::text('curp', $profile->externo->curp, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-4">
				{!! Form::label('', 'Correo electrónico: ') !!}
				{!! Form::text('email', $profile->externo->email, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
	{{-- <br><br> --}}

{{-- 	<div style="width: 100%; height: 17px; font-size: 16pt; font-weight: bold; border-bottom: 2px solid #044A90;">
		<span style="background-color: #fff; color:#044A90; padding-right: 5px;">
			DATOS DEL PUESTO
		</span>
	</div>
	<br>

	<div class="row">
		<div class="form-group">
			<div class="col-md-6">
				{!! Form::label('', 'Nombre del puesto: ') !!}
				{!! Form::text('puesto', isset($job->puesto) ? $job->puesto : '', ['class'=>'form-control', 'readonly'=>'readonly']) !!}
				{!! Form::hidden('job_id', isset($job->id) ? $job->id : '', []) !!}
			</div>
			<div class="col-md-6">
				{!! Form::label('', 'Departamento: ') !!}
				{!! Form::text('jobDepartment', isset($job->jobDepartment) ? $job->jobDepartment : '', ['class'=>'form-control', 'readonly'=>'readonly']) !!}
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="form-group">
			<div class="col-md-4">
				{!! Form::label('', 'Descripción breve: ') !!}
				{!! Form::textarea('jobDescription', isset($job->jobDescription) ? $job->jobDescription : '', ['class'=>'form-control','rows'=>'3', 'style'=>'resize: none;', 'readonly'=>'readonly']) !!}
			</div>
			<div class="col-md-4">
				{!! Form::label('', 'Experiencia: ') !!}
				{!! Form::textarea('experience', isset($job->experience) ? $job->experience : '', ['class'=>'form-control','rows'=>'3', 'style'=>'resize: none;', 'readonly'=>'readonly']) !!}
			</div>
			<div class="col-md-4">
				{!! Form::label('', 'Conocimientos: ') !!}
				{!! Form::textarea('knowledge', isset($job->knowledge) ? $job->knowledge : '', ['class'=>'form-control','rows'=>'3', 'style'=>'resize: none;', 'readonly'=>'readonly']) !!}
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="form-group">
			<div class="col-md-8">
				{!! Form::label('', 'Comentarios: ') !!}
				{!! Form::textarea('comment', isset($job->comment) ? $job->comment : '', ['class'=>'form-control','rows'=>'3', 'style'=>'resize: none;', 'readonly'=>'readonly']) !!}
			</div>
			{{-- <div class="col-md-4">
				{!! Form::label('', 'Archivo: ') !!}
				@if (!empty($job->file))
					{{ $archivo = $job->file }}
				@else
					{{ $archivo = '' }}
				@endif
				<br>
				<a class="btn btn-primary" href="{{ asset('img/puesto/'. $archivo) }}" target="_blank"><i class="glyphicon glyphicon-file"></i>abrir archivo</a>
			</div> --}
		</div>
	</div>
	<br><br> --}}

	{{-- <div style="width: 100%; height: 17px; font-size: 16pt; font-weight: bold; border-bottom: 2px solid #044A90;">
		<span style="background-color: #fff; color:#044A90; padding-right: 5px;">
			DATOS PERSONALES
		</span>
	</div> --}}
	<br>
	<div class="row">
		<div class="form-group">
	    	<div class="col-md-6">
				{!! Form::label('', 'Domicilio: ') !!}
				{!! Form::text('address', $profile->address, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-2">
				{!! Form::label('', 'No. Casa: ') !!}
				{!! Form::text('number_ext', $profile->number_ext, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-2">
				{!! Form::label('', 'No. Interior: ') !!}
				{!! Form::text('number_int', $profile->number_int, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-2">
				{!! Form::label('', 'Colonia: ') !!}
				{!! Form::text('colony', $profile->colony, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="form-group">
	    	<div class="col-md-3">
				{!! Form::label('', 'Teléfono: ') !!}
				{!! Form::text('phone', $profile->phone, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-3">
				{!! Form::label('', 'Celular: ') !!}
				{!! Form::text('cel_phone', $profile->cel_phone, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="form-group">
	    	<div class="col-md-4">
				{!! Form::label('', 'Estado civil: ') !!}
				{!! Form::select('civil_status', [
					''=>'Estado civil',
					'Soltero'=>'Soltero/a',
					'Comprometido'=>'Comprometido/a',
					'Casado'=>'Casado/a',
					'Divorsiado'=>'Divorsiado/a',
					'Viudo'=>'Viudo/a'
					], $profile->civil_status, ['class'=>'form-control'])
				!!}
			</div>
			<div class="col-md-4">
				{!! Form::label('', 'Nombre cónyuge: ') !!}
				{!! Form::text('spouse_name', $profile->spouse_name, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-4">
				{!! Form::label('', 'Nacimiento cónyuge: ') !!}
				{!! Form::date('date_birth_spouse', $profile->date_birth_spouse, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
	<br>
	<div class="row agregar_hijo">
		<div class="col-md-12" style="margin-bottom: 2rem">
			<div class="medium_blue_background" style="padding: .1rem 1rem; color: white">
				<h5 style="font-size: 14px">HIJOS</h5>
			</div>
		</div>
	</div>
	@foreach ($profile->hijos as $hijo)
		<div class="row">
			<div class="col-md-4">
				{!! Form::label('', 'Nombre: ') !!}
				{!!  Form::text('name_child[]', $hijo->name, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-4">
				{!! Form::label('', 'Edad: ') !!}
				{!! Form::text('age_child[]', $hijo->age, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-3">
				{!! Form::label('', 'Fecha Nacimiento: ') !!}
				{!! Form::date('date_birth_child[]', $hijo->date_birth, ['class'=>'form-control']) !!}
			</div>
		</div>	
	@endforeach
	
	<br>
	<div class="row">
		<br>
		<div class="col-md-12" style="margin-bottom: 2rem">
			<div class="medium_blue_background" style="padding: .1rem 1rem; color: white">
				<h5 style="font-size: 14px">NOMBRE DE TÚS PADRES</h5>
			</div>
		</div>
		<div class="form-group">
	    	<div class="col-md-6">
				{!! Form::label('', 'Nombre del padre: ') !!}
				{!! Form::text('father_name', $profile->father_name, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-6">
				{!! Form::label('', 'Nombre de la madre: ') !!}
				{!! Form::text('mother_name', $profile->mother_name, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
	<br><br>

	<div class="strong_blue_background" style="padding: .1rem 2rem; margin-bottom: 1rem">
		<h4 style="color: white">FORMACIÓN ACADÉMICA</h4>
	</div>
	<br>
	@foreach($profile->escolaridad as $escolaridad)
		<div class="row">
			<div class="col-md-4">
				{!! Form::label('', 'Nivel de escolaridad: ') !!}
				{!! Form::select('studio[]', [
					''=>'Nivel de escolaridad...',
					'secundaria'=>'Secundaria',
					'bachillerato'=>'Bachillerato',
					'profesional tecnica'=>'Profesional Técnica',
					'tecnico superior'=>'Técnico Superior',
					'licenciatura'=>'Licenciatura',
					'maestria'=>'Maestría',
					'doctorado'=>'Doctorado',
					'diplomado'=>'Diplomado'
				], $escolaridad->studio, ['class'=>'form-control'])
				!!}
			</div>
			<div class="col-md-4">
				{!! Form::label('', 'Carrera: ') !!}
				{!! Form::text('career[]', $escolaridad->career, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-3">
				{!! Form::label('', 'Institución: ') !!}
				{!! Form::text('school[]', $escolaridad->school, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-4">
				{!! Form::label('', 'Fecha inicio: ') !!}
				{!! Form::date('date_begin_scholarship[]', $escolaridad->date_begin, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-4">
				{!! Form::label('', 'Fecha termino: ') !!}
				{!! Form::date('date_end_scholarship[]', $escolaridad->date_end, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-3">
				{!! Form::label('', 'Especialidad: ') !!}
				{!! Form::text('speciality[]', $escolaridad->speciality, ['class'=>'form-control']) !!}
			</div>
		</div>
	@endforeach
	<br>
	@foreach ($profile->idiomas as $idioma)
		<div class="row">
			<div class="form-group">
				<div class="col-md-5">
					{!! Form::label('', 'Idioma: ') !!}
					{!! Form::text('language[]', $idioma->language, ['class'=>'form-control']) !!}
				</div>
				<div class="col-md-2">
					{!! Form::label('', 'Conversación: ') !!}
					{!! Form::text('spoken[]', $idioma->spoken, ['class'=>'form-control']) !!}
				</div>
				<div class="col-md-2">
					{!! Form::label('', 'Lectura: ') !!}
					{!! Form::text('reading[]', $idioma->reading, ['class'=>'form-control']) !!}
				</div>
				<div class="col-md-2">
					{!! Form::label('', 'Escritura: ') !!}
					{!! Form::text('writing[]', $idioma->writing, ['class'=>'form-control']) !!}
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	@endforeach
	<br>
	@foreach($profile->softwares as $software)
		<div class="row">
			<div class="form-group">
				<div class="col-md-6">
					{!! Form::label('', 'Software: ') !!}
					{!! Form::text('name_software[]', $software->name, ['class'=>'form-control']) !!}
				</div>
				<div class="col-md-5">
					{!! Form::label('', 'Nivel: ') !!}
					{!! Form::select('level[]', [
							''=>'Elige el adecuado...',
							'Basico'=>'Básico',
							'Medio'=>'Medio',
							'Avanzado'=>'Avanzado'
						], $software->level, ['class'=>'form-control'])
					!!}
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	@endforeach
	<br>
	<div class="row">
		<div class="form-group">
			<div class="col-md-6">
				{!! Form::label('', 'Curso(s): ') !!}
				{!! Form::textarea('course', !is_null($profile->estudios)?$profile->estudios->course:'', ['class'=>'form-control','rows'=>'3', 'style'=>'resize: none;']) !!}
			</div>
			<div class="col-md-6">
				{!! Form::label('', 'Certificación(es): ') !!}
				{!! Form::textarea('certifications', !is_null($profile->estudios)?$profile->estudios->certifications:'', ['class'=>'form-control','rows'=>'3', 'style'=>'resize: none;']) !!}
			</div>
		</div>
	</div>
	<br><br>
	<div class="strong_blue_background" style="padding: .1rem 2rem; margin-bottom: 1rem">
		<h4 style="color: white">EXPERIENCIA PROFESIONAL</h4>
	</div>
	<br>
	@foreach ($profile->experiencias as $experiencia)	
		<div class="row">
			<div class="col-md-4">
				{!! Form::label('', 'Empresa: ') !!}
				{!! Form::text('company[]', $experiencia->company, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-4">
				{!! Form::label('', 'Fecha inicio: ') !!}
				{!! Form::date('date_begin_experience[]', $experiencia->date_begin, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-3">
				{!! Form::label('', 'Fecha termino: ') !!}
				{!! Form::date('date_end_experience[]', $experiencia->date_end, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-4">
				{!! Form::label('', 'Puesto: ') !!}
				{!! Form::text('job_experience[]', $experiencia->activity, ['class'=>'form-control']) !!}
			</div>
			<div class="col-md-7">
				{!! Form::label('', 'Actividades desarrolladas: ') !!}
				{!! Form::text('activity[]', $experiencia->job, ['class'=>'form-control']) !!}
			</div>
		</div>
	@endforeach
	<hr class="strong_blue_hr" style="margin: 2rem 0 1rem 0">
	<div class="row">
		<div class="col-xs-4">
			<a class="btn btn-small btn-medium_blue" href="{{ URL::previous() }}">Regresar</a>
		</div>
		<div class="col-xs-8">
		</div>
	</div>
	<br>
	{{--  {!! Form::close() !!}  --}}
</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			//timepo en mostrar los mensajes
			setTimeout(function() {
				$('#mensaje').slideUp('slow');
			},3000);

			//funciones para agregar input text
			var maxCampos = 5; //Máximo de campos a insertar

			/*Agregar renglones para los campos de hijo*/
			var hijosHTML = '<div class="form-group" id="campos_hijo"> \
				<div class="col-md-4"> \
					{!! Form::label('', 'Nombre: ') !!} \
					{!! Form::text('name_child[]', '', ['class'=>'form-control']) !!} \
				</div> \
				<div class="col-md-4"> \
					{!! Form::label('', 'Edad: ') !!} \
					{!! Form::text('age_child[]', '', ['class'=>'form-control']) !!} \
				</div> \
				<div class="col-md-3"> \
					{!! Form::label('', 'Fecha Nacimiento: ') !!} \
					{!! Form::date('date_birth_child[]', '', ['class'=>'form-control']) !!} \
				</div> \
				<div class="col-md-1"> \
					{!! Form::label('', 'Eliminar: ') !!} \
					<a href="javascript:void(0);" class="btn_eliminar_hijo btn btn-danger btn-gly_pd" title="Eliminar Hijos"><span class="big-gly glyphicon glyphicon-remove"></span></a> \
				</div> \
			</div>';

			var x = 1; //valor inicial
			$('.btn_agregar_hijo').click(function() { //boton agregar renglon
				if(x < maxCampos) {
					x++;
					$('.agregar_hijo').append(hijosHTML); //agregar el campo preformateado
				}
			});

			$('.agregar_hijo').on('click', '.btn_eliminar_hijo', function(e){ //boton eliminar renglon
				e.preventDefault();
				$('#campos_hijo').remove(); //eliminar el campo preformateado
					x--;
			});

			/*Agregar renglones para los campos de estudio (FORMACIÓN ACADÉMICA)*/
			var estudioHTML = '<div class="form-group" id="campos_estudio"> \
				<div class="col-md-4"> \
					{!! Form::label('', 'Nivel de escolaridad: ') !!} \
					{!! Form::select('studio[]', [
						''=>'Nivel de escolaridad...',
						'secundaria'=>'Secundaria',
						'bachillerato'=>'Bachillerato',
						'profesional tecnica'=>'Profesional Técnica',
						'tecnico superior'=>'Técnico Superior',
						'licenciatura'=>'Licenciatura',
						'maestria'=>'Maestría',
						'doctorado'=>'Doctorado',
						'diplomado'=>'Diplomado'
						], null, ['class'=>'form-control'])	!!} \
				</div> \
				<div class="col-md-4"> \
					{!! Form::label('', 'Carrera: ') !!} \
					{!! Form::text('career[]', '', ['class'=>'form-control']) !!} \
				</div> \
				<div class="col-md-3"> \
					{!! Form::label('', 'Institución: ') !!} \
					{!! Form::text('school[]', '', ['class'=>'form-control']) !!} \
				</div> \
				<div class="col-md-1"> \
					{!! Form::label('', 'Eliminar: ') !!} \
					<a href="javascript:void(0);" class="btn_eliminar_estudio btn btn-danger btn-gly_pd" title="Eliminar Hijos"><span class="big-gly glyphicon glyphicon-remove"></span></a> \
				</div> \
				<div class="col-md-4"> \
					{!! Form::label('', 'Fecha inicio: ') !!} \
					{!! Form::date('date_begin_scholarship[]', '', ['class'=>'form-control']) !!} \
				</div> \
				<div class="col-md-4"> \
					{!! Form::label('', 'Fecha termino: ') !!} \
					{!! Form::date('date_end_scholarship[]', '', ['class'=>'form-control']) !!} \
				</div> \
				<div class="col-md-3"> \
					{!! Form::label('', 'Especialidad: ') !!} \
					{!! Form::text('speciality[]', '', ['class'=>'form-control']) !!} \
				</div> \
			</div>';

			var y = 1; //valor inicial
			$('.btn_agregar_estudio').click(function() { //boton agregar renglon
				if(y < maxCampos) {
					y++;
					$('.agregar_estudio').append(estudioHTML); //agregar el campo preformateado
				}
			});

			$('.agregar_estudio').on('click', '.btn_eliminar_estudio', function(e){ //boton eliminar renglon
				e.preventDefault();
				$('#campos_estudio').remove(); //eliminar el campo preformateado
					y--;
			});

			/*Agregar renglones para los campos de idioma (FORMACIÓN ACADÉMICA)*/
			var idiomaHTML = '<div class="form-group" id="campos_idioma"> \
				<div class="col-md-5"> \
					{!! Form::label('', 'Idioma: ') !!} \
					{!! Form::text('language[]', '', ['class'=>'form-control']) !!} \
				</div> \
				<div class="col-md-2"> \
					{!! Form::label('', 'Conversación: ') !!} \
					{!! Form::text('spoken[]', '', ['class'=>'form-control']) !!} \
				</div> \
				<div class="col-md-2"> \
					{!! Form::label('', 'Lectura: ') !!} \
					{!! Form::text('reading[]', '', ['class'=>'form-control']) !!} \
				</div> \
				<div class="col-md-2"> \
					{!! Form::label('', 'Escritura: ') !!} \
					{!! Form::text('writing[]', '', ['class'=>'form-control']) !!} \
				</div> \
				<div class="col-md-1"> \
					{!! Form::label('', 'Eliminar: ') !!} \
					<a href="javascript:void(0);" class="btn_eliminar_idioma btn btn-danger btn-gly_pd" title="Eliminar Idioma"><span class="big-gly glyphicon glyphicon-remove"></span></a> \
				</div> \
			</div>';

			var z = 1; //valor inicial
			$('.btn_agregar_idioma').click(function() { //boton agregar renglon
				if(z < maxCampos) {
					z++;
					$('.agregar_idioma').append(idiomaHTML); //agregar el campo preformateado
				}
			});

			$('.agregar_idioma').on('click', '.btn_eliminar_idioma', function(e){ //boton eliminar renglon
				e.preventDefault();
				$('#campos_idioma').remove(); //eliminar el campo preformateado
					z--;
			});


			/*Agregar renglones para los campos de software (FORMACIÓN ACADÉMICA)*/
			var softwareHTML = '<div class="form-group" id="campos_software"> \
				<div class="col-md-6"> \
					{!! Form::label('', 'Software: ') !!} \
					{!! Form::text('name_software[]', '', ['class'=>'form-control']) !!} \
				</div> \
				<div class="col-md-5"> \
					{!! Form::label('', 'Nivel: ') !!} \
					{!! Form::select('level[]', [
							'Basico'=>'Básico',
							'Medio'=>'Medio',
							'Avanzado'=>'Avanzado'
						], null, ['class'=>'form-control'])
					!!} \
				</div> \
				<div class="col-md-1"> \
					{!! Form::label('', 'Eliminar: ') !!} \
					<a href="javascript:void(0);" class="btn_eliminar_software btn btn-danger btn-gly_pd" title="Eliminar Software"><span class="big-gly glyphicon glyphicon-remove"></span></a> \
				</div> \
			</div>';

			var w = 1; //valor inicial
			$('.btn_agregar_software').click(function() { //boton agregar renglon
				if(w < maxCampos) {
					w++;
					$('.agregar_software').append(softwareHTML); //agregar el campo preformateado
				}
			});

			$('.agregar_software').on('click', '.btn_eliminar_software', function(e){ //boton eliminar renglon
				e.preventDefault();
				$('#campos_software').remove(); //eliminar el campo preformateado
					w--;
			});


			/*Agregar renglones para los campos de experiencia profesional*/
			var experienciaHTML = '<div class="form-group" id="campos_experiencia"> \
				<div class="col-md-4"> \
					{!! Form::label('', 'Empresa: ') !!} \
					{!! Form::text('company[]', '', ['class'=>'form-control']) !!} \
				</div> \
				<div class="col-md-4"> \
					{!! Form::label('', 'Fecha inicio: ') !!} \
					{!! Form::date('date_begin_experience[]', '', ['class'=>'form-control']) !!} \
				</div> \
				<div class="col-md-3"> \
					{!! Form::label('', 'Fecha termino: ') !!} \
					{!! Form::date('date_end_experience[]', '', ['class'=>'form-control']) !!} \
				</div> \
				<div class="col-md-1"> \
					{!! Form::label('', 'Eliminar: ') !!} \
					<a href="javascript:void(0);" class="btn_eliminar_experiencia btn btn-danger btn-gly_pd" title="Eliminar Hijos"><span class="big-gly glyphicon glyphicon-remove"></span></a> \
				</div> \
				<div class="col-md-4"> \
					{!! Form::label('', 'Puesto: ') !!} \
					{!! Form::text('job_experience[]', '', ['class'=>'form-control']) !!} \
				</div> \
				<div class="col-md-7"> \
					{!! Form::label('', 'Actividades desarrolladas: ') !!} \
					{!! Form::text('activity[]', '', ['class'=>'form-control']) !!} \
				</div> \
			</div>';

			var ww = 1; //valor inicial
			$('.btn_agregar_experiencia').click(function() { //boton agregar renglon
				if(ww < maxCampos) {
					ww++;
					$('.agregar_experiencia').append(experienciaHTML); //agregar el campo preformateado
				}
			});

			$('.agregar_experiencia').on('click', '.btn_eliminar_experiencia', function(e){ //boton eliminar renglon
				e.preventDefault();
				$('#campos_experiencia').remove(); //eliminar el campo preformateado
					ww--;
			});

		});
	</script>
@endsection