<div class="container-fluid">
    <h5>Datos Generales</h5>
    @isset($profile)
        <div class="row">
            <div class="form-group col-3">
                <div class="row">
                    <div class="form-group col text-center">
                        @if (isset($profile) && !is_null($profile->image))
                            <img src="{{ asset('uploads/profile/'.$profile->image) }}" class="mb-3" style="max-width: 270px; max-height: 183px">
                        @else
                            <img src="{{ asset('img/vacantes/sinimagen.png') }}" class="mb-3" style="max-width: 300px; max-height: 183px">
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-9">
                <div class="row">
                    <input type="hidden" name="user_id" value="{{ $user_id }}">
                    <div class="form-group col">
                        <label for="name" class="requerido">Nombre(s)</label>
                        <input type="text" class="form-control" value="{{ $profile->name }}" readonly={{ is_null($user) ? true : false }}">
                    </div>
                    <div class="form-group col">
                        <label for="surname_father" class="requerido">Apellido Paterno</label>
                        <input type="text" name="surname_father" id="surname_father" class="form-control" value="{{ $profile->surname_father }}" readonly={{ is_null($user) ? true : false }}">
                    </div>
                    <div class="form-group col">
                        <label for="surname_mother" class="requerido">Apellido Materno</label>
                        <input type="text" name="surname_mother" id="surname_mother" class="form-control" value="{{ $profile->surname_mother }}" readonly={{ is_null($user) ? true : false }}">
                    </div>
                </div>
    
                <div class="row">
                    <div class="form-group col">
                        <label for="cellphone" class="requerido">Número de Celular</label>
                        <input type="text" name="cellphone" id="cellphone" class="form-control" value="{{ $profile->cellphone }}" readonly={{ is_null($user) ? true : false }}">
                    </div>
    
                    <div class="form-group col">
                        <label for="phone" class="requerido">Número de Teléfono Fijo</label>
                        <input type="text" name="phone" id="phone" class="form-control" value="{{ $profile->phone }}" readonly={{ is_null($user) ? true : false }}">
                    </div>
    
                    <div class="form-group col">
                        <label for="email" class="requerido">Correo Electrónico</label>
                        <input type="text" name="email" id="email" class="form-control" value="{{ $profile->email }}" readonly={{ is_null($user) ? true : false }}">
                    </div>
                </div>
    
                <div class="row">
                    <div class="form-group col">
                        <label for="gender" class="requerido">Sexo</label>
                        <input type="text" name="gender" id="gender" class="form-control" value="{{ $profile->gender }}" readonly={{ is_null($user) ? true : false }}">
                    </div>
                    <div class="form-group col">
                        <label for="date_birth" class="requerido">Fecha de Nacimiento</label>
                        <input type="date" name="date_birth" id="date_birth" class="form-control" value="{{ $profile->date_birth }}" readonly={{ is_null($user) ? true : false }}">
                    </div>
                    <div class="form-group col">
                        <label for="rfc">Estado Civil</label>
                        <input class="form-control mb-3" type="text" name="rfc" id="rfc" value="{{ (is_null($profile) ? null : is_null($profile->user->employee) ? null : $profile->user->employee->civil) }}" disabled>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col">
                        <label for="age" class="requerido">Edad</label>
                        <input type="text" id="age" class="form-control" value="{{ $profile->getAge() }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
                    <div class="form-group col">
                        <label for="igss" class="requerido">IMSS</label>
                        <input type="text" id="igss" class="form-control"  value="{{ (is_null($profile) ? null : is_null($profileRegistros) ? null : $profileRegistros->imss_registry) }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
                    <div class="form-group col">
                         <label for="rfc" class="requerido">RFC</label>
                         <input type="text" id="igss" class="form-control"  value="{{ (is_null($profile) ? null : is_null($profileRegistros) ? null : $profileRegistros->rfc_registry) }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
                </div>
            </div>
        </div>
        <br>

        @if(!is_null($user))
            <h5>ÚLTIMO GRADO DE ESCOLARIDAD</h5>
            @foreach ($profile->perfilEscuela as $escuela )
                @if($escuela->type == 'ultimo')
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="career" class="requerido">Nivel alcanzado</label>
                            <input type="text" name="career_basica[]" id="career_basica" class="form-control" value="{{ $escuela->career }}">
                        </div>
                        <div class="form-group col-4">
                            <label for="school" class="requerido">Institución que expidió constancia</label>
                            <input type="text" name="school_basica[]" id="school_basica" class="form-control" value="{{ $escuela->school }}">
                        </div>
                        <div class="form-group col-4">
                            <label for="date_end" class="requerido">Fecha terminación</label>
                            <input type="date" name="date_end_basica[]" id="date_end_basica" class="form-control" value="{{ $escuela->date_end }}">
                        </div>
                    </div>
                @endif  
            @endforeach
            <br>

            @if(!is_null(Auth::user()->external))
                <h5>PUESTO(S) DE INTERÉS</h5>
                <div class="row">
                    <div class="form-group col">
                        <label for="puesto" class="requerido">Nombre del puesto</label>
                        <input type="text" name="puesto" id="puesto" class="form-control" value="{{ $puesto }}" readonly>
                    </div>
                </div> 
                <br>
            @endif
            
            <h5>CÓMO ME ENTERÉ DE LA VACANTE</h5>
            <div class="row">
                <div class="form-group col">
                    <input type="radio" name="know_vacancy" id="know_vacancy" value="empresa" {{ ($profile->know_vacancy == 'empresa') ? 'checked' : ''}}> 
                    <label for="">Página de la empresa</label>
                </div>
                <div class="form-group col">
                    <input type="radio" name="know_vacancy" id="know_vacancy" value="facebook" {{ ($profile->know_vacancy == 'facebook') ? 'checked' : ''}}>
                    <label for="">Facebook</label>
                </div>
                <div class="form-group col">
                    <input type="radio" name="know_vacancy" id="know_vacancy" value="linkedin" {{ ($profile->know_vacancy == 'linkedin') ? 'checked' : ''}}>
                    <label for="">Linkedin</label>
                </div>
                <div class="form-group col">
                    <input type="radio" name="know_vacancy" id="know_vacancy" value="computrabajo" {{ ($profile->know_vacancy == 'computrabajo') ? 'checked' : ''}}>
                    <label for="">Computrabajo</label>
                </div>
                <div class="form-group col">
                    <input type="radio" name="know_vacancy" id="know_vacancy" value="indeed" {{ ($profile->know_vacancy == 'indeed') ? 'checked' : ''}}>
                    <label for="">Indeed</label>
                </div>
            </div>
            <div class="row">
                <div class="form-group col">
                    <input type="radio" name="know_vacancy" id="know_vacancy" value="empleado" {{ ($profile->know_vacancy == 'empleado') ? 'checked' : ''}}>
                    <label for="">Referido por un empleado</label>
                </div>
                <div class="form-group col">
                    <input type="radio" name="know_vacancy" id="know_vacancy" value="rh" {{ ($profile->know_vacancy == 'rh') ? 'checked' : ''}}>
                    <label for="">Grupo de RH</label>
                </div>
                <div class="form-group col">
                    <input type="radio" name="know_vacancy" id="know_vacancy" value="volante" {{ ($profile->know_vacancy == 'volante') ? 'checked' : ''}}>
                    <label for="">Volante</label>
                </div>
                <div class="form-group col">
                    <input type="radio" name="know_vacancy" id="know_vacancy" value="periodico" {{ ($profile->know_vacancy == 'periodico') ? 'checked' : ''}}>
                    <label for="">Anuncio Periódico</label>
                </div>
                <div class="form-group col">
                    <input type="radio" name="know_vacancy" id="know_vacancy" value="llamada" {{ ($profile->know_vacancy == 'llamada') ? 'checked' : ''}}>
                    <label for="">Me Llamaron</label>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="form-group col">
                    <label for="">Otros:</label>
                    <textarea name="know_vacancy_other" id="know_vacancy_other" cols="30" rows="5" style="resize:none;" class="form-control">{{ $profile->know_vacancy_other }}</textarea>
                </div>
            </div>
            <br>

            @if (!is_null($profile->file_cv))
                <div class="row">
                    <a class="btn morado" href="{{ asset('/uploads/profile/'. $profile->file_cv) }}" target="_blank">
                        <i class="fa fa-file-alt"></i>VER CV
                    </a>
                </div>
            @endif
        @endif

        <div class="row">
            <div class="col text-right">
                <button type="submit" class="btn morado">Actualizar</button>
            </div>
        </div>

    @else
        <div class="form-row">
            <div class="col-12 col-md-3 text-center">
                <img src="{{ asset('img/vacantes/sinimagen.png') }}" class="mb-3" style="max-width: 300px; max-height: 183px">
            </div>
            <div class="col-9">
                <div class="row">
                    <input type="hidden" name="user_id" value="{{ $user_id }}">
                    <div class="form-group col">
                        <label for="name" class="requerido">Nombre</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->nombre : $user->first_name : null }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
                    <div class="form-group col">
                        <label for="surname_father" class="requerido">Apellido Paterno</label>
                        <input type="text" name="surname_father" id="surname_father" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->paterno : $user->last_name : null }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
                    <div class="form-group col">
                        <label for="surname_mother" class="requerido">Apellido Materno</label>
                        <input type="text" name="surname_mother" id="surname_mother" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->materno : null : null }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
                </div>
    
                <div class="row">
                    <div class="form-group col">
                        <label for="cellphone" class="requerido">Número de Celular</label>
                        <input type="text" name="cellphone" id="cellphone" class="form-control">
                    </div>
                    <div class="form-group col">
                        <label for="phone" class="requerido">Número de Teléfono Fijo</label>
                        <input type="text" name="phone" id="phone" class="form-control">
                    </div>
                    <div class="form-group col">
                        <label for="email" class="requerido">Correo Electrónico</label>
                        <input type="text" name="email" id="email" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->correoempresa : $user->email : null }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
                </div>
    
                <div class="row">
                    <div class="form-group col">
                        <label for="gender" class="requerido">Sexo</label>
                        <input type="text" name="gender" id="gender" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->sexo : null : null }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
                    <div class="form-group col">
                        <label for="date_birth" class="requerido">Fecha de Nacimiento</label>
                        <input type="date" name="date_birth" id="date_birth" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->nacimiento : null : null }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
                    <div class="form-group col">
                         <label for="rfc">Estado Civil</label>
                        <input class="form-control mb-3" type="text" name="rfc" id="rfc" value="{{ (is_null($user) ? null : is_null($user->employee) ? null : $user->employee->civil) }}" disabled>                       
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col">
                        <label for="age" class="requerido">Edad</label>
                        <input type="text" id="age" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->getAge() : null : null }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
                    <div class="form-group col">
                        <label for="igss" class="requerido">IMSS</label>
                        <input type="text" name="irtra" id="irtra" class="form-control" value="{{ !is_null($profile) ? !is_null($profileRegistros) ? $profileRegistros->imss_registry : null : null }}" readonly={{ is_null($user) ? false : true }}>
                            
                    </div>
                    <div class="form-group col">
                        <label for="rfc" class="requerido">RFC</label>    
                        <input type="text" name="irtra" id="irtra" class="form-control" value="{{ !is_null($profile) ? !is_null($profileRegistros) ? $profileRegistros->rfc_registry : null : null }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
                </div>
            </div>
        </div>
        <br>

        @if(is_null($user))
            <h5>ÚLTIMO GRADO DE ESCOLARIDAD</h5>
                <div class="float-right">
                    <button type="button" class="btn btn-success" id="agregar" style="margin-top: 32px;">
                        <span class="fa fa-plus"></span>
                    </button>
                </div>
                
                <div id="escolar">
                    <div class="row">
                        <input type="hidden" name="type_basica[]" value="ultimo">
                        <div class="form-group col-4">
                            <label for="career_basica" class="requerido">Nivel alcanzado</label>
                            <input type="text" name="career_basica[]" id="career_basica[]" class="form-control">
                        </div>

                        <div class="form-group col-4">
                            <label for="school_basica" class="requerido">Institución que expidió constancia</label>
                            <input type="text" name="school_basica[]" id="school_basica[]" class="form-control">
                        </div>

                        <div class="form-group col-3">
                            <label for="date_end_basica" class="requerido">Fecha terminación</label>
                            <input type="date" name="date_end_basica[]" id="date_end_basica[]" class="form-control">
                        </div>

                        <div class="form-group col-1 text-right">
                            <button type="button" class="btn btn-danger" id="borrar" style="margin-top: 32px;">
                                <span class="fa fa-minus"></span>
                            </button>
                        </div>
                    </div>
                </div>
        
                <div id="destino"></div>
            <br>

            {{-- <h5>PUESTO(S) DE INTERÉS</h5>
            <div class="row">
                <div class="form-group col">
                    <label for="puesto" class="requerido">Nombre del puesto</label>
                    <input type="text" name="puesto" id="puesto" class="form-control" value="{{ $puesto }}" readonly>
                </div>
            </div> 
            <br> --}}

            <h5>CÓMO ME ENTERÉ DE LA VACANTE</h5>
            <div class="row">
                <div class="form-group col">
                    <input type="radio" name="know_vacancy[]" id="know_vacancy" value="empresa" required> 
                    <label for="">Página de la empresa</label>
                </div>
                <div class="form-group col">
                    <input type="radio" name="know_vacancy[]" id="know_vacancy" value="facebook" required>
                    <label for="">Facebook</label>
                </div>
                <div class="form-group col">
                    <input type="radio" name="know_vacancy[]" id="know_vacancy" value="linkedin" required>
                    <label for="">Linkedin</label>
                </div>
                <div class="form-group col">
                    <input type="radio" name="know_vacancy[]" id="know_vacancy" value="computrabajo" required>
                    <label for="">Computrabajo</label>
                </div>
                <div class="form-group col">
                    <input type="radio" name="know_vacancy[]" id="know_vacancy" value="indeed" required>
                    <label for="">Indeed</label>
                </div>
            </div>
            <div class="row">
                <div class="form-group col">
                    <input type="radio" name="know_vacancy[]" id="know_vacancy" value="empleado" required>
                    <label for="">Referido por un empleado</label>
                </div>
                <div class="form-group col">
                    <input type="radio" name="know_vacancy[]" id="know_vacancy" value="rh" required>
                    <label for="">Grupo de RH</label>
                </div>
                <div class="form-group col">
                    <input type="radio" name="know_vacancy[]" id="know_vacancy" value="volante" required>
                    <label for="">Volante</label>
                </div>
                <div class="form-group col">
                    <input type="radio" name="know_vacancy[]" id="know_vacancy" value="periodico" required>
                    <label for="">Anuncio Periódico</label>
                </div>
                <div class="form-group col">
                    <input type="radio" name="know_vacancy[]" id="know_vacancy" value="llamada" required>
                    <label for="">Me Llamaron</label>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="form-group col">
                    <label for="">Otros:</label>
                    <textarea name="know_vacancy_other" id="know_vacancy_other" cols="30" rows="5" style="resize:none;" class="form-control"></textarea>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="form-group col-3">
                    <button type="button" id="solicitud" class="btn morado" data-toggle="modal" data-target="#solicitud-modal" data-backdrop="static" data-keyboard="false">
                        <i class="fa fa-file-alt">&nbsp;&nbsp;Llenar solicitud</i>
                    </button>
                </div>
                <div class="custom-file form-group col-4">                
                    <input type="file" name="file[file_cv]" class="form-control custom-file-input" id="file_cv" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="file_cv" data-browse="Subir cv"></label>
                </div>
            </div>
        @endif
        
        {{-- @if(!isset($user_id))
            <input type="hidden" name="user_id" value="{{$user_id}}">
        @endif --}}

        <div class="row">
            <div class="col-10">
                Ratifico que todos los datos asentados son verídicos y que, en caso de probarse lo contrario, se cancelará mi registro.
            </div>
            <div class="col text-right">
                <button type="submit" class="btn morado">{{ ($crear) ? 'Guardar' : 'Postularme' }}</button>
            </div>
        </div>
    @endisset
</div>

{{-- modal para agregar solicitud de empleo --}}
<div class="modal fade" id="solicitud-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('profile.solicitud')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    <style type="text/css">
        .custom-file-label::after {
         /* left: 0;
            right: auto;
            border-left-width: 0;
            border-right: inherit; */
            background-color: #244395;
            color: #ffffff;
        }

        h5 {
            color: #244395;
            font-weight: bold;
        }

        .morado {
            background-color: #244395;
            color: #ffffff;
        }

        .clase_error {
            display: none;
            color: red;
        }
    </style>
    
    <script type="text/javascript">
        $(function() {
            $('#tableCursos').DataTable({
                "order": [[ 0, "asc" ]],
                "paging": true,
                "pagingType": "numbers",
                //"scrollX":true,
                //"scrollCollapse": true,
                "scrollX": true,
                // "fixedColumns":{
                // 	"leftColumns": 1,
                // 	"rightColumns": 1,
                // },
                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     ">",
                        "sPrevious": "<"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });

            $('#plan_id').change(function(){
                var plan_id = $(this).val();
                var job_id = $('input[name=job_position_id]').val();
                var user_id = $('input[name=user_id]').val();
                // console.log('plan_id', plan_id);
                // console.log('job_id', job_id);
                $('#course_id').empty();
                $('#course_id').selectpicker('refresh');
                // console.log(plan_id, job_id, user_id);
                getMoodleCoursesByPlanAndJob(plan_id, job_id, user_id);
            });

            $('#cursos-tab').on('shown.bs.tab', function (e) {
                redrawTableWrapper();
            });

            function redrawTableWrapper() {
                $('.tableWrapper').hide();
                setTimeout(() => {
                    $('.tableWrapper').show();
                    $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust()
                    .fixedColumns().relayout();
                }, 1000);
            }

            function getMoodleCoursesByPlanAndJob(plan_id, job_id, user_id) {
                $.ajax({
                    url: "{{url('escalafon/get_moodle_courses_by_plan_and_job')}}/"+plan_id+'/'+job_id+'/'+user_id,
                    Type:'GET',
                    success: function(result){
                        $.each(result, function(i, item){
                            $('#course_id').append($('<option>', { 
                                value: i,
                                text : item.name + (item.enrolled?' (Ya matriculado)':''),
                                disabled: item.enrolled
                            })).selectpicker('refresh');
                        });
                    }
                });
            }

            function formatNumber(n) {
                return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }

            function formatCurrency(input, blur) {
                var input_val = input.val();
                if (input_val === "") { return; }
                var original_len = input_val.length;
                var caret_pos = input.prop("selectionStart");
                
                if (input_val.indexOf(".") >= 0) {
                    var decimal_pos = input_val.indexOf(".");
                    var left_side = input_val.substring(0, decimal_pos);
                    var right_side = input_val.substring(decimal_pos);

                    left_side = formatNumber(left_side);
                    right_side = formatNumber(right_side);

                    if (blur === "blur") {
                        right_side += "00";
                    }
                    right_side = right_side.substring(0, 2);
                    input_val = "$ " + left_side + "." + right_side;

                } else {
                    input_val = formatNumber(input_val);
                    input_val = "$ " + input_val;
            
                    if (blur === "blur") {
                        input_val += ".00";
                    }
                }
                input.val(input_val);
                var updated_len = input_val.length;
                caret_pos = updated_len - original_len + caret_pos;
                input[0].setSelectionRange(caret_pos, caret_pos);
            }

            $(".currency").each(function( index ) {
                formatCurrency($(this));
            });

            $(document).on('keyup', '.currency', function(){  
                formatCurrency($(this));
            });

            $(".comprobante").click(function() {
                var profile = $(this).attr("data-profile");
                var documento = $(this).attr("data-documento");
                
                $('#ver-profile').val(profile);
                $('#ver-documento').val(documento);
               
            });
            
            setTimeout(function() {
                $('#mensaje').slideUp('slow');
            }, 3500); 


            $('#userB').on('change', function() {
                var id = $(this).find(":selected").val();
    
                $(".inputs-bienes").removeAttr('required');
                $(".inputs-bienes-"+id).attr("required","required");

                if(id == '')
                    $("#code").css("display","none");
                else{
                    $("#code").css("display","block");
                }

                $(".dataBienes").css("display","none");
                $("."+id).css("display","block");
            });


            $(".verDetalles").click(function() {
                var id = $(this).attr("data-id");
                var codigo = $(this).attr("data-codigo");
                var bien = $(this).attr("data-bien");
                var entrega = $(this).attr("data-entrega");

                $(".dataDetallesBienes").css("display","none");
                $("."+id).css("display","block");
                $(".InputDetallesBienes").attr("readonly",true);

                $('#ver-codigo').val(codigo);
                $('#ver-bien').val(bien);
                $('#ver-entrega').val(entrega);
               
            });
            


        $(".comprobante").click(function() {

            var profile = $(this).attr("data-profile");
            var documento = $(this).attr("data-documento");
            
            $('.ver-profile').val(profile);
            $('.ver-documento').val(documento);
           
        });
            
        $(".comprobante_b").click(function() {

            var motivo = $(this).attr("data-motivo");
            var fecha = $(this).attr("data-fecha");
            var detalle = $(this).attr("data-detalle");
            
            $('.ver-motivo').val(motivo);
            $('.ver-fecha').val(fecha);
            $('.ver-detalle').val(detalle);
           
        });














            $('.custom-file input').change(function (e) {
                $(this).next('.custom-file-label').html(e.target.files[0].name);
            });

            /* $('input').attr('readonly', 'readonly'); */
            var largo = 0;
            $("#agregar").click(function() {
                var cloned = $("#escolar").clone(true,true).appendTo("#destino");
                // cloned.find('input,textarea').val('');
                largo++;
            });

            $("#borrar").click(function() {
                if(largo > 0) {
                    $("#escolar").remove().end().appendTo("#destino");
                    largo--;
                }
            });

            $('#solicitud').on('click', function (){
                $('#name_sol').val($('#name').val());
                $('#surname_father_sol').val($('#surname_father').val());
                $('#surname_mother_sol').val($('#surname_mother').val());
                $('#cellphone_sol').val($('#cellphone').val());
                $('#phone_sol').val($('#phone').val());
                $('#email_sol').val($('#email').val());
                $('#gender_sol').val($('#gender').val());
                $('#date_birth_sol').val($('#date_birth').val());
                $('#rfc_sol').val($('#rfc').val());
            });

            /***********************************************************************************************************/
            /*****************************funciones jquery para la ventana modal de solicitud **************************/
            /***********************************************************************************************************/
            $("#solicitud-modal").on('change', '#date_begin_experience, #date_end_experience', function(e) {
                e.preventDefault();
                var edad_min = $("#date_begin_experience").val();
                var edad_max = $("#date_end_experience").val();
                if (edad_min > edad_max) {
                    $("#edad_max_error").slideDown('slow');
                } else{
                    $("#edad_max_error").slideUp('slow');
                }
            });

            $("#solicitud-modal").on('change', '#state', function(e) {
                e.preventDefault();
                var id = $(this).val();
                $.ajax ({
                    type: 'GET',
                    url: "{{ url('listStates') }}/"+id,
                    dataType: 'json',
                    success: function(response) {
                        $('#city').empty();
                        $.each(response, function(key, element) {
                            $('#city').append("<option value='" + element.id + "'>" + element.name + "</option>").selectpicker('refresh');
                        });
                    }
                });
            });

            var largo_esc = 0;
            var indice_esc = 1;
            var maximo_esc = 3;
            $('#agregar_application').on('click', function() { 
                if(largo_esc < maximo_esc) {     
                    var clone_esc = $("#escolar_application").clone(true);
                    clone_esc.find('input[type=radio]').each(function (index) {
                        var name = $(this).prop('name');
                        $(this).prop('name', name + indice_esc);
                    });
                    //clone_esc.find("input").val("").end();
                    
                    clone_esc.appendTo("#destino_application:last");               
                    largo_esc++;
                    indice_esc++;
                }
            }); 

            $('.borrar_application').on('click', function(){
                if(largo_esc > 0) {
                    var valOption = $(this).parent().parent().parent().find('#studio').children("option:selected").val();
                    $('.s-' + valOption).show();

                    $(this).parent().parent().parent().remove();
                    largo_esc--;
                }
            });

            $("#solicitud-modal").on('change', '#studio', function(e) {
                e.preventDefault();
                var valOption = $(this).val();
                $('.s-' + valOption).hide();
            });



            var largo_esc_uni = 0;
            var indice_esc_uni = 1;
            var maximo_esc_uni = 2;
            $('#agregar_application_uni').on('click', function() {
                if(largo_esc_uni < maximo_esc_uni) {
                    var clone_esc_uni = $("#escolar_application_uni").clone(true);
                    clone_esc_uni.find('input[type=radio]').each(function (index) {
                        var name = $(this).prop('name');
                        $(this).prop('name', name + indice_esc_uni);
                    });
                    // clone_esc.find("input").val("").end();

                    clone_esc_uni.appendTo("#destino_application_uni:last");               
                    largo_esc_uni++;
                    indice_esc_uni++;
                }
            }); 

            $('.borrar_application_uni').on('click', function(){
                if(largo_esc_uni > 0) {
                    var valOptionUni = $(this).parent().parent().parent().find('#studio_uni').children("option:selected").val();
                    $('.s-' + valOptionUni).show();

                    $(this).parent().parent().parent().remove();
                    largo_esc_uni--;
                }
            });

            $("#solicitud-modal").on('change', '#studio_uni', function(e) {
                e.preventDefault();
                var valOptionUni = $(this).val();
                $('.s-' + valOptionUni).hide();
            });


            var largo_esc_language = 0;
            $('#agregar_application_language').on('click', function(){
                var clone_esc_language = $("#escolar_application_language").clone(true);
                // clone_esc.find("input").val("").end();

                clone_esc_language.appendTo("#destino_application_language:last");               
                largo_esc_language++;
            }); 

            $('.borrar_application_language').on('click', function(){
                if(largo_esc_language > 0) {
                    $(this).parent().parent().parent().remove();
                    largo_esc_language--;
                }
            });

            var largo_esc_knowledge = 0;
            $('#agregar_application_knowledge').on('click', function(){
                var clone_esc_knowledge = $("#escolar_application_knowledge").clone(true);
                // clone_esc.find("input").val("").end();

                clone_esc_knowledge.appendTo("#destino_application_knowledge:last");
                largo_esc_knowledge++;
            }); 

            $('.borrar_application_knowledge').on('click', function(){
                if(largo_esc_knowledge > 0) {
                    $(this).parent().parent().parent().remove();
                    largo_esc_knowledge--;
                }
            });


            var largo_esc_experience = 0;
            $('#agregar_application_experience').on('click', function(){
                var clone_esc_experience = $("#escolar_application_experience").clone(true);
                // clone_esc.find("input").val("").end();

                clone_esc_experience.appendTo("#destino_application_experience:last");
                largo_esc_experience++;
            }); 

            $('.borrar_application_experience').on('click', function(){
                if(largo_esc_experience > 0) {
                    $(this).parent().parent().parent().remove();
                    largo_esc_experience--;
                }
            });


            $("#family_working_1, #family_working_0").on('change', function (e) {
                e.preventDefault();
                if ($("#family_working_0").is(":checked")) {
                    $( "#family_working_name" ).prop( "readonly", true );
                    $( "#family_working_name" ).val("");
                } else  {
                    $( "#family_working_name" ).prop( "readonly", false );
                }
            });


            var largo_esc_reference = 0;
            $('#agregar_application_reference').on('click', function(){
                var clone_esc_reference = $("#escolar_application_reference").clone(true);
                // clone_esc.find("input").val("").end();

                clone_esc_reference.appendTo("#destino_application_reference:last");
                largo_esc_reference++;
            }); 

            $('.borrar_application_reference').on('click', function(){
                if(largo_esc_reference > 0) {
                    $(this).parent().parent().parent().remove();
                    largo_esc_reference--;
                }
            });

            $("#unionized_0, #unionized_1").on('change', function (e) {
                e.preventDefault();
                if ($("#unionized_0").is(":checked")) {
                    $( "#unionized_name" ).prop( "readonly", true );
                    $( "#unionized_name" ).val("");

                } else  {
                    $( "#unionized_name" ).prop( "readonly", false );
                }
            });


            /***********************************************************************************************************/
            /*****************************funciones jquery para la ventana complementaria ******************************/
            /***********************************************************************************************************/
            $('#state_c').on('change', function(e) {
                e.preventDefault();
                var id = $(this).val();
                $.ajax ({
                    type: 'GET',
                    url: "{{ url('listStates') }}/"+id,
                    dataType: 'json',
                    success: function(response) {
                        $('#city_c').empty();
                        $.each(response, function(key, element) {
                            $('#city_c').append("<option value='" + element.id + "'>" + element.nombre + "</option>").selectpicker('refresh');
                        });
                    }
                });
            });


            var primero_esc_c = $('select#studio_c:first').val();
            if (primero_esc_c == null) {
                var largo_esc_c = $("div[id='escolar_c']").length;
                var maximo_esc_c = 4;
            } else {
                var largo_esc_c = $("div[id='escolar_c']").length;
                var maximo_esc_c = 4;
            }

            var indice_esc_c = 1;
            $('#agregar_escolar_c').on('click', function() {
                if(largo_esc_c < maximo_esc_c) {     
                    var clone_esc = $("#escolar_c").clone(true);
                    clone_esc.find('.radio_check').each(function (index) {
                        var name = $(this).prop('name');
                        var name = name.replace('[0]', '[' + largo_esc_c + ']');
                        $(this).prop('name', name);
                    });
                    //clone_esc.find("input").val("").end();
                    
                    clone_esc.appendTo("#destino_escolar_c:last");               
                    largo_esc_c++;
                    indice_esc_c++;
                }
            }); 

            $('.borrar_escolar_c').on('click', function() {
                if(largo_esc_c > 1) {
                    var valOption = $(this).parent().parent().parent().find('#studio_c').children("option:selected").val();
                    $('.s-' + valOption +'-c').show();

                    $(this).parent().parent().parent().remove();
                    largo_esc_c--;

                    cont = 0;
                    let entrar = false;
                    $('.radio_check').each(function(i) {
                        $(this).attr('name', 'voucher[' + cont + ']' );
                        if(entrar){
                            entrar = false;
                            cont++;
                        }else{
                            entrar = true;
                        }
                    });
                }
            });

            $("#studio_c").on('change', function(e) {
                e.preventDefault();
                var valOption = $(this).val();
                $('.s-' + valOption + '-c').hide();
            });

            $('select#studio_c').each(function (i) {
                var valOption = $(this).val();
                $('.s-' + valOption + '-c').hide();
            });

            



            $('select #studio_uni_c').each(function (i) {
                var valOptionUni = $(this).val();
                $('.s-' + valOptionUni + '-c').hide();
            });

            var primero_esc_uni_c = $('select#studio_uni_c:first').val();
            
            if (primero_esc_uni_c == null) {
                var largo_esc_uni_c = $("div[id='escolar_uni_c']").length;
                var maximo_esc_uni_c = 3;
            } else {
                var largo_esc_uni_c = $("div[id='escolar_uni_c']").length;
                var maximo_esc_uni_c = 3;
            }
            var indice_esc_uni_c = 1;
            $('#agregar_escolar_uni_c').on('click', function() { 
                if(largo_esc_uni_c < maximo_esc_uni_c) {     
                    var clone_esc_uni = $("#escolar_uni_c").clone(true);
                    clone_esc_uni.find('input[type=radio]').each(function (index) {
                        var name = $(this).prop('name');
                        var name = name.replace('[0]', '[' + indice_esc_uni_c + ']');
                        $(this).prop('name', name );
                    });
                    
                    clone_esc_uni.find('.ver').each(function (index) {
                        
                        $(this).remove();
                    }); 

                    clone_esc_uni.find('input[type=text]').each(function (index) {
                        $(this).prop('value', '');
                    });
                    clone_esc_uni.find('input[type=file]').each(function (index) {
                        $(this).prop('value', '');
                    });
                    



                    //clone_esc_uni.find("input").val("").end();
                    
                    clone_esc_uni.appendTo("#destino_escolar_uni_c:last");               
                    largo_esc_uni_c++;
                    indice_esc_uni_c++;
                }
            }); 

            $('.borrar_escolar_uni_c').on('click', function(){
                if(largo_esc_uni_c > 1) {
                    var valOptionUni = $(this).parent().parent().parent().find('#studio_uni_c').children("option:selected").val();
                    $('.s-' + valOptionUni +'-c').show();

                    $(this).parent().parent().parent().remove();
                    largo_esc_uni_c--;

                    cont = 0;
                    let entrar = false;
                    $('.radio_check_uni').each(function(i) {
                        $(this).attr('name', 'voucher_uni[' + cont + ']' );
                        if(entrar){
                            entrar = false;
                            cont++;
                        }else{
                            entrar = true;
                        }
                    });
                }
            });

            $("#studio_uni_c").on('change', function(e) {
                e.preventDefault();
                var valOptionUni = $(this).val();
                $('.s-' + valOptionUni + '-c').hide();
            });

            

            var largo_language = $("div[id='language_c']").length;
            var maximo_language_c = 4;
            $('#agregar_language_c').on('click', function(){
                if(largo_language < maximo_language_c) {
                    var clone_language = $("#language_c").clone(true);
                    // clone_esc.find("input").val("").end();

                    clone_language.find('input[type=text]').each(function (index) {
                        $(this).prop('value', '');
                    });

                    clone_language.find('input[type=number]').each(function (index) {
                        $(this).prop('value', '');
                    });
                    
                    clone_language.appendTo("#destino_language_c:last");               
                    largo_language++;
                }
            }); 

            $('.borrar_language_c').on('click', function(){
                if(largo_language > 1) {
                    $(this).parent().parent().parent().remove();
                    largo_language--;
                }
            });


            var primero_knowledge = $('select#knowledge_type_c:first').val();
            if (primero_knowledge == null) {
                var largo_knowledge = $("div[id='escolar_knowledge_c']").length
                var maximo_knowledge_c = 3;
            } else {
                var largo_knowledge = $("div[id='escolar_knowledge_c']").length
                var maximo_knowledge_c = 3;
            }
            $('#agregar_knowledge_c').on('click', function(){
                if(largo_knowledge < maximo_knowledge_c) {
                    var clone_knowledge = $("#escolar_knowledge_c").clone(true);
                    // clone_knowledge.find("input").val("").end();

                    clone_knowledge.find('select').each(function (index) {
                        $(this).prop('value', 'Maquinaria');
                    });

                    clone_knowledge.find('input[type=text]').each(function (index) {
                        $(this).prop('value', '');
                    });
                    clone_knowledge.appendTo("#destino_knowledge_c:last");               
                    largo_knowledge++;
                }
            }); 

            $('.borrar_knowledge_c').on('click', function(){
                if(largo_knowledge > 1) {
                    $(this).parent().parent().parent().remove();
                    largo_knowledge--;
                }
            });



            var largo_experience = $("div[id='escolar_experience_c']").length
            var maximo_experience_c = 4;

            $('#agregar_experience_c').on('click', function(){
                if(largo_experience < maximo_experience_c) {
                    var clone_experience = $("#escolar_experience_c").clone(true);
                    // clone_experience.find("input").val("").end();

                    clone_experience.appendTo("#destino_experience_c:last");               
                    largo_experience++;
                }
            }); 

            $('.borrar_experience_c').on('click', function(){
                if(largo_experience > 1) {
                    $(this).parent().parent().parent().remove();
                    largo_experience--;
                }
            });


            var largo_reference = $("div[id='escolar_reference_c']").length
            var maximo_reference_c = 6;

            $('#agregar_reference_c').on('click', function(){
                if(largo_reference < maximo_reference_c) {
                    var clone_reference = $("#escolar_reference_c").clone(true);
                    // clone_reference.find("input").val("").end();

                    clone_reference.find('input[type=text]').each(function (index) {
                        $(this).prop('value', '');
                    });
                    
                    clone_reference.appendTo("#destino_reference_c:last");               
                    largo_reference++;
                }
            }); 

            $('.borrar_reference_c').on('click', function(){
                if(largo_reference > 1) {
                    $(this).parent().parent().parent().remove();
                    largo_reference--;
                }
            });


            
            if ($("#unionized_0_c").is(":checked")) {
                $( "#unionized_name_c" ).prop( "readonly", true );
                $( "#unionized_name_c" ).val("");
            } else  {
                $( "#unionized_name_c" ).prop( "readonly", false );
            }

            $("#unionized_0_c, #unionized_1_c").on('change', function (e) {
                e.preventDefault();
                if ($("#unionized_0_c").is(":checked")) {
                    $( "#unionized_name_c" ).prop( "readonly", true );
                    $( "#unionized_name_c" ).val("");

                } else  {
                    $( "#unionized_name_c" ).prop( "readonly", false );
                }
            });

            /***********************************************************************************************************/
            /***************************** funciones jquery para la ventana salud **************************************/
            /***********************************************************************************************************/
            var largo_salud = $("div[id='salud_s']").length;
            var maximo_salud_c = 4;
            $('#agregar_salud').on('click', function(){
                if(largo_salud < maximo_salud_c) {
                    var clone_salud = $("#salud_s").clone(true);
                    clone_salud.find("input").val("").end();

                    clone_salud.appendTo("#destino_salud:last");               
                    largo_salud++;
                }
            }); 

            $('.borrar_salud').on('click', function(){
                if(largo_salud > 1) {
                    $(this).parent().parent().parent().remove();
                    largo_salud--;
                }
            });


            /***********************************************************************************************************/
            /***************************** funciones jquery para la ventana beneficiarios ******************************/
            /***********************************************************************************************************/
            var largo_beneficiario = $("div[id='beneficiario_s']").length;
            var maximo_beneficiario_c = 6;
            $('#agregar_beneficiarios_hijos').on('click', function(){
                if(largo_beneficiario < maximo_beneficiario_c) {
                    var clone_beneficiario = $("#beneficiario_s").clone(true);
                    clone_beneficiario.find("input").val("").end();

                    clone_beneficiario.appendTo("#destino_beneficiarios_hijos:last");               
                    largo_beneficiario++;
                }
            }); 

            $('.borrar_beneficiarios_hijos').on('click', function(){
                if(largo_beneficiario > 1) {
                    $(this).parent().parent().parent().remove();
                    largo_beneficiario--;
                }
            });
            
            /* if ($("#family_working_c").is(":checked")) {
                $('.parientes').toggleClass("d-none");
                $('#family_working_name_c').selectpicker('refresh');
                console.log('checked primera carga');
            } else  {
                // $('.parientes').toggleClass("d-none");
                $('#family_working_name_c').append("<option value=''>Seleccione una opción...</option>").selectpicker('refresh');
                console.log('no checked primera carga');
            } */

            $("#family_working_c").on('change', function (e) {
                e.preventDefault();
                if ($('#family_working_c').is(':checked')) {
                    console.log('checked change check');
                    // $('.parientes').toggleClass("d-none");
                    $.ajax ({
                        type: 'GET',
                        // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url: "{{ url('listsprofile') }}",
                        dataType: 'json',
                        success: function(response) {
                            $('#family_working_name_c').empty();
                            $.each(response, function(key, element) {
                                $('#family_working_name_c').append('<option value="' + element.id + '">' + element.first_name + ' ' + element.last_name + '</option>');
                            });
                            $('#family_working_name_c').selectpicker('refresh');
                        }
                    });
                } else  {
                    console.log('no checked change check');
                    //$('.parientes').toggleClass("d-none");
                    $("#family_working_name_c").val("");
                    $("#family_working_name_c").empty();
                    $('#family_working_name_c').append("<option value=''>Seleccione una opción...</option>").selectpicker('refresh');
                }
            });

            /***********************************************************************************************************/
            /*************** funciones jquery para agregar campos dinamicos para sindicatos***************************/
            /***********************************************************************************************************/

            var largo_sindicato_c = $("div[id='sindicato_c']").length;
            var maximo_sindicato_c = 3;
            
            var indice_sindicato_c = 1;


            $('#agregar_sindicatos_c').on('click', function() { 
                if(largo_sindicato_c < maximo_sindicato_c) {     
                    var clone_sindicato = $("#sindicato_c").clone(true);

                    clone_sindicato.find('input[type=text]').each(function (index) {
                        $(this).prop('value', '');
                    });

                    clone_sindicato.appendTo("#destino_sindicato_c:last"); 

                    largo_sindicato_c++;
                    indice_sindicato_c++;
                }
            }); 

            $('.borrar_sindicato_c').on('click', function(){
                if(largo_sindicato_c > 1) {
                    var valOptionUni = $(this).parent().parent().parent().find('#studio_uni_c').children("option:selected").val();
                    $('.s-' + valOptionUni +'-c').show();

                    $(this).parent().parent().parent().remove();
                    largo_sindicato_c--;

                }
            });

        });
    </script>
@endsection

