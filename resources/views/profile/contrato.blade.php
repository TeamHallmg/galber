<div class="container-fluid">
    <h5>DATOS CONTRATO:</h5>

        <div class="form-row">
            <div class="form-group col">
                <label for="name" class="requerido">Nombre</label>
                <input type="text" name="name" id="name" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->nombre : null : $profile->user->first_name : null  }}" disabled={{ is_null($user) ? false : true }}>
            </div>
            
            <div class="form-group col">
                <label for="surname_father" class="requerido">Apellido Paterno</label>
                <input type="text" name="surname_father" id="surname_father" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->paterno : null : $profile->user->last_name : null }}" disabled={{ is_null($user) ? false : true }}>
            </div>

            <div class="form-group col">
                <label for="surname_mother" class="requerido">Apellido Materno</label>
                <input type="text" name="surname_mother" id="surname_mother" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->materno : null : null : null }}" disabled={{ is_null($user) ? false : true }}>
            </div>

            <div class="form-group col">
                <label for="" class="requerido">Estado Civil:</label>
                <input type="text" name="" id="" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->civil : null : null : null }}" disabled="{{ is_null($user) ? false : true }}">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col">
                <label for="nacionalidad" class="requerido">Nacionalidad</label>
               <input type="text" name="nacionalidad" id="nacionalidad" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->contrato)  ? $profile->contrato->nacionalidad : null : null  }}">
            </div>
            
            <div class="form-group col">
                <label for="profesion" class="requerido">Profesión</label>
                <input type="text" name="profesion" id="profesion" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->contrato)  ? $profile->contrato->profesion : null : null  }}">
            </div>

            <div class="form-group col">
                <label for="sede_principal" class="requerido">Sede Principal</label>
                <input type="text" name="sede_principal" id="sede_principal" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->contrato)  ? $profile->contrato->sede_principal : null : null  }}">
            </div>

            <div class="form-group col">
                <label for="sede_secundaria" class="requerido">Sede Secundaria:</label>
                <input type="text" name="sede_secundaria" id="sede_secundaria" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->contrato)  ? $profile->contrato->sede_secundaria : null : null  }}">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col">
                <label for="tipo_contrato" class="requerido">Tipo Contrato</label>
                <input type="text" name="tipo_contrato" id="tipo_contrato" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->contrato)  ? $profile->contrato->tipo_contrato : null : null  }}">
            </div>
            
            <div class="form-group col">
                <label for="horario" class="requerido">Horario</label>
                <input type="text" name="horario" id="horario" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->contrato)  ? $profile->contrato->horario : null : null  }}">
            </div>

            <div class="form-group col">
                <label for="salario" class="requerido">Salario</label>
                <input type="text" name="salario" id="salario" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->contrato)  ? $profile->contrato->salario : null : null  }}">
            </div>

            <div class="form-group col">
                <label for="bono" class="requerido">Bono:</label>
                <input type="text" name="bono" id="bono" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->contrato)  ? $profile->contrato->bono : null : null  }}">
            </div>
        </div>


    <div class="row mt-3">
        <div class="col text-right">
            <button type="submit" class="btn morado">{{ is_null($profileBeneficiarios) ? 'Guardar' : 'Actualizar' }}</button>
        </div>
    </div>
    
    <hr>
    <h5>INFORMACIÓN DEL CONTRATO:</h5>
    <?php $validado = true; ?>
         @foreach($mensajesContrato as $mensajes) 

            @if( $mensajes['validacion'] )
            <span style="color: green">
                <i class="fa fa-check"></i> 
                {{ $mensajes['msg'] }}
            </span>
            @else
            <?php $validado = false; ?>
            <span style="color: red">
                <i class="fa fa-exclamation-triangle"></i>
                {{ $mensajes['msg'] }}
            </span>
                 
            @endif

            
            <br>
        @endforeach 

    <div class="row mt-3">
        <div class="col text-right">

            @if(!$validado)
            <a class="btn btn-danger mx-1 editBtn card-1" title="No se Puede Generar el Contrato">
                 <i class="fa fa-exclamation-triangle"></i>
                Debe completar todos los campos
            </a>

            
            @else

            <a href="/generar-contrato/{{$profile->user->employee->id}}" target="_blank" class="btn btn-success mx-1 editBtn card-1" title="Ver Contrato">
                <i class="fas fa-file-pdf"></i> Imprimir Contrato
            </a>
            @endif
        </div>
    </div>
    

</div>
    
