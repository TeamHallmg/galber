<div class="container-fluid">
    <h5>RECURSOS</h5>

    <div class="form-row">
        <div class="form-group col-3">
            <div class="row">
                <div class="form-group col text-center">
                    @if (isset($profile) && !is_null($profile->image))
                        <img src="{{ asset('uploads/profile/'.$profile->image) }}" class="mb-3" style="max-width: 270px; max-height: 183px">
                    @else
                        <img src="{{ asset('img/vacantes/sinimagen.png') }}" class="mb-3" style="max-width: 300px; max-height: 183px">
                    @endif
                </div>
            </div>
        </div>
        <div class="col-9">
            <div class="row">
                <div class="form-group col"></div>
                
                <div class="form-group col"></div>

                <div class="form-group col">
                    <label for="" class="requerido">Número de empleado</label>
                    <input type="text" name="" id="" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->idempleado : null : null : null }}" disabled="{{ is_null($user) ? false : true }}">
                </div>
            </div>
            <div class="row">
                <div class="form-group col">
                    <label for="name" class="requerido">Nombre</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->nombre : null : $user->first_name : null }}" disabled={{ is_null($user) ? false : true }}>
                </div>
                
                <div class="form-group col">
                    <label for="surname_father" class="requerido">Apellido Paterno</label>
                    <input type="text" name="surname_father" id="surname_father" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->paterno : null : $user->last_name : null }}" disabled={{ is_null($user) ? false : true }}>
                </div>

                <div class="form-group col">
                    <label for="surname_mother" class="requerido">Apellido Materno</label>
                    <input type="text" name="surname_mother" id="surname_mother" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->materno : null : null : null }}" disabled={{ is_null($user) ? false : true }}>
                </div>
            </div>

            <div class="row">
                <div class="form-group col"></div>
                
                <div class="form-group col"></div>

                <div class="form-group col">
                    <label for="">Puesto</label><br>
{{-- 
                    @if(!is_null($profile))

                        <a href="{{ url('jobPosition/' .  $profile->user->employee->jobPosition->id . '/edit') }}"> {{ $profile->user->employee->jobPosition->name }} </a>
                     @else
                        <a href="{{ url('jobPosition/' .  $user->employee->jobPosition->id . '/edit') }}"> {{ $user->employee->jobPosition->name }} </a>
                     @endif --}}

                 
                </div>
            </div>
            <div class="row col"></div>
        </div>
    </div>
    <br>
   
    <div class="row">
        <div class="col-md-8">
            <h3 class="font-weight-bold">Recursos Asignados</h3>
        </div>
        <div class="col-md-4" style="text-align: right;">
            

            <a href="#" class="btn morado" data-toggle="modal" data-target="#nuevo-bien-modal">
                Asignar Recurso
            </a>
         

        </div>
    </div>
    <hr style="border: 2px solid #000000;">
            
    <table class="table table-hover table-bordered" id="tableBienes">
        <thead class="blue_header">
            <tr>
                <th>Código</th>
                <th>Recurso</th>
                <th>Entrega</th>
                <th>Estatus</th>
                <th>Opciones</th>
                {{-- <th>ruta</th> --}}
            </tr>
        </thead>
        <tbody>
            @if(!is_null($profile))

                @if (isset($bienesasociados))

                    @if (count($bienesasociados) == 0)
                        <tr>
                            <td colspan="5" align="center">
                                No tienes recursos Asignados
                            </td>
                        </tr>
                    @endif

                    @foreach($bienesasociados as $bien)
                        <tr>
                            <td> 
                                <a href="#" data-toggle="modal" data-target="#ver-bien-modal" class="verDetalles" data-id="{{ $bien['id'] }}" data-codigo="{{ $bien['codigo'] }}" data-bien="{{ $bien['nombre_bien'] }}" data-entrega="{{ Carbon\Carbon::parse($bien['fecha_entrega'])->format('d-m-Y') }}"> 
                                    {{ $bien['codigo'] }} 
                                </a>
                            </td>
                            <td>{{ $bien['nombre_bien'] }}</td>
                            <td>{{ Carbon\Carbon::parse($bien['fecha_entrega'])->format('d-m-Y') }}</td>
                           <td>
                            @if($bien['estatus'] == 1)
                                <span style="color: green;">Entregado</span>
                            @elseif($bien['estatus'] == 2)
                                <span style="color: green;">Descargado</span>
                            @elseif($bien['estatus'] == 3)
                                <span style="color: green;">Con Comprobante</span>
                            @elseif($bien['estatus'] == 4)
                            <a href="#" data-toggle="modal" data-target="#desincorporar-detalle-modal" class="comprobante_b mr-1" title="Desincorporar Recurso" data-motivo="{{$bien['motivo_desincorporacion']}}" data-fecha="{{$bien['fecha_desincorporacion']}}" data-detalle="{{$bien['detalle_desincorporacion']}}"> 
                                    
                                <span style="color: red;">Desincorporado</span>
                                </a>   

                            @endif
                        </td>
                           
                        <td>
                                <div class="btn-group">
                                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Documentos
                                  </button>
                                  <div class="dropdown-menu">
                                   
                                    <a class="dropdown-item" href="{{url('NotaEntregaBien/'.$profile->id.'/'.$bien['id'])}}" target="_blank">
                                        <i class="fas fa-file-pdf"></i> Descargar PDF
                                    </a>

                                    @if($bien['estatus'] != 4)
                                        <a class="dropdown-item comprobante" href="#" data-toggle="modal" data-target="#nuevo-comprobante-modal" data-profile="{{$profile->id}}" data-documento="{{$bien['id']}}">
                                            
                                            <i class="fa fa-upload"></i> Adjuntar Comprobante
                                           
                                        </a>
                                    @endif
                                    
                                    @if($bien['comprobante'] != null)
                                    <a class="dropdown-item" href="/descargar_comprobante/{{$bien['id']}}" target="_blank">
                                        <i class="fa fa-download"></i> Ver Comprobante
                                    </a>
                                    @endif
                                    
                                    {{-- <div class="dropdown-divider"></div>

                                    <a class="dropdown-item" href="#">
                                        <i class="fas fa-times-circle"></i> Anular Documento
                                    </a> --}}

                                  </div>
                                </div>     

                                @if($bien['estatus'] != 4)
                                    <a href="#" data-toggle="modal" data-target="#desincorporar-bien-modal" class="btn btn-danger comprobante mr-1" title="Desincorporar Recurso"  data-profile="{{$profile->id}}" data-documento="{{$bien['id']}}"> 
                                        <i class="fa fa-user-times"> </i>
                                    </a>   
                                @endif

                        </td>
                        </tr>
                    @endforeach
                @endif
                @else
                 <tr>
                    <td colspan="5" align="center">
                        No tienes recursos Asignados
                    </td>
                </tr>
            @endif
        </tbody>
    </table>

</div>

@if(!is_null($profile))
{{-- modal para agregar solicitud de empleo --}}
<div class="modal fade" id="nuevo-bien-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Asignar Recurso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ url('AsignarBienes') }}" method="POST" enctype="multipart/form-data">
            @csrf
                <input type="hidden" name="profile_id" id="profile_id" value="{{ isset($profile) ? $profile->id : null }}">
                {{-- <input type="hidden" name="user_id" id="user_id" value="{{ !is_null($user) ? $user->id : null }}"> --}}
                
                <div class="modal-body">
                   <div class="row">
                        <div class="col-6 col-md-6">
                            <div class="form-group">
                                <label class="an-title font-weight-bold" for="type">Tipo de Recursos Disponibles</label>
                                <select id="userB" class="form-control" name="tipoBien" required>
                                        <option value="">Elije un tipo Recurso...</option>
                                        @foreach ($profile->user->employee->jobPosition->Jobtipobienes as $Jobtipobienes)
                                            <option value="{{$Jobtipobienes->tipobien->id}}">{{ $Jobtipobienes->tipobien->nombre }}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                    </div>


                   <div id="code" style="display: none">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="an-title font-weight-bold" for="codigo">Código Del Recurso</label>
                                <input type="text" name="codigo" id="codigo" class="form-control" required="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="an-title font-weight-bold" for="fecha_entrega">Fecha de Entrega</label>
                                <input type="date" name="fecha_entrega" id="fecha_entrega" class="form-control" required="">
                            </div>
                        </div>
                    </div>
                    </div>
                    
                    
                   <div class="row">
                        <div class="col-12 col-md-12">

                                    <div class="row">
                                @foreach ($profile->user->employee->jobPosition->Jobtipobienes as $Jobtipobienes)


                                        @foreach ($Jobtipobienes->tipobien->tipobiendetalles as $tipobiendetalles)

                                            <div class="form-group dataBienes col-md-3 {{ $Jobtipobienes->tipobien->id }}" style="display: none">

                                                <label for="{{ $Jobtipobienes->tipobien->id }}{{ $tipobiendetalles->variable->id }}" class="an-title font-weight-bold"> {{ $tipobiendetalles->variable->etiqueta }}</label>

                                                <input type="text" name="bienesDatos[{{ $Jobtipobienes->tipobien->id }}][{{ $tipobiendetalles->variable->id }}][]" id="{{ $Jobtipobienes->tipobien->id }}{{ $tipobiendetalles->variable->id }}" class="form-control inputs-bienes inputs-bienes-{{ $Jobtipobienes->tipobien->id }}" required="">

                                            </div>

                                        @endforeach


                                @endforeach

                                    </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success">Aceptar</button>
                </div>

            </form>
        </div>
    </div>
</div>

{{-- modal para agregar solicitud de empleo --}}
<div class="modal fade" id="ver-bien-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Detalles del Recurso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body">

                   <div class="row">
                        <div class="col-4 col-md-4">
                            <div class="form-group">
                                <label class="an-title font-weight-bold" for="codigo">Recurso</label>
                                <input type="text" name="codigo" id="ver-bien" class="form-control InputDetallesBienes">
                            </div>
                        </div>
                        <div class="col-4 col-md-4">
                            <div class="form-group">
                                <label class="an-title font-weight-bold" for="codigo">Código Del Recurso</label>
                                <input type="text" name="codigo" id="ver-codigo" class="form-control InputDetallesBienes" >
                            </div>
                        </div>
                        <div class="col-4 col-md-4">
                            <div class="form-group">
                                <label class="an-title font-weight-bold" for="codigo">Fecha Entrega Del Recurso</label>
                                <input type="text" name="codigo" id="ver-entrega" class="form-control InputDetallesBienes" >
                            </div>
                        </div>
                    </div>

                  
                    <div class="row">
                        @foreach($bienesasociados as $bien)


                            @foreach ($bien['variables'] as $tipobiendetalles)

                                <div class="form-group dataBienes col-md-3 dataDetallesBienes {{ $bien['id'] }}" style="display: none">

                                    <label class="an-title font-weight-bold"> {!! $tipobiendetalles['variable'] !!}</label>
                                    
                                    <input type="text" name="" id="" value="{{ $tipobiendetalles['variable_valor'] }}" class="form-control InputDetallesBienes">
                                    
                                </div>

                            @endforeach


                    @endforeach

                        </div>
                </div>

                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-success">Aceptar</button>
                </div>

        </div>
    </div>
</div>
@endif


    {{-- modal para agregar solicitud de empleo --}}
<div class="modal fade" id="nuevo-comprobante-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Subir Comprobante</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ url('/adjuntar_comprobante_bienes') }}" method="POST" enctype="multipart/form-data">
            @csrf                
            <input type="hidden" name="profile_id" class="ver-profile">
            <input type="hidden" name="documento_id" class="ver-documento">
                <div class="modal-body">
                   <div class="row">
                        <div class="col-12 col-md-12">
                            <div class="form-group col-12 col-md-12">
                                <label class="an-title font-weight-bold" for="type">Buscar Comprobante</label>
                                <input type="file" name="select_file" id="select_file" />    
                            </div>
                        </div>
                    </div>
                    
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success">Aceptar</button>
                </div>

            </form>
        </div>
    </div>
</div>
    {{-- modal para agregar solicitud de empleo --}}
<div class="modal fade" id="desincorporar-bien-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Desincorporar Recurso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ url('/desincorporar_bien') }}" method="POST" enctype="multipart/form-data">
            @csrf                
            <input type="hidden" name="profile_id" class="ver-profile">
            <input type="hidden" name="documento_id" class="ver-documento">
                <div class="modal-body">
                  
                   <div class="row">

                        <div class="col-12 col-md-12">
                            <div class="form-group col-12 col-md-12">
                                <label class="an-title font-weight-bold" for="motivo">Motivo</label>
                                <select class="form-control" id="motivo" name="motivo_desincorporacion" required>
                                        <option value="">Seleccione...</option>
                                        <option value="Devolver">Devolver</option>
                                        <option value="Descompuesto">Descompuesto</option>
                                        <option value="Robado">Robado </option>
                                        <option value="Perdido">Perdido</option>
                                </select>
                                
                            </div>
                        </div>
                        
                        <div class="col-12 col-md-12">
                            <div class="form-group col-12 col-md-12">
                                <label class="an-title font-weight-bold" for="fecha">Fecha de la Desincorporación</label>
                                <input type="date" name="fecha_desincorporacion" id="fecha" class="form-control">
                            </div>
                        </div>

                        <div class="col-12 col-md-12">
                            <div class="form-group col-12 col-md-12">
                                <label class="an-title font-weight-bold" for="detalle">Detalles del Motivo:</label>
                                <textarea id="detalle" class="form-control" name="detalle_desincorporacion">
                                </textarea>
                            </div>
                        </div>
                        
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success mostrar">Desincorporar</button>
                </div>

            </form>
        </div>
    </div>
</div>    {{-- modal para agregar solicitud de empleo --}}
<div class="modal fade" id="desincorporar-detalle-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Detalles de la Desincorporación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                         
                <div class="modal-body">
                  
                   <div class="row">

                        <div class="col-12 col-md-12">
                            <div class="form-group col-12 col-md-12">
                                <label class="an-title font-weight-bold" for="motivo">Motivo</label>
                                <select class="form-control ver-motivo"  readonly="">
                                        <option value="">Seleccione...</option>
                                        <option value="Devolver">Devolver</option>
                                        <option value="Descompuesto">Descompuesto</option>
                                        <option value="Robado">Robado </option>
                                        <option value="Perdido">Perdido</option>
                                </select>
                                
                            </div>
                        </div>
                        
                        <div class="col-12 col-md-12">
                            <div class="form-group col-12 col-md-12">
                                <label class="an-title font-weight-bold" for="fecha">Fecha de la Desincorporación</label>
                                <input type="date" class="form-control ver-fecha" readonly="">
                            </div>
                        </div>

                        <div class="col-12 col-md-12">
                            <div class="form-group col-12 col-md-12">
                                <label class="an-title font-weight-bold" for="detalle">Detalles del Motivo:</label>
                                <textarea id="detalle" class="form-control ver-detalle" readonly="">
                                </textarea>
                            </div>
                        </div>
                        
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>

        </div>
    </div>
</div>