<div class="container-fluid">
    <h5>REGISTRO</h5>

    <div class="form-row">
        <div class="form-group col-3">
            <div class="row">
                <div class="form-group col text-center">
                    @if (isset($profile) && !is_null($profile->image))
                        <img src="{{ asset('uploads/profile/'.$profile->image) }}" class="mb-3" style="max-width: 270px; max-height: 183px">
                    @else
                        <img src="{{ asset('img/vacantes/sinimagen.png') }}" class="mb-3" style="max-width: 300px; max-height: 183px">
                    @endif
                </div>
            </div>
        </div>
        <div class="col-9">
            <div class="row">
                <div class="form-group col"></div>
                
                <div class="form-group col"></div>

                <div class="form-group col">
                    <label for="" class="requerido">Número de empleado</label>
                    <input type="text" name="" id="" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->idempleado : null : null : null }}" disabled="{{ is_null($user) ? false : true }}">
                </div>
            </div>
            <div class="row">
                <div class="form-group col">
                    <label for="name" class="requerido">Nombre</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->nombre : null : $user->first_name : null }}" disabled={{ is_null($user) ? false : true }}>
                </div>
                
                <div class="form-group col">
                    <label for="surname_father" class="requerido">Apellido Paterno</label>
                    <input type="text" name="surname_father" id="surname_father" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->paterno : null : $user->last_name : null }}" disabled={{ is_null($user) ? false : true }}>
                </div>

                <div class="form-group col">
                    <label for="surname_mother" class="requerido">Apellido Materno</label>
                    <input type="text" name="surname_mother" id="surname_mother" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->materno : null : null : null }}" disabled={{ is_null($user) ? false : true }}>
                </div>
            </div>

            <div class="row col"></div>
        </div>
    </div>
    <br>

    <h5>REGISTROS</h5>
    @if(is_null($profileRegistros))
            <div class="form-row">
                <div class="col">
                    <label for="">CURP</label>
                    <input type="text" name="curp_registry" id="curp_registry" class="form-control">
                </div>
                <div class="custom-file col">                
                    <input type="file" name="file[file_curp_registry]" class="form-control custom-file-input" id="curp" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="curp" data-browse="Subir documento"></label>
                </div>
                {{-- <div class="col">
                    <label for="">INFONAVIT</label>
                    <input type="text" name="infonavit_registry" id="infonavit_registry" class="form-control">
                </div>
                <div class="custom-file col">                
                    <input type="file" name="file[file_infonavit_registry]" class="form-control custom-file-input" id="infonavit" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="infonavit" data-browse="Subir documento"></label>
                </div> --}}
            </div>
            <br>
            <div class="form-row">
                <div class="col">
                    <label for="">RFC</label>
                    <input type="text" name="rfc_registry" id="rfc_registry" class="form-control">
                </div>
                <div class="custom-file col">                
                    <input type="file" name="file[file_rfc_registry]" class="form-control custom-file-input" id="rfc" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="rfc" data-browse="Subir documento"></label>
                </div>
                {{-- <div class="col">
                    <label for="">FONACOT</label>
                    <input type="text" name="fonacot_registry" id="fonacot_registry" class="form-control">
                </div>
                <div class="custom-file col">                
                    <input type="file" name="file[file_fonacot_registry]" class="form-control custom-file-input" id="fonacot" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="fonacot" data-browse="Subir documento"></label>
                </div> --}}
            </div>
            <br>
            <div class="form-row">
                <div class="col">
                    <label for="">IMSS</label>
                    <input type="text" name="imss_registry" id="imss_registry" class="form-control">
                </div>
                <div class="custom-file col">                
                    <input type="file" name="file[file_imss_registry]" class="form-control custom-file-input" id="imss" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="curp" data-browse="Subir documento"></label>
                </div>
                {{-- <div class="col">
                    <label for="">CLABE interbancaria</label>
                    <input type="text" name="clabe_registry" id="clabe_registry" class="form-control">
                </div>
                <div class="custom-file col">                
                    <input type="file" name="file[file_clabe_registry]" class="form-control custom-file-input" id="clabe" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="clabe" data-browse="Subir documento"></label>
                </div> --}}
            </div>
            <br>
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div class="col-4">
                            <label for="">INE</label>
                            <input type="text" name="ine_registry" id="ine_registry" class="form-control">
                        </div>
                        <div class="col-4">
                            <label for="">Expira</label>
                            <input type="date" name="ine_exp_registry" id="ine_exp_registry" class="form-control">
                        </div>
                        <div class="custom-file col-4">                
                            <input type="file" name="file[file_ine_registry]" class="form-control custom-file-input" id="ine" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="ine" data-browse="Subir documento"></label>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col-6">
                            <label for="">OTROS</label>
                            <input type="text" name="other_registry" id="other_registry" class="form-control">
                        </div>
                        <div class="custom-file col-6">                
                            <input type="file" name="file[file_other_registry]" class="form-control custom-file-input" id="otros" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="otros" data-browse="Subir documento"></label>
                        </div>
                    </div>
                </div>
            </div>
    @else
        <div class="form-row">
            <div class="col">
                <label for="">CURP</label>
                <input type="text" name="curp_registry" id="curp_registry" class="form-control" value="{{ $profileRegistros->curp_registry }}">
            </div>
            <div class="custom-file col mt-4">                
                <input type="file" name="file[file_curp_registry]" class="form-control custom-file-input" id="curp" aria-describedby="inputGroupFileAddon01">
                <label class="custom-file-label" for="curp" data-browse="Subir documento"></label>
            </div>
            <div class="col mt-4">
                @if (!is_null($profileRegistros->file_curp_registry))
                    @if ($profile->getFileExists($profileRegistros->file_curp_registry))
                        <a class="btn morado" href="{{ asset('/uploads/profile/'. $profileRegistros->file_curp_registry) }}" target="_blank">
                            <i class="fa fa-file-alt"></i> Ver
                        </a>
                    @endif
                @endif
            </div>
        </div>
    
        <div class="form-row mt-4">
            {{-- <div class="col">
                <label for="">INFONAVIT</label>
                <input type="text" name="infonavit_registry" id="infonavit_registry" class="form-control" value="{{ $profileRegistros->infonavit_registry }}">
            </div>
            <div class="custom-file col mt-4">                
                <input type="file" name="file[file_infonavit_registry]" class="form-control custom-file-input" id="infonavit" aria-describedby="inputGroupFileAddon01">
                <label class="custom-file-label" for="infonavit" data-browse="Subir documento"></label>
            </div>
            <div class="col mt-4">
                @if (!is_null($profileRegistros->file_infonavit_registry))
                    @if ($profile->getFileExists($profileRegistros->file_infonavit_registry))
                    <a class="btn morado" href="{{ asset('/uploads/profile/'. $profileRegistros->file_infonavit_registry) }}" target="_blank">
                            <i class="fa fa-file-alt"></i> Ver
                        </a>
                    @endif
                @endif
            </div> --}}
        </div>

        <div class="form-row mt-4">
            <div class="col">
                <label for="">RFC</label>
                <input type="text" name="rfc_registry" id="rfc_registry" class="form-control" value="{{ $profileRegistros->rfc_registry }}">
            </div>
            <div class="custom-file col mt-4">                
                <input type="file" name="file[file_rfc_registry]" class="form-control custom-file-input" id="rfc" aria-describedby="inputGroupFileAddon01">
                <label class="custom-file-label" for="rfc" data-browse="Subir documento"></label>
            </div>
            <div class="col mt-4">
                @if (!is_null($profileRegistros->file_rfc_registry))
                    @if ($profile->getFileExists($profileRegistros->file_rfc_registry))
                    <a class="btn morado" href="{{ asset('/uploads/profile/'. $profileRegistros->file_rfc_registry) }}" target="_blank">    
                            <i class="fa fa-file-alt"></i> Ver
                        </a>
                    @endif
                @endif
            </div>
        </div>

        <div class="form-row mt-4">
            {{-- <div class="col">
                <label for="">FONACOT</label>
                <input type="text" name="fonacot_registry" id="fonacot_registry" class="form-control" value="{{ $profileRegistros->fonacot_registry }}">
            </div>
            <div class="custom-file col mt-4">                
                <input type="file" name="file[file_fonacot_registry]" class="form-control custom-file-input" id="fonacot" aria-describedby="inputGroupFileAddon01">
                <label class="custom-file-label" for="fonacot" data-browse="Subir documento"></label>
            </div>
            <div class="col mt-4">
                @if (!is_null($profileRegistros->file_fonacot_registry))
                    @if ($profile->getFileExists($profileRegistros->file_fonacot_registry))
                        <a class="btn morado" href="{{ asset('/uploads/profile/'. $profileRegistros->file_fonacot_registry) }}" target="_blank">
                            <i class="fa fa-file-alt"></i> Ver
                        </a>
                    @endif
                @endif
            </div> --}}
        </div>
        
        <div class="form-row mt-4">
            <div class="col">
                <label for="">IMSS</label>
                <input type="text" name="imss_registry" id="imss_registry" class="form-control" value="{{ $profileRegistros->imss_registry }}">
            </div>
            <div class="custom-file col mt-4">                
                <input type="file" name="file[file_imss_registry]" class="form-control custom-file-input" id="imss" aria-describedby="inputGroupFileAddon01">
                <label class="custom-file-label" for="curp" data-browse="Subir documento"></label>
            </div>
            <div class="col mt-4">
                @if (!is_null($profileRegistros->file_imss_registry))
                    @if ($profile->getFileExists($profileRegistros->file_imss_registry))
                        <a class="btn morado" href="{{ asset('/uploads/profile/'. $profileRegistros->file_imss_registry) }}" target="_blank">
                            <i class="fa fa-file-alt"></i> Ver
                        </a>
                    @endif
                @endif
            </div>
        </div>

        <div class="form-row mt-4">
            {{-- <div class="col">
                <label for="">CLABE interbancaria</label>
                <input type="text" name="clabe_registry" id="clabe_registry" class="form-control" value="{{ $profileRegistros->clabe_registry }}">
            </div>
            <div class="custom-file col mt-4">                
                <input type="file" name="file[file_clabe_registry]" class="form-control custom-file-input" id="clabe" aria-describedby="inputGroupFileAddon01">
                <label class="custom-file-label" for="clabe" data-browse="Subir documento"></label>
            </div>
            <div class="col mt-4">
                @if (!is_null($profileRegistros->file_clabe_registry))
                    @if ($profile->getFileExists($profileRegistros->file_clabe_registry))
                        <a class="btn morado" href="{{ asset('/uploads/profile/'. $profileRegistros->file_clabe_registry) }}" target="_blank">
                            <i class="fa fa-file-alt"></i> Ver
                        </a>
                    @endif
                @endif
            </div> --}}
        </div>
        
        <div class="form-row mt-4">
            <div class="col">
                <label for="">INE</label>
                <input type="text" name="ine_registry" id="ine_registry" class="form-control" value="{{ $profileRegistros->ine_registry }}">
            </div>
            <div class="col">
                <label for="">Expira</label>
                <input type="date" name="ine_exp_registry" id="ine_exp_registry" class="form-control" value="{{ $profileRegistros->ine_exp_registry }}">
            </div>
            <div class="custom-file col mt-4">                
                <input type="file" name="file[file_ine_registry]" class="form-control custom-file-input" id="ine" aria-describedby="inputGroupFileAddon01">
                <label class="custom-file-label" for="ine" data-browse="Subir documento"></label>
            </div>
            <div class="col mt-4">
                @if (!is_null($profileRegistros->file_ine_registry))
                    @if ($profile->getFileExists($profileRegistros->file_ine_registry))
                        <a class="btn morado" href="{{ asset('/uploads/profile/'. $profileRegistros->file_ine_registry) }}" target="_blank">
                            <i class="fa fa-file-alt"></i> Ver
                        </a>
                    @endif
                @endif
            </div>
        </div>

        <div class="form-row mt-4">
            <div class="col">
                <label for="">OTROS</label>
                <input type="text" name="other_registry" id="other_registry" class="form-control" value="{{ $profileRegistros->other_registry }}">
            </div>
            <div class="custom-file col mt-4">                
                <input type="file" name="file[file_other_registry]" class="form-control custom-file-input" id="otros" aria-describedby="inputGroupFileAddon01">
                <label class="custom-file-label" for="otros" data-browse="Subir documento"></label>
            </div>
            <div class="col mt-4">
                @if (!is_null($profileRegistros->file_other_registry))
                    @if ($profile->getFileExists($profileRegistros->file_other_registry))
                        <a class="btn morado" href="{{ asset('/uploads/profile/'. $profileRegistros->file_other_registry) }}" target="_blank">
                            <i class="fa fa-file-alt"></i> Ver
                        </a>
                    @endif
                @endif
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col text-right">
            <button type="submit" class="btn morado">{{ is_null($profileRegistros) ? 'Guardar' : 'Actualizar' }}</button>
        </div>
    </div>
</div>
