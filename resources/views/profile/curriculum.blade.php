<div class="container-fluid">

        <div class="form-row">
            <div class="col-md-10 my-auto">
                <h5><strong>CURRICULUM VITAE</strong></h5>
            </div>
            <div class="col-md-2">
                <div class="col text-right">
                    @if(!isset($show_only))
                        <button type="submit" class="btn morado">{{ ($crear) ? 'Guardar' : 'Actualizar' }}</button>
                    @endif
                </div>
            </div>
        </div>
        <hr class="mt-1" style="border-bottom: 4px solid #373435;">
    
        <div class="form-row my-4">
            <div class="col-md-12">
                <h5><strong>Datos Generales</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h5>
            </div>
        </div>
    
        <div class="form-row">
            <div class="col-12 col-md-3 text-center">
                @if (isset($profile) && !is_null($profile->image))
                    <img src="{{ asset('uploads/profile/'.$profile->image) }}" class="mb-3" style="max-width: 270px; max-height: 183px">
                @else
                    <img src="{{ asset('img/vacantes/sinimagen.png') }}" class="mb-3" style="max-width: 300px; max-height: 183px">
                @endif


                @if(Auth::user()->hasRolePermission('user_admin'))
                <div class="custom-file form-group col">
                    <label class="btn morado float-left" for="my-file-selector">
                        <input id="my-file-selector" name="file[image]" type="file" style="display:none" 
                        onchange="$('#upload-file-info').html(this.files[0].name)">
                        Subir Fotografía
                    </label>
                    <textarea class="form-control" style="resize: none;" id="upload-file-info" cols="30" rows="2" readonly></textarea>
                    <small class="float-left"> <strong> 2MB Max * </strong></small>
                </div>
                @endif
                
            </div>
            <div class="col-12 col-md-3 d-flex flex-column align-content-start justify-content-start">
    
                <label for="name">Nombre(s)</label>
                <input class="form-control mb-3" type="text" name="name" id="name" value="{{ isset($profile) ? $profile->name : (is_null($user) ? null : is_null($user->employee) ? $user->first_name : $user->employee->nombre)}}" readonly={{ is_null($user) ? false : true }}>
    
                <label for="cellphone">Número de Celular</label>
                <input class="form-control mb-3" type="text" name="cellphone" id="cellphone" value="{{ isset($profile) ? $profile->cellphone : (is_null($user) ? null : is_null($user->employee) ? null : $user->employee->celular)}}" {{ is_null($profile) ? '' : 'readonly' }}>
    
                <label for="gender">Sexo:</label>
                <input class="form-control mb-3" type="text" name="gender" id="gender" value="{{ isset($profile) ? $profile->gender : (is_null($user) ? null : is_null($user->employee) ? null : $user->employee->sexo)}}" readonly={{ is_null($user) ? false : true }}>
    
            </div>
            <div class="col-12 col-md-3 d-flex flex-column align-content-start justify-content-start">
    
                <label for="surname_father">Apellido Paterno</label>
                <input class="form-control mb-3" type="text" name="surname_father" id="surname_father" value="{{ isset($profile) ? $profile->surname_father : (is_null($user) ? null : is_null($user->employee) ? $user->last_name : $user->employee->paterno)}}" readonly={{ is_null($user) ? false : true }}>
                
                <label for="phone">Número de Teléfono Fijo</label>
                <input class="form-control mb-3" type="text" name="phone" id="phone" value="{{ isset($profile) ? $profile->phone : (is_null($user) ? null : is_null($user->employee) ? null : $user->employee->telefono)}}" {{ is_null($profile) ? '' : 'readonly' }}>
    
                <label for="date_birth">Fecha de Nacimiento</label>
                <input class="form-control mb-3" type="text" name="date_birth" id="date_birth" value="{{ isset($profile) ? $profile->date_birth : (is_null($user) ? null : is_null($user->employee) ? null : $user->employee->nacimiento) }}" readonly={{ is_null($user) ? false : true }}>
                
            </div>
            <div class="col-12 col-md-3  d-flex flex-column align-items-start justify-content-start">
    
                <label for="surname_mother">Apellido Materno</label>
                <input class="form-control mb-3" type="text" name="surname_mother" id="surname_mother" value="{{ isset($profile) ? $profile->surname_mother : (is_null($user) ? null : is_null($user->employee) ? null : $user->employee->materno)}}" readonly={{ is_null($user) ? false : true }}>
    
                <label for="lname">Correo Eléctronico</label>
                <input class="form-control mb-3" type="text" name="email" id="email" value="{{ isset($profile) ? $profile->email : (is_null($user) ? null : is_null($user->employee) ? $user->email : $user->employee->correoempresa)}}" readonly={{ is_null($user) ? false : true }}>
    
                <label for="rfc">Estado Civil</label>
                <input class="form-control mb-3" type="text" name="rfc" id="rfc" value="{{ (is_null($user) ? null : is_null($user->employee) ? null : $user->employee->civil) }}" disabled>
    
            </div>

            <div class="col-12 col-md-3 offset-md-3  d-flex flex-column align-items-start justify-content-start">
                <label for="age" class="requerido">Edad</label>
                <input type="text" id="age" class="form-control mb-3" value="{{ isset($profile) ? $profile->getAge() : (is_null($user) ? null : is_null($user->employee) ? null : $user->employee->getAge())}}" readonly={{ is_null($user) ? false : true }}>
            </div>
            <div class="col-12 col-md-3  d-flex flex-column align-items-start justify-content-start">
                <label for="imss_registry" class="requerido">IMSS</label>
                <input class="form-control mb-3" type="text" name="imss_registry" id="imss_registry" value="{{ isset($profile->profileRegistros) ? $profile->profileRegistros->imss_registry : null }}" readonly={{ is_null($user) ? false : true }}>

            </div>
            <div class="col-12 col-md-3  d-flex flex-column align-items-start justify-content-start">
                <label for="rfc" class="requerido">RFC</label>
                <input type="text" name="rfc" id="rfc" class="form-control mb-3" value="{{ isset($profile) ? $profile->rfc : (is_null($user) ? null : is_null($user->employee) ? null : $user->employee->rfc)}}" readonly={{ is_null($user) ? false : true }}>
            </div>
        </div>
        



        <div class="form-row mb-4 my-4">
            <div class="col-md-12">
                <h5><strong>Documento CV</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h5>
            </div>
        </div>
        
        <div class="form-row">

            @isset($profile)
                @if (!is_null($profile->file_cv))
                    <div class="col-12 mb-3  col-md-3">

                        <label for="">Documento Adjunto</label>
                        <div class="custom-file col">
                            @if ($profile->getFileExists($profile->file_cv))
                                <a class="btn morado btn-block" href="{{ asset('/uploads/profile/'. $profile->file_cv) }}" target="_blank">
                                    <i class="fa fa-file-alt"></i> Ver
                                </a>
                            @endif
                        </div>

                    </div>
                @endif
            @endisset
            <div class="col-12 mb-3 col-md-6 pull-right">
                @if(isset($profile))
                    @if (!is_null($profile->file_cv))
                        <label for="files_cv">Actualizar CV</label>
                    @else
                        <label for="files_cv">Adjuntar CV</label>
                    @endif
                @else
                    <label for="files_cv">Adjuntar CV</label>
                @endif
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file[file_cv]" id="files_cv">
                    <label class="custom-file-label" for="files_cv" data-browse="Subir documento"></label>
                </div>

            </div>

        </div>






        <div class="form-row my-4">
            <div class="col-md-12">
                <h5><strong>Domicilio Actual</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h5>
            </div>
        </div>
   
        <div class="form-row">
            <div class="col-12 mb-3 col-md-6">
                <label for="address">Dirección</label>
                <input class="form-control mb-3" type="text" name="address" id="address_c" value="{{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->address : null }}">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="num_outside">No. Casa</label>
                <input class="form-control mb-3" type="text" name="num_outside" id="num_outside_c" value="{{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->num_outside : null }}">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="num_inside">Zona</label>
                <input class="form-control mb-3" type="text" name="num_inside" id="num_inside_c" value="{{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->num_inside : null }}" >
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="colony">Colonia</label>
                <input class="form-control mb-3" type="text" name="colony" id="colony_c" value="{{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->colony : null }}">
            </div>
        </div>
            
        <div class="form-row">
            <div class="col-12 mb-3 col-md-3">
                <label for="zip_code">Código Postal</label>
                <input class="form-control mb-3" type="number" name="zip_code" id="zip_code_c" value="{{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->zip_code : null }}">
            </div>
    
            <div class="col-12 mb-3 col-md-3">
                <label for="state">Departamento</label>
                <select name="state" id="state_c" class="selectpicker form-control mb-3" data-live-search="true">
                    @foreach ($estados as $id => $estado)
                        @isset($profile->perfilAdicional->state)
                            <option value="{{ $id }}" {{ ($id==$profile->perfilAdicional->estado->id) ? 'selected' : null }}> {{ $estado }}</option>
                        @else
                            <option disabled value="" selected hidden>Debe seleccionar un Departamento...</option>
                            <option value="{{ $id }}"> {{ $estado }}</option>
                        @endisset
                    @endforeach
                </select>
            </div>
            <div class="col-12 mb-3 col-md-3">
                <label for="city">Municipio</label>
                <select class="selectpicker form-control mb-3" name="city" id="city_c" data-live-search="true">
                    @isset($profile->perfilAdicional)
                        @foreach ($municipios as $id => $municipio)
                            <option value="{{ $id }}" {{ ($id==$profile->perfilAdicional->municipio->id) ? 'selected' : null }}> {{ $municipio }}</option>
                        @endforeach
                    @else
                        <option disabled value="" selected hidden>Debe seleccionar un Municipio...</option>
                    @endisset
                </select>
            </div>
                
            <div class="col-12 mb-3 col-md-3">
                <label for=""><small>Comprobante de Domicilio</small></label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file_address" id="file_address">
                    <label class="custom-file-label" for="file_address" data-browse="Subir documento"></label>
                </div>
            </div>
    
            <div class="col-12 col-md-2">
                @isset($profile->perfilAdicional)
                    @if (!is_null($profile->perfilAdicional->file_address))
                        @if ($profile->getFileExists($profile->perfilAdicional->file_address))
                            <a class="btn morado" href="{{ asset('/uploads/profile/'. $profile->perfilAdicional->file_address) }}" target="_blank">
                                <i class="fa fa-file-alt"></i> Ver
                            </a>
                        @endif
                    @endif
                @endisset
            </div>
        </div>
    
        <div class="form-row my-4">
            <div class="col-md-12">
                <h5><strong>Estudios</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h5>
            </div>
        </div>
    
        <div class="float-right">
            <button type="button" class="btn btn-danger mt-4" id="agregar_escolar_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse ($profile->perfilEscuelaBasica as $escuela) --}}
        @forelse (isset($profile) ? $profile->perfilEscuelaBasica : [] as $escuela)
            <div id="escolar_c">
                <hr class="separador {{(!$loop->first?'':'d-none')}}">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-2">
                        <input type="hidden" name="type[]" value="basica">
                        <label for="studio">Nivel Alcanzado</label>
                        <select name="studio[]" id="studio_c" class="form-control mb-3">
                            {{-- <option disabled value="" selected hidden>Seleccione una opción...</option> --}}
                            <option value="Primaria" class="s-Primaria-c" {{ ($escuela->studio=='Primaria') ? 'selected' : null }}>Primaria</option>
                            <option value="Secundaria" class="s-Secundaria-c" {{ ($escuela->studio=='Secundaria') ? 'selected' : null }}>Secundaria</option>
                            <option value="Preparatoria" class="s-Preparatoria-c" {{ ($escuela->studio=='Preparatoria') ? 'selected' : null }}>Diversificado</option>
                            <option value="Técnico" class="s-Técnico-c" {{ ($escuela->studio=='Técnico') ? 'selected' : null }}>Técnico</option>
                        </select>
                    </div>
                    <div class="col-12 mb-3 col-md-5">
                        <label for="school">Institución que Expidió Constancia</label>
                        <input type="text" name="school[]" id="school_c" class="form-control mb-3" value="{{ $escuela->school }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="date_end">Fecha Terminación</label>
                        <input type="date" name="date_end[]" id="date_end_c" class="form-control mb-3" value="{{ $escuela->date_end }}">
                    </div>
                    <div class="col-12 mb-3 col-md-6">
                        <label for="">Agregar Comprobante del Último Grado de Estudios</label>
                        <div class="custom-file col">
                            <input class="form-control-file custom-file-input" type="file" name="file_studio[]" id="file_studio_c">
                            <label class="file custom-file-label" for="file_studio" data-browse="Subir documento"></label>
                        </div>
                    </div>
                    <div class="col-12 mb-3 mt-4 col-md-5 ">
                        @if (!is_null($escuela->file_studio))
                            @if ($profile->getFileExists($escuela->file_studio))
                                <a class="btn morado ver" href="{{ asset('/uploads/profile/'. $escuela->file_studio) }}" target="_blank">
                                    <i class="fa fa-file-alt"></i> Ver
                                </a>
                            @endif
                        @endif
                    </div>
    
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right {{(!$loop->first?'':'d-none')}}">
                        <button type="button" class="btn btn-danger borrar_escolar_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
    
                </div>
            </div>
        @empty
            <div id="escolar_c">
                <hr class="separador d-none">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-2">
                        <input type="hidden" name="type[]" value="basica">
                        <label for="studio">Nivel Alcanzado</label>
                        <select name="studio[]" id="studio_c" class="form-control mb-3">
                            <option disabled value="" selected hidden>Seleccione una opción...</option>
                            <option value="Primaria" class="s-Primaria-c">Primaria</option>
                            <option value="Secundaria" class="s-Secundaria-c">Secundaria</option>
                            <option value="Preparatoria" class="s-Preparatoria-c">Diversificado</option>
                            <option value="Técnico" class="s-Técnico-c">Técnico</option>
                        </select>
                    </div>
                    <div class="col-12 mb-3 col-md-5">
                        <label for="school">Institución que Expidió Constancia</label>
                        <input type="text" name="school[]" id="school_c" class="form-control mb-3">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="date_end">Fecha Terminación</label>
                        <input type="date" name="date_end[]" id="date_end_c" class="form-control mb-3">
                    </div> 
                    <div class="col-12 mb-3 col-md-6">
                        <label for="">Agregar Comprobante del Último Grado de Estudios</label>
                        <div class="custom-file col">
                            <input class="form-control-file custom-file-input" type="file" name="file_studio[]" id="file_studio_c">
                            <label class="custom-file-label" for="file_studio" data-browse="Subir documento"></label>
                        </div>
                    </div>
    
                    <div class="col-12 mb-3 col-md-6 text-right d-none">
                        <button type="button" class="btn btn-danger borrar_escolar_c mt-4">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
    
                </div>
            </div>
        @endforelse
    
        <div id="destino_escolar_c"></div>
    
        <div class="form-row my-4">
            <div class="col-md-12">
                <h5><strong>Universitarios</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h5>
            </div>
        </div>
    
        <div class="float-right">
            <button type="button" class="btn btn-danger mt-4" id="agregar_escolar_uni_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse ($profile->perfilEscuelaSuperior as $escuela ) --}}
        @forelse (isset($profile) ? $profile->perfilEscuelaSuperior : [] as $escuela )
            <div id="escolar_uni_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <input type="hidden" name="type_uni[]" value="superior">
                        <label for="studio_uni">Tipo de carrera:</label>
                        <select name="studio_uni[]" id="studio_uni_c" class="form-control">
                            {{-- <option disabled value="" selected hidden>Seleccione una opción...</option> --}}
                            <option value="Licenciatura" class="s-Licenciatura-c" {{ ($escuela->studio=='Licenciatura') ? 'selected' : null }}>Licenciatura</option>
                            <option value="Maestria" class="s-Maestria-c" {{ ($escuela->studio=='Maestria') ? 'selected' : null }}>Maestria</option>
                            <option value="Doctorado" class="s-Doctorado-c" {{ ($escuela->studio=='Doctorado') ? 'selected' : null }}>Doctorado/Posgrado</option>
                                <option value="Diplomado" class="s-Diplomado-c" {{ ($escuela->studio=='Diplomado') ? 'selected' : null }}>Diplomado</option>
                        </select>
                    </div>
                    <div class="col-12 mb-3 col-md-6">
                        <label for="career">Carrera:</label>
                        <input class="form-control mb-3" type="text" name="career_uni[]" id="career_uni_c" value="{{ $escuela->career }}">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_escolar_uni_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                    <div class="col-12 mb-3 col-md-4">
                        <label for="">Agregar comprobante de estudios:</label>
                        <div class="custom-file col">
                            <input class="form-control-file custom-file-input" type="file" name="file_studio_uni[]" id="file_studio_uni">
                            <label class="custom-file-label" for="file_studio_uni" data-browse="Subir documento"></label>
                        </div>
                    </div>
                    <div class="col-12 mb-3 mt-4 col-md-2">
                        @if (!is_null($escuela->file_studio))
                            @if ($profile->getFileExists($escuela->file_studio))
                                <a class="btn morado ver" href="{{ asset('/uploads/profile/'. $escuela->file_studio) }}" target="_blank">
                                    <i class="fa fa-file-alt"></i> Ver
                                </a>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        @empty 
            <div id="escolar_uni_c">
                <hr class="separador d-none">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <input type="hidden" name="type_uni[]" value="superior">
                        <label for="studio_uni_c">Tipo de carrera:</label>
                        <select name="studio_uni[]" id="studio_uni_c" class="form-control">
                            <option disabled value="" selected hidden>Seleccione una opción...</option>
                            <option value="Licenciatura" class="s-Licenciatura-c">Licenciatura</option>
                            <option value="Maestria" class="s-Maestria-c">Maestria</option>
                            <option value="Doctorado" class="s-Doctorado-c">Doctorado/Posgrado</option>
                            <option value="Diplomado" class="s-Diplomado-c">Diplomado</option>
                        </select>
                    </div>
                    <div class="col-12 mb-3 col-md-6">
                        <label for="career">Carrera:</label>
                        <input class="form-control mb-3" type="text" name="career_uni[]" id="career_uni_c">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right d-none">
                        <button type="button" class="btn btn-danger borrar_escolar_uni_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                    <div class="col-12 mb-3 col-md-6">
                        <label for="">Agregar comprobante de estudios:</label>
                        <div class="custom-file col">
                            <input class="form-control-file custom-file-input" type="file" name="file_studio_uni[]" id="file_studio_uni">
                            <label class="custom-file-label" for="file_studio_uni" data-browse="Subir documento"></label>
                        </div>
                    </div>
                </div>
            </div>
        @endforelse
        <div id="destino_escolar_uni_c"></div>
    
        <div class="form-row my-4">
            <div class="col-md-12">
                <h5><strong>Otros Conocimientos</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h5>
            </div>
        </div>
    
        <div class="float-right">
            <button type="button" class="btn btn-danger mt-4" id="agregar_language_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse ($profile->perfilLenguaje as $idioma ) --}}
        @forelse (isset($profile) ? $profile->perfilLenguaje : [] as $idioma )
            <div id="language_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-5">
                        <label for="language">Idioma:</label>
                        <input type="text" name="language[]" id="lenguaje_c" class="form-control" value="{{ $idioma->language }}">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="spoken">Hablado:</label>
                        <input type="number" min="1" max="100" name="spoken[]" id="spoken_c" class="form-control" value="{{ $idioma->spoken }}">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="reading">Lectura:</label>
                        <input type="number" min="1" max="100" name="reading[]" id="reading_c" class="form-control" value={{ $idioma->reading }}>
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="writing">Escritura:</label>
                        <input type="number" min="1" max="100" name="writing[]" id="writing_c" class="form-control" value="{{ $idioma->writing }}">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_language_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @empty
            <div id="language_c">
                <hr class="separador d-none">
                <div class="form-row">
                    <div class="col-2 col-md-12">
                        Idioma
                    </div>
    
                    <div class="col-12 col-md">
                        <div class="form-group">
                            <label for="reading" class="mr-2 font-weight-bold">A</label>
                            <input type="text" name="language[]" id="language_c" class="form-control">
                        </div>
                    </div>
                    
                    <div class="col-12 col-md">
                        <div class="form-group">
                            <label for="reading" class="mr-2 font-weight-bold">B</label>
                            <div class="input-group col-md-12 border-0 input-group-text bg-check mr-0">
                                <span>Lectura</span>
                                <input type="number" min="1" max="100" name="reading[]" id="reading_c" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>
    
                    <div class="col-12 col-md">
                        <div class="form-group">
                            <label for="writing_c" class="mr-2 font-weight-bold">C</label>
                            <div class="input-group col-md-12 border-0 input-group-text bg-check">
                                <span>Escritura</span>
                                <input type="number" min="1" max="100" name="writing[]" id="writing_c" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>
    
                    <div class="col-12 col-md">
                        <div class="form-group">
                            <label for="writing_c" class="mr-2 font-weight-bold">D</label>
                            <div class="input-group col-md-12 border-0 input-group-text bg-check">
                                <span>Conversación</span>
                                <input type="number" min="1" max="100" name="spoken[]" id="spoken_c" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>
    
                    <div class="col-12 col-md-1 mt-4 text-right d-none">
                        <button type="button" class="btn btn-danger borrar_language_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
    
                </div>
            </div>
        @endforelse
    
        <div id="destino_language_c"></div>
    
    
        <div class="float-right">
            <button type="button" class="btn btn-danger mt-4" id="agregar_knowledge_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse ($profile->perfilConocimientos as $conocimiento) --}}
        @forelse (isset($profile) ? $profile->perfilConocimientos : [] as $conocimiento)
            <div id="escolar_knowledge_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-4">
                        <label for="knowledge_type">Competencias Desarrolladas:</label>
                        <select name="knowledge_type[]" id="knowledge_type_c" class="form-control">
                            <option disabled value="" selected hidden>Seleccione una opción...</option>
                            <option value="Maquinaria" {{ ($conocimiento->knowledge_type == "Maquinaria") ? 'selected' : null }}>Maquinaria y equipo</option>
                            <option value="Programas" {{ ($conocimiento->knowledge_type == "Programas") ? 'selected' : null }}>Programas y sistemas</option>
                            <option value="Funciones" {{ ($conocimiento->knowledge_type == "Funciones") ? 'selected' : null }}>Funciones de oficina</option>
                        </select>
                    </div>
                    
                    <div class="col-12 mb-3 col-md-7">
                        <label for="knowledge_name">Detalle:</label>     
                        <input type="text" name="knowledge_name[]" id="knowledge_name_c" class="form-control" value="{{ $conocimiento->knowledge_name }}">
                    </div>
        
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_knowledge_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @empty
            <div id="escolar_knowledge_c">
                <hr class="separador d-none">
                <div class="form-row">
                    <div class="col-12 col-md-4">
                        <label for="knowledge_type">Competencias Desarrolladas</label>
                        <select name="knowledge_type[]" id="knowledge_type_c" class="form-control">
                            <option disabled value="" selected hidden>Selecciona una opción...</option>
                            <option value="Maquinaria">Maquinaria y equipo</option>
                            <option value="Programas">Programas y sistemas</option>
                            <option value="Funciones">Funciones de oficina</option>
                        </select>
                    </div>
                    
                    <div class="col-12 mb-3 col-md-7">
                        <label for="knowledge_name">Detalle:</label>     
                        <input type="text" name="knowledge_name[]" id="knowledge_name_c" class="form-control">
                    </div>
    
                    {{-- <input type="hidden" name="knowledge_name[]" id="knowledge_name_c" class="form-control">  --}}

                    {{-- Oculta porque en diseño no viene --}}
        
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right d-none">
                        <button type="button" class="btn btn-danger borrar_knowledge_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @endforelse
        <div id="destino_knowledge_c"></div>
    
        <div class="form-row my-4">
            <div class="col-md-12">
                <h5><strong>Experiencia Laboral</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h5>
            </div>
        </div>
    
        <div class="float-right">
            <button type="button" class="btn btn-danger mt-4" id="agregar_experience_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse($profile->perfilExperiencia as $experiencia) --}}
        @forelse(isset($profile) ? $profile->perfilExperiencia : [] as $experiencia)
            <div id="escolar_experience_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-5">
                        <label for="job">Puesto ocupado:</label>
                        <input class="form-control mb-3" type="text" name="job[]" id="job_c" value="{{ $experiencia->job }}">
                    </div>
                    <div class="col-12 mb-3 col-md-6">
                        <label for="company">Empresa/domicilio:</label>
                        <input class="form-control mb-3" type="text" name="company[]" id="company_c" value="{{ $experiencia->company }}">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_experience_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <label for="date_begin_experience">Fecha inicio:</label>
                        <input class="form-control mb-3" type="date" name="date_begin_experience[]" id="date_begin_experience_c" value="{{ $experiencia->date_begin_experience }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="date_end_experience">Fecha fin:</label>
                        <input class="form-control mb-3" type="date" name="date_end_experience[]" id="date_end_experience_c" value="{{ $experiencia->date_end_experience }}">
                        <span id="edad_max_error" class="clase_error"><strong>Mayor o igual a edad mínima</strong></span>
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="salary">Sueldo:</label>
                        <input class="form-control mb-3" type="text" name="salary[]" id="salary_c" value="{{ $experiencia->salary }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reason_separation">Motivo de separación:</label>
                        <input class="form-control mb-3" type="text" name="reason_separation[]" id="reason_separation_c" value="{{ $experiencia->reason_separation }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-12">
                        <label for="activity">Descripción de actividades realizadas:</label>
                        <input class="form-control mb-3" type="text" name="activity[]" id="activity_c" value="{{ $experiencia->activity }}">
                    </div>
                </div>
            </div>
        @empty
            <div id="escolar_experience_c">
                <hr class="separador d-none">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-5">
                        <label for="job">Puesto Ocupado</label>
                        <input class="form-control mb-3" type="text" name="job[]" id="job_c">
                    </div>
                    <div class="col-12 mb-3 col-md-6">
                        <label for="company">Empresa/Domicilio</label>
                        <input class="form-control mb-3" type="text" name="company[]" id="company_c">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right d-none">
                        <button type="button" class="btn btn-danger borrar_experience_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <label for="date_begin_experience">Fecha Inicio</label>
                        <input class="form-control mb-3" type="date" name="date_begin_experience[]" id="date_begin_experience_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="date_end_experience">Fecha Fin</label>
                        <input class="form-control mb-3" type="date" name="date_end_experience[]" id="date_end_experience_c">
                        <span id="edad_max_error" class="clase_error"><strong>Mayor o igual a edad mínima</strong></span>
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="salary">Sueldo</label>
                        <input class="form-control mb-3" type="text" name="salary[]" id="salary_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reason_separation">Motivo de Separación</label>
                        <input class="form-control mb-3" type="text" name="reason_separation[]" id="reason_separation_c">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-12">
                        <label for="activity">Principales actividades y Responsabilidades del Puesto</label>
                        <textarea class="form-control mb-3" name="activity[]" id="activity_c" rows="4"></textarea>
                    </div>
                </div>
            </div>
        @endforelse
    
        <div id="destino_experience_c"></div>
    
        <div class="form-row">
            <div class="col-12 mb-3 col-md-12">
                <p class="text-white p-3 bg-gray">Anota al menos dos últimos empleos</p>
            </div>
        </div>
    
        <div class="form-row mb-4">
            <div class="col-md-12">
                <h5><strong>Referencias Personales</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h5>
            </div>
        </div>
    
        <div class="float-right">
            <button type="button" class="btn btn-danger mt-4" id="agregar_reference_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse ($profile->perfilReferencias as $referencia) --}}
        @forelse (isset($profile) ? $profile->perfilReferencias : [] as $referencia)
            <div id="escolar_reference_c">
                <div class="form-row">
                  
                    <input class="form-control mb-3" type="hidden" name="type_ref[]" value="Personal">

                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_name">Nombres:</label>
                        <input class="form-control mb-3" type="text" name="reference_name[]" id="reference_name_c" value="{{ $referencia->reference_name }}">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="reference_phone">Teléfono:</label>
                        <input class="form-control mb-3" type="text" name="reference_phone[]" id="reference_phone_c" value="{{ $referencia->reference_phone }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_time_meet">Tiempo de conocerla:</label>
                        <input class="form-control mb-3" type="text" name="reference_time_meet[]" id="reference_time_meet_c" value="{{ $referencia->reference_time_meet }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_occupation">Ocupación:</label>
                        <input class="form-control mb-3" type="text" name="reference_occupation[]" id="reference_occupation_c" value="{{ $referencia->reference_occupation }}">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right pull-right">
                        <button type="button" class="btn btn-danger borrar_reference_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @empty
            <div id="escolar_reference_c">
                <hr class="separador d-none">
                <div class="form-row">
                </div>
                <div class="form-row">
                    
                    <input class="form-control mb-3" type="hidden" name="type_ref[]" value="Personal">

                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_name">Nombres:</label>
                        <input class="form-control mb-3" type="text" name="reference_name[]" id="reference_name_c">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="reference_phone">Teléfono:</label>
                        <input class="form-control mb-3" type="text" name="reference_phone[]" id="reference_phone_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_time_meet">Tiempo de conocerla:</label>
                        <input class="form-control mb-3" type="text" name="reference_time_meet[]" id="reference_time_meet_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_occupation">Ocupación:</label>
                        <input class="form-control mb-3" type="text" name="reference_occupation[]" id="reference_occupation_c">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_reference_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @endforelse
        
        <div id="destino_reference_c"></div>

        <div class="form-row mb-4">
            <div class="col-md-12">
                <h5><strong>Referencias Laborales</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h5>
            </div>
        </div>
    
        <div class="float-right">
            <button type="button" class="btn btn-danger mt-4" id="agregar_work_reference_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>

        @forelse (isset($profile) ? $profile->perfilReferenciasLaborales : [] as $referencia)
            <div id="escolar_work_reference_c">
                <div class="form-row">

                    <input class="form-control mb-3" type="hidden" name="type_ref[]" value="Laboral">

                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_name">Nombres:</label>
                        <input class="form-control mb-3" type="text" name="reference_name[]" id="reference_name_c" value="{{ $referencia->reference_name }}">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="reference_phone">Teléfono:</label>
                        <input class="form-control mb-3" type="text" name="reference_phone[]" id="reference_phone_c" value="{{ $referencia->reference_phone }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_time_meet">Tiempo de conocerla:</label>
                        <input class="form-control mb-3" type="text" name="reference_time_meet[]" id="reference_time_meet_c" value="{{ $referencia->reference_time_meet }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_occupation">Ocupación:</label>
                        <input class="form-control mb-3" type="text" name="reference_occupation[]" id="reference_occupation_c" value="{{ $referencia->reference_occupation }}">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right pull-right">
                        <button type="button" class="btn btn-danger borrar_work_reference_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @empty
            <div id="escolar_work_reference_c">
                <hr class="separador d-none">
                <div class="form-row">

                   
                    <input class="form-control mb-3" type="hidden" name="type_ref[]" value="Laboral">

                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_name">Nombre:</label>
                        <input class="form-control mb-3" type="text" name="reference_name[]" id="reference_name_c">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="reference_phone">Teléfono:</label>
                        <input class="form-control mb-3" type="text" name="reference_phone[]" id="reference_phone_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_time_meet">Tiempo de conocerla:</label>
                        <input class="form-control mb-3" type="text" name="reference_time_meet[]" id="reference_time_meet_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_occupation">Ocupación:</label>
                        <input class="form-control mb-3" type="text" name="reference_occupation[]" id="reference_occupation_c">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_reference_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @endforelse
        
        <div id="destino_work_reference_c"></div>
    
        <div class="form-row">
            <div class="col-12 mb-3 col-md-12">
                <p class="text-white p-3 bg-gray">Anota al menos dos referencias personales y dos laborales</p>
            </div>
        </div>
    
 
    
        {{-- <div class="form-row mb-4">
            <div class="col-md-12">
                <h5><strong>Tiene Parientes Trabajando en la Empresa?</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h5>
            </div>
        </div>

        <div class="form-row">

            <div class="col-1 col-md-1 p-1">
                
                <input class="mb-3" type="checkbox" name="family_working" id="family_working_0_c"  {{ isset($profile->perfilSolicitud) ? $profile->perfilSolicitud->family_working == '1' ? 'checked' : null : null }}>

                <label for="family_working_0_c">Sí</label>

            </div>

            <div class="col-12 mb-3 col-md-3" style="display: inline-flex;">

                <label for="family_working_name_c" class="mt-1 pr-2">Empleado</label>


                <select name="family_working_name" id="family_working_name_c" class="selectpicker form-control mb-3" data-live-search="true">

                    @foreach ($Listausuarios as $id => $usuarios)
                        @isset($profile->perfilSolicitud->family_working)
                            <option value="{{ $usuarios->id }}" {{ ($usuarios->id==$profile->perfilSolicitud->family_working_name) ? 'selected' : null }}> {{ $usuarios->nombre }} {{ $usuarios->paterno }}</option>
                        @else
                            <option disabled value="" selected hidden>No tiene Familiar...</option>
                            <option value="{{ $usuarios->id }}"> {{ $usuarios->nombre }} {{ $usuarios->paterno }}</option>
                        @endisset
                    @endforeach

                </select>

            </div>
        </div> --}}

        <h5>Tiene Parientes Trabajando en la Empresa?</h5>
        <div class="form-row">
            <div class="col-1 col-md-1 p-1 mb-3">
                <input class="mb-3" type="checkbox" name="family_working" id="family_working_c"  {{ !is_null($profile) ? !is_null($profile->perfilSolicitud) ? $profile->perfilSolicitud->family_working == '1' ? 'checked' : '' : '' : '' }}>
                <label for="family_working">Sí</label>
            </div>
            <div class="col-12 mb-3 col-md-6 parientes" style="display: inline-flex;">
                <label for="family_working_name" class="mt-1 pr-2">Empleado: </label>
                <select name="family_working_name" id="family_working_name_c" class="selectpicker form-control mb-3" data-live-search="true">
                    @foreach ($Listausuarios as $id => $usuario)
                        @if(!is_null($profile) && !is_null($profile->perfilSolicitud))
                            @if(!is_null($profile->perfilSolicitud->family_working_name))
                                <option value="{{ $id }}" {{ ($id==$profile->perfilSolicitud->family_working_name) ? 'selected' : null }}> {{ $usuario }}</option>
                            @else
                                <option disabled value="" selected hidden>Seleccione una opción...</option>
                            @endif
                        @else
                            <option disabled value="" selected hidden>Seleccione una opción...</option>
                            {{-- <option value="{{ $id }}"> {{ $usuario }}</option> --}}
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        

        <div class="form-row mb-4">
            <div class="col-md-12">
                <h5><strong>Disponibilidad Para</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h5>
            </div>
        </div>

        <div class="form-row">

            <div class="col-3 col-md-3">
                
                <input class="mb-3" type="checkbox" name="availability_travel" id="availability_travel_id" {{ isset($profile->perfilSolicitud) ? $profile->perfilSolicitud->availability_travel == '1' ? 'checked' : null : null}}>
                <label for="availability_travel_id">Viajar</label>
            </div>


            <div class="col-3 col-md-3">
                
                <input class="mb-3" type="checkbox" name="availability_shifts" id="availability_shifts_id" {{ isset($profile->perfilSolicitud) ? $profile->perfilSolicitud->availability_shifts == '1' ? 'checked' : null : null}}>
                <label for="availability_shifts_id">Rolar turno</label>
            </div>



            <div class="col-3 col-md-3">
                
                <input class="mb-3" type="checkbox" name="availability_residence" id="availability_residence_id" {{ isset($profile->perfilSolicitud) ? $profile->perfilSolicitud->availability_residence == '1' ? 'checked' : null : null}}>
                <label for="availability_residence_id">Cambiar de residencia</label>
            </div>

        </div>
        <br>

{{-- 

        <div class="form-row mb-4">
            <div class="col-md-12">
                <h5><strong>Sindicatos</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h5>
            </div>
        </div>
        
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_sindicatos_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        @forelse (isset($profile) ? $profile->perfilSindicatos : [] as $sindicato )
            <div id="sindicato_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-11">
                        <label for="career">Nombre del Sindicato:</label>
                        <input class="form-control mb-3 col-md-6" type="text" name="unionized_name[]" id="career_uni_c" value="{{ $sindicato->unionized_name }}">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_sindicato_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @empty 
            <div id="sindicato_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-11">
                        <label for="career">Nombre del Sindicato:</label>
                        <input class="form-control mb-3 col-md-6" type="text" name="unionized_name[]" id="career_uni_c">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_sindicato_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @endforelse
        <div id="destino_sindicato_c"></div>

 --}}



        <div class="form-row mb-4">
            <div class="col-md-12">
                <h5><strong>Bienes Asignados</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h5>
            </div>
        </div>
        


<table class="table table-striped table-bordered nowrap" id="tableBienes">
        <thead  class="blue_header">
            <tr>
                <th class="blue_header">Código</th>
                <th class="blue_header">Bien</th>
                <th class="blue_header">Entrega</th>
                <th class="blue_header">Estatus</th>
                <th class="blue_header">Opciones</th>
                {{-- <th>ruta</th> --}}
            </tr>
        </thead>
        <tbody>

            @if(!is_null($profile))

                    @if (count($profile->bienes) == 0)
                    <tr>
                        <td colspan="5" align="center">
                            No tienes bienes Asignados
                        </td>
                    </tr>
                @endif


                @foreach($profile->bienes as $bien)

                    <tr>
                        <td> 
                            <a href="#" data-toggle="modal" data-target="#ver-bien-modal" class="verDetalles" data-id="{{ $bien->id }}" data-codigo="{{ $bien->codigo }}" data-bien="{{ $bien->tipobien->nombre }}" data-entrega="{{ Carbon\Carbon::parse($bien->created_at)->format('d-m-Y') }}"> {{ $bien->codigo }} </a>
                        </td>
                        <td>{{ $bien->tipobien->nombre }}</td>
                        <td>{{ Carbon\Carbon::parse($bien->created_at)->format('d-m-Y') }}</td>
                        <td>
                            @if($bien->estatus == 1)
                                <span style="color: green;">Entregado</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{url('NotaEntregaBien/'.$profile->id.'/'.$bien->id)}}" target="_blank" class="btn btn-sm btn-primary">Pdf</a>

                        </td>
                    </tr>
                @endforeach
            @endif
</tbody>
</table>
    
        <div class="form-row mt-5">
            <div class="col-md-12">
                <h6><strong style="color: #244395">Ratifico que todos los datos asentados son verídicos y que, en caso de probarse lo contrario, se cancelará mi registro.</strong><hr class="mt-1" style="border-bottom: 4px solid #373435;"></h6>
            </div>
        </div>
        
        <div class="row">
            <div class="col text-right">
                @if(!isset($show_only))
                    <button type="submit" class="btn morado">{{ ($crear) ? 'Guardar' : 'Actualizar' }}</button>
                @endif
            </div>
        </div>
    </div>

@if(!is_null($profile))

{{-- modal para agregar solicitud de empleo --}}
<div class="modal fade" id="ver-bien-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Detalles Bien</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body">

                   <div class="row">
                        <div class="col-12 col-md-12">
                            <div class="form-group col-12 col-md-12">
                                <label class="an-title font-weight-bold" for="codigo">Nombre Del Bien</label>
                                <input type="text" name="codigo" id="ver-bien" class="form-control InputDetallesBienes">
                            </div>
                        </div>
                        <div class="col-6 col-md-6">
                            <div class="form-group col-12 col-md-12">
                                <label class="an-title font-weight-bold" for="codigo">Código Del Bien</label>
                                <input type="text" name="codigo" id="ver-codigo" class="form-control InputDetallesBienes" >
                            </div>
                        </div>
                        <div class="col-6 col-md-6">
                            <div class="form-group col-12 col-md-12">
                                <label class="an-title font-weight-bold" for="codigo">Fecha Entrega Del Bien</label>
                                <input type="text" name="codigo" id="ver-entrega" class="form-control InputDetallesBienes" >
                            </div>
                        </div>
                    </div>

                    @foreach($profile->bienes as $bien)

                        <div class="row">

                            @foreach ($bien->tipobien->tipobiendetalles as $tipobiendetalles)

                                <div class="form-group dataBienes col-md-6 dataDetallesBienes {{ $bien->id }}" style="display: none">

                                    <label class="an-title font-weight-bold"> {{ $tipobiendetalles->variable->etiqueta }}</label>                                    
                                    @if(isset($tipobiendetalles->variable->valores))
                                    <input type="text" name="" id="" value="{{ $tipobiendetalles->variable->valores->valor }}" class="form-control InputDetallesBienes">
                                    @endif
                                </div>

                            @endforeach

                        </div>

                    @endforeach

                </div>

                <div class="modal-footer">
                    
                    <button type="button" data-dismiss="modal" class="btn btn-success">Aceptar</button>
                </div>

        </div>
    </div>
</div> @endif