@extends('profile.app')
@section('content')
	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	<br>
	
	<div class="row">
		<h3><center>VACANTES DE PERSONAL</center></h3>
	</div>
	<div class="container-fluid">
	<br><br>
	<table class="table table-striped table-bordered dt-responsive" id="tableVacantes">
		<thead>
			<tr>
				<th>Vacante</th>
				<th>Puesto</th>
				<th>Tipo Vacante</th>
				<th>Horario</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			@foreach($vacantes as $pos => $vacante)
				<tr>
					<td>{{ $vacante->id }}</td>
					<td>{{ $vacante->requisicion->puesto }}</td>
					<td>{{ $vacante->requisicion->tipo_vacante }}</td>
					<td>{{ $vacante->requisicion->horario_descripcion }} </td>
					{{--  we will also add show, edit, and delete buttons  --}}
					<td>
						<a class="btn btn-small btn-success" href="{{ url('vacantes/' . $vacante->id . '/edit') }}">Ver</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
	$('#tableVacantes').DataTable({
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
   	});
</script>
@endsection