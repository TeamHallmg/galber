<div class="container-fluid">
    <h5>SOLICITUD DE EMPLEO:</h5>
    {{-- @isset($profile)
        <div class="form-row">
            <div class="col-12 col-md-3 text-center">
                @if (isset($profile) && !is_null($profile->image))
                    <img src="{{ asset('storage/profile/'.$profile->image) }}" class="mb-3" style="max-width: 270px; max-height: 183px">
                @else
                    <img src="{{ asset('img/vacantes/sinimagen.png') }}" class="mb-3" style="max-width: 300px; max-height: 183px">
                @endif
                <div class="custom-file form-group col">                
                    <input type="file" name="image" class="form-control custom-file-input" id="image" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="image" data-browse="Actualizar"></label>
                </div>
            </div>
            <div class="col-12 col-md-3 d-flex flex-column align-content-start justify-content-start">
                <label for="name">Nombre:</label>
                <input class="form-control mb-3" type="text" name="name" id="name" value="{{ isset($profile) ? $profile->name : null}}" readonly>
                <label for="cellphone">Número de celular:</label>
                <input class="form-control mb-3" type="text" name="cellphone" id="cellphone" value="{{ isset($profile) ? $profile->cellphone : null}}" readonly>
                <label for="gender">Sexo:</label>
                <input class="form-control mb-3" type="text" name="gender" id="gender" value="{{ isset($profile) ? $profile->gender : null}}" readonly>
            </div>
            <div class="col-12 col-md-3 d-flex flex-column align-content-start justify-content-start">
                <label for="surname_father">Apellido Paterno:</label>
                <input class="form-control mb-3" type="text" name="surname_father" id="surname_father" value="{{ isset($profile) ? $profile->surname_father : null}}" readonly>
                <label for="phone">Número de teléfono fijo:</label>
                <input class="form-control mb-3" type="text" name="phone" id="phone" value="{{ isset($profile) ? $profile->phone : null}}" readonly>
                <label for="date_birth">Fecha de Nacimiento:</label>
                <input class="form-control mb-3" type="text" name="date_birth" id="date_birth" value="{{ isset($profile) ? Carbon\Carbon::createFromFormat('Y-m-d', $profile->date_birth)->format('d-m-Y') : null }}" readonly>
            </div>
            <div class="col-12 col-md-3  d-flex flex-column align-items-start justify-content-start">
                <label for="surname_mother">Apellido Materno:</label>
                <input class="form-control mb-3" type="text" name="surname_mother" id="surname_mother" value="{{ isset($profile) ? $profile->surname_mother : null}}" readonly>
                <label for="lname">Correo Eléctronico:</label>
                <input class="form-control mb-3" type="text" name="email" id="email" value="{{ isset($profile) ? $profile->email : null}}" readonly>
                <label for="rfc">RFC:</label>
                <input class="form-control mb-3" type="text" name="rfc" id="rfc" value="{{ isset($profile) ? $profile->rfc : null}}" readonly>
            </div>
        </div>

        <h5>DOMICILIO ACTUAL</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-6">
                <label for="address">Dirección:</label>
                <input class="form-control mb-3" type="text" name="address" id="address">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="number_ext">No. Casa</label>
                <input class="form-control mb-3" type="text" name="number_ext" id="number_ext">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="number_int">Zona</label>
                <input class="form-control mb-3" type="text" name="number_int" id="number_int">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="colony">Colonia</label>
                <input class="form-control mb-3" type="text" name="colony" id="colony">
            </div>
        </div>

        <div class="form-row">
            <div class="col-12 mb-3 col-md-2">
                <label for="state">Estado:</label>
                <select class="form-control mb-3" name="state" id="state"></select>
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="city">Ciudad:</label>
                <select class="form-control mb-3" name="state" id="state"></select>
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="zip_code">Codigo postal</label>
                <input class="form-control mb-3" type="text" name="zip_code" id="zip_code">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="">Comprobante de domicilio</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file_address" id="file_address">
                    <label class="custom-file-label" for="file_address" data-browse="Buscar"></label>
                </div>
            </div>
        </div>
        <br><br>

        <div class="row px-0 mb-5">
            <div class="col-12 py-3" style="background-color:#51A8B1;color:#FFFFFF">
                <span class="font-weight-bold" style="font-size:24px">Si solamente subió curriculum vitae, favor de llenar los datos:</span>
            </div>
        </div>

        <h5>PREPARACIÓN</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-6">
                <label for="primaria">Primaria:</label>
                <input class="form-control mb-3" type="text" name="primaria" id="primaria">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="endDateP">Fecha de terminación:</label>
                <input class="form-control mb-3" type="date" name="endDateP" id="endDateP">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="comp">Comprobante:</label>
                <div class="form-row">
                    <div class="col-1 col-md-6">
                        <label for="yes">Sí</label>
                        <input class="mb-3" type="checkbox" name="yes" id="yes">
                    </div>
                    <div class="col-1 col-md-6">
                        <label for="no">No</label>
                        <input class="mb-3" type="checkbox" name="no" id="no">
                    </div>
                </div>
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="">Agregar comprobante de estudios:</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file_address" id="file_address">
                    <label class="custom-file-label" for="file_address" data-browse="Buscar"></label>
                </div>
            </div>
        </div>
        <hr>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-6">
                <label for="secundaria">Secundaria:</label>
                <input class="form-control mb-3" type="text" name="secundaria" id="secundaria">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="endDateS">Fecha de terminación:</label>
                <input class="form-control mb-3" type="date" name="endDateS" id="endDateS">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="comp">Comprobante:</label>
                <div class="form-row">
                    <div class="col-1 col-md-6">
                        <label for="yes">Sí</label>
                        <input class="mb-3" type="checkbox" name="yes" id="yes">
                    </div>
                    <div class="col-1 col-md-6">
                        <label for="no">No</label>
                        <input class="mb-3" type="checkbox" name="no" id="no">
                    </div>
                </div>
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="">Agregar comprobante de estudios:</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file_address" id="file_address">
                    <label class="custom-file-label" for="file_address" data-browse="Buscar"></label>
                </div>
            </div>
        </div>
        <hr>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-6">
                <label for="secundaria">Preparatoria:</label>
                <input class="form-control mb-3" type="text" name="secundaria" id="secundaria">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="endDateS">Fecha de terminación:</label>
                <input class="form-control mb-3" type="date" name="endDate" id="endDate">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="comp">Comprobante:</label>
                <div class="form-row">
                    <div class="col-1 col-md-6">
                        <label for="yes">Sí</label>
                        <input class="mb-3" type="checkbox" name="yes" id="yes">
                    </div>
                    <div class="col-1 col-md-6">
                        <label for="no">No</label>
                        <input class="mb-3" type="checkbox" name="no" id="no">
                    </div>
                </div>
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="">Agregar comprobante de estudios:</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file_address" id="file_address">
                    <label class="custom-file-label" for="file_address" data-browse="Buscar"></label>
                </div>
            </div>
        </div>
        <hr>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-6">
                <label for="secundaria">Estudios técnicos:</label>
                <input class="form-control mb-3" type="text" name="secundaria" id="secundaria">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="endDateS">Fecha de terminación:</label>
                <input class="form-control mb-3" type="date" name="endDate" id="endDate">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="comp">Comprobante:</label>
                <div class="form-row">
                    <div class="col-1 col-md-6">
                        <label for="yes">Sí</label>
                        <input class="mb-3" type="checkbox" name="yes" id="yes">
                    </div>
                    <div class="col-1 col-md-6">
                        <label for="no">No</label>
                        <input class="mb-3" type="checkbox" name="no" id="no">
                    </div>
                </div>
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="">Agregar comprobante de estudios:</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file_address" id="file_address">
                    <label class="custom-file-label" for="file_address" data-browse="Buscar"></label>
                </div>
            </div>
        </div>
        <hr>
        <h5>UNIVERSITARIOS:</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-6">
                <label for="secundaria">Estudios técnicos:</label>
                <input class="form-control mb-3" type="text" name="secundaria" id="secundaria">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="endDateS">Fecha de terminación:</label>
                <input class="form-control mb-3" type="date" name="endDate" id="endDate">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="comp">Comprobante:</label>
                <div class="form-row">
                    <div class="col-1 col-md-6">
                        <label for="yes">Sí</label>
                        <input class="mb-3" type="checkbox" name="yes" id="yes">
                    </div>
                    <div class="col-1 col-md-6">
                        <label for="no">No</label>
                        <input class="mb-3" type="checkbox" name="no" id="no">
                    </div>
                </div>
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="">Agregar comprobante de estudios:</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file_address" id="file_address">
                    <label class="custom-file-label" for="file_address" data-browse="Buscar"></label>
                </div>
            </div>
        </div>
        <hr>
        <h5>OTROS CONOCIMIENTOS:</h5>
        <div class="form-group">
            <div class="col-12 mb-3 col-md-12">
                <label for="idiomas">Idiomas:</label>
                <input class="form-control mb-3" type="text" name="idiomas" id="idiomas">
                <label for="maquinaria">Maquinaria y equipos que manejo:</label>
                <input class="form-control mb-3" type="text" name="maquinaria" id="maquinaria">
                <label for="programas">Programas y sistemas que utilizo:</label>
                <input class="form-control mb-3" type="text" name="programas" id="programas">
                <label for="oficina">Funciones de oficina que domino:</label>
                <input class="form-control mb-3" type="text" name="oficina" id="oficina">
            </div>
        </div>
        <h5>EXPERIENCIA LABORAL:</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-6">
                <label for="puesto">Puesto ocupado:</label>
                <input class="form-control mb-3" type="text" name="puesto" id="puesto">
            </div>
            <div class="col-12 mb-3 col-md-6">
                <label for="empresa">Empresa/domicilio:</label>
                <input class="form-control mb-3" type="text" name="empresa" id="empresa">
            </div>
        </div>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-4">
                <label for="periodo">Periodo:</label>
                <input class="form-control mb-3" type="text" name="periodo" id="periodo">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="sueldo">Sueldo:</label>
                <input class="form-control mb-3" type="text" name="sueldo" id="sueldo">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="motivo">Motivo de separación:</label>
                <input class="form-control mb-3" type="text" name="motivo" id="motivo">
            </div>
        </div>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-12">
                <label for="motivo">Descripción de actividades realizadas:</label>
                <input class="form-control mb-3" type="text" name="motivo" id="motivo">
            </div>
        </div>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-12">
                <label for="motivo">Agregar al menos dos últimos empleos:</label>
                <input class="form-control mb-3" type="text" name="motivo" id="motivo">
            </div>
        </div>
        <h5>REFERENCIAS:</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-3">
                <label for="reNombre">Nombre:</label>
                <input class="form-control mb-3" type="text" name="reNombre" id="reNombre">
            </div>
            <div class="col-12 mb-3 col-md-3">
                <label for="reTelefono">Teléfono:</label>
                <input class="form-control mb-3" type="text" name="reTelefono" id="reTelefono">
            </div>
            <div class="col-12 mb-3 col-md-3">
                <label for="reTiempo">Tiempo de conocerla:</label>
                <input class="form-control mb-3" type="text" name="reTiempo" id="reTiempo">
            </div>
            <div class="col-12 mb-3 col-md-3">
                <label for="reOcupacion">Ocupación:</label>
                <input class="form-control mb-3" type="text" name="reOcupacion" id="reOcupacion">
            </div>
        </div>
        <h5>TIENE PARIENTES TRABAJANDO EN LA EMPRESA:</h5>
        <div class="form-row">
            <div class="col-1 col-md-1">
                <label for="yes">Sí</label>
                <input class="mb-3" type="checkbox" name="yes" id="yes">
            </div>
            <div class="col-1 col-md-1">
                <label for="no">No</label>
                <input class="mb-3" type="checkbox" name="no" id="no">
            </div>
            <div class="col-1 col-md-6">
                <label for="paNombre">Nombre:</label>
                <input class="form-control mb-3" type="text" name="paNombre" id="paNombre">
            </div>
        </div>
        <h5>DISPONIBILIDAD PARA:</h5>
        <div class="form-row">
            <div class="col-1 col-md-1">
                <label for="">Viajar</label>
            </div>
            <div class="col-1 col-md-1">
                <label for="yes">Sí</label>
                <input class="mb-3" type="checkbox" name="yes" id="yes">
            </div>
            <div class="col-1 col-md-1">
                <label for="no">No</label>
                <input class="mb-3" type="checkbox" name="no" id="no">
            </div>
            <div class="col-1 col-md-1">
                <label for="">Rolar turno</label>
            </div>
            <div class="col-1 col-md-1">
                <label for="yes">Sí</label>
                <input class="mb-3" type="checkbox" name="yes" id="yes">
            </div>
            <div class="col-1 col-md-1">
                <label for="no">No</label>
                <input class="mb-3" type="checkbox" name="no" id="no">
            </div>
            <div class="col-1 col-md-2">
                <label for="">Cambiar de residencia</label>
            </div>
            <div class="col-1 col-md-1">
                <label for="yes">Sí</label>
                <input class="mb-3" type="checkbox" name="yes" id="yes">
            </div>
            <div class="col-1 col-md-1">
                <label for="no">No</label>
                <input class="mb-3" type="checkbox" name="no" id="no">
            </div>
        </div>
        <h5>HA ESTADO SINDICALIZADO:</h5>
        <div class="form-row">
            <div class="col-1 col-md-1">
                <label for="yes">Sí</label>
                <input class="mb-3" type="checkbox" name="yes" id="yes">
            </div>
            <div class="col-1 col-md-1">
                <label for="no">No</label>
                <input class="mb-3" type="checkbox" name="no" id="no">
            </div>
            <div class="col-1 col-md-6">
                <label for="paNombre">Nombre del sindicato:</label>
                <input class="form-control mb-3" type="text" name="paNombre" id="paNombre">
            </div>
        </div>
    @else --}}
        <div class="form-row">
            <input type="hidden" name="solicitud" value=true>
            <div class="col-12 col-md-3 text-center">
                <img src="{{ asset('img/vacantes/sinimagen.png') }}" class="mb-3" style="max-width: 300px; max-height: 183px">
                @if(Auth::user()->hasRolePermission('user_admin'))
                    <div class="custom-file form-group col">      
                        <input type="file" name="file[image]" class="form-control custom-file-input" id="image" aria-describedby="inputGroupFileAddon01">
                        <label class="custom-file-label" for="image" data-browse="Subir Fotografía"></label>
                        <small> <strong> 2MB Max * </strong></small>
                    </div>
                @endif
            </div>
            <div class="col-12 col-md-3 d-flex flex-column align-content-start justify-content-start">
                <label for="name_sol">Nombre:</label>
                <input class="form-control mb-3" type="text" name="name_sol" id="name_sol" disabled>
                <label for="cellphone_sol">Número de celular:</label>
                <input class="form-control mb-3" type="text" name="cellphone_sol" id="cellphone_sol" disabled>
                <label for="gender_sol">Sexo:</label>
                <input class="form-control mb-3" type="text" name="gender_sol" id="gender_sol" disabled>
            </div>
            <div class="col-12 col-md-3 d-flex flex-column align-content-start justify-content-start">
                <label for="surname_father_sol">Apellido Paterno:</label>
                <input class="form-control mb-3" type="text" name="surname_father_sol" id="surname_father_sol" disabled>
                <label for="phone_sol">Número de teléfono fijo:</label>
                <input class="form-control mb-3" type="text" name="phone_sol" id="phone_sol" disabled>
                <label for="date_birth_sol">Fecha de Nacimiento:</label>
                <input class="form-control mb-3" type="text" name="date_birth_sol" id="date_birth_sol" disabled>
            </div>
            <div class="col-12 col-md-3  d-flex flex-column align-items-start justify-content-start">
                <label for="surname_mother_sol">Apellido Materno:</label>
                <input class="form-control mb-3" type="text" name="surname_mother_sol" id="surname_mother_sol" disabled>
                <label for="email_sol">Correo Eléctronico:</label>
                <input class="form-control mb-3" type="text" name="email_sol" id="email_sol" disabled>
                <label for="rfc_sol">RFC:</label>
                <input class="form-control mb-3" type="text" name="rfc_sol" id="rfc_sol" disabled>
            </div>
        </div>

        <h5>Documento CV</h5>
        
        <div class="form-row">
            
            <div class="col-12 mb-3 col-md-6 pull-right">

                <label for="files_cv">Adjuntar CV</label>

                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file[file_cv]" id="files_cv">
                    <label class="custom-file-label" for="files_cv" data-browse="Subir documento"></label>
                </div>

            </div>

        </div>


        <h5>DOMICILIO ACTUAL</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-6">
                <label for="address">Dirección:</label>
                <input class="form-control mb-3" type="text" name="address" id="address">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="num_outside">No. Casa</label>
                <input class="form-control mb-3" type="text" name="num_outside" id="num_outside">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="num_inside">Zona</label>
                <input class="form-control mb-3" type="text" name="num_inside" id="num_inside">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="colony">Colonia</label>
                <input class="form-control mb-3" type="text" name="colony" id="colony">
            </div>
        </div>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-2">
                <label for="state">Estado:</label>
                <select name="state" id="state" class="selectpicker form-control mb-3" data-live-search="true">
					<option disabled value="" selected hidden>Seleccione una opción...</option>
					@foreach ($estados as $id => $estado)
						<option value="{{ $id }}"> {{ $estado }}</option>
					@endforeach
				</select>
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="city">Ciudad:</label>
                <select class="selectpicker form-control mb-3" name="city" id="city" data-live-search="true">
                    <option disabled value="" selected hidden>Debe seleccionar un Estado...</option>
                </select>
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="zip_code">Código postal:</label>
                <input class="form-control mb-3" type="text" name="zip_code" id="zip_code">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="">Comprobante de domicilio:</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file_address" id="file_address">
                    <label class="custom-file-label" for="file_address" data-browse="Buscar"></label>
                </div>
            </div>
        </div>
        <br><br>

        <h5>PREPARACIÓN</h5>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_application">
                <span class="fa fa-plus"></span>
            </button>
        </div>

        <div id="escolar_application">
            <div class="form-row">
                <div class="col-12 mb-3 col-md-2">
                    <input type="hidden" name="type[]" value="basica">
                    <label for="studio" class="requerido">Nivel alcanzado:</label>
                    <select name="studio[]" id="studio" class="form-control mb-3">
                        <option disabled value="" selected hidden>Seleccione una opción...</option>
                        <option value="Primaria" class="s-Primaria">Primaria</option>
                        <option value="Secundaria" class="s-Secundaria">Secundaria</option>
                        <option value="Preparatoria" class="s-Preparatoria">Preparatoria</option>
                        <option value="Técnico" class="s-Técnico">Técnico</option>
                    </select>
                </div>
                <div class="col-12 mb-3 col-md-5">
                    <label for="school" class="requerido">Institución que expidió constancia:</label>
                    <input type="text" name="school[]" id="school" class="form-control mb-3">
                </div>
                <div class="col-12 mb-3 col-md-2">
                    <label for="date_end" class="requerido">Fecha terminación:</label>
                    <input type="date" name="date_end[]" id="date_end" class="form-control mb-3">
                </div>
                <div class="col-12 mb-3 col-md-2">
                    <label for="comp">Comprobante:</label>
                    <div class="form-row">
                        <div class="col-1 col-md-6">
                            <label for="yes">Sí</label>
                            <input type="radio" name="voucher[]" id="voucher" value="1" class="mb-3">
                        </div>
                        <div class="col-1 col-md-6">
                            <label for="no">No</label>
                            <input type="radio" name="voucher[]" id="voucher" value="0" class="mb-3">
                        </div>
                    </div>
                </div> 
                <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                    <button type="button" class="btn btn-danger borrar_application">
                        <span class="fa fa-minus"></span>
                    </button>
                </div>
                <div class="col-12 mb-3 col-md-4">
                    <label for="">Agregar comprobante de estudios:</label>
                    <div class="custom-file col">
                        <input class="form-control-file custom-file-input" type="file" name="file_studio[]" id="file_studio">
                        <label class="custom-file-label" for="file_studio" data-browse="Buscar"></label>
                    </div>
                </div>
            </div>
        </div>

        <div id="destino_application">
        </div>
        
        <hr>
        <h5>UNIVERSITARIOS:</h5>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_application_uni">
                <span class="fa fa-plus"></span>
            </button>
        </div>

        <div id="escolar_application_uni">
            <div class="form-row">
                <div class="col-12 mb-3 col-md-3">
                    <input type="hidden" name="type_uni[]" value="superior">
                    <label for="studio_uni">Tipo de carrera:</label>
                    <select name="studio_uni[]" id="studio_uni" class="form-control">
                        <option disabled value="" selected hidden>Seleccione una opción...</option>
                        <option value="Na">N/A</option>
                        <option value="Licenciatura" class="s-Licenciatura">Licenciatura</option>
                        <option value="Maestria" class="s-Maestria">Maestria</option>
                        <option value="Doctorado" class="s-Doctorado">Doctorado/Posgrado</option>
                        <option value="Diplomado" class="s-Diplomado">Diplomado</option>
                    </select>
                </div>
                <div class="col-12 mb-3 col-md-6">
                    <label for="career">Carrera:</label>
                    <input class="form-control mb-3" type="text" name="career_uni[]" id="career_uni">
                </div>
                <div class="col-12 mb-3 col-md-2">
                    <label for="comp">Comprobante:</label>
                    <div class="form-row">
                        <div class="col-1 col-md-6">
                            <label for="yes">Sí</label>
                            <input class="mb-3" type="radio" name="voucher_uni[]" value="1" id="voucher_uni">
                        </div>
                        <div class="col-1 col-md-6">
                            <label for="no">No</label>
                            <input class="mb-3" type="radio" name="voucher_uni[]" value="0" id="voucher_uni" checked>
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                    <button type="button" class="btn btn-danger borrar_application_uni">
                        <span class="fa fa-minus"></span>
                    </button>
                </div>
                <div class="col-12 mb-3 col-md-4">
                    <label for="">Agregar comprobante de estudios:</label>
                    <div class="custom-file col">
                        <input class="form-control-file custom-file-input" type="file" name="file_studio_uni[]" id="file_studio_uni">
                        <label class="custom-file-label" for="file_studio_uni" data-browse="Buscar"></label>
                    </div>
                </div>
            </div>
        </div>
        <div id="destino_application_uni"></div>
        <hr>
        <h5>OTROS CONOCIMIENTOS:</h5>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_application_language">
                <span class="fa fa-plus"></span>
            </button>
        </div>
        <div id="escolar_application_language">
            <div class="form-row">
                <div class="col-12 mb-3 col-md-5">
                    <label for="language">Idioma:</label>
                    <input type="text" name="language[]" id="language" class="form-control">
                </div>
                <div class="col-12 mb-3 col-md-2">
                    <label for="spoken">Hablado:</label>
                    <input type="number" min="1" max="100" name="spoken[]" id="spoken" class="form-control">
                </div>
                <div class="col-12 mb-3 col-md-2">
                    <label for="reading">Lectura:</label>
                    <input type="number" min="1" max="100" name="reading[]" id="reading" class="form-control">
                </div>
                <div class="col-12 mb-3 col-md-2">
                    <label for="writing">Escritura:</label>
                    <input type="number" min="1" max="100" name="writing[]" id="writing" class="form-control">
                </div>
                <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                    <button type="button" class="btn btn-danger borrar_application_language">
                        <span class="fa fa-minus"></span>
                    </button>
                </div>
            </div>
        </div>
        <div id="destino_application_language"></div>


        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_application_knowledge">
                <span class="fa fa-plus"></span>
            </button>
        </div>
        <div id="escolar_application_knowledge">
            <div class="form-row">
                <div class="col-12 mb-3 col-md-3">
                    <label for="knowledge_type">Conocimientos que domino:</label>
                    <select name="knowledge_type[]" id="knowledge_type[]" class="form-control">
                        <option disabled value="" selected hidden>Seleccione una opción...</option>
                        <option value="Maquinaria">Maquinaria y equipo</option>
                        <option value="Programas">Programas y sistemas</option>
                        <option value="Funciones">Funciones de oficina</option>
                    </select>
                </div>
                <div class="col-12 mb-3 col-md-8">
                    <label for="knowledge_name">Descripción:</label>
                    <input type="text" name="knowledge_name[]" id="knowledge_name[]" class="form-control">
                </div>
                
                <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                    <button type="button" class="btn btn-danger borrar_application_knowledge">
                        <span class="fa fa-minus"></span>
                    </button>
                </div>
            </div>
        </div>
        <div id="destino_application_knowledge"></div>
        <hr>

        <h5>EXPERIENCIA LABORAL:</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-12">
                <label for="motivo">Agregar al menos dos últimos empleos:</label>
            </div>
        </div>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_application_experience">
                <span class="fa fa-plus"></span>
            </button>
        </div>

        <div id="escolar_application_experience">
            <div class="form-row">
                <div class="col-12 mb-3 col-md-5">
                    <label for="job">Puesto ocupado:</label>
                    <input class="form-control mb-3" type="text" name="job[]" id="job">
                </div>
                <div class="col-12 mb-3 col-md-6">
                    <label for="company">Empresa/domicilio:</label>
                    <input class="form-control mb-3" type="text" name="company[]" id="company">
                </div>
                <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                    <button type="button" class="btn btn-danger borrar_application_experience">
                        <span class="fa fa-minus"></span>
                    </button>
                </div>
            </div>
            <div class="form-row">
                <div class="col-12 mb-3 col-md-3">
                    <label for="date_begin_experience">Fecha inicio:</label>
                    <input class="form-control mb-3" type="date" name="date_begin_experience[]" id="date_begin_experience">
                </div>
                <div class="col-12 mb-3 col-md-3">
                    <label for="date_end_experience">Fecha fin:</label>
                    <input class="form-control mb-3" type="date" name="date_end_experience[]" id="date_end_experience">
                    <span id="edad_max_error" class="clase_error"><strong>Mayor o igual a edad mínima</strong></span>
                </div>
                <div class="col-12 mb-3 col-md-3">
                    <label for="salary">Sueldo:</label>
                    <input class="form-control mb-3" type="text" name="salary[]" id="salary">
                </div>
                <div class="col-12 mb-3 col-md-3">
                    <label for="reason_separation">Motivo de separación:</label>
                    <input class="form-control mb-3" type="text" name="reason_separation[]" id="reason_separation">
                </div>
            </div>
            <div class="form-row">
                <div class="col-12 mb-3 col-md-12">
                    <label for="activity">Descripción de actividades realizadas:</label>
                    <input class="form-control mb-3" type="text" name="activity[]" id="activity">
                </div>
            </div>
        </div>

        <div id="destino_application_experience"></div>
        
        <h5>REFERENCIAS:</h5>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_application_reference">
                <span class="fa fa-plus"></span>
            </button>
        </div>

        <div id="escolar_application_reference">
            <div class="form-row">
                <div class="col-12 mb-3 col-md-3">

                        <label for="type">Referecia</label>
                        <select name="type_ref[]" id="type_ref" class="form-control mb-3">
                            <option value="Personal">Personal</option>
                          <option value="Laboral">Laboral</option>
                         
                        </select>

                    </div>

                    <div class="col-12 mb-3 col-md-9 mt-4 text-right pull-right">
                        <button type="button" class="btn btn-danger borrar_reference_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>

            </div>

            <div class="form-row">

                <div class="col-12 mb-3 col-md-3">
                    <label for="reference_name">Nombre:</label>
                    <input class="form-control mb-3" type="text" name="reference_name[]" id="reference_name">
                </div>
                <div class="col-12 mb-3 col-md-3">
                    <label for="reference_phone">Teléfono:</label>
                    <input class="form-control mb-3" type="text" name="reference_phone[]" id="reference_phone">
                </div>
                <div class="col-12 mb-3 col-md-3">
                    <label for="reference_time_meet">Tiempo de conocerla:</label>
                    <input class="form-control mb-3" type="text" name="reference_time_meet[]" id="reference_time_meet">
                </div>
                <div class="col-12 mb-3 col-md-3">
                    <label for="reference_occupation">Ocupación:</label>
                    <input class="form-control mb-3" type="text" name="reference_occupation[]" id="reference_occupation">
                </div>
            </div>
        </div>

        <div id="destino_application_reference"></div>
        
     
        {{-- <h5>Tiene Parientes Trabajando en la Empresa?</h5>

        <div class="form-row">

            <div class="col-1 col-md-1 p-1">
                
                <input class="mb-3" type="checkbox" name="family_working" id="family_working_0_c"  {{ isset($profile->perfilSolicitud) ? $profile->perfilSolicitud->family_working == '1' ? 'checked' : null : null }}>

                <label for="family_working_0_c">Sí</label>

            </div>

            <div class="col-12 mb-3 col-md-3" style="display: inline-flex;">

                <label for="family_working_name_c" class="mt-1 pr-2">Empleado</label>


                <select name="family_working_name" id="family_working_name_c" class="selectpicker form-control mb-3" data-live-search="true">

                    @foreach ($Listausuarios as $id => $usuarios)
                        @isset($profile->perfilSolicitud->family_working)
                            <option value="{{ $usuarios->id }}" {{ ($usuarios->id==$profile->perfilSolicitud->family_working_name) ? 'selected' : null }}> {{ $usuarios->nombre }} {{ $usuarios->paterno }}</option>
                        @else
                            <option disabled value="" selected hidden>No tiene Familiar...</option>
                            <option value="{{ $usuarios->id }}"> {{ $usuarios->nombre }} {{ $usuarios->paterno }}</option>
                        @endisset
                    @endforeach

                </select>

            </div>
        </div> --}}


       <h5>Disponibilidad Para</h5>
        <div class="form-row">

            <div class="col-3 col-md-3">
                
                <input class="mb-3" type="checkbox" name="availability_travel" id="availability_travel_id" >
                <label for="availability_travel_id">Viajar</label>
            </div>


            <div class="col-3 col-md-3">
                
                <input class="mb-3" type="checkbox" name="availability_shifts" id="availability_shifts_id">
                <label for="availability_shifts_id">Rolar turno</label>
            </div>



            <div class="col-3 col-md-3">
                
                <input class="mb-3" type="checkbox" name="availability_residence" id="availability_residence_id">
                <label for="availability_residence_id">Cambiar de residencia</label>
            </div>

        </div>
        <br>

{{-- 
        <h5>Sindicatos</h5>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_sindicatos_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>

       <div id="sindicato_c">
            <div class="form-row">
                <div class="col-12 mb-3 col-md-11">
                    <label for="career">Nombre del Sindicato:</label>
                    <input class="form-control mb-3 col-md-6" type="text" name="unionized_name[]" id="career_uni_c">
                </div>
                <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                    <button type="button" class="btn btn-danger borrar_sindicato_c">
                        <span class="fa fa-minus"></span>
                    </button>
                </div>
            </div>
        </div>

        <div id="destino_sindicato_c"></div> --}}
    {{-- @endisset --}}
</div>
