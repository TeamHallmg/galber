<div class="container-fluid">
    <h5>CURSOS</h5>

    <div class="form-row">
        <div class="form-group col-3">
            <div class="row">
                <div class="form-group col text-center">
                    @if (isset($profile) && !is_null($profile->image))
                        <img src="{{ asset('uploads/profile/'.$profile->image) }}" class="mb-3" style="max-width: 270px; max-height: 183px">
                    @else
                        <img src="{{ asset('img/vacantes/sinimagen.png') }}" class="mb-3" style="max-width: 300px; max-height: 183px">
                    @endif
                </div>
            </div>
        </div>
        <div class="col-9">
            <div class="row">
                <div class="form-group col"></div>
                
                <div class="form-group col"></div>

                <div class="form-group col">
                    <label for="" class="requerido">Número de empleado</label>
                    <input type="text" name="" id="" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->idempleado : null : null : null }}" disabled="{{ is_null($user) ? false : true }}">
                </div>
            </div>
            <div class="row">
                <div class="form-group col">
                    <label for="name" class="requerido">Nombre</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->nombre : null : $user->first_name : null }}" disabled={{ is_null($user) ? false : true }}>
                </div>
                
                <div class="form-group col">
                    <label for="surname_father" class="requerido">Apellido Paterno</label>
                    <input type="text" name="surname_father" id="surname_father" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->paterno : null : $user->last_name : null }}" disabled={{ is_null($user) ? false : true }}>
                </div>

                <div class="form-group col">
                    <label for="surname_mother" class="requerido">Apellido Materno</label>
                    <input type="text" name="surname_mother" id="surname_mother" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->materno : null : null : null }}" disabled={{ is_null($user) ? false : true }}>
                </div>
            </div>

            <div class="row">
                <div class="form-group col"></div>
                
                <div class="form-group col"></div>

                <div class="form-group col">
                    <label for="">Puesto</label><br>
                </div>
            </div>
            <div class="row col"></div>
        </div>
    </div>
    <br>
   
    <div class="row">
        <div class="col-md-8">
            <h3 class="font-weight-bold text-blue">Cursos Matriculados</h3>
        </div>
        {{-- <div class="col-md-4" style="text-align: right;">
            <a href="#" class="btn morado" data-toggle="modal" data-target="#matricularModal">
                Matricular a Curso
            </a>
        </div> --}}
    </div>
    <hr style="border: 2px solid #000000;">
            
    <table class="table table-hover table-bordered w-100 tableWrapper" id="tableCursos">
        <thead class="bg-lightblue text-white">
            <tr>
                <th>ID</th>
                <th>Plan</th>
                <th>Curso</th>
                <th>Costo</th>
                <th>Calificación</th>
                {{-- <th>Opciones</th> --}}
            </tr>
        </thead>
        <tbody>
            @if(!is_null($profile->user))
                @foreach($profile->user->getMoodleEnrolledCourses(true) as $key => $row)
                    <tr>
                        <td>{{ $key }}</td>
                        <td>{!! $profile->user->getScalePlanName($key) ?? '<span class="text-muted">-</span>' !!}</td>
                        <td>{{ $row['name'] }}</td>
                        <td>{{ $profile->user->getFormattedScalePlanPrice($key) }}</td>
                        <td>{{ $row['grade'] }}</td>
                        {{-- <td>
                            <button type="button" class="btn btn-danger">
                                x
                            </button>
                        </td> --}}
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>

</div>


<!-- Modal -->
<div class="modal fade" id="matricularModal" tabindex="-1" role="dialog" aria-labelledby="matricularModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form method="POST" action="{{ url('escalafon/matricular_usuario_cursos') }}">
            @csrf
            <input type="hidden" name="user_id" value="{{ $user_id }}">
            <input type="hidden" name="job_position_id" value="{{ $job_position_id }}">
            <div class="modal-header bg-blue">
                <h5 class="modal-title text-white" id="exampleModalLabel">Matricular usuario a cursos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div class="form-group">
                            <label for="plan_id" class="col-form-label font-weight-bold requerido">Plan:</label>
                            <select name="plan_id" id="plan_id" class="selectpicker form-control border" data-live-search="true" title="Selecciona un plan..." required>
                                @foreach ($plans as $plan)
                                    <option value="{{ $plan->id }}">{{ $plan->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="form-group">
                            <label for="course_id" class="col-form-label font-weight-bold requerido">Curso:</label>
                            <select name="course_id" id="course_id" class="selectpicker form-control border" data-live-search="true" title="Selecciona uno o varios cursos..." required>
                                {{-- @foreach ($courses as $course)
                                    <option value="{{ $course->moodle_id }}">{{ $course->name }}</option>
                                @endforeach --}}
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="price" class="col-form-label font-weight-bold">Costo:</label>
                        <input type="text" id="price" name="price" class="form-control currency">
                    </div>
                    <div class="col-12">
                        <p class="font-weight-bold m-0 rounded bg-lightgray px-3 py-1 text-center">
                            <span class="text-danger mr-2">*</span>Sólo serán visibles los cursos ligados a moodle
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Matricular</button>
            </div>
        </form>
      </div>
    </div>
  </div>