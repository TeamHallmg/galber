

<div class="row">
        <div class="col-10">
            <h4 class="m-0 text-danger">
                Requisición de Personal
            </h4>
        </div>
        {{-- <div class="col-2 d-flex align-items-end">
            <div class="dropdown">
                @if(Auth::user()->hasRolePermission('vacancies_admin'))
                    <a href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><h4 class="m-0 text-danger"><i class="fas fa-cog"></i></h4></a>
                    <div class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton">
                        @if(Auth::user()->hasRolePermission('see_vacancies'))
                            @if(Auth::user()->isRecruiter() && !Auth::user()->hasRolePermission('generate_vacant'))
                                <a class="dropdown-item text-white" href="{{ url('vacantes/reclutador') }}">Admin Vacantes</a>
                            @else
                                <a class="dropdown-item text-white" href="{{ url('vacantes/administrar') }}">Admin Vacantes</a>
                            @endif
                        @endif
                        @if(Auth::user()->hasRolePermission('add_remove_recruiter'))
                            <a class="dropdown-item text-white" href="{{ url('reclutador') }}">Reclutadores</a>
                        @endif
                    </div>
                @endif
            </div>
        </div> --}}
    </div>
    <hr>
    <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link text-decoration-none text-dark font-weight-bold" href="{{ url('requisitions') }}">Requisiciones</a>
        </li>
        {{-- <li class="nav-item">
          <a class="nav-link text-decoration-none text-dark font-weight-bold" href="{{ url('reclutador') }}">Reclutadores</a>
        </li> --}}
        {{-- @if(Auth::user()->hasRolePermission('see_vacancies'))
            <li class="nav-item">
                <a class="nav-link text-decoration-none text-dark font-weight-bold" href="{{ url('vacantes/administrar') }}">Admin Vacantes</a>
            </li>
        @endif --}}
        <li class="nav-item">
          <a class="nav-link text-decoration-none text-dark font-weight-bold" href="{{ url('curriculum') }}">Currículum</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-decoration-none text-dark font-weight-bold" href="{{ url('vacantes') }}">Vacantes</a>
        </li>
        {{-- @if(Auth::user()->isRecruiter())
            <li class="nav-item">
                <a class="nav-link text-decoration-none text-dark font-weight-bold" href="{{ url('vacantes/reclutador') }}">Reclutador</a>
            </li>
        @endif --}}
      </ul>