<div class="container-fluid">
        <div class="row mt-n4 mb-3">
            <div class="col text-right">
                <button type="submit" class="btn morado">{{ ($crear) ? 'Guardar' : 'Actualizar' }}</button>
            </div>
        </div>

        @if(!is_null($profile))
            <h5>Datos Generales</h5>

            <div class="form-row">
                <div class="col-12 col-md-3 text-center">
                    @if (isset($profile) && !is_null($profile->image) && $fileExists)
                        <img src="{{ asset('uploads/profile/'.$profile->image) }}" class="mb-3" style="max-width: 270px; max-height: 183px">
                    @else
                        <img src="{{ asset('img/vacantes/sinimagen.png') }}" class="mb-3" style="max-width: 300px; max-height: 183px">
                    @endif
                    @if(Auth::user()->hasRolePermission('user_admin') || Auth::user()->role === 'admin')
                        <div class="custom-file form-group col">
                            <label class="btn morado float-left" for="my-file-selector">
                                <input id="my-file-selector" name="file[image]" type="file" style="display:none" 
                                onchange="$('#upload-file-info').html(this.files[0].name)">
                                Subir Fotografía
                            </label>
                            <textarea class="form-control" style="resize: none;" id="upload-file-info" cols="30" rows="2" readonly></textarea>
                            <small class="float-left"> <strong> 2MB Max * </strong></small>
                        </div>
                    @endif
                </div>
                <div class="col-12 col-md-3 d-flex flex-column align-content-start justify-content-start">
                    <label for="name">Nombre(s)</label>
                    <input class="form-control mb-3" type="text" name="name" id="name" value="{{ isset($profile) ? $profile->name : null}}" disabled>
                    <label for="cellphone">Número de Celular</label>
                    <input class="form-control mb-3" type="text" name="cellphone" id="cellphone" value="{{ isset($profile) ? $profile->cellphone : null}}" disabled>
                    <label for="gender">Sexo</label>
                    <input class="form-control mb-3" type="text" name="gender" id="gender" value="{{ isset($profile) ? $profile->gender : null}}" disabled>
                </div>
                <div class="col-12 col-md-3 d-flex flex-column align-content-start justify-content-start">
                    <label for="surname_father">Apellido Paterno</label>
                    <input class="form-control mb-3" type="text" name="surname_father" id="surname_father" value="{{ isset($profile) ? $profile->surname_father : null}}" disabled>
                    <label for="phone">Número de Teléfono Fijo</label>
                    <input class="form-control mb-3" type="text" name="phone" id="phone" value="{{ isset($profile) ? $profile->phone : null}}" disabled>
                    <label for="date_birth">Fecha de Nacimiento</label>
                    {{-- <input class="form-control mb-3" type="text" name="date_birth" id="date_birth" value="{{ isset($profile) ? Carbon\Carbon::createFromFormat('Y-m-d', $profile->date_birth)->format('d-m-Y') : null }}" disabled> --}}
                    <input class="form-control mb-3" type="text" name="date_birth" id="date_birth" value="{{ isset($profile) ? $profile->date_birth : null }}" disabled>
                </div>
                <div class="col-12 col-md-3  d-flex flex-column align-items-start justify-content-start">
                    <label for="surname_mother">Apellido Materno</label>
                    <input class="form-control mb-3" type="text" name="surname_mother" id="surname_mother" value="{{ isset($profile) ? $profile->surname_mother : null}}" disabled>
                    <label for="lname">Correo Eléctronico</label>
                    <input class="form-control mb-3" type="text" name="email" id="email" value="{{ isset($profile) ? $profile->email : null}}" disabled>
                    <label for="rfc">Estado Civil</label>
                    <input type="text" name="" id="" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->civil : null : null : null }}" disabled="{{ is_null($user) ? false : true }}">
        
                </div>

                <div class="col-12 col-md-3 offset-md-3  d-flex flex-column align-items-start justify-content-start">
                    <label for="age" class="requerido">Edad</label>
                    <input type="text" id="age" class="form-control mb-3" value="{{ isset($profile) ? $profile->getAge() : null}}" readonly={{ is_null($user) ? false : true }}>
                </div>
                <div class="col-12 col-md-3  d-flex flex-column align-items-start justify-content-start">
                    <label for="imss_registry" class="requerido">IMSS</label>
                    <input type="text" name="irtra" id="irtra" class="form-control" value="{{ !is_null($profile) ? !is_null($profileRegistros) ? $profileRegistros->imss_registry : null : null }}" readonly={{ is_null($user) ? false : true }}>
                </div>
                <div class="col-12 col-md-3  d-flex flex-column align-items-start justify-content-start">
                    <label for="rfc" class="requerido">RFC</label>
                    <input type="text" name="irtra" id="irtra" class="form-control" value="{{ !is_null($profile) ? !is_null($profileRegistros) ? $profileRegistros->rfc_registry : null : null }}" readonly={{ is_null($user) ? false : true }}>
                </div>

            </div>
            <br><br>
        @else
              <div class="form-row">
            <div class="col-12 col-md-3 text-center">
                <img src="{{ asset('img/vacantes/sinimagen.png') }}" class="mb-3" style="max-width: 300px; max-height: 183px">

                @if(Auth::user()->hasRolePermission('user_admin') || Auth::user()->role === 'admin')
                    <div class="custom-file form-group col">
                        <label class="btn morado float-left" for="my-file-selector">
                            <input id="my-file-selector" name="file[image]" type="file" style="display:none" 
                            onchange="$('#upload-file-info').html(this.files[0].name)">
                            Subir Fotografía
                        </label>
                        <textarea class="form-control" style="resize: none;" id="upload-file-info" cols="30" rows="2" readonly></textarea>
                        <small class="float-left"> <strong> 2MB Max * </strong></small>
                    </div>
                @endif
            </div>
            <div class="col-9">
                <div class="row">

                    <input type="hidden" name="user_id" value="{{ $user_id }}">

                    <div class="form-group col">
                        <label for="name" class="requerido">Nombre</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->nombre : $user->first_name : null }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
                    
                    <div class="form-group col">
                        <label for="surname_father" class="requerido">Apellido Paterno</label>
                        <input type="text" name="surname_father" id="surname_father" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->paterno : $user->last_name : null }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
    
                    <div class="form-group col">
                        <label for="surname_mother" class="requerido">Apellido Materno</label>
                        <input type="text" name="surname_mother" id="surname_mother" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->materno : null : null }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
                </div>
    
                <div class="row">
                    <div class="form-group col">
                        <label for="cellphone" class="requerido">Número de Celular</label>
                        <input type="text" name="cellphone" id="cellphone" class="form-control">
                    </div>
    
                    <div class="form-group col">
                        <label for="phone" class="requerido">Número de Teléfono Fijo</label>
                        <input type="text" name="phone" id="phone" class="form-control">
                    </div>
    
                    <div class="form-group col">
                        <label for="email" class="requerido">Correo Electrónico</label>
                        <input type="text" name="email" id="email" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->correoempresa : $user->email : null }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
                </div>
    
                <div class="row">
                    <div class="form-group col">
                        <label for="gender" class="requerido">Sexo</label>
                        <input type="text" name="gender" id="gender" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->sexo : null : null }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
    
                    <div class="form-group col">
                        <label for="date_birth" class="requerido">Fecha de Nacimiento</label>
                        <input type="date" name="date_birth" id="date_birth" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->nacimiento : null : null }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
                    
                    <div class="form-group col">
                         <label for="rfc">Estado Civil</label>
                        <input class="form-control mb-3" type="text" name="rfc" id="rfc" value="{{ (is_null($user) ? null : is_null($user->employee) ? null : $user->employee->civil) }}" disabled>
    

                       
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col">
                        <label for="age" class="requerido">Edad</label>
                        <input type="text" id="age" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->getAge() : null : null }}" readonly={{ is_null($user) ? false : true }}>
                    </div>
    
                    <div class="form-group col">
                        <label for="igss" class="requerido">IMSS</label>
                        <input type="text" name="irtra" id="irtra" class="form-control" value="{{ !is_null($profile) ? !is_null($profileRegistros) ? $profileRegistros->imss_registry : null : null }}" readonly={{ is_null($user) ? false : true }}>
                            
                    </div>
                    
                    <div class="form-group col">
                            <label for="rfc" class="requerido">RFC</label>
                            
                        <input type="text" name="irtra" id="irtra" class="form-control" value="{{ !is_null($profile) ? !is_null($profileRegistros) ? $profileRegistros->rfc_registry : null : null }}" readonly={{ is_null($user) ? false : true }}>
                            
                    </div>
                </div>
            </div>
        </div>
        @endif
        
        <h5>Documento CV</h5>

        <div class="form-row">
            
            @isset($profile)
                @if (!is_null($profile->file_cv))
                    <div class="col-12 mb-3  col-md-3">

                        <label for="">Documento Adjunto</label>
                        <div class="custom-file col">
                            @if ($profile->getFileExists($profile->file_cv))
                                <a class="btn morado btn-block" href="{{ asset('/uploads/profile/'. $profile->file_cv) }}" target="_blank">
                                    <i class="fa fa-file-alt"></i> Ver
                                </a>
                            @endif
                        </div>

                    </div>
                @endif
            @endisset
            
            <div class="col-12 mb-3 col-md-6 pull-right">

                @if (isset($profile) && !is_null($profile->file_cv))
                    <label for="files_cv">Actualizar CV</label>
                @else
                    <label for="files_cv">Adjuntar CV</label>
                @endif

                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file[file_cv]" id="files_cv">
                    <label class="custom-file-label" for="files_cv" data-browse="Subir documento"></label>
                </div>

            </div>

        </div>

        <br><br>


        <h5>Domicilio Actual</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-6">
                <label for="address">Dirección</label>
                <input class="form-control mb-3" type="text" name="address" id="address_c" value="{{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->address : null }}">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="num_outside">No. Casa</label>
                <input class="form-control mb-3" type="text" name="num_outside" id="num_outside_c" value="{{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->num_outside : null }}">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="num_inside">Zona</label>
                <input class="form-control mb-3" type="text" name="num_inside" id="num_inside_c" value="{{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->num_inside : null }}">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="colony">Colonia</label>
                <input class="form-control mb-3" type="text" name="colony" id="colony_c" value="{{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->colony : null }}">
            </div>
        </div>
        
        <div class="form-row">
            <div class="col-12 mb-3 col-md-2">
                <label for="state">Estado</label>
                <select name="state" id="state_c" class="selectpicker form-control mb-3" data-live-search="true">
                    @foreach ($estados as $id => $estado)
                        @isset($profile->perfilAdicional->state)
                            <option value="{{ $id }}" {{ ($id==$profile->perfilAdicional->estado->id) ? 'selected' : null }}> {{ $estado }}</option>
                        @else
                            <option disabled value="" selected hidden>Debe seleccionar un Departamento...</option>
                            <option value="{{ $id }}"> {{ $estado }}</option>
                        @endisset
                    @endforeach
                </select>
            </div>
            
            <div class="col-12 mb-3 col-md-2">
                <label for="city">Ciudad</label>
                <select class="selectpicker form-control mb-3" name="city" id="city_c" data-live-search="true">
                    @isset($profile->perfilAdicional)
                        @foreach ($municipios as $id => $municipio)
                            <option value="{{ $id }}" {{ !is_null($profile->perfilAdicional->municipio) ? ($id==$profile->perfilAdicional->municipio->id) ? 'selected' : null : null}}> {{ $municipio }}</option>
                        @endforeach
                    @else
                        <option disabled value="" selected hidden>Debe seleccionar un Departamento...</option>
                    @endisset
                </select>
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="zip_code">Código Postal</label>
                <input class="form-control mb-3" type="number" name="zip_code" id="zip_code_c" value="{{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->zip_code : null }}">
            </div>
                
            <div class="col-12 mb-3 col-md-4">
                <label for="">Comprobante de Domicilio</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file_address" id="file_address">
                    <label class="custom-file-label" for="file_address" data-browse="Buscar"></label>
                </div>
            </div>
    
            <div class="col-12 mb-3 mt-4 col-md-2">
                @isset($profile->perfilAdicional)
                    @if (!is_null($profile->perfilAdicional->file_address))
                        @if ($profile->getFileExists($profile->perfilAdicional->file_address))
                            <a class="btn morado" href="{{ asset('/uploads/profile/'. $profile->perfilAdicional->file_address) }}" target="_blank">
                                <i class="fa fa-file-alt"></i> Ver
                            </a>
                        @endif
                    @endif
                @endisset
            </div>
        </div>
        <br><br>
    
        <h5>Estudios</h5>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_escolar_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse ($profile->perfilEscuelaBasica as $escuela) --}}
        @forelse (isset($profile) ? $profile->perfilEscuelaBasica : [] as $escuela)
            <div id="escolar_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-2">
                        <input type="hidden" name="type[]" value="basica">
                        <label for="studio" class="requerido">Nivel Alcanzado</label>
                        <select name="studio[]" id="studio_c" class="form-control mb-3">
                            {{-- <option disabled value="" selected hidden>Seleccione una opción...</option> --}}
                            <option value="Primaria" class="s-Primaria-c" {{ ($escuela->studio=='Primaria') ? 'selected' : null }}>Primaria</option>
                            <option value="Secundaria" class="s-Secundaria-c" {{ ($escuela->studio=='Secundaria') ? 'selected' : null }}>Secundaria</option>
                            <option value="Preparatoria" class="s-Preparatoria-c" {{ ($escuela->studio=='Preparatoria') ? 'selected' : null }}>Diverisificado</option>
                            <option value="Técnico" class="s-Técnico-c" {{ ($escuela->studio=='Técnico') ? 'selected' : null }}>Técnico</option>
                        </select>
                    </div>
                    <div class="col-12 mb-3 col-md-5">
                        <label for="school" class="requerido">Institución que Expidió Constancia</label>
                        <input type="text" name="school[]" id="school_c" class="form-control mb-3" value="{{ $escuela->school }}">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="date_end" class="requerido">Fecha Terminación</label>
                        <input type="date" name="date_end[]" id="date_end_c" class="form-control mb-3" value="{{ $escuela->date_end }}">
                    </div>
                    {{-- <div class="col-12 mb-3 col-md-2">
                        <label for="comp">Comprobante</label>
                        <div class="form-row">
                            <div class="col-1 col-md-6">
                                <label for="yes">Sí</label>
                                <input type="radio" name="voucher[{{ $loop->index }}]" id="voucher_c" value="1" class="mb-3 radio_check" {{ ($escuela->voucher == 1) ? 'checked' : '' }}>
                            </div>
                            <div class="col-1 col-md-6">
                                <label for="no">No</label>
                                <input type="radio" name="voucher[{{ $loop->index }}]" id="voucher_c" value="0" class="mb-3 radio_check" {{ ($escuela->voucher == 0) ? 'checked' : '' }}>
                            </div>
                        </div>
                    </div>  --}}
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_escolar_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                    <div class="col-12 mb-3 col-md-4">
                        <label for="">Agregar Comprobante de Estudios</label>
                        <div class="custom-file col">
                            <input class="form-control-file custom-file-input" type="file" name="file_studio[]" id="file_studio_c">
                            <label class="custom-file-label" for="file_studio" data-browse="Buscar"></label>
                        </div>
                    </div>
                    <div class="col-12 mb-3 mt-4 col-md-2">
                        @if (!is_null($escuela->file_studio))
                            @if ($profile->getFileExists($escuela->file_studio))
                                <a class="btn morado ver" href="{{ asset('/uploads/profile/'. $escuela->file_studio) }}" target="_blank">
                                    <i class="fa fa-file-alt"></i> Ver
                                </a>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        @empty
            <div id="escolar_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-2">
                        <input type="hidden" name="type[]" value="basica">
                        <label for="studio" class="requerido">Nivel Alcanzado</label>
                        <select name="studio[]" id="studio_c" class="form-control mb-3">
                            <option disabled value="" selected hidden>Seleccione una opción...</option>
                            <option value="Primaria" class="s-Primaria-c">Primaria</option>
                            <option value="Secundaria" class="s-Secundaria-c">Secundaria</option>
                            <option value="Preparatoria" class="s-Preparatoria-c">Diversificado</option>
                            <option value="Técnico" class="s-Técnico-c">Técnico</option>
                        </select>
                    </div>
                    <div class="col-12 mb-3 col-md-5">
                        <label for="school" class="requerido">Institución que Expidió Constancia</label>
                        <input type="text" name="school[]" id="school_c" class="form-control mb-3">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="date_end" class="requerido">Fecha Terminación</label>
                        <input type="date" name="date_end[]" id="date_end_c" class="form-control mb-3">
                    </div>
                    {{-- <div class="col-12 mb-3 col-md-2">
                        <label for="comp">Comprobante</label>
                        <div class="form-row">
                            <div class="col-1 col-md-6">
                                <label for="yes">Sí</label>
                                <input type="radio" name="voucher[0]" id="voucher_c" value="1" class="mb-3 radio_check">
                            </div>
                            <div class="col-1 col-md-6">
                                <label for="no">No</label>
                                <input type="radio" name="voucher[0]" id="voucher_c" value="0" class="mb-3 radio_check">
                            </div>
                        </div>
                    </div>  --}}
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_escolar_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                    <div class="col-12 mb-3 col-md-4">
                        <label for="">Agregar Comprobante de Estudios</label>
                        <div class="custom-file col">
                            <input class="form-control-file custom-file-input" type="file" name="file_studio[]" id="file_studio_c">
                            <label class="custom-file-label" for="file_studio" data-browse="Buscar"></label>
                        </div>
                    </div>
                </div>
            </div>
        @endforelse
        <div id="destino_escolar_c"></div>
        <hr>
    
        <h5>Universitarios</h5>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_escolar_uni_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse ($profile->perfilEscuelaSuperior as $escuela ) --}}
        @forelse (isset($profile) ? $profile->perfilEscuelaSuperior : [] as $escuela )
                <div id="escolar_uni_c">
                    <div class="form-row">
                        <div class="col-12 mb-3 col-md-3">
                            <input type="hidden" name="type_uni[]" value="superior">
                            <label for="studio_uni">Tipo de Carrera</label>
                            <select name="studio_uni[]" id="studio_uni_c" class="form-control">
                                {{-- <option disabled value="" selected hidden>Seleccione una opción...</option> --}}
                                <option value="Licenciatura" class="s-Licenciatura-c" {{ ($escuela->studio=='Licenciatura') ? 'selected' : null }}>Licenciatura</option>
                                <option value="Maestria" class="s-Maestria-c" {{ ($escuela->studio=='Maestria') ? 'selected' : null }}>Maestria</option>
                                <option value="Doctorado" class="s-Doctorado-c" {{ ($escuela->studio=='Doctorado') ? 'selected' : null }}>Doctorado/Posgrado</option>
                                <option value="Diplomado" class="s-Diplomado-c" {{ ($escuela->studio=='Diplomado') ? 'selected' : null }}>Diplomado</option>
                            </select>
                        </div>
                        <div class="col-12 mb-3 col-md-6">
                            <label for="career">Carrera</label>
                            <input class="form-control mb-3" type="text" name="career_uni[]" id="career_uni_c" value="{{ $escuela->career }}">
                        </div>
                        {{-- <div class="col-12 mb-3 col-md-2">
                            <label for="comp">Comprobante</label>
                            <div class="form-row">
                                <div class="col-1 col-md-6">
                                    <label for="yes">Sí</label>
                                    <input class="mb-3 radio_check_uni" type="radio" name="voucher_uni[{{ $loop->index }}]" value="1" id="voucher_uni_c" {{ ($escuela->voucher == '1') ? 'checked' : null }}>
                                </div>
                                <div class="col-1 col-md-6">
                                    <label for="no">No</label>
                                    <input class="mb-3 radio_check_uni" type="radio" name="voucher_uni[{{ $loop->index }}]" value="0" id="voucher_uni_c" {{ ($escuela->voucher == '0') ? 'checked' : null }}>
                                </div>
                            </div>
                        </div> --}}
                        <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                            <button type="button" class="btn btn-danger borrar_escolar_uni_c">
                                <span class="fa fa-minus"></span>
                            </button>
                        </div>
                        <div class="col-12 mb-3 col-md-4">
                            <label for="">Agregar Comprobante de Último Grado de Estudios</label>
                            <div class="custom-file col">
                                <input class="form-control-file custom-file-input" type="file" name="file_studio_uni[]" id="file_studio_uni">
                                <label class="custom-file-label" for="file_studio_uni" data-browse="Buscar"></label>
                            </div>
                        </div>
                        <div class="col-12 mb-3 mt-4 col-md-2">
                            @if (!is_null($escuela->file_studio))
                                @if ($profile->getFileExists($escuela->file_studio))
                                    <a class="btn morado ver" href="{{ asset('/uploads/profile/'. $escuela->file_studio) }}" target="_blank">
                                        <i class="fa fa-file-alt"></i> Ver
                                    </a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
        @empty 
            <div id="escolar_uni_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <input type="hidden" name="type_uni[]" value="superior">
                        <label for="studio_uni_c">Tipo de Carrera</label>
                        <select name="studio_uni[]" id="studio_uni_c" class="form-control">
                            <option disabled value="" selected hidden>Seleccione una opción...</option>
                            <option value="Licenciatura" class="s-Licenciatura-c">Licenciatura</option>
                            <option value="Maestria" class="s-Maestria-c">Maestria</option>
                            <option value="Doctorado" class="s-Doctorado-c">Doctorado/Posgrado</option>
                            <option value="Diplomado" class="s-Diplomado-c">Diplomado</option>
                        </select>
                    </div>
                    <div class="col-12 mb-3 col-md-6">
                        <label for="career">Carrera</label>
                        <input class="form-control mb-3" type="text" name="career_uni[]" id="career_uni_c">
                    </div>
                    {{-- <div class="col-12 mb-3 col-md-2">
                        <label for="comp">Comprobante</label>
                        <div class="form-row">
                            <div class="col-1 col-md-6">
                                <label for="yes">Sí</label>
                                <input class="mb-3 radio_check_uni" type="radio" name="voucher_uni[0]" value="1" id="voucher_uni_c">
                            </div>
                            <div class="col-1 col-md-6">
                                <label for="no">No</label>
                                <input class="mb-3 radio_check_uni" type="radio" name="voucher_uni[0]" value="0" id="voucher_uni_c" checked>
                            </div>
                        </div>
                    </div> --}}
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_escolar_uni_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                    <div class="col-12 mb-3 col-md-4">
                        <label for="">Agregar Comprobante de Último Grado de Estudios</label>
                        <div class="custom-file col">
                            <input class="form-control-file custom-file-input" type="file" name="file_studio_uni[]" id="file_studio_uni">
                            <label class="custom-file-label" for="file_studio_uni" data-browse="Buscar"></label>
                        </div>
                    </div>
                </div>
            </div>
        @endforelse
        <div id="destino_escolar_uni_c"></div>
        <hr>
        
        <h5>Otros Conocimientos</h5>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_language_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse ($profile->perfilLenguaje as $idioma ) --}}
        @forelse (isset($profile) ? $profile->perfilLenguaje : [] as $idioma )
            <div id="language_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-5">
                        <label for="language">Idioma</label>
                        <input type="text" name="language[]" id="lenguaje_c" class="form-control" value="{{ $idioma->language }}">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="spoken">Conversación</label>
                        <input type="number" min="1" max="100" name="spoken[]" id="spoken_c" class="form-control" value="{{ $idioma->spoken }}">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="reading">Lectura</label>
                        <input type="number" min="1" max="100" name="reading[]" id="reading_c" class="form-control" value={{ $idioma->reading }}>
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="writing">Escritura</label>
                        <input type="number" min="1" max="100" name="writing[]" id="writing_c" class="form-control" value="{{ $idioma->writing }}">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_language_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @empty
            <div id="language_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-5">
                        <label for="language">Idioma</label>
                        <input type="text" name="language[]" id="language_c" class="form-control">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="spoken">Conversación</label>
                        <input type="number" min="1" max="100" name="spoken[]" id="spoken_c" class="form-control">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="reading">Lectura</label>
                        <input type="number" min="1" max="100" name="reading[]" id="reading_c" class="form-control">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="writing">Escritura</label>
                        <input type="number" min="1" max="100" name="writing[]" id="writing_c" class="form-control">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_language_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @endforelse
    
        <div id="destino_language_c"></div>
    
    
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_knowledge_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse ($profile->perfilConocimientos as $conocimiento) --}}
        @forelse (isset($profile) ? $profile->perfilConocimientos : [] as $conocimiento)
            <div id="escolar_knowledge_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <label for="knowledge_type">Competencias Desarrolladas</label>
                        <select name="knowledge_type[]" id="knowledge_type_c" class="form-control">
                            <option disabled value="" selected hidden>Seleccione una opción...</option>
                            <option value="Maquinaria" {{ ($conocimiento->knowledge_type == "Maquinaria") ? 'selected' : null }}>Maquinaria y equipo</option>
                            <option value="Programas" {{ ($conocimiento->knowledge_type == "Programas") ? 'selected' : null }}>Programas y sistemas</option>
                            <option value="Funciones" {{ ($conocimiento->knowledge_type == "Funciones") ? 'selected' : null }}>Funciones de oficina</option>
                        </select>
                    </div>
                    
                    <div class="col-12 mb-3 col-md-8">
                        <label for="knowledge_name">Detalle:</label>     
                        <input type="text" name="knowledge_name[]" id="knowledge_name_c" class="form-control" value="{{ $conocimiento->knowledge_name }}">
                    </div>
        
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_knowledge_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @empty
            <div id="escolar_knowledge_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <label for="knowledge_type">Competencias Desarrolladas</label>
                        <select name="knowledge_type[]" id="knowledge_type_c" class="form-control">
                            <option disabled value="" selected hidden>Seleccione una opción...</option>
                            <option value="Maquinaria">Maquinaria y equipo</option>
                            <option value="Programas">Programas y sistemas</option>
                            <option value="Funciones">Funciones de oficina</option>
                        </select>
                    </div>
                    
                    <div class="col-12 mb-3 col-md-8">
                        <label for="knowledge_name">Detalle:</label>     
                        <input type="text" name="knowledge_name[]" id="knowledge_name_c" class="form-control">
                    </div>
        
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_knowledge_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @endforelse
        <div id="destino_knowledge_c"></div>
        <hr>
    
        <h5>Experiencia Laboral</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-12">
                <label for="motivo">Agregar al menos dos últimos empleos:</label>
            </div>
        </div>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_experience_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse($profile->perfilExperiencia as $experiencia) --}}
        @forelse(isset($profile) ? $profile->perfilExperiencia : [] as $experiencia)
            <div id="escolar_experience_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-5">
                        <label for="job">Puesto Ocupado</label>
                        <input class="form-control mb-3" type="text" name="job[]" id="job_c" value="{{ $experiencia->job }}">
                    </div>
                    <div class="col-12 mb-3 col-md-6">
                        <label for="company">Empresa/Domicilio</label>
                        <input class="form-control mb-3" type="text" name="company[]" id="company_c" value="{{ $experiencia->company }}">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_experience_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <label for="date_begin_experience">Fecha Inicio</label>
                        <input class="form-control mb-3" type="date" name="date_begin_experience[]" id="date_begin_experience_c" value="{{ $experiencia->date_begin_experience }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="date_end_experience">Fecha Fin</label>
                        <input class="form-control mb-3" type="date" name="date_end_experience[]" id="date_end_experience_c" value="{{ $experiencia->date_end_experience }}">
                        <span id="edad_max_error" class="clase_error"><strong>Mayor o igual a edad mínima</strong></span>
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="salary">Sueldo</label>
                        <input class="form-control mb-3" type="text" name="salary[]" id="salary_c" value="{{ $experiencia->salary }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reason_separation">Motivo de Separación</label>
                        <input class="form-control mb-3" type="text" name="reason_separation[]" id="reason_separation_c" value="{{ $experiencia->reason_separation }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-12">
                        <label for="activity">Principales Actividades y Responsabilidades del Puesto</label>
                        <input class="form-control mb-3" type="text" name="activity[]" id="activity_c" value="{{ $experiencia->activity }}">
                    </div>
                </div>
            </div>
        @empty
            <div id="escolar_experience_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-5">
                        <label for="job">Puesto Ocupado</label>
                        <input class="form-control mb-3" type="text" name="job[]" id="job_c">
                    </div>
                    <div class="col-12 mb-3 col-md-6">
                        <label for="company">Empresa/Domicilio</label>
                        <input class="form-control mb-3" type="text" name="company[]" id="company_c">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_experience_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <label for="date_begin_experience">Fecha Inicio</label>
                        <input class="form-control mb-3" type="date" name="date_begin_experience[]" id="date_begin_experience_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="date_end_experience">Fecha Fin</label>
                        <input class="form-control mb-3" type="date" name="date_end_experience[]" id="date_end_experience_c">
                        <span id="edad_max_error" class="clase_error"><strong>Mayor o igual a edad mínima</strong></span>
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="salary">Sueldo</label>
                        <input class="form-control mb-3" type="text" name="salary[]" id="salary_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reason_separation">Motivo de Separación</label>
                        <input class="form-control mb-3" type="text" name="reason_separation[]" id="reason_separation_c">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-12">
                        <label for="activity">Principales Actividades y Responsabilidades del Puesto</label>
                        <input class="form-control mb-3" type="text" name="activity[]" id="activity_c">
                    </div>
                </div>
            </div>
        @endforelse
    
        <div id="destino_experience_c"></div>
    
        <div class="form-row mb-4">
            <div class="col-md-12">
                <h5><strong>Referencias Personales</strong><hr class="mt-1"></h5>
            </div>
        </div>
    
        <div class="float-right">
            <button type="button" class="btn btn-danger mt-4" id="agregar_reference_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse ($profile->perfilReferencias as $referencia) --}}
        @forelse (isset($profile) ? $profile->perfilReferencias : [] as $referencia)
            <div id="escolar_reference_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3 d-none">
                        <label for="type">Referencia</label>
                        <select name="type_ref[]" id="type_ref" class="form-control mb-3">
                            <option value="Personal" selected>Personal</option>
                            {{-- <option value="Laboral" {{ ($referencia->type_ref=='Laboral') ? 'selected' : null }}>Laboral</option> --}}
                        </select>
                    </div>
                    <div class="col-12 mb-3 col-md-9 mt-4 text-right pull-right">
                        <button type="button" class="btn btn-danger borrar_reference_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_name">Nombre:</label>
                        <input class="form-control mb-3" type="text" name="reference_name[]" id="reference_name_c" value="{{ $referencia->reference_name }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_phone">Teléfono:</label>
                        <input class="form-control mb-3" type="text" name="reference_phone[]" id="reference_phone_c" value="{{ $referencia->reference_phone }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_time_meet">Tiempo de conocerla:</label>
                        <input class="form-control mb-3" type="text" name="reference_time_meet[]" id="reference_time_meet_c" value="{{ $referencia->reference_time_meet }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_occupation">Ocupación:</label>
                        <input class="form-control mb-3" type="text" name="reference_occupation[]" id="reference_occupation_c" value="{{ $referencia->reference_occupation }}">
                    </div>
                </div>
            </div>
        @empty
            <div id="escolar_reference_c">
                <hr class="separador d-none">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3 d-none">
                        <label for="type">Referecia</label>
                        <select name="type_ref[]" id="type_ref" class="form-control mb-3">
                            <option value="Personal" selected>Personal</option>
                            {{-- <option value="Laboral">Laboral</option> --}}
                        </select>
                    </div>
                    <div class="col-12 mb-3 col-md-9 mt-4 text-right pull-right">
                        <button type="button" class="btn btn-danger borrar_reference_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_name">Nombre:</label>
                        <input class="form-control mb-3" type="text" name="reference_name[]" id="reference_name_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_phone">Teléfono:</label>
                        <input class="form-control mb-3" type="text" name="reference_phone[]" id="reference_phone_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_time_meet">Tiempo de conocerla:</label>
                        <input class="form-control mb-3" type="text" name="reference_time_meet[]" id="reference_time_meet_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_occupation">Ocupación:</label>
                        <input class="form-control mb-3" type="text" name="reference_occupation[]" id="reference_occupation_c">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right d-none">
                        <button type="button" class="btn btn-danger borrar_reference_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @endforelse
        
        <div id="destino_reference_c"></div>

        <div class="form-row mb-4">
            <div class="col-md-12">
                <h5><strong>Referencias Laborales</strong><hr class="mt-1"></h5>
            </div>
        </div>
    
        <div class="float-right">
            <button type="button" class="btn btn-danger mt-4" id="agregar_work_reference_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>

        @forelse (isset($profile) ? $profile->perfilReferenciasLaborales : [] as $referencia)
            <div id="escolar_work_reference_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3 d-none">
                        <label for="type">Referencia</label>
                        <select name="type_ref[]" id="type_work_ref" class="form-control mb-3">
                            {{-- <option value="Personal" selected>Personal</option> --}}
                            <option value="Laboral" selected>Laboral</option>
                        </select>
                    </div>
                    <div class="col-12 mb-3 col-md-9 mt-4 text-right pull-right">
                        <button type="button" class="btn btn-danger borrar_work_reference_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_name">Nombre:</label>
                        <input class="form-control mb-3" type="text" name="reference_name[]" id="reference_name_c" value="{{ $referencia->reference_name }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_phone">Teléfono:</label>
                        <input class="form-control mb-3" type="text" name="reference_phone[]" id="reference_phone_c" value="{{ $referencia->reference_phone }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_time_meet">Tiempo de conocerla:</label>
                        <input class="form-control mb-3" type="text" name="reference_time_meet[]" id="reference_time_meet_c" value="{{ $referencia->reference_time_meet }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_occupation">Ocupación:</label>
                        <input class="form-control mb-3" type="text" name="reference_occupation[]" id="reference_occupation_c" value="{{ $referencia->reference_occupation }}">
                    </div>
                </div>
            </div>
        @empty
            <div id="escolar_work_reference_c">
                <hr class="separador d-none">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3 d-none">
                        <label for="type">Referecia</label>
                        <select name="type_ref[]" id="type_ref" class="form-control mb-3">
                            {{-- <option value="Personal" selected>Personal</option> --}}
                            <option value="Laboral" selected>Laboral</option>
                        </select>
                    </div>
                    <div class="col-12 mb-3 col-md-9 mt-4 text-right pull-right">
                        <button type="button" class="btn btn-danger borrar_work_reference_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_name">Nombre:</label>
                        <input class="form-control mb-3" type="text" name="reference_name[]" id="reference_name_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_phone">Teléfono:</label>
                        <input class="form-control mb-3" type="text" name="reference_phone[]" id="reference_phone_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_time_meet">Tiempo de conocerla:</label>
                        <input class="form-control mb-3" type="text" name="reference_time_meet[]" id="reference_time_meet_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_occupation">Ocupación:</label>
                        <input class="form-control mb-3" type="text" name="reference_occupation[]" id="reference_occupation_c">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right d-none">
                        <button type="button" class="btn btn-danger borrar_reference_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @endforelse
        
        <div id="destino_work_reference_c"></div>
        <h5>Tiene Parientes Trabajando en la Empresa?</h5>
        <div class="form-row">
            <div class="col-1 col-md-1 p-1 mb-3">
                <input class="mb-3" type="checkbox" name="family_working" id="family_working_c"  {{ !is_null($profile) ? !is_null($profile->perfilSolicitud) ? $profile->perfilSolicitud->family_working == '1' ? 'checked' : '' : '' : '' }}>
                <label for="family_working">Sí</label>
            </div>
            <div class="col-12 mb-3 col-md-6 parientes" style="display: inline-flex;">
                <label for="family_working_name" class="mt-1 pr-2">Empleado: </label>
                <select name="family_working_name" id="family_working_name_c" class="selectpicker form-control mb-3" data-live-search="true">
                    @foreach ($Listausuarios as $id => $usuario)
                        @if(!is_null($profile) && !is_null($profile->perfilSolicitud))
                            @if(!is_null($profile->perfilSolicitud->family_working_name))
                                <option value="{{ $id }}" {{ ($id==$profile->perfilSolicitud->family_working_name) ? 'selected' : null }}> {{ $usuario }}</option>
                            @else
                                <option disabled value="" selected hidden>Seleccione una opción...</option>
                            @endif
                        @else
                            <option disabled value="" selected hidden>Seleccione una opción...</option>
                            {{-- <option value="{{ $id }}"> {{ $usuario }}</option> --}}
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        
        <h5>Disponibilidad Para</h5>
        <div class="form-row">

            <div class="col-3 col-md-3">
                
                <input class="mb-3" type="checkbox" name="availability_travel" id="availability_travel_id" {{ isset($profile->perfilSolicitud) ? $profile->perfilSolicitud->availability_travel == '1' ? 'checked' : null : null}}>
                <label for="availability_travel_id">Viajar</label>
            </div>


            <div class="col-3 col-md-3">
                
                <input class="mb-3" type="checkbox" name="availability_shifts" id="availability_shifts_id" {{ isset($profile->perfilSolicitud) ? $profile->perfilSolicitud->availability_shifts == '1' ? 'checked' : null : null}}>
                <label for="availability_shifts_id">Rolar turno</label>
            </div>



            <div class="col-3 col-md-3">
                
                <input class="mb-3" type="checkbox" name="availability_residence" id="availability_residence_id" {{ isset($profile->perfilSolicitud) ? $profile->perfilSolicitud->availability_residence == '1' ? 'checked' : null : null}}>
                <label for="availability_residence_id">Cambiar de residencia</label>
            </div>

        </div>
        <br>

        <div class="row">
            <div class="col text-right">
                <button type="submit" class="btn morado">{{ ($crear) ? 'Guardar' : 'Actualizar' }}</button>
            </div>
        </div>
    </div>
    