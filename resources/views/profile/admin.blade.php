@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="flash-message" id="mensaje">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-'.$msg))
                <p class="alert alert-{{ $msg }}">{!! Session::get('alert-'.$msg) !!} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    
    @if(Auth::user()->hasRolePermission('user_admin') || Auth::user()->role === 'admin')
        <div class="d-flex flex-row-reverse">
            <a href="{{ url('admin-de-usuarios') }}" class="btn btn-dark" role="button" aria-pressed="true">Listado de Usuarios</a>
        </div>
    @endif
    
    <div class="card">
        <ul class="nav nav-pills mt-3 nav-justified" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="btn-tab active font-weight-bold blue_header" id="basica-tab" data-toggle="pill" href="#basica" role="tab" aria-controls="basica" aria-selected="true">Básica</a>
            </li>
            <li class="nav-item">
                <a class="btn-tab font-weight-bold blue_header" id="complementaria-tab" data-toggle="pill" href="#complementaria" role="tab" aria-controls="complementaria" aria-selected="false">Complementaria</a>
            </li>

            @if(Auth::check())
                @if(is_null(Auth::user()->external))
                    {{-- <li class="nav-item">
                        <a style="background: #8e8e8e;" class="btn-tab font-weight-bold" id="tramite-tab" data-toggle="pill" href="#tramite" role="tab" aria-controls="tramite" aria-selected="false">En trámite</a>
                    </li> --}}
                    <li class="nav-item">
                        <a class="btn-tab font-weight-bold blue_header" id="registro-tab" data-toggle="pill" href="#registro" role="tab" aria-controls="registro" aria-selected="false">Registro</a>
                    </li>
                    {{-- <li class="nav-item">
                        <a style="background: #8e8e8e;" class="btn-tab font-weight-bold" id="nominas-tab" data-toggle="pill" href="#nominas" role="tab" aria-controls="nominas" aria-selected="false">Nóminas</a>
                    </li>
                    <li class="nav-item">
                        <a style="background: #8e8e8e;" class="btn-tab font-weight-bold" id="incidencias-tab" data-toggle="pill" href="#incidencias" role="tab" aria-controls="incidencias" aria-selected="false">Incidencias</a>
                    </li>
                    <li class="nav-item">
                        <a style="background: #8e8e8e;" class="btn-tab font-weight-bold" id="disciplinarios-tab" data-toggle="pill" href="#disciplinarios" role="tab" aria-controls="disciplinarios" aria-selected="false">Disciplinarios</a>
                    </li>
                    <li class="nav-item">
                        <a style="background: #8e8e8e;" class="btn-tab font-weight-bold" id="estadisticas-tab" data-toggle="pill" href="#estadisticas" role="tab" aria-controls="estadisticas" aria-selected="false">Estadísticas</a>
                    </li> --}}
                    <li class="nav-item">
                        <a class="btn-tab font-weight-bold blue_header" id="salud-tab" data-toggle="pill" href="#salud" role="tab" aria-controls="salud" aria-selected="false">Salud</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn-tab font-weight-bold blue_header" id="beneficiarios-tab" data-toggle="pill" href="#beneficiarios" role="tab" aria-controls="beneficiarios" aria-selected="false">Beneficiarios</a>
                    </li>
                    @if (!is_null($profile))
                        <li class="nav-item">
                            <a class="btn-tab font-weight-bold blue_header" id="bienes-tab" data-toggle="pill" href="#bienes" role="tab" aria-controls="bienes" aria-selected="false">Recursos</a>
                        </li> 
                    {{--     <li class="nav-item">
                            <a class="btn-tab font-weight-bold blue_header" id="contrato-tab" data-toggle="pill" href="#contrato" role="tab" aria-controls="contrato" aria-selected="false">Contrato</a>
                        </li> --}}
                        <li class="nav-item">
                            <a class="btn-tab font-weight-bold blue_header" id="cursos-tab" data-toggle="pill" href="#cursos" role="tab" aria-controls="cursos" aria-selected="false">Cursos</a>
                        </li>
                    @endif
                @endif
            @endif
        </ul>
    </div>
    <hr>
    <div class="card-body">
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="basica" role="tabpanel" aria-labelledby="basica-tab">
                @if(is_null($profile))  
                    @isset($vacante)
                        <form action="{{ url('postulante') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="vacante_id" id="vacante_id" value="{{ isset($vacante_id) ? $vacante_id : null }}">
                            @include('profile.basica', ['puesto'=>$puesto])
                        </form>
                    @else
                        <form action="{{ url('createBasica') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @include('profile.basica')
                        </form>
                    @endisset
                @else
                    <form action="{{ url('updateBasica') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="profile_id" id="profile_id" value="{{ isset($profile) ? $profile->id : null }}">
                        @include('profile.basica')
                    </form>

                @endif  
            </div>
            <div class="tab-pane fade" id="complementaria" role="tabpanel" aria-labelledby="complementaria-tab">
                @if(is_null($profile))
                    <form action="{{ url('createComplementaria') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @include('profile.complementaria')
                    </form>
                @else
                    <form action="{{ route('profile.update', $profile->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        @include('profile.complementaria')
                    </form>
                @endif 
            </div>

            <div class="tab-pane fade" id="registro" role="tabpanel" aria-labelledby="registro-tab">
                @if (is_null($profileRegistros))
                    <form action="{{ url('createRegistro') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <input type="hidden" name="profile_id" id="profile_id" value="{{ isset($profile) ? $profile->id : null }}">
                        <input type="hidden" name="user_id" id="user_id" value="{{ !is_null($user) ? $user->id : null }}">
                        @include('profile.registro')
                    </form>
                @else
                    <form action="{{ url('updateRegistro') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <input type="hidden" name="profile_id" id="profile_id" value="{{ isset($profile) ? $profile->id : null }}">
                        @include('profile.registro')
                    </form>
                @endif
            </div>
            <div class="tab-pane fade" id="salud" role="tabpanel" aria-labelledby="salud-tab">
                @if (is_null($profileSalud))
                    <form action="{{ url('createSalud') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <input type="hidden" name="profile_id" id="profile_id" value="{{ isset($profile) ? $profile->id : null }}">
                        <input type="hidden" name="user_id" id="user_id" value="{{ !is_null($user) ? $user->id : null }}">
                        @include('profile.salud')
                    </form>
                @else
                    <form action="{{ url('updateSalud') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <input type="hidden" name="profile_id" id="profile_id" value="{{ isset($profile) ? $profile->id : null }}">
                        @include('profile.salud')
                    </form>
                @endif
            </div>
            <div class="tab-pane fade" id="beneficiarios" role="tabpanel" aria-labelledby="beneficiarios-tab">
                @if (is_null($profileBeneficiarios))
                    <form action="{{ url('createBeneficiarios') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <input type="hidden" name="profile_id" id="profile_id" value="{{ isset($profile) ? $profile->id : null }}">
                        <input type="hidden" name="user_id" id="user_id" value="{{ !is_null($user) ? $user->id : null }}">
                        @include('profile.beneficiarios')
                    </form>
                @else
                    <form action="{{ url('updateBeneficiarios') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <input type="hidden" name="profile_id" id="profile_id" value="{{ isset($profile) ? $profile->id : null }}">
                        @include('profile.beneficiarios')
                    </form>
                @endif
            </div>
            <div class="tab-pane fade" id="bienes" role="tabpanel" aria-labelledby="bienes-tab">

                @if (!is_null($profile))
                    @include('profile.bienes')
                @endif
            </div>
          {{--   <div class="tab-pane fade" id="contrato" role="tabpanel" aria-labelledby="contrato-tab">

                @if (!is_null($profile))
                    <form action="{{ url('updateContrato') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <input type="hidden" name="profile_id" id="profile_id" value="{{ isset($profile) ? $profile->id : null }}">
                        @include('profile.contrato')
                    </form>
                @endif
            </div> --}}
        </div>
    </div>
</div>


@endsection

@section('scripts')
    <style type="text/css">
        .custom-file-label::after {
            left: 0;
            right: auto;
            border-left-width: 0;
            border-right: inherit;
            background-color: #244395;
            color: #ffffff;
        }

        h5 {
            color: #244395;
            font-weight: bold;
        }

        .morado {
            background-color: #244395;
            color: #ffffff;
        }

        .clase_error {
            display: none;
            color: red;
        }
    </style>
    
    <script type="text/javascript">
        $(function() {


            $('#userB').on('change', function() {
                alert( $(this).find(":selected").val() );
            });


            setTimeout(function() {
                $('#mensaje').slideUp('slow');
            }, 3500); 

            $('.custom-file input').change(function (e) {
                $(this).next('.custom-file-label').html(e.target.files[0].name);
            });

            /* $('input').attr('readonly', 'readonly'); */
            var largo = 0;
            $("#agregar").click(function() {
                var cloned = $("#escolar").clone(true,true).appendTo("#destino");
                // cloned.find('input,textarea').val('');
                largo++;
            });

            $("#borrar").click(function() {
                if(largo > 0) {
                    $("#escolar").remove().end().appendTo("#destino");
                    largo--;
                }
            });

            $('#solicitud').on('click', function (){
                $('#name_sol').val($('#name').val());
                $('#surname_father_sol').val($('#surname_father').val());
                $('#surname_mother_sol').val($('#surname_mother').val());
                $('#cellphone_sol').val($('#cellphone').val());
                $('#phone_sol').val($('#phone').val());
                $('#email_sol').val($('#email').val());
                $('#gender_sol').val($('#gender').val());
                $('#date_birth_sol').val($('#date_birth').val());
                $('#rfc_sol').val($('#rfc').val());
            });

            /***********************************************************************************************************/
            /*****************************funciones jquery para la ventana modal de solicitud **************************/
            /***********************************************************************************************************/
            $("#solicitud-modal").on('change', '#date_begin_experience, #date_end_experience', function(e) {
                e.preventDefault();
                var edad_min = $("#date_begin_experience").val();
                var edad_max = $("#date_end_experience").val();
                if (edad_min > edad_max) {
                    $("#edad_max_error").slideDown('slow');
                } else{
                    $("#edad_max_error").slideUp('slow');
                }
            });

            $("#solicitud-modal").on('change', '#state', function(e) {
                e.preventDefault();
                var id = $(this).val();
                $.ajax ({
                    type: 'GET',
                    url: "{{ url('listStates') }}/"+id,
                    dataType: 'json',
                    success: function(response) {
                        $('#city').empty();
                        $.each(response, function(key, element) {
                            $('#city').append("<option value='" + element.id + "'>" + element.name + "</option>").selectpicker('refresh');
                        });
                    }
                });
            });

            var largo_esc = 0;
            var indice_esc = 1;
            var maximo_esc = 3;
            $('#agregar_application').on('click', function() { 
                if(largo_esc < maximo_esc) {     
                    var clone_esc = $("#escolar_application").clone(true);
                    clone_esc.find('input[type=radio]').each(function (index) {
                        var name = $(this).prop('name');
                        $(this).prop('name', name + indice_esc);
                    });
                    //clone_esc.find("input").val("").end();
                    
                    clone_esc.appendTo("#destino_application:last");               
                    largo_esc++;
                    indice_esc++;
                }
            }); 

            $('.borrar_application').on('click', function(){
                if(largo_esc > 0) {
                    var valOption = $(this).parent().parent().parent().find('#studio').children("option:selected").val();
                    $('.s-' + valOption).show();

                    $(this).parent().parent().parent().remove();
                    largo_esc--;
                }
            });

            $("#solicitud-modal").on('change', '#studio', function(e) {
                e.preventDefault();
                var valOption = $(this).val();
                $('.s-' + valOption).hide();
            });



            var largo_esc_uni = 0;
            var indice_esc_uni = 1;
            var maximo_esc_uni = 2;
            $('#agregar_application_uni').on('click', function() {
                if(largo_esc_uni < maximo_esc_uni) {
                    var clone_esc_uni = $("#escolar_application_uni").clone(true);
                    clone_esc_uni.find('input[type=radio]').each(function (index) {
                        var name = $(this).prop('name');
                        $(this).prop('name', name + indice_esc_uni);
                    });
                    // clone_esc.find("input").val("").end();

                    clone_esc_uni.appendTo("#destino_application_uni:last");               
                    largo_esc_uni++;
                    indice_esc_uni++;
                }
            }); 

            $('.borrar_application_uni').on('click', function(){
                if(largo_esc_uni > 0) {
                    var valOptionUni = $(this).parent().parent().parent().find('#studio_uni').children("option:selected").val();
                    $('.s-' + valOptionUni).show();

                    $(this).parent().parent().parent().remove();
                    largo_esc_uni--;
                }
            });

            $("#solicitud-modal").on('change', '#studio_uni', function(e) {
                e.preventDefault();
                var valOptionUni = $(this).val();
                $('.s-' + valOptionUni).hide();
            });


            var largo_esc_language = 0;
            $('#agregar_application_language').on('click', function(){
                var clone_esc_language = $("#escolar_application_language").clone(true);
                // clone_esc.find("input").val("").end();

                clone_esc_language.appendTo("#destino_application_language:last");               
                largo_esc_language++;
            }); 

            $('.borrar_application_language').on('click', function(){
                if(largo_esc_language > 0) {
                    $(this).parent().parent().parent().remove();
                    largo_esc_language--;
                }
            });

            var largo_esc_knowledge = 0;
            $('#agregar_application_knowledge').on('click', function(){
                var clone_esc_knowledge = $("#escolar_application_knowledge").clone(true);
                // clone_esc.find("input").val("").end();

                clone_esc_knowledge.appendTo("#destino_application_knowledge:last");
                largo_esc_knowledge++;
            }); 

            $('.borrar_application_knowledge').on('click', function(){
                if(largo_esc_knowledge > 0) {
                    $(this).parent().parent().parent().remove();
                    largo_esc_knowledge--;
                }
            });


            var largo_esc_experience = 0;
            $('#agregar_application_experience').on('click', function(){
                var clone_esc_experience = $("#escolar_application_experience").clone(true);
                // clone_esc.find("input").val("").end();

                clone_esc_experience.appendTo("#destino_application_experience:last");
                largo_esc_experience++;
            }); 

            $('.borrar_application_experience').on('click', function(){
                if(largo_esc_experience > 0) {
                    $(this).parent().parent().parent().remove();
                    largo_esc_experience--;
                }
            });


            $("#family_working_1, #family_working_0").on('change', function (e) {
                e.preventDefault();
                if ($("#family_working_0").is(":checked")) {
                    $( "#family_working_name" ).prop( "readonly", true );
                    $( "#family_working_name" ).val("");
                } else  {
                    $( "#family_working_name" ).prop( "readonly", false );
                }
            });


            var largo_esc_reference = 0;
            $('#agregar_application_reference').on('click', function(){
                var clone_esc_reference = $("#escolar_application_reference").clone(true);
                // clone_esc.find("input").val("").end();

                clone_esc_reference.appendTo("#destino_application_reference:last");
                largo_esc_reference++;
            }); 

            $('.borrar_application_reference').on('click', function(){
                if(largo_esc_reference > 0) {
                    $(this).parent().parent().parent().remove();
                    largo_esc_reference--;
                }
            });

            var largo_work_reference = $("div[id='escolar_work_reference_c']").length
            var maximo_work_reference_c = 6;

            $('#agregar_work_reference_c').on('click', function(){
                if(largo_reference < maximo_reference_c) {
                    var clone_reference = $("#escolar_work_reference_c").clone(true);
                    // clone_reference.find("input").val("").end();

                    clone_reference.appendTo("#destino_work_reference_c:last");               
                    largo_reference++;
                }
            }); 

            $('.borrar_work_reference_c').on('click', function(){
                if(largo_work_reference > 1) {
                    $(this).parent().parent().parent().remove();
                    largo_work_reference--;
                }
            });

            $("#unionized_0, #unionized_1").on('change', function (e) {
                e.preventDefault();
                if ($("#unionized_0").is(":checked")) {
                    $( "#unionized_name" ).prop( "readonly", true );
                    $( "#unionized_name" ).val("");

                } else  {
                    $( "#unionized_name" ).prop( "readonly", false );
                }
            });


            /***********************************************************************************************************/
            /*****************************funciones jquery para la ventana complementaria ******************************/
            /***********************************************************************************************************/
            $('#state_c').on('change', function(e) {
                e.preventDefault();
                var id = $(this).val();
                $.ajax ({
                    type: 'GET',
                    url: "{{ url('listStates') }}/"+id,
                    dataType: 'json',
                    success: function(response) {
                        $('#city_c').empty();
                        $.each(response, function(key, element) {
                            $('#city_c').append("<option value='" + element.id + "'>" + element.name + "</option>").selectpicker('refresh');
                        });
                    }
                });
            });


            var primero_esc_c = $('select#studio_c:first').val();
            if (primero_esc_c == null) {
                var largo_esc_c = $("div[id='escolar_c']").length;
                var maximo_esc_c = 4;
            } else {
                var largo_esc_c = $("div[id='escolar_c']").length;
                var maximo_esc_c = 4;
            }
            
            var indice_esc_c = 1;
            $('#agregar_escolar_c').on('click', function() { 
                if(largo_esc_c < maximo_esc_c) {     
                    var clone_esc = $("#escolar_c").clone(true);
                    clone_esc.find('input[type=radio]').each(function (index) {
                        var name = $(this).prop('name');
                        $(this).prop('name', name + indice_esc_c);
                    });
                    
                    //clone_esc.find("input").val("").end();
                    clone_esc.find('.borrar_escolar_c').parent().removeClass('d-none');
                    clone_esc.find('.separador').removeClass('d-none');
                    
                    clone_esc.appendTo("#destino_escolar_c:last");               
                    largo_esc_c++;
                    indice_esc_c++;
                }
            }); 

            $('.borrar_escolar_c').on('click', function() {
                if(largo_esc_c > 1) {
                    var valOption = $(this).parent().parent().parent().find('#studio_c').children("option:selected").val();
                    $('.s-' + valOption +'-c').show();

                    $(this).parent().parent().parent().remove();
                    largo_esc_c--;
                }
            });

            $("#studio_c").on('change', function(e) {
                e.preventDefault();
                var valOption = $(this).val();
                $('.s-' + valOption + '-c').hide();
            });

            $('select#studio_c').each(function (i) {
                var valOption = $(this).val();
                $('.s-' + valOption + '-c').hide();
            });

            

            $('select #studio_uni_c').each(function (i) {
                var valOptionUni = $(this).val();
                $('.s-' + valOptionUni + '-c').hide();
            });

            var primero_esc_uni_c = $('select#studio_uni_c:first').val();
            
            if (primero_esc_uni_c == null) {
                var largo_esc_uni_c = $("div[id='escolar_uni_c']").length;
                var maximo_esc_uni_c = 3;
            } else {
                var largo_esc_uni_c = $("div[id='escolar_uni_c']").length;
                var maximo_esc_uni_c = 3;
            }
            var indice_esc_uni_c = 1;
            $('#agregar_escolar_uni_c').on('click', function() { 
                if(largo_esc_uni_c < maximo_esc_uni_c) {     
                    var clone_esc_uni = $("#escolar_uni_c").clone(true);
                    
                    clone_esc_uni.find('input[type=radio]').each(function (index) {
                        var name = $(this).prop('name');
                        $(this).prop('name', name + indice_esc_uni_c);
                    });
                    
                 
                    clone_esc_uni.find('.ver').each(function (index) {
                        
                        $(this).remove();
                    }); 

                    clone_esc_uni.find('input[type=text]').each(function (index) {
                        $(this).prop('value', '');
                    });
                    clone_esc_uni.find('input[type=file]').each(function (index) {
                        $(this).prop('value', '');
                    });
                    

                    //clone_esc_uni.find("input").val("").end();
                    clone_esc_uni.find('.borrar_escolar_uni_c').parent().removeClass('d-none');
                    clone_esc_uni.find('.separador').removeClass('d-none');
                    
                    clone_esc_uni.appendTo("#destino_escolar_uni_c:last");               
                    largo_esc_uni_c++;
                    indice_esc_uni_c++;
                }
            }); 

            $('.borrar_escolar_uni_c').on('click', function(){
                if(largo_esc_uni_c > 1) {
                    var valOptionUni = $(this).parent().parent().parent().find('#studio_uni_c').children("option:selected").val();
                    $('.s-' + valOptionUni +'-c').show();

                    $(this).parent().parent().parent().remove();
                    largo_esc_uni_c--;
                }
            });

            $("#studio_uni_c").on('change', function(e) {
                e.preventDefault();
                var valOptionUni = $(this).val();
                $('.s-' + valOptionUni + '-c').hide();
            });

            

            var largo_language = $("div[id='language_c']").length;
            var maximo_language_c = 4;
            $('#agregar_language_c').on('click', function(){
                if(largo_language < maximo_language_c) {
                    var clone_language = $("#language_c").clone(true);
                    // clone_esc.find("input").val("").end();

                    clone_language.find('input[type=text]').each(function (index) {
                        $(this).prop('value', '');
                    });

                    clone_language.find('input[type=number]').each(function (index) {
                        $(this).prop('value', '');
                    });
                    
                    clone_language.appendTo("#destino_language_c:last");               
                    largo_language++;

                    clone_language.find('.borrar_language_c').parent().removeClass('d-none');
                    clone_language.find('.separador').removeClass('d-none');
                }
            }); 

            $('.borrar_language_c').on('click', function(){
                if(largo_language > 1) {
                    $(this).parent().parent().parent().remove();
                    largo_language--;
                }
            });


            var primero_knowledge = $('select#knowledge_type_c:first').val();
            if (primero_knowledge == null) {
                var largo_knowledge = $("div[id='escolar_knowledge_c']").length
                var maximo_knowledge_c = 3;
            } else {
                var largo_knowledge = $("div[id='escolar_knowledge_c']").length
                var maximo_knowledge_c = 3;
            }
            $('#agregar_knowledge_c').on('click', function(){
                if(largo_knowledge < maximo_knowledge_c) {
                    var clone_knowledge = $("#escolar_knowledge_c").clone(true);
                    // clone_knowledge.find("input").val("").end();

                    clone_knowledge.find('select').each(function (index) {
                        $(this).prop('value', 'Maquinaria');
                    });

                    clone_knowledge.find('input[type=text]').each(function (index) {
                        $(this).prop('value', '');
                    });
                    
                    clone_knowledge.appendTo("#destino_knowledge_c:last");               
                    largo_knowledge++;

                    clone_knowledge.find('.borrar_knowledge_c').parent().removeClass('d-none');
                    clone_knowledge.find('.separador').removeClass('d-none');
                }
            }); 

            $('.borrar_knowledge_c').on('click', function(){
                if(largo_knowledge > 1) {
                    $(this).parent().parent().parent().remove();
                    largo_knowledge--;
                }
            });



            var largo_experience = $("div[id='escolar_experience_c']").length
            var maximo_experience_c = 4;

            $('#agregar_experience_c').on('click', function(){
                if(largo_experience < maximo_experience_c) {
                    var clone_experience = $("#escolar_experience_c").clone(true);
                    // clone_experience.find("input").val("").end();

                    clone_experience.appendTo("#destino_experience_c:last");               
                    largo_experience++;

                    clone_experience.find('.borrar_experience_c').parent().removeClass('d-none');
                    clone_experience.find('.separador').removeClass('d-none');
                }
            }); 

            $('.borrar_experience_c').on('click', function(){
                if(largo_experience > 1) {
                    $(this).parent().parent().parent().remove();
                    largo_experience--;
                }
            });


            var largo_reference = $("div[id='escolar_reference_c']").length
            var maximo_reference_c = 6;

            $('#agregar_reference_c').on('click', function(){
                if(largo_reference < maximo_reference_c) {
                    var clone_reference = $("#escolar_reference_c").clone(true);
                    // clone_reference.find("input").val("").end();

                    clone_reference.find('input[type=text]').each(function (index) {
                        $(this).prop('value', '');
                    });
                    
                    clone_reference.appendTo("#destino_reference_c:last");               
                    largo_reference++;

                    clone_reference.find('.borrar_reference_c').parent().removeClass('d-none');
                    clone_reference.find('.separador').removeClass('d-none');
                }
            }); 

            $('.borrar_reference_c').on('click', function(){
                if(largo_reference > 1) {
                    $(this).parent().parent().parent().remove();
                    largo_reference--;
                }
            });

            
            if ($("#unionized_0_c").is(":checked")) {
                $( "#unionized_name_c" ).prop( "readonly", true );
                $( "#unionized_name_c" ).val("");
            } else  {
                $( "#unionized_name_c" ).prop( "readonly", false );
            }

            $("#unionized_0_c, #unionized_1_c").on('change', function (e) {
                e.preventDefault();
                if ($("#unionized_0_c").is(":checked")) {
                    $( "#unionized_name_c" ).prop( "readonly", true );
                    $( "#unionized_name_c" ).val("");

                } else  {
                    $( "#unionized_name_c" ).prop( "readonly", false );
                }
            });

            /***********************************************************************************************************/
            /***************************** funciones jquery para la ventana salud **************************************/
            /***********************************************************************************************************/
            var largo_salud = $("div[id='salud_s']").length;
            var maximo_salud_c = 4;
            $('#agregar_salud').on('click', function(){
                if(largo_salud < maximo_salud_c) {
                    var clone_salud = $("#salud_s").clone(true);
                    clone_salud.find("input").val("").end();

                    clone_salud.appendTo("#destino_salud:last");               
                    largo_salud++;
                }
            }); 

            $('.borrar_salud').on('click', function(){
                if(largo_salud > 1) {
                    $(this).parent().parent().parent().remove();
                    largo_salud--;
                }
            });


            $("#family_working_0_c").on('change', function (e) {
                console.log("aja");
                e.preventDefault();
                if (!$("#family_working_0_c").is(":checked")) {
                    $( "#family_working_name_c" ).prop( "readonly", true );
                    $( "#family_working_name_c" ).val("");

                    $('#family_working_name_c').empty();
                    $('#family_working_name_c').append("<option value=''>No Tiene familiar</option>").selectpicker('refresh');
                } else  {
                    
                    $.ajax ({
                        type: 'GET',
                        url: "{{ url('listsprofile') }}",
                        dataType: 'json',
                        success: function(response) {

                            $('#family_working_name_c').empty();
                            $.each(response, function(key, element) {
                                console.log(element);
                                $('#family_working_name_c').append("<option value='" + element.id + "'>" + element.nombre + " " + element.paterno + "</option>").selectpicker('refresh');
                            });

                        }
                    });

                    $( "#family_working_name_c" ).prop( "readonly", false );
                }
            });


            /***********************************************************************************************************/
            /*************** funciones jquery para agregar campos dinamicos para sindicatos***************************/
            /***********************************************************************************************************/

            var largo_sindicato_c = $("div[id='sindicato_c']").length;
            var maximo_sindicato_c = 3;
            
            var indice_sindicato_c = 1;


            $('#agregar_sindicatos_c').on('click', function() { 
                if(largo_sindicato_c < maximo_sindicato_c) {     
                    var clone_sindicato = $("#sindicato_c").clone(true);

                    clone_sindicato.find('input[type=text]').each(function (index) {
                        $(this).prop('value', '');
                    });

                    clone_sindicato.appendTo("#destino_sindicato_c:last"); 

                    largo_sindicato_c++;
                    indice_sindicato_c++;
                }
            }); 

            $('.borrar_sindicato_c').on('click', function(){
                if(largo_sindicato_c > 1) {
                    var valOptionUni = $(this).parent().parent().parent().find('#studio_uni_c').children("option:selected").val();
                    $('.s-' + valOptionUni +'-c').show();

                    $(this).parent().parent().parent().remove();
                    largo_sindicato_c--;

                }
            });


            /***********************************************************************************************************/
            /***************************** funciones jquery para la ventana beneficiarios ******************************/
            /***********************************************************************************************************/
            var largo_beneficiario = $("div[id='beneficiario_s']").length;
            var maximo_beneficiario_c = 6;
            $('#agregar_beneficiarios_hijos').on('click', function(){
                if(largo_beneficiario < maximo_beneficiario_c) {
                    var clone_beneficiario = $("#beneficiario_s").clone(true);
                    clone_beneficiario.find("input").val("").end();

                    clone_beneficiario.appendTo("#destino_beneficiarios_hijos:last");               
                    largo_beneficiario++;
                }
            }); 

            $('.borrar_beneficiarios_hijos').on('click', function(){
                if(largo_beneficiario > 1) {
                    $(this).parent().parent().parent().remove();
                    largo_beneficiario--;
                }
            });

        });
    </script>
@endsection
