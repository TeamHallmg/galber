@extends('layouts.app')
@section('content')

<div class="row justify-content-center px-3 mb-4">
	<div class="col-md-10 bg-blue rounded-xl pt-3 py-1 px-5">
		<p class="text-white font-weight-bold font-italic announcement-title">
			<i class="fas fa-handshake"></i>
			Convenios
		</p>
	</div>
</div>

<div class="row justify-content-center">
	<div class="col-md-10">
		@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner']])
	</div>

	@if(Auth::user()->hasRolePermission('announcement_categories'))
		<div class="col-md-10 text-right">
			<div class="my-3">
				<a href="{{ url('announcement_categories') }}" class="btn btn-blue">Editar Categorías</a>
			</div>
		</div>
	@endif

	<div class="col-md-10">
		<div id="category-slider">
			@foreach ($categories as $category)
				<div class="{{ $category->id }}">
					<div style="margin: 0 5px" class="category-slide">
						<div class="convenio-container" 
                            style="
                                background-image:url('{{ asset('img/announcements/categories/' . $category->image) }}');
                                background-size:cover;
                                background-position:center;
						">
						</div>
					</div>
					
					<div class="slider-div slider-text-bg">
						<h3 class="slider-h">{{ $category->name }}</h3>
					</div>
				</div>
			@endforeach
		</div>

		<div class="float-right mt-2">
			<button type="button" class="btn btn-lightblue convenios-btn-info" id="convenios-clear_filter">Todas las categorías</button>
		</div>

		<div class="clearfix mb-2"></div>

		<hr class="hr-convenios">

		@if(Auth::user()->hasRolePermission('announcement_convenios'))
			<div class="float-right mt-2">
				<a href="{{ url('announcements?view=' . $display_announcements['convenios']['view'] . '&type=' . $display_announcements['convenios']['type']->id )}}" class="btn btn-blue">Editar Convenios</a>
				<a href="{{ url('announcement_actions_report')}}" class="btn btn-blue">Reporte Convenios</a>
			</div>
		@endif

		<div class="clearfix mb-2"></div>

		<div class="row">
			<div class="col-md-12 text-right">
				<button class="btn convenio-nuevos-btn btn-convenio" style="margin-bottom: 2rem" id="nuevos_convenios"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 1rem"></i> Ver nuevos</button>
				<button class="btn convenio-favoritos-btn btn-convenio" style="margin-bottom: 2rem" id="favoritos_convenios"><i class="fa fa-star" aria-hidden="true" style="margin-right: 1rem"></i> Mis Favoritos</button>
				<button class="btn convenio-likes-btn btn-convenio" style="margin-bottom: 2rem" id="likes_convenios"><i class="fa fa-thumbs-up" aria-hidden="true" style="margin-right: 1rem"></i> De mi interés</button>
			</div>
			<div class="col-md-7 col-xs-12" style="margin-top: 1rem">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text" id="lbl_searchbox">Buscar: </span>
					</div>
					<input type="text" class="form-control" id="searchbox" aria-describedby="lbl_searchbox">
				</div>
			</div>
			{{-- <div class="col-md-5 col-xs-12" style="margin-top: 1rem">
				<select class="form-control selectpicker selectpicker-small" data-size="6" data-live-search="true" id="area_select">
					<option value="">Todas las áreas...</option>
					@foreach ($areas as $key => $area)
						<option value="{{ $key }}">{{ $area }}</option>
					@endforeach
				</select>
			</div> --}}
		</div>
		<div class="clearfix" style="margin-bottom: 1rem"></div>
		<div class="text-center bg-warning" id="no_categories" style="padding: 1rem 0; display: none">
			<h3>¡Próximamente!</h3>
		</div>
		<div class="row" id="row_convenios">
			@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['convenios']])
		</div>
	</div>
</div>


<div id="convenios_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.5);" data-convenio="">
	<div class="modal-dialog modal-lg">
	  <div class="modal-content">
		<div class="modal-header bg-blue text-white">
			<h5 class="modal-title" id="exampleModalLabel">Convenio</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="convenio-modal-container mx-auto" id="convenios-modal-img"
				style="
					background-image:url('https://via.placeholder.com/300x150');
					background-size:cover;
					background-position:center;
			">
			</div>
			<div class="text-center" style="margin-top: 1rem">
				<h3 class="convenios-modal-title" id="convenios-modal-title">
					SIN INFORMACIÓN
				</h3>
				<a href="" id="convenios-modal-link" target="_blank">Visitar el sitio <i class="fas fa-external-link-alt"></i></a>
			</div>
			<div style="margin-top: 2rem">
				<p class="text-center" id="convenios-modal-description">
					Sin información
				</p>
			</div>
		</div>
		<div class="text-center">
			<i class="convenios-modal-btn fa-star big-fa" data-id="" id="convenios-modal-favorite" aria-hidden="true"></i>
			<i class="convenios-modal-btn fa-thumbs-up big-fa" data-id="" id="convenios-modal-like" aria-hidden="true"></i>
		</div>
		<div class="text-right" style="margin: 0 1rem 1rem 0">
			<button type="button" class="btn btn-success" data-dismiss="modal">Regresar</button>
		</div>
	  </div>
  
	</div>
</div>
@endsection

@section('scripts')
<script>
	$(function(){
		$('#category-slider').slick({
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			adaptiveHeight: true,
			autoplay: true,
			autoplaySpeed: 4500,
			responsive: [
				{
					breakpoint: 768,
					settings: {
						arrows: false,
						dots: false,
						infinite: true,
						slidesToShow: 2,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 600,
					settings: {
						arrows: false,
						dots: false,
						infinite: true,
						slidesToShow: 1,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 480,
					settings: {
						arrows: false,
						dots: false,
						infinite: true,
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			] 
		});


		$('.convenio-card').on('click', function(){
			const img_src = $(this).attr('src');
			console.log(img_src);
			const title = $(this).attr('alt');
			const description = $(this).data('description');
			const parent_convenio = $(this).closest('.convenios-card');
			const id = parent_convenio.attr('id');
			const link = $(this).data('link') === '#' ? null : $(this).data('link');
			const isLike = parent_convenio.hasClass('c_like');
			const isFavorite = parent_convenio.hasClass('c_favorite');
			$('#convenios-modal-favorite').toggleClass('convenios-favorite', isFavorite).toggleClass('far', !isFavorite).toggleClass('fas', isFavorite).data('id', id);
			$('#convenios-modal-like').toggleClass('convenios-like', isLike).toggleClass('far', !isLike).toggleClass('fas', isLike).data('id', id);
			// $('#convenios-modal-img').attr('src', img_src);
			$('#convenios-modal-img').css('background-image', 'url('+'"'+img_src+'"'+')');
			if(!!link){
				$('#convenios-modal-link').show();
				$('#convenios-modal-link').attr('href', link);
			}else{
				$('#convenios-modal-link').hide();
			}
			$('#convenios-modal-title').html(title);
			$('#convenios-modal-description').html(description);
			sendWatchedActionAjax(id, 'watched');
		});

		$('#convenios-clear_filter').on('click', function(){
			$('.c_hidden:not(.n_hidden):not(.a_hidden)').show();
			$('.c_hidden').removeClass('c_hidden');
			$('#row_convenios').show();
			$('#no_categories').hide();
		});

		$('.slick-slide').on('click', function(){
			let valorTot = $(this).attr('class');
			let valorId = valorTot.split(' ');
			const id = valorId[0];
			// const id = $(this).attr('id');
			const count = $(`.c_${id}`).length;

			// console.log(slideClicked, id);
			// console.log(id, count);
			if(count === 0){
				$('#no_categories').show();
				$('#row_convenios').hide();
			}else{
				$('#row_convenios').show();
				$('#no_categories').hide();
			}
			$('.c_hidden:not(.n_hidden):not(.f_hidden):not(.l_hidden):not(.a_hidden)').show();
			$('.c_hidden').removeClass('c_hidden');
			$(`.convenios-card:not(.c_${id})`).addClass('c_hidden').hide();
			$('.slider-text-bg_active').toggleClass('slider-text-bg_active slider-text-bg');
			$(this).find('.slider-text-bg').toggleClass('slider-text-bg_active slider-text-bg');
			filter_textbox();
		});

		$('#nuevos_convenios').on('click', function(){
			if($(this).hasClass('clicked')){
				$('.n_hidden:not(.c_hidden):not(.f_hidden):not(.l_hidden):not(.a_hidden)').show();
				$('.n_hidden').removeClass('n_hidden');
			}else{
				$('.convenios-card:not(.convenios_nuevos)').addClass('n_hidden').hide();
			}
			$(this).toggleClass('clicked');
			$(this).find('i').toggleClass('fa-eye fa-eye-slash')
			filter_textbox();
		});
		
		$('#favoritos_convenios').on('click', function(){
			if($(this).hasClass('clicked')){
				$('.f_hidden:not(.n_hidden):not(.c_hidden):not(.l_hidden):not(.a_hidden)').show();
				$('.f_hidden').removeClass('f_hidden');
			}else{
				$('.convenios-card:not(.c_favorite)').addClass('f_hidden').hide();
			}
			$(this).toggleClass('clicked');
			$(this).find('i').toggleClass('far fa')
			filter_textbox();
		});

		$('#likes_convenios').on('click', function(){
			if($(this).hasClass('clicked')){
				$('.l_hidden:not(.n_hidden):not(.c_hidden):not(.f_hidden):not(.a_hidden)').show();
				$('.l_hidden').removeClass('l_hidden');
			}else{
				$('.convenios-card:not(.c_like)').addClass('l_hidden').hide();
			}
			$(this).toggleClass('clicked');
			$(this).find('i').toggleClass('far fa')
			filter_textbox();
		});

		function changeActionStyle(parent_card, btn, i){
			if(i.hasClass('fa-star')){
				btn.toggleClass('convenios-favorite');
				parent_card.toggleClass('c_favorite');
			}else{
				btn.toggleClass('convenios-like');
				parent_card.toggleClass('c_like');
			}
			i.toggleClass('far fas');
		}

		function sendActionAjax(id, type, btn, i){
			$.ajax({
				url: "{{ route('announcement_user_action') }}",
				method: 'POST',
				data: {
					announcement_id: id,
					action: type,
					_token: "{{csrf_token()}}",
				},
				success: function(data){
					if(!data.success){
						setTimeout(function(){
							alert(data.error);
							changeActionStyle(btn, i);
						}, 500);
					}
				}
			})
		}

		function sendWatchedActionAjax(id, type){
			$.ajax({
				url: "{{ route('announcement_mark_as_watched') }}",
				method: 'POST',
				data: {
					announcement_id: id,
					action: type,
					_token: "{{csrf_token()}}",
				},
			})
		}

		/* $('#area_select').on('change', function(){
			const val = $(this).val();
			if(val){
				$.ajax({
					url: "{{ route('announcement_in_area') }}",
					method: 'GET',
					data: {
						'area': val,
					},
					success: function(data){
						if(data.success){
							let queryIds = [];
							for (const area of data.areas) {
								queryIds.push('#' + area);
							}
							queryIds = queryIds.join(',');
							$('.convenios-card').removeClass('a_hidden');
							$('.convenios-card').removeClass('convenio-area');
							$(queryIds).addClass('convenio-area');
							$('.convenios-card:not(.convenio-area)').addClass('a_hidden').hide();
							$('.convenio-area:not(.c_hidden):not(.n_hidden):not(.f_hidden):not(.l_hidden)').show();
							filter_textbox();
						}
					}
				})
			}else{
				$('.convenios-card:not(.c_hidden):not(.n_hidden):not(.f_hidden):not(.l_hidden)').show();
				$('.convenios-card').removeClass('convenio-area').removeClass('a_hidden');
			}
			filter_textbox();
		}); */

		$('.convenios-modal-btn').on('click', function(){
			const convenio_id = $(this).data('id');
			const fa_id = $(this).attr('id');
			$('#convenios_modal').data('convenio', convenio_id);
			const action_class = fa_id === 'convenios-modal-like'?'convenios-like':'convenios-favorite';
			const action_type = fa_id === 'convenios-modal-like'?'like':'favorite';
			$('#'+fa_id).toggleClass(action_class).toggleClass('far fas');
			const parent_card = $('#'+convenio_id);
			const btn = parent_card.find('.convenios-btn[data-type="' + action_type + '"]');
			const i = btn.find('i');
			const id = btn.data('id');
			const type = btn.data('type');
			changeActionStyle(parent_card, btn, i);
			sendActionAjax(id, type, btn, i);
		});

		$('.convenios-btn').on('click', function(){
			const btn = $(this);
			const parent_card = $(this).closest('.convenios-card');
			const i = $(this).find('i');
			const id = $(this).data('id');
			const type = $(this).data('type');
			changeActionStyle(parent_card, btn, i);
			sendActionAjax(id, type, btn, i);
		});		

		$('#searchbox').on('keyup', function(){
			filter_textbox();
		})

		function filter_textbox(){
			const text = $('#searchbox').val().toLowerCase();
			const boxes = $('.convenios-card:not(.c_hidden):not(.n_hidden):not(.f_hidden):not(.l_hidden):not(.a_hidden)');
			boxes.filter(function() {
				$(this).toggle($(this).find('.convenio_text').text().toLowerCase().indexOf(text) > -1);
			});
		}
	});
</script>
@endsection