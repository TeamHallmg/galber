@extends('layouts.app')

@section('content')

<div class="card border-0">
    <div class="card-body">
        <div class="p-2">
            <div class="row mb-4">
                <div class="col bg-blue rounded-xl pt-3 py-1 px-5 mx-3">
                    <p class="text-white font-weight-bold font-italic announcement-title">
                        Objetivos de Calidad
                    </p>
                </div>
            </div>

            {{-- @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner']]) --}}
            
            <div class="row justify-content-center mt-5">
                <iframe width="1280" height="720" src="https://app.powerbi.com/view?r=eyJrIjoiZWYyY2EyMDEtNWNkMS00ZDRkLTg3ODQtYmMzYTRlZDAyMDAxIiwidCI6IjQ1MmEzNTNmLWQ0MjMtNDZiYy05ZDhkLTdjM2ViYmViOTA0ZCJ9&pageName=ReportSectiona341c3943a185342a3a4" frameborder="0" allowFullScreen="true"></iframe>
            </div>
        </div>
    </div>
</div>
@endsection