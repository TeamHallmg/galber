<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Ucin') }}</title>
    <link rel="shortcut icon" type="image/ico" href="{{ asset('favicon.ico') }}"/>

    <!-- Scripts -->
    <!--<script src="{{ asset('js/app.js') }}" defer></script>-->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vacations.css') }}" rel="stylesheet">
    <link href="{{ asset('fullcalendar/core/main.css') }}" rel='stylesheet' />
    <link href="{{ asset('fullcalendar/daygrid/main.css') }}" rel='stylesheet' />
    <link href="{{ asset('fullcalendar/bootstrap/main.css') }}" rel='stylesheet' />
    <link href="{{ asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

    <link href="https://cdn.datatables.net/rowgroup/1.1.2/css/rowGroup.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
    <!-- Date Range Picker -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-91RFRCCLVC"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-91RFRCCLVC');
    </script>

</head>
<body class="bg-white">
    <div id="app">
        <header class="bg-white">   
            <div class="container-fluid py-2">
                <div class="row justify-content-center">
                    <div class="col-10">
                        <div class="row d-flex justify-content-around align-content-center">
                            <div class="col-6 d-flex align-items-center">
                                <a href="{{ url('home') }}">
                                    <img class="img-fluid header_logo" src="/img/logo.png" alt="">
                                </a>
                            </div>
                            <div class="col-6 row">
                                <div class="col-md-10 text-right mt-4">
                                    <h5><strong>Bienvenido</strong></h5>

                                    <div class="row justify-content-end">
                                        <div class="dropdown"> 
                                            <a class="dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="align-middle mr-2"><i class="fas fa-th text-blue fa-2x"></i></span> <strong>{{ Auth::user()->getFullNameAttribute() }}</strong>
                                            </a>
                                        </div>
                                    </div>
                                    
                                    <a class="text-blue" href="{{ route('logout') }}"><i class="fas fa-power-off"></i> Cerrar sesión</a>
                                </div>
                                <div class="col-md-2 d-flex align-items-center">
                                    <!--<a href="{{ url('curriculum') }}">-->
                                        <div class="photo-container" 
                                        style="
                                        background-image:url('{{  asset(Auth::user()->getEmployeePhoto()) }}');
                                        background-size:cover;
                                        background-position:center;
                                        ">
                                        </div>
                                    <!--</a>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <nav class="navbar navbar-expand-md navbar-dark navbar-laravel shadow-3">
            <div class="container-fluid d-flex justify-content-center">
                {{--<a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>--}}
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="col-10">
                    <div class="collapse navbar-collapse text-white" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav d-flex justify-content-between w-100">
                            <li class="nav-item dropdown col p-0">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Desarrollo <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu dropdown-menu-center bg-blue border-0 rounded-0" aria-labelledby="navbarDropdown">
                                    <a class="text-decoration-none" tabindex="-1" href="/que-es-evaluacion-desempeno"><li class="dropdown-item text-white dropdown-item-dark">Evaluación Cualitativa</li></a>
                                    <a class="text-decoration-none" tabindex="-1" href="/que-es-evaluacion-resultados"><li class="dropdown-item text-white dropdown-item-dark">Evaluación Cuantitativa</li></a>
                                </ul>
                            </li>
                            <li class="nav-item dropdown col p-0">
                                <a id="navbarDropdown" class="nav-link" href="{{ url('/que-es') }}" role="button" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Evaluación de Desempeño y Resultados <span class="caret"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="row justify-content-center">
                <div class="col-md-8 text-center">
                    @include('flash::message')
                </div>
            </div>

            @if(View::hasSection('sidebar'))
                <div class="container-fluid col-12 col-md-10">
                    <div class="row">
                        <div class="col-md-3">
                            @yield('sidebar')
                        </div>
                        <div class="col-md-9">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @yield('content')
                        </div>
                    </div>
                </div>
            @else
                <div class="container-fluid col-12 col-md-10">
                    <div class="row">
                        <div class="col-md-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @yield('content')
                        </div>
                    </div>
                </div>
            @endif
            
        </main>
    </div>
</body>
<footer>
    <div class="container col-md-8">
        <div class="row pt-5">
        </div>
        <div class="row pt-5">
        </div>
        <div class="row pt-5">
        </div>
        <div class="row d-flex align-items-end pt-5">
            <div class="col-md-12 text-white text-center">
                Copyright 2019 HallMG. Desingned by HallMG Internet Solutions.
            </div>
        </div>
    </div>
</footer>
</html>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-64XRSJ51RZ"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-64XRSJ51RZ');
</script>

{{-- @yield('mainScripts') --}}
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('fullcalendar/core/main.js') }}"></script>
<script src="{{ asset('fullcalendar/daygrid/main.js') }}"></script>
<script src="{{ asset('fullcalendar/interaction/main.js') }}"></script>
<script src="{{ asset('fullcalendar/bootstrap/main.js') }}"></script>
<script src="{{ asset('/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap-switch.min.js') }}"></script>
<script src="{{ asset('/js/highcharts.js') }}"></script>
<script src="{{ asset('/js/highcharts-3d.js') }}"></script>
<script src="{{ asset('/js/html2canvas.js') }}"></script>
<script src="{{ asset('/js/jquery.autocomplete.js') }}"></script>
<script src="{{ asset('/js/bootstrap-datepicker.min.js') }}"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script src="https://cdn.datatables.net/rowgroup/1.1.2/js/dataTables.rowGroup.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>

<!-- Date range picker -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<!-- Input Mask -->
<script src="{{ asset('/js/jquery.inputmask.bundle.js') }}"></script>
<script type="text/javascript">
    $(document).ready( function () {
        //$('div.alert').not('.alert-important').delay(3000).fadeOut(350);

        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
<script>
    $('#summernote').summernote({
      placeholder: '',
      tabsize: 2,
      height: 400,
      toolbar: [
        ['style', ['style']],
        ['hr', ['hr']],
        ['fontsize', ['fontsize']],
        ['font', ['bold', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']],
      ]
    });

    $('.multiple-items').slick({
        dots: false,
        infinite: true,
        speed: 2500,
        autoplay:true,
        autoplaySpeed:2500,
        slidesToShow: 5,
        slidesToScroll: 1,
        centerMode: true,
        arrows:true,
        responsive: [
            {
                breakpoint: 900,
                settings: {
                    arrows: false,
                    dots: false,
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    dots: false,
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.multiple-items').removeClass("d-none");
</script>

@yield('scripts')
@stack('scripts')