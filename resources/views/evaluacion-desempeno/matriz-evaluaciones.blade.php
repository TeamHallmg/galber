@extends('layouts.app')

@section('title', 'Red de Interacción')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }}" alt="">
		
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Red de Interacción
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
  

				<div class="row"> 
					<div class="col-12 offset-md-4 col-md-3 text-center">
						<div class="form-group">
							<label for="id_plan" class="font-weight-bolder mb-0">Periodo:</label>							
							<select class="form-control periodo_matriz">
							
			@foreach ($periodos as $periodo)

			<option value="{{$periodo->id}}" <?php if ($id_periodo == $periodo->id){ ?>selected="selected"<?php } ?>>{{$periodo->descripcion}}</option>
		@endforeach
							</select>
						</div>
					</div> 
				</div>
 
				<div class="card mt-3">
					<h5 class="card-header bg-info text-white font-weight-bolder">¿Desea generar la red de interacción para el Periodo <?php echo $descripcion?>?</h5>
					<div class="card-body">
						<div class="form-group text-center">
							<a href="/Matriz-Evaluacion?id_periodo={{$id_periodo}}" class="btn btn-primary">Generar Red de Interacción de Manera Manual</a>
					  </div>
					</div>
				</div>

	 

    	@if ($status == 'Preparatorio')
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Red de interacción</h5>
			<div class="card-body">
				<div class="row">
					<div class="form-group text-center col-md-6">
						<a href="/importacion-evaluadores-csv?id_periodo={{$id_periodo}}" class="btn btn-primary">Generar Red de Interacción con Archivo Externo</a>
					</div>
					<div class="form-group text-center col-md-6">
						<a href="/create-red/{{$id_periodo}}" class="btn btn-primary">Generar Red de Interacción de Manera Automática</a>
					</div>
				</div>
			</div>
		</div>
 
		  
    	@endif
    	
		
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Tipos de Evaluación</h5>
			<div class="card-body">
				<div class="form-group text-center">
					<a href="/tipos-evaluacion" class="btn btn-primary">Editar</a>
			  </div>
			</div>
		</div>
		 
</div>
</div>
</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

	$(document).ready(function(){

		$('select.periodo_matriz').change(function(){

			window.location = '/matriz-evaluaciones/' + $(this).val();
		});
	});
</script>
@endsection