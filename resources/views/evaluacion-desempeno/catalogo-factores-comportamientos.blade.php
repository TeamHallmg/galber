@extends('layouts.app')

@section('title', 'Catalogo de Factores - Comportamientos')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-responsive" src="{{ asset('img/evaluacion-desempeno/que-es/ban.png') }}" alt="">

	<!--</div>
		<div class="col-md-12">-->
			<p class="margin-top-20 titulos-evaluaciones" style="font-size: 18px;">
			Catalogo de Factores y Comportamientos
			</p>


			<p class="margin-top-20">
				Las competencias que se evalúan están ligadas al desempeño idóneo de una actividad inherente a un proceso de trabajo.
			</p>
			<p class="margin-top-20" >
				Dichas competencias son:
			</p>
			<p class="margin-top-20" >
				- Definibles
				<br>- Observables
				<br>- Medibles
				<br>- Factibles de ser jerarquizadas
				<br>- Factibles de ser desarrolladas
			</p>
			<p class="margin-top-20" >
Teniendo en cuenta que las grandes empresas valoran a quienes han desarrollado sus habilidades blandas, entre las competencias más buscadas con el MED figuran: sentido de responsabilidad, honestidad, habilidad para comunicarse en forma efectiva, disposición para trabajar en equipo, adaptación al cambio y proactividad.
			</p>

			<p class="margin-top-20" >
				<img class="img-responsive" src="img/evaluacion-desempeno/catalogo/tabla.png" alt="">
			</p>

			<!--<p class="margin-top-20" >
				<a href="{{ url('doc/CatalogodeFactoresyComportamientos.xlsx') }}">
					<img class="img-responsive center" src="img/evaluacion-desempeno/catalogo/boton.png" alt="">
				</a>
			</p>

			<div class="margin-top-20">

					<ul class="list-unstyled font-18">

					</ul>
				</div>-->

			</div>

</div>
@endsection
