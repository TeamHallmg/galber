@extends('layouts.app')

@section('title', 'Formato de Evaluacion')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('/img/evaluacion_desempeno.png') }}" alt="">

	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Evaluación
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">



			<div class="card mb-3">
				<h5 class="card-header bg-info text-white font-weight-bolder">Estás evaluando a</h5>
				<div class="card-body">
				<div class="media">
					<img src="{{asset('img/vacantes/sinimagen.png')}}" style="width: 120px" class="align-self-center mr-3" alt="...">
					<div class="media-body">
					
					<p><h5 class="mt-0 d-inline">Nombres: </h5> {{ str_replace('Ã‘','Ñ',$empleado[0]->first_name)}} {{str_replace('Ã‘','Ñ',$empleado[0]->last_name) }}</p>
					</div>
				</div>
				</div>
				</div>

 
	<?php $total_preguntas = 0;
				$comentario = 0; ?>
		
	@if (count($results) > 0)
		<table class="table-bordered">
			<thead>
				<th class="text-center cabeceras-tablas-evaluaciones" style="padding: 10px; display: none">#</th>
				<th class="text-center cabeceras-tablas-evaluaciones" style="padding: 10px; width: 50%">Factor</th>
				<th class="text-center cabeceras-tablas-evaluaciones" style="padding: 10px">Preguntas</th>
			</thead>
			<tbody>
		
		@for($i = 0; $i < count($results); $i++)

<?php $total_preguntas++; ?>
	
			@if ($current_factor != $results[$i]->id)

		  	@if ($current_factor != 0)
          			
          </td>
	      </tr>

	      	@if ($comentario == 1)

	      <tr <?php if (empty($comentarios[$results[$i-1]->id]) && $evaluation->status != 'No Iniciada'){ ?>style="background-color: red; color: white"<?php } ?>>
	      	<td colspan="2">
	      		<textarea class="form-control comentarios" data-factor="{{$current_factor}}" placeholder="Agregue un comentario que ayude a mejorar su competencia o que sugieres para mejorar su competencia">{{(in_array($results[$i-1]->id, $factores) && !empty($comentarios[$results[$i-1]->id]) ? $comentarios[$results[$i-1]->id] : '')}}</textarea>
	      	</td>
	      </tr>
	      	@endif
	    	@endif

	<?php $current_factor = $results[$i]->id;
				$comentario = $results[$i]->comentarios; ?>

	      <tr <?php if ($total_preguntas % 2 == 0){ ?>style="background-color: #0098DA; color: white"<?php } ?>>
	      	<td style="padding: 10px; display: none" class="id_factor">{{ $results[$i]->id}}</td>
	      	<td class="text-center" style="padding: 10px">
	      		<h3 class="text-center titulos-evaluaciones" style="margin-top: 0">{{ $results[$i]->nombre }}</h3>
	      		<p class="text-center">{{ $results[$i]->descripcion }}</p>
	      		<!--<div>De las 4 opciones de la derecha selecciona el comportamiento que más describa a la persona que estas evaluando.</div>-->
	      	</td>
	      	<td>
	  	@endif

	  				<div class="d-flex align-items-center align-content-center" style="border-top: 1px solid #999; position: relative; padding: 10px; <?php if (!isset($niveles_dominio[$results[$i]->id_pregunta]) && $evaluation->status != 'No Iniciada'){ ?>background-color: red; color: white<?php } ?>">
	  					<div style="display: inline-block; width: 60%" class="text-justify">{{ $results[$i]->nivel_dominio }}</div>
	  					<div class="d-flex align-items-center align-content-center mr-2" style="display: inline-block; position: absolute; right: 0; width: 33%">
	  						<select class="form-control" name="factor[][{{ $results[$i]->id_pregunta }}]" id="{{$results[$i]->id_pregunta}}">

	  	@if (!isset($niveles_dominio[$results[$i]->id_pregunta]))

	  							<option value="nada">--Selecciona--</option>
	  	@endif
	  							
	  	@foreach($etiquetas as $value)

	  							<option value="{{$value->valor}}" <?php if (isset($niveles_dominio[$results[$i]->id_pregunta]) && $niveles_dominio[$results[$i]->id_pregunta] == $value->valor){ ?>selected="selected"<?php } ?>>{{$value->name}}</option>
	  	@endforeach

	  						</select>
	  					</div>
	  				</div>
		@endfor

		@if ($current_factor != 0)
          			
          </td>
	      </tr>

	    @if ($comentario == 1)

	      <tr <?php if (empty($comentarios[$results[$i-1]->id]) && $evaluation->status != 'No Iniciada'){ ?>style="background-color: red; color: white"<?php } ?>>
	      	<td colspan="2">
	      		<textarea class="form-control comentarios" data-factor="{{$current_factor}}" placeholder="Agregue un comentario que ayude a mejorar su competencia o que sugieres para mejorar su competencia">{{(in_array($results[$i-1]->id, $factores) && !empty($comentarios[$results[$i-1]->id]) ? $comentarios[$results[$i-1]->id] : '')}}</textarea>
	      	</td>
	      </tr>
	    @endif
	  @endif

			</tbody>
		</table>
		<p class="text-center margin-top-20">
			<form action="/guardar-respuestas" method="post" class="guardar_respuestas">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="total_preguntas" value="{{$total_preguntas}}">
				<button class="btn btn-primary btn-Finvivir-yellow" type="submit">Guardar</button>
				<a href="{{ url()->previous() }}" class="btn btn-danger">Regresar</a>
			</form>
		</p>
	@else

	<div class="row">
				
		<div class="col-md-12 text-center">
			
			<div class="alert alert-warning h4" role="alert">
				<i class="fa fa-exclamation-triangle"></i>	Aún no hay preguntas para esta evaluación
			</div>
	
		</div>
	</div>
 	@endif
	<!--</div>
		<div class="col-md-12">-->
	</div>
</div>
</div>
</div>
@endsection

@section('scripts')
	<script>

	var id_evaluado = '<?php echo $empleado[0]->id_evaluado?>';
	var total_preguntas = <?php echo $total_preguntas ?>;
	var guardar = false;
	var alertar = false;
		
	$(document).ready(function(){

		$.ajaxSetup({
    		headers: {
        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
		});

		$('select').change(function(){

			var id_factor = $(this).parent().parent().parent().parent().find('.id_factor').text();
			var nivel_dominio = $(this).val();
			var id_pregunta = $(this).attr('id');
			$(this).find('option[value="nada"]').remove();

			$.ajax({
        		type:'POST',    
        		url: '../guardar-respuesta',
        		data:{
          			id_factor : id_factor,
          			id_pregunta : id_pregunta,
          			nivel_dominio : nivel_dominio,
          			id_evaluado : id_evaluado,
          			total_preguntas : total_preguntas
        			}
      		});
		});

		$('form.guardar_respuestas').submit(function(){

			var comentarios_blanco = 0;
			var preguntas_en_blanco = 0;

			$('textarea.comentarios').each(function(){

				if ($(this).val().trim().length != 0){

					$('form.guardar_respuestas').prepend('<input type="hidden" name="factores[]" value="' + $(this).attr('data-factor') + '">');
					$('form.guardar_respuestas').prepend('<input type="hidden" name="comentarios[]" value="' + $(this).val() + '">');
				}

				else{

					comentarios_blanco++;
				}

				if ($(this).parent().parent().prev().find('select option[value="nada"]').length > 0){

					preguntas_en_blanco++;
				}
			});

			if (comentarios_blanco > 0 || preguntas_en_blanco > 0){

				alert('El cuestionario no ha sido concluido correctamente, favor de llenar los campos y/o comentarios.');
			}

			$('form.guardar_respuestas').prepend('<input type="hidden" name="id_evaluado" value="' + id_evaluado + '">');
			guardar = true;
		});
	});

	setInterval(function(){

		if (alertar){

			var num_comentarios = 0;
			var preguntas_en_blanco = 0;

			$('textarea.comentarios').each(function(){

				if ($(this).val().trim().length == 0){

					num_comentarios++;
				}

				if ($(this).parent().parent().prev().find('select option[value="nada"]').length > 0){

					preguntas_en_blanco++;
				}
			});

			if (num_comentarios > 0 || preguntas_en_blanco > 0){

				alertar = false;
				alert('El cuestionario no ha sido concluido correctamente, favor de llenar los campos y/o comentarios');
				window.reload();
			}
		}
	}, 100)

	window.onbeforeunload = function(e){ 
  
  if (!guardar){
    
  	alertar = true;
  	$(window).unbind();
    return undefined;
  }
}
	</script>
@endsection
