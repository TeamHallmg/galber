@extends('layouts.app')

@section('title', '¿Que es?')

@section('content')


	<div class="row">
		<div class="col-md-2 text-right">
			@include('evaluacion-desempeno/partials/sub-menu')
		</div>
		<div class="col-md-10">
			<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }}" alt="">
		</div>
	</div>
		
	<div class="row">
	 
		<div class="col-md-12 sub_menu">
			<div class="card mt-3">
				<h5 class="card-header bg-info text-white font-weight-bolder">¿Qué es?
	<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
					 
	</h5>
				<div class="card-body">
	   
			<p class="text-justify">La Evaluación Cualitativa, es una herramienta que apoyara a cada colaborador y su jefe inmediato a visualizar su crecimiento y áreas de oportunidad en las competencias y comportamientos que requiere su puesto dentro de GALBER.</p>
			<p class="text-justify">Para esta herramienta se seleccionan competencias las cuales se dividen en tres principales áreas: Generales que aplican para todos los colaboradores, de Mando que aplican a todos los colaboradores con personas a su cargo y las de área que son especificas de ciertos puestos.</p>
			<p class="text-justify">Esta evaluación será 360, lo que quiere decir que serás evaluado por tu jefe directo, compañeros de área, compañeros de otros departamentos con los cuales tienes interacción y colaboradores a tu cargo en caso de tenerlos.</p>
			<p class="text-justify">Recuerda el objetivo principal de esta evaluación es lograr el desarrollo de todo el equipo GALBER por lo que tu honestidad y participación se traducirán en crecimiento.</p>
		</div>
	</div>
	</div>
	</div>

	@if (!Session::has('primer_login'))

	<div class="row">
		<div class="col-md-12 text-center mt-3">
			<a class="btn btn-primary" href="/evaluaciones"><span class="fas fa-clipboard-list"></span> Evaluaciones</a>
		</div>
	</div>

@endif
@endsection

@section('scripts')

<script type="text/javascript">

	var sesion = <?php echo (Session::has('primer_login') ? '1' : '0')?>;

	$(document).ready(function(){

		if (sesion == 1){

			alert('Debes cambiar tu contraseña antes de seguir a cualquier otra sección');
		}
	});

</script>
@endsection