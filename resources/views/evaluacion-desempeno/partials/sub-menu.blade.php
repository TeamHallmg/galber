    <?php function activeSubMenu($url){
      return request()->is($url) ? 'active' : '';
    }?>

    @if (!Session::has('primer_login'))

	<h4 class="font-weight-bold">Evaluación Cualitativa</h4>
	<hr>
	<ul class="nav nav-pills flex-column">

		<li class="nav-item"><a class="nav-link {{ activeSubMenu('que-es-evaluacion-desempeno') }}" href="{{ url('que-es-evaluacion-desempeno') }}">¿Qué es?</a></li>
		<li class="nav-item"><a class="nav-link {{ activeSubMenu('evaluaciones') }}" href="{{ url('evaluaciones') }}">Evaluaciones</a></li>
		<li class="nav-item"><a class="nav-link {{ activeSubMenu('reporte-desempeno') }} {{ activeSubMenu('ponderaciones') }}" href="{{ url('reporte-desempeno') }}">Reportes</a></li>

		@if(auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin'))
		
		{{--<li class="nav-item"><a class="nav-link {{ activeSubMenu('reporte-consolidado') }}" href="{{ url('reporte-consolidado') }}">Reporte Consolidado</a></li>
		<li class="nav-item"><a class="nav-link {{ activeSubMenu('rol-colaborador-jefe') }}" href="{{ url('rol-colaborador-jefe') }}">Rol Colaborador a Jefe</a></li>--}}
		@endif

		@if(auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin') || auth()->user()->role == 'supervisor')
		
		
	<li class="nav-item"><h4 class="font-weight-bold mt-4  d-flex">Administrador 	
		<a class=" ml-1" data-toggle="collapse" href="#collapseVacationMenu" role="button" aria-expanded="false" aria-controls="collapseVacationMenu"><i class="fas fa-cog"></i></a>
	</h4><hr></li>

		<!--<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('importar-objetivos') }}" href="{{ url('importar-objetivos') }}">Importar Objetivos</a>
		</li>-->
		<div class="collapse {{ is_url_of_hidden_menu(Route::currentRouteName())?'show':'' }}" id="collapseVacationMenu">
 
 
			<li class="nav-item"><a class="nav-link {{ activeSubMenu('estadisticas') }}" href="{{ url('estadisticas') }}">Estado del Avance</a></li>
			<li class="nav-item"><a class="nav-link {{ activeSubMenu('lista-empleados') }} {{ activeSubMenu('cambiar-contrasena/*') }} {{ activeSubMenu('cambiar-jefe/*') }}" href="{{ url('lista-empleados') }}">Listado de Empleados</a></li>
			<!--<li><a href="{{ url('estadisticasAP') }}">Listado de Usuarios</a></li>-->
		
		<?php $periodos = DB::select("SELECT id FROM periodos WHERE status = ? OR status = ?", ['Preparatorio', 'Abierto']);

      	  // Hay periodos preparatorios
      	  if (count($periodos) > 0 && auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin')){ ?>
		
			<li class="nav-item"><a class="nav-link {{ activeSubMenu('matriz-evaluaciones/*') }} {{ activeSubMenu('Matriz-Evaluacion*') }} {{ activeSubMenu('importacion-evaluadores-csv*') }}" href="{{ url('matriz-evaluaciones/0') }}">Red de interacción</a></li>
		<?php } ?>
		
			<li class="nav-item"><a class="nav-link {{ activeSubMenu('tipos-factores') }} {{ activeSubMenu('tipos-factores/*') }}" href="{{ url('tipos-factores') }}">Tipos de Factores</a></li>
			<li class="nav-item"><a class="nav-link {{ activeSubMenu('factores') }} {{ activeSubMenu('factores/*') }}" href="{{ url('factores') }}">Competencias</a></li>
			<li class="nav-item"><a class="nav-link {{ activeSubMenu('etiquetas') }} {{ activeSubMenu('etiquetas/*') }}" href="{{ url('etiquetas') }}">Etiquetas</a></li>
			<li class="nav-item"><a class="nav-link {{ activeSubMenu('familias-factores') }}" href="{{ url('familias-factores') }}">Matriz de Competencias</a></li>
			<!--<li><a href="{{ url('catalogo-factores-comportamientos') }}">Catalogo de Factores</a></li>-->
			<li class="nav-item"><a class="nav-link {{ activeSubMenu('periodos') }} {{ activeSubMenu('periodos/*') }}"  href="{{ url('periodos') }}">Periodos</a></li>
			<li class="nav-item"><a class="nav-link {{ activeSubMenu('puestos') }} {{ activeSubMenu('puestos/*') }}" href="{{ url('puestos') }}">Niveles de Puestos</a></li>
			<li class="nav-item"><a class="nav-link {{ activeSubMenu('areas') }} {{ activeSubMenu('areas*') }}" href="{{ url('areas') }}">Grupos de Áreas</a></li>
			
			<!--<li><a href="{{ url('Matriz-Evaluacion') }}">Matriz de Evaluación</a></li>-->
			{{--<li><a href="{{ url('informe-consolidado') }}">Informe Consolidado</a></li>--}}
			</div>
			@endif
	</ul>

	@endif

	@if (auth()->user()->isSuperAdmin())

  <span class="font-weight-bold font-sepanka font-version-sepanka h4">20250130.YARM</span>
@endif