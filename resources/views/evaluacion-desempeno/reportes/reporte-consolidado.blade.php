@extends('layouts.app')

@section('title', 'Reporte Consolidado')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Reporte Consolidado</h3>
		<hr class="mb-5">

@if (!empty($periodos))

	<div class="col text-center mb-4">
		<div>Periodo: <select class="periodos">

			    <?php foreach ($periodos as $key => $periodo){ ?>
			                                      	
			              <option value="<?php echo $periodo->id?>" <?php if ($id_periodo == $periodo->id){ ?>selected="selected"<?php } ?>><?php echo $periodo->descripcion?></option>
			    <?php } ?>
								 																 
								 	</select>
		</div>
	</div>

@if (!empty($users))

<div class="row margin-top-20">
	<div class="col-md-12">
		<table width="100%" class="table table-striped table-bordered usuarios_informe">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th class="text-center">ID</th>
					<th>Nombre</th>
					<th>Puesto</th>
					<th>Departamento</th>
					<th>Jefe</th>
					<th class="text-center">Detalle</th>
				</tr>
			</thead>
			<tbody>

<?php $actual_usuario = 0;
      $clase_boton = 'btn-success'; ?>

	@for ($i = 0;$i < count($users);$i++)

  	@if ($actual_usuario != $users[$i]->id)

  		@if ($actual_usuario != 0)

  				<td class="text-center">
						<button type="button" class="btn {{$clase_boton}} informe" style="width: 100%">{{($clase_boton == 'btn-success' ? 'Ver Reporte' : ($clase_boton == 'btn-primary' ? 'Retro' : 'En Proceso'))}}</button>
						<form action="/reporte-consolidado/{{$actual_usuario}}" method="post" class="ver_reporte">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="id_periodo" class="id_periodo" value="<?php echo $id_periodo?>">
						@if ($clase_boton != 'btn-danger')
							<input type="hidden" name="terminado" value="1">
						@endif
							<button type="submit" style="display: none">Ver Reporte</button>
						</form>
					</td>
				</tr>
			@endif

				<tr>
					<td class="text-center">{{$users[$i]->id}}</td>
					<td class="text-left">{{str_replace('Ã‘','Ñ',$users[$i]->first_name)}} {{str_replace('Ã‘','Ñ',$users[$i]->last_name)}}</td>
					<td class="text-left">{{(!empty($users[$i]->subdivision) ? str_replace('Ã‘','Ñ',$users[$i]->subdivision) : '')}}</td>
					<td class="text-left">{{(!empty($users[$i]->division) ? str_replace('Ã‘','Ñ',$users[$i]->division) : '')}}</td>
					<td class="text-left">{{str_replace('Ã‘','Ñ',$users[$i]->boss)}}</td>

	<?php $actual_usuario = $users[$i]->id;
				$clase_boton = 'btn-success'; ?>

		@endif

<?php if ($users[$i]->status != 'Terminada'){

					$clase_boton = 'btn-danger';
			}

			else{
					
				if (!empty($users[$i]->comentario) && !empty($users[$i]->objetivo)){
					
					$clase_boton = 'btn-primary';
				}
			} ?>
					
	@endfor

	@if ($actual_usuario != 0)

  				<td class="text-center">
						<button type="button" class="btn {{$clase_boton}} informe" style="width: 100%">{{($clase_boton == 'btn-success' ? 'Ver Reporte' : ($clase_boton == 'btn-primary' ? 'Retro' : 'En Proceso'))}}</button>
						<form action="/reporte-consolidado/{{$actual_usuario}}" method="post" class="ver_reporte">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="id_periodo" class="id_periodo" value="<?php echo $id_periodo?>">
							<button type="submit" style="display: none">Ver Reporte</button>
						</form>
					</td>
				</tr>
	@endif

			</tbody>
		</table>
	</div>
</div>

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20 no_subordinados">No cuenta con colaboradores en la evaluación del periodo.</h4>
	</div>
</div>
@endif

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20">No hay periodos abiertos.</h4>
	</div>
</div>
@endif

<form action="/reporte-consolidado" method="post" class="reporte_consolidado">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="id_periodo" class="id_periodo_reporte">
	<input type="submit" style="display: none">
</form>
	</div>
</div>
@endsection

@section('scripts')
<script>
		
		$(document).ready(function(){

			$('.usuarios_informe').DataTable({
			language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
  	});

   		$('select.periodos').change(function(){

   			$('.id_periodo_reporte').val($(this).val());
   			$('form.reporte_consolidado').submit();
   		});

			$('body').on('click', 'button.informe', function(){

   			$('.id_periodo').val($('select.periodos').val());
   			$(this).next().submit();
   		});
		});
	</script>
@endsection
