@extends('layouts.app')

@section('title', 'Reporte Evaluación Cualitativa')

@section('content')
<?php $results = array();
			$factors = array();
			$results_a_comparar = array();
			$factors_a_comparar = array();
			$resultados_esperados = array(); ?>
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('/img/evaluacion_desempeno.png') }}" alt="">
		
		
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Reporte Evaluación Cualitativa
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
 
				 
<div class="row">
	<div class="col-md-12 text-center">
		<div id="myImageContainer">
			<img id="myImage">
		</div>
	@if ((!empty(auth()->user()->employee) && $user->employee_wt->jefe == auth()->user()->employee->idempleado) || Auth::user()->isAdminOrHasRolePermission('performance_evaluation_admin') || auth()->user()->role == 'supervisor' || auth()->user()->role == 'director')
		<button class="btn btn-primary print_report completo" <?php if (!Auth::user()->isAdminOrHasRolePermission('performance_evaluation_admin') && (count($comentario) == 0 || count($objetivos_entregables_desempeno) == 0)){ ?>style="display: none"<?php } ?>><i class="fas fa-print"></i> Imprimir Reporte Completo</button>
	@endif
	</div>
</div>
<div class="row" id="areaImprimir">
	<div class="text-center">
		<h1 class="text-center mostrar font-weight-bold" style="display: none; margin-top: 0px">Reporte Evaluación Cualitativa</h1>
	</div>
	<div class="col-md-12">
		<div class="row">
		<div class="col-md-8 datos_evaluado mt-5">
			<table class="table-fluid tabla_evaluado table table-bordered"  width="100%" style="font-size: 14px">
				<tbody>
					<tr>
						<td>EVALUADO:</td>
						<td class="text-center">
							<b>{{str_replace('Ã‘','Ñ',$user->first_name)}} {{str_replace('Ã‘','Ñ',$user->last_name)}}</b>
						</td>
					</tr>
					<tr>
						<td>NO. EMPLEADO:</td>
						<td class="text-center">
							<b>{{$user->employee_wt->idempleado}}</b>
						</td>
					</tr>
					<tr>
						<td>PUESTO:</td>
						<td class="text-center">
							<b>{{(!empty($user->employee_wt->jobPosition) ? str_replace('Ã‘','Ñ',$user->employee_wt->jobPosition->name) : '')}}</b>
						</td>
					</tr>
					<tr>
						<td>FECHA:</td>
						<td class="text-center">
							<b>{{$meses[date('m') * 1] . '-' . date('y')}}</b>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-4 text-center datos_logo mt-5">
			<img src="{{ asset('img/logo.png') }}" class="img-fluid" style="position: relative; top: -10px">
		</div>
		</div>
	</div>

	<div class="mt-5 col-md-12">

		@if (count($periodos) > 0)

		<div class="col-md-12 margin-top-20 oculto mb-5 text-center">
		@endif

			<form action="/reporte-desempeno/{{$user->id}}" method="post" class="form_periodo_a_comparar">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_periodo" value="{{$id_periodo}}">

		@if (count($periodos) > 0)

			<strong>PERIODO ANTERIOR A COMPARAR</strong> 	<select name="periodo_a_comparar" class="periodo_a_comparar">
											   		<option value="0">--Selecciona periodo--</option>

<?php foreach ($periodos as $key => $value){ ?>
										
												 		<option value="{{$value->id}}" <?php if (!empty($_POST['periodo_a_comparar']) && $_POST['periodo_a_comparar'] == $value->id){ ?>selected="selected"<?php } ?>>{{$value->descripcion}}</option>
<?php } ?>

													</select>
		@else
	
				<select name="periodo_a_comparar" class="periodo_a_comparar" style="display: none">
					<option value="0">--Selecciona periodo--</option>
				</select>
		@endif
			
			</form>

		@if (count($periodos) > 0)
	
		</div>
		@endif

		@if (count($resultados_a_comparar))

		<div class="row mb-5">
			<div class="col-md-5 margin-top-20 oculto">
				<table class="table-fluid" border="1" width="100%">
					<thead>
						<tr>
							<th>Competencias</th>
							<th class="text-center">Promedio</th>
						</tr>
					</thead>
					<tbody>

<?php $actual_factor = 0;
			$contador = 0;
			$contador_factores = 0;
			$total = 0;
			$total_factor = 0;
			$promedio = 0;
			$comentarios = array();

			foreach ($resultados_a_comparar as $key => $value){
			
				if ($actual_factor != $value->id_factor){
					
					$factors_a_comparar[] = $value->nombre;

					if ($actual_factor != 0){

						$contador_factores++;

						if ($contador == 0){

							$contador = 1;
						}

						$promedio = $total_factor / $contador;
						$total += $promedio;
						$results_a_comparar[] = number_format($promedio, 2, '.', ',') * 1; ?>

							<td class="text-center" style="color: white; background-color: {{($promedio >= 3.5 ? '#008000' : ($promedio >= 3 ? '#73CB77' : ($promedio >= 2.5 ? '#FFFF00; color: black' : ($promedio >= 2 ? '#FF4343' : '#C00000'))))}}"><b>{{number_format($promedio, 2, '.', ',')}}</b></td>
						</tr>
		<?php }

					$contador = 0;
					$actual_factor = $value->id_factor;
					$total_factor = 0; ?>

						<tr>
							<td>{{$value->nombre}}</td>
	<?php }

				if ((empty($value->nombre_evaluacion) || $value->nombre_evaluacion != 'SUBORDINADO') && $value->jefe != $user->employee_wt->idempleado){

					$total_factor += $value->nivel_desempeno;
					$contador++;
				}

				else{

					//$user_id = auth()->user()->id;

					//if ($user_id == 26 || $user_id == 1){

						$total_factor += $value->nivel_desempeno;
						$contador++;
					//}
				}
			}

			if ($actual_factor != 0){

				$contador_factores++;

				if ($contador == 0){

					$contador = 1;
				}

				$promedio = $total_factor / $contador;
				$results_a_comparar[] = number_format($promedio, 2, '.', ',') * 1;
				$total += $promedio; ?>

							<td class="text-center" style="color: white; background-color: {{($promedio >= 3.5 ? '#008000' : ($promedio >= 3 ? '#73CB77' : ($promedio >= 2.5 ? '#FFFF00; color: black' : ($promedio >= 2 ? '#FF4343' : '#C00000'))))}}"><b>{{number_format($promedio, 2, '.', ',')}}</b></td>
						</tr>
<?php }

			if ($contador_factores == 0){

				$contador_factores = 1;
			}

			$promedio = $total / $contador_factores; ?>

						<tr>
							<td>RESULTADO GLOBAL</td>
							<td class="text-center" style="color: white; background-color: {{($promedio >= 3.5 ? '#008000' : ($promedio >= 3 ? '#73CB77' : ($promedio >= 2.5 ? '#FFFF00; color: black' : ($promedio >= 2 ? '#FF4343' : '#C00000'))))}}"><b>{{number_format($total / $contador_factores, 2, '.', ',')}}</b></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-7 margin-top-20 oculto">
				<h4 class="text-center titulos-evaluaciones font-weight-bold" style="margin-top: 0">Resultados Evaluación Cualitativa</h4>
				<div id="container_a_comparar" style="width: 100%; margin: 0 auto"></div>
			</div>
		</div>
		@endif

		@if (count($resultados_a_comparar) > 0)

		<div class="row margin-top-20 oculto">
			<div class="col-md-12 mb-5 text-center">
				<strong>PERIODO ACTUAL</strong>
			</div>
		</div>
		@endif


		<div class="row mb-5">
			<div class="col-md-12 datos_competencias">
				<table class="table-fluid tabla_competencias table table-bordered" width="100%">
					<thead>
						<tr>
							<th>Competencias</th>
							<th class="text-center">Porcentaje Alcanzado</th>
							<th class="text-center">Esperado</th>
						</tr>
					</thead>
					<tbody>

<?php $actual_factor = 0;
		$contador = 0;
		$contador_factores = 0;
		$total = 0;
		$total_factor = 0;
		$promedio = 0;
		$comentarios = array();
		$total_peso = 0;
		$autoevaluacion = $jefe = $colaborador = $par = $cliente = 0;
		$contador_colaborador = $contador_par = $contador_cliente = 0;
		$final_autoevaluacion = $final_jefe = $final_colaborador = $final_par = $final_cliente = 0;
		$contador_final_autoevaluacion = $contador_final_jefe = $contador_final_colaborador = $contador_final_par = $contador_final_cliente = 0;

		foreach ($resultados as $key => $value){
			
			if ($actual_factor != $value->id_factor){

				$resultados_esperados[] = 100;
					
				$factors[] = $value->nombre;

				if ($actual_factor != 0){

					$contador_factores++;

					if ($contador == 0){

						$contador = 1;
					}

					$promedio_sin_ponderacion = $total_factor / $contador;
					//$promedio = $promedio * 100 / 5;
					$ponderacion_a_usar = 0;
					$total_sin_ponderacion = 0;
					$contador_sin_ponderacion = 0;
					$promedio = 0;

					if ($autoevaluacion != 0){

						$final_autoevaluacion += $autoevaluacion;
						$contador_final_autoevaluacion++;

						if (isset($ponderaciones->autoevaluacion) && $ponderaciones->autoevaluacion != 0){

							$ponderacion_a_usar += $ponderaciones->autoevaluacion;
							$promedio += $autoevaluacion * $ponderaciones->autoevaluacion / 100;
						}

						else{

							$total_sin_ponderacion += $autoevaluacion;
							$contador_sin_ponderacion++;
						}
					}

					if ($jefe != 0){

						$final_jefe += $jefe;
						$contador_final_jefe++;

						if (isset($ponderaciones->jefe) && $ponderaciones->jefe != 0){

							$ponderacion_a_usar += $ponderaciones->jefe;
							$promedio += $jefe * $ponderaciones->jefe / 100;
						}

						else{

							$total_sin_ponderacion += $jefe;
							$contador_sin_ponderacion++;
						}
					}

					if ($contador_par != 0){

						$final_par += $par;
						$contador_final_par += $contador_par;

						if (isset($ponderaciones->par) && $ponderaciones->par != 0){

							$ponderacion_a_usar += $ponderaciones->par;
							$promedio += ($par / $contador_par) * $ponderaciones->par / 100;
						}

						else{

							$total_sin_ponderacion += $par / $contador_par;
							$contador_sin_ponderacion++;
						}
					}

					if ($contador_colaborador != 0){

						$final_colaborador += $colaborador;
						$contador_final_colaborador += $contador_colaborador;

						if (isset($ponderaciones->colaborador) && $ponderaciones->colaborador != 0){

							$ponderacion_a_usar += $ponderaciones->colaborador;
							$promedio += ($colaborador / $contador_colaborador) * $ponderaciones->colaborador / 100;
						}

						else{

							$total_sin_ponderacion += $colaborador / $contador_colaborador;
							$contador_sin_ponderacion++;
						}
					}

					if ($contador_cliente != 0){

						$final_cliente += $cliente;
						$contador_final_cliente += $contador_cliente;

						if (isset($ponderaciones->psicometricos) && $ponderaciones->psicometricos != 0){

							$ponderacion_a_usar += $ponderaciones->psicometricos;
							$promedio += ($cliente / $contador_cliente) * $ponderaciones->psicometricos / 100;
						}

						else{

							$total_sin_ponderacion += $cliente / $contador_cliente;
							$contador_sin_ponderacion++;
						}
					}

					if ($total_sin_ponderacion != 0){

						$total_sin_ponderacion = $total_sin_ponderacion / $contador_sin_ponderacion;
						$ponderacion_restante = 100 - $ponderacion_a_usar;
						$promedio += $total_sin_ponderacion * $ponderacion_restante / 100;
					}

					else{

						if ($ponderacion_a_usar < 100){

							$ponderacion_restante = 100 - $ponderacion_a_usar;
							$promedio += $ponderacion_restante;
						}
					}

					//$total_peso += $pesos_factores[$actual_factor];
					/*$promedio = $colaborador + $par + $cliente;
					$promedio = $promedio / ($contador_par + $contador_colaborador + $contador_cliente);
					$promedio = $promedio * 71 / 100;
					$promedio = $promedio + $autoevaluacion + $jefe;*/
					//$results[] = number_format($promedio_sin_ponderacion, 2, '.', ',') * 1;
					$results[] = number_format($promedio, 2, '.', ',') * 1;
					//$total += $promedio * $pesos_factores[$actual_factor] / 105;
					$total += $promedio; ?>

							<td class="text-center" style="color: white; background-color: {{($promedio >= 80 ? '#008000' : ($promedio >= 60 ? '#73CB77' : ($promedio >= 40 ? '#FFFF00; color: black' : ($promedio >= 20 ? '#FF4343' : '#C00000'))))}}"><b>{{number_format($promedio, 2, '.', ',')}}</b></td>

					@if (in_array($actual_factor, $factores_con_nivel_de_puesto))

							<td class="text-center" style="color: white; background-color: #008000">
								<b>100</b>
							</td>
					@else

							<td class="text-center" style="color: white; background-color: black">
								<b>N/A</b>
							</td>
					@endif

						</tr>
	<?php }

				$contador = 0;
				$actual_factor = $value->id_factor;
				$total_factor = 0;
				$promedio = 0;
				$autoevaluacion = $jefe = $colaborador = $par = $cliente = 0;
				$contador_colaborador = $contador_par = $contador_cliente = 0; ?>

						<tr>
							<td>{{$value->nombre}}</td>
<?php }

			if ($user->id == $value->id_evaluador){

				//$autoevaluacion = $value->nivel_desempeno * $ponderaciones->autoevaluacion / 100;
				$autoevaluacion = $value->nivel_desempeno;
			}

			else{

				if ($value->nombre_evaluacion == 'JEFE' || $employee->jefe == $value->idempleado){

					//$jefe = $value->nivel_desempeno * $ponderaciones->jefe / 100;
					$jefe = $value->nivel_desempeno;
				}

				else{

					if ($modalidad > 2 && ($value->nombre_evaluacion == 'SUBORDINADO' || $employee->idempleado == $value->jefe)){

						$colaborador += $value->nivel_desempeno;
						$contador_colaborador++;
					}

					else{

						if ($modalidad > 1 && ($value->nombre_evaluacion == 'PARES' || $employee->jefe == $value->jefe)){

							$par += $value->nivel_desempeno;
							$contador_par++;
						}

						else{
								
							if ($modalidad == 4){

								$cliente += $value->nivel_desempeno;
								$contador_cliente++;
							}
						}
					}
				}
			}

			$total_factor += $value->nivel_desempeno;
			$contador++;

			if (!empty($value->comentario)){

	      if (empty($comentarios[$value->nombre])){

	      	$comentarios[$value->nombre] = $value->comentario;
	      }

	      else{

	      	$comentarios[$value->nombre] .= '*' . $value->comentario;
	      }
	    }
		}

		if ($actual_factor != 0){

			$contador_factores++;

			if ($contador == 0){

				$contador = 1;
			}

			$promedio_sin_ponderacion = $total_factor / $contador;
			$ponderacion_a_usar = 0;
			$total_sin_ponderacion = 0;
			$contador_sin_ponderacion = 0;
			$promedio = 0;

					if ($autoevaluacion != 0){

						$final_autoevaluacion += $autoevaluacion;
						$contador_final_autoevaluacion++;

						if (isset($ponderaciones->autoevaluacion) && $ponderaciones->autoevaluacion != 0){

							$ponderacion_a_usar += $ponderaciones->autoevaluacion;
							$promedio += $autoevaluacion * $ponderaciones->autoevaluacion / 100;
						}

						else{

							$total_sin_ponderacion += $autoevaluacion;
							$contador_sin_ponderacion++;
						}
					}

					if ($jefe != 0){

						$final_jefe += $jefe;
						$contador_final_jefe++;

						if (isset($ponderaciones->jefe) && $ponderaciones->jefe != 0){

							$ponderacion_a_usar += $ponderaciones->jefe;
							$promedio += $jefe * $ponderaciones->jefe / 100;
						}

						else{

							$total_sin_ponderacion += $jefe;
							$contador_sin_ponderacion++;
						}
					}

					if ($contador_par != 0){

						$final_par += $par;
						$contador_final_par += $contador_par;

						if (isset($ponderaciones->par) && $ponderaciones->par != 0){

							$ponderacion_a_usar += $ponderaciones->par;
							$promedio += ($par / $contador_par) * $ponderaciones->par / 100;
						}

						else{

							$total_sin_ponderacion += $par / $contador_par;
							$contador_sin_ponderacion++;
						}
					}

					if ($contador_colaborador != 0){

						$final_colaborador += $colaborador;
						$contador_final_colaborador += $contador_colaborador;

						if (isset($ponderaciones->colaborador) && $ponderaciones->colaborador != 0){

							$ponderacion_a_usar += $ponderaciones->colaborador;
							$promedio += ($colaborador / $contador_colaborador) * $ponderaciones->colaborador / 100;
						}

						else{

							$total_sin_ponderacion += $colaborador / $contador_colaborador;
							$contador_sin_ponderacion++;
						}
					}

					if ($contador_cliente != 0){

						$final_cliente += $cliente;
						$contador_final_cliente += $contador_cliente;

						if (isset($ponderaciones->psicometricos) && $ponderaciones->psicometricos != 0){

							$ponderacion_a_usar += $ponderaciones->psicometricos;
							$promedio += ($cliente / $contador_cliente) * $ponderaciones->psicometricos / 100;
						}

						else{

							$total_sin_ponderacion += $cliente / $contador_cliente;
							$contador_sin_ponderacion++;
						}
					}

					if ($total_sin_ponderacion != 0){

						$total_sin_ponderacion = $total_sin_ponderacion / $contador_sin_ponderacion;
						$ponderacion_restante = 100 - $ponderacion_a_usar;
						$promedio += $total_sin_ponderacion * $ponderacion_restante / 100;
					}

					else{

						if ($ponderacion_a_usar < 100){

							$ponderacion_restante = 100 - $ponderacion_a_usar;
							$promedio += $ponderacion_restante;
						}
					}
			
			//$total_peso += $pesos_factores[$actual_factor];
			/*$promedio = $colaborador + $par + $cliente;
			$promedio = $promedio / ($contador_par + $contador_colaborador + $contador_cliente);
			$promedio = $promedio * 71 / 100;
			$promedio = $promedio + $autoevaluacion + $jefe;*/
			$results[] = number_format($promedio, 2, '.', ',') * 1;
			//$total += $promedio * $pesos_factores[$actual_factor] / 105;
			$total += $promedio; ?>

							<td class="text-center" style="color: white; background-color: {{($promedio >= 80 ? '#008000' : ($promedio >= 60 ? '#73CB77' : ($promedio >= 40 ? '#FFFF00; color: black' : ($promedio >= 20 ? '#FF4343' : '#C00000'))))}}"><b>{{number_format($promedio, 2, '.', ',')}}</b></td>

			@if (in_array($actual_factor, $factores_con_nivel_de_puesto))

							<td class="text-center" style="color: white; background-color: #008000">
								<b>100</b>
							</td>
			@else

							<td class="text-center" style="color: white; background-color: black">
								<b>N/A</b>
							</td>
			@endif

						</tr>
<?php }

		if ($contador_factores == 0){

			$contador_factores = 1;
		}

		$alto_grafica = 40 * (1 + $contador_factores);
		$promedio = $total / $contador_factores;
		$ponderacion_a_usar = 0;
    $total_sin_ponderacion = 0;
    $contador_sin_ponderacion = 0;
    $promedio = 0;

    if ($final_autoevaluacion != 0){

      if (isset($ponderaciones->autoevaluacion) && $ponderaciones->autoevaluacion != 0){

        $ponderacion_a_usar += $ponderaciones->autoevaluacion;
        $promedio += ($final_autoevaluacion / $contador_final_autoevaluacion) * $ponderaciones->autoevaluacion / 100;
      }

      else{

        $total_sin_ponderacion += $final_autoevaluacion / $contador_final_autoevaluacion;
        $contador_sin_ponderacion++;
      }
    }

  	if ($final_jefe != 0){

    	if (isset($ponderaciones->jefe) && $ponderaciones->jefe != 0){

        $ponderacion_a_usar += $ponderaciones->jefe;
        $promedio += ($final_jefe / $contador_final_jefe) * $ponderaciones->jefe / 100;
      }

     	else{

        $total_sin_ponderacion += $final_jefe / $contador_final_jefe;
        $contador_sin_ponderacion++;
      }
    }

    if ($final_par != 0){

     	if (isset($ponderaciones->par) && $ponderaciones->par != 0){

        $ponderacion_a_usar += $ponderaciones->par;
        $promedio += ($final_par / $contador_final_par) * $ponderaciones->par / 100;
      }

    	else{

      	$total_sin_ponderacion += $final_par / $contador_final_par;
      	$contador_sin_ponderacion++;
    	}
    }

    if ($final_colaborador != 0){

      if (isset($ponderaciones->colaborador) && $ponderaciones->colaborador != 0){

        $ponderacion_a_usar += $ponderaciones->colaborador;
        $promedio += ($final_colaborador / $contador_final_colaborador) * $ponderaciones->colaborador / 100;
      }

      else{

        $total_sin_ponderacion += $final_colaborador / $contador_final_colaborador;
        $contador_sin_ponderacion++;
      }
    }

    if ($final_cliente != 0){

      if (isset($ponderaciones->psicometricos) && $ponderaciones->psicometricos != 0){

        $ponderacion_a_usar += $ponderaciones->psicometricos;
        $promedio += ($final_cliente / $contador_final_cliente) * $ponderaciones->psicometricos / 100;
      }

      else{

        $total_sin_ponderacion += $final_cliente / $contador_final_cliente;
        $contador_sin_ponderacion++;
     	}
    }

    if ($total_sin_ponderacion != 0){

      $total_sin_ponderacion = $total_sin_ponderacion / $contador_sin_ponderacion;
      $ponderacion_restante = 100 - $ponderacion_a_usar;
      $promedio += $total_sin_ponderacion * $ponderacion_restante / 100;
    }

    else{

			if ($ponderacion_a_usar < 100){

				$ponderacion_restante = 100 - $ponderacion_a_usar;
				$promedio += $ponderacion_restante;
			}
		}
		//$promedio = $promedio * 105 / 100;
		//$promedio = $total ?>

						<tr>
							<td>RESULTADO GLOBAL</td>
							<td class="text-center" style="color: white; background-color: {{($promedio >= 80 ? '#008000' : ($promedio >= 60 ? '#73CB77' : ($promedio >= 40 ? '#FFFF00; color: black' : ($promedio >= 20 ? '#FF4343' : '#C00000'))))}}"><b>{{number_format($promedio, 2, '.', ',')}}</b></td>
							<td class="text-center" style="color: white; background-color: #008000"><b>105</b></td>
						</tr>
						<tr>
							<td>NIVEL ALCANZADO</td>
							<td class="text-center">
								<button class="btn btn-primary btn-sm" style="cursor: default; width: 100%; font-size: 25px">

							@foreach ($etiquetas as $key => $value)
							
								@if ($promedio >= $value->valor)

									{{$value->name}}

						<?php break; ?>
								@endif
							@endforeach
							
								</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-12 mt-4 datos_grafica">
				<h4 class="text-center titulos-evaluaciones font-weight-bold" style="margin-top: 0">Resultados Evaluación Cualitativa</h4>
				<div id="container" style="width: 100%; margin: 0 auto"></div>
			</div>
		</div>

		@if (!empty($comentarios))

		<div class="col-md-12 oculto2 mb-5">
			<div class="text-center titulos-evaluaciones font-weight-bold" style="font-size: 20px">Comentarios u Observaciones</div>
			<table class="table-fluid comentarios_reporte" border="1" width="100%">
				<tbody>

<?php $contador = 0;

			foreach ($comentarios as $key => $value){ ?>

					<tr>

				@if ($contador == 0)

						<td rowspan="{{count($comentarios)}}" class="text-center">
							<h4>COMPETENCIAS</h4>
						</td>
				@endif

	<?php $contador++?>

						<td class="text-center">{{$contador}}</td>
						<td class="text-center">{{$key}}</td>
						<td class="text-center">

	<?php $comments = explode('*', $value)?>

				@for ($i = 0;$i < count($comments);$i++)	

							<p style="margin: 5px 0; {{($i > 0 ? 'border-top: 1px solid gray' : '')}}">{{$comments[$i]}}</p>
				@endfor

						</td>
					</tr>
<?php	} ?>

				</tbody>
			</table>
		</div>
		@endif

		<div style="display: none">
		<form action="/guardar-comentarios-reporte" method="post" target="response" class="comentarios_reporte">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id_evaluado" value="{{$user->id}}">
			<input type="hidden" name="id_periodo" value="{{$id_periodo}}">
			<div class="col-md-12 my-4 oculto2">
				<div class="text-center titulos-evaluaciones font-weight-bold" style="font-size: 20px">Retroalimentación</div>
				<textarea style="width: 100%; height: 100px; resize: none;" name="comentarios_adicionales" <?php if (empty($user->employee_wt->boss) || ($user->employee_wt->boss->user->id != auth()->user()->id) || !$evaluaciones_terminadas){ ?>readonly="readonly"<?php } ?> class="retro">{{(count($comentario) > 0 ? $comentario[0]->comentario : '')}}</textarea>

		@if (!empty($user->employee_wt->boss) && $user->employee_wt->boss->user->id == auth()->user()->id && $evaluaciones_terminadas)

				<div class="text-center oculto" style="font-size: 13px; color: red">
					<strong>* Este campo es obligatorio para poder imprimir el reporte.</strong>
				</div>
		@endif

			</div>
			<div class="col-md-12">
				<div class="text-center titulos-evaluaciones objetivos_entregables font-weight-bold" style="font-size: 20px">Competencias y acciones a realizar</div>
				<p class="text-center">¿Qué competencias y/o comportamientos relacionados con mi puesto de trabajo necesito mejorar? ¿Qué acciones específicas se pueden realizar para lograrlo?</p>
				<table class="table-fluid objetivos_entregables" border="1" width="100%">
					<thead>
						<tr>
							<th class="text-center" width="50%">Nombre de la competencia</th>
							<th class="text-center" width="50%">Acciones</th>
						</tr>
					</thead>
					<tbody>

<?php foreach ($objetivos_entregables_desempeno as $key => $value){ ?>

						<tr>
							<td>

			@if ($user->employee_wt->boss->user->id != auth()->user()->id || !$evaluaciones_terminadas)

								<textarea style="width: 100%;height: 100%;resize: none;border: none;overflow: auto;outline: none;" readonly="readonly" class="retro">{{$value->objetivo}}</textarea>
			@else

								<select class="form-control oculto" style="width: 100%" name="objetivo{{$key + 1}}">

									<option value="0">-- Selecciona Competencia --</option>

				@foreach ($competencias as $key2 => $competencia)
						
									<option value="{{$competencia->nombre}}" <?php if ($competencia->nombre == $value->objetivo){ ?>selected="selected"<?php } ?>>{{$competencia->nombre}}</option>
				@endforeach		

								</select>
								<textarea style="width: 100%;height: 100%;resize: none;border: none;overflow: auto;outline: none;display: none;padding-left: 10px" class="mostrar">{{$value->objetivo}}</textarea>
			@endif

							</td>
							<td>
								<textarea style="width: 100%;height: 100%;resize: none;border: none;overflow: auto;outline: none;padding-left: 10px" name="accion{{$key + 1}}" <?php if ($user->employee_wt->boss->user->id != auth()->user()->id || !$evaluaciones_terminadas){ ?>readonly="readonly"<?php } ?> class="retro">{{$value->accion}}</textarea>
							</td>
						</tr>
<?php }

		for ($i = count($objetivos_entregables_desempeno) + 1; $i < 6;$i++){ ?>

						<tr>
							<td>

			@if (empty($user->employee_wt->boss) || $user->employee_wt->boss->user->id != auth()->user()->id || !$evaluaciones_terminadas)

								<textarea style="width: 100%;height: 100%;resize: none;border: none;overflow: auto;outline: none;" readonly="readonly" class="retro"></textarea>
			@else

								<select class="form-control oculto" style="width: 100%" name="objetivo{{$i}}">

									<option value="0">-- Selecciona Competencia --</option>

				@foreach ($competencias as $key => $competencia)
						
									<option value="{{$competencia->nombre}}">{{$competencia->nombre}}</option>
				@endforeach		

								</select>
			@endif
						
							</td>
							<td>
								<textarea style="width: 100%;height: 100%;resize: none;border: none;overflow: auto;outline: none;" name="accion{{$i}}" <?php if (empty($user->employee_wt->boss) || $user->employee_wt->boss->user->id != auth()->user()->id || !$evaluaciones_terminadas){ ?>readonly="readonly"<?php } ?> class="retro"></textarea>
							</td>
						</tr>
<?php } ?>

					</tbody>
				</table>
			</div>

<?php if (!empty($user->employee_wt->boss) && $user->employee_wt->boss->user->id == auth()->user()->id  && $evaluaciones_terminadas){ ?>

			<div style="color: red; font-size: 13px" class="text-center oculto">
				<b>* Es necesario indicar mínimo una competencia a mejorar para poder imprimir el reporte.</b>
			</div>
			<div class="col-md-12 margin-top-20 text-center oculto">
				<input type="submit" class="btn btn-primary" value="Guardar Comentario y Objetivos">
			</div>
<?php } ?>

		</form></div>
		<div class="row mt-5">
			<div class="col-md-6 col-sm-6 text-center datos_firmas">
				<div style="width: 300px; margin: 70px auto 0 auto; border-top: 2px solid black; font-size: 12px">
					<div>{{str_replace('Ã‘','Ñ',$user->first_name)}} {{str_replace('Ã‘','Ñ',$user->last_name)}}</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 text-center datos_firmas">
				<div style="width: 300px; margin: 70px auto 0 auto; border-top: 2px solid black; font-size: 12px">
					<div>{{(!empty($user->employee_wt->boss) ? str_replace('Ã‘','Ñ',$user->employee_wt->boss->user->first_name) . ' ' . str_replace('Ã‘','Ñ',$user->employee_wt->boss->user->last_name) : '')}}</div>
				</div>
			</div>
		</div>
	</div>

</div>
<iframe frameborder="0" width="0" height="0" name="response"></iframe>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.5/bluebird.min.js"></script>

</div>
</div>
@endsection

@section('scripts')
	<script type="text/javascript">

		var resultados = <?php echo json_encode($resultados)?>;
		var factors = <?php echo json_encode($factors)?>;
		var resultados_esperados = <?php echo json_encode($resultados_esperados)?>;
		var results = <?php echo json_encode($results)?>;
		var resultados_a_comparar = <?php echo json_encode($resultados_a_comparar)?>;
		var factors_a_comparar = <?php echo (!empty($factors_a_comparar) ? json_encode($factors_a_comparar) : '0')?>;
		var results_a_comparar = <?php echo (!empty($results_a_comparar) ? json_encode($results_a_comparar) : '0')?>;
		var tipos_evaluadores = <?php echo json_encode($tipos_evaluadores)?>;
		var ponderaciones = <?php echo json_encode($ponderaciones)?>;
		var user = <?php echo json_encode($user)?>;
		var employee = <?php echo json_encode($employee)?>;
		var alertar = false;
		var abandonar_pagina = false;
		var canEdit = <?php echo (!empty($user->employee_wt->boss) && $user->employee_wt->boss->user->id == auth()->user()->id && $evaluaciones_terminadas ? '1' : '0')?>;
		var modalidad = <?php echo $modalidad?>;
		var modalidad_comparar = <?php echo $modalidad_comparar?>;
		var id = <?php echo auth()->user()->id?>;

		$(document).ready(function(){

			var autoevaluacion = [], jefe = [], colaborador = [], par = [], cliente = [];
			var contador_autoevaluacion = 0, contador_jefe = 0, contador_colaborador = 0, contador_par = 0, contador_cliente = 0;
			var factor_colaborador = '', factor_par = '', factor_cliente = '', factor_jefe = '';
			var evaluciones_colaborador = 0, evaluciones_par = 0, evaluaciones_cliente = 0, evaluaciones_jefe = 0, total_colaborador = 0, total_par = 0, total_cliente = 0, total_jefe = 0;

			if (window.navigator.userAgent.indexOf("Edge") > -1){

				$('table.objetivos_entregables th, table.objetivos_entregables textarea').css('border', '1px solid grey');
				$('table.objetivos_entregables').removeAttr('border');
			}

			if ($('div.datos_firmas').length != 0){

			for(var i = 0;i < resultados.length;i++){

				if (user.id == resultados[i].id_evaluador){

					while (resultados[i].nombre != factors[contador_autoevaluacion]){

						autoevaluacion[contador_autoevaluacion] = 0;
						contador_autoevaluacion++;
					}

					autoevaluacion[contador_autoevaluacion] = resultados[i].nivel_desempeno * 1;
					contador_autoevaluacion++;
				}

				else{

					if (resultados[i].nombre_evaluacion == 'JEFE' || employee.jefe == resultados[i].idempleado){

						if (factor_jefe == resultados[i].nombre){

							contador_jefe--;
							evaluaciones_jefe++;
							total_jefe += resultados[i].nivel_desempeno * 1;
							jefe[contador_jefe] = (total_jefe / evaluaciones_jefe).toFixed(1) * 1;
							contador_jefe++;
						}

						else{

							while (resultados[i].nombre != factors[contador_jefe]){

								jefe[contador_jefe] = 0;
								contador_jefe++;
							}

							total_jefe = resultados[i].nivel_desempeno * 1
							jefe[contador_jefe] = total_jefe;
							factor_jefe = resultados[i].nombre;
							contador_jefe++;
							evaluaciones_jefe = 1;
						}
					}

					else{

						if (modalidad > 2 && (resultados[i].nombre_evaluacion == 'SUBORDINADO' || employee.idempleado == resultados[i].jefe)){

							if (factor_colaborador == resultados[i].nombre){

								contador_colaborador--;
								evaluciones_colaborador++;
								total_colaborador += resultados[i].nivel_desempeno * 1;
								colaborador[contador_colaborador] = (total_colaborador / evaluciones_colaborador).toFixed(1) * 1;
								contador_colaborador++;
							}

							else{

								while (resultados[i].nombre != factors[contador_colaborador]){

									colaborador[contador_colaborador] = 0;
									contador_colaborador++;
								}
								
								total_colaborador = resultados[i].nivel_desempeno * 1;
								colaborador[contador_colaborador] = total_colaborador;
								factor_colaborador = resultados[i].nombre;
								contador_colaborador++;
								evaluciones_colaborador = 1;
							}
						}

						else{

							if (modalidad > 1 && (resultados[i].nombre_evaluacion == 'PARES' || employee.jefe == resultados[i].jefe)){
								
								if (factor_par == resultados[i].nombre){

									contador_par--;
									evaluciones_par++;
									total_par += resultados[i].nivel_desempeno * 1;
									par[contador_par] = (total_par / evaluciones_par).toFixed(1) * 1;
									contador_par++;
								}

								else{

									while (resultados[i].nombre != factors[contador_par]){

										par[contador_par] = 0;
										contador_par++;
									}
									
									total_par = resultados[i].nivel_desempeno * 1;
									par[contador_par] = total_par;
									factor_par = resultados[i].nombre;
									contador_par++;
									evaluciones_par = 1;
								}
							}

							else{

								if (modalidad == 4){

									if (factor_cliente == resultados[i].nombre){

										contador_cliente--;
										evaluaciones_cliente++;
										total_cliente += resultados[i].nivel_desempeno * 1;
										cliente[contador_cliente] = (total_cliente / evaluaciones_cliente).toFixed(1) * 1;
										contador_cliente++;
									}

									else{

										while (resultados[i].nombre != factors[contador_cliente]){

											cliente[contador_cliente] = 0;
											contador_cliente++;
										}
									
										total_cliente = resultados[i].nivel_desempeno * 1;
										cliente[contador_cliente] = total_cliente;
										factor_cliente = resultados[i].nombre;
										contador_cliente++;
										evaluaciones_cliente = 1;
									}
								}
							}
						}
					}
				}
			}

			var i = 0;

			for (i = contador_autoevaluacion;i < factors.length;i++){

				autoevaluacion[i] = 0;
			}

			for (i = contador_jefe;i < factors.length;i++){

				jefe[i] = 0;
			}

			for (i = contador_colaborador;i < factors.length;i++){

				colaborador[i] = 0;
			}

			for (i = contador_par;i < factors.length;i++){

				par[i] = 0;
			}

			for (i = contador_cliente;i < factors.length;i++){

				cliente[i] = 0;
			}

			/*for (var i = contador_cliente;i < factors.length;i++){

				cliente[i] = 0;
			}*/
   
   		var chart = {
      	renderTo: 'container',
      	options3d: {
        	enabled: true,
        	alpha: 0,
        	beta: 0,
        	depth: 50,
        	viewDistance: 25
      	}
   		};

   		var title = {
      	text: ''   
   		};

   		var legend = {
        align: 'center',
        verticalAlign: 'bottom',
        layout: 'horizontal',
				floating: true,
				y: 34
      };
   
   		var plotOptions = {
      	column: {
        	depth: 25
      	}
   		};

    	var series = [{
      	name: 'PROMEDIO',
            data: results,
            color: '#004A91',
			type: 'column',
        },{
      	name: 'ESPERADO',
            data: resultados_esperados,
            color: 'green',
			type: 'column',
        },{
            name: tipos_evaluadores[1],
            data: autoevaluacion,
            //color: '#C00000',
            color: '#000000',
			type: 'line',
        },{
        	name: tipos_evaluadores[2],
            data: jefe,
            //color: '#008000',
            color: '#D2691E',
			type: 'line',
        }
      ];
      
      if (modalidad > 2){

      	series.push({name: tipos_evaluadores[5], data: colaborador, color: 'purple', type: 'line'});
      }

      if (modalidad > 1){

      	series.push({name: tipos_evaluadores[3], data: par, color: '#A020F0', type: 'line'});
      }

      series.push({name: tipos_evaluadores[4], data: cliente, color: '#FF4343', type: 'line'});

      /*if (modalidad == 4){

      	series.push({name: 'CLIENTE', data: cliente, color: '#008000', type: 'line'});
      }*/
        /*,{
        	name: 'COLABORADOR',
            data: colaborador,
            color: '#FFA500',
			type: 'line',
        },{
        	name: 'PAR',
          data: par,
          color: '#A020F0',
		  type: 'line',
        }*/
		
			var yAxis = [{
				labels: {
        	style: {
          	fontSize:'12px',
          	fontWeight:'bold'
        	}
      	},
      	max: 105,
      	tickInterval: 10
			}];

   		var xAxis = [{
      	categories: factors,
	  		labels: {
          style: {
            fontSize:'8px',
            fontWeight:'bold'
          },
          step: 1
        },
    	}];     
      
   		var json = {};
   		json.chart = chart;
   		json.title = title;
   		json.series = series;
   		json.plotOptions = plotOptions;
   		json.xAxis = xAxis;
			json.yAxis = yAxis;
			json.legend = legend;
   		highchart = new Highcharts.Chart(json);
   		//var alto = (alto_grafica > 210 ? alto_grafica : 210);

   		// Se seleccionó un periodo a comparar... se hacen los cálculos para mostrar la segunda gráfica
			if (factors_a_comparar != '0'){

   			var autoevaluacion = [], jefe = [], colaborador = [], par = [], cliente = [];
				var contador_autoevaluacion = 0, contador_jefe = 0, contador_colaborador = 0, contador_par = 0, contador_cliente = 0;
				var factor_colaborador = '', factor_par = '', factor_cliente = '', factor_jefe = '';
				var evaluciones_colaborador = 0, evaluciones_par = 0, evaluaciones_cliente = 0, evaluaciones_jefe = 0, total_colaborador = 0, total_par = 0, total_cliente = 0, total_jefe = 0;

				for(var i = 0;i < resultados_a_comparar.length;i++){

					if (user.id == resultados_a_comparar[i].id_evaluador){

						while (resultados_a_comparar[i].nombre != factors_a_comparar[contador_autoevaluacion]){

							autoevaluacion[contador_autoevaluacion] = 0;
							contador_autoevaluacion++;
						}

						autoevaluacion[contador_autoevaluacion] = resultados_a_comparar[i].nivel_desempeno * 1;
						contador_autoevaluacion++;
					}

					else{

						if (resultados_a_comparar[i].nombre_evaluacion == 'JEFE' || employee.jefe == resultados_a_comparar[i].idempleado){

							if (factor_jefe == resultados_a_comparar[i].nombre){

								contador_jefe--;
								evaluaciones_jefe++;
								total_jefe += resultados_a_comparar[i].nivel_desempeno;
								jefe[contador_jefe] = (total_jefe / evaluaciones_jefe).toFixed(1) * 1;
								contador_jefe++;
							}

							else{

								while (resultados_a_comparar[i].nombre != factors_a_comparar[contador_jefe]){

									jefe[contador_jefe] = 0;
									contador_jefe++;
								}

								total_jefe = resultados_a_comparar[i].nivel_desempeno * 1
								jefe[contador_jefe] = total_jefe;
								factor_jefe = resultados_a_comparar[i].nombre;
								contador_jefe++;
								evaluaciones_jefe = 1;
							}
						}

						else{

							if (modalidad_comparar > 2 && (resultados_a_comparar[i].nombre_evaluacion == 'SUBORDINADO' || employee.idempleado == resultados_a_comparar[i].jefe)){
							
								if (factor_colaborador == resultados_a_comparar[i].nombre){

									contador_colaborador--;
									evaluciones_colaborador++;
									total_colaborador += resultados_a_comparar[i].nivel_desempeno;
									colaborador[contador_colaborador] = (total_colaborador / evaluciones_colaborador).toFixed(1) * 1;
									contador_colaborador++;
								}

								else{

									while (resultados_a_comparar[i].nombre != factors_a_comparar[contador_colaborador]){

										colaborador[contador_colaborador] = 0;
										contador_colaborador++;
									}
								
									total_colaborador = resultados_a_comparar[i].nivel_desempeno * 1;
									colaborador[contador_colaborador] = total_colaborador;
									factor_colaborador = resultados_a_comparar[i].nombre;
									contador_colaborador++;
									evaluciones_colaborador = 1;
								}
							}

							else{

								if (modalidad_comparar > 1 && (resultados_a_comparar[i].nombre_evaluacion == 'PARES' || employee.jefe == resultados[i].jefe)){

									if (factor_par == resultados_a_comparar[i].nombre){

										contador_par--;
										evaluciones_par++;
										total_par += resultados_a_comparar[i].nivel_desempeno;
										par[contador_par] = (total_par / evaluciones_par).toFixed(1) * 1;
										contador_par++;
									}

									else{

										while (resultados_a_comparar[i].nombre != factors_a_comparar[contador_par]){

											par[contador_par] = 0;
											contador_par++;
										}
									
										total_par = resultados_a_comparar[i].nivel_desempeno * 1;
										par[contador_par] = total_par;
										factor_par = resultados_a_comparar[i].nombre;
										contador_par++;
										evaluciones_par = 1;
									}
								}

								else{

									if (modalidad_comparar == 4){

										if (factor_cliente == resultados_a_comparar[i].nombre){

											contador_cliente--;
											evaluaciones_cliente++;
											total_cliente += resultados_a_comparar[i].nivel_desempeno;
											cliente[contador_cliente] = (total_cliente / evaluaciones_cliente).toFixed(1) * 1;
											contador_cliente++;
										}

										else{

											while (resultados_a_comparar[i].nombre != factors_a_comparar[contador_cliente]){

												cliente[contador_cliente] = 0;
												contador_cliente++;
											}
									
											total_cliente = resultados_a_comparar[i].nivel_desempeno * 1;
											cliente[contador_cliente] = total_cliente;
											factor_cliente = resultados_a_comparar[i].nombre;
											contador_cliente++;
											evaluaciones_cliente = 1;
										}
									}
								}
							}
						}		
					}
				}

				for (var i = contador_autoevaluacion;i < factors_a_comparar.length;i++){

					autoevaluacion[i] = 0;
				}

				for (var i = contador_jefe;i < factors_a_comparar.length;i++){

					jefe[i] = 0;
				}

				for (var i = contador_colaborador;i < factors_a_comparar.length;i++){

					colaborador[i] = 0;
				}

				for (var i = contador_par;i < factors_a_comparar.length;i++){

					par[i] = 0;
				}

				for (var i = contador_cliente;i < factors_a_comparar.length;i++){

					cliente[i] = 0;
				}
   
   			var chart = {
      		renderTo: 'container_a_comparar',
      		options3d: {
        		enabled: true,
        		alpha: 0,
        		beta: 0,
        		depth: 50,
        		viewDistance: 25
      		}
   			};

   			var title = {
      		text: ''   
   			};

   			var legend = {
        	align: 'center',
        	verticalAlign: 'bottom',
        	layout: 'horizontal',
					floating: true,
					y: 34
      	};
   
   			var plotOptions = {
      		column: {
        		depth: 25
      		}
   			};

    		var series = [{
      		name: 'PROMEDIO',
          data: results_a_comparar,
          color: '#004A91',
					type: 'column',
        },{
            name: tipos_evaluadores[1],
            data: autoevaluacion,
            //color: '#C00000',
            color: '#000000',
						type: 'line',
        },{
        	name: tipos_evaluadores[2],
            data: jefe,
            //color: '#008000',
            color: '#D2691E',
						type: 'line',
        }
        ];

        if (modalidad_comparar > 2){

      		series.push({name: tipos_evaluadores[5], data: colaborador, color: 'purple', type: 'line'});
      	}

      	if (modalidad > 1){

      		series.push({name: tipos_evaluadores[3], data: par, color: '#A020F0', type: 'line'});
      	}

      	if (modalidad_comparar == 4){

      		series.push({name: tipos_evaluadores[4], data: cliente, color: '#008000', type: 'line'});
      	}

        /*,{
        		name: 'COLABORADOR',
            data: colaborador,
            color: '#FFA500',
						type: 'line',
        },{
        		name: 'PAR',
          	data: par,
          	color: '#A020F0',
		 	 			type: 'line',
        }*/
		
				var yAxis = [{
					labels: {
        		style: {
          		fontSize:'12px',
          		fontWeight:'bold'
        		}
      		},
      		max: 5,
      		tickInterval: 0.5
				}];

   			var xAxis = [{
      		categories: factors_a_comparar,
	  			labels: {
          	style: {
            	fontSize:'8px',
            	fontWeight:'bold'
          	},
          	step: 1
        	},
    		}];     
      
   			var json = {};
   			json.chart = chart;
   			json.title = title;
   			json.series = series;
   			json.plotOptions = plotOptions;
   			json.xAxis = xAxis;
				json.yAxis = yAxis;
				json.legend = legend;
   			highchart = new Highcharts.Chart(json);
   		}

   		$('form.comentarios_reporte').submit(function(){

   			// Temporizador para que se guarde la retroalimentación y recargar la página
   			setTimeout(function(){

   				alert('Comentarios y Objetivos Guardados');

   			/*if ($('textarea[name="comentarios_adicionales"]').val().trim().length > 0){

   				for (var i = 1;i < 6;i++){

   					if ($('textarea[name="objetivo' + i + '"]').val().trim().length > 0 && $('textarea[name="accion' + i + '"]').val().trim().length > 0 && $('textarea[name="entregable' + i + '"]').val().trim().length > 0){

   						$('button.print_report').show();
   					}
   				}
   			}*/

   				alertar = false;
   			
   				// Se envía el formulario para comparar periodos
					$('form.form_periodo_a_comparar').submit();
				}, 1000);
   		});

   		/*$('table.objetivos_entregables textarea').keydown(function(event){
				
				var altura = $(this).prop('scrollHeight') * 1;

   			if (altura > 82){

   				$(this).val($(this).val().substr(0, $(this).val().length - 2));
   			}
   		});*/

   		/*$('table.objetivos_entregables textarea').keyup(function(event){
				
				var altura = $(this).prop('scrollHeight') * 1;

   			if (altura > 82){

   				$(this).val($(this).val().substr(0, $(this).val().length - 1));
   			}
   		});*/

   		$('body').on('click', '.print_report', function(){

   			if ($(this).hasClass('completo')){

   				printDiv('areaImprimir', 2);
   			}

   			else{

   				printDiv('areaImprimir', 1);
   			}
   		});

   		// Write in some field for retroalimentation
   		$('.retro').keyup(function(){

   			if (canEdit == '1'){

   				alertar = true;
   			}
   		});

			// Se seleccionó un periodo para comparar
			$('body').on('change', 'select.periodo_a_comparar', function(){

				// Se envía el formulario para comparar periodos
				$('form.form_periodo_a_comparar').submit();
			});

			// Evento que muestra una alerta cuando se quiere abandonar la pagina
			window.onbeforeunload = function(e){

				// No hay cambios sin guardar
				if (!alertar){

					// No se muestra la alerta
					return null;
				}

  			// Se desea abandonar la página
  			abandonar_pagina = true;

  			// Se debe retornar algo diferente a null para que se muestre la alerta antes de abanadonar la página
  			return 'algo';
			};

			// El navegador no es Edge
			if (window.navigator.userAgent.indexOf("Edge") == -1){

				// Se crea un intérvalo que corre cada medio segundo
				setInterval(function(){

					// Se desea abandonar la pagina y debe mostrarse la alerta
					if (abandonar_pagina && alertar){

						abandonar_pagina = false;
					
						// Se muestra una segunda alerta con el mensaje correcto
						alert("¿Quieres salir de la página?\n\nSi sales de la página los cambios no se guardarán.");
					}
				}, 500);
			}

			var i = 0;

			/*$('g.highcharts-axis-labels.highcharts-xaxis-labels text').each(function(){

				if (i % 2 != 0){

					$(this).attr('transform', 'translate(0,19)');
				}

				i++;
			});*/

			var alto_contenedor = $('div.highcharts-container').height() + 28;
			$('div.highcharts-container, div.highcharts-container_a_comparar').css('height', alto_contenedor + 'px');
			$('div.highcharts-container svg, div.highcharts-container_a_comparar svg').attr('height', alto_contenedor);
			//$('g.highcharts-legend').attr('transform', 'translate(115,385)');
			}
   	});

		function printDiv(nombreDiv, completo){
			
			// Se oculta todo lo que no debe imprimirse
			$('.oculto').hide();

			// Se va a imprimir el resumen del reporte
			if (completo == 1){

				// Se ocultan los comentarios y retroalimentación
				$('.oculto2').hide();
			}

			//var output = $('textarea.retro').val().replace(/\r?\n/g,'<br/>');
			//$('textarea.retro').next().html(output);
			$('.mostrar').show();
			//$('html').css('margin', 0);
    	//$('body').css('margin', 0);
    	//$('.highcharts-container').attr('height', '350');
    	//$('.highcharts-container').css('height', '350px');
    	$('g.highcharts-yaxis text, text.highcharts-credits').remove();

    	if (navigator.userAgent.indexOf('Trident') > -1){

    		var anchura = $('.highcharts-container').attr('width');
				var altura = $('.highcharts-container').attr('height');
				$('.highcharts-container').attr('width', '546');
				$('.highcharts-container svg').attr('width', '546');
				$('.highcharts-container').attr('height', '388');
				$('.highcharts-container svg').attr('height', '388');
				$('.highcharts-container').css('overflow', 'visible');
    		/*$('.datos_evaluado').addClass('estilos_evaluado');
    		$('.datos_logo').addClass('estilos_logo');
    		$('.datos_competencias').addClass('estilos_competencias');
    		$('.datos_grafica').addClass('estilos_grafica');
    		$('.datos_firmas').addClass('estilos_firmas');
				$('#' + nombreDiv).print();
				$('.datos_evaluado').removeClass('estilos_evaluado');
    		$('.datos_logo').removeClass('estilos_logo');
    		$('.datos_competencias').removeClass('estilos_competencias');
    		$('.datos_grafica').removeClass('estilos_grafica');
    		$('.datos_firmas').removeClass('estilos_firmas');*/
    		$('.oculto').show();

    		// Se muestran los comentarios y retroalimentación
				$('.oculto2').show();

				$('.mostrar').hide();

				setTimeout(function(){
			
					$('.highcharts-container').attr('width', anchura);
					$('.highcharts-container svg').attr('width', anchura);
					$('.highcharts-container').attr('height', altura);
					$('.highcharts-container svg').attr('height', altura);
				}, 4000);
    	}

    	else{

    		// El navegador Edge
    		if (window.navigator.userAgent.indexOf("Edge") > -1){

    			// Agrega un borde a las celdas de la tabla de comentarios u observaciones, a la tabla de competencias y a la de los datos del evaluado
    			$('table.comentarios_reporte tbody tr td, table.tabla_competencias tbody tr td, table.tabla_evaluado tbody tr td, table.objetivos_entregables tbody tr td').css('border', '1px solid grey');

    			// Quita el atributo border de la tabla de comentarios u observaciones, de la de competencias y de la de los datos del evaluado
    			$('table.comentarios_reporte, table.tabla_competencias, table.tabla_evaluado, table.objetivos_entregables').removeAttr('border');
    		}

    		/*var areaImprimir = document.getElementById(nombreDiv);
				html2canvas(areaImprimir).then(function(canvas){
    			var myImage = canvas.toDataURL("image/png");
					$('#myImage').attr('src', myImage);
				
					setTimeout(function(){*/
						var heights = [];
						var counter = 0;

						$('table.objetivos_entregables tbody tr').each(function(){

							if ($(this).find('td:nth-child(2) textarea').length > 0){

								heights.push($(this).find('td:nth-child(2) textarea').height());
								$(this).find('td:nth-child(2) textarea').height($(this).find('td:nth-child(2) textarea').prop('scrollHeight'));
								$(this).find('td:nth-child(2) textarea').css('overflow', 'hidden');
								counter++;
							}
						});

						//$('textarea.retro').autoResize();
						var height = $('form.comentarios_reporte textarea[name="comentarios_adicionales"]').height();
						$('form.comentarios_reporte textarea[name="comentarios_adicionales"]').height($('form.comentarios_reporte textarea[name="comentarios_adicionales"]').prop('scrollHeight'));
						var contenido = document.getElementById('areaImprimir').innerHTML;
     				var contenidoOriginal= document.body.innerHTML;
						document.body.innerHTML = contenido;
						window.print();
						document.body.innerHTML = contenidoOriginal;
						//$('#myImage').attr('src', '');
						$('.oculto').show();

						// Se muestran los comentarios y retroalimentación
						$('.oculto2').show();

						$('.mostrar').hide();

						// El navegador Edge
						if (window.navigator.userAgent.indexOf("Edge") > -1){

    					// Agrega el atributo border con valor de 1 a la tabla de comentarios u observaciones, a la de competencias y a la de los datos del evaluado
    					$('table.comentarios_reporte, table.tabla_competencias, table.tabla_evaluado, table.objetivos_entregables').attr('border', 1);
    				}

    				var counter2 = 0;

    				$('table.objetivos_entregables tbody tr').each(function(){

							if (counter2 < counter){

								$(this).find('td:nth-child(2) textarea').height(heights[counter2]);
								$(this).find('td:nth-child(2) textarea').css('overflow', 'auto');
								counter2++;
							}
						});

						$('form.comentarios_reporte textarea[name="comentarios_adicionales"]').height(height);
					//}, 500);
				//});
			}
		}
	</script>
@endsection