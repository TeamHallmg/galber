@extends('layouts.app')

@section('title', 'Reporte Evaluación Cualitativa')

@section('content')
<style>div.dt-buttons {
	float: right;
	margin-left:10px;
	display: none;
	}</style>
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/evaluacion_desempeno.png" alt="">

		
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Reporte Evaluación Cualitativa
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
 
 

@if (!empty($periodos))

<div class="row"> 
	<div class="col-12 offset-md-4 col-md-3 text-center">
		<div class="form-group">
			<label for="id_plan" class="font-weight-bolder mb-0">Periodo:</label>							
			<select class="form-control periodos">
				<?php foreach ($periodos as $key => $periodo){ ?>
			                                      	
					<option value="<?php echo $periodo->id?>" <?php if (!empty($id_periodo) && $id_periodo == $periodo->id){ ?>selected="selected"<?php } ?>><?php echo $periodo->descripcion?></option>
		  <?php } ?>
			</select>
		</div>
	</div> 
</div>
 

@if (!empty($resultados))
<div class="card border-0">
	<h5 class="card-header bg-primary text-white font-weight-bolder">Resultados
		<button class="btn btn-success float-right text-left mr-2 btn-excel" title="Exportar a Excel">
			<i class="fa fa-file-excel"></i> Exportar a Excel
		</button>
	</h5>
	<div class="card-body px-0"> 

<div class="row margin-top-20">
	<div class="col-md-12">
		<div class="table-responsive">
		<table width="100%" class="data-table table table-striped">
			<thead class="cabeceras-tablas-evaluaciones" style="background-color: #222B64; color:white;">
				<tr>
					<th>ID</th>
					<th>Evaluado</th>
					<th>Departamento</th>
					<th>Puesto</th>
					<th>Evaluador</th>
					<th>Tipo de Evaluación</th>
					<th>Finalizó</th>

	<?php foreach ($factores as $key => $factor){ ?>
		
					<th>{{$factor->nombre}}</th>
	<?php } ?>
					
					<th>Promedio</th>
				</tr>
			</thead>
			<tbody>

 <?php 	$actual_evaluador = 0;
 				$actual_evaluado = 0;
 				$actual_columna = 0;
 				$num_factores = 0;
 				$i = 0;
 				$calificaciones = 0;

 				foreach ($resultados as $key => $resultado){

 					if ($actual_evaluador != $resultado->id_evaluador || $actual_evaluado != $resultado->id_evaluado){

 						if ($actual_evaluador != 0 || $actual_evaluado != 0){

 							for ($i = $actual_columna;$i < count($factores);$i++){ ?>

					<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
				<?php }

							$promedio = $calificaciones / $num_factores; ?>

						<td class="text-center" style="vertical-align: middle; font-weight: bold; font-size: 20px; border: 1px solid #DDD; background-color: {{($promedio >= 80 ? '#008000' : ($promedio >= 60 ? '#73CB77' : ($promedio >= 40 ? '#FFFF00' : ($promedio >= 20 ? '#FF4343' : '#C00000'))))}}">{{number_format($promedio, 2, '.', ',')}}</td>
 				</tr>
 			
 			<?php }

 						$actual_evaluador = $resultado->id_evaluador;
 						$actual_evaluado = $resultado->id_evaluado;
 						$actual_columna = 0;
 						$num_factores = 0;
 						$calificaciones = 0; ?>

				<tr>
					<td class="text-left" style="border: 1px solid #DDD">{{$resultado->idempleado}}</td>
					<td class="text-left" style="border: 1px solid #DDD">{{str_replace('Ã‘','Ñ',$resultado->last_name)}} {{str_replace('Ã‘','Ñ',$resultado->first_name)}}</td>
					<td class="text-left" style="border: 1px solid #DDD">{{str_replace('Ã‘','Ñ',$resultado->department)}}</td>
					<td class="text-left" style="border: 1px solid #DDD">{{str_replace('Ã‘','Ñ',$resultado->puesto)}}</td>
					<td class="text-left" style="border: 1px solid #DDD">{{(!empty($resultado->evaluador) ? str_replace('Ã‘','Ñ',$resultado->evaluador) : '')}}</td>
					<td class="text-center" style="border: 1px solid #DDD; vertical-align: middle;">{{(!empty($resultado->descripcion_empresa) ? $resultado->descripcion_empresa : ($resultado->id_evaluador == $resultado->id_evaluado ? $tipos_evaluadores[1] : ($resultado->jefe == $resultado->idempleado_evaluador ? $tipos_evaluadores[2] : ($resultado->id_evaluado == $resultado->id_jefe_evaluador ? $tipos_evaluadores[5] : ($resultado->jefe == $resultado->idempleado_jefe_evaluador ? $tipos_evaluadores[3] : $tipos_evaluadores[4])))))}}</td>
					<td nowrap>{{$fechas_termino[$resultado->id_evaluador][$resultado->id_evaluado]}}</td>
		<?php }

					for ($i = $actual_columna;$i < count($factores);$i++){
						
						if ($factores[$i]->id == $resultado->id_factor){

							break;
						} ?>

					<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
		<?php }

					$actual_columna = $i;
					$actual_columna++;
					$num_factores++;
					$calificaciones += $resultado->nivel_desempeno/* * $pesos_factores[$resultado->id_nivel_puesto][$resultado->id_factor] / 105*/; 
					if($resultado->nivel_desempeno >= 80){
						$color = '#008000';
						$color_text = '#fff';
					}else if ($resultado->nivel_desempeno >= 60) {
						$color = '#73CB77';
						$color_text = '#000';
					}else if ($resultado->nivel_desempeno >= 40) {
						$color = '#FFFF00';
						$color_text = '#000';
					}else if ($resultado->nivel_desempeno >= 20) {
						$color = '#FF4343';
						$color_text = '#fff';
					}else{
						$color = '#C00000';
						$color_text = '#fff';
					}
					
					?>


					<td class="text-center h4">
						<span class="badge  badge-pill" style="background:<?php echo $color; ?>;color:<?php echo $color_text; ?>"  > {{number_format($resultado->nivel_desempeno, 2, '.', ',')}} </span>
					</td>

	<?php }

				if ($actual_evaluador != 0 || $actual_evaluado != 0){

					for ($i = $actual_columna;$i < count($factores);$i++){ ?>

					<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
		<?php }

					$promedio = $calificaciones / $num_factores; 
					if($promedio >= 80){
						$color = '#008000';
						$color_text = '#fff';
					}else if ($promedio >= 60) {
						$color = '#73CB77';
						$color_text = '#000';
					}else if ($promedio >= 40) {
						$color = '#FFFF00';
						$color_text = '#000';
					}else if ($promedio >= 20) {
						$color = '#FF4343';
						$color_text = '#fff';
					}else{
						$color = '#C00000';
						$color_text = '#fff';
					}
					
					?>
 

					<td class="text-center h4">
						<span class="badge  badge-pill" style="background:<?php echo $color; ?>;color:<?php echo $color_text; ?>"  > {{number_format($promedio, 2, '.', ',')}} </span>
					</td>
					 
  				</tr>
 	<?php } ?>

			</tbody>
		</table>
	</div>
	</div>
</div>
</div>
</div>

@else

<div class="row">
				
	<div class="col-md-12 text-center">
		
		<div class="alert alert-warning h4" role="alert">
			<i class="fa fa-exclamation-triangle"></i>	Aún no hay resultados en la evaluación del periodo
		</div>

	</div>
</div>
 
@endif

@else

<div class="row">
				
	<div class="col-md-12 text-center">
		
		<div class="alert alert-warning h4" role="alert">
			<i class="fa fa-exclamation-triangle"></i>	No Hay Periodos Abiertos o Cerrados
		</div>

	</div>
</div> 
@endif
</div>
</div>
@endsection

@section('scripts')
	<script>
		
		$('.btn-excel').click(function(){
			$('.buttons-excel').trigger('click');
		});

		$(document).ready(function(){

			$('.data-table').DataTable({
					dom:"<'row'<'col-md-6'l><'col-md-6'Bf>>" +
"<'row'<'col-md-6'><'col-md-6'>>" +
"<'row'<'col-md-12't>><'row'<'col-md-12'ip>>",	
   			'order': [[0,'asc']],
   			'scrollX': true,
   			language: {
		 		'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
      		  },
      	buttons: [
        {
          extend: 'excel',
					text: 'Exportar a Excel',
					titleAttr: 'Exportar a Excel',
					title: 'Reporte Evaluación Cualitativa'
				}
        ]
   		});

   		$('select.periodos').change(function(){

   			window.location = '/reporte-sabana/' + $(this).val();
   		});
		});
	</script>
@endsection