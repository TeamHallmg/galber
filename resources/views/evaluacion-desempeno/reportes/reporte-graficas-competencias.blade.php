@extends('layouts.app')

@section('title', 'Reporte Gráficas por Competencias')

@section('content')
<div class="row margin-top-20 mt-2">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/evaluacion_desempeno.png" alt="">
	</div>
</div>
<div class="row">
	<div class="col-md-12 text-center">
		<div id="myImageContainer">
			<img id="myImage">
		</div>
		<button class="btn btn-primary print_report" >
			<i class="fas fa-print"></i> Imprimir Reporte
		</button>
	</div>
</div>
<div class="row margin-top-20 mt-2" id="areaImprimir">
	<div class="col-md-12">
		<h3 class="titulos-evaluaciones text-center" style="margin-bottom: 0">Reporte con Gráficas para las Competencias</h3>
	</div>

@if (!empty($periodos))

	<div class="col-md-6 margin-top-20 mt-2">
		<div>Periodo: <select class="periodos form-control" style="display: inline-block; width: auto">

			    	<?php foreach ($periodos as $key => $periodo){ ?>
			                                      	
			              <option value="<?php echo $periodo->id?>" <?php if ($id_periodo == $periodo->id){ ?>selected="selected"<?php } ?>><?php echo $periodo->descripcion?></option>
			   		<?php } ?>
								 																 
								 	</select>
		</div>
	</div>
	<div class="col-md-6 margin-top-20 mt-2">
		<div><b style="margin-right: 50px">Filtro Inicial:</b> Direcciones <input type="radio" checked="checked" name="filtro_inicial" value="1"> Niveles de Puestos <input type="radio" name="filtro_inicial" value="2">
		</div>
	</div>
	<div class="col-md-6 margin-top-20 mt-2">
		<div>
			<select class="competencias form-control">
				<option value="-1">-- Seleccione Competencia --</option>

<?php foreach ($factores as $key => $factor){ ?>
			                                      	
			  <option value="<?php echo $factor->id?>"><?php echo $factor->nombre?></option>
<?php } ?>
								 																 
			</select>
		</div>
	</div>
	<div class="col-md-6 margin-top-20 mt-2">
		<div>
			<select class="grupo_areas form-control">
				<option value="-1">-- Seleccione Grupo de Area --</option>

<?php foreach ($grupo_areas as $key => $grupo_area){ ?>
			                                      	
			  <option value="<?php echo $grupo_area->id?>"><?php echo $grupo_area->nombre?></option>
			<?php } ?>
								 																 
			</select>
		</div>
	</div>
	<div class="col-md-6 margin-top-20 mt-2">
		<div>
			<select class="direcciones form-control">
				<option value="-1">-- Seleccione Dirección --</option>

<?php foreach ($direcciones as $key => $direccion){ ?>
			                                      	
			  <option value="<?php echo $direccion->id?>"><?php echo $direccion->name?></option>
			<?php } ?>
								 																 
			</select>
		</div>
	</div>
	<div class="col-md-6 margin-top-20 mt-2">
		<div>
			<select class="niveles_puestos form-control">
				<option value="-1">-- Seleccione Nivel de Puesto --</option>

<?php foreach ($niveles_puestos as $key => $nivel_puesto){ ?>
			                                      	
			  <option value="<?php echo $nivel_puesto->id?>"><?php echo $nivel_puesto->nombre?></option>
<?php } ?>
								 																 
			</select>
		</div>
	</div>
	<div class="col-md-6 margin-top-20 mt-2">
		<div>
			<select class="areas form-control">
				<option value="-1">-- Seleccione Área --</option>

<?php foreach ($areas as $key => $area){ ?>
			                                      	
			  <option value="<?php echo $area->id?>"><?php echo $area->name?></option>
			<?php } ?>
								 																 
			</select>
		</div>
	</div>
	<div class="col-md-12 margin-top-20 mt-2">
		<div id="container" style="width: 100%; margin: 0 auto"></div>
	</div>
</div>
<form action="/reporte-graficas-competencias/{{$id_periodo}}" method="post" class="ver_reporte_grafico">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="submit" style="display: none">
</form>

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20 mt-2">No hay periodos cerrados ni abiertos</h4>
	</div>
</div>
@endif
@endsection

@section('scripts')
<script>

	var resultados = <?php echo json_encode($resultados)?>;
	var direcciones = <?php echo json_encode($direcciones)?>;
	var area = 0;
	var grupo_area = 0;
	var nivel_puesto = 0;
	var direccion = 0;
	var competencia = 0;
	var contador = 0;

	$(document).ready(function(){

			generar_grafica();

   		$('body').on('change', 'select.periodos', function(){

   			window.location = '/reporte-graficas/' + $(this).val();
   		});

   		$('body').on('change', 'select.competencias', function(){

   			generar_grafica();
   		});

   		$('body').on('change', 'select.grupo_areas', function(){

   			$('select.areas, select.direcciones, select.niveles_puestos').val(-1);
   			
   			var grupo_area = $(this).val();

   			if (grupo_area != -1){

   				for(var i = 0;i < areas.length;i++){

   					if (areas[i].id_grupos_areas == grupo_area){

   						$('select.areas option[value="' + areas[i].id + '"]').show();
   					}

   					else{

   						$('select.areas option[value="' + areas[i].id + '"]').hide();
   					}
   				}
   			}

   			else{

   				$('select.areas option').show();
   			}

   			generar_grafica();
   		});

   		$('body').on('change', 'select.niveles_puestos', function(){

   			$('select.grupo_areas').val(-1);
   			$('select.areas').val(-1);
   			$('select.direcciones').val(-1);
   			$('select.areas option').show();
   			generar_grafica();
   		});

   		$('body').on('change', 'select.areas', function(){

   			$('select.niveles_puestos').val(-1);
   			generar_grafica();
   		});

   		$('body').on('change', 'select.direcciones', function(){

   			$('select.areas').val(-1);
   			$('select.niveles_puestos').val(-1);
   			$('select.puestos').val(-1);
   			var i, j;
   			var direccion = $(this).val();

   			if (direccion != -1){

          for(i = 0;i < areas.length;i++){

            if (areas[i].direction_id == direccion){

              $('select.areas option[value="' + areas[i].id + '"]').show();

              for(j = 0;j < puestos.length;j++){

                if (puestos[j].area_id == areas[i].id){

                  $('select.puestos option[value="' + puestos[j].id + '"]').show();
                }
              }
            }

            else{

              $('select.areas option[value="' + areas[i].id + '"]').hide();

              for(j = 0;j < puestos.length;j++){

                if (puestos[j].area_id == areas[i].id){

                  $('select.puestos option[value="' + puestos[j].id + '"]').hide();
                }
              }
            }
          }
   			}

   			else{

   				$('select.areas option').show();
   				$('select.puestos option').show();
   			}

   			generar_grafica();
   		});

   		$('body').on('click', 'input[name="filtro_inicial"]', function(){

   			generar_grafica();
   		});

   		$('body').on('click', '.print_report', function(){

   			imprimir('areaImprimir');
   		});
		});

		function generar_grafica(){

			var calificaciones = [];
			var calificaciones_autoevaluacion = [];
			var calificaciones_jefe = [];
			var filtros = [];
			var posiciones = []
			var contador_filtros = 0;
			var contador_evaluaciones = [];
			var contador_autoevaluacion = [];
			var contador_jefe = [];
			var posicion = 0;
			var filtro = '';
			area = $('select.areas').val();
			grupo_area = $('select.grupo_areas').val();
			nivel_puesto = $('select.niveles_puestos').val();
			direccion = $('select.direcciones').val();
			competencia = $('select.competencias').val();

			for (var i = 0;i < resultados.length;i++){

				if (competencia == -1 || resultados[i].id_factor == competencia){

					if (nivel_puesto != -1){

						if (resultados[i].nivel_puesto == nivel_puesto){

							filtro = resultados[i].puesto;
						}
					}

					else{

						if (area != -1){

							if (resultados[i].area == area){

								filtro = resultados[i].puesto;
							}
						}

						else{

							if (direccion != -1){

								if (resultados[i].workstation == direccion){

									filtro = resultados[i].division;
								}	
							}

							else{

								if (grupo_area != -1){

									if (resultados[i].grupo_area == grupo_area){

										filtro = resultados[i].area;
									}	
								}

								else{

									if ($('input[name="filtro_inicial"]:checked').val() == 1){

										filtro = $('select.direcciones option[value="' + resultados[i].direction + '"]').text();
									}

									else{

										filtro = $('select.niveles_puestos option[value="' + resultados[i].nivel_puesto + '"]').text();
									}
								}									
							}
						}
					}

					if (filtro != ''){

						if (posiciones[filtro] === undefined){

							posiciones[filtro] = contador_filtros;
							calificaciones[contador_filtros] = 0;
							contador_evaluaciones[contador_filtros] = 0;
							calificaciones_autoevaluacion[contador_filtros] = 0;
							contador_autoevaluacion[contador_filtros] = 0;
							calificaciones_jefe[contador_filtros] = 0;
							contador_jefe[contador_filtros] = 0;
							filtros[contador_filtros] = filtro;
							contador_filtros++;
						}

						posicion = posiciones[filtro];
						calificaciones[posicion] += resultados[i].nivel_desempeno;
						contador_evaluaciones[posicion]++;

						if (resultados[i].id_evaluador == resultados[i].id_evaluado){

							calificaciones_autoevaluacion[posicion] += resultados[i].nivel_desempeno;
							contador_autoevaluacion[posicion]++;
						}

						else{

							calificaciones_jefe[posicion] += resultados[i].nivel_desempeno;
							contador_jefe[posicion]++;
						}
					}
				}
			}

			for (var i = 0;i < contador_filtros;i++){

				calificaciones[i] = calificaciones[i] / contador_evaluaciones[i];
				calificaciones[i] = calificaciones[i].toFixed(2) * 1;

				if (contador_autoevaluacion[i] != 0){

					calificaciones_autoevaluacion[i] = calificaciones_autoevaluacion[i] / contador_autoevaluacion[i];
					calificaciones_autoevaluacion[i] = calificaciones_autoevaluacion[i].toFixed(2) * 1;
				}

				if (contador_jefe[i] != 0){

					calificaciones_jefe[i] = calificaciones_jefe[i] / contador_jefe[i];
					calificaciones_jefe[i] = calificaciones_jefe[i].toFixed(2) * 1;
				}
			}

			var chart = {
      	renderTo: 'container',
      	type: 'column',
      	options3d: {
        	enabled: true,
        	alpha: 0,
        	beta: 0,
        	depth: 50,
        	viewDistance: 25
      	}
   		};

   		var title = {
      	text: ''   
   		};
   
   		var plotOptions = {
      	column: {
        	depth: 25,
        	zones: [
        		{
          		value: 2,
          		color: '#C00000'
        		},
        		{
          		value: 2.5,
          		color: '#FF4343'
        		},
        		{
          		value: 3,
          		color: '#FFFF00'
        		},
        		{
          		value: 3.5,
          		color: '#73CB77'
        		},
        		{
          		color: '#008000'
        		}
       		]
      	}
   		};

    	var series = [{
    		name: 'PROMEDIO',
      	data: calificaciones,
      	type: 'column'
   			},{
        name: 'AUTOEVALUACION',
        data: calificaciones_autoevaluacion,
        //color: '#C00000',
        color: '#000000',
				type: 'line'
       	},{
        name: 'JEFE',
        data: calificaciones_jefe,
        //color: '#008000',
        color: '#D2691E',
				type: 'line'
      }];
		
			var yAxis = [{
				labels: {
        	style: {
          	fontSize:'12px',
          	fontWeight:'bold'
        	},
        	/*formatter: function(){
       			return this.value + "%";
    			}*/
      	},
      	max: 4,
      	tickInterval: 1
			}];

   		var xAxis = [{
      	categories: filtros,
	  		labels: {
          style: {
            fontSize:'8px',
            fontWeight:'bold'
          },
          step: 1
        },
    	}];     
      
   		var json = {};
   		json.chart = chart;
   		json.title = title;
   		json.series = series;
   		json.plotOptions = plotOptions;
   		json.xAxis = xAxis;
			json.yAxis = yAxis;
   		new Highcharts.Chart(json);
		}

		function imprimir(nombreDiv){

			$('g.highcharts-yaxis text, text.highcharts-credits').remove();
			var areaImprimir = document.getElementById(nombreDiv);
			html2canvas(areaImprimir).then(function(canvas){
    	
    		var myImage = canvas.toDataURL("image/png");
				$('#myImage').attr('src', myImage);
				
				setTimeout(function(){
					
					var contenido = document.getElementById('myImageContainer').innerHTML;
     			var contenidoOriginal= document.body.innerHTML;
					document.body.innerHTML = contenido;
					window.print();
					document.body.innerHTML = contenidoOriginal;
					$('#myImage').attr('src', '');
					$('select.areas').val(area);
					$('select.grupo_areas').val(grupo_area);
					$('select.niveles_puestos').val(nivel_puesto);
					$('select.direcciones').val(direccion);
					$('select.competencias').val(competencia);
				}, 500);
			});
		}
	</script>
@endsection
