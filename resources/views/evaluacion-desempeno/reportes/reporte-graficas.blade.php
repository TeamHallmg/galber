@extends('layouts.app')

@section('title', 'Reporte Evaluación Cualitativa')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/evaluacion_desempeno.png" alt="">
	</div>
</div>
<div class="row">
  <div class="col-md-12 text-center">
    <div id="myImageContainer">
      <img id="myImage">
    </div>
    <button class="btn btn-primary print_report">
      <i class="fas fa-print"></i> Imprimir Reporte
    </button>
  </div>
</div>
<div class="row margin-top-20 mt-2" id="areaImprimir">
	<div class="col-md-12">
		<h3 class="titulos-evaluaciones text-center" style="margin-bottom: 0">Reporte Evaluación Cualitativa</h3>
	</div>

@if (!empty($periodos))

	<div class="col-md-12 mt-2">
		<div>Periodo: <select class="periodos form-control" style="display: inline-block; width: auto">

			    	<?php foreach ($periodos as $key => $periodo){ ?>
			                                      	
			              <option value="<?php echo $periodo->id?>" <?php if ($id_periodo == $periodo->id){ ?>selected="selected"<?php } ?>><?php echo $periodo->descripcion?></option>
			   		<?php } ?>
								 																 
								 	</select>
		</div>
	</div>
	<div class="col-md-6 mt-2">
		<div>
      <input type="text" class="form-control grupo_areas" placeholder="Escribe el Grupo de Área">
		</div>
	</div>
	<div class="col-md-6 mt-2">
		<div>
      <input type="text" class="form-control niveles_puestos" placeholder="Escribe el Nivel de Puesto">
		</div>
	</div>
	<div class="col-md-6 mt-2">
		<div>
      <input type="text" class="form-control direcciones" placeholder="Escribe la Dirección">
		</div>
	</div>
  <div class="col-md-6 mt-2">
    <div>
      <input type="text" class="form-control departments" placeholder="Escribe el Departamento">
    </div>
  </div>
	<div class="col-md-6 mt-2">
		<div>
      <input type="text" class="form-control puestos" placeholder="Escribe el Puesto">
		</div>
	</div>
  <div class="col-md-6 mt-2 text-center">
    <button class="btn btn-primary clean">Limpiar</button>
  </div>
  <div class="col-md-12 mt-2 text-center">
    <h2>Promedio: <span class="promedio"></span></h2>
  </div>
	<div class="col-md-12 mt-2">
		<div id="container" style="width: 100%; margin: 0 auto"></div>
	</div>
</div>
<form action="/reporte-graficas/{{$id_periodo}}" method="post" class="ver_reporte_grafico">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="submit" style="display: none">
</form>

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20">No hay periodos cerrados ni abiertos</h4>
	</div>
</div>
@endif
@endsection

@section('scripts')
<script>

	var resultados = <?php echo json_encode($resultados)?>;
	var factores = <?php echo json_encode($factores)?>;
	var puestos = <?php echo json_encode($puestos)?>;
	var direcciones = <?php echo json_encode($direcciones)?>;
  var departments = <?php echo json_encode($departments)?>;
  var grupo_areas = <?php echo json_encode($grupo_areas)?>;
  var niveles_puestos = <?php echo json_encode($niveles_puestos)?>;
  var tipos_evaluadores = <?php echo json_encode($tipos_evaluadores)?>;
	var competencias = [];
	var contador = 0;
  var department = 0;
  var grupo_area = 0;
  var nivel_puesto = 0;
  var direccion = 0;
  var puesto = 0;
  var nombre_puesto = 0;
  var puestos_sin_repetir = [];
  var puestos_filtrados = [];

	$(document).ready(function(){

			for (var i = 0;i < factores.length;i++){

				competencias[i] = factores[i].nombre;
			}

			generar_grafica(true);

   		$('body').on('change', 'select.periodos', function(){

   			window.location = '/reporte-graficas/' + $(this).val();
   		});

      // Reset the filters
      $('button.clean').click(function(){

        grupo_area = nivel_puesto = direccion = department = puesto = 0;
        $('.grupo_areas, .niveles_puestos, .departments, .direcciones, .puestos').val('');
        puestos_filtrados = [];

        // Autocomplete 
        $('.grupo_areas').autocomplete({
      
          lookup: grupo_areas,
          minChars: 0,
          onSelect: function (suggestion){

            grupo_area = suggestion.data;
            cambio_grupo_area();
          }
        });

        // Autocomplete 
        $('.niveles_puestos').autocomplete({
      
          lookup: niveles_puestos,
          minChars: 0,
          onSelect: function (suggestion){

            nivel_puesto = suggestion.data;
            cambio_nivel_puesto();
          }
        });

        // Autocomplete 
        $('.departments').autocomplete({
      
          lookup: departments,
          minChars: 0,
          onSelect: function (suggestion){

            department = suggestion.data;
            cambio_departments();
          }
        });

        // Autocomplete 
        $('.puestos').autocomplete({
      
          lookup: puestos_sin_repetir,
          minChars: 0,
          onSelect: function (suggestion){

            puesto = suggestion.data;
            nombre_puesto = suggestion.value;
            generar_grafica(true);
          }
        });

        // Autocomplete 
        $('.direcciones').autocomplete({
      
          lookup: direcciones,
          minChars: 0,
          onSelect: function (suggestion){

            direccion = suggestion.data;
            cambio_direcciones();
          }
        });

        generar_grafica(true);
      });

      // Autocomplete 
      $('.grupo_areas').autocomplete({
      
        lookup: grupo_areas,
        minChars: 0,
        onSelect: function (suggestion){

          grupo_area = suggestion.data;
          cambio_grupo_area();
        }
      });

      // Autocomplete 
      $('.niveles_puestos').autocomplete({
      
        lookup: niveles_puestos,
        minChars: 0,
        onSelect: function (suggestion){

          nivel_puesto = suggestion.data;
          cambio_nivel_puesto();
        }
      });

      // Autocomplete 
      $('.departments').autocomplete({
      
        lookup: departments,
        minChars: 0,
        onSelect: function (suggestion){

          department = suggestion.data;
          cambio_departments();
        }
      });

      var band = false;

      for (var i = 0;i < puestos.length;i++){
        
        band = false;

        for (var j = 0;j < puestos_sin_repetir.length;j++){

          if (puestos[i].value == puestos_sin_repetir[j].value){

            band = true;
          }
        }

        if (!band){

          puestos_sin_repetir.push(puestos[i]);
        }
      }

      // Autocomplete 
      $('.puestos').autocomplete({
      
        lookup: puestos_sin_repetir,
        minChars: 0,
        onSelect: function (suggestion){

          puesto = suggestion.data;
          nombre_puesto = suggestion.value;
          generar_grafica(true);
        }
      });

      // Autocomplete 
      $('.direcciones').autocomplete({
      
        lookup: direcciones,
        minChars: 0,
        onSelect: function (suggestion){

          direccion = suggestion.data;
          cambio_direcciones();
        }
      });

      $('body').on('click', '.print_report', function(){

        imprimir('areaImprimir');
      });

      $('body').on('click', '.highcharts-legend-item text', function(){

        setTimeout(function(){

          generar_grafica(false);
        }, 500);
      });
		});

		function generar_grafica(regenerar){

			var calificaciones = [];
      var calificaciones_reales = [];
			var calificaciones_autoevaluacion = [];
			var calificaciones_jefe = [];
      var calificaciones_colaborador = [];
      var calificaciones_par = [];
      var calificaciones_psicometricos = [];
			var actual_factor = 0;
			var contador_factores = 0;
			var contador_evaluaciones = contador_autoevaluacion = contador_jefe = contador_colaborador = contador_par = contador_psicometricos = contador_real = 0;
			var promedio = promedio_autoevaluacion = promedio_jefe = promedio_colaborador = promedio_par = promedio_psicometricos = promedio_real = 0;
			/*department = $('select.departments').val();
			puesto = $('select.puestos').val();
			grupo_area = $('select.grupo_areas').val();
			nivel_puesto = $('select.niveles_puestos').val();
		  direccion = $('select.direcciones').val();*/
      var leyenda_autoevaluacion = true, leyenda_jefe = true, leyenda_colaborador = true, leyenda_par = true, leyenda_psicometricos = true;
			var band = true;

      $('g.highcharts-legend-item').each(function(){

        if ($(this).hasClass('highcharts-legend-item-hidden')){

          if ($(this).find('text tspan').text() == tipos_evaluadores[1]){

            leyenda_autoevaluacion = false;
          }

          else{

            if ($(this).find('text tspan').text() == tipos_evaluadores[2]){

              leyenda_jefe = false;
            }

            else{

              if ($(this).find('text tspan').text() == tipos_evaluadores[5]){

                leyenda_colaborador = false;
              }
            }
          }
        }
      });

			for(var i = 0;i < resultados.length;i++){

				if (actual_factor != resultados[i].id_factor){

					if (actual_factor != 0){

						if (contador_evaluaciones != 0){

              calificacion = promedio / contador_evaluaciones;
						  calificaciones[contador_factores] = calificacion.toFixed(2) * 1;

              if (contador_real != 0){

               calificacion = promedio_real / contador_real;
               calificaciones_reales[contador_factores] = calificacion.toFixed(2) * 1;
              }

              else{

               calificaciones_reales[contador_factores] = 0;
              }

						  if (contador_autoevaluacion != 0){

							 calificacion = promedio_autoevaluacion / contador_autoevaluacion;
						   calificaciones_autoevaluacion[contador_factores] = calificacion.toFixed(2) * 1;
						  }

						  else{

							 calificaciones_autoevaluacion[contador_factores] = 0
						  }

						  if (contador_jefe != 0){

							  calificacion = promedio_jefe / contador_jefe;
							  calificaciones_jefe[contador_factores] = calificacion.toFixed(2) * 1;
						  }

						  else{

							  calificaciones_jefe[contador_factores] = 0
						  }

              if (contador_colaborador != 0){

                calificacion = promedio_colaborador / contador_colaborador;
                calificaciones_colaborador[contador_factores] = calificacion.toFixed(2) * 1;
              }

              else{

                calificaciones_colaborador[contador_factores] = 0
              }

              if (contador_par != 0){

                calificacion = promedio_par / contador_par;
                calificaciones_par[contador_factores] = calificacion.toFixed(2) * 1;
              }

              else{

                calificaciones_par[contador_factores] = 0
              }

              if (contador_psicometricos != 0){

                calificacion = promedio_psicometricos / contador_psicometricos;
                calificaciones_psicometricos[contador_factores] = calificacion.toFixed(2) * 1;
              }

              else{

                calificaciones_psicometricos[contador_factores] = 0
              }
            }

            else{

              calificaciones[contador_factores] = 0;
              calificaciones_reales[contador_factores] = 0;
              calificaciones_autoevaluacion[contador_factores] = 0;
              calificaciones_jefe[contador_factores] = 0;
              calificaciones_colaborador[contador_factores] = 0;
              calificaciones_par[contador_factores] = 0;
              calificaciones_psicometricos[contador_factores] = 0;
            }

						//calificaciones[contador_factores] = promedio / contador_evaluaciones;
						//calificaciones[contador_factores] = calificaciones[contador_factores].toFixed(2) * 1;
						//calificaciones[contador_factores] = parseInt(calificaciones[contador_factores] * 100 / 4);
						contador_factores++;
					}

					actual_factor = resultados[i].id_factor;
					contador_evaluaciones = contador_real = contador_autoevaluacion = contador_jefe = contador_colaborador = contador_par = contador_psicometricos = 0;
					promedio = promedio_real = promedio_autoevaluacion = promedio_jefe = promedio_colaborador = promedio_par = promedio_psicometricos = 0;

					while(factores[contador_factores].id != actual_factor){

						calificaciones[contador_factores] = calificaciones_reales[contador_factores] = calificaciones_autoevaluacion[contador_factores] = calificaciones_jefe[contador_factores] = calificaciones_colaborador[contador_factores] = calificaciones_par[contador_factores] = calificaciones_psicometricos[contador_factores] = 0;
						contador_factores++;
					}
				}

				band = false;

				if (department != 0 && resultados[i].department != department){

					band = true;
				}

				if (!band && ((puesto != 0 && resultados[i].puesto != puesto) || (puesto == 0 && puestos_filtrados.length > 0 && !puestos_filtrados.includes(resultados[i].puesto)))){

					band = true;
				}

				if (!band && grupo_area != 0 && resultados[i].grupo_area != grupo_area){

					band = true;
				}

				if (!band && nivel_puesto != 0 && resultados[i].nivel_puesto != nivel_puesto){

					band = true;
				}

				if (!band && direccion != 0 && resultados[i].direction != direccion){

					band = true;
				}

				if (!band){

					//promedio += resultados[i].total_evaluacion / resultados[i].numero_evaluaciones;
					promedio += resultados[i].nivel_desempeno;
					contador_evaluaciones++;

					if (resultados[i].id_evaluador == resultados[i].id_evaluado){

						promedio_autoevaluacion += resultados[i].nivel_desempeno;
						contador_autoevaluacion++;

            if (leyenda_autoevaluacion == true){

              promedio_real += resultados[i].nivel_desempeno;
              contador_real++;
            }
					}

					else{

            if (resultados[i].idempleado_evaluador == resultados[i].jefe || resultados[i].nombre_evaluacion == 'JEFE'){

						  promedio_jefe += resultados[i].nivel_desempeno;
						  contador_jefe++;

              if (leyenda_jefe == true){

                promedio_real += resultados[i].nivel_desempeno;
                contador_real++;
              }
            }

            else{

              if (resultados[i].id_jefe_evaluador == resultados[i].id_evaluado || resultados[i].nombre_evaluacion == 'SUBORDINADO'){

                promedio_colaborador += resultados[i].nivel_desempeno;
                contador_colaborador++;

                if (leyenda_colaborador == true){

                  promedio_real += resultados[i].nivel_desempeno;
                  contador_real++;
                }
              }

              else{

                if (resultados[i].idempleado_jefe_evaluador == resultados[i].jefe || resultados[i].nombre_evaluacion == 'PARES'){

                  promedio_par += resultados[i].nivel_desempeno;
                  contador_par++;

                  if (leyenda_par == true){

                    promedio_real += resultados[i].nivel_desempeno;
                    contador_real++;
                  }
                }

                else{

                  //if (resultados[i].nombre_evaluacion == 'COLABORADORES'){

                    promedio_psicometricos += resultados[i].nivel_desempeno;
                    contador_psicometricos++;

                    if (leyenda_psicometricos == true){

                      promedio_real += resultados[i].nivel_desempeno;
                      contador_real++;
                    }
                  //}  
                } 
              }
            }
					}
				}
			}

			if (actual_factor != 0){

        if (contador_evaluaciones != 0){

				  calificacion = promedio / contador_evaluaciones;
				  calificaciones[contador_factores] = calificacion.toFixed(2) * 1;

          if (contador_real != 0){

            calificacion = promedio_real / contador_real;
            calificaciones_reales[contador_factores] = calificacion.toFixed(2) * 1;
          }

          else{

            calificaciones_reales[contador_factores] = 0;
          }

				  if (contador_autoevaluacion != 0){

					  calificacion = promedio_autoevaluacion / contador_autoevaluacion;
					  calificaciones_autoevaluacion[contador_factores] = calificacion.toFixed(2) * 1;
				  }

				  else{

					  calificaciones_autoevaluacion[contador_factores] = 0
				  }

			 	  if (contador_jefe != 0){

					  calificacion = promedio_jefe / contador_jefe;
					  calificaciones_jefe[contador_factores] = calificacion.toFixed(2) * 1;
				  }

				  else{

					  calificaciones_jefe[contador_factores] = 0
				  }

          if (contador_colaborador != 0){

            calificacion = promedio_colaborador / contador_colaborador;
            calificaciones_colaborador[contador_factores] = calificacion.toFixed(2) * 1;
          }

          else{

            calificaciones_colaborador[contador_factores] = 0
          }

          if (contador_par != 0){

            calificacion = promedio_par / contador_par;
            calificaciones_par[contador_factores] = calificacion.toFixed(2) * 1;
          }

          else{

            calificaciones_par[contador_factores] = 0
          }

          if (contador_psicometricos != 0){

            calificacion = promedio_psicometricos / contador_psicometricos;
            calificaciones_psicometricos[contador_factores] = calificacion.toFixed(2) * 1;
          }

          else{

            calificaciones_psicometricos[contador_factores] = 0
          }
        }

        else{

          calificaciones[contador_factores] = 0;
          calificaciones_autoevaluacion[contador_factores] = 0;
          calificaciones_jefe[contador_factores] = 0;
          calificaciones_colaborador[contador_factores] = 0;
          calificaciones_par[contador_factores] = 0;
          calificaciones_psicometricos[contador_factores] = 0;
        }

				//calificaciones[contador_factores] = parseInt(calificaciones[contador_factores] * 100 / 4);
				contador_factores++;
			}

      var temp_competencias = [];
      var temp_calificaciones = [];
      var temp_calificaciones_autoevaluacion = [];
      var temp_calificaciones_jefe = [];
      var temp_calificaciones_colaborador = [];
      var temp_calificaciones_par = [];
      var temp_calificaciones_psicometricos = [];
      var j = 0;
      var contador_calificaciones = 0;
      var contador_calificaciones_real = 0;
      var total_calificaciones = 0;
      var total_calificacion = 0;

      for (var i = 0;i < calificaciones.length;i++){

        if (calificaciones[i] != 0){

          contador_calificaciones++;
          total_calificaciones += calificaciones[i];
          temp_competencias[j] = competencias[i];
          temp_calificaciones[j] = calificaciones[i] * 100 / 5;
          temp_calificaciones_autoevaluacion[j] = calificaciones_autoevaluacion[i] * 100 / 5;
          temp_calificaciones_jefe[j] = calificaciones_jefe[i] * 100 / 5;
          temp_calificaciones_colaborador[j] = calificaciones_colaborador[i] * 100 / 5;
          temp_calificaciones_par[j] = calificaciones_par[i] * 100 / 5;
          temp_calificaciones_psicometricos[j] = calificaciones_psicometricos[i] * 100 / 5;
          j++;
        }
      }

      if (calificaciones_reales.length > 0){

        total_calificaciones = 0;

        for (var i = 0;i < calificaciones_reales.length;i++){

          if (calificaciones_reales[i] != 0){

            contador_calificaciones_real++;
            total_calificaciones += calificaciones_reales[i];
          }
        }
      }

      if (contador_calificaciones_real > 0){

        total_calificacion = total_calificaciones / contador_calificaciones_real;
      }

      else{

        total_calificacion = total_calificaciones / contador_calificaciones;
      }

      total_calificacion = total_calificacion * 100 / 5;

      $('span.promedio').text(total_calificacion.toFixed(2));

			/*for (var i = contador_factores;i < factores.length;i++){

				calificaciones[i] = 0;
			}*/

      if (regenerar == true){

			var chart = {
      	renderTo: 'container',
      	type: 'column',
      	options3d: {
        	enabled: true,
        	alpha: 0,
        	beta: 0,
        	depth: 50,
        	viewDistance: 25
      	}
   		};

   		var title = {
      	text: ''   
   		};
   
   		var plotOptions = {
      	column: {
        	depth: 25,
        	zones: [
        {
              value: 20,
              color: '#C00000'
            },
            {
              value: 40,
              color: '#FF4343'
            },
            {
              value: 60,
              color: '#FFFF00'
            },
            {
              value: 80,
              color: '#73CB77'
            },
            {
              color: '#008000'
            }
       	]
      	}
   		};

    	var series = [{
    		name: 'PROMEDIO',
      	data: temp_calificaciones,
      	type: 'column'
   		},{
        name: tipos_evaluadores[1],
        data: temp_calificaciones_autoevaluacion,
            //color: '#C00000',
        color: '#000000',
				type: 'line'
       },{
        name: tipos_evaluadores[2],
        data: temp_calificaciones_jefe,
            //color: '#008000',
        color: '#D2691E',
				type: 'line'
       }];

      if (temp_calificaciones_colaborador.length > 0){

        series.push({name: tipos_evaluadores[5], data: temp_calificaciones_colaborador, color: 'purple', type: 'line'});
      }

      if (temp_calificaciones_par.length > 0){

        series.push({name: tipos_evaluadores[3], data: temp_calificaciones_par, color: '#A020F0', type: 'line'});
      }

      if (temp_calificaciones_psicometricos.length > 0){

        series.push({name: tipos_evaluadores[4], data: temp_calificaciones_psicometricos, color: '#FF4343', type: 'line'});
      }
		
			var yAxis = [{
				labels: {
        	style: {
          	fontSize:'12px',
          	fontWeight:'bold'
        	},
        	/*formatter: function(){
       			return this.value + "%";
    			}*/
      	},
      	max: 100,
      	tickInterval: 10
			}];

   		var xAxis = [{
      	categories: temp_competencias,
	  		labels: {
          style: {
            fontSize:'8px',
            fontWeight:'bold'
          },
          step: 1
        },
    	}];     
      
   		var json = {};
   		json.chart = chart;
   		json.title = title;
   		json.series = series;
   		json.plotOptions = plotOptions;
   		json.xAxis = xAxis;
			json.yAxis = yAxis;
   		new Highcharts.Chart(json);
   		var valores = '';

   		/*if (grupo_area != -1){

   			valores += '<td>' + $('select.grupo_areas option[value="' + grupo_area + '"]').text() + '</td>';
   		}

   		else{

   			valores += '<td></td>';
   		}

   		if (nivel_puesto != -1){

   			valores += '<td>' + $('select.niveles_puestos option[value="' + nivel_puesto + '"]').text() + '</td>';
   		}

   		else{

   			valores += '<td></td>';
   		}

   		if (department != -1){

   			valores += '<td>' + $('select.departments option[value="' + department + '"]').text() + '</td>';
   		}

   		else{

   			valores += '<td></td>';
   		}

   		if (puesto != -1){

   			valores += '<td>' + $('select.puestos option[value="' + puesto + '"]').text() + '</td>';
   		}

   		else{

   			valores += '<td></td>';
   		}*/

   		for (var i = 0;i < calificaciones.length;i++){

   			if (isNaN(calificaciones[i])){

   				valores += '<td>0%</td>';
   			}

   			else{

   				valores += '<td>' + calificaciones[i] + '%</td>';
   			}
   		}

   		$('tr.valores').html(valores);

   		/*if (contador > 0){

   			$('table.data-table-grafica').DataTable().destroy();
   		}

   		$('table.data-table-grafica').DataTable({
      	dom: 'rt',
				language: {
		 			'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
      	}*//*,
	  		buttons: [
        {
          extend: 'excel',
					text: 'Exportar a Excel',
					titleAttr: 'Exportar a Excel',
					title: 'Reporte Gráfica'
				}
        ]*/
  		//});

  		contador++;

      var i = 0;

      $('g.highcharts-axis-labels.highcharts-xaxis-labels text').each(function(){

        if (i % 2 != 0){

          $(this).attr('transform', 'translate(0,20)');
        }

        i++;
      });

      $('g.highcharts-yaxis text, text.highcharts-credits').remove();
      }
		}

    function imprimir(nombreDiv){

      $('.oculto').hide();
      $('g.highcharts-yaxis text, text.highcharts-credits').remove();
      var areaImprimir = document.getElementById(nombreDiv);
      html2canvas(areaImprimir).then(function(canvas){
      
        var myImage = canvas.toDataURL("image/png");
        $('#myImage').attr('src', myImage);
        
        setTimeout(function(){
          
          var contenido = document.getElementById('myImageContainer').innerHTML;
          var contenidoOriginal= document.body.innerHTML;
          document.body.innerHTML = contenido;
          window.print();
          document.body.innerHTML = contenidoOriginal;
          $('#myImage').attr('src', '');
          /*$('select.areas').val(area);
          $('select.grupo_areas').val(grupo_area);
          $('select.niveles_puestos').val(nivel_puesto);
          $('select.direcciones').val(direccion);
          $('select.puestos').val(puesto);*/
          $('.oculto').show();
        }, 500);
      });
    }

    function cambio_grupo_area(){

      $('input.departments, input.direcciones, input.niveles_puestos, input.puestos').val('');
      department = direccion = nivel_puesto = puesto = 0;
      puestos_filtrados = [];

      if (grupo_area != 0){

        var i, j, k;

        var temp_departments = [];
        var temp_puestos = [];
        var band = false;

        for(i = 0;i < departments.length;i++){

          if (departments[i].id_grupos_areas == grupo_area){

            temp_departments.push(departments[i]);

            for (j = 0;j < puestos.length;j++){

              if (puestos[j].department_id == departments[i].data){

                puestos_filtrados.push(puestos[j].data);
                band = false;

                for (k = 0;k < temp_puestos.length;k++){

                  if (puestos[j].value == temp_puestos[k].value){

                    band = true;
                  }
                }

                if (!band){ 

                  temp_puestos.push(puestos[j]);
                }
              }
            }
          }
        }

        // Autocomplete 
        $('.departments').autocomplete({
      
          lookup: temp_departments,
          minChars: 0,
          onSelect: function (suggestion){

            department = suggestion.data;
            cambio_departments();
          }
        });

        // Autocomplete 
        $('.puestos').autocomplete({
      
          lookup: temp_puestos,
          minChars: 0,
          onSelect: function (suggestion){

            puesto = suggestion.data;
            nombre_puesto = suggestion.value;
            generar_grafica(true);
          }
        });
      }

      else{

        // Autocomplete 
        $('.departments').autocomplete({
      
          lookup: departments,
          minChars: 0,
          onSelect: function (suggestion){

            department = suggestion.data;
            cambio_departments();
          }
        });

        // Autocomplete 
        $('.puestos').autocomplete({
      
          lookup: puestos_sin_repetir,
          minChars: 0,
          onSelect: function (suggestion){

            puesto = suggestion.data;
            nombre_puesto = suggestion.value;
            generar_grafica(true);
          }
        });
      }

      generar_grafica(true);
    }

    function cambio_nivel_puesto(){

      $('input.grupo_areas, input.departments, input.direcciones, input.puestos').val('');
      grupo_area = department = direccion = puesto = 0;
      puestos_filtrados = [];

      if (nivel_puesto != 0){

        var temp_puestos = [];
        var band = false;

        for(var i = 0;i < puestos.length;i++){

          if (puestos[i].id_nivel_puesto == nivel_puesto){

            puestos_filtrados.push(puestos[i].data);
            band = false;

            for (var j = 0;j < temp_puestos.length;j++){

              if (puestos[i].value == temp_puestos[j].value){

                band = true;
              }
            }

            if (!band){

              temp_puestos.push(puestos[i]);
            }
          }
        }

        // Autocomplete 
        $('.puestos').autocomplete({
      
          lookup: temp_puestos,
          minChars: 0,
          onSelect: function (suggestion){

            puesto = suggestion.data;
            nombre_puesto = suggestion.value;
            generar_grafica(true);
          }
        });
      }

      else{

        // Autocomplete 
        $('.puestos').autocomplete({
      
          lookup: puestos_sin_repetir,
          minChars: 0,
          onSelect: function (suggestion){

            puesto = suggestion.data;
            nombre_puesto = suggestion.value;
            generar_grafica(true);
          }
        });
      }

      generar_grafica(true);
    }

    function cambio_departments(){

      $('input.niveles_puestos, input.puestos').val('');
      nivel_puesto = puesto = 0;
      puestos_filtrados = [];
        
      if (department != 0){

        var temp_puestos = [];
        var band = false;

        for(var i = 0;i < puestos.length;i++){

          if (puestos[i].department_id == department){

            puestos_filtrados.push(puestos[i].data);
            band = false;

            for (var j = 0;j < temp_puestos.length;j++){

              if (puestos[i].value == temp_puestos[j].value){

                band = true;
              }
            }

            if (!band){ 

              temp_puestos.push(puestos[i]);
            }
          }
        }

        // Autocomplete 
        $('.puestos').autocomplete({
      
          lookup: temp_puestos,
          minChars: 0,
          onSelect: function (suggestion){

            puesto = suggestion.data;
            nombre_puesto = suggestion.value;
            generar_grafica(true);
          }
        });
      }

      else{

        // Autocomplete 
        $('.puestos').autocomplete({
      
          lookup: puestos_sin_repetir,
          minChars: 0,
          onSelect: function (suggestion){

            puesto = suggestion.data;
            nombre_puesto = suggestion.value;
            generar_grafica(true);
          }
        });
      }

      generar_grafica(true);
    }

    function cambio_direcciones(){

      $('input.departments, input.niveles_puestos, input.puestos').val('');
      department = nivel_puesto = puesto = 0;
      puestos_filtrados = [];
      var i, j, k;

      if (direccion != 0){

        var temp_departments = [];
        var temp_puestos = [];
        var band = false;

        for(i = 0;i < departments.length;i++){

          if (departments[i].direction_id == direccion){

            temp_departments.push(departments[i]);

            for(j = 0;j < puestos.length;j++){

              if (puestos[j].department_id == departments[i].data){

                puestos_filtrados.push(puestos[j].data);
                band = false;

                for (k = 0;k < temp_puestos.length;k++){

                  if (puestos[j].value == temp_puestos[k].value){

                    band = true;
                  }
                }

                if (!band){

                  temp_puestos.push(puestos[j]);
                }
              }
            }
          }
        }

        // Autocomplete 
        $('.departments').autocomplete({
      
          lookup: temp_departments,
          minChars: 0,
          onSelect: function (suggestion){

            department = suggestion.data;
            cambio_departments();
          }
        });

        // Autocomplete 
        $('.puestos').autocomplete({
      
          lookup: temp_puestos,
          minChars: 0,
          onSelect: function (suggestion){

            puesto = suggestion.data;
            nombre_puesto = suggestion.value;
            generar_grafica(true);
          }
        });
      }

      else{

        // Autocomplete 
        $('.departments').autocomplete({
      
          lookup: departments,
          minChars: 0,
          onSelect: function (suggestion){

            department = suggestion.data;
            cambio_departments();
          }
        });

        // Autocomplete 
        $('.puestos').autocomplete({
      
          lookup: puestos_sin_repetir,
          minChars: 0,
          onSelect: function (suggestion){

            puesto = suggestion.data;
            nombre_puesto = suggestion.value;
            generar_grafica(true);
          }
        });
      } 

      generar_grafica(true);
    }
	</script>
@endsection