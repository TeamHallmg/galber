@extends('layouts.app')

@section('title', 'Reporte Evaluación Cualitativa')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
		
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Reportes
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
  

<?php $exportar_objetivos = false?>

@if (count($periodos) > 0)



<div class="row"> 
	<div class="col-12 offset-md-4 col-md-3 text-center">
		<div class="form-group">
			<label for="id_plan" class="font-weight-bolder mb-0">Periodo:</label>							
			<select class="form-control periodos">
				<?php foreach ($periodos as $key => $periodo){ ?>
													
					<option value="<?php echo $periodo->id?>" <?php if ($id_periodo == $periodo->id){ ?>selected="selected"<?php } ?>><?php echo $periodo->descripcion?></option>
		<?php } ?>
			</select>
		</div>
	</div> 
</div>


	@if (Auth::user()->isAdminOrHasRolePermission('performance_evaluation_admin') || auth()->user()->role == 'supervisor')

	<div class="row mb-3">
		<div class="offset-md-4 col-md-4 text-center">
			<a class="btn btn-primary w-75" href="/ponderaciones">Ponderaciones</a>
		</div>
	</div>
	
	@endif
	


@if (!empty($users))

<div class="row mb-5">
	@if (Auth::user()->isAdminOrHasRolePermission('performance_evaluation_admin') || auth()->user()->role == 'supervisor')
	<!--<div class="col-md-4 text-center">
		<a class="btn btn-primary" href="/reporte-graficas/{{$id_periodo}}">Reporte Gráfica</a>
	</div>-->
	<!--<div class="col-md-3 text-center">
		<a class="btn btn-primary" href="/reporte-graficas-competencias/{{$id_periodo}}">Reporte Competencias</a>
	</div>-->
	<div class="col-md-6 text-center">
		<a class="btn btn-primary" href="/reporte-sabana/{{$id_periodo}}">Reporte Sabana</a>
	</div>
	@endif

	@if (Auth::user()->isAdminOrHasRolePermission('performance_evaluation_admin') || auth()->user()->role == 'supervisor')
	
	<div class="col-md-6 text-center">
		<a class="btn btn-primary" href="/reporte-detalle/{{$id_periodo}}">Reporte Detallado</a>
	</div>
	@else

<?php $evaluaciones_jefe = true; ?>

		@for ($i = 0;$i < count($users);$i++)

			@if ($users[$i]->status != 'Terminada' && $users[$i]->id != $users[$i]->id_evaluador)

	<?php $evaluaciones_jefe = false;
				break; ?>
			@endif
		@endfor
	@endif
</div>

@if ((Auth::user()->isAdminOrHasRolePermission('performance_evaluation_admin') || auth()->user()->role == 'supervisor') && $retro)

<div class="row mb-5">
	<div class="col-md-4 text-center">
		<a class="btn btn-primary" href="/reporte-usuarios-competencias-acciones/{{$id_periodo}}">Exportar Usuarios con Acciones</a>
	</div>
	<div class="col-md-4 text-center">
		<a class="btn btn-primary" href="/reporte-competencias-acciones/{{$id_periodo}}">Exportar Competencias con Acciones</a>
	</div>
	<div class="col-md-4 text-center">
		<a class="btn btn-primary" href="/reporte-retroalimentacion/{{$id_periodo}}">Exportar Usuarios con Retroalimentación</a>
	</div>
</div>
@endif

<div class="row exportar_objetivos margin-top-20" style="display: none">
	<div class="col-md-12">
		<a class="btn btn-primary" href="/reporte-objetivos/{{$id_periodo}}">Exportar Objetivos Acordados</a>
	</div>
</div>

<div class="row margin-top-20">
	<div class="col-md-12">
		<table width="100%" class="table table-striped table-bordered usuarios_informe">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th class="text-center">ID</th>
					<th>Nombre</th>
					<th>Puesto</th>
					<th>Departamento</th>
					<th>Jefe</th>
					<th class="text-center">Detalle</th>
				</tr>
			</thead>
			<tbody>

<?php $actual_usuario = 0;
      $clase_boton = 'btn-success'; ?>

	@for ($i = 0;$i < count($users);$i++)

  	@if ($actual_usuario != $users[$i]->id)

  		@if ($actual_usuario != 0)

  				<td class="text-center">
						<button type="button" class="btn {{$clase_boton}} informe" style="width: 100%">{{($clase_boton == 'btn-success' ? 'Ver Reporte' : ($clase_boton == 'btn-primary' ? 'Retro' : 'En Proceso'))}}</button>
						<form action="/reporte-desempeno/{{$actual_usuario}}" method="post" class="ver_reporte">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="id_periodo" class="id_periodo" value="<?php echo $id_periodo?>">
						@if ($clase_boton != 'btn-danger')
							<input type="hidden" name="terminado" value="1">
						@endif
							<button type="submit" style="display: none">Ver Reporte</button>
						</form>
					</td>
				</tr>
			@endif

				<tr>
					<td class="text-center">{{$users[$i]->idempleado}}</td>
					<td class="text-left">{{str_replace('Ã‘','Ñ',$users[$i]->first_name)}} {{str_replace('Ã‘','Ñ',$users[$i]->last_name)}}</td>
					<td class="text-left">{{(!empty($users[$i]->job_position) ? str_replace('Ã‘','Ñ',$users[$i]->job_position) : '')}}</td>
					<td class="text-left">{{(!empty($users[$i]->department) ? str_replace('Ã‘','Ñ',$users[$i]->department) : '')}}</td>
					<td class="text-left">{{str_replace('Ã‘','Ñ',$users[$i]->boss)}}</td>

	<?php $actual_usuario = $users[$i]->id;
				$clase_boton = 'btn-success'; ?>

		@endif

<?php if ($users[$i]->status != 'Terminada'){

					$clase_boton = 'btn-danger';
			}

			else{
					
				if (!empty($users[$i]->comentario) && !empty($users[$i]->objetivo)){
					
					$clase_boton = 'btn-primary';

					if (!$exportar_objetivos && !empty(auth()->user()->employee) && $users[$i]->jefe == auth()->user()->employee->idempleado){

						$exportar_objetivos = true;
					}
				}
			} ?>
					
	@endfor

	@if ($actual_usuario != 0)

  				<td class="text-center" nowrap>
						<button type="button" class="btn {{$clase_boton}} informe" style="width: 100%">{{($clase_boton == 'btn-success' ? 'Ver Reporte' : ($clase_boton == 'btn-primary' ? 'Retro' : 'En Proceso'))}}</button>
						<form action="/reporte-desempeno/{{$actual_usuario}}" method="post" class="ver_reporte">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="id_periodo" class="id_periodo" value="<?php echo $id_periodo?>">
							<button type="submit" style="display: none">Ver Reporte</button>
						</form>
					</td>
				</tr>
	@endif

			</tbody>
		</table>
	</div>
</div>

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20 no_subordinados">No cuenta con colaboradores en la evaluación del periodo.</h4>
	</div>
</div>
@endif

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20">No hay periodos abiertos.</h4>
	</div>
</div>
@endif

<form action="/reporte-desempeno" method="post" class="reporte_desempeno">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="id_periodo" class="id_periodo_reporte">
	<input type="submit" style="display: none">
</form>
	</div>
</div>
</div>
</div>
@endsection

@section('scripts')
<script>

		var exportar_objetivos = <?php echo ($exportar_objetivos == true ? '1' : '0')?>;
		
		$(document).ready(function(){

			// Se deben exportar objetivos
			if (exportar_objetivos == '1'){

				// Se muestra el boton
				$('.exportar_objetivos').show();
			}

			// Remover el boton para exportar
			else{

				$('.exportar_objetivos').remove();
			}

			$('.usuarios_informe').DataTable({
			language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
  	});

   		$('select.periodos').change(function(){

   			$('.id_periodo_reporte').val($(this).val());
   			$('form.reporte_desempeno').submit();
   		});

			$('body').on('click', 'button.informe', function(){

   			$('.id_periodo').val($('select.periodos').val());
   			$(this).next().submit();
   		});
		});
	</script>
@endsection