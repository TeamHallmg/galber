@extends('layouts.app')

@section('title', 'Reporte Consolidado')

@section('content')
<?php $results = array();
			$factors = array(); ?>
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('/img/evaluacion_desempeno.png') }}" alt="">
	</div>
</div>
<div class="row mt-4">
	<div class="col">
		<h1 class="text-center titulos-evaluaciones font-weight-bold">Reporte Consolidado</h1>
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-md-12 text-center">
		<!--<div id="myImageContainer">
			<img id="myImage">
		</div>-->
	@if ($user[0]->boss_id == auth()->user()->id  || Auth::user()->isAdminOrHasRolePermission('performance_evaluation_admin') || auth()->user()->role == 'supervisor' || auth()->user()->role == 'director')
		<button class="btn btn-primary print_report completo" style="opacity: 0<?php if (!Auth::user()->isAdminOrHasRolePermission('performance_evaluation_admin') && (count($comentario) == 0 || count($objetivos_entregables_desempeno) == 0)){ ?>;display: none<?php } ?>"><i class="fas fa-print"></i> Imprimir Reporte Completo</button>
	@endif
	</div>
</div>
<div class="row" id="areaImprimir">
	<div class="text-center">
		<h1 class="text-center mostrar font-weight-bold" style="display: none; margin-top: 0px">Reporte Consolidado</h1>
	</div>
	<div class="col-md-12">
		<div class="row">
		<div class="col-md-8 datos_evaluado mt-5">
			<table class="table-fluid tabla_evaluado" border="1" width="100%" style="font-size: 14px">
				<tbody>
					<tr>
						<td>EVALUADO:</td>
						<td class="text-center">
							<b>{{str_replace('Ã‘','Ñ',$user[0]->first_name)}} {{str_replace('Ã‘','Ñ',$user[0]->last_name)}}</b>
						</td>
					</tr>
					<tr>
						<td>NO. EMPLEADO:</td>
						<td class="text-center">
							<b>{{$user[0]->id}}</b>
						</td>
					</tr>
					<tr>
						<td>PUESTO:</td>
						<td class="text-center">
							<b>{{(!empty($user[0]->subdivision) ? str_replace('Ã‘','Ñ',$user[0]->subdivision) : '')}}</b>
						</td>
					</tr>
					<tr>
						<td>FECHA:</td>
						<td class="text-center">
							<b>{{$meses[date('m') * 1] . '-' . date('y')}}</b>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-4 text-center datos_logo mt-5">
			<img src="{{ asset('img/logo.png') }}" class="img-fluid" style="position: relative; top: -10px">
		</div>
		</div>
	</div>
	<div class="mt-5 col-md-12">
		<div class="row mb-5">
			<div class="col-md-5 datos_competencias">
				<table class="table-fluid tabla_competencias" border="1" width="100%">
					<thead>
						<tr>
							<th>Competencias</th>
							<th class="text-center">Promedio</th>
						</tr>
					</thead>
					<tbody>

<?php $actual_factor = 0;
		$contador = 0;
		$contador_factores = 0;
		$total = 0;
		$total_factor = 0;
		$promedio = 0;
		$comentarios = array();

		foreach ($resultados as $key => $value){
			
			if ($actual_factor != $value->id_factor){
					
				$factors[] = $value->nombre;

				if ($actual_factor != 0){

					$contador_factores++;

					if ($contador == 0){

						$contador = 1;
					}

					$promedio = $total_factor / $contador;
					$total += $promedio;
					$results[] = number_format($promedio, 2, '.', ',') * 1; ?>

							<td class="text-center" style="color: white; background-color: {{($promedio >= 3.5 ? '#008000' : ($promedio >= 3 ? '#73CB77' : ($promedio >= 2.5 ? '#FFFF00; color: black' : ($promedio >= 2 ? '#FF4343' : '#C00000'))))}}"><b>{{number_format($promedio, 2, '.', ',')}}</b></td>
						</tr>
	<?php }

				$contador = 0;
				$actual_factor = $value->id_factor;
				$total_factor = 0; ?>

						<tr>
							<td>{{$value->nombre}}</td>
<?php }

			if ((empty($value->nombre_evaluacion) || $value->nombre_evaluacion != 'SUBORDINADO') && $value->boss_id != $user[0]->id){

				$total_factor += $value->nivel_desempeno;
				$contador++;

				if (!empty($value->comentario)){

	      	if (empty($comentarios[$value->nombre])){

	      		$comentarios[$value->nombre] = $value->comentario;
	      	}

	      	else{

	      		$comentarios[$value->nombre] .= '*' . $value->comentario;
	      	}
	    	}
			}

			else{

				$user_id = auth()->user()->id;

				if ($user_id == 26 || $user_id == 1){

					$total_factor += $value->nivel_desempeno;
					$contador++;

					if (!empty($value->comentario)){

	      		if (empty($comentarios[$value->nombre])){

	      			$comentarios[$value->nombre] = $value->comentario;
	      		}

	      		else{

	      			$comentarios[$value->nombre] .= '*' . $value->comentario;
	      		}
	    		}
				}
			}
		}

		if ($actual_factor != 0){

			$contador_factores++;

			if ($contador == 0){

				$contador = 1;
			}

			$promedio = $total_factor / $contador;
			$results[] = number_format($promedio, 2, '.', ',') * 1;
			$total += $promedio; ?>

							<td class="text-center" style="color: white; background-color: {{($promedio >= 3.5 ? '#008000' : ($promedio >= 3 ? '#73CB77' : ($promedio >= 2.5 ? '#FFFF00; color: black' : ($promedio >= 2 ? '#FF4343' : '#C00000'))))}}"><b>{{number_format($promedio, 2, '.', ',')}}</b></td>
						</tr>
<?php }

		if ($contador_factores == 0){

			$contador_factores = 1;
		}

		$alto_grafica = 40 * (1 + $contador_factores);
		$promedio = $total / $contador_factores; ?>

						<tr>
							<td>RESULTADO GLOBAL</td>
							<td class="text-center" style="color: white; background-color: {{($promedio >= 3.5 ? '#008000' : ($promedio >= 3 ? '#73CB77' : ($promedio >= 2.5 ? '#FFFF00; color: black' : ($promedio >= 2 ? '#FF4343' : '#C00000'))))}}"><b>{{number_format($total / $contador_factores, 2, '.', ',')}}</b></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-7 margin-top-20 datos_grafica">
				<h4 class="text-center titulos-evaluaciones font-weight-bold" style="margin-top: 0">Resultados</h4>
				<div id="container" style="width: 100%; margin: 0 auto; padding: 0 20px 0 0" class="oculto"></div>
				<div id="myImageContainer" class="mostrar" style="display: none">
					<img id="myImage" width="100%">
				</div>
			</div>
		</div>

		@if (!empty($comentarios))

		<div class="oculto2 mb-5">
			<div class="text-center titulos-evaluaciones font-weight-bold" style="font-size: 20px">Comentarios u Observaciones</div>
			<table class="table-fluid comentarios_reporte" border="1" width="100%">
				<tbody>

<?php $contador = 0;

			foreach ($comentarios as $key => $value){ ?>

					<tr>

				@if ($contador == 0)

						<td rowspan="{{count($comentarios)}}" class="text-center">
							<h4>COMPETENCIAS</h4>
						</td>
				@endif

	<?php $contador++?>

						<td class="text-center">{{$contador}}</td>
						<td class="text-center">{{$key}}</td>
						<td class="text-center">

	<?php $comments = explode('*', $value)?>

				@for ($i = 0;$i < count($comments);$i++)	

							<p style="margin: 5px 0; {{($i > 0 ? 'border-top: 1px solid gray' : '')}}">{{$comments[$i]}}</p>
				@endfor

						</td>
					</tr>
<?php	} ?>

				</tbody>
			</table>
		</div>
		@endif
		
	</div>
</div>
<iframe frameborder="0" width="0" height="0" name="response"></iframe>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.5/bluebird.min.js"></script>
@endsection

@section('scripts')
	<script type="text/javascript">

		var resultados = <?php echo json_encode($resultados)?>;
		var factors = <?php echo json_encode($factors)?>;
		var results = <?php echo json_encode($results)?>;
		var user = <?php echo json_encode($user[0])?>;
		var alertar = false;
		var abandonar_pagina = false;
		var canEdit = <?php echo ($user[0]->boss_id == auth()->user()->id && $evaluaciones_terminadas ? '1' : '0')?>;
		var modalidad = <?php echo $modalidad?>;
		var id = <?php echo auth()->user()->id?>;
		var highchart = 0;

		$(document).ready(function(){

			var autoevaluacion = [], jefe = [], colaborador = [], par = [], cliente = [];
			var contador_autoevaluacion = 0, contador_jefe = 0, contador_colaborador = 0, contador_par = 0, contador_cliente;
			var factor_colaborador = '', factor_par = '', factor_cliente = '', factor_jefe = '';
			var evaluciones_colaborador = 0, evaluciones_par = 0, evaluaciones_cliente = 0, evaluaciones_jefe = 0, total_colaborador = 0, total_par = 0, total_cliente = 0, total_jefe = 0;

			if (window.navigator.userAgent.indexOf("Edge") > -1){

				$('table.objetivos_entregables th, table.objetivos_entregables textarea').css('border', '1px solid grey');
				$('table.objetivos_entregables').removeAttr('border');
			}

			//if ($('div.datos_firmas').length != 0){

			for(var i = 0;i < resultados.length;i++){

				if (user.id == resultados[i].id_evaluador){

					while (resultados[i].nombre != factors[contador_autoevaluacion]){

						autoevaluacion[contador_autoevaluacion] = 0;
						contador_autoevaluacion++;
					}

					autoevaluacion[contador_autoevaluacion] = resultados[i].nivel_desempeno * 1;
					contador_autoevaluacion++;
				}

				else{

					if (resultados[i].nombre_evaluacion == 'JEFE' || user.boss_id == resultados[i].id_evaluador){

						if (factor_jefe == resultados[i].nombre){

							contador_jefe--;
							evaluaciones_jefe++;
							total_jefe += resultados[i].nivel_desempeno;
							jefe[contador_jefe] = (total_jefe / evaluaciones_jefe).toFixed(1) * 1;
							contador_jefe++;
						}

						else{

							while (resultados[i].nombre != factors[contador_jefe]){

								jefe[contador_jefe] = 0;
								contador_jefe++;
							}

							total_jefe = resultados[i].nivel_desempeno * 1
							jefe[contador_jefe] = total_jefe;
							factor_jefe = resultados[i].nombre;
							contador_jefe++;
							evaluaciones_jefe = 1;
						}
					}

					else{

						if (modalidad > 2 && (resultados[i].nombre_evaluacion == 'SUBORDINADO' || user.id == resultados[i].boss_id)){

							if (factor_colaborador == resultados[i].nombre){

								contador_colaborador--;
								evaluciones_colaborador++;
								total_colaborador += resultados[i].nivel_desempeno;
								colaborador[contador_colaborador] = (total_colaborador / evaluciones_colaborador).toFixed(1) * 1;
								contador_colaborador++;
							}

							else{

								while (resultados[i].nombre != factors[contador_colaborador]){

									colaborador[contador_colaborador] = 0;
									contador_colaborador++;
								}
								
								total_colaborador = resultados[i].nivel_desempeno * 1;
								colaborador[contador_colaborador] = total_colaborador;
								factor_colaborador = resultados[i].nombre;
								contador_colaborador++;
								evaluciones_colaborador = 1;
							}
						}

						else{

							if (modalidad > 1 && (resultados[i].nombre_evaluacion == 'PARES' || user.boss_id == resultados[i].boss_id)){
								
								if (factor_par == resultados[i].nombre){

									contador_par--;
									evaluciones_par++;
									total_par += resultados[i].nivel_desempeno;
									par[contador_par] = (total_par / evaluciones_par).toFixed(1) * 1;
									contador_par++;
								}

								else{

									while (resultados[i].nombre != factors[contador_par]){

										par[contador_par] = 0;
										contador_par++;
									}
									
									total_par = resultados[i].nivel_desempeno * 1;
									par[contador_par] = total_par;
									factor_par = resultados[i].nombre;
									contador_par++;
									evaluciones_par = 1;
								}
							}

							else{

								if (modalidad == 4){

									if (factor_cliente == resultados[i].nombre){

										contador_cliente--;
										evaluaciones_cliente++;
										total_cliente += resultados[i].nivel_desempeno;
										cliente[contador_cliente] = (total_cliente / evaluaciones_cliente).toFixed(1) * 1;
										contador_cliente++;
									}

									else{

										while (resultados[i].nombre != factors[contador_cliente]){

											cliente[contador_cliente] = 0;
											contador_cliente++;
										}
									
										total_cliente = resultados[i].nivel_desempeno * 1;
										cliente[contador_cliente] = total_cliente;
										factor_cliente = resultados[i].nombre;
										contador_cliente++;
										evaluaciones_cliente = 1;
									}
								}
							}
						}
					}		
				}
			}

			for (var i = contador_autoevaluacion;i < factors.length;i++){

				autoevaluacion[i] = 0;
			}

			for (var i = contador_jefe;i < factors.length;i++){

				jefe[i] = 0;
			}

			for (var i = contador_colaborador;i < factors.length;i++){

				colaborador[i] = 0;
			}

			for (var i = contador_par;i < factors.length;i++){

				par[i] = 0;
			}

			for (var i = contador_cliente;i < factors.length;i++){

				cliente[i] = 0;
			}
   
   		var chart = {
      	renderTo: 'container',
      	options3d: {
        	enabled: true,
        	alpha: 0,
        	beta: 0,
        	depth: 50,
        	viewDistance: 25
      	},
      	printWidth: 900
   		};

   		var title = {
      	text: ''   
   		};

   		var legend = {
        align: 'center',
        verticalAlign: 'bottom',
        layout: 'horizontal',
				floating: true,
				y: 34
      };
   
   		var plotOptions = {
      	column: {
        	depth: 25
      	}
   		};

    	var series = [{
      	name: 'PROMEDIO',
            data: results,
            color: '#004A91',
			type: 'column',
        },{
            name: 'AUTOEVALUACION',
            data: autoevaluacion,
            //color: '#C00000',
            color: '#000000',
			type: 'line',
        },{
        	name: 'JEFE',
            data: jefe,
            //color: '#008000',
            color: '#D2691E',
			type: 'line',
        }
      ];
      
      if (modalidad > 2){

      	series.push({name: 'COLABORADOR', data: colaborador, color: 'purple', type: 'line'});
      }

      /*if (modalidad > 1){

      	series.push({name: 'PAR', data: par, color: '#A020F0', type: 'line'});
      }*/

      if (modalidad == 4){

      	series.push({name: 'CLIENTE', data: cliente, color: '#008000', type: 'line'});
      }
        /*,{
        	name: 'COLABORADOR',
            data: colaborador,
            color: '#FFA500',
			type: 'line',
        },{
        	name: 'PAR',
          data: par,
          color: '#A020F0',
		  type: 'line',
        }*/
		
			var yAxis = [{
				labels: {
        	style: {
          	fontSize:'12px',
          	fontWeight:'bold'
        	}
      	},
      	max: 4,
      	tickInterval: 0.5
			}];

   		var xAxis = [{
      	categories: factors,
	  		labels: {
          style: {
            fontSize:'8px',
            fontWeight:'bold'
          },
          step: 1
        }
    	}];     
      
   		var json = {};
   		json.chart = chart;
   		json.title = title;
   		json.series = series;
   		json.plotOptions = plotOptions;
   		json.xAxis = xAxis;
			json.yAxis = yAxis;
			json.legend = legend;
   		highchart = new Highcharts.Chart(json);
   		//var alto = (alto_grafica > 210 ? alto_grafica : 210);

   		$('body').on('click', '.print_report', function(){

   			if ($(this).hasClass('completo')){

   				printDiv('areaImprimir', 2);
   			}

   			else{

   				printDiv('areaImprimir', 1);
   			}
   		});

			// Evento que muestra una alerta cuando se quiere abandonar la pagina
			window.onbeforeunload = function(e){

				// No hay cambios sin guardar
				if (!alertar){

					// No se muestra la alerta
					return null;
				}

  			// Se desea abandonar la página
  			abandonar_pagina = true;

  			// Se debe retornar algo diferente a null para que se muestre la alerta antes de abanadonar la página
  			return 'algo';
			};

			// El navegador no es Edge
			if (window.navigator.userAgent.indexOf("Edge") == -1){

				// Se crea un intérvalo que corre cada medio segundo
				setInterval(function(){

					// Se desea abandonar la pagina y debe mostrarse la alerta
					if (abandonar_pagina && alertar){

						abandonar_pagina = false;
					
						// Se muestra una segunda alerta con el mensaje correcto
						alert("¿Quieres salir de la página?\n\nSi sales de la página los cambios no se guardarán.");
					}
				}, 500);
			}

			var i = 0;

			$('g.highcharts-axis-labels.highcharts-xaxis-labels text').each(function(){

				if (i % 2 != 0){

					$(this).attr('transform', 'translate(0,19)');
				}

				i++;
			});

			var alto_contenedor = $('div.highcharts-container').height() + 28;
			$('div.highcharts-container').css('height', alto_contenedor + 'px');
			$('div.highcharts-container svg').attr('height', alto_contenedor);
			$('g.highcharts-yaxis text, text.highcharts-credits').remove();
			//$('g.highcharts-legend').attr('transform', 'translate(115,385)');
			//}

			setTimeout(function(){

				html2canvas(document.getElementById("container")).then(function(canvas){
					
					var img = canvas.toDataURL("image/png");
					$('#myImageContainer #myImage').attr('src', img);
					$('button.print_report').css('opacity', 1);
				});
			}, 2000);
   	});

		function printDiv(nombreDiv, completo){
			
			//$('#container').css('position', 'fixed');
			// Se oculta todo lo que no debe imprimirse
			$('.oculto').hide();

			// Se va a imprimir el resumen del reporte
			if (completo == 1){

				// Se ocultan los comentarios y retroalimentación
				$('.oculto2').hide();
			}

			//var output = $('textarea.retro').val().replace(/\r?\n/g,'<br/>');
			//$('textarea.retro').next().html(output);
			$('.mostrar').show();
			//$('html').css('margin', 0);
    	//$('body').css('margin', 0);
    	//$('.highcharts-container').attr('height', '350');
    	//$('.highcharts-container').css('height', '350px');

    	if (navigator.userAgent.indexOf('Trident') > -1){

    		//var anchura = $('.highcharts-container').attr('width');
				//var altura = $('.highcharts-container').attr('height');
				//$('.highcharts-container').attr('width', '700');
				//$('.highcharts-container svg').attr('width', '700');
				//$('.highcharts-container').attr('height', '388');
				//$('.highcharts-container svg').attr('height', '388');
				//$('.highcharts-container').css('overflow', 'visible');
    		/*$('.datos_evaluado').addClass('estilos_evaluado');
    		$('.datos_logo').addClass('estilos_logo');
    		$('.datos_competencias').addClass('estilos_competencias');
    		$('.datos_grafica').addClass('estilos_grafica');
    		$('.datos_firmas').addClass('estilos_firmas');
				$('#' + nombreDiv).print();
				$('.datos_evaluado').removeClass('estilos_evaluado');
    		$('.datos_logo').removeClass('estilos_logo');
    		$('.datos_competencias').removeClass('estilos_competencias');
    		$('.datos_grafica').removeClass('estilos_grafica');
    		$('.datos_firmas').removeClass('estilos_firmas');*/
    		$('.oculto').show();

    		// Se muestran los comentarios y retroalimentación
				$('.oculto2').show();

				$('.mostrar').hide();
				//$('#container').css('position', 'relative');

				//setTimeout(function(){
			
					//$('.highcharts-container').attr('width', anchura);
					//$('.highcharts-container svg').attr('width', anchura);
					//$('.highcharts-container').attr('height', altura);
					//$('.highcharts-container svg').attr('height', altura);
				//}, 4000);
    	}

    	else{

    		// El navegador Edge
    		if (window.navigator.userAgent.indexOf("Edge") > -1){

    			// Agrega un borde a las celdas de la tabla de comentarios u observaciones, a la tabla de competencias y a la de los datos del evaluado
    			$('table.comentarios_reporte tbody tr td, table.tabla_competencias tbody tr td, table.tabla_evaluado tbody tr td, table.objetivos_entregables tbody tr td').css('border', '1px solid grey');

    			// Quita el atributo border de la tabla de comentarios u observaciones, de la de competencias y de la de los datos del evaluado
    			$('table.comentarios_reporte, table.tabla_competencias, table.tabla_evaluado, table.objetivos_entregables').removeAttr('border');
    		}

    		//var areaImprimir = document.getElementById(nombreDiv);
				/*html2canvas(document.getElementById("container")).then(function(canvas){
					var img = canvas.toDataURL("image/png");
					$('#myImageContainer #myImage').attr('src', img);
				
					//setTimeout(function(){*/
					
						var contenido = document.getElementById('areaImprimir').innerHTML;
     				var contenidoOriginal= document.body.innerHTML;
						document.body.innerHTML = contenido;
						window.print();
						document.body.innerHTML = contenidoOriginal;
						//$('#myImage').attr('src', '');
						$('.oculto').show();

						// Se muestran los comentarios y retroalimentación
						$('.oculto2').show();

						$('.mostrar').hide();
						//$('#container').css('position', 'relative');

						// El navegador Edge
						if (window.navigator.userAgent.indexOf("Edge") > -1){

    					// Agrega el atributo border con valor de 1 a la tabla de comentarios u observaciones, a la de competencias y a la de los datos del evaluado
    					$('table.comentarios_reporte, table.tabla_competencias, table.tabla_evaluado, table.objetivos_entregables').attr('border', 1);
    				}
					//}, 3000);
				//});
			}
		}
	</script>
@endsection
