@extends('layouts.app')

@section('title', 'Reporte Evaluación Cualitativa')

@section('content')
<style>div.dt-buttons {
	float: right;
	margin-left:10px;
	display: none;
	}</style>
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/evaluacion_desempeno.png" alt="">
		
		
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Reporte Evaluación Cualitativa
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
 
		 
@if (!empty($periodos))

<div class="row"> 
	<div class="col-12 offset-md-4 col-md-3 text-center">
		<div class="form-group">
			<label for="id_plan" class="font-weight-bolder mb-0">Periodo:</label>							
			<select class="form-control periodos">
				<?php foreach ($periodos as $key => $periodo){ ?>
			                                      	
					<option value="<?php echo $periodo->id?>" <?php if (!empty($id_periodo) && $id_periodo == $periodo->id){ ?>selected="selected"<?php } ?>><?php echo $periodo->descripcion?></option>
		  <?php } ?>
			</select>
		</div>
	</div> 
</div>



@if (!empty($resultados))
<div class="card border-0">
	<h5 class="card-header bg-primary text-white font-weight-bolder">Resultados
		<button class="btn btn-success float-right text-left mr-2 btn-excel" title="Exportar a Excel">
			<i class="fa fa-file-excel"></i> Exportar a Excel
		</button>
	</h5>
	<div class="card-body px-0"> 

		<div class="row mb-2">
 
			<div class="col-md-8"></div>
			<div class="col-md-4">
				<select id="field" class="form-control">
				<option value="-1">-- Columna en cual buscar --</option>
				<option value="1">Evaluado</option>
				<option value="2">Dirección</option>
				<option value="3">Departamento</option>
				<option value="4">Nombre jefe Directo</option>
				</select>
			</div>
		</div>

		<div class="row margin-top-20">
			<div class="col-md-12">
				<table width="100%" class="data-table table table-striped table-bordered table-hover">
					<thead style="background-color: #222B64; color:white;">
						<tr>
							<th>ID</th>
							<th>Evaluado</th>
							<th>Dirección</th>
							<th>Departamento</th>
							<th>Nombre jefe Directo</th>
							<th>Promedio {{$tipos_evaluadores[2]}}</th>
							<th>Promedio {{$tipos_evaluadores[3]}}</th>
							<th>Promedio {{$tipos_evaluadores[5]}}</th>
							<th>Promedio {{$tipos_evaluadores[4]}}</th>
							<th>Promedio {{$tipos_evaluadores[1]}}</th>
							<th>Retro</th>

			<?php foreach ($factores as $key => $factor){ ?>
				
							<th>{{$factor->nombre}}</th>
			<?php } ?>
							
							<!--<th>Promedio</th>-->
							<th>Promedio Ponderado</th>
						</tr>
					</thead>
					<tbody>

		<?php 	$actual_usuario = 0;
						$total = 0;
						$promedio = 0;
						$contador_factores = 0;
						$actual_columna = 0;
						$i = 0;
						$j = 0;
						$autoevaluacion = $jefe = $colaborador = $par = $psicometricos = false;
						$promedio_ponderado = 0;
						$promedio_ponderado_final = 0;

						foreach ($resultados as $key => $resultado){

							if ($actual_usuario != $resultado->id_evaluado){

								if ($actual_usuario != 0){

									for ($i = $actual_columna;$i < count($factores);$i++){
								
										if ($factores[$i]->id == $resultado->id_factor){

											break;
										} ?>

							<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
						<?php }

									//$total = $promedio / $contador_factores;
									//$total = $promedio_ponderado_final;

									//if ($total != 0){ ?>

							<!--<td class="text-center" style="vertical-align: middle; font-weight: bold; font-size: 20px; border: 1px solid #DDD; background-color: {{($total >= 80 ? '#008000' : ($total >= 60 ? '#73CB77' : ($total >= 40 ? '#FFFF00' : ($total >= 20 ? '#FF4343' : '#C00000'))))}}">{{number_format($total, 1, '.', ',')}}</td>-->

						<?php //}

									//else{ ?>


							<!--<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>-->

						<?php //}

									if ($promedio_ponderado_final != 0){ 

										if($promedio_ponderado_final >= 80){
						$color = '#008000';
						$color_text = '#fff';
					}else if ($promedio_ponderado_final >= 60) {
						$color = '#73CB77';
						$color_text = '#000';
					}else if ($promedio_ponderado_final >= 40) {
						$color = '#FFFF00';
						$color_text = '#000';
					}else if ($promedio_ponderado_final >= 20) {
						$color = '#FF4343';
						$color_text = '#fff';
					}else{
						$color = '#C00000';
						$color_text = '#fff';
					}
					
					?>
 

					<td class="text-center h4" style="vertical-align: middle;">
						<span class="badge  badge-pill" style="background:<?php echo $color; ?>;color:<?php echo $color_text; ?>"  > {{number_format($promedio_ponderado_final, 2, '.', ',')}} </span>
					</td>

						<?php }

									else{ ?>

							<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
						<?php } ?>

						</tr>
					<?php }

								$actual_usuario = $resultado->id_evaluado;
								$contador_factores = 0;
								$total = 0;
								$actual_columna = 0;
								$promedio = 0;
								$autoevaluacion = $jefe = $colaborador = $par = $psicometricos = false;
								$promedio_ponderado = 0;
								$promedio_ponderado_final = 0; ?>

						<tr>
							<td class="text-left" style="border: 1px solid #DDD">{{$resultado->idempleado}}</td>
							<td class="text-left" style="border: 1px solid #DDD">{{str_replace('Ã‘','Ñ',$promedios_tipo_evaluacion[$j]->last_name)}} {{str_replace('Ã‘','Ñ',$promedios_tipo_evaluacion[$j]->first_name)}}</td>
							<td class="text-left" style="border: 1px solid #DDD">{{str_replace('Ã‘','Ñ',$promedios_tipo_evaluacion[$j]->direction)}}</td>
							<td class="text-left" style="border: 1px solid #DDD">{{str_replace('Ã‘','Ñ',$promedios_tipo_evaluacion[$j]->department)}}</td>
							<td class="text-left" style="border: 1px solid #DDD">{{str_replace('Ã‘','Ñ',$promedios_tipo_evaluacion[$j]->jefe)}}</td>

					<?php if (!empty($promedios_tipo_evaluacion[$j]->promedio_jefe)){

									$jefe = true;
									$promedio_ponderado_final = $promedios_tipo_evaluacion[$j]->promedio_ponderado; 

									if($promedios_tipo_evaluacion[$j]->promedio_jefe >= 80){
						$color = '#008000';
						$color_text = '#fff';
					}else if ($promedios_tipo_evaluacion[$j]->promedio_jefe >= 60) {
						$color = '#73CB77';
						$color_text = '#000';
					}else if ($promedios_tipo_evaluacion[$j]->promedio_jefe >= 40) {
						$color = '#FFFF00';
						$color_text = '#000';
					}else if ($promedios_tipo_evaluacion[$j]->promedio_jefe >= 20) {
						$color = '#FF4343';
						$color_text = '#fff';
					}else{
						$color = '#C00000';
						$color_text = '#fff';
					}
					
					?>
 

					<td class="text-center h4" style="vertical-align: middle;">
						<span class="badge  badge-pill" style="background:<?php echo $color; ?>;color:<?php echo $color_text; ?>"  > {{number_format($promedios_tipo_evaluacion[$j]->promedio_jefe, 2, '.', ',')}} </span>
					</td>



					<?php }

								else{ ?>

							<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
					<?php }

								if (!empty($promedios_tipo_evaluacion[$j]->promedio_par)){

									$par = true;
									$promedio_ponderado_final = $promedios_tipo_evaluacion[$j]->promedio_ponderado; 
									
									if($promedios_tipo_evaluacion[$j]->promedio_par >= 80){
						$color = '#008000';
						$color_text = '#fff';
					}else if ($promedios_tipo_evaluacion[$j]->promedio_par >= 60) {
						$color = '#73CB77';
						$color_text = '#000';
					}else if ($promedios_tipo_evaluacion[$j]->promedio_par >= 40) {
						$color = '#FFFF00';
						$color_text = '#000';
					}else if ($promedios_tipo_evaluacion[$j]->promedio_par >= 20) {
						$color = '#FF4343';
						$color_text = '#fff';
					}else{
						$color = '#C00000';
						$color_text = '#fff';
					}
					
					?>
 

					<td class="text-center h4" style="vertical-align: middle;">
						<span class="badge  badge-pill" style="background:<?php echo $color; ?>;color:<?php echo $color_text; ?>"  > {{number_format($promedios_tipo_evaluacion[$j]->promedio_par, 2, '.', ',')}} </span>
					</td>


					<?php }

								else{ ?>

							<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
					<?php }

								

									if (!empty($promedios_tipo_evaluacion[$j]->promedio_colaborador)){

										$colaborador = true;
										$promedio_ponderado_final = $promedios_tipo_evaluacion[$j]->promedio_ponderado; 
										
										
									if($promedios_tipo_evaluacion[$j]->promedio_colaborador >= 80){
						$color = '#008000';
						$color_text = '#fff';
					}else if ($promedios_tipo_evaluacion[$j]->promedio_colaborador >= 60) {
						$color = '#73CB77';
						$color_text = '#000';
					}else if ($promedios_tipo_evaluacion[$j]->promedio_colaborador >= 40) {
						$color = '#FFFF00';
						$color_text = '#000';
					}else if ($promedios_tipo_evaluacion[$j]->promedio_colaborador >= 20) {
						$color = '#FF4343';
						$color_text = '#fff';
					}else{
						$color = '#C00000';
						$color_text = '#fff';
					}
					
					?>
 

					<td class="text-center h4" style="vertical-align: middle;">
						<span class="badge  badge-pill" style="background:<?php echo $color; ?>;color:<?php echo $color_text; ?>"  > {{number_format($promedios_tipo_evaluacion[$j]->promedio_colaborador, 2, '.', ',')}} </span>
					</td>


						<?php }

									else{ ?>

							<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
						<?php }

								if (!empty($promedios_tipo_evaluacion[$j]->promedio_psicometricos)){

										$psicometricos = true;
										$promedio_ponderado_final = $promedios_tipo_evaluacion[$j]->promedio_ponderado; 
										
										if($promedios_tipo_evaluacion[$j]->promedio_psicometricos >= 80){
							$color = '#008000';
							$color_text = '#fff';
						}else if ($promedios_tipo_evaluacion[$j]->promedio_psicometricos >= 60) {
							$color = '#73CB77';
							$color_text = '#000';
						}else if ($promedios_tipo_evaluacion[$j]->promedio_psicometricos >= 40) {
							$color = '#FFFF00';
							$color_text = '#000';
						}else if ($promedios_tipo_evaluacion[$j]->promedio_psicometricos >= 20) {
							$color = '#FF4343';
							$color_text = '#fff';
						}else{
							$color = '#C00000';
							$color_text = '#fff';
						}
						
						?>
	 
	
						<td class="text-center h4" style="vertical-align: middle;">
							<span class="badge  badge-pill" style="background:<?php echo $color; ?>;color:<?php echo $color_text; ?>"  > {{number_format($promedios_tipo_evaluacion[$j]->promedio_psicometricos, 2, '.', ',')}} </span>
						</td>

						<?php }

									else{ ?>

							<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
						<?php }

								if (!empty($promedios_tipo_evaluacion[$j]->promedio_autoevaluacion)){

									$autoevaluacion = true;
									$promedio_ponderado_final = $promedios_tipo_evaluacion[$j]->promedio_ponderado;
									
										if($promedios_tipo_evaluacion[$j]->promedio_autoevaluacion >= 80){
							$color = '#008000';
							$color_text = '#fff';
						}else if ($promedios_tipo_evaluacion[$j]->promedio_autoevaluacion >= 60) {
							$color = '#73CB77';
							$color_text = '#000';
						}else if ($promedios_tipo_evaluacion[$j]->promedio_autoevaluacion >= 40) {
							$color = '#FFFF00';
							$color_text = '#000';
						}else if ($promedios_tipo_evaluacion[$j]->promedio_autoevaluacion >= 20) {
							$color = '#FF4343';
							$color_text = '#fff';
						}else{
							$color = '#C00000';
							$color_text = '#fff';
						}
						
						?>
	 
	
						<td class="text-center h4" style="vertical-align: middle;">
							<span class="badge  badge-pill" style="background:<?php echo $color; ?>;color:<?php echo $color_text; ?>"  > {{number_format($promedios_tipo_evaluacion[$j]->promedio_autoevaluacion, 2, '.', ',')}} </span>
						</td>

					<?php }

								else{ ?>

							<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
					<?php } ?>

							<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">{{(!empty($resultado->comentario) && !empty($resultado->objetivo) ? 'SI' : 'NO')}}
				
					<?php if (!$ponderaciones->isEmpty() && in_array($resultado->id_nivel_puesto, $niveles_puestos)){

									$total_sin_ponderacion = 0;
									$total_ponderacion = 0;
									$contador_sin_ponderacion = 0;

									if (empty($ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->jefe)){

										if (!empty($promedios_tipo_evaluacion[$j]->promedio_jefe)){

											$total_sin_ponderacion += $promedios_tipo_evaluacion[$j]->promedio_jefe;
											$contador_sin_ponderacion++;
										}
									}

									else{

										$total_ponderacion += $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->jefe;
										$promedio_ponderado += $promedios_tipo_evaluacion[$j]->promedio_jefe * $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->jefe / 100;
									}

									if (empty($ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->par)){

										if (!empty($promedios_tipo_evaluacion[$j]->promedio_par)){

											$total_sin_ponderacion += $promedios_tipo_evaluacion[$j]->promedio_par;
											$contador_sin_ponderacion++;
										}
									}

									else{

										$total_ponderacion += $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->par;
										$promedio_ponderado += $promedios_tipo_evaluacion[$j]->promedio_par * $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->par / 100;
									}

									if (empty($ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->colaborador)){

										if (!empty($promedios_tipo_evaluacion[$j]->promedio_colaborador)){

											$total_sin_ponderacion += $promedios_tipo_evaluacion[$j]->promedio_colaborador;
											$contador_sin_ponderacion++;
										}
									}

									else{

										$total_ponderacion += $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->colaborador;
										$promedio_ponderado += $promedios_tipo_evaluacion[$j]->promedio_colaborador * $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->colaborador / 100;
									}

									if (empty($ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->psicometricos)){

										if (!empty($promedios_tipo_evaluacion[$j]->promedio_psicometricos)){

											$total_sin_ponderacion += $promedios_tipo_evaluacion[$j]->promedio_psicometricos;
											$contador_sin_ponderacion++;
										}
									}

									else{

										$total_ponderacion += $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->psicometricos;
										$promedio_ponderado += $promedios_tipo_evaluacion[$j]->promedio_psicometricos * $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->psicometricos / 100;
									}

									if (empty($ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->autoevaluacion)){

										if (!empty($promedios_tipo_evaluacion[$j]->promedio_autoevaluacion)){

											$total_sin_ponderacion += $promedios_tipo_evaluacion[$j]->promedio_autoevaluacion;
											$contador_sin_ponderacion++;
										}
									}

									else{

										$total_ponderacion += $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->autoevaluacion;
										$promedio_ponderado += $promedios_tipo_evaluacion[$j]->promedio_autoevaluacion * $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->autoevaluacion / 100;
									}

									if ($contador_sin_ponderacion > 0){

										$ponderacion_restante = 100 - $total_ponderacion;
										$total_sin_ponderacion = $total_sin_ponderacion / $contador_sin_ponderacion;
										$promedio_ponderado += $total_sin_ponderacion * $ponderacion_restante / 100;
									}

									/*$promedio_ponderado = $promedios_tipo_evaluacion[$j]->promedio_jefe * $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->jefe / 100 + $promedios_tipo_evaluacion[$j]->promedio_par * $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->par / 100 + $promedios_tipo_evaluacion[$j]->promedio_colaborador * $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->colaborador / 100 + $promedios_tipo_evaluacion[$j]->promedio_psicometricos * $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->psicometricos / 100 + $promedios_tipo_evaluacion[$j]->promedio_autoevaluacion * $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->autoevaluacion / 100;*/
								}

								$j++;
							}

							for ($i = $actual_columna;$i < count($factores);$i++){
								
								if ($factores[$i]->id == $resultado->id_factor){

									break;
								} ?>

							<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
				<?php }

							$actual_columna = $i;
							$contador_factores++;
							//$total = $resultado->total_evaluacion / $resultado->numero_evaluaciones;
							$total = $calificaciones_factores[$actual_usuario][$resultado->id_factor];
							//$promedio += $total * $pesos_factores[$resultado->id_nivel_puesto][$resultado->id_factor] / 105;
							$actual_columna++;

										if($total >= 80){
							$color = '#008000';
							$color_text = '#fff';
						}else if ($total >= 60) {
							$color = '#73CB77';
							$color_text = '#000';
						}else if ($total >= 40) {
							$color = '#FFFF00';
							$color_text = '#000';
						}else if ($total >= 20) {
							$color = '#FF4343';
							$color_text = '#fff';
						}else{
							$color = '#C00000';
							$color_text = '#fff';
						}
						
						?>
	 
	
						<td class="text-center h4" style="vertical-align: middle;">
							<span class="badge  badge-pill" style="background:<?php echo $color; ?>;color:<?php echo $color_text; ?>"  > {{number_format($total, 2, '.', ',')}} </span>
						</td>

			<?php }

						if ($actual_usuario != 0){

							for ($i = $actual_columna;$i < count($factores);$i++){
								
								if ($factores[$i]->id == $resultado->id_factor){

									break;
								} ?>

							<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
				<?php }

							//$total = $promedio_ponderado_final;

							//if ($total != 0){ ?>

							<!--<td class="text-center" style="vertical-align: middle; font-weight: bold; font-size: 20px; border: 1px solid #DDD; background-color: {{($total >= 80 ? '#008000' : ($total >= 60 ? '#73CB77' : ($total >= 40 ? '#FFFF00' : ($total >= 20 ? '#FF4343' : '#C00000'))))}}">{{number_format($total, 1, '.', ',')}}</td>-->

				<?php //}

							//else{ ?>


							<!--<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>-->

				<?php //}

							if ($promedio_ponderado_final != 0){ 
								
								if($promedio_ponderado_final >= 80){
							$color = '#008000';
							$color_text = '#fff';
						}else if ($promedio_ponderado_final >= 60) {
							$color = '#73CB77';
							$color_text = '#000';
						}else if ($promedio_ponderado_final >= 40) {
							$color = '#FFFF00';
							$color_text = '#000';
						}else if ($promedio_ponderado_final >= 20) {
							$color = '#FF4343';
							$color_text = '#fff';
						}else{
							$color = '#C00000';
							$color_text = '#fff';
						}
						
						?>
	 
	
						<td class="text-center h4" style="vertical-align: middle;">
							<span class="badge  badge-pill" style="background:<?php echo $color; ?>;color:<?php echo $color_text; ?>"  > {{number_format($promedio_ponderado_final, 2, '.', ',')}} </span>
						</td>

				<?php }

							else{ ?>

							<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
				<?php } ?>
									
						</tr>
			<?php } ?>

					</tbody>
				</table>
			</div>
		</div>
		</div>
		</div>

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20 no_subordinados">No cuenta con colaboradores o aún no hay resultados en la evaluación del periodo</h4>
	</div>
</div>
@endif

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20">No Hay Periodos Abiertos o Cerrados</h4>
	</div>
</div>
@endif
</div>
</div>
</div>
</div>
@endsection

@section('scripts')
	<script>

$('.btn-excel').click(function(){
			$('.buttons-excel').trigger('click');
		});

		var table = '';
		
		$(document).ready(function(){

			table = $('.data-table').DataTable({
					dom:"<'row'<'col-md-6'l><'col-md-6'Bf>>" +
"<'row'<'col-md-6'><'col-md-6'>>" +
"<'row'<'col-md-12't>><'row'<'col-md-12'ip>>",	
   			'order': [[0,'asc']],
   			'scrollX': true,
   			language: {
		 		'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
      		  },
      	buttons: [
        {
          extend: 'excel',
					text: 'Exportar a Excel',
					titleAttr: 'Exportar a Excel',
					title: 'Reporte Evaluación Cualitativa'
				}
        ]
   		});

   		$('body').on('keyup', '#DataTables_Table_0_wrapper #DataTables_Table_0_filter label input', function(){

    		var busqueda = $(this).val();
    		var numero_columna = $('select#field').val() * 1;

    		if (numero_columna != -1){

    			table.columns().search('').draw();
      		table.columns([numero_columna]).search(busqueda).draw();
    		}
  		});
		
			$('select#field').change(function(){

    		var busqueda = $('#DataTables_Table_0_wrapper #DataTables_Table_0_filter label input').val();
    		var numero_columna = $(this).val() * 1;

    		if (numero_columna != -1){

      		table.columns().search('').draw();
      		table.columns([numero_columna]).search(busqueda).draw();
    		}

    		else{

    			table.columns().search('').draw();
    		}
  		});

   		$('select.periodos').change(function(){

   			window.location = '/reporte-detalle/' + $(this).val();
   		});
		});
	</script>
@endsection