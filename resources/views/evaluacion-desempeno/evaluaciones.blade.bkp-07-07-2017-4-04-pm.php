@extends('layout')

@section('title', 'Evaluación de Desempeño')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-responsive" src="img/evaluacion-desempeno/evaluaciones/ban.png" alt="">
	
		<div class="col-md-12 margin-top-20 table-responsive">
			
			<table class="table table-striped data-table" style="border: 1px solid #999">
				<caption align="center">Evaluaciones</caption>
				<thead>
					<tr>
						<th class="text-center" style="background-color: #555759; color: white; padding: 10px">#</th>
						<th class="text-center" style="background-color: #555759; color: white; padding: 10px">Evaluado</th>
						<th class="text-center" style="background-color: #555759; color: white; padding: 10px">Foto</th>
						<th class="text-center" style="background-color: #555759; color: white; padding: 10px">Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach($evaluados as $eval)
						<tr>
							<td>
								{{ $eval->id_evaluado }}
							</td>
							<td>
								{{ $eval->nameCompleteUserEvaluado->fullname }}
							</td>
							<td>
								@if(File::exists(public_path() . '/img/evaluacion-desempeno/' . $eval->id_evaluado . '.jpg'))
									<img class="img-responsive center-img" width="150" src="{{ asset('/img/evaluacion-desempeno/' . $eval->id_evaluado . '.jpg' ) }}" alt="{{ $eval->id_evaluado }}" >
								@else
									<img class="img-responsive center-img" width="150" src="{{ asset('/img/evaluacion-desempeno/' . $eval->id_evaluado . '.png' ) }}" alt="{{ $eval->id_evaluado }}" >
								@endif
							</td>
							{{--<td>
								  @if($eval->status == 'No Iniciada')
								@if($eval->status == 'No Iniciada')
									<span class="label label-danger">No Iniciada</span>
								  @elseif($eval->status == 'Iniciada')
								@elseif($eval->status == 'Iniciada')
									<span class="label label-warning">Iniciada</span>
								@else
									<span class="label label-success">Terminada</span>
								@endif
							</td>--}}
							<td>
								@if($eval->status == 'No Iniciada')
									<a class="btn btn-danger" href="{{ url('evaluado', $eval->id_evaluado) }}">No Iniciado</a>
								@elseif($eval->status == 'Iniciada')
									<a class="btn btn-warning" href="{{ url('evaluado', $eval->id_evaluado) }}">Iniciado</a>
								@else
									<a class="btn btn-success" href="{{ url('evaluado', $eval->id_evaluado) }}">Terminado</a>
								@endif
								</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	<script>
		
		$(document).ready(function(){

			$('.data-table').DataTable({
   			'order': [[3,'asc']]
   		});
		});
	</script>
@endsection