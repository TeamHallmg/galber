@extends('layouts.app')

@section('title', 'Competencias')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
		
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Competencias
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
 
 
	@if (auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin'))
	<div class="col-md-12 text-center">
		<a href="" id='agregarFactor' class="btn btn-primary"><span class="fas fa-plus-circle"></span> Agregar Competencia</a>
	</div>
	@endif
	<br>
	<br>

	{!! Form::open(['route' => 'factores.store', 'method' => 'POST', 'onsubmit' => 'return validaFormulario()'])!!}
		<div class="col-md-12" id="formAddFactor">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
					{!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre', 'required']) !!}
					</div>
					<div class="form-group">
					{!! Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Descripción', 'required', 'style' => 'resize:none', 'COLS' => 2, 'ROWS' => 2]) !!}
					</div>
					<div class="form-group">
					{!! Form::select('id_tipo_factor', $tiposFactores, null, ['class' => 'form-control', 'id' => 'id_tipo_factor']) !!}
					</div>
					<div class="form-group">
					{!! Form::select('familia', $FamiliasFactores, null, ['class' => 'form-control', 'onchange' => 'getval(this);', 'id' => 'familiasDeFactores']) !!}
					</div>
					<div class="grupoArea" style="display: none" id="gruposAreas">
    				<div>
    				{!! Form::select('gruposAreas', $gruposAreas, null, ['class' => 'form-control', 'id' => 'grupoAreas']) !!}
    				<br>
    				</div>
  				</div>
					<div style="display: none">
    			{!! Form::select('subFamiliasFactores', $subFamiliasFactores, null, ['class' => 'form-control', 'id' => 'subFamiliasFactores']) !!}
    				<br>
    			</div>
    			<div class="form-group">
					{!! Form::select('comentarios', array('1' => 'Con Comentarios', '2' => 'Sin Comentarios'), null, ['class' => 'form-control', 'id' => 'comentarios']) !!}
					</div>
					<div class="form-group">
					{!! Form::select('orden', $orders, null, ['class' => 'form-control', 'id' => 'orden']) !!}
					</div>

					@if (count($nivelesPuestos) > 0)

					<div class="form-group row">
						<div class="col-md-6">
							<h5>Nivel de Puesto</h5>
						</div>
						<div class="col-md-3">
							<h5>Esperado</h5>
						</div>
						<div class="col-md-3">
							<h5>Peso</h5>
						</div>
					</div>
					<div class="nivelesPuestos form-group row" id="nivelesPuestos">
  				
  					@foreach($nivelesPuestos as $nivelP)
  						{!! Form::hidden('id_niveles_puesto[]', $nivelP->id, ['class' => 'form-control idnivelPuestos', 'readonly' ]) !!}
  						<div class="col-md-6">
  							{!! Form::text('niveles_Puesto[]', $nivelP->nombre, ['class' => 'form-control nivelPuestos', 'readonly' ]) !!}
  						</div>
  						<div class="col-md-3">
  							{!! Form::text('Calificación_Esperada[]', null, ['class' => 'form-control calificacionEsperada', 'placeholder' => 'Calificacion Esperada', 'min' => 0]) !!}
  						</div>
  						<div class="col-md-3">
  							{!! Form::text('peso[]', null, ['class' => 'form-control peso', 'placeholder' => 'Peso', 'min' => 0]) !!}
  						</div>
  						<br><br>
  					@endforeach
  				
  				</div>
  				@endif

				</div>
				<div class="col-md-6">
					<div class="form-group">
					{!! Form::button('<span class="fas fa-plus-circle"></span> Agregar Pregunta', ['class' => 'btn btn-primary btn-Finvivir-yellow', 'id' => 'agregarNivel']) !!}
      		</div>
					<div class="niveles"></div>
					<div class="form-group">
					{{ Form::button('<i class="fas fa-check-circle"></i> Guardar Competencia', array('type' => 'submit', 'class' => 'btn btn-success')) }}
					</div>
				</div>
			</div>
		</div>
	{!! Form::close() !!}


	<div class="nivel" style="display: none">
		<div>
		<div class="row">
	    	<div class="col-md-12" style="padding: 0;">
	    		{!! Form::textarea('niveles_de_dominio[]', null, ['class' => 'form-control mb-4', 'placeholder' => 'Escribe la pregunta', 'required', 'style' => 'resize:none', 'COLS' => 2, 'ROWS' => 1]) !!}
	    	</div>
	    	{{--<div class="col-md-3">
	    		{!! Form::number('calificacionDominio[]', null, ['class' => 'form-control', 'required', 'min' => 0]) !!}
	    	</div>
	    	<div class="col-md-12">
          {!! Form::select('etiqueta[]', $etiquetas, ['class' => 'form-control']) !!}
        </div>--}}
	    </div>

    	{!! Form::button('<i class="fas fa-times-circle"></i> Remover Pregunta', ['class' => 'btn btn-danger removerNivel mb-4']) !!}

    </div>
   </div>
  
	
	<div class="col-md-12 mt-5">
		@if ($factores->isEmpty())
			<table class="table table-striped table-bordered factores table-responsive">
				<thead  style="background-color: #222B64; color:white;">
					<tr>
						<th>ID</th>
						<th>Nombre</th>
						<th>Descripción</th>
						<th>Matriz de Competencias</th>
						<th>Grupo del Área</th>
						<!--<th>Niveles de Dominios</th>
						<th>Niveles de Puestos</th>-->
						<!--<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Sub Familia</th>-->
						<th>Comentarios</th>
						<th>Orden</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		@else
			<table class="table table-striped table-bordered factores table-responsive" id="factores">
				<thead  style="background-color: #222B64; color:white;">
					<tr>
						<th>ID</th>
						<th>Nombre</th>
						<th>Descripción</th>
						<th>Matriz de Competencias</th>
						<th>Grupo del Área</th>
						<!--<th>Niveles de Dominios</th>
						<th>Niveles de Puestos</th>-->
						<!--<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Sub Familia</th>-->
						<th>Comentarios</th>
						<th>Orden</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>

					@foreach ($factores as $factor)
						<tr>
							{{--<td class="text-center" style="padding: 10px 5px">
								<span style="padding: 5px; background-color: #555759; color: white">{{$periodos[$i]->status}}</span>
							</td>--}}
							<td>{{$factor->id}}</td>
							<td>{{$factor->nombre}}</td>
							<td>{{$factor->descripcion}}</td>
							<td>
							@if(empty($factor->factoresFamilia))
							@else
								{{$factor->factoresFamilia->nombre_para_organizacion}}
							@endif
							</td>
							<td class="text-center" style="padding: 10px 5px">
								@if(empty($factor->grupoArea))
									{{ 'General' }}
								@else
									{{ $factor->grupoArea->Name }}
								@endif
							</td>
							{{--<td>--}}
								{{--  @if(empty($factor->nivelDeDominio))
								@else--}}
								{{--@foreach($factor->nivelDeDominio as $dom)
									<b>Dominio:</b> {{ $dom->nivel_dominio }} <b>Calificacion:</b> {{ $dom->calificacion }}<br>
								@endforeach--}}
								{{--{{ $factor->nivelDeDominio}}
							  @endif--}}
							{{--</td>--}}
							{{--<td>
								@if(empty($factor->nPuesto))
								@else
									@foreach($factor->nPuesto as $p)
										@foreach($p->nombreNivel as $nombreN)
											{{ $nombreN->nombre }}
											<br>
										@endforeach
									@endforeach
								@endif
							</td>--}}
							<!--<td>
								@if(empty($factor->subFamilia))
								@else
									{{ $factor->subFamilia->nombre }}
								@endif
							</td>-->
							<td>{{ ($factor->comentarios == 1 ? 'Si' : 'No') }}</td>
							<td>{{ $factor->orden }}</td>
							<td nowrap>
								<a href="{{ url('factores/' . $factor->id . '/edit') }}" class="btn btn-primary"><i class="fas fa-edit"></i> Editar</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		@endif
	</div>
</div>
</div>
</div>
</div>

<!-- Modal -->
<div id="modalContainer" class="modal fade">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          
          <h4 class="modal-title text-center text-warning"><span class="glyphicon glyphicon-warning-sign"></span>&nbsp;&nbsp;&nbsp;ADVERTENCIA</h4>
        </div>
        <div class="modal-body">
          <p id="modalMessage">Debes agregar por lo menos una pregunta.</p>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
	</div>
</div>
@endsection
@section('scripts')
<script>
	var cont = 0;
	var factores = <?php echo json_encode($factores)?>;

	function validaFormulario(){
		if( $("input[name=nombre]").val() == '' || $("input[name=descripcion]").val() == '' || cont == 0){
				$('#modalContainer #modalMessage').text('Debes agregar por lo menos una pregunta.');
				$('#modalContainer').modal('show');
				return false;
			}/*else{

				var orden = $('select#orden').val();
        var band = 0;

        for (var i = 0;i < factores.length;i++){

          if (orden == factores[i].orden){

            band = 1;
          }
        }

        if (band == 1){

          $('#modalContainer #modalMessage').text('Ya existe una competencia con el orden seleccionado.');
          $('#modalContainer').modal('show');
          return false;
        }

				return true;
			}*/
	}

	$('.factores').DataTable({
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
	});

	$('#formAddFactor').hide();

	$(document).on('click', '#agregarFactor', function(e){
		e.preventDefault();
		$('#formAddFactor').toggle('slow');
		$('#agregarFactor').hide();		
	});

	function getval(sel){
		if(sel.value == 5){
			$('div#gruposAreas').show('slow');
		}

		else{
			$('div#gruposAreas').hide('slow');
		}

		/*if(sel.value == 2){
			$('#nivelesPuestos').hide('slow');
			$('.calificacionEsperada').removeAttr('required');
		}

		if(sel.value == 3){
			$('#nivelesPuestos').toggle('slow');
			$('.calificacionEsperada').attr('required', 'required');
		}*/
	}

	$(document).ready(function(){

    $('#agregarNivel').on('click', function(){
    	cont++;
    	if(cont < 10){
    		$('.niveles').append($('.nivel').html());
    	}else if(cont == 10){
    		$('.niveles').append($('.nivel').html());
    		$('#agregarNivel').hide();
    	}
    });

    $('body').on('click', '.removerNivel', function(){
      $(this).parent().remove();
      cont--;
      if(cont < 10){
      	$('#agregarNivel').show();	
      }else if(cont == 10){
      	$('#agregarNivel').hide();
      }
    });
  });
	
</script>
@endsection