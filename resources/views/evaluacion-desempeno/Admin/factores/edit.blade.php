@extends('layouts.app')

@section('title', 'Competencias')

@section('content')
<div class="row">
  <div class="col-md-2 text-right">
    @include('evaluacion-desempeno/partials/sub-menu')
  </div>
  <div class="col-md-10">
    <img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
    
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Editar Competencia
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
 
 
  {!! Form::open(['route' => ['factores.update', $id], 'method' => 'PUT', 'onsubmit' => 'return validaFormulario()'])!!}
    <div class="col-md-12" id="formAddFactor">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
          {!! Form::text('nombre', $factores->nombre, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
          </div>
          <div class="form-group">
          {!! Form::textarea('descripcion', $factores->descripcion, ['class' => 'form-control', 'placeholder' => 'Descripción', 'required', 'style' => 'resize:none', 'COLS' => 2, 'ROWS' => 2]) !!}
          </div>
          <div class="form-group">
          {!! Form::select('id_tipo_factor', $tiposFactores, $factores->id_tipo_factor, ['class' => 'form-control', 'id' => 'id_tipo_factor']) !!}
          </div>
          <div class="form-group">
          {!! Form::hidden('familia', $factores->id_familia, ['class' => 'form-control']) !!}
          {!! Form::select('familia', $FamiliasFactores, $factores->id_familia, ['class' => 'form-control', 'onchange' => 'getval(this);', 'id' => 'familiasDeFactores']) !!}
          </div>
          <div class="grupoArea form-group" id="gruposAreas" style="display: none;">
            <div>
            {!! Form::select('gruposAreas', $gruposAreas, $factores->id_grupo_area, ['class' => 'form-control', 'id' => 'grupoAreas']) !!}
            </div>
          </div>
        <!--<div class="form-group">
          {!! Form::select('subFamiliasFactores', $subFamiliasFactores, $factores->id_sub_familia, ['class' => 'form-control', 'id' => 'subFamiliasFactores']) !!}
          <br>
        </div>-->
          <div class="form-group">
          {!! Form::select('comentarios', array('1' => 'Con Comentarios', '2' => 'Sin Comentarios'), $factores->comentarios, ['class' => 'form-control', 'id' => 'comentarios']) !!}
          </div>
		      <div class="form-group">
          {!! Form::select('orden', $orders, $factores->orden, ['class' => 'form-control', 'id' => 'orden']) !!}
          </div>

          @if (count($nivelesPuestos) > 0)

          <div class="form-group row">
            <div class="col-md-6">
              <h5>Nivel de Puesto</h5>
            </div>
            <div class="col-md-3">
              <h5>Esperado</h5>
            </div>
            <div class="col-md-3">
              <h5>Peso</h5>
            </div>
          </div>
          <div class="nivelesPuestos" id="nivelesPuestos">
            <div class="row">
              @foreach($nivelesPuestos as $nivelP)
              {!! Form::hidden('id_niveles_puesto[]', $nivelP->id, ['class' => 'form-control idnivelPuestos', 'readonly' ]) !!}
              <div class="col-md-6">
                {!! Form::text('niveles_Puesto[]', $nivelP->nombre, ['class' => 'form-control nivelPuestos', 'readonly' ]) !!}
              </div>
          <?php $calificacionEsperada = 0;
                $peso = 0; ?>
                @if(count($nivelesPuestosEdit) > 0)
                  @foreach($nivelesPuestosEdit as $nivelPEdit)
                    @if ($nivelPEdit->id_nivel_puesto == $nivelP->id)
                <?php $calificacionEsperada = $nivelPEdit->nivel_esperado;
                      $peso = $nivelPEdit->peso; ?>
                    @endif
                  @endforeach
                @endif
              <div class="col-md-3">
                @if (!empty($calificacionEsperada))
                {!! Form::text('Calificación_Esperada[]', $calificacionEsperada, ['class' => 'form-control valificacionEsperada', 'placeholder' => 'Calificacion Esperada']) !!}
                @else
                {!! Form::text('Calificación_Esperada[]', null, ['class' => 'form-control calificacionEsperada', 'placeholder' => 'Calificacion Esperada', 'min' => 0]) !!}
                @endif
              </div>
              <div class="col-md-3">
                @if (!empty($peso))
                {!! Form::text('peso[]', $peso, ['class' => 'form-control peso', 'placeholder' => 'Peso']) !!}
                @else
                {!! Form::text('peso[]', null, ['class' => 'form-control peso', 'placeholder' => 'Peso', 'min' => 0]) !!}
                @endif
              </div>
              <br><br>
            @endforeach
            </div>
          </div>
          @endif
          
        </div>
        <div class="col-md-6">
          <div class="form-group">
          
          {{ Form::button('<i class="fas fa-plus-circle"></i> Agregar Pregunta', ['class' => 'btn btn-primary', 'id' => 'agregarNivel']) }}
          </div>
          <div class="niveles">
        @foreach($nivelesDominios as $nivel)

            <div class="row">
              <div class="col">
              {!! Form::textarea('niveles_de_dominio[]', $nivel->nivel_dominio, ['class' => 'form-control', 'placeholder' => 'Escribe la pregunta', 'required', 'style' => 'resize:none', 'COLS' => 2, 'ROWS' => 2]) !!}
              </div>
              {{--<div class="col">
              {!! Form::number('calificacionDominio[]', $nivel->calificacion, ['class' => 'form-control', 'required']) !!}
              </div>
              <div class="col-md-12">
              {!! Form::select('etiqueta[]', $etiquetas, $nivel->etiqueta, ['class' => 'form-control']) !!}
              </div>--}}
            </div>
          
            {!! Form::button('<i class="fas fa-times-circle"></i> Remover Pregunta', ['class' => 'btn btn-danger removerNivel mt-4']) !!}

            <hr>

        @endforeach 
          </div>
          <div class="form-group">
          {{ Form::button('<i class="fas fa-sync-alt"></i> Actualizar Competencia', array('type' => 'submit', 'class' => 'btn btn-success')) }}
          </div>
        </div>
      </div>
    </div>
  {!! Form::close() !!}

<div class="nivel" style="display: none">
    <div>
      <div class="col-md-12" style="padding: 0;">
        {!! Form::textarea('niveles_de_dominio[]', null, ['class' => 'form-control', 'placeholder' => 'Escribe la pregunta', 'required', 'style' => 'resize:none', 'COLS' => 2, 'ROWS' => 2]) !!}
      </div>
      {{--<div class="col-md-3 my-4">
        {!! Form::number('calificacionDominio[]', null, ['class' => 'form-control', 'required']) !!}
      </div>
      <div class="col-md-12">
        {!! Form::select('etiqueta[]', $etiquetas, ['class' => 'form-control']) !!}
      </div>--}}
      {!! Form::button('<i class="fas fa-times-circle"></i> Remover Pregunta', ['class' => 'btn btn-danger removerNivel']) !!}
      <br>
      <br>
    </div>
</div>
  </div>
</div>
<div id="modalContainer" class="modal fade">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          
          <h4 class="modal-title text-center text-warning"><span class="glyphicon glyphicon-warning-sign"></span>&nbsp;&nbsp;&nbsp;ADVERTENCIA</h4>
        </div>
        <div class="modal-body">
          <p id="modalMessage">Debes agregar por lo menos una pregunta.</p>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
</div>
</div>

@endsection
@section('scripts')
<script>
  var cont = {{ $nivelCount }};
  var famFactor = {{ $factores->id_familia }};
  var factores = <?php echo json_encode($all_factores)?>;
  var id = {{ $factores->id }};

  if(cont == 10){
    $('#agregarNivel').hide();
  }else{
    $('#agregarNivel').show();
  }

  function validaFormulario(){
    if( $("input[name=nombre]").val() == '' || $("input[name=descripcion]").val() == '' || cont == 0){
        $('#modalContainer #modalMessage').text('Debes agregar por lo menos una pregunta.');
        $('#modalContainer').modal('show');
        return false;
      }/*else{
        var orden = $('select#orden').val();
        var band = 0;

        for(var i = 0;i < factores.length;i++){

          if (orden == factores[i].orden && id != factores[i].id){

            band = 1;
          }
        }

        if (band == 1){

          $('#modalContainer #modalMessage').text('Ya existe una competencia con el orden seleccionado.');
          $('#modalContainer').modal('show');
          return false;
        }

        $("input[name=familia]").removeAttr('disabled');
        return true;
      }*/
  }

  $('#formEditFactor').hide();

  /*if(famFactor == 2){
    $('#gruposAreas').toggle('slow');
  }else{
    $('#gruposAreas').hide('slow');
  }*/

  function getval(sel){
    if(sel.value == 5){
      $('div#gruposAreas').show('slow');
    }

    else{
      $('div#gruposAreas').hide('slow');
    }

    /*if(sel.value == 2){

      $("#nivelesPuestos").hide('slow');
      $('.calificacionEsperada').removeAttr('required');
    }

    if(sel.value == 3){

      $("#nivelesPuestos").show('slow');
      $('.calificacionEsperada').attr('required', 'required');
    }*/
  }

  $(document).ready(function(){

    $('#agregarNivel').on('click', function(){
      cont++;
      if(cont < 10){
        $('.niveles').append($('.nivel').html());
      }else if(cont == 10){
        $('.niveles').append($('.nivel').html());
        $('#agregarNivel').hide();
      }
    });

    $('body').on('click', '.removerNivel', function(){
      $(this).parent().remove();
      cont--;
      if(cont < 10){
        $('#agregarNivel').show();  
      }else if(cont == 10){
        $('#agregarNivel').hide();
      }
    });
  });
  
</script>
@endsection