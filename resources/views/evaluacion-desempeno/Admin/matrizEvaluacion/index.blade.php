@extends('layouts.app')

@section('title', 'Red de Interacción')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }}" alt="">
		
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Red de Interacción
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
  
	

				<div class="row"> 
					<div class="col-12 offset-md-4 col-md-3 text-center">
						<div class="form-group">
							<label for="id_plan" class="font-weight-bolder mb-0">Periodo:</label>							
							<select class="form-control periodo_matriz">
							
								@foreach ($periodos as $periodo)

								<option value="{{$periodo->id}}" <?php if ($periodo->id == $_GET['id_periodo']){ ?>selected="selected"<?php } ?>>{{$periodo->descripcion}}</option>
						@endforeach
							</select>
						</div>
					</div> 
				</div>
 
		
				<div class="card mt-3">
					<h5 class="card-header bg-info text-white font-weight-bolder">Agregar Evaluador/Evaluado</h5>
					<div class="card-body">

						<div class="col-md-12 text-center my-4">
							<a href="" id='agregarEvaluadorEvaluado' class="btn btn-primary"><i class="fas fa-plus-circle"></i> Agregar Evaluador/Evaluado</a>
						</div>

 
							<div id="formEvaluacion" style="width: 100%">
								{!! Form::open(['id' => 'form-evaluador-evaluado', 'action' => 'EvaluacionDesempeno\Admin\MatrizEvaluacionController@store', 'method' => 'POST']) !!}
								<div class="row">
									<div class="col form-group" align="center">
										
											<label>Evaluador</label>
									<br>
									<input type="text" class="form-control evaluador_autocomplete" placeholder="Escribe el nombre">
									<input type="hidden" name="evaluador" id="idEvaluador">
									<!--<select class="selectpicker" data-show-subtext="true" data-live-search="true" name="evaluador" id="idEvaluador">-->
									{{--<select class="form-control" name="evaluador" id="idEvaluador">
										<option data-subtext="" value="0">Selecciona una opcion</option>
												@foreach($usuarios as $user)
													@if($user->id == 1 || empty($user->employee_wt))
													@else
														<option data-subtext="{{ $user->id }}" value="{{ $user->id }}">{{ $user->fullname }}</option>
													@endif
												@endforeach
									</select>--}}
									
									</div>
									<div class="col form-group" align="center">
										
											<label>Evaluado</label>
											<br>
											<input type="text" class="form-control evaluado_autocomplete" placeholder="Escribe el nombre">
											<input type="hidden" name="evaluado" id="idEvaluado">
									<!--<select class="selectpicker" data-show-subtext="true" data-live-search="true" name="evaluado" id="idEvaluado">-->
									{{--<select class="form-control" name="evaluado" id="idEvaluado">
										<option data-subtext="" value="0">Selecciona una opcion</option>
											@foreach($usuarios as $user)
												@if($user->id == 1 || empty($user->employee_wt))
												@else
												<option data-subtext="{{ $user->id }}" value="{{ $user->id }}">{{ $user->fullname }}</option>
												@endif
											@endforeach
									</select>--}}
									
									</div>
									<div class="col form-group" align="center">
											
											<label>Tipo de Evaluación</label>
									<br>
									<select class="form-control" name="tipo" id="tipoEvaluacion">
										<option data-subtext="" value="0">Selecciona una opcion</option>
											@foreach($tipos_evaluacion as $key => $tipo)
												<option data-subtext="{{ $key }}" value="{{ $key }}">{{ $tipo }}</option>
											@endforeach
									</select>

									</div>
									
								</div>
								<div class="form-group" align="center">
										<input type="hidden" name="periodo" value="{{$_GET['id_periodo']}}">
										{{--  
										{!! Form::submit('Guardar Evaluador/Evaluado', ['class' => 'btn btn-primary'])!!}
										--}}
										<button class="btn btn-primary btn-Finvivir-yellow agregar_eval" value="agregarEval" name="btnMatrizEval"><i class="fas fa-plus-circle"></i> Agregar Evaluador/Evaluado</button>
									</div>
								{!! Form::close() !!}
							</div>
					</div>
				</div>

				

		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Detalles</h5>
			<div class="card-body">




 
	<div class="col-md-12">
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-hover matrizEvaluacion ">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th>ID Evaluador</th>
					<th>Nombre Evaluador</th>
					<th>Departamento</th>
					<th>Puesto</th>
					<th>ID Evaluado</th>
					<th>Nombre Evaluado</th>
					<th>Departamento</th>
					<th>Puesto</th>
					<th>Tipo</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@if(empty($matrizEva) || is_null($matrizEva) || $countPeriodos == 0)
					<tr>
						<td colspan="6" class="text-center" style="padding: 10px 5px">Esta sección solo esta disponible cuando hay periodos Preparatorios o Abiertos</td>
					</tr>
				@else
					@foreach($matrizEva as $matriz)
						<tr>
							<td class="text-center" style="padding: 10px 5px">
								{{ (isset($matriz->nameCompleteUserEvaluador->employee_wt->idempleado) ? $matriz->nameCompleteUserEvaluador->employee_wt->idempleado : '') }}
							</td>
							<td class="text-center" style="padding: 10px 5px">
								{{ (!empty($matriz->nameCompleteUserEvaluador->fullname) ? str_replace('Ã‘','Ñ',$matriz->nameCompleteUserEvaluador->fullname) : '') }}
							</td>
							<td class="text-center" style="padding: 10px 5px">
								{{ (isset($matriz->nameCompleteUserEvaluador->employee_wt->jobPosition->area->department->name) ? $matriz->nameCompleteUserEvaluador->employee_wt->jobPosition->area->department->name : '') }}
							</td>
							<td class="text-center" style="padding: 10px 5px">
								{{ (isset($matriz->nameCompleteUserEvaluador->employee_wt->jobPosition->name) ? $matriz->nameCompleteUserEvaluador->employee_wt->jobPosition->name : '') }}
							</td>
							<td class="text-center" style="padding: 10px 5px">
								{{ (isset($matriz->nameCompleteUserEvaluado->employee_wt->idempleado) ? $matriz->nameCompleteUserEvaluado->employee_wt->idempleado : '') }}
							</td>
							<td class="text-center" style="padding: 10px 5px">
								{{ (!empty($matriz->nameCompleteUserEvaluado->fullname) ? str_replace('Ã‘','Ñ',$matriz->nameCompleteUserEvaluado->fullname) : '')}}
							</td>
							<td class="text-center" style="padding: 10px 5px">
								{{ (isset($matriz->nameCompleteUserEvaluado->employee_wt->jobPosition->area->department->name) ? $matriz->nameCompleteUserEvaluado->employee_wt->jobPosition->area->department->name : '') }}
							</td>
							<td class="text-center" style="padding: 10px 5px">
								{{ (isset($matriz->nameCompleteUserEvaluado->employee_wt->jobPosition->name) ? $matriz->nameCompleteUserEvaluado->employee_wt->jobPosition->name : '') }}
							</td>
							<td class="text-center" style="padding: 10px 5px">
								{{ (isset($matriz->tipoEvaluacion->descripcion) ? $matriz->tipoEvaluacion->descripcion_empresa : ($matriz->id_evaluador == $matriz->id_evaluado ? 'AUTOEVALUACIÓN' : (isset($matriz->nameCompleteUserEvaluado->employee_wt->boss) && $matriz->nameCompleteUserEvaluado->employee_wt->boss->user->id == $matriz->id_evaluador ? $tipos_evaluacion[2] : (isset($matriz->nameCompleteUserEvaluador->employee_wt->boss) && $matriz->nameCompleteUserEvaluador->employee_wt->boss->user->id == $matriz->id_evaluado ? $tipos_evaluacion[5] : (isset($matriz->nameCompleteUserEvaluado->employee_wt) && isset($matriz->nameCompleteUserEvaluador->employee_wt) && $matriz->nameCompleteUserEvaluado->employee_wt->jefe == $matriz->nameCompleteUserEvaluador->employee_wt->jefe ? $tipos_evaluacion[3] : (isset($matriz->nameCompleteUserEvaluado->employee_wt) && isset($matriz->nameCompleteUserEvaluador->employee_wt) && $matriz->nameCompleteUserEvaluado->employee_wt->job_position_id == $matriz->nameCompleteUserEvaluador->employee_wt->job_position_id ? $tipos_evaluacion[6] : $tipos_evaluacion[4])))))) }}
							</td>
							<td class="text-center" style="padding: 10px 5px">
								<div class="row">
									<!--<div class="col-md-6" align="center">
										<form action="{{'/Matriz-Evaluacion/' . $matriz->id . '/edit'}}" method="post">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<input type="hidden" value="{{$_GET['id_periodo']}}" name="id_periodo">
											<input type="submit" class="btn btn-primary" value="Editar">
										</form>-->
										<!--<a href="{{ url('Matriz-Evaluacion/' . $matriz->id . '/edit') }}" class="btn btn-primary btn-Finvivir-yellow">Editar</a>-->
									<!--</div>-->
									<div class="col-md-12" align="center">
										{!! Form::open(['route' => ['Matriz-Evaluacion.destroy', $matriz->id], 'method' => 'DELETE'])!!}
										@if ($matriz->status == 'No Iniciada')
                			{!! Form::button('<i class="fas fa-trash-alt"></i>', array('class'=>'btn btn-danger', 'type'=>'submit', 'onclick' => 'return confirm("¿Desea eliminarla?")')) !!}
                		@else
                			{!! Form::button('<i class="fas fa-trash-alt"></i>', array('class'=>'btn btn-danger', 'type'=>'submit', 'onclick' => 'return confirm("Esta evaluación ya ha sido iniciada. ¿Desea eliminarla?")')) !!}
                		@endif	
              			{!! Form::close() !!}
              		</div>
								</div>
							</td>
						</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
	</div>
</div>
</div> 
<form action="/Matriz-Evaluacion" method="get" class="matriz_evaluacion">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="id_periodo" value="<?php echo $_GET['id_periodo']?>" class="id_periodo">
  <input type="submit" style="display: none">
</form>

<!-- Modal -->
<div id="modalContainer" class="modal fade">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          
          <h4 class="modal-title text-center text-warning"><span class="glyphicon glyphicon-warning-sign"></span>&nbsp;&nbsp;&nbsp;ADVERTENCIA</h4>
        </div>
        <div class="modal-body">
          <p id="modalMessage"></p>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
</div>
</div>
</div>
</div>
@endsection
@section('scripts')
<script>
	var contPeriodos = {{ $countPeriodos }};
	var id_evaluado = 0;
	var id_evaluador = 0;
	var users = <?php echo json_encode($usuarios)?>;

	if(contPeriodos == 0){
		$('#agregarEvaluadorEvaluado').hide();
	}

	$('button.agregar_eval').on('click', function(e) {
		e.preventDefault();

		var evaluador = $('input#idEvaluador').val();
		var evaluado = $('input#idEvaluado').val();
		var periodo = $('select[name="periodo"]').val();

		var btn = $(this);
		var form = $('#form-evaluador-evaluado');
		var url = form.attr('action');
		var data = form.serialize();

		if(contPeriodos > 1){
			if(periodo == 0){
				$('#modalMessage').html('Debes seleccionar un periodo.');
				$('#modalContainer').modal('show');
			}else if(evaluador == 0){
				$('#modalMessage').html('Debes seleccionar a un evaluador.');
				$('#modalContainer').modal('show');
			}else if(evaluado == 0){
				$('#modalMessage').html('Debes seleccionar a un evaluado.');
				$('#modalContainer').modal('show');
			}else{
				$.post(url, data, function (reply){
					if(reply.success){
						//$('#modalMessage').html(reply.msn);
						//$('#modalContainer').modal('show');
						alert('Se agregó la evaluación');
						location.reload(true);
					}else{
						$('#modalMessage').html(reply.msn);
						$('#modalContainer').modal('show');
					}
				});
			}
		}else{
			if(evaluador == 0){
				$('#modalMessage').html('Debes seleccionar a un evaluador.');
				$('#modalContainer').modal('show');
			}else if(evaluado == 0){
				$('#modalMessage').html('Debes seleccionar a un evaluado.');
				$('#modalContainer').modal('show');
			}else{
				$.post(url, data, function (reply){
					if(reply.success){
						//$('#modalMessage').html(reply.msn);
						//$('#modalContainer').modal('show');
						alert('Se agregó la evaluación');
						location.reload(true);
					}else{
						$('#modalMessage').html(reply.msn);
						$('#modalContainer').modal('show');
					}
				});
			}
		}

				/*if(periodo == 0 && contPeriodos > 1){
					$('#modalMessage').html('Debes seleccionar un periodo.');
					$('#modalContainer').modal('show');
				}else if(evaluador == 0){
					$('#modalMessage').html('Debes seleccionar a un evaluador.');
					$('#modalContainer').modal('show');
				}else if(evaluado == 0){
					$('#modalMessage').html('Debes seleccionar a un evaluado.');
					$('#modalContainer').modal('show');
				}else{
					$.post(url, data, function (reply){
						if(reply.success){
							$('#modalMessage').html(reply.msn);
							$('#modalContainer').modal('show');
							location.reload(true);
						}else{
							$('#modalMessage').html(reply.msn);
							$('#modalContainer').modal('show');
						}
					});
				}*/
		});

	$(document).ready(function(){
		$('#formEvaluacion').hide();

		$(document).on('click', '#agregarEvaluadorEvaluado', function(e){
			e.preventDefault();
			$('#formEvaluacion').toggle('slow');
			$('#agregarEvaluadorEvaluado').hide();
		});

		$('select.periodo_matriz').change(function(){

			window.location = 'Matriz-Evaluacion?id_periodo=' + $(this).val();
		});

		if(contPeriodos > 1){
			$('.matrizEvaluacion').DataTable({
      	'order': [[5,'DESC']],
      	language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
  		});
		}else{
			$('.matrizEvaluacion').DataTable({
      	'order': [[0,'DESC']],
      	language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
  		});
		}

		// Autocomplete 
    $('.evaluador_autocomplete').autocomplete({
    	
    	lookup: users,
    	onSelect: function (suggestion){

    		id_evaluador = suggestion.data;
    		$('input#idEvaluador').val(id_evaluador);
    	}
		});

		$('.evaluado_autocomplete').autocomplete({
    	
    	lookup: users,
    	onSelect: function (suggestion){

    		id_evaluado = suggestion.data;
    		$('input#idEvaluado').val(id_evaluado);
    	}
		});
	});
	
</script>
@endsection