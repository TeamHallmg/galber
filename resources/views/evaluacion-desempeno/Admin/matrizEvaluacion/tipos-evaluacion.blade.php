@extends('layouts.app')

@section('title', 'Tipos de Evaluación')

@section('content')

<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	
		<div class="col-md-10">

			<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
			
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Tipos de Evaluación
<a href="/matriz-evaluaciones/0" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
 

			<div class="row">
				<div class="col-md-12">
				 <div class="table-responsive">

			<table class="table table-striped table-bordered list ">
				<thead style="background-color: #222B64; color:white;">
					<tr>
						<th>#</th>
						<th>Nombre</th>
						<th>Descripción</th>
						<th>Nombre para Organización</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>

				@for ($i = 0; $i < count($tipos_evaluacion); $i++)

					<tr>
						<td class="text-center">{{$tipos_evaluacion[$i]->id}}</td>
						<td class="text-center">{{$tipos_evaluacion[$i]->nombre}}</td>
						<td class="text-center">{{$tipos_evaluacion[$i]->descripcion}}</td>
						<td class="text-center">
							<span class="descripcion_empresa">{{$tipos_evaluacion[$i]->descripcion_empresa}}</span>
							<input type="text" class="form-control descripcion_empresa" value="{{$tipos_evaluacion[$i]->descripcion_empresa}}" style="display: none">
						</td>
						<td class="text-center">
							<button type="button" class="btn btn-primary editar"><i class="fas fa-edit"></i> Editar</button>
							<form method="post" action="editar-tipo-evaluacion" class="tipo_evaluacion_form">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="id" value="{{$tipos_evaluacion[$i]->id}}">
								<input type="hidden" name="descripcion_empresa" class="descripcion_empresa">
								<button type="submit" class="btn btn-success" style="display: none"><i class="fas fa-check-circle"></i> Guardar</button>
							</form>
						</td>
					</tr>
				@endfor
				</tbody>
			</table>
		</div>
		</div>
		</div>
		</div>
	</div>
	</div>
	</div>
@endsection

@section('scripts')
	<script>

	$('.list').DataTable({
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
   	});
		
	$(document).ready(function(){

    $('body').on('click', 'button.editar', function(){

      	$('input.descripcion_empresa').hide();
      	$('span.descripcion_empresa').show();
      	$('button[type="submit"]').hide();
      	$('button[type="button"]').show();
      	$(this).parent().parent().find('span.descripcion_empresa').hide();
      	$(this).parent().parent().find('input.descripcion_empresa').show();
      	$(this).hide();
      	$(this).parent().parent().find('button[type="submit"]').show();
    });

      $('form.tipo_evaluacion_form').submit(function(){

				$(this).find('.descripcion_empresa').val($(this).parent().parent().find('input.descripcion_empresa').val());
			});
		});
	</script>
@endsection