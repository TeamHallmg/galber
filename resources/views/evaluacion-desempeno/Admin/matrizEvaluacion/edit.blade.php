@extends('layouts.app')

@section('title', 'Matriz de Evaluación')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }}" alt="">
		
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Matriz de Evaluación
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
 
 
<div class="row margin-top-20">
{{-- , 'onSubmit' => 'return validaFormulario()' --}}

	<div class="row margin-top-20" id="formEvaluacion">
		{!! Form::open(['route' => ['Matriz-Evaluacion.update', $id], 'method' => 'PUT', 'onSubmit' => 'return validaFormulario()'])!!}
		<div class="col-md-12">
			@if($countPeriodos > 1)
				<div class="col-md-4 form-group" align="center">
					<div class="row-fluid" >
						<label>Periodo</label>
      			<br>
      			<select class="selectpicker" data-show-subtext="true" data-live-search="true" name="periodo" id="idPeriodo">
      				<option data-subtext="" value="0">Selecciona una opcion</option>
							@foreach($periodosPreparatorios as $periodosPreparatorio)
								@if($editEvaluadorEvaluado->id_periodo == $periodosPreparatorio->id)
									<option data-subtext="{{ $periodosPreparatorio->id }}" value="{{ $periodosPreparatorio->id }}" selected="selected">{{ $periodosPreparatorio->name }}</option>
								@else
									<option data-subtext="{{ $periodosPreparatorio->id }}" value="{{ $periodosPreparatorio->id }}">{{ $periodosPreparatorio->name }}</option>
								@endif
							@endforeach
	      		</select>
	    		</div>
				</div> 
				<div class="col-md-4 form-group" align="center">
				<div class="row-fluid" >
					<label>Evaluador</label>
      		<br>
      		<select class="selectpicker" data-show-subtext="true" data-live-search="true" name="evaluador" id="idEvaluador">
      			<option data-subtext="" value="0">Selecciona una opcion</option>
						@foreach($usuarios as $user)
							@if($user->id == 1)
							@else
								@if($editEvaluadorEvaluado->id_evaluador == $user->id)
									<option data-subtext="{{ $user->id }}" value="{{ $user->id }}" selected="selected">{{ $user->fullname }}</option>
								@else
									<option data-subtext="{{ $user->id }}" value="{{ $user->id }}">{{ $user->fullname }}</option>
								@endif
							@endif
						@endforeach
	      	</select>
	    	</div>
				</div>
				<div class="col-md-4 form-group" align="center">
				<div class="row-fluid">
					<label>Evaluado</label>
					<br>
      		<select class="selectpicker" data-show-subtext="true" data-live-search="true" name="evaluado" id="idEvaluado">
      			<option data-subtext="" value="0">Selecciona una opcion</option>
						@foreach($usuarios as $user)
							@if($user->id == 1)
							@else
								@if($editEvaluadorEvaluado->id_evaluado == $user->id)
									<option data-subtext="{{ $user->id }}" value="{{ $user->id }}" selected="selected">{{ $user->fullname }}</option>
								@else
									<option data-subtext="{{ $user->id }}" value="{{ $user->id }}">{{ $user->fullname }}</option>
								@endif
							@endif
						@endforeach
	      	</select>
	    	</div>
				</div>
			@else
				<div class="col-md-6 form-group" align="center">
				<div class="row-fluid" >
					<label>Evaluador</label>
      		<br>
      		<select class="selectpicker" data-show-subtext="true" data-live-search="true" name="evaluador" id="idEvaluador">
      			<option data-subtext="" value="0">Selecciona una opcion</option>
						@foreach($usuarios as $user)
							@if($user->id == 1)
							@else
								@if($editEvaluadorEvaluado->id_evaluador == $user->id)
									<option data-subtext="{{ $user->id }}" value="{{ $user->id }}" selected="selected">{{ $user->fullname }}</option>
								@else
									<option data-subtext="{{ $user->id }}" value="{{ $user->id }}">{{ $user->fullname }}</option>
								@endif
							@endif
						@endforeach
	      	</select>
	    	</div>
				</div>
				<div class="col-md-6 form-group" align="center">
				<div class="row-fluid">
					<label>Evaluado</label>
					<br>
      		<select class="selectpicker" data-show-subtext="true" data-live-search="true" name="evaluado" id="idEvaluado">
      			<option data-subtext="" value="0">Selecciona una opcion</option>
						@foreach($usuarios as $user)
							@if($user->id == 1)
							@else
								@if($editEvaluadorEvaluado->id_evaluado == $user->id)
									<option data-subtext="{{ $user->id }}" value="{{ $user->id }}" selected="selected">{{ $user->fullname }}</option>
								@else
									<option data-subtext="{{ $user->id }}" value="{{ $user->id }}">{{ $user->fullname }}</option>
								@endif
							@endif
						@endforeach
	      	</select>
	    	</div>
				</div>
			@endif
			<div class="form-group" align="center">
				{{--  
				{!! Form::submit('Guardar Evaluador/Evaluado', ['class' => 'btn btn-primary'])!!}
				--}}
				{!! Form::hidden('id_periodo', $periodo) !!}
				{!! Form::submit('Actualizar Evaluador/Evaluado', ['class' => 'btn btn-primary btn-Finvivir-yellow']) !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
		<br>
		<br>
</div>
</div>
</div>

<!-- Modal -->
<div id="modalContainer" class="modal fade">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          
          <h4 class="modal-title text-center text-warning"><span class="glyphicon glyphicon-warning-sign"></span>&nbsp;&nbsp;&nbsp;ADVERTENCIA</h4>
        </div>
        <div class="modal-body">
          <p id="modalMessage"></p>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
@endsection
@section('scripts')
<script>
	var contPeriodos = {{ $countPeriodos }};
	
	function validaFormulario(){
		var evaluador = $('select[name="evaluador"]').val();
		var evaluado = $('select[name="evaluado"]').val();
		var periodo = $('select[name="periodo"]').val();

		if(contPeriodos > 1){
			if(periodo == 0){
				$('#modalMessage').html('Debes seleccionar un periodo.');
				$('#modalContainer').modal('show');
				return false;
			}else if(evaluador == 0){
				$('#modalMessage').html('Debes seleccionar a un evaluador.');
				$('#modalContainer').modal('show');
				return false;
			}else if(evaluado == 0){
				$('#modalMessage').html('Debes seleccionar a un evaluado.');
				$('#modalContainer').modal('show');
				return false;
			}else{
				
				return true;
			}
		}else{
			if(evaluador == 0){
				$('#modalMessage').html('Debes seleccionar a un evaluador.');
				$('#modalContainer').modal('show');
				return false;
			}else if(evaluado == 0){
				$('#modalMessage').html('Debes seleccionar a un evaluado.');
				$('#modalContainer').modal('show');
				return false;
			}else{
		
				return true;
			}
		}
	}
	
</script>
@endsection