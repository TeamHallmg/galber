@extends('layouts.app')

@section('title', 'Importar Evaluador/Evaluado por Archivo')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }}" alt="">
		<br>
		<br>
		@if(!empty($csvData) && $countCsvData > 2)
			<div class="col-md-12" align="center">
				{!! Form::open(['route' => ['importacion-evaluadores-csv.store'], 'method' => 'POST', 'onsubmit' => 'return validaFormulario()']) !!}
				<div class="col-md-12">
					{!! Form::hidden('reemplazar', 'reemplazar') !!}
					{!! Form::hidden('id_periodo', $_POST['id_periodo'], ['class' => 'form-control']) !!}
					{!! Form::submit('Importar', ['class' => 'btn btn-primary btn-Finvivir-red']) !!}

				@if (!empty($preguntar_borrar))

					<h3>¿Borrar Red de Interacción? <input type="checkbox" name="borrar_red" value="1" style="margin-left: 10px; -ms-transform: scale(2); -moz-transform: scale(2); -webkit-transform: scale(2); -o-transform: scale(2);"></h3>
				@endif

					<div class="text-center">
						<a href="/importacion-evaluadores-csv?id_periodo={{$_POST['id_periodo']}}" class="btn btn-success">Regresar</a>
					</div>
					
					{{--  {!! Form::select('accion', ['seleccion' => 'Seleccione una opción...','reemplazar' => 'Reemplazar', 'nuevo' => 'Cargar Nuevo Archivo'], 'seleccion', ['class' => 'form-control selectAcciones']) !!}--}}
				</div>
				{!! Form::close() !!}
				<br>
				<br>
				<table width="100%" style="font-size: 13px" class="table table-striped">
					<thead class="cabeceras-tablas-evaluaciones">
						<tr>
							<th style="color: white; padding: 5px 10px; text-align: center; border: 1px solid white;">No.</th>
							<th style="color: white; padding: 5px 10px; text-align: center; border: 1px solid white;">ID Evaluador</th>
							<th style="color: white; padding: 5px 10px; text-align: center; border: 1px solid white;">Nombre Evaluador</th>
							<th style="color: white; padding: 5px 10px; text-align: center; border: 1px solid white;">ID Evaluado</th>
							<th style="color: white; padding: 5px 10px; text-align: center; border: 1px solid white;">Nombre Evaluado</th>
							<th style="color: white; padding: 5px 10px; text-align: center; border: 1px solid white;">Tipo</th>
						</tr>
					</thead>
					<tbody>
						
		<?php $contador = 0;

					foreach ($csvData as $key => $data){

						if ($key > 1){
							
							$user1 = $user2 = 0;
									
							for ($i = 0;$i < count($usuarios);$i++){

								if ($data[0] == $usuarios[$i]->idempleado){
									
									$user1 = $usuarios[$i];
										  
								  if (!empty($user2)){
											
										break;
									}
								}

								if ($data[2] == $usuarios[$i]->idempleado){

									$user2 = $usuarios[$i];

									if (!empty($user1)){
												  
										break;
									}
								}
							}
							
							if (!empty($user1) && !empty($user2)){

								$tipo_evaluacion = '';

								if (!empty($data[4])){

                  foreach ($tipos_evaluacion as $key => $tipo){
                  
                    if (strpos($tipo->descripcion_empresa, $data[4]) !== false || $data[4] == $tipo->id){

                      $tipo_evaluacion = $tipo->descripcion_empresa;
                      break;
                    }
                  }
                }

								$contador++; ?>

						<tr>
							<td>{{ $contador}}</td>
							<td>{{ $user1->idempleado }}</td>
							<td>{{ str_replace('Ã‘','Ñ',$user1->user->fullname) }}</td>
							<td>{{ $user2->idempleado }}</td>
							<td>{{ str_replace('Ã‘','Ñ',$user2->user->fullname) }}</td>
							<td>{{ $tipo_evaluacion }}</td>
						</tr>
				<?php }
						}
					} ?>

					</tbody>
				</table>
				<br>
				<div class="col-md-12" align="center"><a href="{{ URL::previous() }}" class="btn btn-danger">Regresar</a></div>
				
			</div>
		@endif
	</div>
</div>

<!-- Modal -->
<div id="modalContainer" class="modal fade">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          
          <h4 class="modal-title text-center text-warning"><span class="glyphicon glyphicon-warning-sign"></span>&nbsp;&nbsp;&nbsp;ADVERTENCIA</h4>
        </div>
        <div class="modal-body">
          <p id="modalMessage"></p>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
@endsection

@section('scripts')
<script>

	//var countCsvData = {{ $countCsvData }};

	function validaFormulario(){

		var selectAccion = $('select[name="accion"]').val();

		if(selectAccion == "seleccion"){
			$('#modalMessage').html('Debes seleccionar una opción.');
			$('#modalContainer').modal('show');
			return false;
		}else{
			return true;
		}
	}

	/*$('button.agregar_archivo').on('click', function(e) {
		e.preventDefault();

		var archivo = $('.archivo_import').val();

		var btn = $(this);
		var form = $('#importar-evaluador-evaluado');
		var url = form.attr('action');
		var data = form.serialize();


		if(archivo == ""){
			$('#modalMessage').html('No debe de estar el campo vacio del archivo.');
			$('#modalContainer').modal('show');
		}else{
			$.post(url, data, function (reply){
					if(reply.success){
						/*$('#modalMessage').html(reply.msn);
						$('#modalContainer').modal('show');
						location.reload(true);
					}else{
						$('#modalMessage').html(reply.msn);
						$('#modalContainer').modal('show');
					}
				});
		}
	});*/

	$('.estadisticas').DataTable({
		'order': [[0,'asc']],
		"language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
	});
</script>
@endsection