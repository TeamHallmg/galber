@extends('layouts.app')

@section('title', 'Estadisticas')

@section('content')
<style>div.dt-buttons {
	float: right;
	margin-left:10px;
	display: none;
	}</style>
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>

	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">

		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Estado del Avance
				<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>


			</h5>
			<div class="card-body">
  
			
		@if (count($periodos) > 0)
		
		<input type="hidden" value="{{ number_format($porcentajeNoIniciado, 1, '.', ',') }}" id="num_noiniciada">
		<input type="hidden" value="{{ number_format($porcentajeIniciado, 1, '.', ',') }}" id="num_inconclusa">
		<input type="hidden" value="{{ number_format($porcentajeTerminado, 1, '.', ',') }}" id="num_terminada">
		<input type="hidden" value="{{ number_format($porcentajeRetro, 1, '.', ',') }}" id="num_retro">
		<input type="hidden" value="{{ $noIniciado }} / {{ $totalEvaluaciones }}" id="text_noiniciada">
		<input type="hidden" value="{{ $iniciado }} / {{ $totalEvaluaciones }}" id="text_inconclusa">
		<input type="hidden" value="{{ $terminado }} / {{ $totalEvaluaciones }}" id="text_terminada">
		<input type="hidden" value="{{ $total_retros }} / {{ $totalEvaluados }}" id="text_retro">
		<input type="hidden" value="{{ $totalEvaluados }}" id="text_retro_p">

		<div class="row mb-5">
			<div class="col-md-6">
			
				<div class="card">
					<h5 class="card-header bg-primary text-white font-weight-bolder">Avance</h5>
					<div class="card-body">
						
						<div id="porc_encuentas" style="min-width: 260px; height: 260px; margin: 0 auto"></div>

					</div>
				</div>

			</div>
			<div class="col-md-6">
			
				<div class="card">
					<h5 class="card-header bg-primary text-white font-weight-bolder">Retro</h5>
					<div class="card-body">
						
						<div id="porc_retro" style="min-width: 260px; height: 260px; margin: 0 auto"></div>

					</div>
				</div>

			</div>
		</div>
		 
		<hr>
		
		<div class="row mb-3"> 
			<div class="col-12 offset-md-4 col-md-3 text-center">
				<div class="form-group">
					<label for="id_plan" class="font-weight-bolder mb-0">Periodo:</label>							
					<select class="form-control periodos">
						@foreach($periodos as $periodo)

						<option value="{{$periodo->id}}" <?php if ($id_periodo == $periodo->id){ ?>selected="selected"<?php } ?>>{{$periodo->descripcion}}</option>
					@endforeach
							
					</select>
				</div>
			</div> 
		</div>

		@endif

		<div class="card border-0">
			<h5 class="card-header bg-primary text-white font-weight-bolder">Resultados
				<button class="btn btn-success float-right text-left mr-2 btn-excel" title="Exportar a Excel">
					<i class="fa fa-file-excel"></i> Exportar a Excel
				</button>
			</h5>
			<div class="card-body px-0"> 
		

			<div class="row">
		 

			<div class="col-md-3 offset-md-9 mb-3">
				<select id="field" class="form-control">
				<option value="-1">-- Columna en cual buscar --</option>
				<option value="0">ID Evaluador</option>
				<option value="1">Evaluador</option>
				<option value="6">ID Evaluado</option>
				<option value="7">Evaluado</option>
					</select>
				</div>
			</div>

			
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped table-bordered stats dt-responsive">
				<thead style="background-color: #222B64; color:white;">
					<tr>
						<th>ID Evaluador</th>
						<th>Evaluador</th>
						<th>Departamento</th>
						<th>Puesto</th>
						<th>Rol de Evaluador</th>
						<th>ID Evaluado</th>
						<th>Evaluado</th>
						<th>Departamento</th>
						<th>Puesto</th>
						<th>Estado</th>
					</tr>
				</thead>
				<tbody>
					@if($periodos->count() > 0)
						@foreach($estadisticas as $estadistica)

							<tr>
								<td class="text-center">
									{{ (!empty($estadistica->nameCompleteUserEvaluador->employee_wt->idempleado) ? $estadistica->nameCompleteUserEvaluador->employee_wt->idempleado : '') }}
								</td>
								<td class="text-center">
									{{ (!empty($estadistica->nameCompleteUserEvaluador->fullname) ? str_replace('Ã‘','Ñ',$estadistica->nameCompleteUserEvaluador->fullname) : '') }}
								</td>
								<td class="text-center" style="display: none">
									{{ (!empty($estadistica->nameCompleteUserEvaluador->employee_wt->jobPosition->area->department) ? str_replace('Ã‘','Ñ',$estadistica->nameCompleteUserEvaluador->employee_wt->jobPosition->area->department->name) : '') }}
								</td>
								<td class="text-center" style="display: none">
									{{ (!empty($estadistica->nameCompleteUserEvaluador->employee_wt->jobPosition) ? str_replace('Ã‘','Ñ',$estadistica->nameCompleteUserEvaluador->employee_wt->jobPosition->name) : '') }}
								</td>
								<td class="text-center" style="display: none">
									{{ (!empty($estadistica->tipoEvaluacion->descripcion) ? $estadistica->tipoEvaluacion->descripcion_empresa : ($estadistica->id_evaluador == $estadistica->id_evaluado ? $tipos_evaluacion[1] : (!empty($estadistica->nameCompleteUserEvaluado->employee_wt->boss) && $estadistica->id_evaluador == $estadistica->nameCompleteUserEvaluado->employee_wt->boss->user->id ? $tipos_evaluacion[2] : (!empty($estadistica->nameCompleteUserEvaluador->employee_wt->boss) && $estadistica->id_evaluado == $estadistica->nameCompleteUserEvaluador->employee_wt->boss->user->id ? $tipos_evaluacion[5] : (!empty($estadistica->nameCompleteUserEvaluador->employee_wt) && !empty($estadistica->nameCompleteUserEvaluado->employee_wt) && $estadistica->nameCompleteUserEvaluado->employee_wt->jefe == $estadistica->nameCompleteUserEvaluador->employee_wt->jefe ? $tipos_evaluacion[3] : $tipos_evaluacion[4]))))) }}
								</td>
								<td class="text-center">
									{{ (!empty($estadistica->nameCompleteUserEvaluado->employee_wt->idempleado) ? $estadistica->nameCompleteUserEvaluado->employee_wt->idempleado : '') }}
								</td>
								<td class="text-center">
									{{ (!empty($estadistica->nameCompleteUserEvaluado->fullname) ? str_replace('Ã‘','Ñ',$estadistica->nameCompleteUserEvaluado->fullname) : '') }}
								</td>
								<td class="text-center" style="display: none">
									{{ (!empty($estadistica->nameCompleteUserEvaluado->employee_wt->jobPosition->area->department) ? str_replace('Ã‘','Ñ',$estadistica->nameCompleteUserEvaluado->employee_wt->jobPosition->area->department->name) : '') }}
								</td>
								<td class="text-center" style="display: none">
									{{ (!empty($estadistica->nameCompleteUserEvaluado->employee_wt->jobPosition) ? str_replace('Ã‘','Ñ',$estadistica->nameCompleteUserEvaluado->employee_wt->jobPosition->name) : '') }}
								</td>
								<td class="text-center">
									@if (!empty($estadistica->retro))
										<span class="btn btn-primary" style="width: 100%">Retro</span>
									@else
										@if($estadistica->status == 'No Iniciada')
										<span class="btn btn-danger">No Iniciada</span>
										@else
											@if($estadistica->status == 'Iniciada')
										<span class="btn btn-warning">Inconclusa</span>
											@else
										<span class="btn btn-success">Terminada</span>
											@endif
										@endif
									@endif
								</td>
							</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
	</div>
	</div>

	<form action="/estadisticas" method="post" class="form_estadisticas">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="id_periodo" value="{{$id_periodo}}" class="id_periodo">
		<input type="submit" style="display: none">
	</form>
	</div>
	</div>
	</div>
	</div>
@endsection

@section('scripts')
<script>

	var table = '';

	$(document).ready(function(){


		$('.btn-excel').click(function(){ 
			$('.buttons-excel').trigger('click');
		});

		$(function() {
        // Create the chart 

		var num_noiniciada = parseFloat($('#num_noiniciada').val());
		var text_noiniciada = $('#text_noiniciada').val();
		var num_inconclusa = parseFloat($('#num_inconclusa').val());
		var text_inconclusa = $('#text_inconclusa').val();
		var num_terminada = parseFloat($('#num_terminada').val()); 
		var text_terminada = $('#text_terminada').val();

		var num_retro = parseFloat($('#num_retro').val()); 
		var text_retro = $('#text_retro').val();
		var text_retro_p = $('#text_retro_p').val();
		var num_retro_p = (100 - num_retro).toFixed(2);
 
		// var users_approved_percentage = parseFloat($('#users_approved_percentage').val());
		// var users_non_approved_percentage = parseFloat($('#users_non_approved_percentage').val());
		
        chart = Highcharts.chart('porc_encuentas', {
			chart: {
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false,
				type: 'pie'
			},
			credits: {
				enabled: false
			},
			title: {
				// text: '<b>80%</b>',
				// y:225
				text: null
			},
			legend:{
				enabled:true
			},
			tooltip: {
				pointFormat: '<b>{point.percentage:.1f}%</b>'
				// pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					allowPointSelect: false,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						formatter:function(){
							return this.y + '%';
						}
					},
					showInLegend: true
				}
			},
			series: [{
				name: 'Avance',
				colorByPoint: true,
				innerSize: '70%',
				data: [
					{
						name: 'No Iniciada <br>( '+ text_noiniciada + ' )',
						color: '#c51f1a',
						y: num_noiniciada
					}, 
					{
						name: 'Inconclusa <br>( '+ text_inconclusa + ' )',
						color: '#ffed4a',
						y: num_inconclusa
					}, 
					{
						name: 'Terminada <br>( '+ text_terminada + ' )',
						color: '#38c172',
						y: num_terminada
					}
				]
			}]
		});

        chart2 = Highcharts.chart('porc_retro', {
			chart: {
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false,
				type: 'pie'
			},
			credits: {
				enabled: false
			},
			title: {
				// text: '<b>80%</b>',
				// y:225
				text: null
			},
			legend:{
				enabled:true
			},
			tooltip: {
				pointFormat: '<b>{point.percentage:.1f}%</b>'
				// pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					allowPointSelect: false,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						formatter:function(){
							return this.y + '%';
						}
					},
					showInLegend: true
				}
			},
			series: [{
				name: 'Retro',
				colorByPoint: true,
				innerSize: '70%',
				data: [
					{
						name: 'Retro <br> (' + text_retro +')',
						color: '#2176bd',
						y: num_retro,
					},
					{
						name: 'Pendientes <br> (' + text_retro_p+')',
						color: '#c51f1a',
						y: num_retro_p,
					}
				]
			}]

		});


    });















		table = $('.stats').DataTable({
      	
					dom:"<'row'<'col-md-6'l><'col-md-6'Bf>>" +
"<'row'<'col-md-6'><'col-md-6'>>" +
"<'row'<'col-md-12't>><'row'<'col-md-12'ip>>",
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  },
      		  
	  buttons: [
        {
          			extend: 'excel',
					text: 'Exportar a Excel',
					titleAttr: 'Exportar a Excel',
					title: 'Evaluaciones de Desempeño',
					
				}
        ]

  	});

		$('body').on('keyup', '#DataTables_Table_0_wrapper #DataTables_Table_0_filter label input', function(){

    	var busqueda = $(this).val();
    	var numero_columna = $('select#field').val() * 1;

    	if (numero_columna != -1){

    		table.columns().search('').draw();
      	table.columns([numero_columna]).search(busqueda).draw();
    	}
  	});
		
		$('select#field').change(function(){

    	var busqueda = $('#DataTables_Table_0_wrapper #DataTables_Table_0_filter label input').val();
    	var numero_columna = $(this).val() * 1;

    	if (numero_columna != -1){

      	table.columns().search('').draw();
      	table.columns([numero_columna]).search(busqueda).draw();
    	}

    	else{

    		table.columns().search('').draw();
    	}
  	});

  	$('select#periodos').change(function(){

  		$('form.form_estadisticas .id_periodo').val($(this).val());
  		$('form.form_estadisticas').submit();
  	});

  });


</script>
@endsection