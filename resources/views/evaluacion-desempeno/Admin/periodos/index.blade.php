@extends('layouts.app')

@section('title', 'Periodos')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Periodos
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
  

<?php // No existen periodos preparatorios
			if (!$preparatorio){ ?>


		@if (auth()->user()->role === 'admin' || Auth::user()->hasRolePermission('performance_evaluation_admin'))
		<div class="row">
					 
			<div class="text-center col">

				<a href="/periodos/create" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Agregar Periodo</a>
				
			</div>
		</div>
		@endif

<?php } ?>

<div class="row">
					 
	<div class=" col">
		
		<table class="table table-striped table-bordered periodos">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th>Status</th>
					<th>Descripción</th>
					<th>Fecha de Inicio</th>
					<th>Fecha de Cierre</th>
					<th>Modalidad</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>

			@for ($i = 0; $i < count($periodos); $i++)

				<tr>
					<td class="text-center">
						<h5><span class="badge" style="background-color: <?php echo ($periodos[$i]->status != 'Abierto' ? '#4B4B4D' : '#0090DA')?>; color: white">{{$periodos[$i]->status}}</span></h5>
					</td>
					<td>{{$periodos[$i]->descripcion}}</td>
					<td>{{$periodos[$i]->fecha_inicio}}</td>
					<td>{{$periodos[$i]->fecha_cierre}}</td>
					<td>{{$periodos[$i]->nombre}}</td>
					<td class="text-center">

				@if (auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin'))

						<a href="/periodos/{{$periodos[$i]->id}}/edit" class="btn btn-primary"><i class="fas fa-edit"></i> Editar</a> <?php if ($periodos[$i]->status == 'Cancelado'){ ?><a href="/borrar-periodo/{{$periodos[$i]->id}}" class="btn btn-danger">Eliminar</a><?php } ?>
				@endif
					</td>
				</tr>
			@endfor
			</tbody>
		</table>
	</div>
	</div>
	</div>
</div>
</div>
</div>


@endsection
@section('scripts')

<script type="text/javascript">
	$('.periodos').DataTable({
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
   	});
</script>

@endsection