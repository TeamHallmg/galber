@extends('layouts.app')

@section('title', 'Crear Periodo')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">

		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Crear Periodo
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
  

		<form action="/crear-periodo" method="post" class="periodos">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="row">
        <div class="col-md-6">
          
        <div class="form-group">
          <label for="descripcion" class="font-weight-bold">Descripción</label>
          <input type="text" class="form-control" name="descripcion" required>
        </div>
      </div>
      <div class="col-md-6">
        
        <div class="form-group">
          <label for="fecha_inicio" class="font-weight-bold">Fecha de Inicio</label>
          <input type="date" class="form-control" name="fecha_inicio" required>
        </div>
      </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="fecha_cierre" class="font-weight-bold">Fecha de Cierre</label>
        <input type="date" class="form-control" name="fecha_cierre" required>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="id_modalidad" class="font-weight-bold">Modalidad</label>
        <select class="form-control" name="modalidad">
          <option value="1">90°</option>
          <option value="2">180°</option>
          <option value="3">270°</option>
          <option value="4">360°</option>
        </select>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="status" class="font-weight-bold">Status</label>
        <select class="form-control" name="status">
			    <option value="Preparatorio">Preparatorio</option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
	  <div class="form-group">
        <label for="status" class="font-weight-bold">Factores</label>
        <div>
          <table class="table table-striped table-bordered table-factores dt-responsive">
            <thead style="background-color: #222B64; color:white;">
              <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Orden</th>
                <th class="mx-auto">Todos <input type="checkbox" class="select_all"></th>
              </tr>
            </thead>
            <tbody>

      <?php foreach ($factores as $key => $factor){ ?>

              <tr>
                <td><?php echo $factor->id?></td>
                <td><?php echo $factor->nombre?></td>
                <td><?php echo $factor->orden?></td>
                <td>
                  <input type="checkbox" value="<?php echo $factor->id?>" class="factores">
                </td>
              </tr>
      <?php } ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-12">
			<div class="form-group">
        <button class="btn btn-success mr-3" type="submit" style="color: white"><span class="fas fa-check-circle"></span> Crear</button> 
        <a class="btn btn-danger" href="/periodos"><span class="fas fa-times-circle"></span> Regresar</a>
      </div>
      </div>
      </div>
    </form>

</div>
</div>
</div>
</div>
@endsection

@section('scripts')
  <script>
	  
	var factors_table = $('.table-factores').DataTable({
    language: {
        "sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
            }
    });
    
    $(document).ready(function(){

      $('.select_all').click(function(){
          //var cells = questions_table.cells().nodes();
          var cells = factors_table.rows({ search: 'applied' }).nodes();
            if ($(this).prop('checked')){
                //$('.preguntas').prop('checked', true);
                $(cells).find(':checkbox').prop('checked', true);
            }else{
                //$('.preguntas').prop('checked', false);
                $(cells).find(':checkbox').prop('checked', false);
            }
        });

        // Handle form submission event
   $('form.periodos').on('submit', function(e){
      var form = this;

      //checkboxes should have a general class to traverse
      var rowcollection = factors_table.$(".factores:checked", {"page": "all"});

      var checkbox_value = 0;

      //Now loop through all the selected checkboxes to perform desired actions
      rowcollection.each(function(index,elem){
      //You have access to the current iterating row
        checkbox_value = $(elem).val();
        $(form).append(
             $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'factores[]')
                .val(checkbox_value)
         );
      });
    });
  });
  </script>
@endsection
