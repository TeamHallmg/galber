@extends('layouts.app')

@section('title', 'Estadística de Avance')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-responsive" src="{{ asset('img/evaluacion-desempeno/que-es/ban.png') }}" alt="">
	
		<div class="col-md-12 margin-top-20 table-responsive">
			
			<table class="table table-striped data-table">
				<caption align="center" >Evaluaciones</caption>
				<thead>
					<tr>
						<th>#</th>
						<th>Evaluador</th>
						<th>#</th>
						<th>Evaluado</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach($calificaciones as $cal)
						<tr>
							<td>
								{{ $cal->id_evaluador }}
							</td>
							<td>
								{{ $cal->nameCompleteUserEvaluador->fullname }}
							</td>
							<td>
								{{ $cal->id_evaluado }}
							</td>
							<td>
								{{ $cal->nameCompleteUserEvaluado->fullname }}
							</td>
							<td>
								@if($cal->status == 'No Iniciada')
									<span class="btn btn-danger">No Iniciada</span>
								@elseif($cal->status == 'Iniciada')
									<span class="btn btn-warning">Iniciada</span>
								@else
									<span class="btn btn-success">Terminada</span>
								@endif
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
	
	$(document).ready(function(){

		$('table.data-table').DataTable({
			"language": {
            	"url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        	},
      		'order': [[4,'asc']]
   		});
	});
</script>
@endsection