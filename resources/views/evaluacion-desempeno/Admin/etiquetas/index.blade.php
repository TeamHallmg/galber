@extends('layouts.app')

@section('title', 'Etiquetas')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Etiquetas
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
  
		@if (auth()->user()->role === 'admin' || Auth::user()->hasRolePermission('performance_evaluation_admin'))
		<div class="row">
		<div class="col text-center">
		<a href="/etiquetas/create" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Agregar Etiqueta</a>
	</div></div>
		@endif

	<div class="row mt-5">
		<div class="col-md-12">
		<table class="table table-striped table-bordered etiquetas dt-responsive">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th>Nombre</th>
					<th>Valor</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>

			@for ($i = 0;$i < count($etiquetas);$i++)

				<tr>
					<td>{{$etiquetas[$i]->name}}</td>
					<td>{{$etiquetas[$i]->valor}}</td>
					<td class="text-center">

				@if (auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin'))

						<a href="/etiquetas/{{$etiquetas[$i]->id}}/edit" class="btn btn-primary"><i class="fas fa-edit"></i> Editar</a>
				@endif
					</td>
				</tr>
			@endfor
			</tbody>
		</table>
	</div>
	</div>
	</div>
</div>
</div>
</div>


@endsection
@section('scripts')

<script type="text/javascript">
	$('.etiquetas').DataTable({
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
   	});
</script>

@endsection