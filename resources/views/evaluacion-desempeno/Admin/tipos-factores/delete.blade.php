@extends('layouts.app')

@section('title', 'Borrar Tipo de Factor')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }}" alt="">

	</div>
	<div class="col-md-12">
		<h4 class="margin-top-20 titulos-evaluaciones">Borrar Tipo de Factor</h4>
	</div>
</div>
<div class="row margin-top-20">
	<div class="col-md-3"></div>
	<div class="col-md-6 text-center">
		<form action="/borrar-tipo-factor" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id" value="{{$tipoFactor[0]->id}}">
			<div class="form-group">
        <label for="nombre">¿De verdad quieres eliminar el tipo de factor {{$tipoFactor[0]->name}}?</label>
      </div>
			<div class="form-group">
        <button class="btn btn-danger" type="submit">Borrar</button> <a href="/tipos-factores" class="btn btn-primary">Cancelar</a>
      </div>
    </form>
	</div>
	<div class="col-md-3"></div>
</div>
@endsection
