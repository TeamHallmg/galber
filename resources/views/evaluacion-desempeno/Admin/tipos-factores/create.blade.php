@extends('layouts.app')

@section('title', 'Crear Tipo de Factor')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">

		<img class="img-fluid" src="{{ asset('../img/evaluacion_desempeno.png') }} " alt="">
    
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Crear Tipo de Factor
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
  
		<form action="/crear-tipo-factor" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form-group">
        <label for="name" class="font-weight-bold">Nombre</label>
        <input type="text" class="form-control" name="name" required>
      </div>
			<div class="form-group">
        <button class="btn btn-success mr-3" type="submit" style="color: white"><span class="fas fa-check-circle"></span> Crear</button> 
        <a class="btn btn-danger" href="/tipos-factores"><span class="fas fa-times-circle"></span> Regresar</a>
      </div>
    </form>

</div>
</div>
</div>
</div>
@endsection