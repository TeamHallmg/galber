@extends('layouts.app')

@section('title', 'Niveles de Puestos')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Niveles de Puestos
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
 
 
		@if (auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin'))
		<div class="row">
		<div class="col-md-12 text-center">
			<a class="btn btn-primary" href="{{ url('puestos/create') }}"><i class="fas fa-plus-circle"></i> Nuevo Nivel de Puesto</a>
		</div>
		</div>
		@endif 
		<div class="row">
			<div class="col-md-12">
		<table width="100%" class="table table-striped table-bordered puestos">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th>ID</th>
					<th>Nivel de puesto</th>
					<th>Puesto -- Departamento</th>
					<th>Mando</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>

					<tr>
						<td></td>
						<td>Otros</td>
						<td>
				@if (!empty($job_positions))
					@foreach ($job_positions as $job_position)
							{{$job_position->name}} {{ (!empty($job_position->area->department) ? ' -- ' . $job_position->area->department->name : '') }}
							<br>
					@endforeach
				@endif
						</td>
						<td>No</td>
						<td></td>
					</tr>

      	@foreach($nivelesPuestos as $nPuestos)
      		<tr>
						<td>{{ $nPuestos->id }}</td>
						<td>{{ str_replace('Ã‘','Ñ',$nPuestos->Name) }}</td>
						<td>
							@foreach($puestos_NivelesPuestos as $nivelPuestos)
								@foreach($nivelPuestos as $nivelPuesto)
									@if($nPuestos->id == $nivelPuesto->id_nivel_puesto)
										{{ str_replace('Ã‘','Ñ',$nivelPuesto->Puestos->name) }} {{ (!empty($nivelPuesto->Puestos->area->department) ? ' -- ' . $nivelPuesto->Puestos->area->department->name : '') }}
										<br>
									@endif
								@endforeach
							@endforeach
						</td>
						<td>
							@if( $nPuestos->mando == 0 )
								No
							@else
								Si
							@endif
						</td>
						<td nowrap>
							@if (auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin'))
								<a class="btn btn-primary" href="{{ url('puestos/' . $nPuestos->id . '/edit') }}" title="Editar"><i class="fas fa-edit"></i> </a>
							@endif
							{!! Form::open(['route' => ['puestos.destroy', $nPuestos->id],'class'=>'d-inline', 'method' => 'DELETE'])!!}
								{{--{!! Form::submit('<i class="fas fa-delete"></i> Borrar', ['class' => 'btn btn-danger', 'onclick' => 'return confirm("¿Esta seguro de borrar?")'])!!}--}}
								{{ Form::button('<i class="fas fa-trash"></i> ', array('title' => 'Borrar', 'type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => 'return confirm("¿Está seguro de borrar?")')) }}
							{!! Form::close() !!}
							  {{--<a class="btn btn-danger" onclick="return confirm('¿Está seguro  eliminar?')" id='{{ $nPuestos->id }}' style="background-color: #555759; border-color: #555759" href="{{ url('puestos/' . $nPuestos->id . '/delete') }}">Borrar</a>--}}
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	</div>
	</div>
</div>
</div>
</div>
@endsection
@section('scripts')
<script>
	$('.puestos').DataTable({
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
   	});
</script>
@endsection