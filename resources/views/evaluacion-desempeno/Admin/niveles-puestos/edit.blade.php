@extends('layouts.app')

@section('title', 'Editar niveles de puesto')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Editar Nivel de Puestos
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
  
		{!! Form::open(['route' => ['puestos.update', $id], 'method' => 'PUT', 'id' => 'job_positions_form'])!!}
			<div class="row">
				<div class="col-md-9">
					<div class="form-group">
						{!! Form::label('nombreNivelPuesto', 'Nombre del nivel de puesto: ') !!}
						{!! Form::text('nombreNivelPuesto', $nombreNP,  ['class' => 'form-control', 'required'])!!}
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					{!! Form::label('mandoNivelPuesto', 'Mando: ') !!}
					{!! Form::select('mandoNivelPuesto', array(0 => 'No', 1 => 'Si'), $mando, ['class' => 'form-control', 'required']); !!}
					</div>
				</div>
			</div>


				<div class="form-group">
						<table class="table table-hover table-striped table-bordered td-responsive" id="table-puestos">
							<thead style="background-color: #222B64; color:white;">
								<tr>
									<th>
										ID
									</th>
									<th >
										Nombre
									</th>
									<th >
										Departamento
									</th>
									<th>
										Todos
										<input type="checkbox" class="select_all_job_positions" id="job_positions_check">
									</th>
								</tr>
							</thead>
							<tbody>
								@if($puestos->isEmpty())
									<tr>
										<td colspan="3">No hay puestos para seleccionar.</td>
									</tr>
								@endif
								@foreach($puestos as $key => $puesto)

									@if (!in_array($puesto->id, $selectedOtros))
										<tr>
											<td>{{ $puesto->id }}</td>
											<td>{{ str_replace('Ã‘','Ñ',$puesto->name) }}</td>
											<td>{{ (!empty($puesto->area->department) ? $puesto->area->department->name : '') }}</td>
											<td>
												{!! Form::checkbox($puesto->name, $puesto->id, in_array($puesto->id, $selected), ['class' => 'field']) !!}
											</td>
										</tr>
									@endif
								@endforeach
							</tbody>
						</table>
				</div>

				<div class="form-group">
					{{ Form::button('<i class="fas fa-check-circle"></i> Guardar', array('type' => 'submit', 'class' => 'btn btn-success mr-3')) }}
					<a class="btn btn-primary" href="{{ URL::previous() }}"><span class="fas fa-chevron-circle-left"></span> Regresar</a>
				</div>
		{!! Form::close() !!}
		
	</div>
</div>
</div>
</div>
@endsection

@section('scripts')
<script>
	
	var checked = false;
	var job_positions_table = $('#table-puestos').DataTable({
    language: {
        "sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
            }
    });
	
	$('#table-puestos tbody tr').click(function (e) {
    if(!$(e.target).is('#table-puestos td input:checkbox'))
    $(this).find('input:checkbox').trigger('click');
});

	$('#job_positions_form').on('submit', function(e){
    var job_positions_array = job_positions_table.$('input').serializeArray();
		var job_positions = [];
		$.each(job_positions_array, function(i, field){
			job_positions.push(this.value);
		});
		if(job_positions.length > 0){
				$(this).append(
					$('<input>')
						.attr('type', 'hidden')
						.attr('name', 'id_puestos')
						.val(job_positions)
				);
			}
	});

	$('body').on('click', '.select_all_job_positions', function(){
            
    var allPstarted_ats = job_positions_table.rows({ search: 'applied' }).nodes();
    checked = !checked;
           
    if (checked){

      $('input[type="checkbox"]', allPstarted_ats).prop('checked', true);
    }

    else{

      $('input[type="checkbox"]', allPstarted_ats).prop('checked', false);
    }
	});
</script>
@endsection