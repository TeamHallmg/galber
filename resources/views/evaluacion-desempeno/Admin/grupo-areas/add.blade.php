@extends('layouts.app')

@section('title', 'Crear grupo de areas')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
		
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Crear Grupo de Áreas
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
  
		{!! Form::open(['route' => 'areas.store', 'method' => 'POST'])!!}
		@if(!$departments->isEmpty())
				<div class="form-group">
					{!! Form::label('nombreGrupoArea', 'Nombre del Grupo de Áreas: ') !!}
					{!! Form::text('nombreGrupoArea', null,  ['class' => 'form-control', 'required'])!!}
				</div>
		@endif
			<div class="form-group">
					<table class="table table-hover table-striped table-bordered departments dt-responsive" id="table-departments">
						<thead style="background-color: #222B64; color:white;">
							<tr>
								<th>
									ID
								</th>
								<th>
									Nombre
								</th>
								<th>
									Acción
								</th>
							</tr>
						</thead>
						<tbody>

							@foreach($departments as $department)
								<tr>
									<td>{{ $department->id }}</td>
									<td>{{ $department->name }}</td>
									<td align="center">
										{!! Form::checkbox('id_departments[]', $department->id, null, ['class' => 'field']) !!}
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				
			</div>
			<div class="form-group">
				@if(!$departments->isEmpty())
					{{ Form::button('<i class="fas fa-check-circle"></i> Guardar', array('type' => 'submit', 'class' => 'btn btn-success mr-3')) }}
				@endif
				<a class="btn btn-primary" href="{{ URL::previous() }}"><span class="fas fa-chevron-circle-left"></span> Regresar</a>
			</div>
			{!! Form::close() !!}

	</div>
</div>
</div>
</div>
@endsection

@section('scripts')
<script>
$('.departments').DataTable({
    language: {
        "sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
            }
    });


	$('#table-departments tbody tr').click(function (e) {
    if(!$(e.target).is('#table-departments td input:checkbox'))
    $(this).find('input:checkbox').trigger('click');
});
</script>
@endsection