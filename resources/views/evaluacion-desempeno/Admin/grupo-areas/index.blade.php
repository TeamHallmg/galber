@extends('layouts.app')

@section('title', 'Grupos de Áreas')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Grupos de Áreas
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
 
 
		@if (auth()->user()->role === 'admin' || Auth::user()->hasRolePermission('performance_evaluation_admin'))

		<div class="row">
		<div class="col-md-12 text-center">
			<a class="btn btn-primary" href="{{ url('areas/create') }}"><i class="fas fa-plus-circle"></i> Nuevo Grupo de Áreas</a>
		</div>
		</div>
		@endif
		<div class="row">
		<div class="col-md-12">
		<table width="100%" class="table table-striped table-bordered departments">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th>ID</th>
					<th>Áreas</th>
					<th>Departamentos</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>

					<tr>
						<td></td>
						<td>Otros</td>
						<td>
		@if (!empty($departments))
			@foreach ($departments as $department)
							{{$department->name}}
							<br>
			@endforeach
		@endif
						</td>
						<td></td>
					</tr>

		@if (count($gruposAreas) > 0)

      	@foreach($gruposAreas as $grupoArea)
      		<tr>
						<td>{{ $grupoArea->id }}</td>
						<td>{{ $grupoArea->Name }}</td>
						<td>
							@foreach($departments_gruposAreas as $departmentsGrupo)
								@foreach($departmentsGrupo as $departmentGrupo)
									@if($grupoArea->id == $departmentGrupo->id_grupos_areas)
										{{ $departmentGrupo->Departments->name }}
										<br>
									@endif
								@endforeach
							@endforeach
						</td>
						<td>

							@if (auth()->user()->isAdminOrHasRolePermission('performance_evaluation_admin'))
								<a class="btn btn-primary" href="{{ url('areas/' . $grupoArea->id . '/edit') }}"><i class="fas fa-edit"></i> Editar</a>
							@endif
							{!! Form::open(['route' => ['areas.destroy', $grupoArea->id], 'method' => 'DELETE'])!!}
								{{--{!! Form::submit('<i class="fas fa-delete"></i> Borrar', ['class' => 'btn btn-danger', 'onclick' => 'return confirm("¿Está seguro de borrar?")'])!!}--}}
								{{ Form::button('<i class="fas fa-window-close"></i> Borrar', array('type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => 'return confirm("¿Está seguro de borrar?")')) }}
							{!! Form::close() !!}
							  {{--<a class="btn btn-danger" onclick="return confirm('¿Está seguro de eliminar?')" id='{{ $grupoArea->id }}' style="background-color: #555759; border-color: #555759" href="{{ url('areas/' . $grupoArea->id . '/delete') }}">Borrar</a>--}}
						</td>
					</tr>
				@endforeach
		@endif

			</tbody>
		</table>
	</div>
	</div>
	</div>
</div>
</div>
</div>
@endsection
@section('scripts')
<script>
	$('.departments').DataTable({
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
   	});
</script>
@endsection