@extends('layouts.app')

@section('title', 'Estadisticas con Areas y Puestos')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-responsive" src="{{ asset('img/evaluacion-desempeno/que-es/ban.png') }}" alt="">
	
		<div class="col-md-12 margin-top-20 table-responsive">
			<h3 class="titulos-evaluaciones" style="margin-bottom: 20px">Lista de usuarios</h3>
			<table class="table table-striped estadisticasAP">
				<thead class="cabeceras-tablas-evaluaciones">
					<tr>
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">ID Evaluador</th>
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Evaluador</th>
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">ID Evaluado</th>
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Evaluado</th>
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Grupo de Area</th>
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Nivel de Puesto</th>
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Estado</th>
					</tr>
				</thead>
				<tbody>
					@if($periodoStatus->isEmpty())
						<tr>
							<td colspan="7" class="text-center">
								No existe un periodo Abierto para mostrar a los usuarios.
							</td>
						</tr>
					@endif
					@if($periodoStatus->count() == 1)
						@foreach($estadisticasAP as $estAP)
							<tr>
								<td class="text-center">
									{{ (!empty($estAP->id_evaluador) ? $estAP->id_evaluador : '') }}
								</td>
								<td class="text-center">
									{{(!empty($estAP->nameCompleteUserEvaluador->fullname) ? $estAP->nameCompleteUserEvaluador->fullname : '') }}
								</td>
								<td class="text-center">
									{{ (!empty($estAP->id_evaluado) ? $estAP->id_evaluado : '') }}
								</td>
								<td class="text-center">
									{{(!empty($estAP->nameCompleteUserEvaluado->fullname) ? $estAP->nameCompleteUserEvaluado->fullname : '') }}
								</td>
								
								<td class="text-center">
								@if (!empty($estAP->areaEvaluado->grupoArea))
									@foreach($estAP->areaEvaluado->grupoArea as $aEvaluado)
										{{ (!empty($aEvaluado->GrupoAreas->Name) ? $aEvaluado->GrupoAreas->Name : '') }}
									@endforeach
								@endif
								</td>
								<td class="text-center">
								@if (!empty($estAP->puestoEvaluado->idPuestoNivelPuesto))
									@foreach($estAP->puestoEvaluado->idPuestoNivelPuesto as $pEvaluador)
										@foreach($pEvaluador->Niveles as $pEvaluadoN)
											@if(empty($pEvaluadoN))
											@else
												{{ $pEvaluadoN->Name }}
											@endif
										@endforeach
									@endforeach
								@endif
								</td>
								<td class="text-center">
									@if($estAP->status == 'No Iniciada')
										<span class="btn btn-danger">No Iniciada</span>
									@elseif($estAP->status == 'Iniciada')
										<span class="btn btn-warning">Iniciada</span>
									@else
										<span class="btn btn-success">Terminada</span>
									@endif
								</td>
							</tr>
						@endforeach
					@endif
					@if($periodoStatus->count() > 1)
						@foreach($periodoStatus as $periodoStatu)
							@foreach($estadisticasAP as $estAP)
								@if($periodoStatu->id == $estAP->id_periodo)
									<tr>
										<td class="text-center">
											{{ $estAP->id_evaluador }}
										</td>
										<td class="text-center">
											{{$estAP->nameCompleteUserEvaluador->fullname }}
										</td>
										<td class="text-center">
											{{ $estAP->id_evaluado }}
										</td>
										<td class="text-center">
											{{$estAP->nameCompleteUserEvaluado->fullname }}
										</td>
										<td class="text-center">
											@foreach($estAP->areaEvaluado->grupoArea as $aEvaluado)
												{{ $aEvaluado->GrupoAreas->Name }}
											@endforeach
										</td>
										<td class="text-center">
											@foreach($estAP->puestoEvaluado->idPuestoNivelPuesto as $pEvaluador)
												@foreach($pEvaluador->Niveles as $pEvaluadoN)
													@if(empty($pEvaluadoN))
													@else
														{{ $pEvaluadoN->Name }}
													@endif
												@endforeach
											@endforeach
										</td>
										<td class="text-center">
											@if($estAP->status == 'No Iniciada')
												<span class="btn btn-danger">No Iniciada</span>
											@elseif($estAP->status == 'Iniciada')
												<span class="btn btn-warning">Iniciada</span>
											@else
												<span class="btn btn-success">Terminada</span>
											@endif
										</td>
									</tr>
								@endif
							@endforeach
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
	$('.estadisticasAP').DataTable({
	  	'order': [[0,'asc']]
	  	"language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
   });
</script>
@endsection