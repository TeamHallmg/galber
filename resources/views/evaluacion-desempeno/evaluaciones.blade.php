@extends('layouts.app')

@section('title', 'Evaluación de Desempeño')

@section('content')

	<div class="row">
		<div class="col-md-2 text-right">
			@include('evaluacion-desempeno/partials/sub-menu')
		</div>
		<div class="col-md-10">
			<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Evaluaciones
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
   
				<div class="row mb-3">
					<div class="col-md-12 table-responsive">
				<table class="table table-striped table-bordered evaluadores  ">
					<thead style="background-color: #222B64; color:white;">
						<tr>
							<th>ID</th>
							<th>Evaluado</th>
							<th>Departamento</th>
							<th>Puesto</th>
							<th>Foto</th>
							<th>Tipo de Evaluación</th>
							<th>Estado</th>
						</tr>
					</thead>
					<tbody>
						@if($periodoStatus->isEmpty())
							<!--<tr>
								<td colspan="5" class="text-center">
									El proceso esta cerrado y no puedes seguir evaluando.
								</td>
							</tr>-->
						@endif
						@if($periodoStatus->count() == 1 && !$evaluados->isEmpty())
							@foreach($evaluados as $eval)
								<tr>
									<td class="text-center">
										{{ $eval->nameCompleteUserEvaluado->employee_wt->idempleado }}
									</td>
									<td class="text-center">
										{{ str_replace('Ã‘','Ñ',$eval->nameCompleteUserEvaluado->fullname) }}
									</td>
									<td class="text-center">
										{{ (isset($eval->nameCompleteUserEvaluado->employee_wt->jobPosition->area->department) ? $eval->nameCompleteUserEvaluado->employee_wt->jobPosition->area->department->name : '') }}
									</td>
									<td class="text-center">
										{{ (isset($eval->nameCompleteUserEvaluado->employee_wt->jobPosition) ? $eval->nameCompleteUserEvaluado->employee_wt->jobPosition->name : '') }}
									</td>
									{{-- <td>
										@foreach($gruposAreas as $key => $grupoAreas)
	          					@foreach($grupoAreas as $grupoArea)
	          						@if($key == 0)
	            						{{ $grupoArea->GrupoAreas->nombre }}
	            					@endif
	          					@endforeach
	        					@endforeach
									</td>
									<td>
										@foreach($gruposPuestos as $key => $grupoPuestos)
	          						@foreach($grupoPuestos as $grupoPuesto)
	          							@if($grupoPuestos[$key])
	          								{{ $grupoPuesto->Niveles->nombre }}
													@else
	            							{{ 'Vacio' }}
	            						@endif
	          						@endforeach
	        					@endforeach
									</td> --}}
									<td class="text-center">
										@if(File::exists(public_path() . '/img/evaluacion-desempeno/' . $eval->id_evaluado . '.jpg'))
											<img class="img-responsive center-img" width="150" src="	{{ asset('/img/evaluacion-desempeno/' . $eval->id_evaluado . '.jpg' ) }}" alt="{{ $eval->id_evaluado }}" >
										@else
											{{--  <img class="img-responsive center-img" width="150" src="{{ asset('/img/evaluacion-desempeno/' . $eval->id_evaluado . '.png' ) }}" alt="{{ $eval->id_evaluado }}" >--}}
										@endif
									</td>
									<td class="text-center">
										{{-- @elseif($eval->nameEvaluaciones->boss_id == auth()->user()->boss_id) --}}
										@if (!empty($eval->tipoEvaluacion->descripcion_empresa))
											{{ ($eval->tipoEvaluacion->nombre == 'SUBORDINADO' ? $tipos_evaluacion[5] : ($eval->tipoEvaluacion->nombre == 'JEFE' ? $tipos_evaluacion[2] : $eval->tipoEvaluacion->descripcion_empresa))}}
										@elseif(auth()->user()->id == $eval->id_evaluado)
											{{$tipos_evaluacion[1]}}
										@elseif(auth()->user()->employee_wt->idempleado == $eval->nameCompleteUserEvaluado->employee_wt->jefe)
											{{$tipos_evaluacion[2]}}
										@elseif(!empty(auth()->user()->employee_wt->boss) && auth()->user()->employee_wt->boss->user->id == $eval->id_evaluado)
											{{$tipos_evaluacion[5]}}
										@elseif(auth()->user()->employee_wt->jefe == $eval->nameCompleteUserEvaluado->employee_wt->jefe)
											{{$tipos_evaluacion[3]}}
										@elseif(auth()->user()->employee_wt->job_position_id == $eval->nameCompleteUserEvaluado->employee_wt->job_position_id)
											{{$tipos_evaluacion[6]}}
										@else
											{{$tipos_evaluacion[4]}}
										@endif
									</td>
									{{--<td>
									  	@if($eval->status == 'No Iniciada')
												@if($eval->status == 'No Iniciada')
													<span class="label label-danger">No Iniciada</span>
									  		@elseif($eval->status == 'Iniciada')
												@elseif($eval->status == 'Iniciada')
													<span class="label label-warning">Iniciada</span>
												@else
													<span class="label label-success">Terminada</span>
												@endif
									</td>--}}
									<td class="text-center" nowrap>
										@if($eval->status == 'No Iniciada')
											<a class="btn btn-danger" href="{{ url('evaluado', $eval->id_evaluado) }}">No Iniciada</a>
										@elseif($eval->status == 'Iniciada')
											<a class="btn btn-warning" href="{{ url('evaluado', $eval->id_evaluado) }}">Inconclusa</a>
							
										@else
											<a class="btn btn-success" href="{{ url('evaluado', $eval->id_evaluado) }}">Terminada</a>
										@endif
									</td>
								</tr>
							@endforeach
						{{--@else
							<tr>
								<td colspan="5" class="text-center">
									No tienes usuarios a evaluar.
								</td>
							</tr>--}}
						@endif
						@if($periodoStatus->count() > 1 && !$evaluados->isEmpty())
							@foreach($periodoStatus as $periodoStatu)
								@foreach($evaluados as $eval)
									@if($periodoStatu->id == $eval->id_periodo && $eval->id_evaluador == auth()->user()->id)
										<tr>
											<td class="text-center">
												{{ $eval->id_evaluado }}
											</td>
											<td class="text-center">
												{{ $eval->nameCompleteUserEvaluado->fullname }}
											</td>
											{{-- <td>
												@foreach($gruposAreas as $key => $grupoAreas)
	          							@foreach($grupoAreas as $grupoArea)
	          								@if($key == 0)
	            								{{ $grupoArea->GrupoAreas->nombre }}
	            							@endif
	          							@endforeach
	        							@endforeach
											</td>
											<td>
												@foreach($gruposPuestos as $key => $grupoPuestos)
	          							@foreach($grupoPuestos as $grupoPuesto)
	          								@if($grupoPuestos[$key])
	          									{{ $grupoPuesto->Niveles->nombre }}
														@else
	            								{{ 'Vacio' }}
	            							@endif
	          							@endforeach
	        							@endforeach
											</td> --}}
											<td class="text-center">
												@if(File::exists(public_path() . '/img/evaluacion-desempeno/' . $eval->id_evaluado . '.jpg'))
													<img class="img-responsive center-img" width="150" src="	{{ asset('/img/evaluacion-desempeno/' . $eval->id_evaluado . '.jpg' ) }}" alt="{{ $eval->id_evaluado }}" >
												@else
													{{--  <img class="img-responsive center-img" width="150" src="{{ asset('/img/evaluacion-desempeno/' . $eval->id_evaluado . '.png' ) }}" alt="{{ $eval->id_evaluado }}" >--}}
												@endif
											</td>
											<td class="text-center">
												{{-- @elseif($eval->nameEvaluaciones->boss_id == auth()->user()->boss_id) --}}
												@if (!empty($eval->tipoEvaluacion->descripcion_empresa))
													{{ ($eval->tipoEvaluacion->nombre == 'COLABORADOR' ? $tipos_evaluacion[2] : ($eval->tipoEvaluacion->nombre == 'JEFE' ? $tipos_evaluacion[5] :  $eval->tipoEvaluacion->descripcion_empresa))}}
												@elseif(auth()->user()->id == $eval->nameEvaluaciones->id)
													{{$tipos_evaluacion[1]}}
												@elseif(auth()->user()->id == $eval->nameEvaluaciones->employee_wt->boss->user->id)
													{{$tipos_evaluacion[5]}}
												@elseif(!empty(auth()->user()->employee->boss->user->id) && auth()->user()->employee->boss->user->id == $eval->nameEvaluaciones->id)
													"$tipos_evaluacion[2]"
												@elseif(auth()->user()->employee->jefe == $eval->nameEvaluaciones->employee_wt->jefe)
													{{$tipos_evaluacion[3]}}
												@elseif(auth()->user()->employee->job_position_id == $eval->nameEvaluaciones->employee_wt->job_position_id)
													{{$tipos_evaluacion[6]}}
												@else
													{{$tipos_evaluacion[4]}}
												@endif
											</td>
											{{--<td>
									  		@if($eval->status == 'No Iniciada')
												@if($eval->status == 'No Iniciada')
													<span class="label label-danger">No Iniciada</span>
									  		@elseif($eval->status == 'Iniciada')
												@elseif($eval->status == 'Iniciada')
													<span class="label label-warning">Iniciada</span>
												@else
													<span class="label label-success">Terminada</span>
												@endif
											</td>--}}
											<td class="text-center">
												@if($eval->status == 'No Iniciada')
													<a class="btn btn-danger" href="{{ url('evaluado', $eval->id_evaluado) }}">No Iniciado</a>
												@elseif($eval->status == 'Iniciada')
													<a class="btn btn-warning" href="{{ url('evaluado', $eval->id_evaluado) }}">Iniciado</a>
												@else
													<button class="btn btn-success">Terminado</button>
												@endif
											</td>
										</tr>
									@endif
								@endforeach
							@endforeach
						{{--@else
							<tr>
								<td colspan="5" class="text-center">
									No tienes usuarios a evaluar.
								</td>
							</tr>--}}
						@endif
					</tbody>
				</table>
			
		</div>
		</div>
		</div>
	</div>
	</div>
	</div>

@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){

		$('.evaluadores').DataTable({
			language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
  	});
		
	});	
</script>
@endsection