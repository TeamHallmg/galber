@extends('layouts.app')

@section('title', 'Lista de Empleados')

@section('content')
<div class="row">

	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>

	<div class="col-md-10">
			<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
		
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Lista de Empleados
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
  
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			
			<table class="table table-striped table-bordered listado  	" style="width: auto !important">
				<thead style="background-color: #222B64; color:white;">
					<tr>
						<th>ID</th>
						<th>Nombre</th>
						<th>Correo</th>
						<!--<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Correo</th>-->
						<th>Puesto</th>
						<th>Departamento</th>
						<th>Jefe</th>
						<th>Accion</th>
					</tr>
				</thead>
				<tbody>
						
				@foreach($users as $user)
							
					@if (!empty($user->employee))
					<tr>
						<td class="text-center">{{ $user->employee->idempleado }}</td>
						<td class="text-center">{{ str_replace('Ã‘','Ñ',$user->fullname) }}</td>
						<td class="text-center">{{ str_replace('Ã‘','Ñ',$user->employee->correoempresa) }}</td>
						<td class="text-center">{{ (isset($user->employee->jobPosition) ? $user->employee->jobPosition->name : '') }}</td>
						<td class="text-center">{{ (isset($user->employee->jobPosition->area->department) ? $user->employee->jobPosition->area->department->name : '') }}</td>
						<td class="text-center">{{(!empty($user->employee->boss) ? str_replace('Ã‘','Ñ',$user->employee->boss->user->fullname) : '') }}</td>
						<td class="text-center" nowrap>

						@if (Auth::user()->isAdminOrHasRolePermission('performance_evaluation_admin'))

							<a class="btn btn-primary" href="/cambiar-jefe/{{$user->id}}"><i class="fas fa-edit"></i> Cambiar Jefe</a>
							<a class="btn btn-primary" href="/cambiar-contrasena/{{$user->id}}">Cambiar Contraseña</a>
						@endif
					
						</td>
					</tr>
					@endif
				@endforeach
					
				</tbody>
			</table>
		
		</div>
	</div>
</div>
	</div>
</div>
</div>
</div>
@endsection

@section('scripts')
<script>
	$('.listado').DataTable({
      'order': [[0,'asc']],
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
   });
</script>
@endsection