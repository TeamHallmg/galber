@extends('layouts.app')

@section('title', 'Cambio de Contraseña')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }}" alt="">
	
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Cambio de Contraseña para {{str_replace('Ã‘','Ñ',$user[0]->first_name)}} {{str_replace('Ã‘','Ñ',$user[0]->last_name)}}
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
 
		<div class="col-md-12 mt-5 table-responsive">
			<form class="form-horizontal cambiar_contrasena" role="form" method="POST" action="/cambiar-contrasena/{{$user[0]->id}}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<label class="col-md-4 control-label">Nueva Contraseña</label>
					<div class="col-md-6">
						<input type="password" class="form-control nueva_contrasena" name="nueva_contrasena" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Confirmar Contraseña</label>
					<div class="col-md-6">
						<input type="password" class="form-control confirm_password" name="confirm_password" required>
						<div>
							<span style="font-size: 10px">Mínimo 8 caracteres, una letra mayúscula y un caracter especial (!"#$%&/()=?¿¡)</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="btn btn-primary" style="margin-right: 15px;">Cambiar</button>
						<a href="{{ url()->previous() }}" class="btn btn-danger">Regresar</a>

					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</div>
</div>
@endsection

@section('scripts')
<script>

	var sesion = <?php echo (Session::has('primer_login') ? '1' : '0')?>;

	$(document).ready(function(){

		if (sesion == 1){

			alert('Debes cambiar tu contraseña antes de seguir a cualquier otra sección');
		}

		$('.nueva_contrasena').focus();

		$('form.cambiar_contrasena').submit(function(){

			var contrasena = $('.nueva_contrasena').val().trim();

			if (contrasena != $('.confirm_password').val()){

				alert('Las contraseñas no coinciden');
				$('.nueva_contrasena').focus();
				return false;
			}

			if (contrasena.length < 8){

				alert('La contraseña debe contener mínimo 8 caracteres');
				$('.nueva_contrasena').focus();
				return false;
			}

			if (contrasena.search(/[A-Z]/) == -1){

				alert('La contraseña debe contener al menos una letra mayúscula');
				$('.nueva_contrasena').focus();
				return false;
			}

			if (contrasena.search(/[\!\@\#\$\%\^\&\*\(\)\_\+\-\%\"\'\!\/\=\?\¿\¡\|\.\,\;\:\<\>\{\}\[\]]/) == -1){

				alert('La contraseña debe contener al menos un caracter especial');
				$('.nueva_contrasena').focus();
				return false;
			}
		});
	});
</script>
@endsection