@extends('layouts.app')

@section('title', 'Cambio de Jefe')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
		
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Cambio de Jefe para {{str_replace('Ã‘','Ñ',$user->first_name)}} {{str_replace('Ã‘','Ñ',$user->last_name)}}
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
 
	
		<div class="col-md-12 table-responsive">
		
			<form class="form-horizontal cambiar_jefe" role="form" method="POST" action="/cambiar-jefe/{{$user->id}}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group text-center">
					<label class="control-label">Seleccione Jefe</label>
					<div>
						<select class="form-control nuevo_jefe" name="nuevo_jefe" required style="width: auto; display: inline-block">

			<?php foreach ($users as $key => $value){ 

							if (!empty($value->employee)){ ?>
			
							<option value="{{$value->id}}" <?php if (!empty($user->employee->boss) && $value->employee->idempleado == $user->employee->jefe){ ?>selected="selected"<?php } ?>>{{str_replace('Ã‘','Ñ',$value->fullname)}}</option>
						
				<?php }
						} ?>

						</select>
					</div>
				</div>
				<div class="form-group text-center">
					<div>
						<button type="submit" class="btn btn-success"><span class="fas fa-check-circle"></span> Guardar Cambios</button>
						<a href="{{ url()->previous() }}" class="btn btn-danger">Regresar</a>

					</div>
				</div>
			</form>
		</div>
	</div>
	</div>
	</div>
</div>
@endsection