@extends('layouts.login')

@section('content')
    <div class="container">
    <div class="row d-flex justify-content-center align-items-center">
        <div class="card-container col-10 col-xs-10 col-sm-10 col-md-10 col-lg-5">
            <div class="card login-card animated zoomIn card-3">
                <div class="card-header card-header-log border-0 d-flex justify-content-center">
                    <img class="img-fluid" src="{{asset('/img/logo login.png')}}" alt="">
                </div>
                <div class="card-body card-body-login border-0">
                    <form method="POST" action="{{ route('check_employee') }}" aria-label="{{ __('Login') }}" autocomplete="off">
                        @csrf

                        <div class="form-group row">
                            <div class="col-3 col-md-2">
                                <span class="d-flex align-items-start" style="font-size: 3.5em"><i class="fas fa-address-card "></i></span>
                            </div>
                            <div class="col-9 col-md-10 my-auto d-flex flex-column align-items-center">
                                <input id="number" type="text" class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}{{ $errors->has('msg') ? ' is-invalid' : '' }}" name="number" placeholder="Ingresa tu número de empleado" value="{{ old('number') }}" required autofocus>
                                @if ($errors->has('number'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('number') }}</strong>
                                </span>
                                @elseif($errors->has('msg'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('msg') }}</strong>
                                    </span>
                                @endif
                            </div>
                           
                        </div>

                        <div class="form-group row mb-2">
                            <div class="col text-center">
                                <button type="submit" class="btn btn-primary col-12 font-weight-bold card-1">
                                    <span style="font-size:1.5em;">{{ __('Ingresar') }}</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
