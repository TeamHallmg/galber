@extends('layouts.login')

@section('content')
    <div class="container">
    <div class="row d-flex justify-content-center align-items-center">
        <div class="card-container col-10 col-xs-10 col-sm-10 col-md-10 col-lg-5">
            <div class="card animated zoomIn card-3">
                <div class="card-header bg-white mt-3 border-0 d-flex justify-content-center">
                    <img class="img-fluid w-50" src="{{asset('/img/logo.png')}}" alt="">
                </div>
                <div class="card-body rounded border-0">
                    @if($messages->isNotEmpty())
                        @foreach ($messages as $message)
                            <div class="alert alert-warning m-0" role="alert">
                                <button type="button" class="close" id="btn-seen" data-notification={{ $message->id }} data-dismiss="alert" aria-label="See">
                                    <span aria-hidden="true"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                </button>
                                <h4 class="alert-heading">{{ $message->title }}!!</h4>
                                {{ $message->description }}
                            </div>
                        @endforeach
                    @endif
                    <div class="row px-5">
                        <div class="col-12 col-md-12 my-4 py-4 bg-blue rounded">
                            <h1 class="text-white font-weight-bold m-0 text-center">¡Registro<br> exitoso!</h1>
                        </div>
                    </div>
                    <div class="row d-flex align-items-center">
                        <div class="col-2 col-md-4">
                            <img class="img-fluid rounded border-blue" src="{{ asset($photo) }}" alt="">
                        </div>
                        <div class="col-10 col-md-8">
                            <span class="text-secondary">Nombre:</span><br>
                            <span class="text-dark">{{ $name }}</span><br>
                            <span class="text-secondary">Número de empleado:</span><br>
                            <span class="text-dark">{{ $number }}</span>
                        </div>
                    </div>
                    <a href="{{ route('checklogin') }}" class="btn btn-primary btn-block mt-4" id="btn-return">
                        Regresando en ...7
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    const URL_MARK_NOTIFICATION = "{{ url('api/mark_notification') }}";
    const URL_LOGIN = "{{ url('checklogin') }}";
    const EMPLOYEE_NUMBER = "{{ $number }}";
    let GLOBAL_TIMER = 7;

    var interval = setInterval(check_countdown, 1000);

    function check_countdown(){
        $('#btn-return').html(`Regresando en ...${--GLOBAL_TIMER}`);
        if(GLOBAL_TIMER === 0){
            clearInterval(interval);
            location.href = URL_LOGIN;
        }
    }

    $(function(){
        $('#btn-seen').on('click', function(){
            const notificationID = $(this).data('notification');
            axios.post(URL_MARK_NOTIFICATION + '/' + notificationID);
        });
    });
</script>
@endsection