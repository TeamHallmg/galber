@extends('layouts.login')

@section('content')
    <div class="container">
    <div class="row d-flex justify-content-center align-items-center">
        <div class="card-container col-10 col-xs-10 col-sm-10 col-md-10 col-lg-5">
            <div class="card animated zoomIn card-3">
                <div class="card-header bg-white mt-3 border-0 d-flex justify-content-center">
                    <img class="img-fluid w-50" src="{{ asset('/img/logo.png') }}" alt="">
                </div>
                <div class="card-body rounded border-0">
                    <form method="POST" action="{{ route('confirm_employee') }}" aria-label="{{ __('Login') }}">
                        @csrf
                        <input type="hidden" name="number" value="{{ $number }}">
                        <div class="form-group row my-3">
                            <div class="col text-center">
                                <img class="img-fluid rounded border-blue w-50" onclick="document.querySelector('form').submit()" style="cursor: pointer" src="{{ asset($photo) }}" alt="">
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <div class="col text-center">
                                <a href="{{ route('checklogin') }}">
                                    <span style="font-size:1em;">{{ __('No eres tu ?.') }}</span>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
