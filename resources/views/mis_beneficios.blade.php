@extends('layouts.app')

@section('content')
<div class="card card-2 border-0">
    <div class="card-body">
        <div class="container-fluid"> 
            <div class="flash-message" id="mensaje">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-'.$msg))
                        <p class="alert alert-{{ $msg }}">{!! Session::get('alert-'.$msg) !!} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                @endforeach
            </div>
            <div class="row mb-4">
                <div class="col bg-blue rounded-xl pt-3 py-1 px-5">
                    <p class="text-white font-weight-bold font-italic announcement-title">
                        <i class="fas fa-thumbs-up"></i> Nuestros Beneficios
                    </p>
                </div>
            </div>
            {{-- <div class="row mb-5 justify-content-center">
                    @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner']])
            </div> --}}
            <div class="row">
                <div class="col-12 col-md-12 table-responsive">
                    @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['blog']])

                    {{-- <div class="card mb-4">
                        <div class="card-header bg-blue text-white text-center">
                            <h3 class="font-weight-bold m-0">PRESTACIONES</h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-borderless m-0">
                                <tbody>
                                    <tr>
                                        <td class="pb-0">
                                            <p><span style="color: rgb(36, 67, 149); font-weight: bold;">DE LEY</span></p>
                                            <ul style="background-color: rgb(242, 242, 242); padding-top: 10px; padding-bottom: 10px;">
                                                <li><strong>IMSS:</strong> Acceso a seguridad social y sus múltiples beneficios.</li>
                                                <li><strong>Utilidades:</strong> Realizamos el pago de las utilidades generadas en el año anterior cada mes de mayo.</li>
                                                <li><strong>Prima vacacional:</strong> Pago del .25% de los días solicitados de vacaciones.</li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-borderless m-0">
                                <tbody>
                                    <tr>
                                        <td class="pb-0">
                                            <p><span style="color: rgb(36, 67, 149); font-weight: bold;">SUPERIORES DE LEY</span></p>
                                            <ul style="background-color: rgb(242, 242, 242); padding-top: 10px; padding-bottom: 10px;">
                                                <li><strong>Aguinaldo:</strong> En el mes de diciembre recibirás el pago de 30 días de aguinaldo proporcional a tu sueldo y acorde a tu fecha de ingreso.</li>
                                                <li><strong>Vales de despensa:</strong> Recibirás mensualmente, los primeros 5 días del mes, $750 pesos o lo proporcional a los días trabajados en el mes, en tu tarjeta de Sí Vale y podrás verlo reflejado en el recibo de nómina los días 15 del mes. (Descarga la APP donde podrás monitorear tu saldo).</li>
                                                <li><strong>Seguro de gastos médicos en contra accidentes (SEGURO MTY):</strong> Este seguro te cubrirá en cualquier tipo de accidente que sea fortuito y cumpla las condiciones de la aseguradora. Para utilizarlo contacta a CH para que asesore, recuerda que funciona como reembolso.</li>
                                                <li><strong>Seguro de vida (SEGURO MTY):</strong> Se te otorga un seguro de vida al cumplir un año en la empresa.</li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="card mb-4">
                        <div class="card-header bg-blue text-white text-center">
                            <h3 class="font-weight-bold m-0">VACACIONES POR AÑO</h3>
                        </div>
                        <div class="card-body">
                            <h5 class="font-weight-bold text-blue text-center mb-4">Otorgamos 4 días de vacaciones fijos que son superiores a la ley.</h5>
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td style="background-color: rgb(242, 242, 242);">
                                            <p style="margin-bottom: 0px;">Diciembre</p>
                                        </td>
                                        <td style="background-color: rgb(211, 239, 251); text-align: center;">
                                            <p style="margin-bottom: 0px;">24 y 31</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="background-color: rgb(242, 242, 242);">
                                            <p style="margin-bottom: 0px;">Jueves y viernes santo</p>
                                        </td>
                                        <td style="background-color: rgb(211, 239, 251); text-align: center;">
                                            <p style="margin-bottom: 0px;">-</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header bg-blue text-white text-center">
                            <h3 class="font-weight-bold m-0">BENEFICIOS UCIN</h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-borderless m-0">
                                <tbody>
                                    <tr style="background-color: rgb(242, 242, 242);">
                                        <td class="px-4 pb-0">
                                            <p class="mb-0"><span style="color: rgb(36, 67, 149); font-weight: bold;">Préstamos personales:</span> Puedes solicitar préstamos por medio de Ucin Médica o de empresas financieras con las cuales tengamos convenio.</p>
                                        </td>
                                    </tr>
                                    <tr style="background-color: rgb(242, 242, 242);">
                                        <td class="py-0">
                                            <ul style=" padding-top: 10px; padding-bottom: 10px;">
                                                <li><strong>Préstamo Ucin:</strong> Puedes solicitar hasta medio mes de sueldo bruto y programar tus pagos en 8 quincenas.</li>
                                                <li><strong>Préstamo por medio de caja de ahorro:</strong> Solicítalo a través del comité de caja de ahorro.</li>
                                                <li><strong>¿Te interesa un préstamo por medio de financiera?</strong> (Agregar el dato de contacto de estos) HSBC (teléfono y correo) Sofom</li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-borderless m-0">
                                <tbody>
                                    <tr>
                                        <td class="px-4">
                                            <p class="mb-0"><span style="color: rgb(36, 67, 149); font-weight: bold;">Acceso a becas:</span> UCIN apoyará a sus colaboradores con BECAS por hasta el 50% del costo de la inscripción y mensualidad para continuar con el nivel de estudios solicitado.</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-borderless m-0">
                                <tbody>
                                    <tr style="background-color: rgb(242, 242, 242);">
                                        <td class="px-4">
                                            <p class="mb-0"><span style="color: rgb(36, 67, 149); font-weight: bold;">Matriz Flex Time:</span> Acorde a las características de tu puesto podrás acceder a diferentes esquemas de horarios y permisos. Puedes encontrar mayor información en nuestro SGC y solicitar los permisos a tavés de esta plataforma.</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-borderless m-0">
                                <tbody>
                                    <tr>
                                        <td class="px-4">
                                            <p class="mb-0"><span style="color: rgb(36, 67, 149); font-weight: bold;">Caja de ahorro:</span> Con el objetivo de promover finanzas sanas en nuestros colaboradores ponemos a su disposición la caja de ahorro donde anualmente recibirán el monto acumulado y sus rendimientos. Los estatutos los podrás encontrar en nuestro SGC.</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> --}}


                </div>
            </div>
        </div>
        
    </div>
</div>
@endsection