@extends('layouts.app')

@section('title', 'Administrar Usuarios')

@section('content')

<div class="row mb-3">
    <div class="col-12 bg-company-primary text-white rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Administrar Usuarios | Resumen de Importación</h2>
    </div>
</div>
<div class="row mb-2">
	<div class="col-12 mt-1 mb-3">
		<h4 class="text-white font-weight-bold m-0 rounded-pill bg-company-secondary px-3 py-1 d-flex justify-content-between">
			Datos a Importados
		</h4>
	</div>
</div>

<div class="table-responsive">
	<div class="col-md-12 tableWrapper d-none">
        <table class="table table-bordered">
            <thead class="bg-company-primary text-white">
                <tr>
                    @foreach($resume as $key => $row)
                        <th>{{ $row['title'] }}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody id="tbody">
                <tr>
		    	    @foreach($resume as $row)
                        <td class="text-center">
                            {{ $row['count'] }}
                        </td>    
				    @endforeach
                </tr>
        	</tbody>
    	</table>
	</div>
</div>

<hr>

@foreach ($resume as $key => $row)
    <div class="row my-2">
        <div class="col-12 mt-1 mb-3">
            <h5 class="text-white m-0 rounded-pill bg-company-primary px-3 py-1 d-flex justify-content-start">
                <span class="font-weight-bold mr-2">{{ $row['title'] }}</span> ({{ $row['count'] }})
            </h5>
        </div>
    </div>
    <div class="table-responsive">
        <div class="col-md-12 tableWrapper d-none">
            <table class="table table-bordered resume-table">
                <thead class="bg-company-secondary text-white">
                    <tr>
                        <th>Comentario</th>
                    </tr>
                </thead>
                <tbody id="tbody">
                    @foreach($row['data'] as $info)
                        <tr>
                            <td class="text-center">
                                {{ $info }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <hr>
@endforeach

@if(!empty($changes))
	<div class="row my-2">
		<div class="col-12 mt-1 mb-3">
			<h5 class="text-white m-0 rounded-pill bg-company-primary px-3 py-1 d-flex justify-content-start">
				<span class="font-weight-bold mr-2">Historial de Actualizaciones</span>
			</h5>
		</div>
	</div>
	<div class="table-responsive">
		<div class="col-md-12 tableWrapper d-none">
			<table class="table table-bordered resume-table">
				<thead class="bg-company-secondary text-white">
					<tr>
						<th>RFC</th>
						<th>Campo</th>
						<th>Antes</th>
						<th>Se cambio a</th>
					</tr>
				</thead>
				<tbody id="tbody">
					@foreach($changes as $key => $update)
						@if(!is_null($update))
							@foreach($update as $key2 => $tipo)
								<tr class="text-center">
									<td>{{ $key }}</td>
									<td>{{ strtoupper($key2) }}</td>
									@foreach($tipo as $valor)
										<td>{{ $valor }}</td>
									@endforeach
								</tr>
							@endforeach
						@endif
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endif

<div class="row">
    <div class="col-12 text-right">
        <a href="{{ url('admin-de-usuarios') }}" class="btn btn-secondary">
            Regresar
        </a>
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('.resume-table').DataTable({
			"order": [[ 0, "asc" ]],
			"paging": true,
			"pagingType": "numbers",
			// "buttons": [
			// 	{
			// 		"extend": 'excelHtml5',
			// 		"text": 'Exportar a Excel',
			// 		"titleAttr": 'Exportar a Excel',
			// 		"title": 'Exportar a Excel',
			// 		"exportOptions": {
			// 			"columns": [1, ':visible']
			// 		}
			// 	}
			// ],
			// "scrollX": true,
			// "fixedColumns":{
			// 	"leftColumns": 4,
			// },
			"language": {
		 		"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     ">",
					"sPrevious": "<"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
      		  }
	  	});

		setTimeout(() => {
			$('.tableWrapper').removeClass('d-none');
			$($.fn.dataTable.tables(true)).DataTable()
			.columns.adjust()
			.fixedColumns().relayout();
		}, 500);
	});
	
		
</script>
@endsection