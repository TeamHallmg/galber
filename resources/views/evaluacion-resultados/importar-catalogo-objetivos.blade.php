@extends('layouts.app')

@section('title', 'Importación del Catálogo de Objetivos')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
	@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/banner_evaluacion_resultados.png') }}" alt="">
		<div class="row mt-3">
			<div class="col-md-12">
				<h4 class="titulos-evaluaciones">Importación del Catálogo de Objetivos</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h5 class="titulos-evaluaciones">Sen van a registrar {{$registrar}} registros</h5>
				<h5 class="titulos-evaluaciones">Se van a actualizar {{$actualizar}} registros</h5>
				<h5 class="titulos-evaluaciones">Se van a ignorar {{$ignorar}} registros</h5>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">

		@if ($registrar != 0 || $actualizar != 0)
				<form action="/evaluacion-resultados/importar-catalogo-objetivos" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="importar" value="1">
					<button type="submit" class="btn btn-primary">Importar</button>
				</form>
		@else

			<h5 class="text-center">Nada se importará ya que no hay diferencias</h5>
		@endif

			</div>
		</div>

	@if (count($results) > 0)

		<div class="row mt-3">
			<div class="col-md-12">
				<table width="100%" style="font-size: 13px" class="data-table table table-hover table-striped table-bordered">
					<thead>
						<tr>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Código</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Objetivo</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Descripción</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Dirección</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Objetivo Corporativo</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Unidad de medida</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Meta</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Peso</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Frecuencia</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Semáforo Rojo</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Semáforo Verde</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Acción</th>
						</tr>
					</thead>
					<tbody>

  	@for ($i = 0;$i < count($results);$i++)

<?php $codigo_completo = $results[$i]->codigo . '';

			while (strlen($codigo_completo) < 5){
				
				$codigo_completo = '0' . $codigo_completo;
			} ?>

						<tr>
							<td style="padding: 10px 5px" class="text-left">{{$codigo_completo}}</td>
							<td style="padding: 10px 5px" class="text-left">{{$results[$i]->objetivo}}</td>
							<td style="padding: 10px 5px" class="text-left">{{$results[$i]->descripcion}}</td>
							<td style="padding: 10px 5px" class="text-left">{{$results[$i]->direccion}}</td>
							<td style="padding: 10px 5px" class="text-left">{{$results[$i]->objetivo_corporativo}}</td>
							<td style="padding: 10px 5px" class="text-left">{{$results[$i]->unidad}}</td>
							<td class="text-center" style="padding: 10px 5px">{{$results[$i]->meta}}</td>
							<td class="text-center" style="padding: 10px 5px">{{$results[$i]->peso}}</td>
							<td style="padding: 10px 5px">{{$results[$i]->frecuencia}}</td>
							<td class="text-center" style="padding: 10px 5px">{{$results[$i]->valor_rojo}}</td>
							<td class="text-center" style="padding: 10px 5px">{{$results[$i]->valor_verde}}</td>
							<td class="text-center" style="padding: 10px 5px">
								<strong>{{$results[$i]->accion}}</strong>
							</td>
						</tr>
		@endfor

					</tbody>
				</table>
			</div>
		</div>
	@endif

	</div>
</div>
@endsection