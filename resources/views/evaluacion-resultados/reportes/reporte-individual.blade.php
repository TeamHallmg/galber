@extends('layouts.app')

@section('title', 'Reporte Individual')

@section('content')
<style>div.dt-buttons {
	float: right;
	margin-left:10px;
	display: none;
	}</style>
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/banner_evaluacion_resultados.png') }}" alt="">
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Reporte Individual Evaluación de Resultados</h5>
			<div class="card-body">

@if (!empty($anios))


<div class="row"> 
	<div class="col-12 offset-md-4 col-md-3 text-center">
		<div class="form-group">
			<label for="anio_plan" class="font-weight-bolder mb-0">Año:</label>							
			<form id="changePlanForm" action="/reporte_individual" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_empleado" value="{{$user->id}}">
			<select name="anio_plan" id="anio_plan" class="form-control anio_plan">
				@for ($i = 0;$i < count($anios);$i++)

					<option value="{{$anios[$i]}}" <?php if ($anio_plan == $anios[$i]){ ?>selected="selected"<?php } ?>>{{$anios[$i]}}</option>
				@endfor

			</select>			
			<input type="submit" style="display: none">
			</form>
		</div>
	</div> 
</div>

 

  
@if (!empty($user))

<div class="card mb-3">
	<h5 class="card-header bg-info text-white font-weight-bolder">Colaborador</h5>
	<div class="card-body">
	<div class="media">
		<img src="{{asset('img/vacantes/sinimagen.png')}}" style="width: 120px" class="align-self-center mr-3" alt="...">
		<div class="media-body">
		
		<p><h5 class="mt-0 d-inline">Evaluado: </h5> {{$user->nombre . ' ' . $user->paterno . ' ' . $user->materno}}</p>
		<p><h5 class="mt-0 d-inline">Dirección: </h5>{{$user->direccion}}</p>
		<p><h5 class="mt-0 d-inline">Departamento: </h5>{{(!empty($user->jobPosition) && !empty($user->jobPosition->area) && !empty($user->jobPosition->area->department) ? $user->jobPosition->area->department->name : '')}}</p>
		<p><h5 class="mt-0 d-inline">Puesto: </h5>{{(!empty($user->jobPosition) ? $user->jobPosition->name : '')}}</p>
		</div>
	</div>
  </div>
  </div>
 
<div class="card border-0">
	<h5 class="card-header bg-primary text-white font-weight-bolder">Resultados
		<button class="btn btn-success float-right text-left mr-2 btn-excel" title="Exportar a Excel">
			<i class="fa fa-file-excel"></i> Exportar a Excel
		</button>
	</h5>
	<div class="card-body px-0"> 


<div class="row margin-top-20">
	<div class="col-md-12">
			<div class="col-md-12 table-responsive" style="overflow-x: auto; padding-top: 20px">

				<table width="100%" style="font-size: 13px" class="table table-striped table-bordered">

		{{-- <table style="font-size: 12px" class="data-table" width="100%"> --}}
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">ID</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">OBJETIVO</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">FRECUENCIA</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">PESO</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">PLAN</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">ENE</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">FEB</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">MAR</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">ABR</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">MAY</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">JUN</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">JUL</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">AGO</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">SEP</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">OCT</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">NOV</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">DIC</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">PROMEDIO</th>
				</tr>
			</thead>
			<tbody>

	<?php $actual_objetivo = 0;
				$contador_meses = 0;
				$promedio = 0;
				$promedios = array();
				$logros = array();
				$colores = array();
				$promedio_total = 0;
				$peso = 0; ?>

      	@for ($i = 0;$i < count($objetivos);$i++)

      		@if ($objetivos[$i]->id != $actual_objetivo)

      			@if ($actual_objetivo != 0)

      				@for ($j = 1;$j <= 12;$j++)

      					@if (isset($promedios[$j]))

      			<?php $promedio = $promedios[$j];
      						$promedio_total += $promedio;
      						$contador_meses++;

      						 if ($promedio == 0) {
					$class_promedio = 'badge-danger';
				}else if($promedio == $peso) {
					$class_promedio = 'badge-success';
				}else{
					$class_promedio = 'badge-warning';
				}?>

      		<td class="text-center h5" >
				<span class="badge badge-pill <?php echo $class_promedio; ?>"> {{number_format($promedio,1)}}% </span>
			</td>



      					@else

      		<td class="text-center" style="padding: 10px 5px; color: black">N/A</td>
								@endif
      				@endfor

      	<?php $promedio_total = $promedio_total / $contador_meses; 
		  if ($promedio_total == 0) {
					$class_prom_total = 'badge-danger';
				}else if($promedio_total == $peso) {
					$class_prom_total = 'badge-success';
				}else{
					$class_prom_total = 'badge-warning';
				} ?>

      		<td class="text-center h5">
				<span class="badge badge-pill <?php echo $class_prom_total; ?>"> {{number_format($promedio_total,1)}}% </span>
			</td>

      	</tr>
      			@endif

      	<tr class="filas">

					<td style="padding: 10px 5px" class="text-center">{{$objetivos[$i]->id}}</td>
					<td style="padding: 10px 5px" class="text-left cortar">{{$objetivos[$i]->nombre}}</td>
					<td style="padding: 10px 5px" class="text-center">{{$objetivos[$i]->frecuencia}}</td>
					<td style="padding: 10px 5px" class="text-center">{{$objetivos[$i]->peso}}</td>
					<td style="padding: 10px 5px" class="text-center">{{$objetivos[$i]->nombre_plan}}</td>

			<?php $actual_objetivo = $objetivos[$i]->id;
		 				$contador_meses = 0;
		 				$promedios = array();
		 				$promedio_total = 0;
		 				$peso = $objetivos[$i]->peso; ?>
					@endif

		<?php $mes = $objetivos[$i]->periodo; ?>

					@if ($objetivos[$i]->frecuencia == 'Trimestral')

		<?php $mes = $objetivos[$i]->periodo * 3; ?>
					@else

						@if ($objetivos[$i]->frecuencia == 'Cuatrimestral')

				<?php $mes = $objetivos[$i]->periodo * 4; ?>
						@else

							@if ($objetivos[$i]->frecuencia == 'Semestral')

					<?php $mes = $objetivos[$i]->periodo * 6; ?>
							@else

								@if ($objetivos[$i]->frecuencia == 'Anual')

						<?php $mes = 12?>
								@endif
							@endif
						@endif
					@endif

		<?php $promedios[$mes] = 0; ?>

					@if ($objetivos[$i]->valor_verde == 'Si' || $objetivos[$i]->valor_verde == 'No')

						@if ($objetivos[$i]->logro == $objetivos[$i]->valor_verde)

				<?php $promedios[$mes] = $objetivos[$i]->peso; ?>
						@endif
					@else

						@if ($objetivos[$i]->logro >= $objetivos[$i]->valor_verde)

				<?php $promedios[$mes] = $objetivos[$i]->peso; ?>
						@else

				<?php $valor = $objetivos[$i]->logro * 100 / $objetivos[$i]->valor_verde;
							$promedios[$mes] = $objetivos[$i]->peso * $valor / 100; ?>
						@endif
					@endif
				@endfor

				@if ($actual_objetivo != 0)

      		@for ($j = 1;$j <= 12;$j++)

      			@if (isset($promedios[$j]))

      	<?php $promedio = $promedios[$j];
      				$promedio_total += $promedio;
      				$contador_meses++;
				if ($promedio == 0) {
					$class_prom_total = 'badge-danger';
				}else if($promedio == $peso) {
					$class_prom_total = 'badge-success';
				}else{
					$class_prom_total = 'badge-warning';
				} ?>

      		<td class="text-center h5">
				<span class="badge  badge-pill <?php echo $class_prom_total; ?>"  > {{number_format($promedio,1)}}% </span>
			</td>
      			@else

      		<td class="text-center" style="padding: 10px 5px; color: black">N/A</td>
						@endif
      		@endfor

    <?php $promedio_total = $promedio_total / $contador_meses; 
	 		if ($promedio_total == 0) {
					$class_promedio_total = 'badge-danger';
				}else if($promedio_total == $peso) {
					$class_promedio_total = 'badge-success';
				}else{
					$class_promedio_total = 'badge-warning';
				}?>

      		<td class="text-center h5">
				<span class="badge badge-pill <?php echo $class_prom_total; ?>">{{number_format($promedio_total,1)}}% </span>
				</td>
      	</tr>
      	@endif

			</tbody>
		</table>
	</div>
	</div>
</div>			
</div>			
</div>			
@endif

@else


<div class="row">
					
	<div class="col-md-12 text-center">
	
		<div class="alert alert-warning h4" role="alert">
			<i class="fa fa-exclamation-triangle"></i> >No hay planes cerrados
		</div>

	</div>
</div>


@endif
	</div>
</div>
</div>
</div>
@endsection

@section('scripts')
 
	<script>
		$(document).ready(function(){
			
		$('.btn-excel').click(function(){
			$('.buttons-excel').trigger('click');
		});

			$('.table').DataTable({
					dom:"<'row'<'col-md-6'l><'col-md-6'Bf>>" +
"<'row'<'col-md-6'><'col-md-6'>>" +
"<'row'<'col-md-12't>><'row'<'col-md-12'ip>>",
   					order: [[0,'desc']],
   					language: {
		 				'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
      				},
   					buttons: [
        			{
          				extend: 'excel',
							text: 'Exportar a Excel',
							titleAttr: 'Exportar a Excel',
							title: 'Reporte Mídete'
					}
        			]
			});
			
			$('select.anio_plan').change(function(){

				$('form#changePlanForm').submit();
			});
		});
	</script>
@endsection