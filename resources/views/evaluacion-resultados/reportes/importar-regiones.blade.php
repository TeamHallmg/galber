@extends('layouts.app')

@section('title', 'Importación de Regiones')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/banner_evaluacion_resultados.png') }}" alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Importar Regiones</h3>
		<hr>
	</div>
</div>

@if (!empty($datos))

<div class="row">
	<div class="col-md-12 text-center">
		<a href="/importar-regiones-empleados" class="btn btn-primary btn-finvivir-blue">Importar Datos</a>
	</div>
</div>
<div class="row margin-top-20">
	<div class="col-md-12">

@if ($montos_y_porcentajes)

		<table width="100%" class="table table-striped">
			<thead>
				<tr>
					<th class="cabeceras-evaluaciones">PUESTO</th>
					<th class="cabeceras-evaluaciones">DEPARTAMENTO</th>
					<th class="cabeceras-evaluaciones">REGION</th>
					<th class="cabeceras-evaluaciones">FRECUENCIA</th>
					<th class="cabeceras-evaluaciones">BONO ANUAL</th>
					<th class="cabeceras-evaluaciones">TIPO BONO</th>
					<th class="cabeceras-evaluaciones">PORCENTAJE</th>
					<th class="cabeceras-evaluaciones">MONTO</th>
				</tr>
			</thead>
			<tbody>

      @for ($i = 0;$i < count($datos);$i++)

      	<tr>		
					<td>{{$datos[$i]->puesto}}</td>
					<td>{{$datos[$i]->departamento}}</td>
					<td>{{$datos[$i]->region}}</td>
					<td>{{$datos[$i]->frecuencia}}</td>
					<td>{{$datos[$i]->bono_anual}}</td>
					<td>{{$datos[$i]->tipo_bono}}</td>
					<td>{{$datos[$i]->porcentaje}}</td>
					<td>{{$datos[$i]->monto}}</td>
      	</tr>
      @endfor

			</tbody>
		</table>
@else

		<table width="100%" class="table table-striped">
			<thead>
				<tr>
					<th class="cabeceras-evaluaciones">RFC</th>
					<th class="cabeceras-evaluaciones">NOMBRE</th>
					<th class="cabeceras-evaluaciones">SUCURSAL</th>
					<th class="cabeceras-evaluaciones">PUESTO</th>
					<th class="cabeceras-evaluaciones">REGION</th>
				</tr>
			</thead>
			<tbody>

      @for ($i = 0;$i < count($datos);$i++)

      	<tr>		
					<td>{{$datos[$i]->rfc}}</td>
					<td>{{$datos[$i]->nombre}}</td>
					<td>{{$datos[$i]->sucursal}}</td>
					<td>{{$datos[$i]->puesto}}</td>
					<td>{{$datos[$i]->region}}</td>
      	</tr>
      @endfor

			</tbody>
		</table>
@endif

	</div>
</div>			
@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20 titulos-evaluaciones">El archivo esta vacío o la extensión no es válida</h4>
	</div>
</div>
@endif
@endsection