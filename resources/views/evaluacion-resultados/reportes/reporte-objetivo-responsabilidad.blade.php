@extends('layouts.app')

@section('title', 'Reporte Objetivo Responsabilidad')

@section('content')
<?php $frequency = 0; ?>
<div class="row margin-top-20">
	<div class="col-md-2">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_evaluacion_resultados.png" alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Reporte Objetivo Responsabilidad</h3>
		<hr>

	@if (!empty($planes))

		<div class="margin-top-20 text-center">
			Plan 
			<select class="form-group planes" style="border-radius: 7px; margin-bottom: 4px">

		@foreach ($planes as $key => $plan)
		
			<option value="{{$plan->id}}" <?php if ($plan->id == $id_plan){ ?>selected="selected"<?php } ?>>{{$plan->nombre}}</option>
		@endforeach

			</select>
		</div>

		@if (!empty($responsabilities) && !$responsabilities->isEmpty())

		<div class="margin-top-20 text-center" style="margin-top: 20px">
			<div>
				<i>Responsabilidades</i>
			</div>
			<div>
				<select class="form-group responsabilidades" style="border-radius: 7px; margin-bottom: 4px">
					<option value="0">-- Seleccione --</option>

			@foreach ($responsabilities as $key => $value)
		
					<option value="{{$value->id}}" <?php if ($id_responsabilidad == $value->id){ ?>selected="selected"<?php } ?>>{{$value->description}}</option>
			@endforeach

				</select>
			</div>
		</div>
		@endif

		@if (!empty($results_responsabilities) && !$results_responsabilities->isEmpty())

		<div class="margin-top-20" style="margin-top: 20px">
			<table class="tabla_responsabilidades table table-striped table-bordered table-hover">
				<thead style="background-color: #222B64; color:white">
					<tr>
						<th class="cabeceras-evaluaciones" style="text-align: left">Objetivo Corporativo</th>
						<th class="cabeceras-evaluaciones" style="text-align: left">Perspectiva</th>
						<th class="cabeceras-evaluaciones">Unidad</th>
						<th class="cabeceras-evaluaciones">Estado</th>
						<th class="cabeceras-evaluaciones">Tend</th>
						<th class="cabeceras-evaluaciones" style="text-align: right">Logro Esperado</th>
					</tr>
				</thead>
				<tbody>

<?php $current_responsability = 0; ?>

			@foreach ($results_responsabilities as $key => $value)

				@if ($value->id != $current_responsability)

		<?php $current_responsability = $value->id; ?>

					<tr>
						<td width="290">{{$value->name}}</td>
						<td>{{$value->tipo_objetivo_corporativo}}</td>
						<td class="text-center">{{($value->unidad == '#' ? '$' : ($value->unidad == '9' ? '0' : $value->unidad))}}</td>
						<td class="text-center">
							<img src="/img/evaluacion-resultados/reportes/{{($value->unidad == 'Si/No' ? ($value->logro == $value->meta ? 'guion.png' : 'flecha_roja.png') : ($value->valor_verde >= $value->valor_rojo ? ($value->logro > $value->meta ? 'flecha_verde.png' : ($value->logro < $value->meta ? 'flecha_roja.png' : 'guion.png')) : ($value->logro < $value->meta ? 'flecha_verde.png' : ($value->logro > $value->meta ? 'flecha_roja.png' : 'guion.png'))))}}">
						</td>
						<td class="text-center">

					@if (!empty($results_responsabilities[$key + 1]) && $results_responsabilities[$key + 1]->id == $current_responsability)

							<img src="/img/evaluacion-resultados/reportes/{{($value->unidad == 'Si/No' ? ($results_responsabilities[$key + 1]->logro == $value->logro ? 'guion2.png' : ($value->logro == $value->meta ? 'flecha_verde2.png' : 'flecha_roja2.png')) : ($value->valor_verde >= $value->valor_rojo ? ($value->logro > $results_responsabilities[$key + 1]->logro ? 'flecha_verde2.png' : ($value->logro < $results_responsabilities[$key + 1]->logro ? 'flecha_roja2.png' : 'guion2.png')) : ($value->logro < $results_responsabilities[$key + 1]->logro ? 'flecha_verde2.png' : ($value->logro > $results_responsabilities[$key + 1]->logro ? 'flecha_roja2.png' : 'guion2.png'))))}}">
					@endif                                                                                                                                                                                                                           

						</td>
						<td class="text-right">{{($value->unidad == 'Si/No' ? $value->meta : number_format($value->meta, 2, '.', ','))}}</td>
					</tr>
				@endif
			@endforeach

				</tbody>
			</table>
		</div>
		@endif

		@if (!empty($responsability) && !$responsability->isEmpty())

		<div class="row margin-top-20" style="margin-top: 20px">
			<div class="col-md-5">
				<table class="tabla_responsabilidad table table-striped">
					<thead>
						<tr>
							<th class="cabeceras-evaluaciones">{{$year}}</th>
							<th class="cabeceras-evaluaciones" style="text-align: right">Logro</th>
							<th class="cabeceras-evaluaciones" style="text-align: right">Logro Esperado</th>
							<th class="cabeceras-evaluaciones">Diferencia</th>
						</tr>
					</thead>
					<tbody>

<?php $meses = array(0,'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
		$frequency = 1;
		$month = 1; ?>

		@if ($responsability[0]->frecuencia_captura == 'Bimestral')

<?php $frequency = 2; ?>

		@else

			@if ($responsability[0]->frecuencia_captura == 'Trimestral')

	<?php $frequency = 3; ?>

			@else

				@if ($responsability[0]->frecuencia_captura == 'Anual')

	<?php $frequency = 12; ?>
				@endif
			@endif
		@endif

		@while ($month < $start_month)

						<tr>
							<td class="text-center" style="background-color: #003DA7; color: white; font-weight: bold">{{$meses[$month]}}</td>
							<td></td>
							<td class="text-right">{{($responsability[0]->unidad == 'Si/No' ? $responsability[0]->meta : number_format($responsability[0]->meta, 2, '.', ','))}}</td>
							<td></td>
						</tr>

<?php $month++; ?>
		@endwhile

		@foreach ($responsability as $key => $value)

<?php $next_month = $value->periodo * $frequency; ?>

			@while ($month < $next_month)

						<tr>
							<td class="text-center" style="background-color: #222B64; color: white; font-weight: bold">{{$meses[$month]}}</td>
							<td></td>
							<td class="text-right">{{($value->unidad == 'Si/No' ? $value->meta : number_format($value->meta, 2, '.', ','))}}</td>
							<td></td>
						</tr>

<?php $month++; ?>
			@endwhile

						<tr>
							<td class="text-center" style="background-color: #222B64; color: white; font-weight: bold">{{$meses[$month]}}</td>
							<td class="text-right">{{($value->unidad == 'Si/No' ? $value->logro : number_format($value->logro, 2, '.', ','))}}</td>
							<td class="text-right">{{($value->unidad == 'Si/No' ? $value->meta : number_format($value->meta, 2, '.', ','))}}</td>
							<td class="text-center">{{($value->unidad != 'Si/No' ? number_format($value->logro - $value->meta, 2, '.', ',') : '')}}</td>
						</tr>

<?php $month++; ?>
		@endforeach

		@while ($month < 13)

						<tr>
							<td class="text-center" style="background-color: #222B64; color: white; font-weight: bold">{{$meses[$month]}}</td>
							<td></td>
							<td class="text-right">{{($value->unidad == 'Si/No' ? $value->meta : number_format($value->meta, 2, '.', ','))}}</td>
							<td></td>
						</tr>

<?php $month++; ?>
		@endwhile

					</tbody>
				</table>
			</div>
			<div class="col-md-7">
				<h4 class="text-center titulos-evaluaciones" style="margin-top: 0">{{$responsability[0]->description}}</h4>
				<div id="container" style="width: 100%; margin: 0 auto"></div>
			</div>
		</div>
		@endif
	@else

		<h2>NO HAY PLANES ABIERTOS NI CERRADOS</h2>
	@endif

	</div>
</div>
@endsection

@section('scripts')
<script>

	var responsability = <?php echo json_encode($responsability)?>;
	var frequency = <?php echo $frequency?>;
	var start_month = <?php echo $start_month?>;
	var id_plan = <?php echo $id_plan?>;
	var id_responsabilidad = <?php echo $id_responsabilidad?>;
		
	$(document).ready(function(){

		var meses = [];
		meses[0] = 'Ene';
		meses[1] = 'Feb';
		meses[2] = 'Mar';
		meses[3] = 'Abr';
		meses[4] = 'Nay';
		meses[5] = 'Jun';
		meses[6] = 'Jul';
		meses[7] = 'Ago';
		meses[8] = 'Sep';
		meses[9] = 'Oct';
		meses[10] = 'Nov';
		meses[11] = 'Dic';

		if (responsability[0] != undefined){

		var metas = [];
		var valor_maximo = responsability[0].meta;

		for (var i = 0;i < 12;i++){

			metas[i] = responsability[0].meta * 1;
		}

		var month = 1;
		var logros = [];

		while (month < start_month){

			logros[month - 1] = 0;
			month++;
		}

		for (var i = 0;i < responsability.length;i++){

			while (month < responsability[i].periodo * frequency){

				logros[month - 1] = 0;
				month++;
			}

			logros[month - 1] = responsability[i].logro * 1;

			if (logros[month - 1] > valor_maximo){

				valor_maximo = logros[month - 1];
			}

			month++;
		}

		var step = valor_maximo / 8;
		var chart = {
      renderTo: 'container',
      options3d: {
        enabled: true,
        alpha: 0,
        beta: 0,
        depth: 50,
        viewDistance: 25
      }
   	};

   	var title = {
      text: ''   
   	};

   	var legend = {
      align: 'center',
      verticalAlign: 'bottom',
      layout: 'horizontal',
			floating: true,
			y: 25
    };
   
   	var plotOptions = {
      column: {
        depth: 25
      }
   	};

    var series = [{
      name: 'Meta',
      data: metas,
      color: '#222B64',
			type: 'column',
    },{
      name: 'Real',
      data: logros,
      color: '#ed3237',
			type: 'line',
    }];
		
		var yAxis = [{
			labels: {
        style: {
          fontSize:'12px',
          fontWeight:'bold'
        }
      },
      max: valor_maximo,
      tickInterval: step
		}];

   	var xAxis = [{
      categories: meses,
	  	labels: {
        style: {
          fontSize:'8px',
          fontWeight:'bold'
        },
        step: 1
      },
   	}];     
      
   	var json = {};
   	json.chart = chart;
   	json.title = title;
   	json.series = series;
   	json.plotOptions = plotOptions;
   	json.xAxis = xAxis;
		json.yAxis = yAxis;
		json.legend = legend;
   	highchart = new Highcharts.Chart(json);
   	}

		// Cambia el plan
		$('select.planes').change(function(){

			window.location = '/evaluacion-resultados/reporte-objetivo-responsabilidad/' + $(this).val() + '/' + id_responsabilidad;
		});

		// Cambia la responsabilidad
		$('select.responsabilidades').change(function(){

			window.location = '/evaluacion-resultados/reporte-objetivo-responsabilidad/' + id_plan + '/' + $(this).val();
		});
	});
</script>
@endsection