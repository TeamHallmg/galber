@extends('layouts.app')

@section('title', 'Reporte Semaforizado')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_evaluacion_resultados.png" alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Reporte Semaforizado</h3>
		<hr>

	@if (!empty($planes))

		<div class="margin-top-20 text-center">
			Plan 
			<select class="form-group planes" style="border-radius: 7px; margin-bottom: 4px">

		@foreach ($planes as $key => $plan)
		
			<option value="{{$plan->id}}" <?php if ($plan->id == $id_plan){ ?>selected="selected"<?php } ?>>{{$plan->nombre}}</option>
		@endforeach

			</select>
		</div>

		@if (!empty($responsabilities))

		<div style="border-radius: 7px; border: 1px solid gray">

<?php $current_responsability = 0;
			$current_type = 0; ?>

			@foreach ($responsabilities as $key => $responsability)

				@if ($current_type != $responsability->id_tipo)

					@if ($current_type != 0)

				</div>
			</div>

					@endif

		<?php $current_type = $responsability->id_tipo; ?>

			<div style="border-radius: 7px; border: 1px solid gray; padding: 10px">
				<div>{{$responsability->tipo_objetivo_corporativo}}</div>
				<div class="row">
				@endif

				@if ($current_responsability != $responsability->id)

		<?php $current_responsability = $responsability->id; ?>

					<div class="col-md-3 text-center" style="margin-bottom: 10px">
						<div style="border-radius: 7px; border: 1px solid gray; padding: 5px 0; background-color: #D8E0F4">
							<div style="border-bottom: 1px solid gray; min-height: 50px">
								<a href="/evaluacion-resultados/reporte-objetivo-corporativo/{{$id_plan}}/{{$responsability->id_objetivo_corporativo}}" class="objetive_name">{{$responsability->name}}</a>
							</div>
							<div class="text-center" style="padding: 5px 0">
								<img src="/img/evaluacion-resultados/reportes/{{($responsability->unidad == 'Si/No' ? ($responsability->logro == $responsability->meta ? 'guion.png' : 'flecha_roja.png') : ($responsability->valor_verde >= $responsability->valor_rojo ? ($responsability->logro > $responsability->meta ? 'flecha_verde.png' : ($responsability->logro < $responsability->meta ? 'flecha_roja.png' : 'guion.png')) : ($responsability->logro < $responsability->meta ? 'flecha_verde.png' : ($responsability->logro > $responsability->meta ? 'flecha_roja.png' : 'guion.png'))))}}">

					@if (!empty($responsabilities[$key + 1]) && $responsabilities[$key + 1]->id == $current_responsability)

								<img src="/img/evaluacion-resultados/reportes/{{($responsability->unidad == 'Si/No' ? ($responsabilities[$key + 1]->logro == $responsability->logro ? 'guion2.png' : ($responsability->logro == $responsability->meta ? 'flecha_verde2.png' : 'flecha_roja2.png')) : ($responsability->valor_verde >= $responsability->valor_rojo ? ($responsability->logro > $responsabilities[$key + 1]->logro ? 'flecha_verde2.png' : ($responsability->logro < $responsabilities[$key + 1]->logro ? 'flecha_roja2.png' : 'guion2.png')) : ($responsability->logro < $responsabilities[$key + 1]->logro ? 'flecha_verde2.png' : ($responsability->logro > $responsabilities[$key + 1]->logro ? 'flecha_roja2.png' : 'guion2.png'))))}}">
					@endif
							</div>
						</div>
					</div>
				@endif
			@endforeach

			@if ($current_type != 0)

				</div>
			</div>
			@endif

		</div>
		@endif
	@else

		<h2>NO HAY PLANES ABIERTOS NI CERRADOS</h2>
	@endif

	</div>
</div>
@endsection

@section('scripts')
<script>
		
	$(document).ready(function(){

		// Cambia el plan
		$('select.planes').change(function(){

			// Recarga la pagina con el nuevo plan
			window.location = '/evaluacion-resultados/reporte-semaforizado/' + $(this).val();
		});
	});
</script>
@endsection