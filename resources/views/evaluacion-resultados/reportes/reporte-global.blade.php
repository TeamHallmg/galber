@extends('layouts.app')

@section('title', 'Reporte Evaluación de Resultados')

@section('content')
<style>div.dt-buttons {
	float: right;
	margin-left:10px;
	display: none;
	}</style>
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/banner_evaluacion_resultados.png') }}" alt="">

	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Reporte Evaluación de Resultados</h5>
			<div class="card-body">


@if (!empty($anios))


<div class="row pb-5">
	<div class="col-md-6">
	
		<div class="card">
			<h5 class="card-header bg-primary text-white font-weight-bolder">Filtros</h5>
			<div class="card-body">
				
			
				<div class="row"> 
					<div class="col-12 col-md-6">
						<label for="anio_plan">Año:</label>
						
						<form id="changePlanForm" action="/reportes" method="post">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<select name="anio_plan" id="anio_plan" class="form-control anio_plan">
							@for ($i = 0;$i < count($anios);$i++)
								<option value="{{$anios[$i]}}" <?php if ($anio_plan == $anios[$i]){ ?>selected="selected"<?php } ?>>{{$anios[$i]}}</option>
							@endfor
						</select>
						<input type="submit" style="display: none">
						</form>
					</div>
					<div class="col-12 col-md-6">
						<label for="anio_plan">Departamento:</label>
						
						<form id="changeAreaForm" action="/reportes" method="post">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="anio_plan" class="anio">
						<select name="anio_plan" id="anio_plan" class="form-control area_reporte">
							<option value="0">-- Selecciona --</option>
							
	
							@for ($i = 0;$i < count($departments);$i++)
	
								<option value="{{$departments[$i]->name}}" <?php if (!empty($_POST['department']) && $_POST['department'] == $departments[$i]->name){ ?>selected="selected"<?php } ?>>{{$departments[$i]->name}}</option>
							@endfor
						</select>
						<input type="submit" style="display: none">
					</form>
					</div>
				</div>

			</div>
		</div>

	</div>
	<div class="col-md-6">
	
		<div class="card h-100">
			<h5 class="card-header bg-primary text-white font-weight-bolder">Reseñas</h5>
			<div class="card-body"> 
					<div class="row">
						<div class="col-md-4">
							<br>
							<span class="btn btn-success btn-block">95 a 100</span>
						</div>
						<div class="col-md-4">
							<br>
							<span class="btn btn-warning btn-block">90 a 94</span>
						</div>
						<div class="col-md-4">
							<br>
							<span class="btn btn-danger btn-block">Menos de 89</span>
						</div> 
					</div>  
			</div>
		</div>

	</div>
</div>
 























 

<?php //$users_ids = $users_promedios = array();?>

@if (!empty($users))
 
<div class="card border-0">
	<h5 class="card-header bg-primary text-white font-weight-bolder">Resultados
		<button class="btn btn-success float-right text-left mr-2 btn-excel" title="Exportar a Excel">
			<i class="fa fa-file-excel"></i> Exportar a Excel
		</button>
	</h5>
	<div class="card-body px-0"> 

<div class="row">
	<div class="col-md-12 table-responsive" style="overflow-x: auto; padding-top: 20px">
		<table width="100%" style="font-size: 13px" class="table table-striped table-bordered">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">DIRECCIÓN</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">DEPARTAMENTO</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">PUESTO</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">NOMBRE</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">ENE</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">FEB</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">MAR</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">ABR</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">MAY</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">JUN</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">JUL</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">AGO</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">SEP</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">OCT</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">NOV</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">DIC</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">PROMEDIO</th>
				</tr>
			</thead>
			<tbody>

	<?php $contador_departamentos = 0;
				$actual_departamento = '';
				$actual_usuario = 0;
				$contador_meses = 0;
				$promedio = 0;
				$promedio_total = 0;
				$promedios = array();
				$pesos = array();
				$peso = 0;

				if (!empty($departments2)){

					$departments = $departments2;
				} ?>

      @for ($i = 0;$i < count($users);$i++)

      	@if ($users[$i]->id != $actual_usuario)

      		@if ($actual_usuario != 0)

      			@for ($j = 1;$j <= 12;$j++)

      				@if (isset($promedios[$j]))

      					@if ($pesos[$j] == 0)

      			<?php $pesos[$j] = 1; ?>
      					@endif

      		<?php $promedio = $promedios[$j] * 100 / $pesos[$j];
      					$promedio_total += $promedio;
      					$contador_meses++; 

						  if ($promedio >= 95) {
							 $color = '#5cb85c';
					$class_color = 'badge-success';
						  }else if($promedio < 90) {
					$class_color = 'badge-danger';
							 $color = '#d9534f';
						  }else{
							 $color = '#d58512';
					$class_color = 'badge-warning';
						  }
						 ?>


		      		<td class="text-center h5" style="padding: 10px 5px;">
						<span class="badge badge-pill badge-pill <?php echo $class_color; ?>">{{number_format($promedio,1)}}%</span>
					 </td>

      				@else

      		<td class="text-center" style="padding: 10px 5px; color: black">N/A</td>
							@endif
      			@endfor

      <?php if ($contador_meses == 0){

      				$contador_meses = 1;
      			}

      			$promedio_total = $promedio_total / $contador_meses;


				if ($promedio_total >= 95) {
					$color_prom_total = '#5cb85c';
					$class_prom_total = 'badge-success';
				}else if($promedio_total < 90) {
					$color_prom_total = '#d9534f';
					$class_prom_total = 'badge-danger';
				}else{
					$color_prom_total = '#d58512';
					$class_prom_total = 'badge-warning';
				}

      			//$users_promedios[] = $promedio_total; ?>

      		<td class="text-center h5" style="padding: 10px 5px;">
			  <span class="badge badge-pill <?php echo $class_prom_total; ?>">{{number_format($promedio_total,1)}}%</span> 
			  </td>
      	</tr>
      		@endif

      	<tr>
      		<td style="padding: 10px 5px" class="text-left">{{$users[$i]->direccion}}</td>		
					<td style="padding: 10px 5px" class="text-left">{{$users[$i]->department}}</td>
					<td style="padding: 10px 5px" class="text-left">{{$users[$i]->job_position}}</td>
					<td style="padding: 10px 5px" class="text-left"><a href="javascript:;" class="reporte_individual">{{$users[$i]->nombre . ' ' . $users[$i]->paterno . ' ' . $users[$i]->materno}}</a>
					   <form action="/reporte_individual" method="post">
					   	<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id_empleado" value="{{ $users[$i]->id }}">
						<input type="hidden" name="anio_plan" class="plan_anio">
						<input type="submit" style="display: none">
					   </form>
						</td>

		<?php $actual_usuario = $users[$i]->id;
					//$users_ids[] = $actual_usuario;
		 			$contador_meses = 0;
		 			$promedio = 0;
		 			$promedio_total = 0;
		 			$promedios = array();
		 			$pesos = array(); ?>
				@endif

	<?php $mes = $users[$i]->periodo;
				$peso = 0; ?>

				@if ($users[$i]->frecuencia == 'Trimestral')

		<?php $mes = $users[$i]->periodo * 3; ?>
				@else

					@if ($users[$i]->frecuencia == 'Cuatrimestral')

			<?php $mes = $users[$i]->periodo * 4; ?>
					@else

						@if ($users[$i]->frecuencia == 'Semestral')

				<?php $mes = $users[$i]->periodo * 6; ?>
						@else

							@if ($users[$i]->frecuencia == 'Anual')

					<?php $mes = 12?>
							@endif
						@endif
					@endif
				@endif

				@if ($users[$i]->valor_verde == 'Si' || $users[$i]->valor_verde == 'No')

					@if ($users[$i]->logro == $users[$i]->valor_verde)

			<?php $peso = $users[$i]->peso; ?>
					@endif
				@else

					@if ($users[$i]->logro >= $users[$i]->valor_verde)

			<?php $peso = $users[$i]->peso; ?>
					@else

			<?php $valor = $users[$i]->logro * 100 / $users[$i]->valor_verde;
						$peso = $users[$i]->peso * $valor / 100; ?>
					@endif
				@endif

				@if (empty($pesos[$mes]))

		<?php $pesos[$mes] = $users[$i]->peso;
					$promedios[$mes] = $peso; ?>
				@else

		<?php $pesos[$mes] += $users[$i]->peso;
					$promedios[$mes] += $peso; ?>
				@endif
			@endfor

			@if ($actual_usuario != 0)

      	@for ($j = 1;$j <= 12;$j++)

      		@if (isset($promedios[$j]))

      			@if ($pesos[$j] == 0)

      	<?php $pesos[$j] = 1; ?>
      			@endif

      <?php $promedio = $promedios[$j] * 100 / $pesos[$j];
      			$promedio_total += $promedio;
      			$contador_meses++;

				if ($promedio >= 95) {
					$color = '#5cb85c';
					$class_prom = 'badge-success';
				}else if($promedio < 90) {
					$color = '#d9534f';
					$class_prom = 'badge-danger';
				}else{
					$color = '#d58512';
					$class_prom = 'badge-warning';
				}
						 ?>


      	
      		<td class="text-center h5" style="padding: 10px 5px;">
			
				<span class="badge badge-pill <?php echo $class_prom; ?>">{{number_format($promedio,1)}}%</span> 
			
			</td>
      		@else

      		<td class="text-center" style="padding: 10px 5px; color: black">N/A</td>
					@endif
      	@endfor

  <?php if ($contador_meses == 0){

      		$contador_meses = 1;
      	}

  			$promedio_total = $promedio_total / $contador_meses;

				if ($promedio_total >= 95) {
					$color_prom_total = '#5cb85c';
					$class_prom_total = 'badge-success';
				}else if($promedio_total < 90) {
					$color_prom_total = '#d9534f';
					$class_prom_total = 'badge-danger';
				}else{
					$color_prom_total = '#d58512';
					$class_prom_total = 'badge-warning';
				}
  			//$users_promedios[] = $promedio_total; ?>

      		
      		<td class="text-center h5" style="padding: 10px 5px;" style="background:#d58512 ">
			  
			  <span class="badge badge-pill <?php echo $class_prom_total; ?>" >{{number_format($promedio_total,1)}}%</span> 
			</td>
      	</tr>
      @endif

			</tbody>
		</table>
	</div>
</div>			
</div>			
</div>			

@else

<div class="row">
					
	<div class="col-md-12 text-center">
	
		<div class="alert alert-warning h4" role="alert">
			<i class="fa fa-exclamation-triangle"></i> No tiene colaboradores
		</div>

	</div>
</div>

@endif

@else

<div class="row">
					
	<div class="col-md-12 text-center">
	
		<div class="alert alert-warning h4" role="alert">
			<i class="fa fa-exclamation-triangle"></i> >No hay planes cerrados o abiertos
		</div>

	</div>
</div>

@endif
</div>
</div>
</div>
</div>
@endsection

@section('scripts')
	<script>
		
		$('.btn-excel').click(function(){
			$('.buttons-excel').trigger('click');
		});

		var suma = 0;
		var anio_plan = <?php echo $anio_plan?>;

		// Obtiene los ids de los usuarios
		//var users_ids = <?php //echo json_encode($users_ids)?>;

		// Obtiene los promedios de los usuarios
		//var users_promedios = <?php //echo json_encode($users_promedios)?>;
		
		$(document).ready(function(){

			// Código necesario para hacer llamadas ajax
			$.ajaxSetup({
    		headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
			});
			
			$('.table').DataTable({
					dom:"<'row'<'col-md-6'l><'col-md-6'Bf>>" +
"<'row'<'col-md-6'><'col-md-6'>>" +
"<'row'<'col-md-12't>><'row'<'col-md-12'ip>>",	
   					order: [[0,'desc']],
   					language: {
		 				"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      				},
   					buttons: [
        			{
          				extend: 'excel',
							text: 'Exportar a Excel',
							titleAttr: 'Exportar a Excel',
							title: 'Reporte Mídete'
					}
        			]
			});
			
			$('select.anio_plan').change(function(){

				$('form#changePlanForm .area').val($('select.area_reporte').val());
				$('form#changePlanForm').submit();
			});

			$('select.area_reporte').change(function(){

				$('form#changeAreaForm .anio').val($('select.anio_plan').val());
				$('form#changeAreaForm').submit();
			});
			
			$('a.reporte_individual').click(function(){

				$(this).next().find('.plan_anio').val($('select.anio_plan').val());
				$(this).next().submit();
			});

			// Guarda los promedios de los usuarios
			/*$.ajax({
        type:'POST',    
        url: 'save-promedio',
        data:{
          promedios: users_promedios,
          anio_plan: anio_plan,
          usuarios: users_ids
        },
        success: function(data){

          alert('Todos los promedios fueron guardados');
        }
  		});*/
		});
	</script>
@endsection