@extends('layouts.app')

@section('title', 'Reporte Objetivo Corporativo')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_evaluacion_resultados.png" alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Reporte Objetivo Corporativo</h3>
		<hr>

	@if (!$planes->isEmpty())

<?php $objetivo_corporativo = '';
			$id_tipo = 0; ?>

		<div class="margin-top-20 text-center">
			Plan 
			<select class="form-group planes" style="border-radius: 7px; margin-bottom: 4px">

		@foreach ($planes as $key => $plan)
		
			<option value="{{$plan->id}}" <?php if ($plan->id == $id_plan){ ?>selected="selected"<?php } ?>>{{$plan->nombre}}</option>
		@endforeach

			</select>
		</div>

		@if (!$objetivos_corporativos->isEmpty())

		<div class="margin-top-20 text-center" style="margin-top: 20px">
			<div>
				<i>Objetivos Corporativos</i>
			</div>
			<div>
				<select class="form-group objetivos_corporativos" style="border-radius: 7px; margin-bottom: 4px">
					<option value="0">-- Seleccione --</option>

			@foreach ($objetivos_corporativos as $key => $objetivo)
		
					<option value="{{$objetivo->id_objetivo_corporativo}}" <?php if ($id_objetivo_corporativo == $objetivo->id_objetivo_corporativo){ $objetivo_corporativo = $objetivo->name; $id_tipo = $objetivo->id_tipo; ?>selected="selected"<?php } ?>>{{$objetivo->name}}</option>
			@endforeach

				</select>
			</div>
		</div>
		@endif

		<div class="margin-top-20" style="margin-top: 20px">

		@if (!$results_responsabilities->isEmpty())

			<table class="tabla_responsabilidades table table-striped table-bordered table-hover">
				<thead style="background-color: #222B64; color:white">
					<tr>
						<th class="cabeceras-evaluaciones" style="text-align: left">Objetivo Corporativo</th>
						<th class="cabeceras-evaluaciones" style="text-align: left">Responsable</th>
						<th class="cabeceras-evaluaciones">Estado</th>
						<th class="cabeceras-evaluaciones">Tend</th>
						<th class="cabeceras-evaluaciones" style="text-align: left">Perspectiva</th>
						<th class="cabeceras-evaluaciones" style="text-align: left">Responsabilidad</th>
					</tr>
				</thead>
				<tbody>

<?php $current_responsability = 0; ?>

			@foreach ($results_responsabilities as $key => $responsability)

				@if ($current_responsability != $responsability->id)

		<?php $current_responsability = $responsability->id; ?>

					<tr>
						<td width="290">{{$objetivo_corporativo}}</td>
						<td width="290">

					@foreach ($users as $key2 => $value2)

						@if ($responsability->id_responsable == $value2->id)

							{{$value2->nombre}} {{$value2->paterno}} {{$value2->materno}}
				<?php break; ?>
						@endif
					@endforeach

						</td>
						<td class="text-center">
							<img src="/img/evaluacion-resultados/reportes/{{($responsability->unidad == 'Si/No' ? ($responsability->logro == $responsability->meta ? 'guion.png' : 'flecha_roja.png') : ($responsability->valor_verde >= $responsability->valor_rojo ? ($responsability->logro > $responsability->meta ? 'flecha_verde.png' : ($responsability->logro < $responsability->meta ? 'flecha_roja.png' : 'guion.png')) : ($responsability->logro < $responsability->meta ? 'flecha_verde.png' : ($responsability->logro > $responsability->meta ? 'flecha_roja.png' : 'guion.png'))))}}">
						</td>
						<td class="text-center">

					@if (!empty($results_responsabilities[$key + 1]) && $results_responsabilities[$key + 1]->id == $current_responsability)

							<img src="/img/evaluacion-resultados/reportes/{{($responsability->unidad == 'Si/No' ? ($results_responsabilities[$key + 1]->logro == $responsability->logro ? 'guion2.png' : ($responsability->logro == $responsability->meta ? 'flecha_verde2.png' : 'flecha_roja2.png')) : ($responsability->valor_verde >= $responsability->valor_rojo ? ($responsability->logro > $results_responsabilities[$key + 1]->logro ? 'flecha_verde2.png' : ($responsability->logro < $results_responsabilities[$key + 1]->logro ? 'flecha_roja2.png' : 'guion2.png')) : ($responsability->logro < $results_responsabilities[$key + 1]->logro ? 'flecha_verde2.png' : ($responsability->logro > $results_responsabilities[$key + 1]->logro ? 'flecha_roja2.png' : 'guion2.png'))))}}">
					@endif

						</td>
						<td class="text-center">{{$responsability->name}}</td>
						<td>
							<a href="/evaluacion-resultados/reporte-objetivo-responsabilidad/{{$id_plan}}/{{$responsability->id}}" class="responsability_name">{{$responsability->description}}</a>
						</td>
					</tr>
				@endif
			@endforeach

				</tbody>
			</table>
		@endif

		@if (!$tendence_responsabilities->isEmpty())

<?php $current_responsability = 0;
			$counter = 0; ?>

			@foreach ($tendence_responsabilities as $key => $responsability)

				@if ($current_responsability != $responsability->id)

		<?php $current_responsability = $responsability->id;
					$counter++; ?>
				@endif
			@endforeach

<?php $peso = intval(100 / $counter); ?>

			<table class="tabla_responsabilidades table table-striped table-bordered table-hover">
				<thead style="background-color: #222B64; color:white">
					<tr>
						<th class="cabeceras-evaluaciones" style="text-align: left">Objetivo</th>
						<th class="cabeceras-evaluaciones" style="text-align: left">Responsable</th>
						<th class="cabeceras-evaluaciones">Estado</th>
						<th class="cabeceras-evaluaciones">Tend</th>
						<th class="cabeceras-evaluaciones" style="text-align: left">Logro</th>
						<th class="cabeceras-evaluaciones" style="text-align: left">Logro Esperado</th>
						<th class="cabeceras-evaluaciones" style="text-align: left">Unidad</th>
						<th class="cabeceras-evaluaciones" style="text-align: left">Peso%</th>
					</tr>
				</thead>
				<tbody>

<?php $current_responsability = 0; ?>

			@foreach ($tendence_responsabilities as $key => $responsability)

				@if ($current_responsability != $responsability->id)

		<?php $current_responsability = $responsability->id; ?>

					<tr>
						<td width="290">
							<a href="/evaluacion-resultados/reporte-objetivo-responsabilidad/{{$id_plan}}/{{$responsability->id}}" class="responsability_name">{{$responsability->description}}</a>
						</td>
						<td width="290">

					@foreach ($users as $key2 => $value2)

						@if ($responsability->id_responsable == $value2->id)

							{{$value2->nombre}} {{$value2->paterno}} {{$value2->materno}}
				<?php break; ?>
						@endif
					@endforeach

						</td>
						<td class="text-center">
							<img src="/img/evaluacion-resultados/reportes/{{($responsability->unidad == 'Si/No' ? ($responsability->logro == $responsability->meta ? 'guion.png' : 'flecha_roja.png') : ($responsability->valor_verde >= $responsability->valor_rojo ? ($responsability->logro > $responsability->meta ? 'flecha_verde.png' : ($responsability->logro < $responsability->meta ? 'flecha_roja.png' : 'guion.png')) : ($responsability->logro < $responsability->meta ? 'flecha_verde.png' : ($responsability->logro > $responsability->meta ? 'flecha_roja.png' : 'guion.png'))))}}">
						</td>
						<td class="text-center">

					@if (!empty($tendence_responsabilities[$key + 1]) && $tendence_responsabilities[$key + 1]->id == $current_responsability)

							<img src="/img/evaluacion-resultados/reportes/{{($responsability->unidad == 'Si/No' ? ($tendence_responsabilities[$key + 1]->logro == $responsability->logro ? 'guion2.png' : ($responsability->logro == $responsability->meta ? 'flecha_verde2.png' : 'flecha_roja2.png')) : ($responsability->valor_verde >= $responsability->valor_rojo ? ($responsability->logro > $tendence_responsabilities[$key + 1]->logro ? 'flecha_verde2.png' : ($responsability->logro < $tendence_responsabilities[$key + 1]->logro ? 'flecha_roja2.png' : 'guion2.png')) : ($responsability->logro < $tendence_responsabilities[$key + 1]->logro ? 'flecha_verde2.png' : ($responsability->logro > $tendence_responsabilities[$key + 1]->logro ? 'flecha_roja2.png' : 'guion2.png'))))}}">
					@endif

						</td>
						<td>{{($responsability->unidad == 'Si/No' ? $responsability->logro : number_format($responsability->logro, 2, '.', ','))}}</td>
						<td>{{($responsability->unidad == 'Si/No' ? $responsability->meta : number_format($responsability->meta, 2, '.', ','))}}</td>
						<td>{{($responsability->unidad == '#' ? '$' : ($responsability->unidad == '9' ? '0' : $responsability->unidad))}}</td>
						<td>{{$peso}}%</td>
					</tr>
				@endif
			@endforeach

				</tbody>
			</table>
		@endif

		@if (!empty($iniciatives))

<?php $color = '';
			$meses = array(0,'ene','feb','mar','abr','may','jun','jul','ago','sep','oct','nov','dic'); ?>

			<table class="tabla_iniciativas table table-striped table-bordered table-hover">
				<thead style="background-color: #222B64; color:white">
					<tr>
						<th class="cabeceras-evaluaciones" style="text-align: left">Iniciativas</th>
						<th class="cabeceras-evaluaciones" style="text-align: left">Corresponsables</th>
						<th class="cabeceras-evaluaciones text-center">Estado</th>
						<th class="cabeceras-evaluaciones text-center">Fecha de Inicio</th>
						<th class="cabeceras-evaluaciones text-center">Fecha de Término</th>
					</tr>
				</thead>
				<tbody>

			@foreach ($iniciatives as $key => $iniciative)

					<tr>
						<td width="290" class="text-left">{{$iniciative->description}}</td>
						<td width="290">

				@if (!empty($iniciative->grupos_responsables))

					@foreach ($users_groups as $key2 => $users_group)
						
						@if (in_array($users_group->id, $iniciative->grupos_responsables))

							<div>{{$users_group->name}}</div>
						@endif
					@endforeach
				@endif

				@if (!empty($iniciative->responsables))

					@foreach ($users as $key2 => $user)
						
						@if (in_array($user->id, $iniciative->responsables))

							<div>{{$user->nombre}} {{$user->paterno}} {{$user->materno}}</div>
						@endif					
					@endforeach
				@endif

						</td>
						<td>

	<?php if (($iniciative->estado == 'Sin Iniciar' && $iniciative->fecha_termino < date('Y-m-d')) || ($iniciative->estado == 'En proceso' && $iniciative->fecha_termino < date('Y-m-d'))){

					$color = '#d9534f';
				}

				else{

					if (($iniciative->estado == 'Sin Iniciar' && $iniciative->fecha_inicio < date('Y-m-d')) || ($iniciative->estado == 'En proceso' && $iniciative->fecha_inicio < date('Y-m-d'))){

						$color = '#f0ad4e';
					}

					else{

						if ($iniciative->estado == 'Concluido' || $iniciative->fecha_inicio > date('Y-m-d')){

							$color = '#5cb85c';
						}

						else{

							if ($iniciative->estado == 'Cancelado'){

								$color = 'gray';
							}
						}
					}
				} ?>

							<div style="background-color: {{$color}}; padding: 5px; color: white" class="text-center">{{$iniciative->estado}}</div>
						</td>
						<td class="text-center">{{substr($iniciative->fecha_inicio, 8, 2) . '-' . $meses[substr($iniciative->fecha_inicio, 5, 2) * 1] . '-' . substr($iniciative->fecha_inicio, 0, 4)}}</td>
						<td class="text-center">{{substr($iniciative->fecha_termino, 8, 2) . '-' . $meses[substr($iniciative->fecha_termino, 5, 2) * 1] . '-' . substr($iniciative->fecha_termino, 0, 4)}}</td>
					</tr>
			@endforeach

				</tbody>
			</table>
		@endif

		</div>
	@else

		<h2>NO HAY PLANES ABIERTOS NI CERRADOS</h2>
	@endif

	</div>
</div>
@endsection

@section('scripts')
<script>

	var id_plan = <?php echo $id_plan?>;
	var id_objetivo_corporativo = <?php echo $id_objetivo_corporativo?>;
		
	$(document).ready(function(){

		// Cambia el plan
		$('select.planes').change(function(){

			window.location = '/evaluacion-resultados/reporte-objetivo-corporativo/' + $(this).val() + '/' + id_objetivo_corporativo;
		});

		// Cambia el objetivo estratégico
		$('select.objetivos_corporativos').change(function(){

			window.location = '/evaluacion-resultados/reporte-objetivo-corporativo/' + id_plan + '/' + $(this).val();
		});
	});
</script>
@endsection