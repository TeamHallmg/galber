@extends('layouts.app')

@section('title', 'Autorizacion')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="img/banner_evaluacion_resultados.png" alt="">
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Autorización</h5>
			<div class="card-body">


@if (!empty($planes))

<?php $meses_completos = array('0', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'); ?>


<div class="row"> 
	<div class="col-12 offset-md-4 col-md-3 text-center">
		<div class="form-group">
			<label for="id_plan" class="font-weight-bolder mb-0">Año:</label>							
			<form id="changePlanForm" action="/autorizacion" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_empleado" value="{{$user->id}}">
			<select name="id_plan" id="id_plan" class="form-control id_plan">
				@for ($i = 0;$i < count($planes);$i++)

					<option value="{{$planes[$i]->id}}" <?php if ($plan->id == $planes[$i]->id){ ?>selected="selected"<?php } ?>>{{$planes[$i]->nombre}}</option>
				@endfor

			</select>
			<input type="submit" style="display: none">
			</form>
		</div>
	</div> 
</div>

 
<div class="card mb-3">
	<h5 class="card-header bg-info text-white font-weight-bolder">Persona</h5>
	<div class="card-body">
	<div class="media">
		<img src="{{asset('img/vacantes/sinimagen.png')}}" style="width: 120px" class="align-self-center mr-3" alt="...">
		<div class="media-body">
		
		<p><h5 class="mt-0 d-inline">Nombres: </h5> {{$user->nombre}} {{$user->paterno}} {{$user->materno}}</p>
		<p><h5 class="mt-0 d-inline">Puesto: </h5>{{(!empty($user->jobPosition) ? $user->jobPosition->name : '')}}</p>
		<p><h5 class="mt-0 d-inline">Departamento: </h5>{{(!empty($user->jobPosition) && !empty($user->jobPosition->area) && !empty($user->jobPosition->area->department) ? $user->jobPosition->area->department->name: '')}}</p>
		<p><h5 class="mt-0 d-inline">Dirección: </h5>{{$user->direccion}}</p>
		<p><h5 class="mt-0 d-inline">Jefe: </h5>{{(!empty($user->boss) ? $user->boss->nombre . ' ' . $user->boss->paterno . ' ' . $user->boss->materno : '')}}</p>
		</div>
	</div>
  </div>
  </div>

@if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin') && $status_plan[0]->status > 2)

<div class="card mb-3">
	<h5 class="card-header bg-info text-white font-weight-bolder">Remover Autorización</h5>
	<div class="card-body">
		
			
			<div class="row">
				<div class="col-md-12 text-center">
					<form method="post" action="/resetear-autorizacion" style="display: inline-block" onsubmit="return confirm('¿Esta seguro de remover la autorizacion de los objetivos? (Si hay resultados serán borrados)')">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id_empleado" value="{{$id_user}}">
						<input type="hidden" name="id_plan" value="{{$plan->id}}">
						<button class="btn btn-danger" type="submit">Remover Autorización</button>
					</form>
				</div>
			</div>

	</div>
</div>
 
@endif

<div class="card mb-3">
	<h5 class="card-header bg-primary text-white font-weight-bolder">Metricas
		
	</h5>
	<div class="card-body"> 
		
<div class="row">
	<div class="col-md-12">
		<table width="100%" style="font-size: 13px" class="tabla_objetivos table table-bordered table-striped">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th class="cabeceras-evaluaciones align-middle" style="padding: 5px 10px; text-align: center; border: 1px solid white;">#</th>
					<th class="cabeceras-evaluaciones align-middle" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Periodo</th>
					<th class="cabeceras-evaluaciones align-middle" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Objetivo</th>
					<th class="cabeceras-evaluaciones align-middle" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Clave</th>
					<th class="cabeceras-evaluaciones align-middle" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Fórmula</th>
					<th class="cabeceras-evaluaciones align-middle" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Unidad de medida</th>
					<th class="cabeceras-evaluaciones align-middle" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Meta</th>
					<th class="cabeceras-evaluaciones align-middle" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Peso</th>
					<th class="cabeceras-evaluaciones align-middle" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Frecuencia</th>
					<!--<th class="cabeceras-evaluaciones align-middle">Co-responsables</th>-->
					<!--<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Tipo de Semáforo</th>-->
					<th class="cabeceras-evaluaciones align-middle" style="padding: 5px 50px; text-align: center; border: 1px solid white;">
						<p style="font-size: 13px">Rango de Semáforo</p>
						<div style="font-size: 13px">
							<span style="background-color: red; padding: 5px 10px">Rojo</span><span style="background-color: #FFDB58; color: #FFDB58; padding: 5px 10px">Am</span><span style="background-color: green; padding: 5px 10px">Verde</span>
						</div>
					</th>
				</tr>
			</thead>
			<tbody>

			<?php $total_peso = 0; ?>

      @for ($i = 0;$i < count($objetivos); $i++)

				<tr class="objetivos">
					
					<td class="text-center">{{$i + 1}}</td>
					<td class="text-center">{{$objetivos[$i]->periodo}}</td>
					<td class="text-center">
						<!--<input type="text" value="{{$objetivos[$i]->nombre}}" style="width: 200px; border-radius: 7px; border: 1px solid #A5A7A8; text-align: center" class="nombre" readonly>-->
						<textarea readonly style="overflow-x: hidden; resize: none" class="nombre form-control size2 text-justify">{{$objetivos[$i]->nombre}}</textarea>
					</td>
					<td class="text-center">
						<select class="id_objetivo_corporativo form-control text-center size3" disabled>

	<?php $counter = 0;
				$current_type = 0; ?>

				@for ($j = 0;$j < count($objetivos_corporativos_sin_responsable);$j++)

					@if ($current_type != $objetivos_corporativos_sin_responsable[$j]->id_tipo)

			<?php $counter = 1;
						$current_type = $objetivos_corporativos_sin_responsable[$j]->id_tipo; ?>
					@endif

					@if ($objetivos_corporativos_sin_responsable[$j]->id == $objetivos[$i]->id_objetivo_corporativo)

							<option >{{$objetivos_corporativos_sin_responsable[$j]->letter}}{{$counter}}</option>
			<?php break; ?>
					@endif
						
		<?php $counter++; ?>
				@endfor

						</select>
					</td>
					<td class="text-center">
						<textarea style="resize: none; overflow-x: hidden" class="formula form-control size2" readonly>{{$objetivos[$i]->formula}}</textarea>
					</td>
					<td class="text-center">
						<select class="tipo form-control size3" disabled>
							<option value="%">%</option>
							<option value="$" <?php if ($objetivos[$i]->tipo == '$'){ ?>selected="selected"<?php } ?>>$</option>
							<option value="#" <?php if ($objetivos[$i]->tipo == '9' || $objetivos[$i]->tipo == '#'){ ?>selected="selected"<?php } ?>>#</option>
							<option value="Si/No" <?php if ($objetivos[$i]->tipo == 'Si/No'){ ?>selected="selected"<?php } ?>>Si/No</option>
						</select>
					</td>
					<td class="text-center">
					  <input type="text" class="objetivo form-control size text-right {{($objetivos[$i]->tipo != 'Si/No' ? ($objetivos[$i]->tipo == '$' ? 'currency' : ($objetivos[$i]->tipo == '#' ? 'numeric' : 'porcentaje')) : '')}}" value="{{$objetivos[$i]->objetivo}}" readonly>
					</td>
					<td class="text-center">
						<input type="text" class="peso size3 text-right form-control numeric" value="{{$objetivos[$i]->peso}}" readonly>
					</td>
					<td class="text-center">
						<select class="frecuencia form-control size" disabled>
							<option value="Mensual">Mensual</option>
							<option value="Bimestral" <?php if ($objetivos[$i]->frecuencia == 'Bimestral'){ ?>selected="selected"<?php } ?>>Bimestral</option>
							<option value="Trimestral" <?php if ($objetivos[$i]->frecuencia == 'Trimestral'){ ?>selected="selected"<?php } ?>>Trimestral</option>
							<option value="Cuatrimestral" <?php if ($objetivos[$i]->frecuencia == 'Cuatrimestral'){ ?>selected="selected"<?php } ?>>Cuatrimestral</option>
							<option value="Semestral" <?php if ($objetivos[$i]->frecuencia == 'Semestral'){ ?>selected="selected"<?php } ?>>Semestral</option>
							<option value="Anual" <?php if ($objetivos[$i]->frecuencia == 'Anual'){ ?>selected="selected"<?php } ?>>Anual</option>
						</select>
					</td>
					<td class="text-center">
					  <button style="cursor: default" class="text-right btn btn-danger middle_size {{($objetivos[$i]->tipo == '%' ? 'porcentaje' : ($objetivos[$i]->tipo == '$' ? 'currency' : ($objetivos[$i]->tipo == '#' ? 'numeric' : '')))}}">{{$objetivos[$i]->valor_rojo}}</button>
						<button style="cursor: default" class="text-right btn btn-success middle_size {{($objetivos[$i]->tipo == '%' ? 'porcentaje' : ($objetivos[$i]->tipo == '$' ? 'currency' : ($objetivos[$i]->tipo == '#' ? 'numeric' : '')))}}">{{$objetivos[$i]->valor_verde}}</button>
					</td>
				</tr>
				
				<?php $total_peso += $objetivos[$i]->peso?>
			@endfor

				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="text-center cabeceras-evaluaciones">Total:</td>
					<td class="text-right peso_total" style="padding-right: 25px"><?php echo $total_peso?></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-12 text-center">
		
@if (count($status_plan) > 0 && $status_plan[0]->status == 2)
	
  @if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin') || (!empty($user->jefe) && $user->jefe == $user_logged->idempleado && $plan->autorizar_propuestas == 2) || ($plan->autorizar_propuestas == 4 && !empty(auth()->user()->employee->grado)))
		<div class="margin-top-20">
			<form action="autorizar" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_plan" value="{{$plan->id}}">
				<input type="hidden" name="id_empleado" value="{{$id_user}}">
				<input type="hidden" name="periodo" value="{{$status_plan[0]->periodo}}">
				<input type="hidden" name="status" value="3">
				<button class="btn btn-primary" type="submit">Aprobar</button>
			</form>
		</div>
		<div class="margin-top-20">
			<form action="autorizar" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_plan" value="{{$plan->id}}">
				<input type="hidden" name="id_empleado" value="{{$id_user}}">
				<input type="hidden" name="periodo" value="{{$status_plan[0]->periodo}}">
				<input type="hidden" name="status" value="1">
				<button class="btn btn-primary" type="submit">Rechazar</button>
			</form>
		</div>
	@endif
@else
	@if (count($status_plan) > 0 && $status_plan[0]->status > 2)

	<div class="col-md-12 h5">
		<span class="d-inline">Fecha de Autorización:</span> 
		<strong>
			{{substr($status_plan[0]->fecha_autorizado, 8)}} de {{$meses_completos[substr($status_plan[0]->fecha_autorizado, 5, 2) * 1]}} del {{substr($status_plan[0]->fecha_autorizado, 0, 4)}}
		</strong>
	</div> 

	@endif
@endif
	</div>
</div>
</div>
</div>



<div class="card mb-3">
	<h5 class="card-header bg-info text-white font-weight-bolder">Observaciones</h5>
	<div class="card-body">
		


 <div class="row margin-top-20">
	<div class="col-md-12">
		<div style="max-height: 200px; overflow-y: auto">
 			<table width="100%" style="font-size: 13px" border="1" class="notas table table-bordered table hover table-striped">
				<thead style="background-color: #222B64; color:white;">
					<tr>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Fecha</th>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Persona</th>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Nota</th>
					</tr>
				</thead>
				<tbody>

      	<?php $meses = array('0', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'); ?>

      	@for ($i = 0;$i < count($notas); $i++) 

					<tr>
						<td class="text-center" style="padding: 5px; border: 0">{{substr($notas[$i]->fecha, 8) * 1}}/{{$meses[substr($notas[$i]->fecha, 5, 2) * 1]}}/{{substr($notas[$i]->fecha, 0, 4)}}</td>
						<td class="text-center" style="padding: 5px; border: 0">{{$notas[$i]->nombre}} {{$notas[$i]->paterno}} {{$notas[$i]->materno}}</td>
						<td class="text-center" style="padding: 5px; border: 0">{{$notas[$i]->mensaje}}</td>
					</tr>
				@endfor

				</tbody>
			</table>
		</div>
		<h4 class="titulos-evaluaciones">Nota</h4>
		<div>
			<textarea style="width: 100%; height: 100px; border-radius: 7px" class="nota"></textarea>
		</div>
		<div class="text-right margin-top-10">
			<button class="btn btn-primary agregar_nota">Agregar nota</button>
		</div>
	</div>
	
</div>
</div>
</div>
@else

<div class="row">
					
	<div class="col-md-12 text-center">
	
		<div class="alert alert-warning h4" role="alert">
			<i class="fa fa-exclamation-triangle"></i> No hay planes abiertos
		</div>

	</div>
</div>
 
@endif
</div>
</div>
</div>
</div>
@endsection

@section('scripts')
	<script>

	  var id_plan = <?php echo (!empty($plan->id) ? $plan->id : 0)?>;
	  var id_empleado = <?php echo (!empty($id_user) ? $id_user : 0)?>;
	  var slide = false;
		
		$(document).ready(function(){

			$.ajaxSetup({
    		headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
			});
			
			$('.tabla_objetivos').DataTable({
				    scrollX: true,
    				scrollCollapse: true,
    				paging: false,
    				searching: false,
    				ordering: false,
    				info: false
				});

			// Clic en la flecha izquierda debajo de los valores rojo y verde de un objetivo
			$('span.semaforos_arrow_left').click(function(){
				
				var total = $(this).parent().attr('data-valores');
				var step = $(this).parent().attr('data-step');
				
				if (step == 0){
					
					step = (total - 1) * -1;
				}
				
				else{
					
					step--;
					step = step * -1;
				}

				$(this).parent().attr('data-step', Math.abs(step));
				$(this).parent().prev().find('.slide').animate({"left":(step * 100) + '%'}, "slow");

				if (!slide){

					slide = true;
					//$('span.metas_arrow_left').click();
				}

				else{

					slide = false;
				}
			});

			// Clic en la flecha derecha debajo de los valores rojo y verde de un objetivo
			$('span.semaforos_arrow_right').click(function(){
				
				var total = $(this).parent().attr('data-valores');
				var step = $(this).parent().attr('data-step');
				
				if (step == (total - 1)){
					
					step = 0;
				}
				
				else{
					
					step++;
					step = step * -1;
				}

				$(this).parent().attr('data-step', Math.abs(step));
				$(this).parent().prev().find('.slide').animate({"left":(step * 100) + '%'}, "slow");

				if (!slide){

					slide = true;
					//$('span.metas_arrow_right').click();
				}

				else{

					slide = false;
				}
			});

			// Clic en la flecha izquierda debajo de las metas de un objetivo
			$('span.metas_arrow_left').click(function(){
				
				var total = $(this).parent().attr('data-valores');
				var step = $(this).parent().attr('data-step');
				
				if (step == 0){
					
					step = (total - 1) * -1;
				}
				
				else{
					
					step--;
					step = step * -1;
				}

				$(this).parent().attr('data-step', Math.abs(step));
				$(this).parent().prev().find('.slide').animate({"left":(step * 100) + '%'}, "slow");

				if (!slide){

					slide = true;
					//$('span.semaforos_arrow_left').click();
				}

				else{

					slide = false;
				}
			});

			// Clic en la flecha derecha debajo de las metas de un objetivo
			$('span.metas_arrow_right').click(function(){
				
				var total = $(this).parent().attr('data-valores');
				var step = $(this).parent().attr('data-step');
				
				if (step == (total - 1)){
					
					step = 0;
				}
				
				else{
					
					step++;
					step = step * -1;
				}

				$(this).parent().attr('data-step', Math.abs(step));
				$(this).parent().prev().find('.slide').animate({"left":(step * 100) + '%'}, "slow");

				if (!slide){

					slide = true;
					//$('span.semaforos_arrow_right').click();
				}

				else{

					slide = false;
				}
			});

			$('select.id_plan').change(function(){

				$('form#changePlanForm').submit();
			});

      $('button.agregar_nota').click(function(){

				var nota = $('textarea.nota').val();

				$.ajax({
        	type:'POST',    
        	url: 'agregar-nota',
          data:{
          	mensaje: nota,
          	id_empleado: id_empleado,
          	id_plan: id_plan
          },
          success: function(data){
             
            var meses = [];
            meses[0] = 'Ene';
            meses[1] = 'Feb';
            meses[2] = 'Mar';
            meses[3] = 'Abr';
            meses[4] = 'May';
            meses[5] = 'Jun';
            meses[6] = 'Jul';
            meses[7] = 'Ago';
            meses[8] = 'Sep';
            meses[9] = 'Oct';
            meses[10] = 'Nov';
            meses[11] = 'Dic';
            var fecha = new Date();
            $('table.notas').prepend('<tr><td class="text-center" style="padding: 5px; border: 0">' + fecha.getDate() + '/' + meses[fecha.getMonth()] + '/' + fecha.getFullYear() + '</td><td class="text-center" style="padding: 5px; border: 0">' + data + '</td><td class="text-center" style="padding: 5px; border: 0">' + nota + '</td></tr>');
            $('textarea.nota').val('');
          }
        });
			});

			$('.porcentaje').inputmask("numeric", {
    		radixPoint: ".",
    		groupSeparator: ",",
    		digits: 2,
    		autoGroup: true,
    		suffix: '%', //No Space, this will truncate the first character
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});

			$('.currency').inputmask("numeric", {
    		radixPoint: ".",
    		groupSeparator: ",",
    		digits: 2,
    		autoGroup: true,
    		prefix: '$', //No Space, this will truncate the first character
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});

			$('.numeric').inputmask("numeric", {
    		groupSeparator: ",",
    		autoGroup: true,
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});
		});
	</script>
@endsection
