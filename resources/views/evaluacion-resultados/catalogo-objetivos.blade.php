@extends('layouts.app')

@section('title', 'Catálogo de Objetivos')

@section('content')
<div class="row">
</div>
<div class="row">
	<div class="col-md-2 text-right">
	@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/banner_evaluacion_resultados.png') }}" alt="">
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Catálogo de Objetivos</h5>
			<div class="card-body">
   
				
				<div class="row mb-3">
					<div class="col-md-12">
					
						<div class="card">
							<h5 class="card-header bg-primary text-white font-weight-bolder">Exportar / Importar Catálogo de Objetivos</h5>
							<div class="card-body">
								
								<div class="row">
									<div class="col-md-3">										
										<form action="/evaluacion-resultados/exportar-plantilla-catalogo-objetivos" method="post" class="exportar_resultados">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<button type="submit" class="btn btn-block btn-success font-weight-bold text-white">Exportar Plantilla</button>
										</form>
									</div>

									<div class="col-md-1 text-center">
										|
									</div>
									<div class="col-md-8">
										
										<form action="/evaluacion-resultados/importar-catalogo-objetivos" method="post" enctype="multipart/form-data">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">

											<div class="custom-file w-75 mr-3">
												<input type="file" name="file" class="custom-file-input" id="customFile">
												<label class="custom-file-label" for="customFile">Seleccionar Archivo a Importar</label>
												
											</div> 
											<button type="submit" class="btn btn-primary font-weight-bold text-white">Importar</button>
									
										</form>  
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
 
 

		<div class="card">
			<h5 class="card-header bg-primary text-white font-weight-bolder">Detalles</h5>
			<div class="card-body">
				

				
				<div class="row mt-3">
					<div class="col-md-12 text-center">
						<a href="/evaluacion-resultados/catalogo-objetivos/create" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Agregar Objetivo de Catálogo</a>
					</div>
				</div>

	@if (count($results) > 0)

		<div class="row mt-3">
			<div class="col-md-12">
				<table width="100%" style="font-size: 13px" class="data-table table table-hover table-striped table-bordered">
					<thead style="background-color: #222B64; color:white;">
						<tr>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Código</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Objetivo</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Descripción</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Dirección</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Objetivo Corporativo</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Plan del Objetivo Corporativo</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Unidad de medida</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Meta</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Peso</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Frecuencia</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Semáforo Rojo</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Semáforo Verde</th>
							<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Acciones</th>
						</tr>
					</thead>
					<tbody>

  	@for ($i = 0;$i < count($results);$i++)

						<tr>
							<td style="padding: 10px 5px" class="text-center">{{$results[$i]->codigo}}</td>
							<td style="padding: 10px 5px" class="text-left">{{$results[$i]->objetivo}}</td>
							<td style="padding: 10px 5px" class="text-left">{{$results[$i]->descripcion}}</td>
							<td style="padding: 10px 5px" class="text-left">{{$results[$i]->direccion}}</td>
							<td style="padding: 10px 5px" class="text-center">

<?php $counter = 0;
      $current_type = 0;
      $objetivo_corporativo = 0;
      $id_plan = 0; ?>

      @foreach ($objetivos_corporativos as $key => $value)
              
        @if ($current_type != $value->id_tipo)

    <?php $counter = 1;
          $current_type = $value->id_tipo; ?>
        @endif

        @if ($value->id == $results[$i]->id_objetivo_corporativo)

    <?php $objetivo_corporativo = $value->letter . $counter;
    			$id_plan = $value->id_plan;
          break; ?>
        @endif

  <?php $counter++; ?>
      @endforeach

      					{{$objetivo_corporativo}}
      				
      				</td>
      				<td style="padding: 10px 5px" class="text-center">{{$planes[$id_plan]}}</td>
							<td style="padding: 10px 5px" class="text-center">{{$results[$i]->unidad}}</td>
							<td class="text-center" style="padding: 10px 5px">{{$results[$i]->meta}}</td>
							<td class="text-center" style="padding: 10px 5px">{{$results[$i]->peso}}</td>
							<td style="padding: 10px 5px" class="text-center">{{$results[$i]->frecuencia}}</td>
							<td class="text-center" style="padding: 10px 5px">{{$results[$i]->valor_rojo}}</td>
							<td class="text-center" style="padding: 10px 5px">{{$results[$i]->valor_verde}}</td>
							<td class="text-center" style="padding: 10px 5px" nowrap><?php if (!in_array($results[$i]->id, $objetivos_usados)){ ?><a href="/evaluacion-resultados/catalogo-objetivos/{{$results[$i]->id}}/edit/" class="btn btn-success" title="Editar"><i class="fas fa-edit"></i></a> <a href="/evaluacion-resultados/catalogo-objetivos/{{$results[$i]->id}}" class="btn btn-danger" onclick="return confirm('Está seguro de borrar este objetivo de catálogo')" title="Eliminar"><i class="fas fa-trash"></i></a><?php } ?></td>
						</tr>
		@endfor

					</tbody>
				</table>
			</div>
		</div>
	@endif

	</div>
	</div>
	</div>
</div>
@endsection

@section('scripts')
	<script>
		
		$(document).ready(function(){
			
			$('.data-table').DataTable({
   				order: [[0,'desc']],
   				language: {
		 				'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
      		}
				});
		});
	</script>
@endsection