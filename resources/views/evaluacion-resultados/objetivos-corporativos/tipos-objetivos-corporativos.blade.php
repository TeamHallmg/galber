@extends('layouts.app')

@section('title', 'Tipos de Objetivos Corporativos')

@section('content')
<?php $index = -1;
			$letters = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'); ?>
<div class="row margin-top-20">
	<div class="col-md-2">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_evaluacion_resultados.png" alt="">
	
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Tipos de Objetivos Corporativos</h5>
			<div class="card-body">
		@if (!empty($planes))

		
		<div class="row"> 
			<div class="col-12 offset-md-4 col-md-3 text-center">
				<div class="form-group">
					<label for="id_plan" class="font-weight-bolder mb-0">Año:</label>							
					
					<select class="form-control planes">
							@foreach ($planes as $key => $plan)				
								<option value="{{$plan->id}}" <?php if ($plan->id == $id_plan){ ?>selected="selected"<?php } ?>>{{$plan->nombre}}</option>
							@endforeach	
					</select>
		
				</div>
			</div> 
		</div>


		
	<div class="card mt-3">
		<h5 class="card-header bg-info text-white font-weight-bolder">Nuevo Tipo de Objetivo Corporativo</h5>
		<div class="card-body">

   
		<form method="post" action="/evaluacion-resultados/save-tipos-objetivos-corporativos" class="save_tipos margin-top-20">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id_plan" value="{{ $id_plan }}">
			<div style="overflow: auto">
				<table class="tabla_tipos table table-striped table-bordered table-hover" style="margin-bottom: 0">
					<thead style="background-color: #222B64; color:white">
						<tr>
							<th class="cabeceras-evaluaciones text-center">ID</th>
							<th class="cabeceras-evaluaciones text-center">Nombre</th>
							<th class="cabeceras-evaluaciones text-center">Descripción</th>
							<th class="cabeceras-evaluaciones text-center">Letra</th>
							<th class="cabeceras-evaluaciones text-center">Color</th>
							<th class="cabeceras-evaluaciones text-center">Responsable de Captura</th>
							<th class="cabeceras-evaluaciones">Borrar</th>
						</tr>
					</thead>
					<tbody>

	 	@foreach ($tipos_objetivos_corporativos as $key => $tipo_objetivo_corporativo)
			
<?php $index++; ?>
			
						<tr class="tipo_objetivo_corporativo">
							<td class="text-center">
								<input type="hidden" name="id_tipo_objetivo_corporativo[{{$index}}]" value="{{$tipo_objetivo_corporativo->id}}">
								{{$tipo_objetivo_corporativo->id}}
							</td>
							<td class="text-center">
								<input type="text" class="form-control size2" name="name[{{$index}}]" required value="{{$tipo_objetivo_corporativo->name}}">
							</td>
							<td class="text-center">
								<textarea class="form-control size2" name="description[{{$index}}]" required>{{$tipo_objetivo_corporativo->description}}</textarea>
							</td>
							<td class="text-center">
								<select class="text-center form-control size" name="letter[{{$index}}]">

			@foreach ($letters as $key => $letter)

									<option value="{{$letter}}" <?php if ($letter == $tipo_objetivo_corporativo->letter){ ?>selected="selected"<?php } ?>>{{$letter}}</option>
			@endforeach

								</select>
							</td>
							<td class="text-center">
								<input type="color" class="form-control size" name="color[{{$index}}]" required value="{{$tipo_objetivo_corporativo->color}}" style="padding: 0">
							</td>
							<td class="text-center">
								<input type="text" class="form-control size2 responsable" placeholder="Escribe el nombre del responsable" style="width: 260px">
								<button type="button" class="btn btn-primary add_responsable" style="display: none" data-index="{{$index}}">
									<i class="fas fa-plus-circle"></i> Agregar
								</button>
								<div style="clear: both">

			@if (!empty($tipo_objetivo_corporativo->id_responsable))

				@foreach ($users as $key2 => $user)
						
					@if ($user->data == $tipo_objetivo_corporativo->id_responsable)

									<input type="hidden" value="{{$user->data}}" name="id_responsable[{{$index}}]">
									<div style="float: left">{{$user->value}}</div>
									<div style="float: right; height: 20px">
										<a href="javascript:;" class="remove_user">X</a>
									</div>
			<?php break; ?>
					@endif					
				@endforeach
			@endif

								</div>
							</td>
							<td class="text-center">
								<button class="btn btn-primary delete_tipo" type="button" data-id="{{$tipo_objetivo_corporativo->id}}">X</button>
							</td>
						</tr>
		@endforeach		

					</tbody>
				</table>
			</div>
			
			<div class="row mt-5">
				<div class="col-md-6">
					<a class="btn btn-primary add_tipo" href="javascript:;">
						<i class="fas fa-plus-circle"></i> Agregar tipo
					</a>
				</div>
				<div class="col-md-6">
					<button class="btn btn-success" type="submit">Guardar tipos</button>
				</div>
			</div>
			 
		</form>
		<form action="/evaluacion-resultados/tipos-objetivos-corporativos" method="post" class="change_id_plan">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id_plan" class="id_plan">
			<input type="submit" style="display: none">
		</form>
	</div>
</div>
	@else

		
	<div class="row">
						
		<div class="col-md-12 text-center">
		
			<div class="alert alert-warning h4" role="alert">
				<i class="fa fa-exclamation-triangle"></i> No hay planes abiertos
			</div>

		</div>
	</div>
	
	@endif
	</div>
</div>
<table style="display: none">
	<tr class="tipos_objetivos_corporativos">
		<td></td>
		<td class="text-center">
			<input type="text" class="form-control size2" name="name[type_number]" required>
		</td>
		<td class="text-center">
			<textarea class="form-control size2" name="description[type_number]" required></textarea>
		</td>
		<td class="text-center">
			<select class="text-center form-control size" name="letter[type_number]">

	@foreach ($letters as $key => $letter)

				<option value="{{$letter}}">{{$letter}}</option>
	@endforeach

			</select>
		</td>
		<td class="text-center">
			<input type="color" class="form-control size" name="color[type_number]" required style="padding: 0">
		</td>
		<td class="text-center">
			<input type="text" class="form-control size2 responsable" placeholder="Escribe el nombre del responsable" style="width: 260px">
			<button type="button" class="btn btn-primary add_responsable" style="display: none" data-index>
				<i class="fas fa-plus-circle"></i> Agregar
			</button>
			<div style="clear: both"></div>
		</td>
		<td class="text-center">
			<button class="btn btn-primary remove_tipo" type="button">X</button>
		</td>
	</tr>
</table>
	</div>
	</div>
@endsection

@section('scripts')
<script>

	// Lista de usuarios para el autocomplete
	var autocomplete_users = <?php echo json_encode($users)?>;
	
	var num_tipos = <?php echo $index?>;
	var nombre = '';
	var id = 0;
		
	$(document).ready(function(){

		$.ajaxSetup({
    	headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    	}
		});

    // Autocomplete
    $('.responsable').autocomplete({
    	lookup: autocomplete_users,
    	onSelect: function (suggestion){
        
        nombre = suggestion.value;
    		id = suggestion.data;
    		$(this).next().show();
    	}
		});

		$('a.add_tipo').click(function(){

			var tipo = $('table tr.tipos_objetivos_corporativos').html();
			num_tipos++;
			tipo = tipo.replace(/type_number/g, num_tipos);
			tipo = tipo.replace(/data-index=""/g, 'data-index="' + num_tipos + '"');
			$('table.tabla_tipos').append('<tr class="tipo_objetivo_corporativo">' + tipo + '</tr>');

			// Autocomplete
			$('.responsable').autocomplete({
    		lookup: autocomplete_users,
    		onSelect: function (suggestion){
        
        	nombre = suggestion.value;
    			id = suggestion.data;
    			$(this).next().show();
    		}
			});

			return false;
		});

		// Agrega un nuevo responsable a un tipo
		$('body').on('click', 'button.add_responsable', function(){

			var num_tipo = $(this).attr('data-index');
			$(this).prev().val('');
			$(this).next().html('<input type="hidden" value="' + id + '" name="id_responsable[' + num_tipo + ']"><div style="float: left">' + nombre + '</div><div style="float: right; height: 20px"><a href="javascript:;" class="remove_user">X</a></div>');
			$(this).hide();
		});

		// Remueve un tipo aun no guardado
		$('body').on('click', 'button.remove_tipo', function(){

			$(this).parent().parent().remove();
		});

		// Borra un tipo
		$('button.delete_tipo').click(function(){

			if (confirm('Todo lo relacionado con esta área estratégica será borrado. ¿De verdad desea eliminarla?')){

				var id_tipo_objetivo_corporativo = $(this).attr('data-id');

				$(this).parent().parent().remove();

				$.ajax({
        	type:'POST',    
        	url: '/evaluacion-resultados/delete-tipo-objetivo-corporativo',
        	data:{
          	id: id_tipo_objetivo_corporativo,
						'_token': '{{ csrf_token() }}'
        	},
        	success: function(data){

        		alert('El área estratégica fue borrada');
        	}
      	});
      }
		});

		// Clic en el boton Guardar Tipos de la parte superior
		$('button.save_tipos').click(function(){

			$('form.save_tipos').submit();
		});

		// Intenta guardar todos los tipos de objetivos corporativos
		$('form.save_tipos').submit(function(){

			// Hay tipos para guardar
			if ($('tr.tipo_objetivo_corporativo').length == 0){

				return false;
			}
		});

		// Borra el responsable
		$('body').on('click', '.remove_user', function(){

			$(this).parent().parent().html('');
		});

		// Cambia el plan
		$('select.planes').change(function(){

			// Recarga la pagina con el nuevo plan
			$('form.change_id_plan .id_plan').val($(this).val());
			$('form.change_id_plan').submit();
		});
	});
</script>
@endsection