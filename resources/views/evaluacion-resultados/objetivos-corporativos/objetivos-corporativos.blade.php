@extends('layouts.app')

@section('title', 'Objetivos Corporativos')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_evaluacion_resultados.png" alt="">
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Objetivos Corporativos</h5>
			<div class="card-body">
    
 
	@if (!empty($planes))


	<div class="row mb-3"> 
		<div class="col-12 offset-md-4 col-md-3 text-center">
			<div class="form-group">
				<label for="id_plan" class="font-weight-bolder mb-0">Plan:</label>	
				<select class="form-control planes">

					@foreach ($planes as $key => $plan)
					
						<option value="{{$plan->id}}" <?php if ($plan->id == $id_plan){ ?>selected="selected"<?php } ?>>{{$plan->nombre}}</option>
					@endforeach
			
						</select>							
			
			</div>
		</div> 
	</div>
 

		@if (!empty($planes_para_copiar) && !$planes_para_copiar->isEmpty())
			
		
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Copiar Objetivos Corporativos</h5>
			<div class="card-body">
				<div class="row mb-3"> 
					<div class="col-12 offset-md-4 col-md-3 text-center">
					
						<form action="/evaluacion-resultados/copiar-objetivos-corporativos" method="post" onsubmit="return confirm('Si cuenta con objetivos corporativos o responsabilidades en el plan actual serán borrados. ¿Desea continuar?')" style="margin-top: 5px">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="plan_a_copiar" value="{{$id_plan}}">

							<div class="form-group">
							<label for="id_plan" class="font-weight-bolder mb-0">Desde el Plan:</label>	
						
							<select class="form-control" name="plan_para_copiar">
			
								@for ($i = 0;$i < count($planes_para_copiar);$i++)

									<option value="{{$planes_para_copiar[$i]->id}}">{{$planes_para_copiar[$i]->nombre}}</option>
								
								@endfor
						
							</select>							
							
						</div>
						<button type="submit" class="btn btn-primary">Copiar</button>
						</form>
					</div> 
				</div>
		</div>
		</div>
		
		
		@endif

		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Detalles</h5>
			<div class="card-body">

<?php $current_id = 0;
			$letter = '';
			$objetives_index = 0; ?>

		@foreach ($objetivos_corporativos as $key => $objetivo_corporativo)

			@if ($current_id != $objetivo_corporativo->id)

				@if ($current_id != 0)

		<div class="row crear_objetivos" style="margin-bottom: 20px">
			<div class="col-md-6">
				<button class="btn btn-primary add_objetive" data-letter="{{$letter}}">
					<i class="fas fa-plus-circle"></i> Agregar objetivo
				</button>
			</div>
			<div class="col-md-6 text-right">
				<button class="btn btn-primary" type="submit">Guardar objetivos</button>
			</div>
		</div>
	</form>	
				@endif

	<?php $current_id = $objetivo_corporativo->id;
				$objetives_index = 0;
				$letter = $objetivo_corporativo->letter; ?>

		<form action="/evaluacion-resultados/save-objetivos-corporativos" method="post" class="save_objetivos">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id_tipo_objetivo_corporativo" value="{{$current_id}}">
			<div class="objetivo_corporativo" style="margin-bottom: 10px; color: white; background-color: {{$objetivo_corporativo->color}}">
				<p>{{$objetivo_corporativo->name}}</p>
				<input type="text" class="form-control" value="{{$objetivo_corporativo->description}}" readonly>
			</div>
			@endif

			@if (!empty($objetivo_corporativo->id_objetivo_corporativo))

	<?php $objetives_index++; ?>

			<div class="row objetivos form-group">
				<div class="col-md-11" style="padding-right: 0">
					<label>{{$objetivo_corporativo->letter}}{{$objetives_index}}</label>
					<input type="text" class="form-control" name="names[]" value="{{$objetivo_corporativo->nombre_objetivo_corporativo}}" required style="display: inline; width: 96%">
					<input type="hidden" name="ids_objetivos[]" value="{{$objetivo_corporativo->id_objetivo_corporativo}}">
				</div>
				<div class="col-md-1">
					<button class="btn btn-primary delete_objetivo" type="button" data-id="{{$objetivo_corporativo->id_objetivo_corporativo}}">X</button>
				</div>
			</div>
			@endif
		@endforeach

		@if ($current_id != 0)

			<div class="row crear_objetivos" style="margin-bottom: 20px">
				<div class="col-md-6">
					<button class="btn btn-primary add_objetive" data-letter="{{$letter}}">
						<i class="fas fa-plus-circle"></i> Agregar objetivo
					</button>
				</div>
				<div class="col-md-6 text-right">
					<button class="btn btn-primary" type="submit">Guardar objetivos</button>
				</div>
			</div>
		</form>	
		@endif

		<form action="/evaluacion-resultados/objetivos-corporativos" method="post" class="change_id_plan">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id_plan" class="id_plan">
			<input type="submit" style="display: none">
		</form>
	</div></div>
	@else


	
	<div class="row">
				
		<div class="col-md-12 text-center">
			
			<div class="alert alert-warning h4" role="alert">
				<i class="fa fa-exclamation-triangle"></i>	No hay planes abiertos
			</div>
	
		</div>
	</div>
		@endif

	</div>
</div>
</div>
</div>
@endsection

@section('scripts')
	<script>
		
		$(document).ready(function(){

			$('button.add_objetive').click(function(){

				var letter = $(this).attr('data-letter');
				var form = $(this).parent().parent().parent();
				var num_elements = form.find('.objetivos').length;
				//var num_elements = $('form.form' + objetive_type + ' .objetivos' + objetive_type).length;
				//var content = '<div class="row objetivos' + objetive_type + '"><div class="col-md-12"><div class="form-group"><label>' + letter + (num_elements + 1) + '</label><input type="text" class="form-control" name="names[]" required style="display: inline; width: 97%" placeholder="Descripción"><input type="hidden" name="ids_objetivos[]"></div></div></div>';
				var content = '<div class="row objetivos form-group"><div class="col-md-11" style="padding-right: 0"><label>' + letter + (num_elements + 1) + '</label><input type="text" class="form-control" name="names[]" required style="display: inline; width: 96%" placeholder="Descripción"></div><div class="col-md-1"><button class="btn btn-primary remove_objetivo" type="button">X</button></div></div>';
				//$('form.form' + objetive_type + ' .crear_objetivos').before(content);
				form.find('.crear_objetivos').before(content);
				return false;
			});

			// Intenta guardar todos los objetivos corporativos
			$('form.save_objetivos').submit(function(){

				// Hay tipos para guardar
				if ($(this).find('.objetivos').length == 0){

					return false;
				}
			});

			// Borra un objetivo corporativo
			$('button.delete_objetivo').click(function(){

				if (confirm('Todo lo relacionado con este objetivo corporativo será borrado. ¿De verdad desea eliminarlo?')){

					var id_objetivo_corporativo = $(this).attr('data-id');
					$(this).parent().parent().remove();

					$.ajax({
        		type:'POST',    
        		url: '/evaluacion-resultados/delete-objetivo-corporativo',
        		data:{
          		id: id_objetivo_corporativo,
							'_token': '{{ csrf_token() }}'
        		},
        		success: function(data){

        			alert('El objetivo corporativo fue borrado');
        		}
      		});
      	}
			});

			// Remueve un objetivo corporativo
			$('body').on('click', 'button.remove_objetivo', function(){

				$(this).parent().parent().remove();
			});

			// Cambia el plan
			$('select.planes').change(function(){

				// Recarga la pagina con el nuevo plan
				$('form.change_id_plan .id_plan').val($(this).val());
				$('form.change_id_plan').submit();
			});
		});
	</script>
@endsection