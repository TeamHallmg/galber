@extends('layout')

@section('title', 'Informe Consolidado')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-responsive" src="../img/evaluacion-resultados/informe-consolidado/ban.png" alt="">

	<!--</div>
		<div class="col-md-12">-->
			<!--<img class="img-responsive" width="100%" src="img/evaluacion-resultados/informe-consolidado/formato.png" alt="">-->

			</div>

</div>

<div class="row">
	<div class="col-md-12">
		<div style="display: inline-block; width: 18%">Nombre del empleado:</div>
		<div style="display: inline-block; width: 81.5%">
			<input type="text" value="{{$empleado->first_name}} {{$empleado->last_name}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; margin-bottom: 10px; width: 100%">
		</div>	
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div style="display: inline-block; width: 13%">Puesto:</div>
		<div style="display: inline-block; width: 86%">
			<input type="text" value="{{(!empty($empleado->puesto) ? $empleado->puesto : '')}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; width: 100%">
		</div>
	</div>
	<div class="col-md-6">
		<div style="display: inline-block; width: 10%">Área:</div>
		<div style="display: inline-block; width: 89%">
			<input type="text" value="{{(!empty($empleado->area) ? $empleado->area : '')}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; width: 100%">
		</div>
	</div>
</div>
<div class="row margin-top-20">
	<div class="col-md-6">
		<div>Periodo: <?php echo $nombre_periodo?></div>
	</div>
	<div class="col-md-6">
		<div>Plan: <?php echo $resultados[0]->nombre?></div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table width="100%" border="1" class="data-table usuarios_informe">
			<thead>
				<tr>
					<th class="cabeceras-tablas-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Evaluación</th>
					<th class="cabeceras-tablas-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Ponderación</th>
					<th class="cabeceras-tablas-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Puntuación Deseada</th>
					<th class="cabeceras-tablas-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Puntuación Obtenida</th>
					<th class="cabeceras-tablas-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Porcentaje</th>
					<th class="cabeceras-tablas-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Calificación Final</th>
				</tr>
			</thead>
			<tbody>

<?php $actual_familia = 0;
    	$calificacion_maxima = 0;
    	$calificacion_obtenida = 0;
    	$calificaciones_finales = 0; 
    	$actual_ponderacion = 0; ?>

			@for ($i = 0;$i < count($calificaciones);$i++)

				@if ($actual_familia != $calificaciones[$i]->id_familia_factor)

					@if ($actual_familia != 0)
				
						@if ($calificacion_maxima == 0)

					<?php $calificacion_maxima = 1?>
						@endif

			<?php $porcentaje = $calificacion_obtenida / $calificacion_maxima;
	      		$calificacion = ($porcentaje * $actual_ponderacion);
						$calificaciones_finales += $calificacion; ?>

					<td class="text-center"><?php echo $actual_ponderacion . '%'?></td>
					<td class="text-center"><?php echo $calificacion_maxima?></td>
					<td class="text-center"><?php echo $calificacion_obtenida?></td>
					<td class="text-center"><?php echo number_format($porcentaje * 100, 2, '.', ',') . '%'?></td>
					<td class="text-center"><?php echo number_format($calificacion, 2, '.', ',')?></td>
				</tr>
				  @endif

		<?php $actual_familia = $calificaciones[$i]->id_familia_factor;
		      $actual_ponderacion = $calificaciones[$i]->ponderacion;
		      $calificacion_maxima = $calificacion_obtenida = $porcentaje = $calificacion = 0; ?>

				<tr>
					<td class="text-center"><?php echo ($actual_familia == 1 ? 'Genericos' : ($actual_familia == 2 ? 'Especificos' : 'Mando y conduccion'))?></td>
				@endif

	<?php $calificacion_maxima += $calificaciones[$i]->valor_esperado;
	      $calificacion_obtenida += $calificaciones[$i]->valor_obtenido; ?>
	    @endfor
					
		@if ($calificacion_maxima == 0)

	   <?php $calificacion_maxima = 1?>
		@endif

<?php $porcentaje = $calificacion_obtenida / $calificacion_maxima;
	    $calificacion = ($porcentaje * $actual_ponderacion);
			$calificaciones_finales += $calificacion;
      $calificaciones_finales += ($resultados[0]->resultado * ($resultados[0]->maxima_cal / 100)); ?>

					<td class="text-center"><?php echo $actual_ponderacion . '%'?></td>
					<td class="text-center"><?php echo $calificacion_maxima?></td>
					<td class="text-center"><?php echo $calificacion_obtenida?></td>
					<td class="text-center"><?php echo number_format($porcentaje * 100, 2, '.', ',') . '%'?></td>
					<td class="text-center"><?php echo number_format($calificacion, 2, '.', ',')?></td>
				</tr>
				<tr>
					<td class="text-center">Objetivos Cuantificables</td>
					<td class="text-center"><?php echo $resultados[0]->maxima_cal . '%'?></td>
					<td class="text-center">100</td>
					<td class="text-center"><?php echo $resultados[0]->resultado?></td>
					<td class="text-center"><?php echo number_format(($resultados[0]->resultado), 2, '.', ',') . '%'?></td>
					<td class="text-center"><?php echo number_format(($resultados[0]->resultado * ($resultados[0]->maxima_cal / 100)), 2, '.', ',')?></td>
				</tr>
				<tr>
					<td class="text-center">Calificacion de Evaluacion Total</td>
					<td class="text-center">100%</td>
					<td></td>
					<td></td>
					<td></td>
					<td class="text-center"><?php echo number_format($calificaciones_finales, 2, '.', ',')?></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="row margin-top-20">
	<div class="col-md-12">
		<div style="max-height: 200px; overflow-y: auto">
			<h3 style="margin-top: 0" class="titulos-evaluaciones">Recomendaciones</h3>
			<table width="100%" style="font-size: 13px" border="1" class="recomendaciones">
				<thead>
					<tr>
						<th class="text-center cabeceras-tablas-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Fecha</th>
						<th class="text-center cabeceras-tablas-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Persona</th>
						<th class="text-center cabeceras-tablas-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Mensaje</th>
					</tr>
				</thead>
				<tbody>

      	<?php $meses = array('0', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');
           		$meses_completos = array('0', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
      	?>

      	@for ($i = 0;$i < count($notas); $i++) 

					<tr>
						<td class="text-center" style="padding: 5px; border: 0">{{substr($notas[$i]->created_at, 8, 2) * 1}}/{{$meses[substr($notas[$i]->created_at, 5, 2) * 1]}}/{{substr($notas[$i]->created_at, 0, 4)}}</td>
						<td class="text-center" style="padding: 5px; border: 0">{{auth()->user()->first_name}} {{auth()->user()->last_name}}</td>
						<td class="text-center" style="padding: 5px; border: 0">{{$notas[$i]->recomendacion}}</td>
					</tr>
				@endfor

				</tbody>
			</table>
		</div>

     @if (!empty($empleado->boss_id) && $empleado->boss_id == auth()->user()->id)
		<h4 style="color: #B6BD00">Recomendación</h4>
		<div>
			<textarea style="width: 100%; height: 100px; border-radius: 7px" class="nota"></textarea>
		</div>
		<div class="text-right margin-top-10">
			<button class="btn btn-primary btn-globalgas-red agregar_recomendacion">Agregar recomendación</button>
		</div>
	@endif

	</div>
</div>
@endsection

@section('scripts')
	<script>

	  var id_periodo = <?php echo $id_periodo?>;
	  var id_plan = <?php echo $resultados[0]->id?>;
	  var id_empleado = <?php echo $empleado->id?>;
		
		$(document).ready(function(){

			$.ajaxSetup({
    		headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
			});

      $('button.agregar_recomendacion').click(function(){

				var nota = $('textarea.nota').val();

				$.ajax({
        	type:'POST',    
        	url: '/agregar-recomendacion',
          data:{
          	mensaje: nota,
          	id_empleado: id_empleado,
          	id_plan: id_plan,
          	id_periodo: id_periodo
          },
          success: function(data){
             
            var meses = [];
            meses[0] = 'Ene';
            meses[1] = 'Feb';
            meses[2] = 'Mar';
            meses[3] = 'Abr';
            meses[4] = 'May';
            meses[5] = 'Jun';
            meses[6] = 'Jul';
            meses[7] = 'Ago';
            meses[8] = 'Sep';
            meses[9] = 'Oct';
            meses[10] = 'Nov';
            meses[11] = 'Dic';
            var fecha = new Date();
            $('table.recomendaciones').prepend('<tr><td class="text-center" style="padding: 5px; border: 0">' + fecha.getDate() + '/' + meses[fecha.getMonth()] + '/' + fecha.getFullYear() + '</td><td class="text-center" style="padding: 5px; border: 0">' + data + '</td><td class="text-center" style="padding: 5px; border: 0">' + nota + '</td></tr>');
            $('textarea.nota').val('');
          }
        });
			});
		});
	</script>
@endsection