﻿@extends('layouts.app')

@section('title', 'Evaluacion de Resultados')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/banner_evaluacion_resultados.png') }}" alt="">
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">¿Qué es?
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
  
			<p class="text-justify" >Este módulo para Evaluación de Resultados facilita la detección de desviaciones respecto de las metas y objetivos planificados por la organización, para que con la información registrada se analicen las causas que las originan e implementar acciones correctivas y de ajuste.<br>El módulo para Evaluación de Resultados, capta información relevante y confiable para sustentar acciones de mejora.</p>

			</div>

</div>
</div>
</div>
@endsection
