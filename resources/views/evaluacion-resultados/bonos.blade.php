@extends('layouts.app')

@section('title', 'Bonos')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/banner_evaluacion_resultados.png') }}" alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Bonos</h3>
		<hr>


@if (!empty($planes))

		<div class="row">
			<div class="col-md-12 text-center">
				<form id="changePlanForm" action="bonos" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div>Plan:  <select name="id_plan" style="border-radius: 7px; margin-bottom: 4px" class="id_plan">

											@for ($i = 0;$i < count($planes);$i++)

											 	<option value="{{$planes[$i]->id}}" <?php if ($plan->id == $planes[$i]->id){ ?>selected="selected"<?php } ?>>{{$planes[$i]->nombre}}</option>
											@endfor

											</select>
					</div>
					<input type="submit" style="display: none">
				</form>
			</div>
		</div>

@if (!empty($users))

<div class="row">
	<div class="col-md-12">
		<a href="/reporte-bonos/{{$plan->id}}" class="btn btn-primary">Exportar Bonos</a>
	</div>
</div>
<div class="row">
	<div class="col-md-12 bonos">
		<table width="100%" style="font-size: 13px" class="data-table table table-striped table-bordered table-hover reportes dt-responsive">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">ID</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Persona</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Puesto</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Área</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Bono</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;" class="detalle cabeceras-evaluaciones">Detalle Bono</th>
				</tr>
			</thead>
			<tbody>

      @for ($i = 0;$i < count($users);$i++)

				<tr>
					<td style="padding: 10px 5px" class="text-center">{{$users[$i]->idempleado}}</td>
					<td style="padding: 10px 5px" class="text-left">{{$users[$i]->nombre}} {{$users[$i]->paterno}} {{$users[$i]->materno}}</td>
					<td style="padding: 10px 5px" class="text-left">{{(!empty($users[$i]->job_position) ? $users[$i]->job_position : '')}}</td>
					<td style="padding: 10px 5px" class="text-left">{{(!empty($users[$i]->department) ? $users[$i]->department : '')}}</td>
					<td style="padding: 10px 5px" class="text-right">{{(!empty($users[$i]->bono) ? '$' .number_format($users[$i]->bono->total_bono, 2) : '')}}</td>
					<td class="text-center detalle" align="center" width="25%">

				@if (!empty($users[$i]->bono))
						<form method="post" action="ultimo-bono" style="display: inline-block">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="id_empleado" value="{{$users[$i]->id}}">
							<input type="hidden" name="id_plan" value="{{$plan->id}}">
							<button class="btn btn-primary" type="submit">Último Cálculo</button>
						</form>
						<form method="post" action="detalle-bono" style="display: inline-block">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="id_empleado" value="{{$users[$i]->id}}">
							<input type="hidden" name="id_plan" value="{{$plan->id}}">
							<button class="btn btn-primary" type="submit">Detalle Bono</button>
						</form>
				@endif

					</td>
				</tr>
			@endfor

			</tbody>
		</table>
	</div>
</div>

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20 titulos-evaluaciones">No tiene colaboradores con resultados para ver su bono alcanzado</h4>
	</div>
</div>
@endif

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20 titulos-evaluaciones">No hay planes abiertos</h4>
	</div>
</div>
@endif
</div>
</div>
@endsection

@section('scripts')
	<script>
		
		var id_plan = <?php echo (!empty($plan) ? $plan->id : 0)?>;
		var tipo_plan = <?php echo (!empty($plan) ? $plan->tipo : 1)?>;
		
		$(document).ready(function(){

			$('select.id_plan').change(function(){

				$('form#changePlanForm').submit();
			});
				
			$('.data-table').DataTable({
					dom: 'lfrtip',
   				order: [[3,'desc']],
   				language: {
		 				'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
      		},
   				buttons: [
        	{
          	extend: 'excel',
						text: 'Exportar a Excel',
						titleAttr: 'Exportar a Excel',
						title: 'Evaluación de Resultados',
						exportOptions: {
							columns: ':not(.detalle)'
						}
					}
        	],
				});
			
		});
	</script>
@endsection
