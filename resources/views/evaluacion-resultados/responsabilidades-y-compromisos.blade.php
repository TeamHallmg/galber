@extends('layouts.app')

@section('title', 'Responsabilidades y Compromisos')

@section('content')
<?php $index = -1; ?>
<div class="row margin-top-20">
	<div class="col-md-2">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_evaluacion_resultados.png" alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Responsabilidades y Compromisos</h3>
		<hr>

	@if (!empty($planes))

		<div class="margin-top-20 text-center">
			Plan 
			<select class="form-group id_plan" style="border-radius: 7px; margin-bottom: 4px">

		@foreach ($planes as $key => $plan)
		
			<option value="{{$plan->id}}" <?php if ($plan->id == $id_plan){ ?>selected="selected"<?php } ?>>{{$plan->nombre}}</option>
		@endforeach

			</select>
		</div>

		@if (!empty($objetivos_corporativos))

		<h4 class="titulos-evaluaciones mt-5 font-weight-bold">Objetivos Corporativos</h4>

<?php $cells = array();
			$counter = 0;
			$current_type = 0;
			$maximum = 0; ?>

			@foreach ($objetivos_corporativos as $key => $objetivo_corporativo)
			
				@if ($current_type != $objetivo_corporativo->id_tipo)

					@if ($current_type != 0)

						@if ($counter > $maximum)

				<?php $maximum = $counter; ?>
						@endif

			<?php $cells[] = $counter; ?>
					@endif

		<?php $current_type = $objetivo_corporativo->id_tipo;
					$counter = 0; ?>
				@endif

	<?php $counter++; ?>
			@endforeach
		
			@if ($current_type != 0)

				@if ($counter > $maximum)

		<?php $maximum = $counter; ?>
				@endif

	<?php $cells[] = $counter; ?>
			@endif

<?php $counter = 0; ?>

		<table class="table table-striped table-bordered table-hover">
			<thead style="background-color: #222B64; color:white">
				<tr>

			@foreach ($cells as $key => $cell)
		
					<th colspan="2" style="color: white; background-color: {{$objetivos_corporativos[$counter]->color}}">{{$objetivos_corporativos[$counter]->nombre_tipo}}</th>
	<?php $counter = $counter + $cell; ?>
			@endforeach					

				</tr>
			</thead>
			<tbody>

<?php $counter = 0; ?>

			@for ($i = 0;$i < $maximum;$i++)

				<tr>

	<?php $counter = 0; ?>

				@foreach ($cells as $key => $cell)

					@if ($cell > $i)

					<td style="color: white; background-color: {{$objetivos_corporativos[$counter]->color}}">{{$objetivos_corporativos[$counter + $i]->letter}}{{$i + 1}}</td>
					<td>{{$objetivos_corporativos[$counter + $i]->name}}</td>
					@else

					<td></td>
					<td></td>
					@endif

		<?php $counter = $counter + $cell; ?>
				@endforeach

				</tr>
			@endfor

			</tbody>
		</table>
		@endif

		@if (!empty($responsabilities) || ($canEdit && !empty($objetivos_corporativos)))

		<hr class="divisor">
		<div class="row">
			<div class="col-md-8">
				<h3 class="bsc_title" style="margin-top: 5px">Responsabilidades y Compromisos</h3>
			</div>
			<div class="col-md-4 text-right">

			@if ($canEdit)
				
				<button class="btn btn-primary save_responsabilities">Guardar responsabilidades</button>
			@endif
			
			</div>
		</div>
		<form method="post" action="/evaluacion-resultados/save-responsabilities" class="save_responsabilities">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id_plan" value="{{ $id_plan }}">
			<div style="overflow: auto">
				<table class="tabla_compromisos table table-striped table-bordered table-hover" style="margin-bottom: 0">
					<thead style="background-color: #222B64; color:white">
						<tr>
							<th class="cabeceras-evaluaciones text-center">Clave</th>
							<th class="cabeceras-evaluaciones text-center">Objetivo</th>
							<th class="cabeceras-evaluaciones text-center">Meta</th>
							<th class="cabeceras-evaluaciones text-center">Responsable</th>
							<th class="cabeceras-evaluaciones text-center">Responsable de Captura</th>
							<th class="cabeceras-evaluaciones text-center">Tipo de Indicador</th>
							<th class="cabeceras-evaluaciones text-center">Frecuencia</th>
							<th class="cabeceras-evaluaciones text-center">Frecuencia de captura</th>
							<th class="cabeceras-evaluaciones text-center">Fuente</th>
							<th class="cabeceras-evaluaciones text-center">Co-responsable</th>
							<th class="cabeceras-evaluaciones text-center">Status</th>
							<th class="cabeceras-evaluaciones text-center">Logro Esperado</th>
							<th class="cabeceras-evaluaciones text-center">Unidad de medida</th>
							<th class="cabeceras-evaluaciones text-center">
								<p style="font-size: 13px">Semáforo de Desempeño</p>
								<div>
									<span style="background-color: red; padding: 5px 10px"></span><span style="background-color: #FFDB58; color: #FFDB58; padding: 5px 10px"></span><span style="background-color: green; padding: 5px 10px"></span>
								</div>
							</th>

						@if ($canEdit)

							<th class="cabeceras-evaluaciones">Borrar</th>
						@else

							<th class="cabeceras-evaluaciones"></th>
						@endif

						</tr>
					</thead>
					<tbody>

	 	<?php foreach ($responsabilities as $key => $value){
			
					$index++;
					$current_type = 0; ?>
			
						<tr class="responsabilidad">
							<td class="text-center">
								<input type="hidden" name="id_responsabilidad[{{$index}}]" value="{{$value->id}}">
								<select class="text-center form-control size3" name="id_objetivo_corporativo[{{$index}}]">

							@if ($canEdit)

					<?php foreach ($objetivos_corporativos as $key2 => $objetivo_corporativo){

									if ($current_type != $objetivo_corporativo->id_tipo){

										$counter = 1;
										$current_type = $objetivo_corporativo->id_tipo;
									}	?>
						
									<option value="{{$objetivo_corporativo->id}}" <?php if ($value->id_objetivo_corporativo == $objetivo_corporativo->id){ ?>selected="selected"<?php } ?>>{{$objetivo_corporativo->letter}}{{$counter}}</option>
						
						<?php $counter++;
								} ?>
							@else

					<?php $current_type = 0;

								foreach ($objetivos_corporativos as $key2 => $objetivo_corporativo){
						
									if ($current_type != $objetivo_corporativo->id_tipo){

										$counter = 1;
										$current_type = $objetivo_corporativo->id_tipo;
									}

									if ($value->id_objetivo_corporativo == $objetivo_corporativo->id){ ?>

									<option value="{{$objetivo_corporativo->id}}">{{$objetivo_corporativo->letter}}{{$counter}}</option>

							<?php break;
									}

									$counter++;
								} ?>
							@endif

								</select>
							</td>
							<td class="text-center">
								<textarea class="form-control size2" name="description[{{$index}}]" required <?php if (!$canEdit){ ?>readonly<?php } ?>>{{$value->description}}</textarea>
							</td>
							<td class="text-center">
								<textarea class="form-control size2" name="formula[{{$index}}]" required <?php if (!$canEdit){ ?>readonly<?php } ?>>{{$value->formula}}</textarea>
							</td>
							<td class="text-center">
								
							@if ($canEdit)

								<input type="text" class="form-control size2 responsable" placeholder="Escribe el nombre del responsable" style="width: 260px">
							@endif

								<button type="button" class="btn btn-primary add_responsable" style="display: none" data-index="{{$index}}">Agregar</button>
								<div style="clear: both">

				<?php if (!empty($value->id_responsable)){

								foreach ($users as $key2 => $user){
						
									if ($user->id == $value->id_responsable){ ?>

									<input type="hidden" value="{{$user->id}}" name="id_responsable[{{$index}}]" class="id_responsable">
									<div style="float: left">{{$user->nombre}} {{$user->paterno}} {{$user->materno}}</div>

										@if ($canEdit)

									<div style="float: right; height: 20px">
										<a href="javascript:;" class="remove_user">X</a>
									</div>
										@endif
							<?php break;
									}
								}
							} ?>

								</div>
							</td>
							<td class="text-center">
							
							@if ($canEdit)

								<input type="text" class="form-control size2 capturista" placeholder="Escribe el nombre del responsable de captura" style="width: 260px">
							@endif

								<button type="button" class="btn btn-primary add_capturista" style="display: none" data-index="{{$index}}">
									<i class="fas fa-plus-circle"></i> Agregar
								</button>
								<div style="clear: both">

				<?php if (!empty($value->id_capturista)){

								foreach ($users as $key2 => $user){
						
									if ($user->id == $value->id_capturista){ ?>

									<input type="hidden" value="{{$user->id}}" name="id_capturista[{{$index}}]" class="id_capturista">
									<div style="float: left">{{$user->nombre}} {{$user->paterno}} {{$user->materno}}</div>

										@if ($canEdit)

									<div style="float: right; height: 20px">
										<a href="javascript:;" class="remove_user">X</a>
									</div>
										@endif
						<?php break;
									}
								}
							} ?>

								</div>
							</td>
							<td class="text-center">
								<select class="text-center form-control size" name="tipo_indicador[{{$index}}]">

								@if ($canEdit)

									<option value="De Resultado">De Resultado</option>
									<option value="De Tendencia" <?php if ($value->tipo_indicador == 'De Tendencia'){ ?>selected="selected"<?php } ?>>De Tendencia</option>
								@else

									<option>{{$value->tipo_indicador}}</option>
								@endif

								</select>
							</td>
							<td class="text-center">
								<select class="text-center form-control size" name="frecuencia[{{$index}}]">

								@if ($canEdit)
									<option name="Mensual">Mensual</option>
									<option name="Bimestral" <?php if ($value->frecuencia == 'Bimestral'){ ?>selected="selected"<?php } ?>>Bimestral</option>
									<option name="Trimestral" <?php if ($value->frecuencia == 'Trimestral'){ ?>selected="selected"<?php } ?>>Trimestral</option>
									<option name="Anual" <?php if ($value->frecuencia == 'Anual'){ ?>selected="selected"<?php } ?>>Anual</option>
								@else

									<option>{{$value->frecuencia}}</option>
								@endif

								</select>
							</td>
							<td class="text-center">
								<select class="text-center form-control size" name="frecuencia_captura[{{$index}}]">

								@if ($canEdit)
									<option name="Mensual">Mensual</option>
									<option name="Bimestral" <?php if ($value->frecuencia_captura == 'Bimestral'){ ?>selected="selected"<?php } ?>>Bimestral</option>
									<option name="Trimestral" <?php if ($value->frecuencia_captura == 'Trimestral'){ ?>selected="selected"<?php } ?>>Trimestral</option>
									<option name="Anual" <?php if ($value->frecuencia_captura == 'Anual'){ ?>selected="selected"<?php } ?>>Anual</option>
								@else

									<option>{{$value->frecuencia_captura}}</option>
								@endif

								</select>
							</td>
							<td class="text-center">
								<textarea class="form-control size" name="fuente[{{$index}}]" required <?php if (!$canEdit){ ?>readonly<?php } ?>>{{$value->fuente}}</textarea>
							</td>
							<td class="text-center">
								<div class="row">

					<?php if (count($users_groups) > 0){ ?>

									<div class="col-md-12" style="min-width: 170px">

									@if ($canEdit)

										<input type="text" class="form-control size2 grupos_corresponsables" placeholder="Escribe el nombre del grupo">
									@endif

										<button type="button" class="btn btn-primary add_grupo" style="display: none" data-index="{{$index}}">Agregar</button>

						<?php if (!empty($value->grupos_corresponsables)){

										foreach ($users_groups as $key2 => $users_group){
						
											if (in_array($users_group->data, $value->grupos_corresponsables)){ ?>

										<div style="clear: both">
											<input type="hidden" value="{{$users_group->data}}" name="grupos_corresponsables[{{$index}}][]">
											<div style="float: left">{{$users_group->value}}</div>

										@if ($canEdit)

											<div style="float: right; height: 20px">
												<!--<button class="btn btn-primary remove_corresponsable" styl
												e="padding: 0 1px">x</button>-->
												<a href="javascript:;" class="remove_corresponsable">X</a>
											</div>
										@endif

										</div>
								<?php }
										}
									} ?>

									</div>
					<?php } ?>

									<div class="col-md-12" style="min-width: 170px">
										
									@if ($canEdit)

										<input type="text" class="form-control size2 corresponsables" placeholder="Escribe el nombre del usuario">
									@endif

										<button type="button" class="btn btn-primary add" style="display: none" data-index="{{$index}}">Agregar</button>

						<?php if (!empty($value->corresponsables)){

										foreach ($users as $key2 => $user){
						
											if (in_array($user->id, $value->corresponsables)){ ?>

										<div style="clear: both">
											<input type="hidden" value="{{$user->id}}" name="corresponsables[{{$index}}][]">
											<div style="float: left">{{$user->nombre}} {{$user->paterno}} {{$user->materno}}</div>

										@if ($canEdit)

											<div style="float: right; height: 20px">
												<!--<button class="btn btn-primary remove_corresponsable" style="padding: 0 1px">x</button>-->
												<a href="javascript:;" class="remove_corresponsable">X</a>
											</div>
										@endif

										</div>
								<?php }
										}
									} ?>

									</div>
								</div>
							</td>
							<td class="text-center">
								<select class="text-center form-control size2" name="id_status[{{$index}}]">

								@if ($canEdit)

									<option value="1">a) Indicador disponible y utilizado.</option>
									<option value="2" <?php if ($value->id_status == 2){ ?>selected="selected"<?php } ?>>b) Indicador disponible pero difiere su fórmula de cálculo</option>
									<option value="3" <?php if ($value->id_status == 3){ ?>selected="selected"<?php } ?>>c) Indicador nuevo e información disponible</option>
									<option value="4" <?php if ($value->id_status == 4){ ?>selected="selected"<?php } ?>>d) Indicador nuevo e información no disponible</option>
								@else

									<option>{{($value->id_status == 1 ? 'a. Indicador disponible y utilizado.' : ($value->id_status == 2 ? 'b. Indicador disponible pero difiere su fórmula de cálculo' : ($value->id_status == 3 ? 'c. Indicador nuevo e información disponible' : 'd. Indicador nuevo e información no disponible')))}}</option>
								@endif

								</select>
							</td>
							<td class="text-center">

							@if ($value->unidad != 'Si/No')

								<input type="text" class="form-control size text-right" name="meta[{{$index}}]" required value="{{$value->meta}}" <?php if (!$canEdit){ ?>readonly<?php } ?> style="width: 90px">
							@else

								<select class="form-control size" name="meta[{{$index}}]" required style="width: 90px">
									
								@if ($canEdit)

									<option value="Si">Si</option>
									<option value="No" <?php if ($value->meta == 'No'){ ?>selected="selected"<?php } ?>>No</option>
								@else

									<option>{{$value->meta}}</option>
								@endif

								</select>
							@endif

							</td>
							<td class="text-center">
								<select class="text-center form-control size3 unidad" name="unidad[{{$index}}]">

								@if ($canEdit)
									<option value="%">%</option>
									<option value="#" <?php if ($value->unidad == '#'){ ?>selected="selected"<?php } ?>>$</option>
									<option value="9" <?php if ($value->unidad == '9'){ ?>selected="selected"<?php } ?>>0</option>
									<option value="Si/No" <?php if ($value->unidad == 'Si/No'){ ?>selected="selected"<?php } ?>>Si / No</option>
								@else

									<option>{{($value->unidad == '#' ? '$' : ($value->unidad == '9' ? '0' : ($value->unidad == 'Si/No' ? 'Si / No' : '%')))}}</option>
								@endif

								</select>
							</td>
							<td class="text-center">
								<div style="min-width: 190px">

							@if ($canEdit)		

								@if ($value->unidad != 'Si/No')

									<input type="number" step=".01" class="form-control size3" name="valor_rojo[{{$index}}]" required style="display: inline-block" value="{{$value->valor_rojo}}">
									<input type="number" step=".01" class="form-control size3" name="valor_verde[{{$index}}]" required style="display: inline-block" value="{{$value->valor_verde}}">
								@else

									<select class="form-control size3" name="valor_rojo[{{$index}}]" required style="display: inline-block">
										<option value="Si">Si</option>
										<option value="No" <?php if ($value->valor_rojo == 'No'){ ?>selected="selected"<?php } ?>>No</option>
									</select>
									<select class="form-control size3" name="valor_verde[{{$index}}]" required style="display: inline-block">
										<option value="Si">Si</option>
										<option value="No" <?php if ($value->valor_verde == 'No'){ ?>selected="selected"<?php } ?>>No</option>
									</select>
								@endif
							@else

									<button style="cursor: default" class="btn btn-danger">{{($value->unidad == '%' ? number_format($value->valor_rojo, 2, '.', ',') . '%' : ($value->unidad == '#' ? '$' . number_format($value->valor_rojo) : ($value->unidad == '9' ? number_format($value->valor_rojo) : $value->valor_rojo)))}}</button>
									<button style="cursor: default" class="btn btn-success">{{($value->unidad == '%' ? number_format($value->valor_verde, 2, '.', ',') . '%' : ($value->unidad == '#' ? '$' . number_format($value->valor_verde) : ($value->unidad == '9' ? number_format($value->valor_verde) : $value->valor_verde)))}}</button>
							@endif

								</div>
							</td>
							<td class="text-center">

							@if ($canEdit)

								<button class="btn btn-primary delete_responsability" type="button" data-id="{{$value->id}}">X</button>
							@endif

							</td>
						</tr>
		<?php } ?>

					</tbody>
				</table>
			</div>

		@if ($canEdit)

			<div class="row margin-top-20">
				<div class="col-md-6 text-center">
					<a class="btn btn-primary add_responsability" href="javascript:;">
						<i class="fas fa-plus-circle"></i> Agregar responsabilidad
					</a>
				</div>
				<div class="col-md-6 text-center">
					<button class="btn btn-primary" type="submit">Guardar responsabilidades</button>
				</div>
			</div>
		@endif

		</form>
		@endif

<?php $meses = array(0,'Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'); ?>

		<hr class="divisor">
		<div class="row">
			<h3 class="bsc_title">Retroalimentación Notas</h3>
			<div class="col-md-12">
				<h4 class="bsc_title">Notas</h4>
				<textarea class="form-control notas_area"></textarea>
				<div class="text-right">
					<button class="btn btn-primary agregar_nota publica">
						<i class="fas fa-plus-circle"></i> Agregar nota
					</button>
				</div>
				<div style="overflow: auto; max-height: 400px">
					<table class="table table-striped table-bordered table-hover">
						<thead style="background-color: #222B64; color:white">
							<tr>
								<th style="text-align: left">Fecha</th>
								<th style="text-align: left">Nombre</th>
								<th style="text-align: left">Nota</th>
							</tr>
						</thead>
						<tbody>

			<?php foreach ($notas as $key => $value){ ?>

							<tr>
								<td class="text-left">{{substr($value->created_at, 8, 2) * 1}} / {{$meses[substr($value->created_at, 5, 2) * 1]}} / {{substr($value->created_at, 0, 4)}}</td>
								<td class="text-left">{{$value->nombre}} {{$value->paterno}} {{$value->materno}}</td>
								<td class="text-left">{{$value->mensaje}}</td>
							</tr>
			<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
		<form action="/evaluacion-resultados/responsabilidades-y-compromisos" method="post" class="change_id_plan">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id_plan" class="id_plan">
			<input type="submit" style="display: none">
		</form>
	@else

		<h2 class="bsc_title">NO HAY PLANES ABIERTOS</h2>
	@endif

	</div>
</div>
<table style="display: none">
	<tr class="responsabilidades">
		<td class="text-center">
			<select class="text-center form-control size3" name="id_objetivo_corporativo[objetive_number]">

<?php $current_type = 0;
			$counter = 0;

			foreach ($objetivos_corporativos as $key => $value){

				if ($current_type != $value->id_tipo){

					$current_type = $value->id_tipo;
					$counter = 1;
				} ?>
						
				<option value="{{$value->id}}">{{$value->letter}}{{$counter}}</option>

<?php $counter++;
			} ?>

			</select>
		</td>
		<td class="text-center">
			<textarea class="form-control size2" name="description[objetive_number]" required></textarea>
		</td>
		<td class="text-center">
			<textarea class="form-control size2" name="formula[objetive_number]" required></textarea>
		</td>
		<td class="text-center">
			<input type="text" class="form-control size2 responsable" placeholder="Escribe el nombre del responsable" style="width: 260px">
			<button type="button" class="btn btn-primary add_responsable" style="display: none" data-index>
				<i class="fas fa-plus-circle"></i> Agregar
			</button>
			<div style="clear: both"></div>
		</td>
		<td class="text-center">
			<input type="text" class="form-control size2 capturista" placeholder="Escribe el nombre del responsable de captura" style="width: 260px">
			<button type="button" class="btn btn-primary add_capturista" style="display: none" data-index>
				<i class="fas fa-plus-circle"></i> Agregar
			</button>
			<div style="clear: both"></div>
		</td>
		<td class="text-center">
			<select class="text-center form-control size" name="tipo_indicador[objetive_number]">
				<option value="De Resultado">De Resultado</option>
				<option value="De Tendencia">De Tendencia</option>
			</select>
		</td>
		<td class="text-center">
			<select class="text-center form-control size" name="frecuencia[objetive_number]">
				<option name="Mensual">Mensual</option>
				<option name="Bimestral">Bimestral</option>
				<option name="Trimestral">Trimestral</option>
				<option name="Anual">Anual</option>
			</select>
		</td>
		<td class="text-center">
			<select class="text-center form-control size" name="frecuencia_captura[objetive_number]">
				<option name="Mensual">Mensual</option>
				<option name="Bimestral">Bimestral</option>
				<option name="Trimestral">Trimestral</option>
				<option name="Anual">Anual</option>
			</select>
		</td>
		<td class="text-center">
			<textarea class="form-control size" name="fuente[objetive_number]" required></textarea>
		</td>
		<td class="text-center">
			<div class="row">

<?php if (count($users_groups) > 0){ ?>

				<div class="col-md-12">
					<input type="text" class="form-control size2 grupos_corresponsables" placeholder="Escribe el nombre del grupo">
					<button type="button" class="btn btn-primary add_grupo" style="display: none" data-index>Agregar</button>
				</div>
<?php } ?>

				<div class="col-md-12">
					<input type="text" class="form-control size2 corresponsables" placeholder="Escribe el nombre del usuario">
					<button type="button" class="btn btn-primary add" style="display: none" data-index>Agregar</button>
				</div>
			</div>
		</td>
		<td class="text-center">
			<select class="text-center form-control size2" name="id_status[objetive_number]">
				<option value="1">a) Indicador disponible y utilizado.</option>
				<option value="2">b) Indicador disponible pero difiere su fórmula de cálculo</option>
				<option value="3">c) Indicador nuevo e información disponible</option>
				<option value="4">d) Indicador nuevo e información no disponible</option>
			</select>
		</td>
		<td class="text-center">
			<input type="text" class="form-control size text-right" name="meta[objetive_number]" required style="width: 90px">
		</td>
		<td class="text-center">
			<select class="text-center form-control size3 unidad" name="unidad[objetive_number]" style="width: 90px">
				<option value="%">%</option>
				<option value="#">$</option>
				<option value="9">0</option>
				<option value="Si/No">Si / No</option>
			</select>
		</td>
		<td class="text-center">
			<div style="width: 170px">
				<input type="number" step=".01" class="form-control size3" name="valor_rojo[objetive_number]" required style="display: inline-block">
				<input type="number" step=".01" class="form-control size3" name="valor_verde[objetive_number]" required style="display: inline-block">
			</div>
		</td>
		<td class="text-center">
			<button class="btn btn-primary remove_responsability" type="button">X</button>
		</td>
	</tr>
</table>
@endsection

@section('scripts')
<script>

	var user = '<?php echo auth()->user()->first_name . ' ' . auth()->user()->last_name?>';

	var id_plan = <?php echo $id_plan?>;

	// Lista de usuarios para el autocomplete
	var autocomplete_users = <?php echo json_encode($autocomplete_users)?>;
	var users_groups = <?php echo json_encode($users_groups)?>;
	
	var meses = [];
	var num_responsabilidades = <?php echo $index?>;
	var nombre = '';
	var id = 0;
		
	$(document).ready(function(){

		$.ajaxSetup({
    	headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    	}
		});

    meses[0] = 'Ene';
    meses[1] = 'Feb';
    meses[2] = 'Mar';
    meses[3] = 'Abr';
    meses[4] = 'May';
    meses[5] = 'Jun';
    meses[6] = 'Jul';
    meses[7] = 'Ago';
    meses[8] = 'Sep';
   	meses[9] = 'Oct';
    meses[10] = 'Nov';
    meses[11] = 'Dic';

    // Autocomplete
    $('.responsable').autocomplete({
    	lookup: autocomplete_users,
    	onSelect: function (suggestion){
        
        nombre = suggestion.value;
    		id = suggestion.data;
    		$(this).next().show();
    	}
		});

		$('.capturista').autocomplete({
    	lookup: autocomplete_users,
    	onSelect: function (suggestion){
        
        nombre = suggestion.value;
    		id = suggestion.data;
    		$(this).next().show();
    	}
		});

    $('.corresponsables').autocomplete({
    	lookup: autocomplete_users,
    	onSelect: function (suggestion){
        
        nombre = suggestion.value;
    		id = suggestion.data;
    		$(this).next().show();
    	}
		});

		$('.grupos_corresponsables').autocomplete({
    	lookup: users_groups,
    	onSelect: function (suggestion){
        
        nombre = suggestion.value;
    		id = suggestion.data;
    		$(this).next().show();
    	}
		});

		$('a.add_responsability').click(function(){

			var objetivo = $('table tr.responsabilidades').html();
			//var index = $('table.tabla_compromisos tbody tr.responsabilidad').length;
			//objetivo = objetivo.replace('grupos_corresponsables[]', 'grupos_corresponsables[' + index + '][]');
			//objetivo = objetivo.replace('corresponsables[]', 'corresponsables[' + index + '][]');
			num_responsabilidades++;
			objetivo = objetivo.replace(/objetive_number/g, num_responsabilidades);
			objetivo = objetivo.replace(/data-index=""/g, 'data-index="' + num_responsabilidades + '"');
			$('table.tabla_compromisos').append('<tr class="responsabilidad">' + objetivo + '</tr>');

			// Autocomplete
			$('.responsable').autocomplete({
    		lookup: autocomplete_users,
    		onSelect: function (suggestion){
        
        	nombre = suggestion.value;
    			id = suggestion.data;
    			$(this).next().show();
    		}
			});

			$('.capturista').autocomplete({
    		lookup: autocomplete_users,
    		onSelect: function (suggestion){
        
        	nombre = suggestion.value;
    			id = suggestion.data;
    			$(this).next().show();
    		}
			});

    	$('.corresponsables').autocomplete({
    		lookup: autocomplete_users,
    		onSelect: function (suggestion){

    			nombre = suggestion.value;
    			id = suggestion.data;
    			$(this).next().show();
    		}
			});

			$('.grupos_corresponsables').autocomplete({
    		lookup: users_groups,
    		onSelect: function (suggestion){

        	nombre = suggestion.value;
    			id = suggestion.data;
    			$(this).next().show();
    		}
			});

			return false;
		});

		// Quita un usuario o un grupo
		$('body').on('click', 'a.remove_corresponsable', function(){

			$(this).parent().parent().remove();
		});

		// Quita un responsable o capturista
		$('body').on('click', 'a.remove_user', function(){

			$(this).parent().parent().html('');
		});

		// Agrega un nuevo corresponsable a una responsabilidad
		$('body').on('click', 'button.add', function(){

			var num_objetivo = $(this).attr('data-index');
			$(this).prev().val('');
			$(this).parent().append('<div style="clear: both"><input type="hidden" value="' + id + '" name="corresponsables[' + num_objetivo + '][]"><div style="float: left">' + nombre + '</div><div style="float: right; height: 20px"><a href="javascript:;" class="remove_corresponsable">X</a></div></div>');
			$(this).hide();
		});

		// Agrega un nuevo grupo corresponsable a una responsabilidad
		$('body').on('click', 'button.add_grupo', function(){

			var num_objetivo = $(this).attr('data-index');
			$(this).prev().val('');
			$(this).parent().append('<div style="clear: both"><input type="hidden" value="' + id + '" name="grupos_corresponsables[' + num_objetivo + '][]"><div style="float: left">' + nombre + '</div><div style="float: right; height: 20px"><a href="javascript:;" class="remove_corresponsable">X</a></div></div>');
			$(this).hide();
		});

		// Agrega un nuevo responsable a una responsabilidad
		$('body').on('click', 'button.add_responsable', function(){

			var num_objetivo = $(this).attr('data-index');
			$(this).prev().val('');
			$(this).next().html('<input type="hidden" value="' + id + '" name="id_responsable[' + num_objetivo + ']" class="id_responsable"><div style="float: left">' + nombre + '</div><div style="float: right; height: 20px"><a href="javascript:;" class="remove_user">X</a></div>');
			$(this).hide();
		});

		// Agrega un nuevo capturista a una responsabilidad
		$('body').on('click', 'button.add_capturista', function(){

			var num_objetivo = $(this).attr('data-index');
			$(this).prev().val('');
			$(this).next().html('<input type="hidden" value="' + id + '" name="id_capturista[' + num_objetivo + ']" class="id_capturista"><div style="float: left">' + nombre + '</div><div style="float: right; height: 20px"><a href="javascript:;" class="remove_user">X</a></div>');
			$(this).hide();
		});

		$('button.agregar_nota').click(function(){

			var button = $(this);
			var nota = button.parent().prev().val();

			if (button.hasClass('publica')){

				$.ajax({
        	type:'POST',    
        	url: '/evaluacion-resultados/save-note',
        	data:{
          	mensaje: nota,
          	id_plan: id_plan
        	},
        	success: function(data){
             
          	var fecha = new Date();
          	button.parent().parent().find('table').prepend('<tr><td>' + fecha.getDate() + '/' + meses[fecha.getMonth()] + '/' + fecha.getFullYear() + '</td><td>' + user + '</td><td>' + nota + '</td></tr>');
          	button.parent().prev().val('');
        	}
      	});
      }

      else{

      	$.ajax({
        	type:'POST',    
        	url: '/evaluacion-resultados/save-note',
        	data:{
          	mensaje: nota,
          	id_plan: id_plan,
          	id_empleado: id_user
        	},
        	success: function(data){
             
          	var fecha = new Date();
          	button.parent().parent().find('table').prepend('<tr><td>' + fecha.getDate() + '/' + meses[fecha.getMonth()] + '/' + fecha.getFullYear() + '</td><td>' + user + '</td><td>' + nota + '</td></tr>');
          	button.parent().prev().val('');
        	}
      	});
      }
		});

		// Cambia el plan
		$('select.id_plan').change(function(){

			// Recarga la pagina con el nuevo plan
			$('form.change_id_plan .id_plan').val($(this).val());
			$('form.change_id_plan').submit();
		});

		//$('select.tipo, select.unidad').change(function(){
		$('body').on('change', 'select.unidad', function(){

			if ($(this).val() == 'Si/No'){

				var name = $(this).parent().prev().find('input').attr('name');
				$(this).parent().prev().html('<select class="form-control size metaSiNo" name="' + name + '" required><option value="Si">Si</option><option value="No">No</option></select>');
				name = $(this).parent().next().find('div input:nth-child(1)').attr('name');
				var name2 = $(this).parent().next().find('div input:nth-child(2)').attr('name');
				$(this).parent().next().find('div').html('<select class="form-control size3" name="' + name + '" required style="display: inline-block"><option value="No">No</option><option value="Si">Si</option></select><select class="form-control size3" name="' + name2 + '" required style="display: inline-block"><option value="Si">Si</option><option value="No">No</option></select>');
			}

			else{

				if ($(this).parent().prev().find('select').length > 0){

					var name = $(this).parent().prev().find('select').attr('name');
					$(this).parent().prev().html('<input type="text" class="form-control size" name="' + name + '" required>');
				}

				if ($(this).parent().next().find('div select').length > 0){

					var name = $(this).parent().next().find('div select:nth-child(1)').attr('name');
					var name2 = $(this).parent().next().find('div select:nth-child(2)').attr('name');
					$(this).parent().next().find('div').html('<input type="number" step=".01" class="form-control size3" name="' + name + '" required style="display: inline-block"><input type="number" step=".01" class="form-control size3" name="' + name2 + '" required style="display: inline-block">');
				}				
			}
		});

		// Remueve una responsabilidad o un objetivo aun no guardados
		$('body').on('click', 'button.remove_responsability', function(){

			$(this).parent().parent().remove();
		});

		// Borra una responsabilidad
		$('button.delete_responsability').click(function(){

			if (confirm('¿De verdad desea eliminar la responsabilidad?')){

				var id_responsabilidad = $(this).attr('data-id');

				$(this).parent().parent().remove();

				$.ajax({
        	type:'POST',    
        	url: '/evaluacion-resultados/delete-responsability',
        	data:{
          	id: id_responsabilidad,
						'_token': '{{ csrf_token() }}'
        	},
        	success: function(data){

        		alert('La responsabilidad fue borrada');
        	}
      	});
      }
		});

		// Clic en el boton Guardar Responsabilidades de la parte superior
		$('button.save_responsabilities').click(function(){

			$('form.save_responsabilities button[type="submit"]').click();
		});

		// Se quiere enviar el formulario con todas la responsabilidades
		$('form.save_responsabilities').submit(function(){

			var total_responsabilities = $('tr.responsabilidad').length;

			// Alguna responsabilidad no cuenta con un responsable
			if ($('input.id_responsable').length != total_responsabilities){

				alert('Todas las responsabilidades deben contar con un coordinador');
				return false;
			}

			// Alguna responsabilidad no cuenta con un capturista
			if ($('input.id_capturista').length != total_responsabilities){

				alert('Todas las responsabilidades deben contar con un capturista');
				return false;
			}
		});
	});
</script>
@endsection