@extends('layout')

@section('title', 'Informe Consolidado')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-responsive" src="img/evaluacion-resultados/informe-consolidado/ban.png" alt="">

	<!--</div>
		<div class="col-md-12">-->
			<!--<img class="img-responsive" width="100%" src="img/evaluacion-resultados/informe-consolidado/formato.png" alt="">-->

			</div>

</div>

@if (!empty($users))

<div class="row margin-top-20">
	<div class="col-md-6">
		<div>Periodo: <?php if (!empty($periodos)){ ?><select class="periodos">

			                                      <?php foreach ($periodos as $key => $periodo){ ?>
			                                      	
			                                      				<option value="<?php echo $periodo->id?>" <?php if (!empty(Session::get('periodo_informe')) && Session::get('periodo_informe') == $periodo->id){ Session::forget('periodo_informe');?>selected="selected"<?php } ?>><?php echo $periodo->descripcion?></option>
			                                      <?php } ?>
								 																 </select>
								 	<?php } else{ ?> No hay periodos cerrados <?php } ?>
		</div>
	</div>
	<div class="col-md-6">
		<div>Plan: <?php if (!empty($planes)){ ?><select class="planes">

			                                      <?php foreach ($planes as $key => $plan){ ?>
			                                      	
			                                      				<option value="<?php echo $plan->id?>" <?php if (!empty(Session::get('plan_informe')) && Session::get('plan_informe') == $plan->id){ Session::forget('plan_informe');?>selected="selected"<?php } ?>><?php echo $plan->nombre?></option>
			                                      <?php } ?>
								 																 </select>
								 	<?php } else{ ?> No hay planes cerrados <?php } ?>
		</div>
	</div>
</div>

<div class="row margin-top-20">
	<div class="col-md-12">
		<table width="100%" class="data-table usuarios_informe">
			<thead>
				<tr>
					<th class="cabeceras-tablas-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Persona</th>
					<th class="cabeceras-tablas-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Puesto</th>
					<th class="cabeceras-tablas-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Área</th>
					<th class="cabeceras-tablas-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Detalle</th>
				</tr>
			</thead>
			<tbody>

  @for ($i = 0;$i < count($users);$i++)

				<tr>
					<td class="text-left">{{$users[$i]->first_name}} {{$users[$i]->last_name}}</td>
					<td class="text-left">{{(!empty($users[$i]->puesto) ? $users[$i]->puesto : '')}}</td>
					<td class="text-left">{{(!empty($users[$i]->area) ? $users[$i]->area : '')}}</td>
					<td class="text-center">
						
				<?php if (!empty($periodos) && !empty($planes)){ ?> 

						<button type="button" class="btn btn-primary btn-globalgas-red informe">Ver Informe</button>
						<form action="/informe-consolidado/{{$users[$i]->id}}" method="post" class="ver_informe">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="id_periodo" class="id_periodo" value="<?php echo $periodos[0]->id?>">
							<input type="hidden" name="id_plan" class="id_plan" value="<?php echo $planes[0]->id?>">
							<button type="submit" class="btn btn-primary btn-globalgas-red" style="display: none">Ver Informe</button>
							<!--<a class="btn btn-primary btn-globalgas-red" href="/informe-consolidado/{{$users[$i]->id}}">Ver Informe</a>-->
						</form>
		<?php }

		      else{	?>

		      	<button type="button" class="btn btn-primary btn-globalgas-red">Ver Informe</button>
		<?php } ?>
					</td>
				</tr>
			@endfor

			</tbody>
		</table>
	</div>
</div>

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20 no_subordinados">No tiene subordinados</h4>
	</div>
</div>
@endif
@endsection

@section('scripts')
	<script>
		
		$(document).ready(function(){

			$('.data-table').DataTable({
   			'order': [[0,'asc']]
   		});
			$('select.planes').change(function(){

   			$('.id_plan').val($(this).val());
   		});

   		$('select.periodos').change(function(){

   			$('.id_periodo').val($(this).val());
   		});
			$('body').on('click', 'button.informe', function(){

   			console.log('test');

   			$('.id_plan').val($('select.planes').val());
   			$('.id_periodo').val($('select.periodos').val());
   			$(this).next().submit();
   		});
		});
	</script>
@endsection
