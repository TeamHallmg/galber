@extends('layouts.app')

@section('title', 'Autorizacion')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="img/banner_evaluacion_resultados.png" alt="">
		<h3 class="margin-top-20 titulos-evaluaciones" style="margin-top: 34px">Autorización</h3>
		<hr>

@if (!empty($planes))

<?php $meses_completos = array('0', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'); ?>

<div class="row">
	<div class="col-md-12">
    <!--<p class="margin-top-20 cabeceras-evaluaciones" style="padding: 5px 10px">Periodo Abierto: <?php //echo $plan->anio?></p>-->
		<div class="row margin-top-20">
			<div class="col-md-12 text-center">
				<form id="changePlanForm" action="autorizacion" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="id_empleado" value="{{$id_user}}">
					<div>Plan: 	<select name="id_plan" style="border-radius: 7px; margin-bottom: 4px" class="id_plan">

											@for ($i = 0;$i < count($planes);$i++)

											 	<option value="{{$planes[$i]->id}}" <?php if ($plan->id == $planes[$i]->id){ ?>selected="selected"<?php } ?>>{{$planes[$i]->nombre}}</option>
											@endfor

											</select>


					</div>
					<input type="submit" style="display: none">
				</form>
			</div>
		</div>
		<div class="row mt-4">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<table width="100%">
					<tr>
						<td width="70">Persona:</td>
						<td>
							<input type="text" value="{{$user->nombre}} {{$user->paterno}} {{$user->materno}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
						</td>
					</tr>
					<tr>
						<td>Puesto:</td>
						<td>
							<input type="text" value="{{(!empty($user->puesto) ? $user->puesto : '')}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
						</td>
					</tr>
					<tr>
						<td>Área:</td>
						<td>
							<input type="text" value="{{(!empty($user->departamento) ? $user->departamento: '')}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
						</td>
					</tr>
					<tr>
						<td>Jefe:</td>
						<td>
							<input type="text" value="{{(!empty($user->boss) ? $user->boss->nombre . ' ' . $user->boss->paterno . ' ' . $user->boss->materno : '')}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
						</td>
					</tr>
				</table>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
</div>
<hr style="border-color: #A5A7A8">

@if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin') || !empty($bono_plan))

<div class="row">
	<div class="col-md-12">
		<strong>Total Bono</strong> <input type="text" value="{{(!empty($bono_plan) ? $bono_plan->total_bono : '')}}" class="form-control total_bono size currency" style="display: inline-block; width: auto" readonly>
	</div>
</div>
@endif

<div class="row">
	<div class="col-md-12">
		<table width="100%" style="font-size: 13px" class="tabla_objetivos table table-bordered table-striped">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">#</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Periodo</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Objetivo</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Clave</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Unidad de medida</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Meta</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Peso</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Monto de Bono</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Frecuencia</th>
					<th class="cabeceras-evaluaciones">Co-responsables</th>
					<!--<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Tipo de Semáforo</th>-->
					<th class="cabeceras-evaluaciones" style="padding: 5px 50px; text-align: center; border: 1px solid white;">
						<p style="font-size: 13px">Rango de Semáforo</p>
						<div style="font-size: 13px">
							<span style="background-color: red; padding: 5px 10px">Rojo</span><span style="background-color: #FFDB58; color: #FFDB58; padding: 5px 10px">Am</span><span style="background-color: green; padding: 5px 10px">Verde</span>
						</div>
					</th>

			@for ($j = 0;$j < 3;$j++)

					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Nivel de logro</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Porcentaje de Bono</th>
			@endfor

				</tr>
			</thead>
			<tbody>

			<?php $total_peso = 0; ?>

      @for ($i = 0;$i < count($objetivos); $i++)

				<tr class="objetivos">
					
					<td class="text-center">{{$i + 1}}</td>
					<td class="text-center">{{$objetivos[$i]->periodo}}</td>
					<td class="text-center">
						<!--<input type="text" value="{{$objetivos[$i]->nombre}}" style="width: 200px; border-radius: 7px; border: 1px solid #A5A7A8; text-align: center" class="nombre" readonly>-->
						<textarea readonly style="overflow-x: hidden; resize: none" class="nombre form-control size2">{{$objetivos[$i]->nombre}}</textarea>
					</td>
					<td class="text-center">
						<select class="id_objetivo_corporativo form-control text-center size3" disabled>

	<?php $counter = 0;
				$current_type = 0; ?>

				@for ($j = 0;$j < count($objetivos_corporativos_sin_responsable);$j++)

					@if ($current_type != $objetivos_corporativos_sin_responsable[$j]->id_tipo)

			<?php $counter = 1;
						$current_type = $objetivos_corporativos_sin_responsable[$j]->id_tipo; ?>
					@endif

					@if ($objetivos_corporativos_sin_responsable[$j]->id == $objetivos[$i]->id_objetivo_corporativo)

							<option >{{$objetivos_corporativos_sin_responsable[$j]->letter}}{{$counter}}</option>
			<?php break; ?>
					@endif
						
		<?php $counter++; ?>
				@endfor

						</select>
					</td>
					<td class="text-center">
						<select class="tipo form-control size3" disabled>
							<option value="%">%</option>
							<option value="$" <?php if ($objetivos[$i]->tipo == '$'){ ?>selected="selected"<?php } ?>>$</option>
							<option value="#" <?php if ($objetivos[$i]->tipo == '9' || $objetivos[$i]->tipo == '#'){ ?>selected="selected"<?php } ?>>#</option>
							<option value="Si/No" <?php if ($objetivos[$i]->tipo == 'Si/No'){ ?>selected="selected"<?php } ?>>Si/No</option>
						</select>
					</td>
					<td class="text-center">
						<input type="text" class="objetivo form-control size text-center {{($objetivos[$i]->tipo != 'Si/No' ? ($objetivos[$i]->tipo == '$' ? 'currency' : ($objetivos[$i]->tipo == '#' ? 'numeric' : 'porcentaje')) : '')}}" value="{{$objetivos[$i]->objetivo}}" readonly>					  	
					</td>
					<td class="text-center">
						<input type="text" class="peso size3 text-center form-control numeric" value="{{$objetivos[$i]->peso}}" readonly>
					</td>
					<td class="text-center">
						<input type="text" value="${{number_format($objetivos[$i]->peso * str_replace(',', '',$bono_plan->total_bono) / 100, 1, '.', ',')}}" readonly class="form-control size">
					</td>
					<td class="text-center">
						<select class="frecuencia form-control size" disabled>
							<option value="Mensual">Mensual</option>
							<option value="Bimestral" <?php if ($objetivos[$i]->frecuencia == 'Bimestral'){ ?>selected="selected"<?php } ?>>Bimestral</option>
							<option value="Trimestral" <?php if ($objetivos[$i]->frecuencia == 'Trimestral'){ ?>selected="selected"<?php } ?>>Trimestral</option>
							<option value="Semestral" <?php if ($objetivos[$i]->frecuencia == 'Semestral'){ ?>selected="selected"<?php } ?>>Semestral</option>
							<option value="Anual" <?php if ($objetivos[$i]->frecuencia == 'Anual'){ ?>selected="selected"<?php } ?>>Anual</option>
						</select>
					</td>
					<td class="text-center">
						<div class="row">

<?php if (count($users_groups) > 0){ ?>

							<div class="col-md-12">

	<?php if (!empty($objetivos[$i]->grupos_corresponsables)){

					foreach ($users_groups as $key2 => $users_group){
						
						if (in_array($users_group->data, $objetivos[$i]->grupos_corresponsables)){ ?>

								<div style="clear: both">
									<div style="float: left" class="size">{{$users_group->value}}</div>
								</div>
			<?php }
					}
				} ?>

							</div>
<?php } ?>

							<div class="col-md-12">

<?php if (!empty($objetivos[$i]->corresponsables)){

				foreach ($usuarios as $key2 => $usuario){
						
					if (in_array($usuario->data, $objetivos[$i]->corresponsables)){ ?>

								<div style="clear: both">
									<div style="float: left" class="size">{{$usuario->value}}</div>
								</div>
		<?php }
				}
			} ?>

							</div>
						</div>
					</td>
					<td class="text-center">

						@if (empty($objetivos[$i]->valores))

					  <button style="cursor: default" class="btn btn-danger middle_size">{{($objetivos[$i]->tipo == '%' ? number_format(str_replace(',', '', $objetivos[$i]->valor_rojo), 2, '.', ',') . '%' : ($objetivos[$i]->tipo == '$' ? '$' . number_format(str_replace(',', '', $objetivos[$i]->valor_rojo)) : ($objetivos[$i]->tipo == '#' ? number_format(str_replace(',', '', $objetivos[$i]->valor_rojo)) : $objetivos[$i]->valor_rojo)))}}</button>
						<button style="cursor: default" class="btn btn-success middle_size">{{($objetivos[$i]->tipo == '%' ? number_format(str_replace(',', '', $objetivos[$i]->valor_verde), 2, '.', ',') . '%' : ($objetivos[$i]->tipo == '$' ? '$' . number_format(str_replace(',', '', $objetivos[$i]->valor_verde)) : ($objetivos[$i]->tipo == '#' ? number_format(str_replace(',', '', $objetivos[$i]->valor_verde)) : $objetivos[$i]->valor_verde)))}}</button>
					  @else

					  <div style="width: 100%; overflow: hidden">
					  	<div style="position: relative; width: {{(count($objetivos[$i]->valores) + 1) * 100}}%" class="slide">
							
					<?php $espacio = 100 / (count($objetivos[$i]->valores) + 1); ?>

					  	@for ($j = 0;$j < count($objetivos[$i]->valores);$j++)

					  		<div style="width: {{$espacio}}%; float: left">
									<div class="text-center">{{($objetivos[$i]->frecuencia == 'Mensual' ? $meses_completos[$objetivos[$i]->valores[$j]->periodo] : ($objetivos[$i]->frecuencia == 'Trimestral' ? $meses_completos[($objetivos[$i]->valores[$j]->periodo - 1) * 3 + 1] : ($objetivos[$i]->frecuencia == 'Semestral' ? 'Julio' : '')))}}</div>
									<input type="text" style="width: 40%; border-radius: 7px; border: 1px solid #A5A7A8; background-color: red; color: white" value="{{($objetivos[$i]->tipo == '$' ? '$' . $objetivos[$i]->valores[$j]->valor_rojo : ($objetivos[$i]->tipo == '%' ? $objetivos[$i]->valores[$j]->valor_rojo . '%' : $objetivos[$i]->valores[$j]->valor_rojo))}}" class="text-center" readonly> <input type="text" style="width: 10%; border-radius: 7px; border: 1px solid #A5A7A8; background-color: #FFDB58" readonly> <input type="text" style="width: 40%; border-radius: 7px; border: 1px solid #A5A7A8; background-color: green; color: white" value="{{($objetivos[$i]->tipo == '$' ? '$' . $objetivos[$i]->valores[$j]->valor_verde : ($objetivos[$i]->tipo == '%' ? $objetivos[$i]->valores[$j]->valor_verde . '%' : $objetivos[$i]->valores[$j]->valor_verde))}}" class="text-center" readonly>
								</div>
					  	@endfor
								<div style="width: {{$espacio}}%; float: left">
									<div class="text-center">{{($objetivos[$i]->frecuencia == 'Mensual' ? $meses_completos[$objetivos[$i]->periodo] : ($objetivos[$i]->frecuencia == 'Trimestral' ? $meses_completos[($objetivos[$i]->periodo - 1) * 3 + 1] : ($objetivos[$i]->frecuencia == 'Semestral' ? 'Enero' : '')))}}</div>
									<input type="text" style="width: 40%; border-radius: 7px; border: 1px solid #A5A7A8; background-color: red; color: white" value="{{($objetivos[$i]->tipo == '$' ? '$' . $objetivos[$i]->valor_rojo : ($objetivos[$i]->tipo == '%' ? $objetivos[$i]->valor_rojo . '%' : $objetivos[$i]->valor_rojo))}}" class="text-center" readonly> <input type="text" style="width: 10%; border-radius: 7px; border: 1px solid #A5A7A8; background-color: #FFDB58" readonly> <input type="text" style="width: 40%; border-radius: 7px; border: 1px solid #A5A7A8; background-color: green; color: white" value="{{($objetivos[$i]->tipo == '$' ? '$' . $objetivos[$i]->valor_verde : ($objetivos[$i]->tipo == '%' ? $objetivos[$i]->valor_verde . '%' : $objetivos[$i]->valor_verde))}}" class="text-center" readonly>
								</div>
					  	</div>
					  </div>
					  <div class="text-center" data-valores="{{count($objetivos[$i]->valores) + 1}}" data-step="0">
					  	<span class="glyphicon glyphicon-chevron-left" style="cursor: pointer"></span> <span class="glyphicon glyphicon-chevron-right" style="cursor: pointer"></span>
					  </div>
					  @endif
					</td>
						
				@for ($j = 0;$j < 3;$j++)

					<td class="text-center">
						<input type="text" class="form-control size3 porcentaje" value="{{(!empty($objetivos[$i]->bonos[$j]) ? $objetivos[$i]->bonos[$j]->nivel_logro : '')}}" readonly>
					</td>
					<td class="text-center">
						<input type="text" class="form-control size3 porcentaje" value="{{(!empty($objetivos[$i]->bonos[$j]) ? $objetivos[$i]->bonos[$j]->porcentaje_bono : '')}}" readonly>
					</td>
				@endfor

				</tr>
				
				<?php $total_peso += $objetivos[$i]->peso?>
			@endfor

				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="text-center cabeceras-evaluaciones">Total:</td>
					<td class="text-center peso_total"><?php echo $total_peso?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>

			@if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin'))

				@for ($j = 0;$j < 6;$j++)

					<td></td>
				@endfor
			@endif

				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-12 text-center">
		
@if (!empty($status_plan) && $status_plan[0]->status == 2)
	
  @if ((!empty($user->jefe) && $user->jefe == $user_logged->idempleado) || $alimentar_propuestas == 2)
		<div class="margin-top-20">
			<form action="autorizar" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_plan" value="{{$plan->id}}">
				<input type="hidden" name="id_empleado" value="{{$id_user}}">
				<input type="hidden" name="periodo" value="{{$status_plan[0]->periodo}}">
				<input type="hidden" name="status" value="3">
				<button class="btn btn-primary" type="submit">Aprobar</button>
			</form>
		</div>
		<div class="margin-top-20">
			<form action="autorizar" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_plan" value="{{$plan->id}}">
				<input type="hidden" name="id_empleado" value="{{$id_user}}">
				<input type="hidden" name="periodo" value="{{$status_plan[0]->periodo}}">
				<input type="hidden" name="status" value="1">
				<button class="btn btn-primary" type="submit">Rechazar</button>
			</form>
		</div>
	@endif
@else
	@if (!empty($status_plan) && $status_plan[0]->status > 2)
	  <div class="margin-top-20">
	  	<p>Fecha de Autorización</p>
			<p>{{substr($status_plan[0]->fecha_autorizado, 8)}} de {{$meses_completos[substr($status_plan[0]->fecha_autorizado, 5, 2) * 1]}} del {{substr($status_plan[0]->fecha_autorizado, 0, 4)}}</p>
		</div>
	@endif
@endif
	</div>
</div>
<hr style="border-color: #A5A7A8">
<div class="row margin-top-20">
	<div class="col-md-12">
		<div style="max-height: 200px; overflow-y: auto">
			<h3 style="margin-top: 0">Observaciones</h3>
			<table width="100%" style="font-size: 13px" border="1" class="notas table table-bordered table hover table-striped">
				<thead style="background-color: #222B64; color:white;">
					<tr>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Fecha</th>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Persona</th>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Nota</th>
					</tr>
				</thead>
				<tbody>

      	<?php $meses = array('0', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'); ?>

      	@for ($i = 0;$i < count($notas); $i++) 

					<tr>
						<td class="text-center" style="padding: 5px; border: 0">{{substr($notas[$i]->fecha, 8) * 1}}/{{$meses[substr($notas[$i]->fecha, 5, 2) * 1]}}/{{substr($notas[$i]->fecha, 0, 4)}}</td>
						<td class="text-center" style="padding: 5px; border: 0">{{$notas[$i]->nombre}} {{$notas[$i]->paterno}} {{$notas[$i]->materno}}</td>
						<td class="text-center" style="padding: 5px; border: 0">{{$notas[$i]->mensaje}}</td>
					</tr>
				@endfor

				</tbody>
			</table>
		</div>
		<h4 class="titulos-evaluaciones">Nota</h4>
		<div>
			<textarea style="width: 100%; height: 100px; border-radius: 7px" class="nota"></textarea>
		</div>
	@if ((!empty($user->jefe) && $user->jefe == $user_logged->idempleado) || $alimentar_propuestas == 2)
		<div class="text-right margin-top-10">
			<button class="btn btn-primary agregar_nota">Agregar nota</button>
		</div>
	@endif
	</div>
	
</div>
@else
<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="titulos-evaluaciones">No hay planes abiertos</h4>
	</div>
</div>
@endif
</div>
</div>
@endsection

@section('scripts')
	<script>

	  var id_plan = <?php echo (!empty($plan->id) ? $plan->id : 0)?>;
	  var id_empleado = <?php echo (!empty($id_user) ? $id_user : 0)?>;
		
		$(document).ready(function(){

			$.ajaxSetup({
    		headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
			});
			
			$('.tabla_objetivos').DataTable({
				    scrollX: true,
    				scrollCollapse: true,
    				paging: false,
    				searching: false,
    				ordering: false,
    				info: false
				});

			// Clic en la flecha izquierda debajo de los valores rojo y verde de un objetivo
			$('span.glyphicon-chevron-left').click(function(){
				
				var total = $(this).parent().attr('data-valores');
				var step = $(this).parent().attr('data-step');
				
				if (step == 0){
					
					step = (total - 1) * -1;
				}
				
				else{
					
					step--;
					step = step * -1;
				}

				$(this).parent().attr('data-step', Math.abs(step));
				$(this).parent().prev().find('.slide').animate({"left":(step * 100) + '%'}, "slow");
			});

			// Clic en la flecha derecha debajo de los valores rojo y verde de un objetivo
			$('span.glyphicon-chevron-right').click(function(){
				
				var total = $(this).parent().attr('data-valores');
				var step = $(this).parent().attr('data-step');
				
				if (step == (total - 1)){
					
					step = 0;
				}
				
				else{
					
					step++;
					step = step * -1;
				}

				$(this).parent().attr('data-step', Math.abs(step));
				$(this).parent().prev().find('.slide').animate({"left":(step * 100) + '%'}, "slow");
			});

			$('select.id_plan').change(function(){

				$('form#changePlanForm').submit();
			});

      $('button.agregar_nota').click(function(){

				var nota = $('textarea.nota').val();

				$.ajax({
        	type:'POST',    
        	url: 'agregar-nota',
          data:{
          	mensaje: nota,
          	id_empleado: id_empleado,
          	id_plan: id_plan
          },
          success: function(data){
             
            var meses = [];
            meses[0] = 'Ene';
            meses[1] = 'Feb';
            meses[2] = 'Mar';
            meses[3] = 'Abr';
            meses[4] = 'May';
            meses[5] = 'Jun';
            meses[6] = 'Jul';
            meses[7] = 'Ago';
            meses[8] = 'Sep';
            meses[9] = 'Oct';
            meses[10] = 'Nov';
            meses[11] = 'Dic';
            var fecha = new Date();
            $('table.notas').prepend('<tr><td class="text-center" style="padding: 5px; border: 0">' + fecha.getDate() + '/' + meses[fecha.getMonth()] + '/' + fecha.getFullYear() + '</td><td class="text-center" style="padding: 5px; border: 0">' + data + '</td><td class="text-center" style="padding: 5px; border: 0">' + nota + '</td></tr>');
            $('textarea.nota').val('');
          }
        });
			});

			$('input.porcentaje').inputmask("numeric", {
    		radixPoint: ".",
    		groupSeparator: ",",
    		digits: 2,
    		autoGroup: true,
    		suffix: '%', //No Space, this will truncate the first character
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});

			$('input.currency').inputmask("numeric", {
    		radixPoint: ".",
    		groupSeparator: ",",
    		digits: 2,
    		autoGroup: true,
    		prefix: '$', //No Space, this will truncate the first character
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});

			$('input.numeric').inputmask("numeric", {
    		groupSeparator: ",",
    		autoGroup: true,
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});
		});
	</script>
@endsection
