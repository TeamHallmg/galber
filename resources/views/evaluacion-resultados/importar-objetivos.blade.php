@extends('layouts.app')

@section('title', 'Importar Objetivos')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/banner_evaluacion_resultados.png') }}" alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Importar Objetivos</h3>
		<hr>


		{!! Form::open(['url' => '/importar-objetivos', 'method' => 'POST', 'files'=>true]) !!}
    <div class="row">
      <div class="col-md-4 mx-auto">
        {!! Form::label('file', 'Archivo a Importar') !!}
        {!! Form::file('file', ['class' => 'filestyle sr-only','data-buttonText'=>'Explorar', 'data-badge'=>"false", 'style'=>"height:0", 'required'=>"required", 'data-buttonName'=>"btn-custom-".config('config.theme')]) !!}
				<div class="bootstrap-filestyle input-group mx-auto">
					<input type="text" class="form-control" placeholder="XLS o XLSX" disabled="">
					<span class="group-span-filestyle input-group-btn" tabindex="0" style="background-color: #0F62AC; font-weight: bold; color: white">
						<label for="file" class="btn btn-custom-herbalife mx-auto my-auto" style="font-weight: bold">
							<span class="fas fa-folder-open"></span> <span class="buttonText">Explorar</span>
						</label>
					</span>
				</div>
      </div>
      </div>
      <div class="row mt-5">
      	<div class="col-md-4 mx-auto" style="width: 200px;">
      		{{ Form::button('<i class="fas fa-cogs"></i> Procesar', array('type' => 'submit', 'class' => 'btn btn-block btn-primary',  'onclick' => 'return confirm("¿Desea eliminar?")')) }}
       	</div>
      </div>
    
    {!! Form::close() !!}

	</div>
</div>
@endsection
@section('scripts')

<script type="text/javascript">

    $(document).ready(function(){
		
		$('.filestyle').change(function(){
			
			$('.bootstrap-filestyle input[type="text"]').val($(this).val().replace(/C:\\fakepath\\/i, ''));
		});
	});
</script>
@endsection