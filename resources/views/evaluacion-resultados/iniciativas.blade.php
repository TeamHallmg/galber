@extends('layouts.app')

@section('title', 'Iniciativas')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_evaluacion_resultados.png" alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Iniciativas</h3>
		<hr>

	@if (!empty($planes))

		<div class="margin-top-20 text-center">
			Plan 
			<select class="form-group planes" style="border-radius: 7px; margin-bottom: 4px">

		@foreach ($planes as $key => $plan)
		
			<option value="{{$plan->id}}" <?php if ($plan->id == $id_plan){ ?>selected="selected"<?php } ?>>{{$plan->nombre}}</option>
		@endforeach

			</select>
		</div>
		<div style="overflow: auto; max-height: 600px">
			<table class="tabla_compromisos table table-striped table-bordered table-hover" style="width: 1500px">
				<thead style="background-color: #222B64; color:white">
					<tr>
						<th>Clave</th>
						<th style="text-align: left">Objetivo</th>
						<th style="text-align: left">Meta</th>
						<th style="text-align: left">Iniciativa</th>
						<th style="text-align: left">Corresponsables</th>
						<th>Fecha de inicio</th>
						<th>Fecha de Término</th>
						<th>Estado</th>

		@if ($canEdit)
		
						<th>Acciones</th>
		@endif

					</tr>
				</thead>
				<tbody>
			
<?php foreach ($iniciatives as $key => $value){

				$found_objetive = false;
				$color = '';

				if (($value->estado == 'Sin Iniciar' && $value->fecha_termino <= date('Y-m-d')) || ($value->estado == 'En proceso' && $value->fecha_termino <= date('Y-m-d'))){

					$color = '#d9534f';
				}

				else{

					if (($value->estado == 'Sin Iniciar' && $value->fecha_inicio <= date('Y-m-d')) || ($value->estado == 'En proceso' && $value->fecha_inicio <= date('Y-m-d'))){

						$color = '#f0ad4e';
					}

					else{

						if ($value->estado == 'Concluido' || $value->fecha_inicio > date('Y-m-d')){

							$color = '#5cb85c';
						}

						else{

							if ($value->estado == 'Cancelado'){

								$color = 'gray';
							}
						}
					}
				} ?>

					<tr class="iniciativa">
						<td class="text-center">

	<?php $counter = 0;
				$current_type = 0; ?>

				@foreach ($objetivos_corporativos as $key2 => $value2)
	
					@if ($value2->id_tipo != $current_type)

			<?php $counter = 1;
						$current_type = $value2->id_tipo; ?>
					@endif

					@if ($value2->id == $value->id_objetivo_corporativo)

						{{$value2->letter}}{{$counter}}
			<?php break; ?>
					@endif

		<?php $counter++; ?>
				@endforeach

						</td>
						<td class="text-left">

				@if ($canEdit)

							<select class="form-control id_responsabilidad" style="width: 250px">

					@foreach ($responsabilities as $key2 => $value2)
			
								<option value="{{$value2->id}}" <?php if ($value->id_responsabilidad == $value2->id){ ?>selected="selected"<?php } ?>>{{$value2->description}}</option>
					@endforeach

							</select>
				@else

							{{$value->descripcion_responsabilidad}}
				@endif

						</td>
						<td class="text-left">
							<div style="width: 200px">{{$value->formula}}</div>
						</td>
						<td class="text-left">

				@if ($canEdit)

							<textarea class="form-control description" style="width: 250px">{{$value->description}}</textarea>
				@else

							{{$value->description}}
				@endif

						</td>
						<td class="text-left" style="width: 250px">
							<div class="row">

	<?php if (count($users_groups) > 0){ ?>

								<div class="col-md-12 text-center">

					@if ($canEdit)

									<input type="text" class="form-control grupos_corresponsables size2" placeholder="Escribe el nombre del grupo">
									<button type="button" class="btn btn-primary add_grupo" style="display: none">
										<i class="fas fa-plus-circle"></i> Agregar
									</button>
					@endif

		<?php if (!empty($value->grupos_responsables)){

						foreach ($users_groups as $key2 => $users_group){
						
							if (in_array($users_group->data, $value->grupos_responsables)){ ?>

									<div style="clear: both">
										<input type="hidden" class="grupo_corresponsable" value="{{$users_group->data}}">
										<div style="float: left">{{$users_group->value}}</div>

								@if ($canEdit)

										<div style="float: right; height: 20px">
											<a href="javascript:;" class="remove_corresponsable">X</a>
										</div>
								@endif

									</div>
				<?php }
						}
					} ?>

								</div>
	<?php } ?>
								
								<div class="col-md-12 text-center">

				@if ($canEdit)

									<input type="text" class="form-control corresponsables size2" placeholder="Escribe el nombre del usuario">
									<button type="button" class="btn btn-primary add" style="display: none">
										<i class="fas fa-plus-circle"></i> Agregar
									</button>
				@endif

	<?php if (!empty($value->responsables)){

					foreach ($users as $key2 => $user){
						
						if (in_array($user->data, $value->responsables)){ ?>

									<div style="clear: both">
										<input type="hidden" class="corresponsable" value="{{$user->data}}">
										<div style="float: left">{{$user->value}}</div>

							@if ($canEdit)

										<div style="float: right; height: 20px">
											<a href="javascript:;" class="remove_corresponsable">X</a>
										</div>
							@endif

									</div>
			<?php }
					}
				} ?>

								</div>
							</div>
						</td>
						<td class="text-center">

				@if ($canEdit)

							<input type="date" class="form-control size start_date" value="{{$value->fecha_inicio}}" style="width: 160px">
				@else

							{{substr($value->fecha_inicio, 8) . '-' . substr($value->fecha_inicio, 5, 3) . substr($value->fecha_inicio, 0, 4)}}
				@endif
						
						</td>
						<td class="text-center">

				@if ($canEdit)

							<input type="date" class="form-control size end_date" value="{{$value->fecha_termino}}" style="width: 160px">
				@else

							{{substr($value->fecha_termino, 8) . '-' . substr($value->fecha_termino, 5, 3) . substr($value->fecha_termino, 0, 4)}}
				@endif

						</td>
						<td class="text-center">
							<form method="post" action="/evaluacion-resultados/save-iniciative-state">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="id" value="{{$value->id}}">
								<select class="text-center form-control size estado_iniciativa" name="estado" style="background-color: {{$color}}; color: white">

				@if ($value->id_capturista == auth()->user()->id)

									<option name="Sin Iniciar">Sin Iniciar</option>
									<option name="En proceso" <?php if ($value->estado == 'En proceso'){ ?>selected="selected"<?php } ?>>En proceso</option>
									<option name="Concluido" <?php if ($value->estado == 'Concluido'){ ?>selected="selected"<?php } ?>>Concluido</option>
									<option name="Cancelado" <?php if ($value->estado == 'Cancelado'){ ?>selected="selected"<?php } ?>>Cancelado</option>
				@else

									<option>{{$value->estado}}</option>
				@endif

								</select>
								<input type="submit" style="display: none">
							</form>
							</div>
						</td>

				@if ($canEdit)

						<td class="text-center">
							<div class="size">
								<button type="button" class="btn btn-primary save_iniciative">Guardar</button>							
								<button type="button" class="btn btn-primary delete_iniciative" data-id="{{$value->id}}">X</button>
							</div>
						</td>
				@endif

					</tr>
<?php	} ?>

				</tbody>
			</table>
		</div>

			@if ($canEdit && !empty($responsabilities))

		<div class="margin-top-20">
			<button class="btn btn-primary add_iniciative">
				<i class="fas fa-plus-circle"></i> Agregar actividad
			</button>
		</div>
			@endif

		<hr class="divisor">
		<div class="row">
			<div class="col-md-12">
				<h3 class="bsc_title">Retroalimentación Notas</h3>
				<div style="overflow: auto; max-height: 400px">
					<table class="table notas table-striped table-bordered table-hover">
						<thead style="background-color: #222B64; color:white">
							<tr>
								<th style="text-align: left">Fecha</th>
								<th style="text-align: left">Nombre</th>
								<th style="text-align: left">Nota</th>
							</tr>
						</thead>
						<tbody>

<?php $meses = array(0,'Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic');

			foreach ($notas as $key => $value){ ?>

							<tr>
								<td>{{substr($value->created_at, 8, 2) * 1}} / {{$meses[substr($value->created_at, 5, 2) * 1]}} / {{substr($value->created_at, 0, 4)}}</td>
								<td>{{$value->nombre}} {{$value->paterno}} {{$value->materno}}</td>
								<td>{{$value->mensaje}}</td>
							</tr>
<?php } ?>

						</tbody>
					</table>
				</div>
				<h3 class="bsc_title">Nota</h3>
				<textarea class="form-control notas_area"></textarea>
				<div class="text-right">
					<button class="btn btn-primary agregar_nota publica">
						<i class="fas fa-plus-circle"></i> Agregar nota
					</button>
				</div>
			</div>
		</div>
		<form action="/evaluacion-resultados/iniciativas" method="post" class="change_id_plan">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id_plan" class="id_plan">
			<input type="submit" style="display: none">
		</form>
		<form action="/evaluacion-resultados/save-iniciative" method="post" class="guardar_iniciativa">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="submit" style="display: none">
		</form>
	@else

		<h4 class="titulos-evaluaciones mt-5 font-weight-bold">NO HAY PLANES ABIERTOS</h4>
	@endif

	</div>
</div>
<table style="display: none">
	<tr class="iniciativas">
		<td class="text-center">

	@if (!empty($responsabilities[0]))

<?php $counter = 0;
			$current_type = 0; ?>

		@foreach ($objetivos_corporativos as $key => $value)
	
			@if ($value->id_tipo != $current_type)

	<?php $counter = 1;
				$current_type = $value->id_tipo; ?>
			@endif

			@if ($value->id == $responsabilities[0]->id_objetivo_corporativo)

				{{$value->letter}}{{$counter}}
	<?php break; ?>
			@endif

<?php $counter++; ?>
		@endforeach
	@endif

		</td>
		<td class="text-left">
			<select class="form-control id_responsabilidad" style="width: 250px">

	@foreach ($responsabilities as $key => $value)
			
				<option value="{{$value->id}}">{{$value->description}}</option>
	@endforeach

			</select>
		</td>
		<td class="text-left">
			<div style="width: 200px">{{(!empty($responsabilities[0]) ? $responsabilities[0]->formula : '')}}</div>
		</td>
		<td class="text-left">
			<textarea class="form-control description" style="width: 250px"></textarea>
		</td>
		<td class="text-center" style="width: 250px">
			<div class="row">

<?php if (count($users_groups) > 0){ ?>

				<div class="col-md-12 text-center">
					<input type="text" class="form-control size2 grupos_corresponsables" placeholder="Escribe el nombre del grupo">
					<button type="button" class="btn btn-primary add_grupo" style="display: none">
						<i class="fas fa-plus-circle"></i> Agregar
					</button>
				</div>
<?php } ?>

				<div class="col-md-12 text-center">
					<input type="text" class="form-control size2 corresponsables" placeholder="Escribe el nombre del usuario">
					<button type="button" class="btn btn-primary add" style="display: none">
						<i class="fas fa-plus-circle"></i> Agregar
					</button>
				</div>
			</div>
		</td>
		<td class="text-center">
			<input type="date" class="form-control size start_date" style="width: 160px">
		</td>
		<td class="text-center">
			<input type="date" class="form-control size end_date" style="width: 160px">
		</td>
		<td class="text-center">
			<select class="text-center form-control size estado" name="estado" style="background-color: #f0ad4e; color: white">
				<option name="Sin Iniciar">Sin Iniciar</option>
			</select>
		</td>
		<td class="text-center">
			<div class="size">
				<button type="button" class="btn btn-primary save_iniciative">Guardar</button>
				<button type="button" class="btn btn-primary remove_iniciative">X</button>
			</div>
		</td>
	</tr>
</table>
@endsection

@section('scripts')
<script>

	var user = '<?php echo auth()->user()->name?>';

	var id_plan = <?php echo $id_plan?>;
	var meses = [];
	var autocomplete_users = <?php echo json_encode($users)?>;
	var users_groups = <?php echo json_encode($users_groups)?>;
	var responsabilities = <?php echo json_encode($responsabilities)?>;
	var objetivos_corporativos = <?php echo json_encode($objetivos_corporativos)?>;
		
	$(document).ready(function(){

		$.ajaxSetup({
    	headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    	}
		});

    meses[0] = 'Ene';
    meses[1] = 'Feb';
    meses[2] = 'Mar';
    meses[3] = 'Abr';
    meses[4] = 'May';
    meses[5] = 'Jun';
    meses[6] = 'Jul';
    meses[7] = 'Ago';
    meses[8] = 'Sep';
   	meses[9] = 'Oct';
    meses[10] = 'Nov';
    meses[11] = 'Dic';

    // Autocomplete 
    $('.corresponsables').autocomplete({
    	lookup: autocomplete_users,
    	onSelect: function (suggestion){
        
        nombre = suggestion.value;
    		id = suggestion.data;
    		$(this).next().show();
    	}
		});

		$('.grupos_corresponsables').autocomplete({
    	lookup: users_groups,
    	onSelect: function (suggestion){
        
        nombre = suggestion.value;
    		id = suggestion.data;
    		$(this).next().show();
    	}
		});

		// Agrega una iniciativa
		$('button.add_iniciative').click(function(){

			var iniciativa = $('table tr.iniciativas').html();
			$(this).parent().prev().find('tbody').append('<tr class="iniciativa">' + iniciativa + '</tr>');

			// Autocomplete 
    	$('.corresponsables').autocomplete({
    		lookup: autocomplete_users,
    		onSelect: function (suggestion){
        
        	nombre = suggestion.value;
    			id = suggestion.data;
    			$(this).next().show();
    		}
			});

			$('.grupos_corresponsables').autocomplete({
    		lookup: users_groups,
    		onSelect: function (suggestion){

        	nombre = suggestion.value;
    			id = suggestion.data;
    			$(this).next().show();
    		}
			});

			return false;
		});

		// Quita un usuario o un grupo
		$('body').on('click', 'a.remove_corresponsable', function(){

			$(this).parent().parent().remove();
		});

		// Agrega un nuevo corresponsable a una responsabilidad
		$('body').on('click', 'button.add', function(){

			$(this).prev().val('');
			$(this).parent().append('<div style="clear: both"><input type="hidden" value="' + id + '" class="corresponsable"><div style="float: left">' + nombre + '</div><div style="float: right; height: 20px"><a href="javascript:;" class="remove_corresponsable">X</a></div></div>');
			$(this).hide();
		});

		// Agrega un nuevo grupo corresponsable a una responsabilidad
		$('body').on('click', 'button.add_grupo', function(){

			$(this).prev().val('');
			$(this).parent().append('<div style="clear: both"><input type="hidden" value="' + id + '" class="grupo_corresponsable"><div style="float: left">' + nombre + '</div><div style="float: right; height: 20px"><a href="javascript:;" class="remove_corresponsable">X</a></div></div>');
			$(this).hide();
		});

		// Remueve una iniciativa aun no guardada
		$('body').on('click', 'button.remove_iniciative', function(){

			$(this).parent().parent().parent().remove();
		});

		// Borra una iniciativa
		$('button.delete_iniciative').click(function(){

			if (confirm('¿De verdad desea eliminar la actividad?')){

				var id_iniciativa = $(this).attr('data-id');
				$(this).parent().parent().parent().remove();

				$.ajax({
        	type:'POST',    
        	url: '/evaluacion-resultados/delete-iniciative',
        	data:{
          	id: id_iniciativa,
						'_token': '{{ csrf_token() }}'
        	},
        	success: function(data){

        		alert('La actividad fue borrada');
        	}
      	});
      }
		});

		$('button.agregar_nota').click(function(){

			var button = $(this);
			var nota = button.parent().prev().val();

			if (button.hasClass('publica')){

				$.ajax({
        	type:'POST',    
        	url: '/evaluacion-resultados/save-note',
        	data:{
          	mensaje: nota,
          	id_plan: id_plan
        	},
        	success: function(data){
             
          	var fecha = new Date();
          	button.parent().parent().find('table').prepend('<tr><td>' + fecha.getDate() + '/' + meses[fecha.getMonth()] + '/' + fecha.getFullYear() + '</td><td>' + user + '</td><td>' + nota + '</td></tr>');
          	button.parent().prev().val('');
        	}
      	});
      }

      else{

      	$.ajax({
        	type:'POST',    
        	url: '/evaluacion-resultados/save-note',
        	data:{
          	mensaje: nota,
          	id_plan: id_plan,
          	id_empleado: id_user
        	},
        	success: function(data){
             
          	var fecha = new Date();
          	button.parent().parent().find('table').prepend('<tr><td>' + fecha.getDate() + '/' + meses[fecha.getMonth()] + '/' + fecha.getFullYear() + '</td><td>' + user + '</td><td>' + nota + '</td></tr>');
          	button.parent().prev().val('');
        	}
      	});
      }
		});

		// Cambia el plan
		$('select.id_plan').change(function(){

			// Recarga la pagina con el nuevo plan
			$('form.change_id_plan .id_plan').val($(this).val());
			$('form.change_id_plan').submit();
		});

		// Cambia el estado de una iniciativa
		$('select.estado_iniciativa').change(function(){

			$(this).parent().submit();
		});

		// Cambia la responsabilidad de una iniciativa
		$('body').on('change', 'select.id_responsabilidad', function(){

			var id_responsabilidad = $(this).val() * 1;
			var objetivo_corporativo = '';
			var current_type = 0;
			var counter = 0;
			var formula = '';

			for (var i = 0;i < responsabilities.length;i++){

				if (responsabilities[i].id == id_responsabilidad){

					formula = responsabilities[i].formula;

					for (var j = 0;j < objetivos_corporativos.length;j++){

						if (objetivos_corporativos[j].id_tipo != current_type){

							counter = 1;
							current_type = objetivos_corporativos[j].id_tipo;
						}

						if (objetivos_corporativos[j].id == responsabilities[i].id_objetivo_corporativo){

							objetivo_corporativo = objetivos_corporativos[j].letter + counter;
						}

						counter++;
					}
				}
			}

			$(this).parent().prev().text(objetivo_corporativo);
			$(this).parent().next().find('div').text(formula);
		});

		// Guarda una iniciativa
		$('body').on('click', 'button.save_iniciative', function(){

			var iniciative = $(this).parent().parent().parent();

			if (iniciative.find('textarea.description').val().trim().length == 0){

				alert('Escribe la descripción');
				iniciative.find('textarea.description').focus();
				return false;
			}

			if (iniciative.find('input.start_date').val().length == 0){

				alert('Escribe la fecha de inicio');
				iniciative.find('input.start_date').focus();
				return false;
			}

			if (iniciative.find('input.end_date').val().length == 0){

				alert('Escribe la fecha de término');
				iniciative.find('input.end_date').focus();
				return false;
			}

			if ($(this).next().hasClass('delete_iniciative')){

				$('form.guardar_iniciativa').append('<input type="hidden" name="id" value="' + $(this).next().attr('data-id') + '">');
			}

			$('form.guardar_iniciativa').append('<input type="hidden" name="id_responsabilidad" value="' + iniciative.find('select.id_responsabilidad').val() + '">');
			$('form.guardar_iniciativa').append('<input type="hidden" name="description" value="' + iniciative.find('textarea.description').val() + '">');
			$('form.guardar_iniciativa').append('<input type="hidden" name="start_date" value="' + iniciative.find('input.start_date').val() + '">');
			$('form.guardar_iniciativa').append('<input type="hidden" name="end_date" value="' + iniciative.find('input.end_date').val() + '">');

			iniciative.find('input.grupo_corresponsable').each(function(){

				$('form.guardar_iniciativa').append('<input type="hidden" name="grupos_corresponsables[]" value="' + $(this).val() + '">');
			});

			iniciative.find('input.corresponsable').each(function(){

				$('form.guardar_iniciativa').append('<input type="hidden" name="corresponsables[]" value="' + $(this).val() + '">');
			});
			
			$('form.guardar_iniciativa').submit();
		});
	});
</script>
@endsection