@extends('layouts.app')

@section('title', 'Registro de Logros')

@section('content')
<style type="text/css">
	table.DTFC_Cloned thead tr {background-color: #222B64 !important}
</style>
<?php $logros_revisados = 0;
			$revision_director = true;
			$mes_limite = (!empty($plan) ? $plan->periodo + 6 : 0); ?>
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/banner_evaluacion_resultados.png') }}" alt="">
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Registro de Logros</h5>
			<div class="card-body">
	 
@if (!empty($planes))

  @if (!empty($periodos_status_plan) && (empty($periodos_status_plan[count($periodos_status_plan) -1]->fecha_solicitado) || $periodos_status_plan[count($periodos_status_plan) -1]->fecha_solicitado == '0000-00-00'))


		
		<div class="row">
						
			<div class="col-md-12 text-center">
				
				<div class="alert alert-warning h4" role="alert">
					<i class="fa fa-exclamation-triangle"></i>	Debe Crear Nuevos Objetivos por el Cambio de Puesto
				</div>

			</div>
		</div>

	@endif


	<div class="row mb-3"> 
		<div class="col-12 offset-md-4 col-md-3 text-center">
			<div class="form-group">
				<label for="id_plan" class="font-weight-bolder mb-0">Plan:</label>							
				<form id="changePlanForm" action="registro-logros" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="id_empleado" value="{{$id_user}}">

				<select name="id_plan" id="id_plan" class="form-control id_plan">
					@for ($i = 0;$i < count($planes);$i++)

					<option value="{{$planes[$i]->id}}" <?php if ($plan->id == $planes[$i]->id){ ?>selected="selected"<?php } ?>>{{$planes[$i]->nombre}}</option>
			   @endfor

				</select>
				<input type="submit" style="display: none">
				</form>
			</div>
		</div> 
	</div>

	<div class="card mb-3">
		<h5 class="card-header bg-info text-white font-weight-bolder">Persona</h5>
		<div class="card-body">
		<div class="media">
			<img src="{{asset('img/vacantes/sinimagen.png')}}" style="width: 120px" class="align-self-center mr-3" alt="...">
			<div class="media-body">
			
			<p><h5 class="mt-0 d-inline">Nombres: </h5> {{$user->nombre}} {{$user->paterno}} {{$user->materno}}</p>
			<p><h5 class="mt-0 d-inline">Puesto: </h5>{{(!empty($user->jobPosition) ? $user->jobPosition->name : '')}}</p>
			<p><h5 class="mt-0 d-inline">Departamento: </h5>{{(!empty($user->jobPosition) && !empty($user->jobPosition->area) && !empty($user->jobPosition->area->department) ? $user->jobPosition->area->department->name: '')}}</p>
			<p><h5 class="mt-0 d-inline">Dirección: </h5>{{$user->direccion}}</p>
 			<p><h5 class="mt-0 d-inline">Jefe: </h5>{{(!empty($user->boss) ? $user->boss->nombre . ' ' . $user->boss->paterno . ' ' . $user->boss->materno : '')}}</p>
			</div>
		</div>
	  </div>
	  </div>


@if ($mostrar_boton_aceptar)


	<div class="row margin-top-20">
		<div class="col-md-12 text-center">
			<form action="/aceptar-objetivos-resultados" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_plan" value="{{$plan->id}}">
				<input type="hidden" name="id_empleado" value="{{$id_user}}">
				<input type="hidden" name="tipo" value="Resultados">
				<button type="submit" class="btn btn-primary">Acepto Los Resultados</button>
			</form>
		</div>
	</div>

@endif

@if (!empty($responsabilities))

<div class="card mb-3">
	<h5 class="card-header bg-info text-white font-weight-bolder">Cumplimiento de Compromisos</h5>
	<div class="card-body">
		 
		<div class="table-responsive" style="overflow: auto; max-height: 600px">
		<div style="overflow: auto; max-height: 600px">
			<table class="tabla_compromisos table table-striped table-bordered table-hover">
				<thead style="background-color: #222B64; color:white">
					<tr>
						<th class="cabeceras-evaluaciones">Clave</th>
						<th class="cabeceras-evaluaciones">Objetivo</th>
						<th class="cabeceras-evaluaciones">Meta</th>
						<th class="cabeceras-evaluaciones">Unidad de medida</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Ene</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Feb</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Mar</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Abr</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>May</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Jun</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Jul</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Ago</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Sep</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Oct</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Nov</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Dic</th>
					</tr>
				</thead>
				<tbody>

<?php $current_responsability = 0;
			$id_responsability = 0;
			$num_months = 1;
			$frequency = 1;
			$counter = 0;
			$current_type = 0; ?>

			@foreach ($responsabilities as $key => $responsability)

				@if ($id_responsability != $responsability->id)

					@if ($id_responsability != 0)

						@for ($i = $num_months;$i < 13;$i++)

						<td></td>
						@endfor

					</tr>
					@endif

		<?php $current_responsability = $responsability;
					$id_responsability = $responsability->id;
					$found_objetive = false;
					$num_months = 1;
					$frequency = 1;
					$new_periodo = true;
					$counter = 0;
					$current_type = 0; ?>

					@if ($responsability->frecuencia_captura == 'Bimestral')

			<?php $frequency = 2; ?>
					@else

						@if ($responsability->frecuencia_captura == 'Trimestral')

				<?php $frequency = 3; ?>
						@else

							@if ($responsability->frecuencia_captura == 'Anual')

					<?php $frequency = 12; ?>
							@endif
						@endif
					@endif

					<tr>
						<td class="text-center">
							<select class="text-center form-control size3">

					@foreach ($objetivos_corporativos as $key2 => $objetivo)

						@if ($current_type != $objetivo->id_tipo)

				<?php $current_type = $objetivo->id_tipo;
							$counter = 1; ?>
						@endif

						@if ($objetivo->id == $responsability->id_objetivo_corporativo)
								
								<option>{{$objetivo->letter}}{{$counter}}</option>

				<?php break; ?>
						@endif

			<?php $counter++; ?>
					@endforeach

							</select>
						</td>
						<td class="text-center">
							<input type="text" class="form-control size2" value="{{$responsability->description}}" readonly>
						</td>
						<td class="text-center">
							<input type="text" class="form-control size text-right" value="{{$responsability->meta}}" readonly>
						</td>
						<td class="text-center">
							<select class="text-center form-control size3">
								<option>{{($responsability->unidad == '#' ? '$' : ($responsability->unidad == '9' ? '0' : $responsability->unidad))}}</option>
							</select>
						</td>
				@endif

				@if (!empty($responsability->id_cumplimiento))

		<?php $month = $frequency * $responsability->periodo; ?>

					@for ($i = $num_months;$i < $month;$i++)

						<td></td>
					@endfor

		<?php $num_months = $month; ?>

					@if ($responsability->revisado == 1)

			<?php $class = '';
						$num_months++; ?>

						@if ($responsability->unidad == 'Si/No')

							@if ($responsability->valor_verde == $responsability->logro)

					<?php $class = 'success'; ?>
							@else

					<?php $class = 'danger'; ?>
							@endif
						@else

							@if ($responsability->valor_verde >= $responsability->valor_rojo)

								@if ($responsability->logro >= $responsability->valor_verde)

						<?php $class = 'success'; ?>
								@else

									@if ($responsability->logro <= $responsability->valor_rojo)

							<?php $class = 'danger'; ?>
									@else

							<?php $class = 'warning'; ?>
									@endif
								@endif
							@else

								@if ($responsability->logro <= $responsability->valor_verde)

						<?php $class = 'success'; ?>
								@else

									@if ($responsability->logro >= $responsability->valor_rojo)

							<?php $class = 'danger'; ?>
									@else

							<?php $class = 'warning'; ?>
									@endif
								@endif
							@endif
						@endif

						<td class="text-center">
							<button style="cursor: pointer" class="btn btn-{{$class}}">{{($responsability->unidad == '%' ? number_format($responsability->logro, 2, '.', ',') . '%' : ($responsability->unidad == '#' ? '$' . number_format($responsability->logro) : ($responsability->unidad == '9' ? number_format($responsability->logro) : $responsability->logro)))}}</button>
						</td>
					@endif
				@endif
			@endforeach

			@if ($id_responsability != 0)

				@for ($i = $num_months;$i < 13;$i++)

						<td></td>
				@endfor

					</tr>
			@endif
						
				</tbody>
			</table>
		</div>
		</div>
		</div>
		</div>
		@endif

<hr style="border-color: #A5A7A8">

<?php $meses = array('0', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');
            	$meses_completos = array('0', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
      $primer_periodo = 13; ?>

<hr class="divisor">

@if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin') && count($objetivos) > 0 && (!empty($objetivos[0]->revisado) || !empty($objetivos[count($objetivos) - 1]->revisado)))


<div class="row">
	<div class="col-md-12 text-center">
		<form method="post" action="/resetear-logros" style="display: inline-block" onsubmit="return confirm('¿Esta seguro de querer borrar los resultados?')">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id_empleado" value="{{$id_user}}">
			<input type="hidden" name="id_plan" value="{{$plan->id}}">
			<button class="btn btn-danger" type="submit">Borrar Resultados</button>
		</form>
	</div>
</div>
@endif


<div class="card mb-3">
	<h5 class="card-header bg-info text-white font-weight-bolder">Logros Personales</h5>
	<div class="card-body">
				

<div class="row">
	<div class="col-md-12 table-responsive">
		<table style="width:100%" class="tabla_logros tabla_compromisos row-border table-striped table-bordered">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th class="text-center">#</th>
					<th class="text-center" style="min-width: 300px">Objetivo</th>
					<th class="text-center">Clave</th>
					<th class="text-center">Meta</th>
					<th class="text-center">PESO</th>
					<th class="text-center">{{$plan->anio}}<br>Ene</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Feb</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Mar</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Abr</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>May</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Jun</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Jul</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Ago</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Sep</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Oct</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Nov</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Dic</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Guardar</th>
				</tr>
			</thead>
			<tbody>

@if (!empty($periodos_status_plan))

<?php $actual_objetivo = 0;
	  	$contador_meses = 1;
	  	$accion = false;
	  	$frecuencias = array();
	  	$periodo = 1;
	  	$frecuencia = 1;
  	  $contador_objetivos = 1;
	  	$anio_plan = (!empty($plan->anio) ? $plan->anio : date('Y'));
	  	$periodos = array();
	  	$meses_validos = array();
	  	$periodo_global = 0;
	  	$frecuencia_menor = 13;
			$contador_logros = 0;
			$show_revision_button = true;
			$show_save_button = false;
			$contador_comisiones = 0;
			$logros_revisados = 0; ?>

  @for ($i = 0;$i < count($objetivos); $i++)

  <?php $periodo_global = $objetivos[$i]->periodo; ?>

    @if ($actual_objetivo != $objetivos[$i]->id)

      @if ($actual_objetivo != 0)

	<?php $contador_objetivos++;
				$next_period = $periodo + 1;
				$next_period = $next_period * $frecuencia; ?>

				@if (($periodo * $frecuencia) < 12 && $objetivos[$i-1]->viejo != 1 && !$cambiar_valores && ($logros_revisados < 6 || $mes_a_registrar < $mes_limite))

					@if ($next_period <= 6)

			<?php $revision_director = false; ?>
					@endif

        	@if (!empty($objetivos[$i-1]->logro) || $objetivos[$i-1]->logro == '0')

        		@if ($plan->registrar_logros == 1 || ($plan->registrar_logros == 2 && $user->jefe == auth()->user()->employee->idempleado) || ($plan->registrar_logros == 4 && !empty(auth()->user()->employee->grado)) || auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin'))

        <?php $periodo++; ?>

        			@for ($j = 1;$j < $frecuencia;$j++)

        	<td></td>
					   	@endfor

				<?php $contador_meses = $contador_meses + $frecuencia?>

					<td class="text-center" style="padding: 5px">
				  
				<?php $meses_validos[] = $periodo * $frecuencia;
              $logro_class = 'logro';

              if (($contador_meses - 1) == $mes_a_registrar){

              	$show_revision_button = false;
              	$show_save_button = true;
              	$accion = true;

    	      		if (($frecuencia * $periodo) < 12){

    	      			if (date('n') > ($frecuencia * $periodo)){

    	      				$logro_class = 'logro obligatorio';
    	      			}
    	      		}

    	      		else{

    	      			if (date('Y') > $anio_plan){

    	      				$logro_class = 'logro obligatorio';
    	      			}
    	      		} ?>

            		@if ($objetivos[$i-1]->tipo != 'Si/No')

						<input type="text" data-metrico="{{$objetivos[$i-1]->id}}" data-periodo="{{$periodo}}" data-exist="0" class="form-control size {{($objetivos[$i-1]->tipo == '$' ? 'currency' : ($objetivos[$i-1]->tipo == '%' ? 'porcentaje' : 'numeric'))}} <?php echo $logro_class?>">
								@else

						<select data-metrico="{{$objetivos[$i-1]->id}}" data-periodo="{{$periodo}}" data-exist="0" class="form-control size <?php echo $logro_class?>">
							<option value="Si">Si</option>
							<option value="No">No</option>
						</select>
								@endif

								@if ($objetivos[$i-1]->tipo_bono == 'Comision')

						<input type="text" class="form-control size currency comision" placeholder="Comision">
								@endif

				<?php } ?>
          
          </td>
				  	@endif
					@endif
        @endif
		
				@for ($j = $contador_meses;$j <= 12; $j++)

          <td></td>
				@endfor
					
					<td class="text-center" style="padding: 5px">
				@if ($accion)

						<button class="btn btn-primary guardar" style="display: none">Guardar</button>
				@endif	  
					
					</td>
				</tr>
		  @endif

				<tr>
					<td class="text-center" style="padding: 5px">{{$contador_objetivos}}</td>
					<td style="padding: 5px">{{$objetivos[$i]->nombre}}</td>
					<td class="text-center" style="padding: 5px">

<?php $current_type = 0;
			$counter = 0;

			if ($objetivos[$i]->tipo_bono == 'Comision'){

				$contador_comisiones = 0;
			} ?>

			@for ($j = 0;$j < count($objetivos_corporativos);$j++)

				@if ($current_type != $objetivos_corporativos[$j]->id_tipo)

		<?php $counter = 1;
					$current_type = $objetivos_corporativos[$j]->id_tipo; ?>
				@endif

				@if ($objetivos_corporativos[$j]->id == $objetivos[$i]->id_objetivo_corporativo)

					{{$objetivos_corporativos[$j]->letter}}{{$counter}}</option>
				@endif
	<?php $counter++; ?>
			@endfor

					</td>
					<td class="text-right" style="padding: 5px">{{($objetivos[$i]->tipo_bono == 'Comision' ? '' : ($objetivos[$i]->tipo == 'Si/No' ? $objetivos[$i]->objetivo : ($objetivos[$i]->tipo == '$' ? '$' . number_format(str_replace(',', '', $objetivos[$i]->objetivo), 0, '.', ',') : ($objetivos[$i]->tipo == '%' ? $objetivos[$i]->objetivo . '%' : number_format(str_replace(',', '',$objetivos[$i]->objetivo), 0, '.', ',')))))}}</td>
					<td class="text-right" style="padding: 5px">{{$objetivos[$i]->peso}}</td>

<?php $actual_objetivo = $objetivos[$i]->id;
		  $contador_meses = 1;
		  $accion = false;
		  $frecuencia = 1;
		  $periodo = 0;
		  $contador_logros = -1;

		  //if ($logros_revisados < 6){

		  	$logros_revisados = 0;
		  //} ?>

		  @if ($objetivos[$i]->frecuencia != 'Mensual')

    	  @if ($objetivos[$i]->frecuencia == 'Anual')

    <?php $frecuencia = 12;
    		  array_push($frecuencias, 12); ?>
    		@endif

    		@if ($objetivos[$i]->frecuencia == 'Trimestral')

    	<?php $frecuencia = 3;
    		    array_push($frecuencias, 3); ?>
    		@endif

    		@if ($objetivos[$i]->frecuencia == 'Cuatrimestral')

    	<?php $frecuencia = 4;
    		    array_push($frecuencias, 4); ?>
    		@endif

    		@if ($objetivos[$i]->frecuencia == 'Semestral')

    	<?php $frecuencia = 6;
    		    array_push($frecuencias, 6); ?>
    		@endif

    		@if ($objetivos[$i]->frecuencia == 'Bimestral')

    	<?php $frecuencia = 2;
    		    array_push($frecuencias, 2); ?>
    		@endif
		  @else

  <?php array_push($frecuencias, 1)?>
    	@endif
    @endif

    @if (!empty($objetivos[$i]->logro) || $objetivos[$i]->logro == '0')

<?php $periodo++;
			$contador_logros++;

			if ($primer_periodo > $objetivos[$i]->periodo_logro){

				$primer_periodo = $objetivos[$i]->periodo_logro;
			}

			$temp_periodo = intval($objetivos[$i]->periodo_logro / $frecuencia);

      if ($objetivos[$i]->periodo_logro % $frecuencia != 0){

        $temp_periodo++;
      }

      $objetivos[$i]->periodo = $temp_periodo;
      $periodo_global = $temp_periodo;

      if ($frecuencia < $frecuencia_menor){

	  		$frecuencia_menor = $frecuencia;
	  		$periodos = array();
	  	}

	  	if ($frecuencia == $frecuencia_menor){

	  		$periodos[] = $temp_periodo + $contador_logros;
	  	}

			//$periodos[] = $objetivos[$i]->periodo; ?>
	    
      @while ($periodo < $objetivos[$i]->periodo_logro)

        @for ($k = 0;$k < $frecuencia;$k++)

          <td></td>
	    @endfor

	<?php $periodo++;
		  $contador_meses = $contador_meses + $frecuencia?>
	  @endwhile

      @for ($j = 1;$j < $frecuencia;$j++)

          <td></td>
		  @endfor

<?php $contador_meses = $contador_meses + $frecuencia; ?>

					<td class="text-center" style="padding: 5px">

    	@if ($objetivos[$i]->revisado == 0)

  <?php $accion = true;
  			$show_save_button = true; ?>

        @if ($objetivos[$i]->tipo != 'Si/No')

						<input type="text" value="{{$objetivos[$i]->logro}}" data-metrico="{{$objetivos[$i]->id}}" data-periodo="{{($objetivos[$i]->status == 5 ? $objetivos[$i]->periodo : $periodo)}}" data-exist="1" class="logro form-control size {{($objetivos[$i]->tipo == '$' ? 'currency' : ($objetivos[$i]->tipo == '%' ? 'porcentaje' : 'numeric'))}}">
			  @else

						<select data-metrico="{{$objetivos[$i]->id}}" data-periodo="{{($objetivos[$i]->status == 5 ? $objetivos[$i]->periodo : $periodo)}}" data-exist="1" class="logro form-control size">
							<option value="Si">Si</option>
							<option value="No" <?php if ($objetivos[$i]->logro == 'No'){ ?>selected="selected"<?php } ?>>No</option>
						</select>
			  @endif

			  @if ($objetivos[$i]->tipo_bono == 'Comision')

						<input type="text" class="form-control size currency comision" value="{{$objetivos[$i]->objetivo}}" placeholder="Comision">
				@endif
		  @else

	<?php $logros_revisados++; ?>

		  	@if ($objetivos[$i]->tipo_bono == 'Comision')

		  			<input type="text" style="color: white; background-color: green" value="{{($objetivos[$i]->tipo == 'Si/No' ? $objetivos[$i]->logro : ($objetivos[$i]->tipo == '$' ? '$' . number_format($objetivos[$i]->logro, 0, '.', ',') : ($objetivos[$i]->tipo == '%' ? number_format($objetivos[$i]->logro, 2, '.', ',') . '%' : number_format($objetivos[$i]->logro, 0, '.', ','))))}}" readonly class="form-control size text-right">
						<input type="text" class="form-control comision size text-right" style="color: white; background-color: green" value="${{number_format($objetivos[$i]->comisiones[$contador_comisiones]->meta, 2)}}" readonly>

		<?php $contador_comisiones++; ?>
				@else

						<input type="text" style="color: white; background-color: {{($objetivos[$i]->tipo == 'Si/No' ? ($objetivos[$i]->valor_verde == $objetivos[$i]->logro ? 'green' : 'red') : ($objetivos[$i]->valor_verde > $objetivos[$i]->valor_rojo ? ($objetivos[$i]->logro >= $objetivos[$i]->valor_verde ? 'green' : ($objetivos[$i]->logro <= $objetivos[$i]->valor_rojo ? 'red' : '#FFDB58')) : ($objetivos[$i]->logro <= $objetivos[$i]->valor_verde ? 'green' : ($objetivos[$i]->logro >= $objetivos[$i]->valor_rojo ? 'red' : '#FFDB58'))))}}" value="{{($objetivos[$i]->tipo == 'Si/No' ? $objetivos[$i]->logro : ($objetivos[$i]->tipo == '$' ? '$' . number_format($objetivos[$i]->logro, 0, '.', ',') : ($objetivos[$i]->tipo == '%' ? number_format($objetivos[$i]->logro, 2, '.', ',') . '%' : number_format($objetivos[$i]->logro, 0, '.', ','))))}}" readonly class="form-control size text-right">
				@endif
		  @endif

					</td>
		@else

		  @if ($fecha_autorizado != null && $objetivos[$i]->viejo != 1)

		  @if ($plan->registrar_logros == 1 || ($plan->registrar_logros == 2 && $user->jefe == auth()->user()->employee->idempleado) || ($plan->registrar_logros == 4 && !empty(auth()->user()->employee->grado)) || auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin'))

	<?php //$accion = true;
          $periodo = intval($plan->periodo / $frecuencia);

          if ($plan->periodo % $frecuencia != 0){

          	$periodo++;
          }
          /*$periodo = intval($objetivos[$i]->periodo / $frecuencia);

          if ($objetivos[$i]->periodo % $frecuencia != 0){

          	$periodo++;
          }*/

          $j = 1; ?>
        
        @while ($j < $periodo)

        	@for ($k = 0;$k < $frecuencia;$k++)

          <td></td>
					@endfor

	<?php $j++;
		  $contador_meses = $contador_meses + $frecuencia?>
		 @endwhile

    		@for ($j = 1;$j < $frecuencia;$j++)

          <td></td>
			  @endfor

	<?php $contador_meses = $contador_meses + $frecuencia?>

					<td class="text-center" style="padding: 5px">
						
	<?php $meses_validos[] = $periodo * $frecuencia;
        $logro_class = 'logro';

        if (($contador_meses - 1) == $mes_a_registrar){

          $show_revision_button = false;
          $accion = true;
          $show_save_button = true;

    	  	if (($frecuencia * $periodo) < 12){

    	    	if (date('n') > ($frecuencia * $periodo)){

    	      	$logro_class = 'logro obligatorio';
    	    	}
    	  	}

    	  	else{

    	    	if (date('Y') > $anio_plan){

    	     		$logro_class = 'logro obligatorio';
    	    	}
    	  	} ?>

    			@if ($objetivos[$i]->tipo != 'Si/No')

						<input type="text" value="{{$objetivos[$i]->logro}}" data-metrico="{{$objetivos[$i]->id}}" data-periodo="{{$periodo}}" data-exist="0" class="<?php echo $logro_class?> form-control size {{($objetivos[$i]->tipo == '$' ? 'currency' : ($objetivos[$i]->tipo == '%' ? 'porcentaje' : 'numeric'))}}">
			  	@else

						<select data-metrico="{{$objetivos[$i]->id}}" data-periodo="{{$periodo}}" data-exist="0" class="<?php echo $logro_class?> form-control size">
							<option value="Si">Si</option>
							<option value="No">No</option>
						</select>
			  	@endif

			  	@if ($objetivos[$i]->tipo_bono == 'Comision')

						<input type="text" class="form-control size currency comision" value="{{$objetivos[$i]->objetivo}}" placeholder="Comision">
					@endif

	<?php } ?>

					</td>
		  @endif
		  @endif
    @endif
	@endfor

	@if ($actual_objetivo == 0)

	<?php $show_revision_button = false; ?>
	@endif

	<?php $next_period = $periodo + 1;
				$next_period = $next_period * $frecuencia; ?>

	  @if (($periodo * $frecuencia) < 12 && $objetivos[$i-1]->viejo != 1 && !$cambiar_valores && ($logros_revisados < 6 || $mes_a_registrar < $mes_limite))

	  	@if ($next_period <= 6)

	<?php $revision_director = false; ?>
			@endif

        @if (!empty($objetivos[$i-1]->logro) || $objetivos[$i-1]->logro == '0')

        	@if ($plan->registrar_logros == 1 || ($plan->registrar_logros == 2 && $user->jefe == auth()->user()->employee->idempleado) || ($plan->registrar_logros == 4 && !empty(auth()->user()->employee->grado)) || auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin'))

      	  <?php $periodo++;
        		//$accion = true; ?>

        		@for ($j = 1;$j < $frecuencia;$j++)

          		<td></td>
						@endfor

		  <?php $contador_meses = $contador_meses + $frecuencia?>

				<td class="text-center" style="padding: 5px">
				  
		  <?php $logro_class = 'logro';

		  			if (($contador_meses - 1) == $mes_a_registrar){

              $show_revision_button = false;
              $accion = true;
              $show_save_button = true;

    	      	if (($frecuencia * $periodo) < 12){

    	      		if (date('n') > ($frecuencia * $periodo)){

    	      			$logro_class = 'logro obligatorio';
    	      		}
    	      	}

    	      	else{

    	      		if (date('Y') > $anio_plan){

    	      			$logro_class = 'logro obligatorio';
    	      		}
    	      	} ?>

            	@if ($objetivos[$i-1]->tipo != 'Si/No')

					<input type="text" data-metrico="{{$objetivos[$i-1]->id}}" data-periodo="{{$periodo}}" data-exist="0" class="<?php echo $logro_class?> form-control size {{($objetivos[$i-1]->tipo == '$' ? 'currency' : ($objetivos[$i-1]->tipo == '%' ? 'porcentaje' : 'numeric'))}}">
							@else

					<select data-metrico="{{$objetivos[$i-1]->id}}" data-periodo="{{$periodo}}" data-exist="0" class="<?php echo $logro_class?> form-control size">
						<option value="Si">Si</option>
						<option value="No">No</option>
					</select>
							@endif

							@if ($objetivos[$i-1]->tipo_bono == 'Comision')

					<input type="text" class="form-control size currency comision" placeholder="Comision">
							@endif
			<?php } ?>
          
          		</td>
			@endif
        @endif
	  @endif

	  @for ($j = $contador_meses;$j <= 12; $j++)

          		<td></td>
	  @endfor

				<td class="text-center" style="padding: 5px">
	  @if ($show_save_button)

					<button class="btn btn-primary guardar" style="display: none">Guardar</button>
	  @endif
				</td>

			</tr>
			<tr>
				<td class="cabeceras-evaluaciones"></td>
				<td class="text-center font-weight-bold" style="background-color: #222B64; color:white;width: 20%">Revisado</td>
				<td class="cabeceras-evaluaciones"></td>
				<td class="cabeceras-evaluaciones"></td>
				<td class="cabeceras-evaluaciones"></td>

<?php $salto = 0?>

	  @if (in_array(1, $frecuencias))

	<?php $salto = 1?>

	  @else
		@if (in_array(3, $frecuencias))

	<?php $salto = 3?>

		@else
			@if (in_array(6, $frecuencias))

		  <?php $salto = 6?>

			@else
				@if (in_array(12, $frecuencias))

			  <?php $salto = 12?>

				@else
					@if (in_array(2, $frecuencias))

					<?php $salto = 2?>

					@else

			  	<?php $salto = 1?>
			  	@endif
				@endif
			@endif
		@endif
	  @endif

<?php $num_meses = 1;

			// Ya hay objetivos y ya tienen logros revisados
			//if (!empty($objetivos[0]) && !empty($objetivos[0]->periodo)){

				// Meses para brincar
				//$num_meses = $periodo_global * $salto;

				//for ($k = 1;$k < $num_meses;$k++){ ?>

				<!--<td></td>-->
	<?php //}
			//}*/

			$periodo = $primer_periodo;
			$periodo = 0; ?>

	  @for ($i = 0;$i < count($periodos_status_plan);$i++)

	  	@if ($periodos_status_plan[$i]->status > 3)

	  		@if ($periodo == 0)

	  <?php $periodo = $periodos_status_plan[$i]->periodo; ?>
	  		@endif
														  
				@while ($num_meses < ($salto * $periodo))

					@for ($j = 0;$j < $salto;$j++)

				<td></td>
			
			<?php $num_meses++?>
					@endfor
				@endwhile

  <?php $num_meses++;
  			$periodo++; ?>

				@if (!empty($periodos_status_plan[$i]->fecha_revisado) && $periodos_status_plan[$i]->fecha_revisado != '0000-00-00')
					
				<td class="text-center" style="padding: 5px">{{substr($periodos_status_plan[$i]->fecha_revisado, 8) * 1}}/{{$meses[substr($periodos_status_plan[$i]->fecha_revisado, 5, 2) * 1]}}/{{substr($periodos_status_plan[$i]->fecha_revisado, 0, 4)}}</td>
				@else
				
					@if (!empty($periodos_status_plan[$i]->fecha_registrado) && $periodos_status_plan[$i]->fecha_registrado != '0000-00-00' && $periodos_status_plan[$i]->status == 4)

				<td class="text-center" style="padding: 5px">En<br>Revision</td>
					@else

				<td></td>
					@endif
				@endif
			@endif
	  @endfor

	  @for ($i = $num_meses;$i <= 13;$i++)

		<td></td>
	  @endfor

			</tr>
	@endif

		  </tbody>

		</table>
	</div>
</div>

@if (!empty($periodos_status_plan) && $show_save_button && ($plan->registrar_logros == 1 || ($plan->registrar_logros == 2 && $user->jefe == auth()->user()->employee->idempleado) || ($plan->registrar_logros == 4 && !empty(auth()->user()->employee->grado)) || auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')))
<div class="row my-4">
	<div class="col-md-12 text-center">
		<button class="btn btn-primary btn-abc-yellow guardar_logros"><i class="fas fa-check-circle"></i> Guardar logros</button>
	</div>
</div>
@endif

<div class="col-md-12 text-center">

		@if ($logros_revisados > 0 && $revision_director == true && !empty(auth()->user()->employee->grado) && empty($revisado_por_director) && $id_user != auth()->user()->employee->id)

		<div class="mt-4 mb-4">
			<form action="/evaluacion-resultados/revision-director" method="post" class="revision-director">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_plan" value="{{$plan->id}}">
				<input type="hidden" name="id_empleado" value="{{$id_user}}">
				<button class="btn btn-primary mt-3" type="submit" style="margin-top: 0 !important"><i class="fas fa-clipboard-check"></i>Aprobar Semestre</button>
			</form>
		</div>
		<div class="mt-4 mb-4">
			<form action="/evaluacion-resultados/rechazo-director" method="post" class="rechazo-director" onsubmit="return confirm('Todos los resultados serán borrados. ¿Esta seguro?')">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_plan" value="{{$plan->id}}">
				<input type="hidden" name="id_empleado" value="{{$id_user}}">
				<button class="btn btn-danger mt-3" type="submit" style="margin-top: 0 !important"><i class="fas fa-times"></i>Rechazar Semestre</button>
			</form>
		</div>
		@endif
					
<?php $status_plan = (!empty($periodos_status_plan) ? $periodos_status_plan[count($periodos_status_plan) - 1] : array()); ?>

	  @if (!empty($status_plan) && $show_save_button)

	  	@if ($show_revision_button && ($plan->registrar_logros == 1 || ($plan->registrar_logros == 2 && $user->jefe == auth()->user()->employee->idempleado) || ($plan->registrar_logros == 4 && !empty(auth()->user()->employee->grado)) || auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')))

		<div class="margin-top-20">
			<form action="solicitar-revision" method="post" class="solicitar-revision">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_plan" value="{{$plan->id}}">
				<input type="hidden" name="periodo" value="{{$status_plan->periodo}}">
				<input type="hidden" name="id_empleado" value="{{$id_user}}">
				<button class="btn btn-primary mt-3" type="submit" style="margin-top: 0 !important"><i class="fas fa-clipboard-check"></i><?php echo ($plan->registrar_logros != $plan->autorizar_logros ? ' Solicitar Revisión' : ' Cerrar Resultados')?></button>
			</form>
		</div>
			@endif
	  @else

	  	@if (!empty($revisado_por_director))

	  <div class="mt-4 mb-4">
	  	<p>Fecha de Revisión <b>por el Director</b></p>
	  	<p>{{substr($revisado_por_director->created_at, 8, 2)}} de {{$meses_completos[substr($revisado_por_director->created_at, 5, 2) * 1]}} del {{substr($revisado_por_director->created_at, 0, 4)}}</p>
	  </div>
	  	@else

				@if (!empty($status_plan))

		<div class="margin-top-20">

					@if (!empty($status_plan->fecha_revisado) && $status_plan->fecha_revisado != '0000-00-00')

			<p>Fecha de Revisión</p>
			<p>{{substr($status_plan->fecha_revisado, 8)}} de {{$meses_completos[substr($status_plan->fecha_revisado, 5, 2) * 1]}} del {{substr($status_plan->fecha_revisado, 0, 4)}}</p>
	    		@else

	      		@if (!empty($status_plan->fecha_registrado) && $status_plan->fecha_registrado != '0000-00-00')
			
			<p>Fecha de Solicitud</p>
			<p>{{substr($status_plan->fecha_registrado, 8)}} de {{$meses_completos[substr($status_plan->fecha_registrado, 5, 2) * 1]}} del {{substr($status_plan->fecha_registrado, 0, 4)}}</p>
			    	@endif
	    		@endif

		</div>
				@endif
	  	@endif
	  @endif

	</div>

</div>
</div>
@if (!empty($objetivos_corresponsables))

<div class="card mb-3">
	<h5 class="card-header bg-info text-white font-weight-bolder">Logros Corresponsable</h5>
	<div class="card-body">
				 
		<div class="table-responsive" style="overflow: auto; max-height: 600px">
			<table class="tabla_logros_corresponsable tabla_compromisos table table-striped">
				<thead style="background-color: #222B64; color:white;">
					<tr>
						<th class="cabeceras-evaluaciones">Clave</th>
						<th class="cabeceras-evaluaciones">Objetivo</th>
						<th class="cabeceras-evaluaciones">Meta</th>
						<th class="cabeceras-evaluaciones">Unidad de medida</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Ene</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Feb</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Mar</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Abr</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>May</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Jun</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Jul</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Ago</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Sep</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Oct</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Nov</th>
						<th class="cabeceras-evaluaciones">{{$plan->anio}}<br>Dic</th>
					</tr>
				</thead>
				<tbody>

	<?php $current_objetivo = 0;
				$id_objetivo = 0;
				$num_months = 1;
				$frequency = 1;
				$counter = 0;
				$current_type = 0; ?>

				@foreach ($objetivos_corresponsables as $key => $objetivo)

					@if ($id_objetivo != $objetivo->id)

						@if ($id_objetivo != 0)

							@for ($i = $num_months;$i < 13;$i++)

						<td></td>
							@endfor

					</tr>
						@endif

			<?php $current_objetivo = $objetivo;
						$id_objetivo = $objetivo->id;
						$found_objetive = false;
						$num_months = 1;
						$frequency = 1;
						$counter = 0;
						$current_type = 0; ?>

						@if ($objetivo->frecuencia == 'Bimestral')

				<?php $frequency = 2; ?>
						@else

							@if ($objetivo->frecuencia == 'Trimestral')

					<?php $frequency = 3; ?>
							@else

								@if ($objetivo->frecuencia == 'Cuatrimestral')

						<?php $frequency = 4; ?>
								@else

									@if ($objetivo->frecuencia == 'Anual')

							<?php $frequency = 12; ?>
									@endif
								@endif
							@endif
						@endif

					<tr>
						<td class="text-center">
							<select class="text-center form-control size3">

						@foreach ($objetivos_corporativos as $key2 => $objetivo_corporativo)

							@if ($current_type != $objetivo_corporativo->id_tipo)

					<?php $current_type = $objetivo_corporativo->id_tipo;
								$counter = 1; ?>
							@endif

							@if ($objetivo_corporativo->id == $objetivo->id_objetivo_corporativo)
								
								<option>{{$objetivo_corporativo->letter}}{{$counter}}</option>

					<?php break; ?>
							@endif

				<?php $counter++; ?>
						@endforeach

							</select>
						</td>
						<td class="text-center">
							<input type="text" class="form-control size2" value="{{$objetivo->nombre}}" readonly>
						</td>
						<td class="text-center">
							<input type="text" class="form-control size text-right" value="{{$objetivo->objetivo}}" readonly>
						</td>
						<td class="text-center">
							<select class="text-center form-control size3">
								<option>{{($objetivo->tipo == '#' ? '$' : ($objetivo->tipo == '9' ? '0' : $objetivo->tipo))}}</option>
							</select>
						</td>
					@endif

					@if (!empty($objetivo->id_logro))

			<?php $month = $frequency * $objetivo->periodo; ?>

						@for ($i = $num_months;$i < $month;$i++)

						<td></td>
						@endfor

			<?php $num_months = $month; ?>

						@if ($objetivo->revisado == 1)

				<?php $class = '';
							$num_months++; ?>

							@if ($objetivo->tipo == 'Si/No')

								@if ($objetivo->valor_verde == $objetivo->logro)

						<?php $class = 'success'; ?>
								@else

						<?php $class = 'danger'; ?>
								@endif
							@else

								@if ($objetivo->valor_verde >= $objetivo->valor_rojo)

									@if ($objetivo->logro >= $objetivo->valor_verde)

							<?php $class = 'success'; ?>
									@else

										@if ($objetivo->logro <= $objetivo->valor_rojo)

								<?php $class = 'danger'; ?>
										@else

								<?php $class = 'warning'; ?>
										@endif
									@endif
								@else

									@if ($objetivo->logro <= $objetivo->valor_verde)

							<?php $class = 'success'; ?>
									@else

										@if ($objetivo->logro >= $objetivo->valor_rojo)

								<?php $class = 'danger'; ?>
										@else

								<?php $class = 'warning'; ?>
										@endif
									@endif
								@endif
							@endif

						<td class="text-center">
							<button style="cursor: pointer" class="btn btn-{{$class}}">{{($objetivo->tipo == '%' ? number_format($objetivo->logro, 2, '.', ',') . '%' : ($objetivo->tipo == '#' ? '$' . number_format($objetivo->logro) : ($objetivo->tipo == '9' ? number_format($objetivo->logro) : $objetivo->logro)))}}</button>
						</td>
						@endif
					@endif
				@endforeach

				@if ($id_objetivo != 0)

					@for ($i = $num_months;$i < 13;$i++)

						<td></td>
					@endfor

					</tr>
				@endif
						
				</tbody>
			</table>
		</div>
		</div>
		</div>
			@endif

			<div class="card mb-3">
				<h5 class="card-header bg-info text-white font-weight-bolder">Observaciones</h5>
				<div class="card-body">
							

<div class="row margin-top-20">
	<div class="col-md-12">
		<div style="max-height: 200px; overflow-y: auto">
 			<table width="100%" style="font-size: 13px" border="1" class="notas table-bordered table-striped">
				<thead style="background-color: #222B64; color:white;">
					<tr>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Fecha</th>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Persona</th>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Nota</th>
					</tr>
				</thead>
				<tbody>

      		@for ($i = 0;$i < count($mensajes); $i++) 

					<tr>
						<td class="text-center" style="padding: 5px; border: 0">{{substr($mensajes[$i]->fecha, 8) * 1}}/{{$meses[substr($mensajes[$i]->fecha, 5, 2) * 1]}}/{{substr($mensajes[$i]->fecha, 0, 4)}}</td>
						<td class="text-center" style="padding: 5px; border: 0">{{$mensajes[$i]->nombre}} {{$mensajes[$i]->paterno}} {{$mensajes[$i]->materno}}</td>
						<td class="text-center" style="padding: 5px; border: 0">{{$mensajes[$i]->mensaje}}</td>
					</tr>
				@endfor

				</tbody>
			</table>
		</div>
		<h4 class="titulos-evaluaciones mt-4">Nota</h4>
		<div>
			<textarea style="width: 100%; height: 100px; border-radius: 7px"></textarea>
		</div>
		<div class="text-right mt-3">
			<button class="btn btn-primary agregar_nota"><i class="fas fa-plus-circle"></i> Agregar nota</button>
		</div>
	</div>
</div>
</div>
</div>
@else
<div class="row">
				
	<div class="col-md-12 text-center">
		
		<div class="alert alert-warning h4" role="alert">
			<i class="fa fa-exclamation-triangle"></i>	No hay planes abiertos
		</div>

	</div>
</div>
@endif
</div>
</div>
</div>
</div>
@endsection

@section('scripts')
	<script>

	  var id_plan = <?php echo (!empty($plan->id) ? $plan->id : 0)?>;
	  var id_user = <?php echo $id_user?>;
	  var guardar_logros = false;
	  var num_logros = 0;
	  var actual_logro = 0;
	  var total_logros = 0;
		
		$(document).ready(function(){

			/*$.ajaxSetup({
    		headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
			});*/
					
			$('.tabla_logros').DataTable({
					scrollX: true,
					scrollCollapse: true,
    				paging: false,
    				searching: false,
					ordering: false,
    				info: false,
    				fixedColumns:   {
          		leftColumns: 2
       			}
				});

  		//$('.table-container').animate({scrollLeft: 1000}, 50);

			$('select.id_plan').change(function(){

				$('form#changePlanForm').submit();
			});

			$('button.guardar').click(function(){

				var propuesta = $(this).parent().parent();
				var logro = propuesta.find('.logro');
				var id_metrico = logro.attr('data-metrico');
				var periodo = logro.attr('data-periodo');
				var exist = logro.attr('data-exist');

				if (logro.val().trim().length == 0){

					alert('Escribe un número o selecciona el logro');
					logro.focus();
					return false;
				}

				var comision = 0;

				if (propuesta.find('input.comision').length == 1){

					if (propuesta.find('input.comision').val().trim().length == 0){

						alert('Escribe la comisión');
						propuesta.find('input.comision').focus();
						return false;
					}

					comision = propuesta.find('input.comision').val();
				}


				
				/*var registro = logro.val();

				if (registro != 'Si' && registro != 'No' && isNaN(registro)){

					alert('Debes registrar sólo numeros');
					logro.focus();
					return false;
				}*/

				$.ajax({
        	type:'POST',    
        	url: 'guardar-logro',
          data:{
          	logro: logro.val().replace('$', '').replace('%', '').replace(/,/g, ''),
          	comision: comision,
            periodo: periodo,
          	id_metrico: id_metrico,
          	exist: exist,
          	id_plan: id_plan,
          	id_empleado: id_user,
			'_token': '{{ csrf_token() }}'
          },
          success: function(data){
             
            //if (data != 0){

            	logro.attr('data-exist', 1);
            	
			  	// No se han guardado todos los logros
            	//if ($('table.tabla_logros tbody tr td button.guardar').length > num_logros){
            	if (num_logros < total_logros){

            		actual_logro++;

            		while($('table.tabla_logros tbody tr:nth-child(' + actual_logro + ') td button.guardar').length == 0){

									actual_logro++;
								}

    						// Se guarda el siguiente logro
            		num_logros++;

            		$('table.tabla_logros tbody tr:nth-child(' + actual_logro + ') td button.guardar').click();
            	}

            	// Se guardaron todos los logros
            	else{

            		num_logros = 0;
            		actual_logro = 0;
            		guardar_logros = false;
            		alert('Los logros fueron guardados');
            		$('form#changePlanForm').submit();
            	}
            //}
          }
        });
			});

      $('button.agregar_nota').click(function(){

				var nota = $('textarea').val();

				$.ajax({
        	type:'POST',    
        	url: 'agregar-nota',
          data:{
          	mensaje: nota,
          	id_plan: id_plan,
          	id_empleado: id_user,
			'_token': '{{ csrf_token() }}'
          },
          success: function(data){
             
            var meses = [];
            meses[0] = 'Ene';
            meses[1] = 'Feb';
            meses[2] = 'Mar';
            meses[3] = 'Abr';
            meses[4] = 'May';
            meses[5] = 'Jun';
            meses[6] = 'Jul';
            meses[7] = 'Ago';
            meses[8] = 'Sep';
            meses[9] = 'Oct';
            meses[10] = 'Nov';
            meses[11] = 'Dic';
            var fecha = new Date();
            $('table.notas').prepend('<tr><td class="text-center" style="padding: 5px; border: 0">' + fecha.getDate() + '/' + meses[fecha.getMonth()] + '/' + fecha.getFullYear() + '</td><td class="text-center" style="padding: 5px; border: 0">' + data + '</td><td class="text-center" style="padding: 5px; border: 0">' + nota + '</td></tr>');
            $('textarea').val('');
          }
        });
			});

      $('form.solicitar-revision').submit(function(){

      	if ($('.logro.obligatorio[data-exist="0"]').length > 0){

      		alert('Debes guardar todos tus logros antes de solicitar revisión');
      		return false;
      	}
		  
		return confirm('¿Está usted seguro?');
      });
			
	  // Se dio click en guardar todos los logros
			$('.guardar_logros').click(function(){

				// Hay logros para guardar
				if ($('table.tabla_logros tbody tr td button.guardar').length > 0){

					total_logros = $('table.tabla_logros tbody tr td button.guardar').length;

					guardar_logros = true;
					actual_logro = 1;
						
					// Se guarda el primer logro
					while($('table.tabla_logros tbody tr:nth-child(' + actual_logro + ') td button.guardar').length == 0){

						actual_logro++;
					}

					num_logros++;

					$('table.tabla_logros tbody tr:nth-child(' + actual_logro + ') td button.guardar').click();
				}
			});

			$('input.porcentaje').inputmask("numeric", {
    		radixPoint: ".",
    		groupSeparator: ",",
    		digits: 2,
    		autoGroup: true,
    		suffix: '%', //No Space, this will truncate the first character
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});

			$('input.currency').inputmask("numeric", {
    		radixPoint: ".",
    		groupSeparator: ",",
    		digits: 2,
    		autoGroup: true,
    		prefix: '$', //No Space, this will truncate the first character
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});

			$('input.numeric').inputmask("numeric", {
    		groupSeparator: ",",
    		autoGroup: true,
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});
		});
	</script>
@endsection