@extends('layouts.app')

@section('title', 'Revisión de Logros')

@section('content')
<style type="text/css">
	table.DTFC_Cloned thead tr {background-color: #222B64 !important}
</style>
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="img/banner_evaluacion_resultados.png" alt="">
		<h3 class="titulos-evaluaciones mt-3 font-weight-bold">Revisión de Logros</h3>
		<hr>
		
@if (!empty($planes))

	<div class="row">
	<div class="col-md-12">
    <!--<p class="margin-top-20 cabeceras-evaluaciones" style="padding: 5px 10px">Periodo Abierto: <?php //echo $plan->anio?></p>-->
		<div class="row margin-top-20">
			<div class="col-md-12 text-center">
				<form id="changePlanForm" action="revision-logros" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="id_empleado" value="{{$id_user}}">
					<div>Plan:  <select name="id_plan" style="border-radius: 7px; margin-bottom: 4px" class="id_plan">

											@for ($i = 0;$i < count($planes);$i++)

											 	<option value="{{$planes[$i]->id}}" <?php if ($plan->id == $planes[$i]->id){ ?>selected="selected"<?php } ?>>{{$planes[$i]->nombre}}</option>
											@endfor

											</select>
					</div>
					<input type="submit" style="display: none">
				</form>

			</div>
		</div>
		<div class="row mt-4">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<table width="100%">
					<tr>
						<td width="70">Persona:</td>
						<td>
							<input type="text" value="{{$user->nombre}} {{$user->paterno}} {{$user->materno}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
						</td>
					</tr>
					<tr>
						<td>Puesto:</td>
						<td>
							<input type="text" value="{{(!empty($user->jobPosition) ? $user->jobPosition->name : '')}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
						</td>
					</tr>
					<tr>
						<td>Departamento:</td>
						<td>
							<input type="text" value="{{(!empty($user->jobPosition) && !empty($user->jobPosition->area) && !empty($user->jobPosition->area->department) ? $user->jobPosition->area->department->name: '')}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
						</td>
					</tr>
					<tr>
						<td>Dirección:</td>
						<td>
							<input type="text" value="{{$user->direccion}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
						</td>
					</tr>
					<tr>
						<td>Jefe:</td>
						<td>
							<input type="text" value="{{(!empty($user->boss) ? $user->boss->nombre . ' ' . $user->boss->paterno . ' ' . $user->boss->materno : '')}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
						</td>
					</tr>
				</table>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
</div>
<hr style="border-color: #A5A7A8">

{{--@if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin') || !empty($bono))

<div class="row">
	<div class="col-md-12">
		<strong>Total Bono</strong> <input type="text" value="{{(!empty($bono) ? $bono->total_bono : '')}}" class="form-control total_bono size currency" style="display: inline-block; width: auto" readonly>
	</div>
</div>
@endif--}}

<?php $meses = array('0', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');
            	$meses_completos = array('0', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'); ?>

<div class="row">
	<div class="col-md-12">
		<table style="width:100%" style="font-size: 13px" class="tabla_logros table-striped table-bordered">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th class="text-center">#</th>
					<th class="text-center" style="min-width: 300px">Objetivo</th>
					<th class="text-center">Clave</th>
					<th class="text-center">Meta</th>
					<th class="text-center">PESO</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Ene</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Feb</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Mar</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Abr</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>May</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Jun</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Jul</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Ago</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Sep</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Oct</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Nov</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Dic</th>
				</tr>
			</thead>
			<tbody>

<?php $actual_objetivo = 0;
			$contador_meses = 1;
			$frecuencias = array();
			$contador_objetivos = 1;
			$periodos = array();
			$meses_validos = array();
			$promedio = 0;
			$periodo_global = 0;
			$frecuencia_menor = 13;
			$contador_logros = 0;
			$contador_comisiones = 0; ?>

      @for ($i = 0;$i < count($objetivos); $i++)

  <?php $periodo_global = $objetivos[$i]->periodo; ?>

        @if ($actual_objetivo != $objetivos[$i]->id)

          @if ($actual_objetivo != 0)

      <?php $contador_objetivos++?>

            @for ($j = $contador_meses;$j <= 12; $j++)

          <td></td>
						@endfor

				</tr>
					@endif

				<tr>
					<td class="text-center" style="padding: 5px">{{$contador_objetivos}}</td>
					<td style="padding: 5px">
						<div style="width: 300px">{{$objetivos[$i]->nombre}}</div>
					</td>
					<td class="text-center" style="padding: 5px">

		<?php $counter = 0;
					$current_type = 0; ?>

					@for ($j = 0;$j < count($objetivos_corporativos_sin_responsable);$j++)

						@if ($current_type != $objetivos_corporativos_sin_responsable[$j]->id_tipo)

				<?php $counter = 1;
							$current_type = $objetivos_corporativos_sin_responsable[$j]->id_tipo; ?>
						@endif

						@if ($objetivos_corporativos_sin_responsable[$j]->id == $objetivos[$i]->id_objetivo_corporativo)

							{{$objetivos_corporativos_sin_responsable[$j]->letter}}{{$counter}}
				<?php break; ?>
						@endif
						
			<?php $counter++; ?>
					@endfor

					</td>
					<td class="text-right" style="padding: 5px">{{($objetivos[$i]->tipo == 'Si/No' ? $objetivos[$i]->objetivo : ($objetivos[$i]->tipo == '$' ? '$' . number_format(str_replace(',', '', $objetivos[$i]->objetivo) * 1, 1, ',', '.') : ($objetivos[$i]->tipo == '%' ? str_replace(',', '', $objetivos[$i]->objetivo) . '%' : number_format(str_replace(',', '', $objetivos[$i]->objetivo), 1, ',', '.'))))}}</td>
					<td class="text-right" style="padding: 5px">{{$objetivos[$i]->peso}}</td>

		<?php $actual_objetivo = $objetivos[$i]->id;
		      $contador_meses = 1;
		      $frecuencia = 1;
		      $periodo = 0;
		      $contador_logros = -1; ?>

		      @if ($objetivos[$i]->frecuencia != 'Mensual')

    	   	 	@if ($objetivos[$i]->frecuencia == 'Anual')

    		<?php $frecuencia = 12;
    		      array_push($frecuencias, 12); ?>
    		    @endif

    				@if ($objetivos[$i]->frecuencia == 'Trimestral')

    		<?php $frecuencia = 3;
    		      array_push($frecuencias, 3); ?>
    		    @endif

    		    @if ($objetivos[$i]->frecuencia == 'Cuatrimestral')

    		<?php $frecuencia = 4;
    		      array_push($frecuencias, 4); ?>
    		    @endif

    		    @if ($objetivos[$i]->frecuencia == 'Semestral')

    		<?php $frecuencia = 6;
    		      array_push($frecuencias, 6); ?>
    		    @endif

    		    @if ($objetivos[$i]->frecuencia == 'Bimestral')

    		<?php $frecuencia = 2;
    		      array_push($frecuencias, 2); ?>
    		    @endif
    		  @else

    		  	<?php array_push($frecuencias, 1)?>
    		  @endif
        @endif

        @if (!empty($objetivos[$i]->logro) || $objetivos[$i]->logro == '0')
				
	  <?php $periodo++;
	  			$contador_logros++;

	  			$temp_periodo = intval($objetivos[$i]->periodo_logro / $frecuencia);

      		if ($objetivos[$i]->periodo_logro % $frecuencia != 0){

        		$temp_periodo++;
      		}

      		$objetivos[$i]->periodo = $temp_periodo;
      		$periodo_global = $temp_periodo; ?>
				
			@while ($periodo < $objetivos[$i]->periodo)

        		@for ($k = 0;$k < $frecuencia;$k++)

          	  <td></td>
				@endfor

	  	  <?php $periodo++;
				$contador_meses = $contador_meses + $frecuencia;?>
			@endwhile
				
	  <?php if ($frecuencia < $frecuencia_menor){

	  				$frecuencia_menor = $frecuencia;
	  				$periodos = array();
	  			}

	  			if ($frecuencia == $frecuencia_menor){

	  				$periodos[] = $temp_periodo + $contador_logros;
	  			}

      		$meses_validos[] = $periodo * $frecuencia;?>

    		  @for ($j = 1;$j < $frecuencia;$j++)

    		  <td></td>
    		  @endfor

    	<?php $contador_meses = $contador_meses + $frecuencia;
    				$peso = $valor = 0;

    				// El logro del objetivo ya fue revisado
    				if ($objetivos[$i]->revisado == 1){

    					// El objetivo es de tipo Si/No 
    					if ($objetivos[$i]->valor_verde == 'Si' || $objetivos[$i]->valor_verde == 'No'){

								// Se cumplió el objetivo
								if ($objetivos[$i]->logro == $objetivos[$i]->valor_verde){

									$peso = $objetivos[$i]->peso;
								}
							}

							// El objetivo es de tipo numérico
							else{

								$objetivos[$i]->valor_verde = str_replace(',', '', $objetivos[$i]->valor_verde) * 1;
								$objetivos[$i]->logro = str_replace(',', '', $objetivos[$i]->logro) * 1;

								// Se cumplió el objetivo
								if ($objetivos[$i]->logro >= $objetivos[$i]->valor_verde){

									$peso = $objetivos[$i]->peso;
								}

								// Se cumplió a medias o no se cumplió
								else{

    							// Se calcula la calificación respecto al peso 
    							$valor = $objetivos[$i]->logro * 100 / $objetivos[$i]->valor_verde;
									$peso = $objetivos[$i]->peso * $valor / 100;
								}
							}

							// Se suma la calificación del objetivo
							$promedio += $peso;
						} ?>

    		  <td class="text-right" style="padding: 5px">

    		  	@if ($objetivos[$i]->tipo_bono == 'Comision')

		  			<input type="text" style="color: white; background-color: green" value="{{($objetivos[$i]->tipo == 'Si/No' ? $objetivos[$i]->logro : ($objetivos[$i]->tipo == '$' ? '$' . number_format($objetivos[$i]->logro, 1, '.', ',') : ($objetivos[$i]->tipo == '%' ? number_format($objetivos[$i]->logro, 2, '.', ',') . '%' : number_format($objetivos[$i]->logro, 2, '.', ','))))}}" readonly class="form-control size text-right">
						<input type="text" class="form-control comision size text-right" style="color: white; background-color: green" value="${{number_format($objetivos[$i]->comisiones[$contador_comisiones]->meta, 2)}}" readonly>

				<?php $contador_comisiones++; ?>
						@else

    		    <input type="text" style="color: white; background-color: {{($objetivos[$i]->tipo == 'Si/No' ? ($objetivos[$i]->valor_verde == $objetivos[$i]->logro ? 'green' : 'red') : ($objetivos[$i]->valor_verde > $objetivos[$i]->valor_rojo ? ($objetivos[$i]->logro >= $objetivos[$i]->valor_verde ? 'green' : ($objetivos[$i]->logro <= $objetivos[$i]->valor_rojo ? 'red' : '#FFDB58')) : ($objetivos[$i]->logro <= $objetivos[$i]->valor_verde ? 'green' : ($objetivos[$i]->logro >= $objetivos[$i]->valor_rojo ? 'red' : '#FFDB58'))))}}" value="{{($objetivos[$i]->tipo == 'Si/No' ? $objetivos[$i]->logro : ($objetivos[$i]->tipo == '$' ? '$' . number_format(str_replace('$', '', $objetivos[$i]->logro), 2, ',', '.') : ($objetivos[$i]->tipo == '%' ?  str_replace('%', '', $objetivos[$i]->logro) . '%' : number_format($objetivos[$i]->logro, 2, ',', '.'))))}}" readonly class="form-control size text-right">
    		    @endif

    		  </td>
    		@endif
			@endfor

			@for ($j = $contador_meses;$j <= 12; $j++)

          <td></td>
			@endfor

				</tr>

<?php $salto = 0?>

			@if (in_array(1, $frecuencias))

	<?php $salto = 1?>

			@else
			  @if (in_array(3, $frecuencias))

		<?php $salto = 3?>

				@else
				  @if (in_array(6, $frecuencias))

			<?php $salto = 6?>

					@else
						@if (in_array(12, $frecuencias))

				<?php $salto = 12?>

						@else
							@if (in_array(2, $frecuencias))

					<?php $salto = 2?>

							@else
							
							<?php $salto = 1?>
							@endif
						@endif
					@endif
				@endif
			@endif

<?php //$num_meses = $objetivos[0]->periodo * $salto;
			$num_meses = $periodo_global * $salto;

			if ($plan->id < 16){

				$bono = 0;
				$promedio = 0;
			} ?>

				<!--<tr>
					<td class="cabeceras-evaluaciones"></td>
					<td class="text-center cabeceras-evaluaciones" style="padding: 5px 10px 5px 150px">BONO LOGRADO</td>
					<td class="cabeceras-evaluaciones"></td>
					<td class="cabeceras-evaluaciones"></td>
					<td class="cabeceras-evaluaciones"></td>

	  	/*@for ($k = 1;$k < $num_meses;$k++)

					<td></td>
			@endfor

					<td class="text-center" style="padding: 5px">${{number_format($bono, 2)}}</td>

			@for ($k = $num_meses + 1;$k <= 12;$k++)

					<td></td>
			@endfor*/

				</tr>-->
				<!--<tr>
					<td class="cabeceras-evaluaciones"></td>
					<td class="text-center cabeceras-evaluaciones" style="padding: 5px 10px 5px 150px">PROMEDIO</td>
					<td class="cabeceras-evaluaciones"></td>
					<td class="cabeceras-evaluaciones"></td>
					<td class="cabeceras-evaluaciones"></td>

			/*@for ($k = 1;$k < $num_meses;$k++)

					<td></td>
			@endfor

					<td class="text-center" style="padding: 5px">{{number_format($promedio, 1)}}</td>

			@for ($k = $num_meses + 1;$k <= 12;$k++)

					<td></td>
			@endfor*/

				</tr>-->
				<tr>
					<td class="cabeceras-evaluaciones"></td>
					<td class="text-center font-weight-bold" style="background-color: #222B64; color:white;width: 20%">Revisado</td>
					<td class="cabeceras-evaluaciones"></td>
					<td class="cabeceras-evaluaciones"></td>
					<td class="cabeceras-evaluaciones"></td>

<?php $periodo = 1; ?>

	  @for ($k = 1;$k < $num_meses;$k++)

			<!--<td></td>-->
		@endfor 

			@for ($i = 0;$i < count($periodos_status_plan);$i++)
																
				@if ($periodos_status_plan[$i]->status > 3)
																
					@while ($num_meses < ($salto * $periodo))

						@for ($j = 1;$j < $salto;$j++)

					<td></td>
					<?php $num_meses++?>
						@endfor
					@endwhile

			  <?php $num_meses++;
					$periodo++;?>
				
					@if (!empty($periodos_status_plan[$i]->fecha_revisado) && $periodos_status_plan[$i]->fecha_revisado != '0000-00-00')
					
					<td class="text-center" style="padding: 5px">{{substr($periodos_status_plan[$i]->fecha_revisado, 8) * 1}}/{{$meses[substr($periodos_status_plan[$i]->fecha_revisado, 5, 2) * 1]}}/{{substr($periodos_status_plan[$i]->fecha_revisado, 0, 4)}}</td>
					@else
						@if (!empty($periodos_status_plan[$i]->fecha_registrado) && $periodos_status_plan[$i]->fecha_registrado != '0000-00-00' && $status_plan->status == 4)

					<td class="text-center" style="padding: 5px">En<br>Revision</td>
						@else

					<td></td>
						@endif
					@endif
				@endif
			@endfor

			@for ($i = $num_meses;$i <= 12;$i++)

					<td></td>
		  @endfor

				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-12 text-center">

@if (!empty($status_plan) && $status_plan->status == 4)
	@if ((!empty($user->boss) && $user->boss->id == auth()->user()->employee_id && $plan->autorizar_logros == 2) || ($plan->autorizar_logros == 4 && !empty(auth()->user()->employee->grado)) || auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin'))
		<div class="margin-top-20">
			<form action="revisar" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_plan" value="{{$plan->id}}">
				<input type="hidden" name="id_empleado" value="{{$id_user}}">
				<input type="hidden" name="periodo" value="{{$status_plan->periodo}}">
				<input type="hidden" name="status" value="5">
				<button class="btn btn-primary" type="submit">Revisión</button>
			</form>
		</div>
		<div class="margin-top-20">
			<form action="revisar" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_plan" value="{{$plan->id}}">
				<input type="hidden" name="id_empleado" value="{{$id_user}}">
				<input type="hidden" name="periodo" value="{{$status_plan->periodo}}">
				<input type="hidden" name="status" value="3">
				<button class="btn btn-primary" type="submit">Rechazar</button>
			</form>
		</div>
	@endif
@else

	@if (!empty($status_plan) && !empty($status_plan->fecha_revisado) && $status_plan->fecha_revisado != '0000-00-00')
		<div class="mt-3">
			<p>Fecha de Revisión</p>
			<p>{{substr($status_plan->fecha_revisado, 8)}} de {{$meses_completos[substr($status_plan->fecha_revisado, 5, 2) * 1]}} del {{substr($status_plan->fecha_revisado, 0, 4)}}</p>
		</div>
	@endif
@endif

	</div>
</div>
<hr style="border-color: #A5A7A8">
<div class="row margin-top-20">
	<div class="col-md-12">
		<div style="max-height: 200px; overflow-y: auto">
			<h3 style="margin-top: 0">Observaciones</h3>
			<table width="100%" style="font-size: 13px" border="1" class="notas table-bordered table-striped table-hover">
				<thead style="background-color: #222B64; color:white;">
					<tr>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Fecha</th>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Persona</th>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Nota</th>
					</tr>
				</thead>
				<tbody>

      	@for ($i = 0;$i < count($notas); $i++) 

					<tr>
						<td class="text-center" style="padding: 5px; border: 0">{{substr($notas[$i]->fecha, 8) * 1}}/{{$meses[substr($notas[$i]->fecha, 5, 2) * 1]}}/{{substr($notas[$i]->fecha, 0, 4)}}</td>
						<td class="text-center" style="padding: 5px; border: 0">{{$notas[$i]->nombre}} {{$notas[$i]->paterno}} {{$notas[$i]->materno}}</td>
						<td class="text-center" style="padding: 5px; border: 0">{{$notas[$i]->mensaje}}</td>
					</tr>
				@endfor

				</tbody>
			</table>
		</div>
		<h4 class="titulos-evaluaciones">Nota</h4>
		<div>
			<textarea style="width: 100%; height: 100px; border-radius: 7px"></textarea>
		</div>
	
		<div class="text-right margin-top-10">
			<button class="btn btn-primary agregar_nota"><i class="fas fa-plus-circle"></i> Agregar nota</button>
		</div>
	</div>
</div>
@else
				
<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="titulos-evaluaciones">No hay planes abiertos</h4>
	</div>
</div>
@endif
</div>
</div>
@endsection

@section('scripts')
	<script>

	  var id_plan = <?php echo (!empty($plan->id) ? $plan->id : 0)?>;
	  var id_empleado = <?php echo (!empty($id_user) ? $id_user : 0)?>;
		
		$(document).ready(function(){

			$.ajaxSetup({
    		headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
			});
				
			/*$('.tabla_logros').DataTable({
					scrollX: true,
    				scrollCollapse: true,
    				paging: false,
    				searching: false,
    				ordering: false,
    				info: false,
					//"sScrollX": "100%",
   					fixedColumns:   {
            			leftColumns: 5
        			}
				});*/
					$('.tabla_logros').DataTable({
					scrollX: true,
					scrollCollapse: true,
    				paging: false,
    				searching: false,
					ordering: false,
    				info: false,
    				fixedColumns:   {
          		leftColumns: 2
       			}
				});

			$('select.id_plan').change(function(){

				$('form#changePlanForm').submit();
			});

      $('button.agregar_nota').click(function(){

				var nota = $('textarea').val();

				$.ajax({
        	type:'POST',    
        	url: 'agregar-nota',
          data:{
          	mensaje: nota,
          	id_empleado: id_empleado,
          	id_plan: id_plan
          },
          success: function(data){
             
            var meses = [];
            meses[0] = 'Ene';
            meses[1] = 'Feb';
            meses[2] = 'Mar';
            meses[3] = 'Abr';
            meses[4] = 'May';
            meses[5] = 'Jun';
            meses[6] = 'Jul';
            meses[7] = 'Ago';
            meses[8] = 'Sep';
            meses[9] = 'Oct';
            meses[10] = 'Nov';
            meses[11] = 'Dic';
            var fecha = new Date();
            $('table.notas').prepend('<tr><td class="text-center" style="padding: 5px; border: 0">' + fecha.getDate() + '/' + meses[fecha.getMonth()] + '/' + fecha.getFullYear() + '</td><td class="text-center" style="padding: 5px; border: 0">' + data + '</td><td class="text-center" style="padding: 5px; border: 0">' + nota + '</td></tr>');
            $('textarea').val('');
          }
        });
			});

			$('input.porcentaje').inputmask("numeric", {
    		radixPoint: ".",
    		groupSeparator: ",",
    		digits: 2,
    		autoGroup: true,
    		suffix: '%', //No Space, this will truncate the first character
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});

			$('input.currency').inputmask("numeric", {
    		radixPoint: ".",
    		groupSeparator: ",",
    		digits: 2,
    		autoGroup: true,
    		prefix: '$', //No Space, this will truncate the first character
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});

			$('input.numeric').inputmask("numeric", {
    		groupSeparator: ",",
    		autoGroup: true,
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});
		});
	</script>
@endsection