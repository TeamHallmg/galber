<div class="sub-menu">
	<?php function activeSubMenu($url){
      return request()->is($url) ? 'active' : '';
    }?>
	<h4 class="font-weight-bold">Evaluación Cuantitativa</h4>
	<hr>
	<ul class="nav nav-pills flex-column">
		<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('que-es-evaluacion-resultados') }}" href="{{ url('que-es-evaluacion-resultados') }}">¿Qué es?</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('propuesta') }}" href="{{ url('propuesta') }}">Mis Objetivos</a>
		</li>
		<!--<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('evaluacion-resultados/responsabilidades-y-compromisos') }}" href="{{ url('evaluacion-resultados/responsabilidades-y-compromisos') }}">Responsabilidades y Compromisos</a>
		</li>-->

<?php if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){ ?>

		<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('reportes') }}" href="{{ url('reportes') }}">Reportes</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('reporte-resultados-mensuales') }}" href="{{ url('evaluacion-resultados/reporte-resultados-mensuales') }}">Resultados Mensuales</a>
		</li>
<?php } ?>

		<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('registro-logros') }}" href="{{ url('registro-logros') }}">Mis Resultados</a>
		</li>
		<!--<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('evaluacion-resultados/cumplimiento-de-compromisos') }}" href="{{ url('evaluacion-resultados/cumplimiento-de-compromisos') }}">Cumplimiento de Compromisos</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('evaluacion-resultados/iniciativas') }}" href="{{ url('/evaluacion-resultados/iniciativas') }}">Iniciativas</a>
		</li>-->
		<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('estadistica-avance') }}" href="{{ url('estadistica-avance') }}">Estadística de Avance</a>
		</li>
		<!--<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('bonos') }}" href="{{ url('bonos') }}">Bonos</a>
		</li>-->

<?php if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')){ ?>


	<li class="nav-item"><h4 class="font-weight-bold mt-4  d-flex">Administrador 	
		<a class=" ml-1" data-toggle="collapse" href="#collapseVacationMenu" role="button" aria-expanded="false" aria-controls="collapseVacationMenu"><i class="fas fa-cog"></i></a>
	</h4><hr></li>

		<!--<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('importar-objetivos') }}" href="{{ url('importar-objetivos') }}">Importar Objetivos</a>
		</li>-->
		<div class="collapse {{ is_url_of_hidden_menu(Route::currentRouteName())?'show':'' }}" id="collapseVacationMenu">
 
		<!--<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('importar-objetivos') }}" href="{{ url('importar-objetivos') }}">Importar Objetivos</a>
		</li>-->
		<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('evaluacion-resultados/catalogo-objetivos') }}" href="{{ url('evaluacion-resultados/catalogo-objetivos') }}">Catálogo de Objetivos</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('evaluacion-resultados/tipos-objetivos-corporativos') }}" href="{{ url('evaluacion-resultados/tipos-objetivos-corporativos') }}">Tipos de Objetivos Corporativos</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('evaluacion-resultados/objetivos-corporativos') }}" href="{{ url('evaluacion-resultados/objetivos-corporativos') }}">Objetivos Corporativos</a>
		</li>
		<!--<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('evaluacion-resultados/reporte-semaforizado') }}" href="{{ url('evaluacion-resultados/reporte-semaforizado') }}">Reporte Semaforizado</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('evaluacion-resultados/reporte-objetivo-corporativo/*/*') }}" href="{{ url('/evaluacion-resultados/reporte-objetivo-corporativo/0/0') }}">Reporte Objetivo Corporativo</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('evaluacion-resultados/reporte-objetivo-responsabilidad/*/*') }}" href="{{ url('/evaluacion-resultados/reporte-objetivo-responsabilidad/0/0') }}">Reporte Objetivo Responsabilidad</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('evaluacion-resultados/users_groups') }} {{ activeSubMenu('evaluacion-resultados/users_groups/create') }} {{ activeSubMenu('users_groups/create') }} {{ activeSubMenu('evaluacion-resultados/users_groups/*/edit') }}" href="{{ url('evaluacion-resultados/users_groups') }}">Grupos de usuarios</a>
		</li>-->
		<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('planes') }} {{ activeSubMenu('crear-plan') }} {{ activeSubMenu('editar-plan/*') }}" href="{{ url('/evaluacion-resultados/planes') }}">Planes</a>
		</li>
		<!--<li class="nav-item">
			<a class="nav-link {{ activeSubMenu('ejes-estrategicos') }} {{ activeSubMenu('crear-eje-estrategico') }} {{ activeSubMenu('editar-eje-estrategico/*') }}" href="{{ url('ejes-estrategicos') }}">Ejes Estratégicos</a>
		</li>-->
		</div>
<?php } ?>
	</ul>
</div>

@if (auth()->user()->isSuperAdmin())

  <span class="font-weight-bold font-sepanka font-version-sepanka h4">20241218.YARM</span>
@endif