<!-- Hidden Elements for Map Points Description -->
<div class="info-window hidden">
	<ul>
		<li>
			<div class="col-md-5">
				<img src="{{ asset('img/nuestra-empresa/nuestras-sedes/sedes/oficinas-corporativas.png') }}" alt="photo">
			</div>
			<div class="col-md-7 text-left">
				<p><a class="address" data-id="0" href="#"><strong>OFICINAS CORPORATIVAS</strong></a></p>
				<div class="address-block">
					<p class="text-blue"><strong>Ubicación:</strong></p>
					<div class="text-gray font-14">
						<p class="font-14">Av. 8 de Julio No. 2270</p>
						<p class="font-14">Zona Industrial</p>
						<p class="font-14">C.P. 44940</p>
						<p class="font-14">Guadalajara, Jalisco</p>
					</div>
					<br>
					<p class="text-blue"><strong>Teléfonos de Contacto:</strong></p>
					<div class="text-gray">
						<p class="font-14">(33) 3134-0500</p>
					</div>
				</div>
				<hr>
				<p><a class="address" data-id="0" href="#"><strong>OFICINAS	REGIÓN OCCIDENTE</strong></a></p>
				<div class="address-block">
					<p class="text-blue"><strong>Ubicación:</strong></p>
					<div class="text-gray">
						<p class="font-14">Av. 8 de Julio No. 2270</p>
						<p class="font-14">Zona Industrial</p>
						<p class="font-14">C.P. 44940</p>
						<p class="font-14">Guadalajara, Jalisco</p>
					</div>
					<br>
					<p class="text-blue"><strong>Territorio que atiende:</strong></p>
					<div class="text-gray">
						<p class="font-14">Nayarit</p>
						<p class="font-14">Aguascalientes</p>
						<p class="font-14">San Luis Potosí</p>
						<p class="font-14">Colima</p>
						<p class="font-14">Michoacán</p>
						<p class="font-14">Guanajuato</p>
						<p class="font-14">Querétaro</p>
						<p class="font-14">Jalisco</p>
					</div>
					<br>
					<p class="text-blue"><strong>Teléfonos de Contacto:</strong></p>
					<div class="text-gray">
						<p class="font-14">(33) 3134-0500 ext. 581</p>
					</div>
				</div>
			</div>
		</li>
		<li>
			<div class="col-md-5">
				<img src="{{ asset('img/nuestra-empresa/nuestras-sedes/sedes/lagos-de-moreno.png') }}" alt="photo">
			</div>
			<div class="col-md-7 text-left">
				<p><a class="address" data-id="1" href="#"><strong>PLANTA LAGOS DE MORENO</strong></a></p>
			<div class="address-block">
				<p class="text-blue"><strong>Ubicación:</strong></p>
				<div class="text-gray">
					<p class="font-14">Lagos de Moreno, Jalisco</p>
				</div>
				<br>
				<p class="text-blue"><strong>Presentaciones que fabrican:</strong></p>
				<div class="text-gray">
					<p class="font-14">Cartucho</p>
					<p class="font-14">Garrafa</p>
					<p class="font-14">Botella</p>
					<p class="font-14">Cubeta</p>
					<p class="font-14">Minigranel</p>
					<p class="font-14">Galón</p>
					<p class="font-14">Tambor</p>
					<p class="font-14">Granel</p>
				</div>
				<br>
				<p class="text-blue"><strong>Certificaciones:</strong></p>
				<div class="text-gray">
					<p class="font-14">API (American Petroleum Institute)</p>
					<p class="font-14">ILSAC (International Lubricant Standardization and Approbal Comitee)</p>
					<p class="font-14">NMMA (National Marine Manufacturers Association)</p>
					<p class="font-14">Mercedes Benz, Ford, Volvo, General Motors.</p>
					<p class="font-14">Cummins, John Deere México, Cincinnati Machine, Vickers y Allison Transmisión, EMD de General Motors y General Electric.</p>
					<p class="font-14">ISO- 9001</p>
					<p class="font-14">ISO-14000</p>
					<p class="font-14">OSHAS-18000. </p>
				</div>
				<br>
				<p class="text-blue"><strong>Zona de Influencia:</strong></p>
				<div class="text-gray">
					<p class="font-14">Aguascalientes</p>
					<p class="font-14">B.C. Norte</p>
					<p class="font-14">B.C. Sur</p>
					<p class="font-14">Coahuila</p>
					<p class="font-14">Colima</p>
					<p class="font-14">Chihuahua</p>
					<p class="font-14">Durango</p>
					<p class="font-14">Guanajuato</p>
					<p class="font-14">Jalisco</p>
					<p class="font-14">Michoacán</p>
					<p class="font-14">Nayarit</p>
					<p class="font-14">Nuevo León</p>
					<p class="font-14">Querétaro</p>
					<p class="font-14">San Luis Potosí</p>
					<p class="font-14">Sinaloa</p>
					<p class="font-14">Sonora</p>
					<p class="font-14">Tamaulipas</p>
					<p class="font-14">Zacatecas</p>
				</div>
				<br>
				<p class="text-blue"><strong>Exportaciones:</strong></p>
				<div class="text-gray">
					<p class="font-14">Bolivia</p>
					<p class="font-14">Colombia</p>
					<p class="font-14">Costa rica</p>
					<p class="font-14">Ecuador</p>
					<p class="font-14">El salvador</p>
					<p class="font-14">Guatemala</p>
					<p class="font-14">Honduras</p>
					<p class="font-14">Jamaica</p>
					<p class="font-14">Nicaragua</p>
					<p class="font-14">Panamá</p>
					<p class="font-14">Perú</p>
				</div>
				<br>
				<p class="text-blue"><strong>Teléfonos de Contacto:</strong></p>
				<div class="text-gray">
					<p class="font-14">(47) 4742-5511</p>
				</div>
			</div>
			</div>
		</li>
		<li>
			<div class="col-md-5">
				<img src="{{ asset('img/nuestra-empresa/nuestras-sedes/sedes/cedis-df.png') }}" alt="photo">
			</div>
			<div class="col-md-7 text-left">
				<p><a class="address" data-id="2" href="#"><strong>CEDIS DF</strong></a></p>
				<div class="address-block">
					<p class="text-blue"><strong>Ubicación:</strong></p>
					<div class="text-gray">
						<p class="font-14">Vía José López Portillo No. 10</p>
						<p class="font-14">Col. San Francisco Chilpan</p>
						<p class="font-14">C.P. 54900</p>
						<p class="font-14">Tultitlán, Estado de México.</p>
					</div>
					<br>
					<p class="text-blue"><strong>Territorio que atiende:</strong></p>
					<div class="text-gray">
						<p class="font-14">Parte del territorio del</p>
						<p class="font-14">Distrito Federal y algunas zonas</p>
						<p class="font-14">del Estado de México.</p>
					</div>
					<br>
					<p class="text-blue"><strong>Teléfonos de Contacto:</strong></p>
					<div class="text-gray">
						<p class="font-14">(55) 5864-3139</p>
					</div>
				</div>
				<hr>
				<p><a class="address" data-id="8" href="#"><strong>OFICINAS	REGIÓN CENTRO</strong></a></p>
				<div class="address-block">
					<p class="text-blue"><strong>Ubicación:</strong></p>
					<div class="text-gray">
						<p class="font-14">Vía José López Portillo No. 10</p>
						<p class="font-14">Col. San Francisco Chilpan</p>
						<p class="font-14">C.P. 54900</p>
						<p class="font-14">Tultitlán, Estado de México.</p>
					</div>
					<br>
					<p class="text-blue"><strong>Territorio que atiende:</strong></p>
					<div class="text-gray">
						<p class="font-14">Hidalgo</p>
						<p class="font-14">Estado de México</p>
						<p class="font-14">D.F.</p>
						<p class="font-14">Morelos</p>
						<p class="font-14">Guerrero</p>
					</div>
					<br>
					<p class="text-blue"><strong>Teléfonos de Contacto:</strong></p>
					<div class="text-gray">
						<p class="font-14">(55) 5864-3139</p>
					</div>
				</div>
			</div>
		</li>
		<li>
			<div class="col-md-5">
				<img src="{{ asset('img/nuestra-empresa/nuestras-sedes/sedes/tijuana.png') }}" alt="photo">
			</div>
			<div class="col-md-7 text-left">
				<p><a class="address" data-id="3" href="#"><strong>CEDIS TIJUANA</strong></a></p>
				<div class="address-block">
					<p class="text-blue"><strong>Ubicación:</strong></p>
					<div class="text-gray">
						<p class="font-14">Andador del rey # 20051 planta 7b</p>
						<p class="font-14">Col. Rancho El Águila.</p>
						<p class="font-14">C.P. 22215</p>
						<p class="font-14">Tijuana, Baja California.</p>
					</div>
					<br>
					<p class="text-blue"><strong>Territorio que atiende:</strong></p>
					<p class="font-14">Tecate, Mexicalli, San Luis Río Colorado,</p>
					<p class="font-14">Rosarito, Ensenada y San Quintin.</p>
					<p class="font-14">Cuando el Distribuidor Local</p>
					<p class="font-14">necesita apoyo cubrimos San Felipe,</p>
					<p class="font-14">BC y Puerto Peñasco, Sonora.</p>
					<br>
					<p class="text-blue"><strong>Teléfonos de Contacto:</strong></p>
					<div class="text-gray">
						<p class="font-14">(66) 4680-5599</p>
					</div>
				</div>
			</div>
		</li>
		<li>
			<div class="col-md-5">
				<img src="{{ asset('img/nuestra-empresa/nuestras-sedes/sedes/cedis-df.png') }}" alt="photo">
			</div>
			<div class="col-md-7 text-left">
				<p><a class="address" data-id="4" href="#"><strong>CEDIS VILLAHERMOSA</strong></a></p>
				<div class="address-block">
					<p class="text-blue"><strong>Ubicación:</strong></p>
					<div class="text-gray">
						<p class="font-14">Carretera Villahermosa-Cárdenas Km.05</p>
						<p class="font-14">Bodega 3B</p>
						<p class="font-14">Ranchería Anacleto Canabal.</p>
						<p class="font-14">C.P. 86860</p>
						<p class="font-14">Villahermosa, Tabasco</p>
					</div>
					<br>
					<p class="text-blue"><strong>Territorio que atiende:</strong></p>
					<p class="font-14">Todo el estado de Tabasco.</p>
					<p class="font-14">Del estado de Veracruz se cubre</p>
					<p class="font-14">Coatzacoalcos.</p>
					<p class="font-14">Del estado de Chiapas se cubre</p>
					<p class="font-14">Palenque y Reforma.</p>
					<br>
					<p class="text-blue"><strong>Teléfonos de Contacto:</strong></p>
					<div class="text-gray">
						<p class="font-14">(99) 3399-1580</p>
					</div>
				</div>
			</div>	
		</li>
		<li>
		</li>
		<li>
			<div class="col-md-5">
				<img src="{{ asset('img/nuestra-empresa/nuestras-sedes/sedes/cedis-df.png') }}" alt="photo">
			</div>
			<div class="col-md-7 text-left">
				<p><a class="address" data-id="6" href="#"><strong>OFICINAS REGIÓN PACIFICO</strong></a></p>
				<div class="address-block">
					<p class="text-blue"><strong>Ubicación:</strong></p>
					<div class="text-gray">
						<p class="font-14">Ignacio López Rayón No. 2300 Norte</p>
						<p class="font-14">Fracc. Parque Residencial</p>
						<p class="font-14">Alamedas Proyecto Tres Ríos</p>
						<p class="font-14">C.P. 80030</p>
						<p class="font-14">Culiacán, Sinaloa</p>
					</div>
					<br>
					<p class="text-blue"><strong>Territorio que atiende:</strong></p>
					<p class="font-14">Baja California</p>
					<p class="font-14">Baja California Sur</p>
					<p class="font-14">Sonora</p>
					<p class="font-14">Chihuahua</p>
					<p class="font-14">Sinaloa</p>
					<br>
					<p class="text-blue"><strong>Teléfonos de Contacto:</strong></p>
					<div class="text-gray">
						<p class="font-14">(66) 7146-6440 y (66) 7146-6439</p>
					</div>
				</div>
			</div>	
		</li>
		<li>
			<div class="col-md-5">
				<img src="{{ asset('img/nuestra-empresa/nuestras-sedes/sedes/cedis-df.png') }}" alt="photo">
			</div>
			<div class="col-md-7 text-left">
				<p><a class="address" data-id="7" href="#"><strong>OFICINAS REGIÓN NORTE</strong></a></p>
				<div class="address-block">
					<p class="text-blue"><strong>Ubicación:</strong></p>
					<div class="text-gray">
						<p class="font-14">Primera Avenida No. 435</p>
						<p class="font-14">Col. Cumbre 1era. Sección.</p>
						<p class="font-14">C.P. 64620</p>
						<p class="font-14">Monterrey, Nuevo León.</p>
					</div>
					<br>
					<p class="text-blue"><strong>Territorio que atiende:</strong></p>
					<p class="font-14">Coahuila</p>
					<p class="font-14">Nuevo León</p>
					<p class="font-14">Tamaulipas</p>
					<p class="font-14">Durango</p>
					<p class="font-14">Zacatecas</p>
					<br>
					<p class="text-blue"><strong>Teléfonos de Contacto:</strong></p>
					<div class="text-gray">
						<p class="font-14">(81) 8345-0732</p>
					</div>
				</div>
			</div>	
		</li>
		<li>
		</li>
		<li>
			<div class="col-md-5">
				<img src="{{ asset('img/nuestra-empresa/nuestras-sedes/sedes/cedis-df.png') }}" alt="photo">
			</div>
			<div class="col-md-7 text-left">
				<p><a class="address" data-id="9" href="#"><strong>OFICINAS REGIÓN SUR</strong></a></p>
				<div class="address-block">
					<p class="text-blue"><strong>Ubicación:</strong></p>
					<div class="text-gray">
						<p class="font-14">Av. La Fragua No. 1114-B</p>
						<p class="font-14">Fracc. Moderno</p>
						<p class="font-14">C.P. 91918</p>
						<p class="font-14">Veracruz, Veracruz</p>
					</div>
					<br>
					<p class="text-blue"><strong>Territorio que atiende:</strong></p>
					<p class="font-14">Tlaxcala</p>
					<p class="font-14">Puebla</p>
					<p class="font-14">Veracruz</p>
					<p class="font-14">Oaxaca</p>
					<p class="font-14">Tabasco</p>
					<p class="font-14">Chiapas</p>
					<p class="font-14">Campeche</p>
					<p class="font-14">Quintana Roo</p>
					<p class="font-14">Yucatán</p>
					<br>
					<p class="text-blue"><strong>Teléfonos de Contacto:</strong></p>
					<div class="text-gray">
						<p class="font-14">(22) 9937-9321</p>
					</div>
				</div>
			</div>	
		</li>
		<li>
			<div class="col-md-5">
				<img src="{{ asset('img/nuestra-empresa/nuestras-sedes/sedes/cedis-df.png') }}" alt="photo">
			</div>
			<div class="col-md-7 text-left">
				<p><a class="address" data-id="3" href="#"><strong>COSTA RICA</strong></a></p>
				<div class="address-block">
					<p class="text-blue"><strong>Ubicación:</strong></p>
					<div class="text-gray">
						<p class="font-14">San José, Costa Rica</p>
					</div>
					<br>
					<p class="text-blue"><strong>Territorio que atiende:</strong></p>
					<p class="font-14">Costa Rica y Nicaragua</p>
					<br>
					<p class="text-blue"><strong>Teléfonos de Contacto:</strong></p>
					<div class="text-gray">
						<p class="font-14">Julio Iván Lara González</p>
						<p class="font-14">00(506) 8721 2184</p>
						<p class="text-blue font-14"><u>j.lara@akron.com.mx</u></p>
					</div>
				</div>
			</div>
		</li>
		<li>
			<div class="col-md-5">
				<img src="{{ asset('img/nuestra-empresa/nuestras-sedes/sedes/cedis-df.png') }}" alt="photo">
			</div>
			<div class="col-md-7 text-left">
				<p><a class="address" data-id="6" href="#"><strong>BOLIVIA</strong></a></p>
			<div class="address-block">
				<p class="text-blue"><strong>Ubicación:</strong></p>
				<div class="text-gray">
					<p class="font-14">Cochabamba, Bolivia</p>
				</div>
				<br>
				<p class="text-blue"><strong>Territorio que atiende:</strong></p>
				<p class="font-14">Bolivia y Perú</p>
				<br>
				<p class="text-blue"><strong>Teléfonos de Contacto:</strong></p>
				<div class="text-gray">
					<p class="font-14">Jorge León Castillo</p>
					<p class="font-14">Tel: 00(591) 6753 4772</p>
					<p class="text-blue font-14"><u>j.leon@akron.com.mx</u></p>
				</div>
			</div>
			</div>
		</li>
		<li>
			<div class="col-md-5">
				<img src="{{ asset('img/nuestra-empresa/nuestras-sedes/sedes/cedis-df.png') }}" alt="photo">
			</div>
			<div class="col-md-7 text-left">
				<p><a class="address" data-id="4" href="#"><strong>COLOMBIA</strong></a></p>
			<div class="address-block">
				<p class="text-blue"><strong>Ubicación:</strong></p>
				<div class="text-gray">
					<p class="font-14">Bogotá, Colombia</p>
				</div>
				<br>
				<p class="text-blue"><strong>Territorio que atiende:</strong></p>
				<p class="font-14">Colombiay Ecuador</p>
				<br>
				<p class="text-blue"><strong>Teléfonos de Contacto:</strong></p>
				<div class="text-gray">
					<p class="font-14">Ricardo Álvarez López</p>
					<p class="font-14">Tel: 00(57) 318 712 4044</p>
					<p class="text-blue font-14"><u>r.alvarez@akron.com.mx</u></p>
				</div>
			</div>
			</div>
		</li>
		<li>
			<div class="col-md-5">
				<img src="{{ asset('img/nuestra-empresa/nuestras-sedes/sedes/cedis-df.png') }}" alt="photo">
			</div>
			<div class="col-md-7 text-left">
				<p><a class="address" data-id="4" href="#"><strong>GUATEMALA</strong></a></p>
				<div class="address-block">
					<p class="text-blue"><strong>Ubicación:</strong></p>
					<div class="text-gray">
						<p class="font-14">Guatemala, Guatemala</p>
					</div>
					<br>
					<p class="text-blue"><strong>Territorio que atiende:</strong></p>
					<div class="text-gray">
						<p class="font-14">Guatemala, El Salvador y Honduras</p>
					</div>
					<br>
					<p class="text-blue"><strong>Teléfonos de Contacto:</strong></p>
					<div class="text-gray">
						<p class="font-14">Javier Lira</p>
						<p class="font-14">Tel: 00(502) 3022 2676</p>
						<p class="text-blue font-14"><u>j.lira@akron.com.mx</u></p>
					</div>
				</div>
			</div>
		</li>
		<li>
			<div class="col-md-5">
				<img src="{{ asset('img/nuestra-empresa/nuestras-sedes/sedes/cedis-df.png') }}" alt="photo">
			</div>
			<div class="col-md-7">
				<p><a class="address" data-id="14" href="#"><strong>PLANTA TULTITLÁN</strong></a></p>
				<div class="address-block">
					<p class="text-blue"><strong>Ubicación:</strong></p>
					<div class="text-gray">
						<p class="font-14">Tultitlán, Estado de México</p>
					</div>
					<br>
					<p class="text-blue"><strong>Presentaciones que fabrican:</strong></p>
					<div class="text-gray">
						<p class="font-14">Garrafa</p>
						<p class="font-14">Botella</p>
						<p class="font-14">Cubeta</p>
						<p class="font-14">Minigranel</p>
						<p class="font-14">Galón</p>
						<p class="font-14">Tambor</p>
						<p class="font-14">Granel</p>
					</div>
					<br>
					<p class="text-blue"><strong>Certificaciones:</strong></p>
					<div class="text-gray">
						<p class="font-14">ISO 9000 desde el año de1997</p>
						<p class="font-14">LAPEM / CFE desde el año de1997</p>
						<p class="font-14">EMA desde el año 1999</p>
						<p class="font-14">Industria Limpia por 8 años consecutivos desde 2007</p>
						<p class="font-14">ISO 14001:2004 desde el año de 2011</p>
						<p class="font-14">OSHAS-18001 desde 2015</p>
					</div>
					<br>
					<p class="text-blue"><strong>Zona de Influencia:</strong></p>
					<div class="text-gray">
						<p class="font-14">Estado de México y D.F.</p>
						<p class="font-14">Guerrero</p>
						<p class="font-14">Hidalgo</p>
						<p class="font-14">Tlaxcala</p>
						<p class="font-14">Puebla</p>
						<p class="font-14">Oaxaca</p>
						<p class="font-14">Veracruz</p>
						<p class="font-14">Tabasco</p>
						<p class="font-14">Chiapas</p>
						<p class="font-14">Campeche</p>
						<p class="font-14">Yucatán</p>
						<p class="font-14">Quintana Roo</p>
						<p class="font-14">Morelos</p>
					</div>
					<br>
					<p class="text-blue"><strong>Exportaciones:</strong></p>
					<div class="text-gray">
						<p class="font-14">Bolivia</p>
						<p class="font-14">Colombia</p>
						<p class="font-14">Costa rica</p>
						<p class="font-14">Ecuador</p>
						<p class="font-14">El salvador</p>
						<p class="font-14">Guatemala</p>
						<p class="font-14">Honduras</p>
						<p class="font-14">Jamaica</p>
						<p class="font-14">Nicaragua</p>
						<p class="font-14">Panamá</p>
						<p class="font-14">Perú</p>
					</div>
					<br>
					<p class="text-blue"><strong>Teléfonos de Contacto:</strong></p>
					<div class="text-gray">
						<p class="font-14">(47) 4742-5511</p>
					</div>
				</div>
			</div>
		</li>
	</ul>
</div>