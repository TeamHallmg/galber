@extends('layouts.app')

@section('title', 'Estadística de Avance')

@section('content')

<style>div.dt-buttons {
	float: right;
	margin-left:10px;
	display: none;
	}</style>
<div class="row">
 
	<div class="col-md-2 sub_menu">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10 content-principal">
		
		<img class="img-fluid" src="/img/banner_evaluacion_resultados.png" alt="">
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Estadística de Avance</h5>
			<div class="card-body">
				
				@if (!empty($planes))

					<div class="row"> 
						<div class="col-12 offset-md-4 col-md-3 text-center">
							<div class="form-group">
								<label for="id_plan" class="font-weight-bolder mb-0">Plan:</label>							
								<form id="changePlanForm" action="estadistica-avance" method="post">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<select name="id_plan" id="id_plan" class="form-control id_plan">
									@for ($i = 0;$i < count($planes);$i++)

									<option value="{{$planes[$i]->id}}" <?php if ($plan->id == $planes[$i]->id){ ?>selected="selected"<?php } ?>>{{$planes[$i]->nombre}}</option>
							@endfor

								</select>
								<input type="submit" style="display: none">
								</form>
							</div>
						</div> 
					</div>

					<hr>


					@if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin'))
					
					
						<input type="hidden" id="objetivos_autorizados" value="{{number_format(($num_users - $objetivos_autorizados) * 100 / $num_users, 1)}}">
						<input type="hidden" id="name_objetivos_autorizados" value="{{$num_users - $objetivos_autorizados}}/{{$num_users}}">

						<input type="hidden" id="non_objetivos_autorizados" value="{{number_format($objetivos_autorizados * 100 / $num_users, 1)}}">
						<input type="hidden" id="name_non_objetivos_autorizados" value="{{$objetivos_autorizados}}/{{$num_users}}">

						<input type="hidden" id="logos_autorizados" value="{{number_format($logros_revisados * 100 / $num_users, 1)}}">
						<input type="hidden" id="name_logos_autorizados" value="{{$logros_revisados}} / {{$num_users}}">
						<input type="hidden" id="name_non_logos_autorizados" value="{{$num_users - $logros_revisados}} / {{$num_users}}">

				<div class="row">
					<div class="col-md-6">
					
						<div class="card">
							<h5 class="card-header bg-primary text-white font-weight-bolder">Personas con Objetivos</h5>
							<div class="card-body">
								
								<div id="personasconobjetivos" style="min-width: 260px; height: 260px; margin: 0 auto"></div>

							</div>
						</div>

					</div>
					<div class="col-md-6">
					
						<div class="card">
							<h5 class="card-header bg-primary text-white font-weight-bolder">Logros Revisados</h5>
							<div class="card-body">
								
								<div id="logrosrevisados" style="min-width: 260px; height: 260px; margin: 0 auto"></div>

							</div>
						</div>

					</div>
				</div>
					@endif
				
				<hr>

					@if (!empty($users))

						@if ($objetivos_autorizados > 0 && $plan->registrar_logros_abierto == 1 && auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin'))

				<div class="row">
					<div class="col-md-5 my-3 text-center">
						<form action="/exportar-plantilla-resultados" method="post" class="exportar_resultados">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="id_plan" value="{{$plan->id}}">
							<input type="hidden" name="ids" class="ids">
							<button type="submit" class="btn btn-primary font-weight-bold text-white">Exportar Plantilla de Resultados</button>
						</form>
					</div>
					<div class="col-md-7 my-3 text-center">
						<form action="/importar-plantilla-resultados" method="post" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="file" class="mb-3" name="file" required style="display: inline-block">
							<button type="submit" class="btn btn-primary font-weight-bold text-white">Importar Plantilla de Resultados</button>
						</form>
					</div>
				</div>
						@endif
					@endif

				<div class="card border-0">
					<h5 class="card-header bg-primary text-white font-weight-bolder">Detalles
 
					</h5>
					<div class="card-body px-0">
						
						
					@if (!empty($users))
 

							<table width="100%" style="font-size: 13px" class="data-table table table-striped table-bordered table-hover reportes dt-responsive">
								<thead style="background-color: #222B64; color:white;">
									<tr>
										<th class="cabeceras-evaluaciones" style="border: 1px solid white;">ID</th>
										<th class="cabeceras-evaluaciones" style="border: 1px solid white;">Persona</th>
										<th class="cabeceras-evaluaciones" style="border: 1px solid white;">Puesto</th>
										<th class="cabeceras-evaluaciones" style="border: 1px solid white;">Área</th>
										<th class="cabeceras-evaluaciones" style="text-align: center; border: 1px solid white;">Autorizado</th>
										<th class="cabeceras-evaluaciones nowrap" style="text-align: center; border: 1px solid white;">Última reunión</th>
										<th class="cabeceras-evaluaciones" style="text-align: center; border: 1px solid white;">Estatus</th>
										
								<?php //if ($plan->tipo == 2){ ?>
					
										<!--<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Cambiar Valores Pasados</th>-->
								<?php //} ?>
										
										<!--<th class="cabeceras-evaluaciones" style="text-align: center; border: 1px solid white;">Bono</th>-->
										<th style="text-align: center; border: 1px solid white; w" class="detalle cabeceras-evaluaciones">Detalle</th>
									</tr>
								</thead>
								<tbody>
					
									<?php $meses = array('0', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');?>
					
								@for ($i = 0;$i < count($users);$i++)
					
									<tr>
										<td style="padding: 10px 5px" class="text-center">{{$users[$i]->idempleado}}</td>
										<td style="padding: 10px 5px" class="text-left">{{$users[$i]->nombre}} {{$users[$i]->paterno}} {{$users[$i]->materno}}</td>
										<td style="padding: 10px 5px" class="text-left">{{(!empty($users[$i]->job_position) ? $users[$i]->job_position : '')}}</td>
										<td style="padding: 10px 5px" class="text-left">{{(!empty($users[$i]->department) ? $users[$i]->department : '')}}</td>
										<td class="text-center" style="padding: 10px 5px"><?php echo (!empty($users[$i]->fecha_autorizado) && $users[$i]->fecha_autorizado != '0000-00-00' ? substr($users[$i]->fecha_autorizado, 8) . '/' . $meses[substr($users[$i]->fecha_autorizado, 5, 2) * 1] . '/' . substr($users[$i]->fecha_autorizado, 0, 4) : '--/---/---')?></td>
										<td class="text-center" style="padding: 10px 5px"><?php echo (!empty($users[$i]->ultima_reunion) && $users[$i]->ultima_reunion != '0000-00-00' ? substr($users[$i]->ultima_reunion, 8) . '/' . $meses[substr($users[$i]->ultima_reunion, 5, 2) * 1] . '/' . substr($users[$i]->ultima_reunion, 0, 4) : '--/---/---')?></td>
										<td style="padding: 10px 5px"><?php echo (empty($users[$i]->status) || $users[$i]->status == 1 ? 'No Solicitado' : ($users[$i]->status == 2 ? 'Solicitado' : ($users[$i]->status == 3 ? 'Autorizado' : ($users[$i]->status == 4 ? 'Registrado' : (isset($director_revisions[$users[$i]->user_id]) ? 'Revisado por el Director' : 'Revisado')))))?></td>
									
								<?php //if ($plan->tipo == 2){ ?>
					
										<!--<td class="text-center" style="padding: 10px 5px">
											<span style="display: none" id="texto{{$users[$i]->id}}">{{(!empty($users[$i]->estado) && $users[$i]->estado == 1 ? 'Si' : 'No')}}</span>
											<input type="checkbox" class="valores_pasados {{$users[$i]->id}}" <?php //if (!empty($users[$i]->estado) && $users[$i]->estado == 1){ ?>checked="checked"<?php //} ?>>
										</td>-->
								<?php //} ?>
									
										<td class="text-center detalle" align="center" width="15%">
					
									@if ($alimentar_propuestas == 1)
					
										@if (!empty($users[$i]->fecha_solicitado) && $users[$i]->fecha_solicitado != '0000-00-00')
											<form method="post" action="autorizacion" style="display: inline-block">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<input type="hidden" name="id_empleado" value="{{$users[$i]->id}}">
												<input type="hidden" name="id_plan" value="{{$plan->id}}">
												<button class="btn btn-primary" type="submit"><i class="far fa-eye"></i> Ver Métricos</button>
											</form>
										@endif
					
										@if ((!empty($users[$i]->fecha_registrado) && $users[$i]->fecha_registrado != '0000-00-00') || (!empty($users[$i]->periodo) && $users[$i]->periodo > 1))
											<form method="post" action="revision-logros" style="display: inline-block">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<input type="hidden" name="id_empleado" value="{{$users[$i]->id}}">
												<input type="hidden" name="id_plan" value="{{$plan->id}}">
												<button class="btn btn-info" type="submit"><i class="far fa-eye"></i> Ver Resultados</button>
											</form>
										@endif
									@else
					
										@if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin'))
											
											@if ($users[$i]->id != auth()->user()->employee_id)
					
												@if (empty($users[$i]->fecha_solicitado) || $users[$i]->fecha_solicitado == '0000-00-00' || $users[$i]->status == 1)
					
											<form method="post" action="propuesta" style="display: inline-block">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<input type="hidden" name="id_empleado" value="{{$users[$i]->id}}">
												<input type="hidden" name="id_plan" value="{{$plan->id}}">
												<button class="btn btn-primary mb-1" type="submit">1. Ver Métricos</button>
											</form>
												@else
											
													@if (!empty($users[$i]->fecha_solicitado) && $users[$i]->fecha_solicitado != '0000-00-00')
					
											<form method="post" action="autorizacion" style="display: inline-block">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<input type="hidden" name="id_empleado" value="{{$users[$i]->id}}">
												<input type="hidden" name="id_plan" value="{{$plan->id}}">
												<button class="btn btn-primary mb-1" type="submit">1. Ver Métricos</button>
											</form>
													@endif
												@endif
											
												@if (!empty($users[$i]->fecha_autorizado) && $users[$i]->fecha_autorizado != '0000-00-00' && ($users[$i]->status == 3 || $users[$i]->status == 5) && ($plan->registrar_logros == 2 || $plan->registrar_logros == 3) && $plan->registrar_logros_abierto == 1 && $plan->estado == 2)
					
											<form method="post" action="registro-logros" style="display: inline-block">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<input type="hidden" name="id_empleado" value="{{$users[$i]->id}}">
												<input type="hidden" name="id_plan" value="{{$plan->id}}">
												<button class="btn btn-primary mb-1" type="submit">Ver Resultados</button>
											</form>
												@else
					
													@if ((!empty($users[$i]->fecha_registrado) && $users[$i]->fecha_registrado != '0000-00-00') || $users[$i]->status > 3)
											<form method="post" action="revision-logros" style="display: inline-block">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<input type="hidden" name="id_empleado" value="{{$users[$i]->id}}">
												<input type="hidden" name="id_plan" value="{{$plan->id}}">
												<button class="btn btn-primary mb-1" type="submit">Ver Resultados</button>
											</form>
													@endif
												@endif
											@endif
										@else

											@if (!empty($users[$i]->fecha_solicitado) && $users[$i]->fecha_solicitado != '0000-00-00' && $plan->autorizar_propuestas == 4 && !empty(auth()->user()->employee->grado) && !empty($users[$i]->status) && $users[$i]->status >= 2)

											<form method="post" action="autorizacion" style="display: inline-block">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<input type="hidden" name="id_empleado" value="{{$users[$i]->id}}">
												<input type="hidden" name="id_plan" value="{{$plan->id}}">
												<button class="btn btn-primary" type="submit"><i class="far fa-eye"></i> Ver Métricos</button>
											</form>
											@else

												@if (empty($users[$i]->status) || (!empty($users[$i]->status) && $users[$i]->status >= 1))
					
											<form method="post" action="propuesta" style="display: inline-block">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<input type="hidden" name="id_empleado" value="{{$users[$i]->id}}">
												<input type="hidden" name="id_plan" value="{{$plan->id}}">
												<button class="btn btn-primary mb-1" type="submit">1. Ver Métricos</button>
											</form>
												@endif
											@endif
					
											@if (!empty($users[$i]->status) && ($users[$i]->status == 3 || $users[$i]->status == 5) && $plan->registrar_logros == 2 && $plan->registrar_logros_abierto == 1)
											<form method="post" action="registro-logros" style="display: inline-block">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<input type="hidden" name="id_empleado" value="{{$users[$i]->id}}">
												<input type="hidden" name="id_plan" value="{{$plan->id}}">
												<button class="btn btn-primary mb-1" type="submit">Ver Resultados</button>
											</form>
											@else
											
												@if (!empty($users[$i]->status) && ($users[$i]->status == 4 || $users[$i]->periodo > 1))
											
											<form method="post" action="revision-logros" style="display: inline-block">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<input type="hidden" name="id_empleado" value="{{$users[$i]->id}}">
												<input type="hidden" name="id_plan" value="{{$plan->id}}">
												<button class="btn btn-primary mb-1" type="submit">Ver Resultados</button>
											</form>
												@endif
											@endif
										@endif
									@endif
					
										</td>
									</tr>
								@endfor
					
								</tbody>
							</table>
						
					@else

						<div class="row">
							
							<div class="col-md-12 text-center">
								
								<div class="alert alert-warning h4" role="alert">
									<i class="fa fa-exclamation-triangle"></i>	No tiene colaboradores
								</div>

 							</div>
						</div>

					@endif

					</div>
				</div>	

				@else

				<div class="row">
					
					<div class="col-md-12 text-center">
					
						<div class="alert alert-warning h4" role="alert">
							<i class="fa fa-exclamation-triangle"></i> No hay planes abiertos
						</div>

					</div>
				</div>

				@endif

			</div>
		  </div>
</div>
</div>
@endsection

@section('scripts')
	<script>
		
		$('.btn-excel').click(function(){
			alert('aasd');
			$('.buttons-excel').trigger('click');
		});

		var periodo = <?php echo (!empty($plan) ? $plan->periodo : 0)?>;
		var months = ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

		$(function() {
        // Create the chart 
		var objetivos_autorizados = parseFloat($('#objetivos_autorizados').val());
		var name_objetivos_autorizados = $('#name_objetivos_autorizados').val();

		var non_objetivos_autorizados = parseFloat($('#non_objetivos_autorizados').val());
		var name_non_objetivos_autorizados = $('#name_non_objetivos_autorizados').val();

		var logos_autorizados = parseFloat($('#logos_autorizados').val());
		var name_logos_autorizados = $('#name_logos_autorizados').val();
		var name_non_logos_autorizados = $('#name_non_logos_autorizados').val();
 
		// var users_approved_percentage = parseFloat($('#users_approved_percentage').val());
		// var users_non_approved_percentage = parseFloat($('#users_non_approved_percentage').val());
		
        chart = Highcharts.chart('personasconobjetivos', {
			chart: {
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false,
				type: 'pie'
			},
			credits: {
				enabled: false
			},
			title: {
				// text: '<b>80%</b>',
				// y:225
				text: null
			},
			legend:{
				enabled:true
			},
			tooltip: {
				pointFormat: '<b>{point.percentage:.1f}%</b>'
				// pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					allowPointSelect: false,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						formatter:function(){
							return this.y + '%';
						}
					},
					showInLegend: true
				}
			},
			series: [{
				name: 'Avance',
				colorByPoint: true,
				innerSize: '70%',
				data: [
					{
						name: 'No Autorizados (' + name_objetivos_autorizados+')',
						color: '#e3342f',
						y: objetivos_autorizados,
					}, {
						name: 'Autorizados (' + name_non_objetivos_autorizados+')',
						color: '#38c172',
						y: non_objetivos_autorizados
					}
				]
			}]
		});

        chart2 = Highcharts.chart('logrosrevisados', {
			chart: {
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false,
				type: 'pie'
			},
			credits: {
				enabled: false
			},
			title: {
				// text: '<b>80%</b>',
				// y:225
				text: null
			},
			legend:{
				enabled:true
			},
			tooltip: {
				pointFormat: '<b>{point.percentage:.1f}%</b>'
				// pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					allowPointSelect: false,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						formatter:function(){
							return this.y + '%';
						}
					},
					showInLegend: true
				}
			},
			series: [{
				name: 'Avance',
				colorByPoint: true,
				innerSize: '70%',
				data: [
					{
						name: months[periodo] + ' (' + name_logos_autorizados+')',
						color: '#38c172',
						y: logos_autorizados,
					},
					{
						name: 'Pendientes (' + name_non_logos_autorizados+')',
						color: '#e3342f',
						y: 100-logos_autorizados,
					}
				]
			}]

		});


    });

		var id_plan = <?php echo (!empty($plan) ? $plan->id : 0)?>;
		var tipo_plan = <?php echo (!empty($plan) ? $plan->tipo : 1)?>;
		var disabled = <?php echo (!empty(Session::get('midete_admin')) ? -1 : true)?>;
		var table = 0;
		var exportar = 0;
		
		$(document).ready(function(){
			
			if (disabled == -1){
				
				disabled = false;
			}

			$('select.id_plan').change(function(){

				$('form#changePlanForm').submit();
			});
			
			$('form.exportar_resultados').submit(function(){
				
				if (exportar == 1){
					
					exportar = 0;
					return true;
				}

				var data = table.rows({search: 'applied'}).data();
				var pos = 0;
				var temp = '';
				var ids = '';
				
				if (tipo_plan == 2){
				
					for (var i = 0;i < data.length;i++){

						pos = data[i][6].indexOf('id="') + 4;
						temp = data[i][6].substr(pos);
						pos = temp.indexOf('"');
						temp = temp.substr(0, pos);
						ids += ',' + temp.replace('texto', '');
					}
				}
				
				else{
					
					for (var i = 0;i < data.length;i++){

						pos = data[i][6].indexOf('id_empleado');
						
						if (pos > -1){
							
							pos += 20;
							temp = data[i][6].substr(pos);
							pos = temp.indexOf('"');
							temp = temp.substr(0, pos);
							ids += ',' + temp;
						}
					}
				}
				
				ids = ids.substr(1);
				$('form.exportar_resultados .ids').val(ids);
				
				exportar = 1;
				setTimeout(function(){
					$('form.exportar_resultados').submit();
				}, 500);
				return false;
			});
			
			if (tipo_plan == 3){

				table = $('.data-table').DataTable({
					// dom: 'lfrtip',
					dom:"<'row'<'col-md-6'l><'col-md-6'Bf>>" +
"<'row'<'col-md-6'><'col-md-6'>>" +
"<'row'<'col-md-12't>><'row'<'col-md-12'ip>>",

   				order: [[8,'desc']],
   				language: {
		 				"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		},
   				buttons: [
        	{
          	extend: 'excel',
						text: 'Exportar a Excel',
						titleAttr: 'Exportar a Excel',
						title: 'Evaluación de Resultados',
						exportOptions: {
							columns: ':not(.detalle)'
						}
					}
        ],
					fnDrawCallback: function(){
            
					jQuery(".valores_pasados").bootstrapSwitch({
						onText: 'SI',
						offText: 'NO',
						disabled: disabled,
						onSwitchChange: function(event, state){
							
							var id_empleado = event.target.classList[1];
							var estado = 2;
							var texto = 'No';
					
							if (state == true){
						
								estado = 1;
								texto = 'Si';
							}

							$('span#texto' + id_empleado).text(texto);
							var td = $('span#texto' + id_empleado).parent();
							var cell = table.cell(td);

							setTimeout(function(){

								cell.data('<span style="display: none" id="texto' + id_empleado + '">' + texto + '</span><input type="checkbox" class="valores_pasados ' + id_empleado + '" ' + (texto == 'Si' ? 'checked="checked"' : '') + '>').draw();
							}, 1000);
					
							$.ajax({
        						type:'POST',    
        						url: 'cambiar-periodos-pasados',
          						data:{
									id_empleado: id_empleado,
									id_plan: id_plan,
          							estado: estado,
									'_token': '{{ csrf_token() }}'
          						}
							});
						}
					});
				}
   				});
			}
			
			else{
				
				table = $('.data-table').DataTable({ 
					dom:"<'row'<'col-md-6'l><'col-md-6'Bf>>" +
"<'row'<'col-md-6'><'col-md-6'>>" +
"<'row'<'col-md-12't>><'row'<'col-md-12'ip>>",
   				order: [[7,'desc']],
   				language: {
		 				'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
      		},
   				buttons: [
        	{
          	extend: 'excel',
						text: 'Exportar a Excel',
						titleAttr: 'Exportar a Excel',
						title: 'Evaluación de Resultados',
						exportOptions: {
							columns: ':not(.detalle)'
						}
					}
        	],
				});
			}
		});
	</script>
@endsection
