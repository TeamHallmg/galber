@extends('layouts.app')

@section('title', 'Borrar Eje Estratégico')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="../img/banner_evaluacion_resultados.png" alt="">

	</div>
	<div class="col-md-12">
		<h4 class="margin-top-20 titulos-evaluaciones">Borrar Eje Estratégico</h4>
	</div>
</div>
<div class="row margin-top-20">
	<div class="col-md-3"></div>
	<div class="col-md-6 text-center">
		<form action="../borrar-eje-estrategico" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id" value="{{$eje_estrategico[0]->id}}">
			<div class="form-group">
        <label for="nombre">¿De verdad quieres borrar el Eje Estratégico {{$eje_estrategico[0]->nombre}}?</label>
      </div>
			<div class="form-group">
        <button class="btn btn-finvivir-blue" type="submit">Borrar</button> <a href="../ejes-estrategicos" class="btn btn-primary">Cancelar</a>
      </div>
    </form>
	</div>
	<div class="col-md-3"></div>
</div>
@endsection
