@extends('layouts.app')

@section('title', 'Planes')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/banner_evaluacion_resultados.png') }}" alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Ejes Estratégicos</h3>
		<hr class="mb-5">

	<div class="col-md-12 text-center mb-5">
		<a href="crear-eje-estrategico" class="btn btn-primary btn-finvivir-blue"><i class="fas fa-plus-circle"></i> Agregar Eje Estratégico</a>
	</div>


		<table class="table table-striped table-bordered table-hover ejes dt-responsive">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th>Código</th>
					<th>Nombre</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>

			@for ($i = 0; $i < count($ejes_estrategicos); $i++)
				<tr>
					<td>{{$ejes_estrategicos[$i]->id}}</td>
					<td>{{$ejes_estrategicos[$i]->nombre}}</td>
					<td align="center" width="25%">
						<a href="editar-eje-estrategico/{{$ejes_estrategicos[$i]->id}}" class="btn btn-primary mr-3"><i class="fas fa-edit"></i> Editar</a> <a href="borrar-eje-estrategico/{{$ejes_estrategicos[$i]->id}}" class="btn btn-primary btn-danger"><i class="fas fa-trash-alt"></i> Eliminar</a>
					</td>
				</tr>
			@endfor
			</tbody>
		</table>
	</div>
</div>
@endsection
@section('scripts')

<script type="text/javascript">
	$('.ejes').DataTable({
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
   	});
</script>

@endsection