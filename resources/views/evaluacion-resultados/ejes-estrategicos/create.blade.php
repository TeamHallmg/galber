@extends('layouts.app')

@section('title', 'Crear Eje Estratégico')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/banner_evaluacion_resultados.png') }}" alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Crear Eje Estratégico</h3>
		<hr class="mb-5">


<div class="row margin-top-20">
	<div class="col-md-3"></div>
	<div class="col-md-6 text-center">
		<form action="crear-eje-estrategico" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form-group">
        <label for="nombre">Nombre</label>
        <input type="text" class="form-control" name="nombre" required>
      </div>
			<div class="form-group">
        <button class="btn btn-success" type="submit"><span class="fas fa-check-circle"></span> Crear</button>
      </div>
    </form>
	</div>
</div>
	</div>
</div>
@endsection
