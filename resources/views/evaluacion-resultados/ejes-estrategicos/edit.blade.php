@extends('layouts.app')

@section('title', 'Editar Eje Estratégico')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
	    <img class="img-fluid" src="{{ asset('img/banner_evaluacion_resultados.png') }}" alt="">
	    <h3 class="titulos-evaluaciones mt-5 font-weight-bold">Editar Eje Estratégico</h3>
	    <hr class="mb-5">
<div class="row mx-auto">
	<div class="col-md-3"></div>
	<div class="col-md-6 text-center">
		<form action="../editar-eje-estrategico" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id" value="{{$eje_estrategico[0]->id}}">
				<div class="form-group">
	        <label for="nombre">Nombre</label>
	        <input type="text" class="form-control" name="nombre" required value="{{$eje_estrategico[0]->nombre}}">
	      </div>
				<div class="form-group">
	        <button class="btn btn-success" type="submit"><i class="fas fa-check-circle"></i> Guardar Cambios</button>
	      </div>
	    </form>
    </div>
</div>

	</div>
</div>
@endsection
