@extends('layouts.app')

@section('title', 'Cumplimiento de Compromisos')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_evaluacion_resultados.png" alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Cumplimiento de Compromisos</h3>
		<hr>

	@if (!empty($planes))

<?php $show_save_button = false;
			$show_revision_button = false; ?>

		<div class="margin-top-20 text-center">
			Plan 
			<select class="form-group id_plan" style="border-radius: 7px; margin-bottom: 4px">

		@foreach ($planes as $key => $plan)
		
			<option value="{{$plan->id}}" <?php if ($plan->id == $id_plan){ ?>selected="selected"<?php } ?>>{{$plan->nombre}}</option>
		@endforeach

			</select>
		</div>

		@if (!empty($responsabilities))

		<div style="overflow: auto; max-height: 600px">
			<table class="tabla_compromisos table table-striped table-bordered table-hover">
				<thead style="background-color: #222B64; color:white">
					<tr>
						<th class="cabeceras-evaluaciones">Clave</th>
						<th class="cabeceras-evaluaciones">Objetivo</th>
						<th class="cabeceras-evaluaciones">Logro Esperado</th>
						<th class="cabeceras-evaluaciones">Unidad de medida</th>
						<th class="cabeceras-evaluaciones">{{$year}}<br>Ene</th>
						<th class="cabeceras-evaluaciones">{{$year}}<br>Feb</th>
						<th class="cabeceras-evaluaciones">{{$year}}<br>Mar</th>
						<th class="cabeceras-evaluaciones">{{$year}}<br>Abr</th>
						<th class="cabeceras-evaluaciones">{{$year}}<br>May</th>
						<th class="cabeceras-evaluaciones">{{$year}}<br>Jun</th>
						<th class="cabeceras-evaluaciones">{{$year}}<br>Jul</th>
						<th class="cabeceras-evaluaciones">{{$year}}<br>Ago</th>
						<th class="cabeceras-evaluaciones">{{$year}}<br>Sep</th>
						<th class="cabeceras-evaluaciones">{{$year}}<br>Oct</th>
						<th class="cabeceras-evaluaciones">{{$year}}<br>Nov</th>
						<th class="cabeceras-evaluaciones">{{$year}}<br>Dic</th>
					</tr>
				</thead>
				<tbody>

<?php $current_responsability = 0;
			$id_responsability = 0;
			$num_months = 1;
			$frequency = 1;
			$new_periodo = true;
			$counter = 0;
			$current_type = 0; ?>

			@foreach ($responsabilities as $key => $responsability)

				@if ($id_responsability != $responsability->id)

					@if ($id_responsability != 0)

						@if ($new_periodo)

				<?php $month = $num_months + $frequency - 1; ?>

							@if ($month < 13)

								@for ($i = $num_months;$i < $month;$i++)

						<td></td>
								@endfor

					<?php $num_months = $month;
								$periodo = $num_months / $frequency;
								$show_save_button = true; ?>

								@if ($canEdit)

						<?php $num_months++; ?>

									@if ($current_responsability->unidad == 'Si/No')

						<td class="text-center">
							<select class="form-control logro_por_guardar" data-periodo="{{$periodo}}" data-frequency="{{$frequency}}" data-responsability="{{$current_responsability->id}}" style="width: 80px">
								<option value="Si">Si</option>
								<option value="No">No</option>
							</select>
						</td>
									@else

						<td class="text-center">
							<input type="number" step=".01" class="form-control logro_por_guardar" data-periodo="{{$periodo}}" data-frequency="{{$frequency}}" data-responsability="{{$current_responsability->id}}" style="width: 100px">
						</td>
									@endif
								@endif
							@endif
						@endif

						@for ($i = $num_months;$i < 13;$i++)

						<td></td>
						@endfor

					</tr>
					@endif

		<?php $current_responsability = $responsability;
					$id_responsability = $responsability->id;
					$found_objetive = false;
					$num_months = 1;
					$frequency = 1;
					$new_periodo = true;
					$counter = 0;
					$current_type = 0; ?>

					@if ($responsability->frecuencia_captura == 'Bimestral')

			<?php $frequency = 2; ?>
					@else

						@if ($responsability->frecuencia_captura == 'Trimestral')

				<?php $frequency = 3; ?>
						@else

							@if ($responsability->frecuencia_captura == 'Anual')

					<?php $frequency = 12; ?>
							@endif
						@endif
					@endif

					<tr>
						<td class="text-center">
							<select class="text-center form-control size3">

					@foreach ($objetivos_corporativos as $key2 => $objetivo)

						@if ($current_type != $objetivo->id_tipo)

				<?php $current_type = $objetivo->id_tipo;
							$counter = 1; ?>
						@endif

						@if ($objetivo->id == $responsability->id_objetivo_corporativo)
								
								<option>{{$objetivo->letter}}{{$counter}}</option>

			<?php break; ?>
						@endif

			<?php $counter++; ?>
					@endforeach

							</select>
						</td>
						<td class="text-center">
							<input type="text" class="form-control size2" value="{{$responsability->description}}" readonly>
						</td>
						<td class="text-center">
							<input type="text" class="form-control size text-right" value="{{$responsability->meta}}" readonly>
						</td>
						<td class="text-center">
							<select class="text-center form-control size3">
								<option>{{($responsability->unidad == '#' ? '$' : ($responsability->unidad == '9' ? '0' : $responsability->unidad))}}</option>
							</select>
						</td>
				@endif

				@if (!empty($responsability->id_cumplimiento))

		<?php $month = $frequency * $responsability->periodo; ?>

					@for ($i = $num_months;$i < $month;$i++)

						<td></td>
					@endfor

		<?php $num_months = $month; ?>

					@if ($responsability->revisado == 1)

			<?php $class = '';
						$num_months++; ?>

						@if ($responsability->unidad == 'Si/No')

							@if ($responsability->valor_verde == $responsability->logro)

					<?php $class = 'success'; ?>
							@else

					<?php $class = 'danger'; ?>
							@endif
						@else

							@if ($responsability->valor_verde >= $responsability->valor_rojo)

								@if ($responsability->logro >= $responsability->valor_verde)

						<?php $class = 'success'; ?>
								@else

									@if ($responsability->logro <= $responsability->valor_rojo)

							<?php $class = 'danger'; ?>
									@else

							<?php $class = 'warning'; ?>
									@endif
								@endif
							@else

								@if ($responsability->logro <= $responsability->valor_verde)

						<?php $class = 'success'; ?>
								@else

									@if ($responsability->logro >= $responsability->valor_rojo)

							<?php $class = 'danger'; ?>
									@else

							<?php $class = 'warning'; ?>
									@endif
								@endif
							@endif
						@endif

						<td class="text-center">
							<button style="cursor: pointer" class="btn btn-{{$class}}">{{($responsability->unidad == '%' ? number_format($responsability->logro, 2, '.', ',') . '%' : ($responsability->unidad == '#' ? '$' . number_format($responsability->logro) : ($responsability->unidad == '9' ? number_format($responsability->logro) : $responsability->logro)))}}</button>
						</td>
					@else

			<?php $new_periodo = false;
						$show_save_button = true;
						$show_revision_button = true; ?>

						@if ($canEdit)

				<?php $num_months++; ?>

							@if ($responsability->unidad == 'Si/No')

						<td class="text-center">
							<select class="form-control logro_guardado" data-id="{{$responsability->id_cumplimiento}}" style="width: 80px">
								<option value="Si">Si</option>
								<option value="No" <?php if ($responsability->logro == 'No'){ ?>selected="selected"<?php } ?>>No</option>
							</select>
						</td>
							@else

						<td class="text-center">
							<input type="number" step=".01" class="form-control logro_guardado" data-id="{{$responsability->id_cumplimiento}}" value="{{$responsability->logro}}" style="width: 100px">
						</td>
							@endif
						@endif
					@endif
				@else
			
		<?php $month = $frequency;
					$new_periodo = false;
					$show_save_button = true; ?>

					@while ($month < $start_month)

			<?php $month = $month + $frequency; ?>
					@endwhile

					@for ($i = $num_months;$i < $month;$i++)

						<td></td>
					@endfor

		<?php $num_months = $month;
					$periodo = $num_months / $frequency; ?>

					@if ($canEdit)

			<?php $num_months++; ?>

						@if ($responsability->unidad == 'Si/No')

						<td class="text-center">
							<select class="form-control logro_por_guardar" data-periodo="{{$periodo}}" data-frequency="{{$frequency}}" data-responsability="{{$responsability->id}}" style="width: 80px">
								<option value="Si">Si</option>
								<option value="No">No</option>
							</select>
						</td>
						@else

						<td class="text-center">
							<input type="number" step=".01" class="form-control logro_por_guardar" data-periodo="{{$periodo}}" data-frequency="{{$frequency}}" data-responsability="{{$responsability->id}}" style="width: 100px">
						</td>
						@endif
					@endif
				@endif
			@endforeach

			@if ($id_responsability != 0)

				@if ($new_periodo)

		<?php $month = $num_months + $frequency - 1; ?>

					@if ($month < 13)

						@for ($i = $num_months + 1;$i < $month;$i++)

						<td></td>
						@endfor

			<?php $num_months = $month;
						$periodo = $num_months / $frequency;
						$show_save_button = true; ?>

						@if ($canEdit)

				<?php $num_months++; ?>

							@if ($current_responsability->unidad == 'Si/No')

						<td class="text-center">
							<select class="form-control logro_por_guardar" data-periodo="{{$periodo}}" data-frequency="{{$frequency}}" data-responsability="{{$current_responsability->id}}" style="width: 80px">
								<option value="Si">Si</option>
								<option value="No">No</option>
							</select>
						</td>
							@else

						<td class="text-center">
							<input type="number" step=".01" class="form-control logro_por_guardar" data-periodo="{{$periodo}}" data-frequency="{{$frequency}}" data-responsability="{{$current_responsability->id}}" style="width: 100px">
						</td>
							@endif
						@endif
					@endif
				@endif

				@for ($i = $num_months;$i < 13;$i++)

						<td></td>
				@endfor

					</tr>
			@endif
						
				</tbody>
			</table>
		</div>

			@if ($show_save_button && $canEdit)

		<div class="row margin-top-20">
			<div class="col-md-12 text-center">
				<button class="btn btn-primary guardar_cumplimientos" type="button">Guardar cumplimientos</button>
			</div>
		</div>
			@endif

		<div>
			<div class="col-md-12 text-center">
				<div style="margin-top: 20px">
					<form action="/evaluacion-resultados/autorize-cumplimientos" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id_plan" value="{{$id_plan}}">
						<button type="submit" class="btn btn-primary autorizar_cumplimientos" <?php if (!$show_revision_button || !$canEdit){ ?>style="display: none"<?php } ?>>Revisión</button>
					</form>
				</div>
			</div>
		</div>
		@endif

<?php $meses = array(0,'Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'); ?>

		<hr class="divisor">
		<div class="row">
			<h3 class="bsc_title">Retroalimentación Notas</h3>
			<div class="col-md-12">
				<h4 class="bsc_title">Notas</h4>
				<textarea class="form-control notas_area"></textarea>
				<div class="text-right">
					<button class="btn btn-primary agregar_nota publica">
						<i class="fas fa-plus-circle"></i> Agregar nota
					</button>
				</div>
				<div style="overflow: auto; max-height: 400px">
					<table class="table table-striped table-bordered table-hover">
						<thead style="background-color: #222B64; color:white">
							<tr>
								<th style="text-align: left">Fecha</th>
								<th style="text-align: left">Nombre</th>
								<th style="text-align: left">Nota</th>
							</tr>
						</thead>
						<tbody>

			<?php foreach ($notas as $key => $value){ ?>

							<tr>
								<td class="text-left">{{substr($value->created_at, 8, 2) * 1}} / {{$meses[substr($value->created_at, 5, 2) * 1]}} / {{substr($value->created_at, 0, 4)}}</td>
								<td class="text-left">{{$value->nombre}} {{$value->paterno}} {{$value->materno}}</td>
								<td class="text-left">{{$value->mensaje}}</td>
							</tr>
			<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
		<form action="/evaluacion-resultados/cumplimiento-de-compromisos" method="post" class="change_id_plan">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id_plan" class="id_plan" value="{{$id_plan}}">
			<input type="submit" style="display: none">
		</form>
	@else

		<h2>NO HAY PLANES ABIERTOS</h2>
	@endif

	</div>
</div>
@endsection

@section('scripts')
<script>

	var user = '<?php echo auth()->user()->name?>';
	var id_plan = <?php echo $id_plan?>;
	var id_user = <?php echo auth()->user()->id?>;
	var meses = [];
		
	$(document).ready(function(){

		$.ajaxSetup({
    	headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    	}
		});

    meses[0] = 'Ene';
    meses[1] = 'Feb';
    meses[2] = 'Mar';
    meses[3] = 'Abr';
    meses[4] = 'May';
    meses[5] = 'Jun';
    meses[6] = 'Jul';
    meses[7] = 'Ago';
    meses[8] = 'Sep';
   	meses[9] = 'Oct';
    meses[10] = 'Nov';
    meses[11] = 'Dic';

		$('button.agregar_nota').click(function(){

			var button = $(this);
			var nota = button.parent().prev().val();

			if (button.hasClass('publica')){

				$.ajax({
        	type:'POST',    
        	url: '/evaluacion-resultados/save-note',
        	data:{
          	mensaje: nota,
          	id_plan: id_plan
        	},
        	success: function(data){
             
          	var fecha = new Date();
          	button.parent().parent().find('table').prepend('<tr><td>' + fecha.getDate() + '/' + meses[fecha.getMonth()] + '/' + fecha.getFullYear() + '</td><td>' + user + '</td><td>' + nota + '</td></tr>');
          	button.parent().prev().val('');
        	}
      	});
      }

      else{

      	$.ajax({
        	type:'POST',    
        	url: '/evaluacion-resultados/save-note',
        	data:{
          	mensaje: nota,
          	id_plan: id_plan,
          	id_empleado: id_user
        	},
        	success: function(data){
             
          	var fecha = new Date();
          	button.parent().parent().find('table').prepend('<tr><td>' + fecha.getDate() + '/' + meses[fecha.getMonth()] + '/' + fecha.getFullYear() + '</td><td>' + user + '</td><td>' + nota + '</td></tr>');
          	button.parent().prev().val('');
        	}
      	});
      }
		});

		// Cambia el plan
		$('select.id_plan').change(function(){

			// Recarga la pagina con el nuevo plan
			$('form.change_id_plan .id_plan').val($(this).val());
			$('form.change_id_plan').submit();
		});

		// Guarda cumplimientos (logros corporativos)
		$('button.guardar_cumplimientos').click(function(){

			var guardado = true;
			var valor = 0;
			var periodo = 0;
			var id_responsability = 0;
			var frequency = 0;
			var month = 0;
			var current_month = 0;
			var id = 0;

			$('table.tabla_compromisos .logro_guardado, table.tabla_compromisos .logro_por_guardar').each(function(){

				if ($(this).hasClass('logro_por_guardar')){

					var cumplimiento = $(this);
					frequency = $(this).attr('data-frequency') * 1;
					periodo = $(this).attr('data-periodo') * 1;
					month = new Date();
					current_month = month.getMonth() * 1 + 1;
					month = frequency * periodo;
				
					/*if ($(this).val().trim().length == 0 && month < current_month){

						alert('Escribe un número');
						$(this).focus();
						guardado = false;
						return false;
					}*/

					valor = $(this).val();

					if (valor != 'Si' && valor != 'No' && isNaN(valor) && month < current_month){

						alert('Debes guardar sólo numeros');
						$(this).focus();
						guardado = false;
						return false;
					}

					id_responsability = $(this).attr('data-responsability') * 1;

					if ($(this).val().trim().length > 0){

						$.ajax({
        			type:'POST',    
        			url: '/evaluacion-resultados/save-cumplimiento',
          		data:{
          			cumplimiento: valor,
            		periodo: periodo,
          			id_responsability: id_responsability,
          			id_plan: id_plan,
								'_token': '{{ csrf_token() }}'
          		}
        		});
        	}
				}

				else{

					if ($(this).val().trim().length == 0){

						alert('Escribe un número');
						$(this).focus();
						guardado = false;
						return false;
					}

					valor = $(this).val();

					if (valor != 'Si' && valor != 'No' && isNaN(valor)){

						alert('Debes guardar sólo numeros');
						$(this).focus();
						guardado = false;
						return false;
					}

					id = $(this).attr('data-id');

					$.ajax({
        		type:'POST',    
        		url: '/evaluacion-resultados/save-cumplimiento',
          	data:{
          		cumplimiento: valor,
          		id: id,
							'_token': '{{ csrf_token() }}'
          	}
        	});
				}
			});

			if (!guardado){

				$('button.autorizar_cumplimientos').hide();
			}

			else{

				alert('Los cumplimientos fueron guardados');
				$('form.change_id_plan').submit();
				//$('button.autorizar_cumplimientos').show();
			}
		});
	});
</script>
@endsection