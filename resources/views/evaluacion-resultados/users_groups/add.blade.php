@extends('layouts.app')

@section('title', 'Crear Grupo de Usuarios')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_evaluacion_resultados.png" alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Crear grupo de usuarios</h3>
		{!! Form::open(['route' => 'users_groups.store', 'method' => 'POST'])!!}
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						{!! Form::label('nombre', 'Nombre') !!}
						{!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Escribe el nombre del grupo', 'required'])!!}
					</div>

				@if (!empty($users))

					<div class="form-group elements_container">
						{!! Form::label('users', 'Usuarios') !!}
						{!! Form::text('users', null, ['class' => 'form-control users_autocomplete', 'placeholder' => 'Escribe el nombre del usuario'])!!}
						<button type="button" class="btn btn-primary add_user" style="display: none">Agregar</button>
					</div>
				@endif

				@if (!empty($departments))

					<div class="form-group elements_container">
						{!! Form::label('departments', 'Departamentos') !!}
						{!! Form::text('departments', null, ['class' => 'form-control departments_autocomplete', 'placeholder' => 'Escribe el nombre del departamento'])!!}
						<button type="button" class="btn btn-primary add_department" style="display: none">Agregar</button>
					</div>
				@endif

				@if (!empty($users_groups))

					<div class="form-group elements_container">
						{!! Form::label('grupos', 'Grupos') !!}
						{!! Form::text('grupos', null, ['class' => 'form-control groups_autocomplete', 'placeholder' => 'Escribe el nombre del grupo'])!!}
						<button type="button" class="btn btn-primary add_user_group" style="display: none">Agregar</button>
					</div>
				@endif

					<div class="form-group">
						{!! Form::submit('Guardar', ['class' => 'btn btn-success'])!!}
						<a class="btn btn-primary" href="{{ URL::previous() }}">Regresar</a>
					</div>
				</div>
			</div>
		{!! Form::close() !!}

	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

	var users = <?php echo json_encode($users)?>;
	var departments = <?php echo json_encode($departments)?>;
	var users_groups = <?php echo json_encode($users_groups)?>;
	var user_name = '';
	var department_name = '';
	var group_name = '';
	var user_id = 0;
	var department_id = 0;
	var group_id = 0;
	
	$(document).ready(function(){

		// Autocomplete 
    $('.users_autocomplete').autocomplete({
    	
    	lookup: users,
    	onSelect: function (suggestion){

    		user_name = suggestion.value;
    		user_id = suggestion.data;
    		$('button.add_user').show();
    	}
		});

		// Autocomplete 
    $('.departments_autocomplete').autocomplete({
    	
    	lookup: departments,
    	onSelect: function (suggestion){

    		department_name = suggestion.value;
    		department_id = suggestion.data;
    		$('button.add_department').show();
    	}
		});

		// Autocomplete 
    $('.groups_autocomplete').autocomplete({
    	
    	lookup: users_groups,
    	onSelect: function (suggestion){

    		group_name = suggestion.value;
    		group_id = suggestion.data;
    		$('button.add_user_group').show();
    	}
		});

		// Agrega un nuevo usuario al grupo
		$('button.add_user').click(function(){

			$('.users_autocomplete').val('');
			$(this).parent().append('<div class="row"><div class="col-md-4 text-left"><input type="hidden" name="users[]" value="' + user_id + '">' + user_name + '</div><div class="col-md-8"><a href="javascript:;" class="remove_element">X</a></div></div>');
			$(this).hide();
		});

		// Agrega un nuevo departamento al grupo
		$('button.add_department').click(function(){

			$('.departments_autocomplete').val('');
			$(this).parent().append('<div class="row"><div class="col-md-4 text-left"><input type="hidden" name="departments[]" value="' + department_id + '">' + department_name + '</div><div class="col-md-8"><a href="javascript:;" class="remove_element">X</a></div></div>');
			$(this).hide();
		});

		// Agrega un nuevo grupo al grupo
		$('button.add_user_group').click(function(){

			$('.groups_autocomplete').val('');
			$(this).parent().append('<div class="row"><div class="col-md-4 text-left"><input type="hidden" name="grupos[]" value="' + group_id + '">' + group_name + '</div><div class="col-md-8"><a href="javascript:;" class="remove_element">X</a></div></div>');
			$(this).hide();
		});

		// Quita un usuario del grupo
		$('body').on('click', 'a.remove_element', function(){

			$(this).parent().parent().remove();
		});
	});
</script>
@endsection