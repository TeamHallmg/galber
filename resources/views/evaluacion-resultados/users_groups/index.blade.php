@extends('layouts.app')

@section('title', 'Grupos de Usuarios')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_evaluacion_resultados.png" alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Grupos de usuarios</h3>
		<hr>
		<div class="text-center">
			<a href="/evaluacion-resultados/users_groups/create" class="btn btn-primary">
				<i class="fas fa-plus-circle"></i> Nuevo Grupo de Usuarios</a>
		</div>

	@if (!$users_groups->isEmpty())

		<table class="table table-striped table-bordered table-hover margin-top-20">
			<thead style="background-color: #222B64; color:white">
				<tr>
					<th>ID</th>
					<th>Grupo</th>
					<th>Elementos</th>
					<th class="text-center">Acciones</th>
				</tr>
			</thead>
			<tbody>

		@foreach ($users_groups as $users_group)

				<tr>
					<td>{{$users_group->id}}</td>
					<td>{{$users_group->nombre}}</td>
					<td>
				@if (!empty($users_group->elements))
					@foreach ($users_group->elements as $element)
						@if ($element->tipo_elemento == 'Persona')	
							{{$users[$element->id_elemento]}}<br>
						@else
							@if ($element->tipo_elemento == 'Departamento')
								{{$departments[$element->id_elemento]}}<br>
							@else
								{{$others_users_groups[$element->id_elemento]}}<br>
							@endif
						@endif
					@endforeach
				@endif
					</td>
					<td class="text-center">
						<a class="btn btn-primary" href="/evaluacion-resultados/users_groups/{{$users_group->id}}/edit">
							<i class="fas fa-edit"></i> Editar</a>
					</td>
				</tr>
		@endforeach
		
			</tbody>
		</table>
	@else

		<div class="text-center">
			<h4 class="titulos-evaluaciones">No hay grupos</h4>
		</div>
	@endif

	</div>
</div>
@endsection

@section('scripts')
	<script>
		
		$(document).ready(function(){

			$('.table').DataTable({
				dom: 'lfrtip',
   			language: {
		 				"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
       	}
      });
		});
	</script>
@endsection