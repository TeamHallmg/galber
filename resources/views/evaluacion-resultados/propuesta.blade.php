@extends('layouts.app')

@section('title', 'Propuesta')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="img/banner_evaluacion_resultados.png" alt="">
	 	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Propuesta</h5>
			<div class="card-body">
				
 
@if (!empty($planes))

<?php $meses_completos = array('0', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
 			$total_peso = 0; ?>



<div class="row mb-3 text-center"> 
	<div class="col-12 col-md-6 text-center">
		<div class="form-group">
			<label for="id_plan" class="font-weight-bolder mb-0">Plan:</label>							
			<form id="changePlanForm" action="propuesta" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_empleado" value="{{$user->id}}">
			<select name="id_plan" id="id_plan" class="form-control id_plan">
				@for ($i = 0;$i < count($planes);$i++)

					<option value="{{$planes[$i]->id}}" <?php if ($plan->id == $planes[$i]->id){ ?>selected="selected"<?php } ?>>{{$planes[$i]->nombre}}</option>
				@endfor

			</select>
			<input type="submit" style="display: none">
			</form>
		</div>
	</div>
	
			@if (count($planes_para_copiar) > 0)
	<div class="col-12 col-md-6 text-center">
		<form action="/copiar-objetivos" method="post" onsubmit="return confirm('Si cuenta con objetivos en el plan actual seran borrados. ¿Desea continuar?')">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="plan_a_copiar" value="{{$plan->id}}">
			<input type="hidden" name="id_empleado" value="{{$id_user}}">
			<div class="form-group">
				<label for="plan_para_copiar" class="font-weight-bolder mb-0">Copiar Objetivos desde el Plan:</label>							
				<select name="plan_para_copiar" id="plan_para_copiar" class="form-control plan_para_copiar">
				@for ($i = 0;$i < count($planes_para_copiar);$i++)
					<option value="{{$planes_para_copiar[$i]->id}}">{{$planes_para_copiar[$i]->nombre}}</option>
				@endfor
				</select>
			</div>
			<div class="form-group">
				<label for="user_para_copiar" class="font-weight-bolder mb-0">Copiar Objetivos del Colaborador:</label>
				<input type="text" class="form-control size2 colaborador" placeholder="Escribe el nombre del colaborador">
				<div style="clear: both"></div>
			</div>
			<div style="margin-top: 5px">
				<button type="submit" class="btn btn-primary btn-finvivir-blue">Copiar</button>
			</div>
		</form>
	</div>
	@endif

</div>
 
<div class="card mb-3">
		<h5 class="card-header bg-info text-white font-weight-bolder">Persona</h5>
		<div class="card-body">
		<div class="media">
			<img src="{{asset('img/vacantes/sinimagen.png')}}" style="width: 120px" class="align-self-center mr-3" alt="...">
			<div class="media-body">
			
			<p><h5 class="mt-0 d-inline">Nombres: </h5> {{$user->nombre}} {{$user->paterno}} {{$user->materno}}</p>
			<p><h5 class="mt-0 d-inline">Puesto: </h5>{{(!empty($user->jobPosition) ? $user->jobPosition->name : '')}}</p>
			<p><h5 class="mt-0 d-inline">Área: </h5>{{(!empty($user->jobPosition) && !empty($user->jobPosition->area) && !empty($user->jobPosition->area->department) ? $user->jobPosition->area->department->name: '')}}</p>
			<p><h5 class="mt-0 d-inline">Jefe: </h5>{{(!empty($user->boss) ? $user->boss->nombre . ' ' . $user->boss->paterno . ' ' . $user->boss->materno : '')}}</p>
			</div>
		</div>
	</div>
</div>

@if ($mostrar_boton_aceptar)


	<div class="card mt-3">
		<h5 class="card-header bg-info text-white font-weight-bolder">Aceptar Mis Objetivos</h5>
		<div class="card-body">
			<div class="col-md-12 text-center">
				<form action="/aceptar-objetivos-resultados" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="id_plan" value="{{$plan->id}}">
					<input type="hidden" name="id_empleado" value="{{$id_user}}">
					<input type="hidden" name="tipo" value="Objetivos">
					<button type="submit" class="btn btn-primary">Acepto Mis Objetivos</button>
				</form>
			</div>
		</div>
	</div> 

@endif

<?php $counter = 0; ?>

@if (!empty($responsabilities))


<div class="card mt-3">
	<h5 class="card-header bg-info text-white font-weight-bolder">Responsabilidades y Compromisos</h5>
	<div class="card-body">
 
<div style="overflow: auto">
	<table class="tabla_compromisos table table-striped table-bordered table-hover" style="margin-bottom: 0">
		<thead style="background-color: #222B64; color:white">
			<tr>
				<th class="cabeceras-evaluaciones text-center">Clave</th>
				<th class="cabeceras-evaluaciones text-center">Responsable</th>
				<th class="cabeceras-evaluaciones text-center">Capturista</th>
				<th class="cabeceras-evaluaciones text-center">Objetivo</th>
				<th class="cabeceras-evaluaciones text-center">Tipo de Indicador</th>
				<th class="cabeceras-evaluaciones text-center">Fórmula</th>
				<th class="cabeceras-evaluaciones text-center">Frecuencia</th>
				<th class="cabeceras-evaluaciones text-center">Frecuencia de captura</th>
				<th class="cabeceras-evaluaciones text-center">Fuente</th>
				<th class="cabeceras-evaluaciones text-center">Co-responsable</th>
				<th class="cabeceras-evaluaciones text-center">Status</th>
				<th class="cabeceras-evaluaciones text-center">Meta</th>
				<th class="cabeceras-evaluaciones text-center">Unidad de medida</th>
				<th class="cabeceras-evaluaciones text-center">
					<p style="font-size: 13px">Semáforo de Desempeño</p>
					<div>
						<span style="background-color: red; padding: 5px 10px"></span><span style="background-color: #FFDB58; color: #FFDB58; padding: 5px 10px"></span><span style="background-color: green; padding: 5px 10px"></span>
					</div>
				</th>
			</tr>
		</thead>
		<tbody>

<?php foreach ($responsabilities as $key => $value){ ?>
			
			<tr class="responsabilidad">
				<td class="text-center">
					<select class="text-center form-control size3">

	<?php $current_type = 0;

				foreach ($objetivos_corporativos as $key2 => $objetivo_corporativo){
						
					if ($current_type != $objetivo_corporativo->id_tipo){

						$counter = 1;
						$current_type = $objetivo_corporativo->id_tipo;
					}

					if ($value->id_objetivo_corporativo == $objetivo_corporativo->id){ ?>

								<option>{{$objetivo_corporativo->letter}}{{$counter}}</option>

			<?php break;
					}

					$counter++;
				} ?>

							</select>
						</td>
						<td class="text-center">
							<div style="clear: both">

	<?php if (!empty($value->id_responsable)){

					foreach ($users as $key2 => $user){
						
						if ($user->data == $value->id_responsable){ ?>

								<div style="float: left" class="size2">{{$user->value}}</div>
				<?php break;
						}
					}
				} ?>

							</div>
						</td>
						<td class="text-center">
							<div style="clear: both">

	<?php if (!empty($value->id_capturista)){

					foreach ($users as $key2 => $user){
						
						if ($user->data == $value->id_capturista){ ?>

								<div style="float: left" class="size2">{{$user->value}}</div>
				<?php break;
						}
					}
				} ?>

							</div>
						</td>
						<td class="text-center">
							<textarea class="form-control size2" readonly>{{$value->description}}</textarea>
						</td>
							<td class="text-center">
								<select class="text-center form-control size">
									<option>{{$value->tipo_indicador}}</option>
								</select>
							</td>
							<td class="text-center">
								<textarea class="form-control size2" readonly>{{$value->formula}}</textarea>
							</td>
							<td class="text-center">
								<select class="text-center form-control size">
									<option>{{$value->frecuencia}}</option>
								</select>
							</td>
							<td class="text-center">
								<select class="text-center form-control size">
									<option>{{$value->frecuencia_captura}}</option>
								</select>
							</td>
							<td class="text-center">
								<textarea class="form-control size" readonly>{{$value->fuente}}</textarea>
							</td>
							<td class="text-center">
								<div class="row">

	<?php if (count($users_groups) > 0){ ?>

									<div class="col-md-12">

		<?php if (!empty($value->grupos_corresponsables)){

						foreach ($users_groups as $key2 => $users_group){
						
							if (in_array($users_group->data, $value->grupos_corresponsables)){ ?>

										<div style="clear: both">
											<div style="float: left" class="size2">{{$users_group->value}}</div>
										</div>
				<?php }
						}
					} ?>

									</div>
	<?php } ?>

									<div class="col-md-12">

	<?php if (!empty($value->corresponsables)){

					foreach ($users as $key2 => $user){
						
						if (in_array($user->data, $value->corresponsables)){ ?>

										<div style="clear: both">
											<div style="float: left" class="size2">{{$user->value}}</div>
										</div>
			<?php }
					}
				} ?>

									</div>
								</div>
							</td>
							<td class="text-center">
								<select class="text-center form-control size2">
									<option>{{($value->id_status == 1 ? 'a. Indicador disponible y utilizado.' : ($value->id_status == 2 ? 'b. Indicador disponible pero difiere su fórmula de cálculo' : ($value->id_status == 3 ? 'c. Indicador nuevo e información disponible' : 'd. Indicador nuevo e información no disponible')))}}</option>
								</select>
							</td>
							<td class="text-center">

				@if ($value->unidad != 'Si/No')

								<input type="text" class="form-control size text-right" value="{{$value->meta}}" readonly style="width: 90px">
				@else

								<select class="form-control size" style="width: 90px">
									<option>{{$value->meta}}</option>
								</select>
				@endif

							</td>
							<td class="text-center">
								<select class="text-center form-control size3 unidad">
									<option>{{($value->unidad == '#' ? '$' : ($value->unidad == '9' ? '0' : ($value->unidad == 'Si/No' ? 'Si / No' : '%')))}}</option>
								</select>
							</td>
							<td class="text-center">
								<div style="min-width: 190px">
									<button style="cursor: default" class="btn btn-danger">{{($value->unidad == '%' ? number_format($value->valor_rojo, 2, '.', ',') . '%' : ($value->unidad == '#' ? '$' . number_format($value->valor_rojo) : ($value->unidad == '9' ? number_format($value->valor_rojo) : $value->valor_rojo)))}}</button>
									<button style="cursor: default" class="btn btn-success">{{($value->unidad == '%' ? number_format($value->valor_verde, 2, '.', ',') . '%' : ($value->unidad == '#' ? '$' . number_format($value->valor_verde) : ($value->unidad == '9' ? number_format($value->valor_verde) : $value->valor_verde)))}}</button>
								</div>
							</td>
						</tr>
<?php } ?>

					</tbody>
				</table>
			</div>
			</div>
			</div>
		@endif

<div class="card mt-3">
	<h5 class="card-header bg-info text-white font-weight-bolder">
		@if ($id_user == auth()->user()->employee_id)
			Mis Objetivos
		@else
			Captura de Objetivos
		@endif
		</h5>
	<div class="card-body">




<div class="row">
	<div class="col-md-12" style="overflow-x: auto">
		<!--<div class="table-responsive">-->
			<table width="100%" style="font-size: 13px" class="table table-bordered table-striped tabla_objetivos">
				<thead style="background-color: #222B64; color:white;">
					<tr>
						<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">#</th>
						<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Periodo</th>
						<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Objetivo</th>
						<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Clave</th>
						<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Fórmula</th>
						<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Unidad de medida</th>
						<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Meta</th>
						<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Peso</th>
						<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Frecuencia</th>
						<!--<th class="cabeceras-evaluaciones">Co-responsables</th>-->
					<!--<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Tipo de Semáforo</th>-->
					<th class="cabeceras-evaluaciones" style="padding: 5px 50px; text-align: center; border: 1px solid white;">
						<p style="font-size: 13px">Rango de Semáforo</p>
						<div style="font-size: 13px">
							<span style="background-color: red; padding: 5px 10px">Rojo</span><span style="background-color: #FFDB58; color: #FFDB58; padding: 5px 10px">Am</span><span style="background-color: green; padding: 5px 10px">Verde</span>
						</div>
					</th>

	@if ((empty($status_plan) || $status_plan[0]->status == 1) && !empty($editar_objetivos))

		@if ($alimentar_propuestas == 1 || ($alimentar_propuestas == 2 && auth()->user()->employee->idempleado == $user->jefe) || ($alimentar_propuestas == 4 && !empty(auth()->user()->employee->grado)) || auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin'))
					<th class="cabeceras-evaluaciones" style="padding: 5px 35px; text-align: center; border: 1px solid white;">Eliminar</th>
					@endif
				@endif
				</tr>
			</thead>
			<tbody>

			<?php $total_peso = 0; ?>
				
		@for ($i = 0;$i < count($objetivos); $i++)

				<tr class="objetivos">
					<td class="text-center">{{$i + 1}}</td>
					<td class="text-center">{{$objetivos[$i]->periodo}}</td>
					<td class="text-center">
						<!--<input type="text" value="{{$objetivos[$i]->nombre}}" style="width: 200px; border-radius: 7px; border: 1px solid #A5A7A8; text-align: center" class="nombre" <?php //echo (!empty($status_plan) && $status_plan[0]->status >= 2 ? 'readonly' : '')?>>-->
						<textarea <?php echo (empty($objetivos[$i]->editar) ? 'readonly' : (empty($objetivos[$i]->id_catalogo) ? '' : 'readonly'))?> style="resize: none; overflow-x: hidden" class="nombre form-control size2">{{$objetivos[$i]->nombre}}</textarea>
					</td>
					<td class="text-center">

<?php $counter = 0;
			$current_type = 0;
			$repetidos = array(); ?>

			@if (!empty($objetivos[$i]->editar) && ((empty($objetivos[$i]->id_catalogo) && !auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')) || empty($objetivos[$i]->objetivo_catalogo) && auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')))

						<select class="id_objetivo_corporativo form-control text-center size3">

				@for ($j = 0;$j < count($objetivos_corporativos_sin_responsable);$j++)

					@if ($current_type != $objetivos_corporativos_sin_responsable[$j]->id_tipo)

			<?php $counter = 1;
						$current_type = $objetivos_corporativos_sin_responsable[$j]->id_tipo; ?>
					@endif

					@if (!in_array($objetivos_corporativos_sin_responsable[$j]->letter . $counter, $repetidos))

			<?php $repetidos[] = $objetivos_corporativos_sin_responsable[$j]->letter . $counter; ?>

							<option value="{{$objetivos_corporativos_sin_responsable[$j]->id}}" <?php if ($objetivos_corporativos_sin_responsable[$j]->id == $objetivos[$i]->id_objetivo_corporativo){ ?>selected="selected"<?php } ?>>{{$objetivos_corporativos_sin_responsable[$j]->letter}}{{$counter}}</option>
					@endif

		<?php $counter++; ?>
				@endfor

						</select>
			@else

						<select class="id_objetivo_corporativo form-control text-center size3" disabled>

				@for ($j = 0;$j < count($objetivos_corporativos_sin_responsable);$j++)

					@if ($current_type != $objetivos_corporativos_sin_responsable[$j]->id_tipo)

			<?php $counter = 1;
						$current_type = $objetivos_corporativos_sin_responsable[$j]->id_tipo; ?>
					@endif

					@if ($objetivos_corporativos_sin_responsable[$j]->id == $objetivos[$i]->id_objetivo_corporativo)

							<option value="{{$objetivos_corporativos_sin_responsable[$j]->id}}">{{$objetivos_corporativos_sin_responsable[$j]->letter}}{{$counter}}</option>
					@endif

		<?php $counter++; ?>
				@endfor

						</select>
			@endif

						</select>
					</td>
					<td class="text-center">
						<textarea <?php echo (empty($objetivos[$i]->editar) ? 'readonly' :  '')?> style="resize: none; overflow-x: hidden" class="formula form-control size2">{{$objetivos[$i]->formula}}</textarea>
					</td>
					<td class="text-center">
						<select class="tipo form-control size3" <?php echo (empty($objetivos[$i]->editar) ? 'disabled' : (empty($objetivos[$i]->tipo_catalogo) ? '' : 'disabled'))?>>
							<option value="%">%</option>
							<option value="$" <?php if ($objetivos[$i]->tipo == '$'){ ?>selected="selected"<?php } ?>>$</option>
							<option value="#" <?php if ($objetivos[$i]->tipo == '#' || $objetivos[$i]->tipo == '9'){ ?>selected="selected"<?php } ?>>#</option>
							<option value="Si/No" <?php if ($objetivos[$i]->tipo == 'Si/No'){ ?>selected="selected"<?php } ?>>Si/No</option>
						</select>
					</td>
					<td class="text-center">
					
			@if ($objetivos[$i]->tipo_bono != 'Comision')
					
				@if (empty($objetivos[$i]->editar) || (empty($objetivos[$i]->valores) && (!empty($objetivos[$i]->meta_catalogo) || !empty($objetivos[$i]->id_catalogo))))

					@if (empty($objetivos[$i]->valores))

						<input type="text" class="objetivo form-control size text-right {{($objetivos[$i]->tipo != 'Si/No' ? ($objetivos[$i]->tipo == '$' ? 'currency' : ($objetivos[$i]->tipo == '#' ? 'numeric' : 'porcentaje')) : '')}}" value="{{$objetivos[$i]->objetivo}}" readonly>
					@else

						<div style="width: 100%; overflow: hidden">
					  	<div style="position: relative; width: {{count($objetivos[$i]->valores) * 100}}%" class="slide text-right">
							
			<?php $espacio = 100 / count($objetivos[$i]->valores); ?>

					  @for ($j = 0;$j < count($objetivos[$i]->valores);$j++)

					  		<div style="width: {{$espacio}}%; float: left">
									<div class="text-center">{{($objetivos[$i]->frecuencia == 'Mensual' ? $meses_completos[$objetivos[$i]->valores[$j]->periodo] : ($objetivos[$i]->frecuencia == 'Trimestral' ? $meses_completos[$objetivos[$i]->valores[$j]->periodo * 3] : ($objetivos[$i]->frecuencia == 'Cuatrimestral' ? $meses_completos[$objetivos[$i]->valores[$j]->periodo * 4] : ($objetivos[$i]->frecuencia == 'Semestral' ? 'Julio' : ''))))}}</div>
									<input type="text" class="objetivo form-control size text-center {{($objetivos[$i]->tipo != 'Si/No' ? ($objetivos[$i]->tipo == '$' ? 'currency' : ($objetivos[$i]->tipo == '#' ? 'numeric' : 'porcentaje')) : '')}}" value="{{$objetivos[$i]->valores[$j]->meta}}" readonly>
								</div>
					  @endfor
					
					  	</div>
					  </div>
					  <div class="text-center" data-valores="{{count($objetivos[$i]->valores)}}" data-step="0">
					  	<span class="fas fa-arrow-left metas_arrow_left" style="cursor: pointer"></span> <span class="fas fa-arrow-right metas_arrow_right" style="cursor: pointer"></span>
					  </div>
					@endif
				@else

					@if (empty($objetivos[$i]->valores))

						<input type="text" class="objetivo form-control size text-right {{($objetivos[$i]->tipo != 'Si/No' ? ($objetivos[$i]->tipo == '$' ? 'currency' : ($objetivos[$i]->tipo == '#' ? 'numeric' : 'porcentaje')) : '')}}" value="{{$objetivos[$i]->objetivo}}" <?php if ($objetivos[$i]->tipo == 'Si/No'){ ?>style="display: none"<?php } ?>>
						<select class="objetivo form-control size" <?php if ($objetivos[$i]->tipo != 'Si/No'){ ?>style="display: none"<?php } ?>>
							<option value="Si">Si</option>
							<option value="No" <?php if ($objetivos[$i]->objetivo == 'No'){ ?>selected="selected"<?php } ?>>No</option>
						</select>
					@else

						<div style="width: 100%; overflow: hidden">
					  	<div style="position: relative; width: {{count($objetivos[$i]->valores) * 100}}%" class="slide text-right">
							
			<?php $espacio = 100 / count($objetivos[$i]->valores); ?>

					  @for ($j = 0;$j < count($objetivos[$i]->valores);$j++)

					  		<div style="width: {{$espacio}}%; float: left">
									<div class="text-center">{{($objetivos[$i]->frecuencia == 'Mensual' ? $meses_completos[$objetivos[$i]->valores[$j]->periodo] : ($objetivos[$i]->frecuencia == 'Trimestral' ? $meses_completos[$objetivos[$i]->valores[$j]->periodo * 3] : ($objetivos[$i]->frecuencia == 'Cuatrimestral' ? $meses_completos[$objetivos[$i]->valores[$j]->periodo * 4] : ($objetivos[$i]->frecuencia == 'Semestral' ? 'Julio' : ''))))}}</div>
									<input type="text" class="objetivo form-control size text-right {{($objetivos[$i]->tipo != 'Si/No' ? ($objetivos[$i]->tipo == '$' ? 'currency' : ($objetivos[$i]->tipo == '#' ? 'numeric' : 'porcentaje')) : '')}}" value="{{$objetivos[$i]->valores[$j]->meta}}" <?php if ($objetivos[$i]->tipo == 'Si/No'){ ?>style="display: none"<?php } ?>>
									<select class="objetivo form-control size" <?php if ($objetivos[$i]->tipo != 'Si/No'){ ?>style="display: none"<?php } ?>>
										<option value="Si">Si</option>
										<option value="No" <?php if ($objetivos[$i]->valores[$j]->meta == 'No'){ ?>selected="selected"<?php } ?>>No</option>
									</select>
								</div>
					  @endfor
					
					  	</div>
					  </div>
					  <div class="text-center" data-valores="{{count($objetivos[$i]->valores)}}" data-step="0">
					  	<span class="fas fa-arrow-left metas_arrow_left" style="cursor: pointer"></span> <span class="fas fa-arrow-right metas_arrow_right" style="cursor: pointer"></span>
					  </div>
					@endif
				@endif
			@endif
					
					</td>
					<td class="text-center" >
						<input type="text" class="form-control size3 peso periodo{{$objetivos[$i]->periodo}} numeric text-right" value="{{$objetivos[$i]->peso}}" <?php echo (empty($objetivos[$i]->editar) ? 'readonly' : (empty($objetivos[$i]->peso_catalogo) ? '' : 'readonly'))?>>
					</td>
					<td class="text-center">
						<select class="frecuencia form-control size" <?php echo (empty($objetivos[$i]->editar) ? 'disabled' : (empty($objetivos[$i]->frecuencia_catalogo) ? '' : 'disabled'))?>>
							<option value="Mensual">Mensual</option>
							<option value="Bimestral" <?php if ($objetivos[$i]->frecuencia == 'Bimestral'){ ?>selected="selected"<?php } ?>>Bimestral</option>
							<option value="Trimestral" <?php if ($objetivos[$i]->frecuencia == 'Trimestral'){ ?>selected="selected"<?php } ?>>Trimestral</option>
							<option value="Cuatrimestral" <?php if ($objetivos[$i]->frecuencia == 'Cuatrimestral'){ ?>selected="selected"<?php } ?>>Cuatrimestral</option>
							<option value="Semestral" <?php if ($objetivos[$i]->frecuencia == 'Semestral'){ ?>selected="selected"<?php } ?>>Semestral</option>
							<option value="Anual" <?php if ($objetivos[$i]->frecuencia == 'Anual'){ ?>selected="selected"<?php } ?>>Anual</option>
						</select>
					</td>
					<!--<td class="text-center" style="padding: 5px">
						<div class="row">

<?php /*if (count($users_groups) > 0){ ?>

							<div class="col-md-12">

				@if (empty($status_plan) || $status_plan[0]->status == 1 || ($status_plan[0]->status == 3 && $peso_periodo == 0) || !empty($editar_objetivos))

								<input type="text" class="size2 grupos_corresponsables form-control" placeholder="Escribe el nombre del grupo" style="width: 260px; border-radius: 7px">
								<button type="button" class="btn btn-primary add_grupo" style="display: none">
									<i class="fas fa-plus-circle"></i> Agregar
								</button>
				@endif

	<?php if (!empty($objetivos[$i]->grupos_corresponsables)){

					foreach ($users_groups as $key2 => $users_group){
						
						if (in_array($users_group->data, $objetivos[$i]->grupos_corresponsables)){ ?>

								<div style="clear: both">
									<input type="hidden" value="{{$users_group->data}}" class="grupo_corresponsable">
									<div style="float: left" class="size2">{{$users_group->value}}</div>

							@if (empty($status_plan) || $status_plan[0]->status == 1 || ($status_plan[0]->status == 3 && $peso_periodo == 0) || !empty($editar_objetivos))

									<div style="float: right; height: 20px">
										<a href="javascript:;" class="remove_corresponsable">X</a>
									</div>
							@endif

								</div>
			<?php }
					}
				} ?>

							</div>
<?php } ?>

							<div class="col-md-12">


			@if (empty($status_plan) || $status_plan[0]->status == 1 || ($status_plan[0]->status == 3 && $peso_periodo == 0) || !empty($editar_objetivos))

								<input type="text" class="size2 corresponsables form-control" placeholder="Escribe el nombre del usuario" style="width: 260px; border-radius: 7px">
								<button type="button" class="btn btn-primary add" style="display: none">
									<i class="fas fa-plus-circle"></i> Agregar
								</button>
			@endif

<?php if (!empty($objetivos[$i]->corresponsables)){

				foreach ($users as $key2 => $user){
						
					if (in_array($user->data, $objetivos[$i]->corresponsables)){ ?>

								<div style="clear: both">
									<input type="hidden" value="{{$user->data}}" name="corresponsable">
									<div style="float: left" class="size2">{{$user->value}}</div>

						@if (empty($status_plan) || $status_plan[0]->status == 1 || ($status_plan[0]->status == 3 && $peso_periodo == 0) || !empty($editar_objetivos))

									<div style="float: right; height: 20px">
										<a href="javascript:;" class="remove_corresponsable">X</a>
									</div>
						@endif

								</div>
		<?php }
				}
			}*/ ?>

							</div>
						</div>
					</td>-->
					<!--<td class="text-center" style="padding: 5px">
						<select style="border-radius: 7px" class="tipo_semaforo" <?php //echo (!empty($status_plan) && $status_plan[0]->status >= 2 ? 'disabled' : ($alimentar_propuestas == 1 || ($alimentar_propuestas == 2 && $id_user != auth()->user()->id) ? '' : 'disabled'))?>>

			<?php //if ($objetivos[$i]->tipo != 'Si/No'){ ?>

							<option value="Mas alto">Mas alto</option>
							<option value="Mas bajo" <?php //if ($objetivos[$i]->tipo_semaforo == 'Mas bajo'){ ?>selected="selected"<?php //} ?>>Mas bajo</option>
			<?php //}

			      //else{ ?>

							<option value="Si/No" <?php //if ($objetivos[$i]->tipo_semaforo == 'Si/No'){ ?>selected="selected"<?php //} ?>>Si/No</option>
			<?php //} ?>

						</select>
					</td>-->
					<td class="text-center">

			@if ($objetivos[$i]->tipo_bono != 'Comision')			

				@if (!empty($status_plan) && $status_plan[0]->status > 1)

					@if (empty($objetivos[$i]->valores))

					  <button style="cursor: default" class="text-right btn btn-danger middle_size {{($objetivos[$i]->tipo == '%' ? 'porcentaje' : ($objetivos[$i]->tipo == '$' ? 'currency' : ($objetivos[$i]->tipo == '#' ? 'numeric' : '')))}}">{{$objetivos[$i]->valor_rojo}}</button>
						<button style="cursor: default" class="text-right btn btn-success middle_size {{($objetivos[$i]->tipo == '%' ? 'porcentaje' : ($objetivos[$i]->tipo == '$' ? 'currency' : ($objetivos[$i]->tipo == '#' ? 'numeric' : '')))}}">{{$objetivos[$i]->valor_verde}}</button>
					@else

						<div style="width: 100%; overflow: hidden">
					  	<div style="position: relative; width: {{count($objetivos[$i]->valores) * 100}}%" class="slide text-right">
							
			<?php $espacio = 100 / count($objetivos[$i]->valores); ?>

					  @for ($j = 0;$j < count($objetivos[$i]->valores);$j++)

					  		<div style="width: {{$espacio}}%; float: left">
									<div class="text-center">{{($objetivos[$i]->frecuencia == 'Mensual' ? $meses_completos[$objetivos[$i]->valores[$j]->periodo] : ($objetivos[$i]->frecuencia == 'Trimestral' ? $meses_completos[$objetivos[$i]->valores[$j]->periodo * 3] : ($objetivos[$i]->frecuencia == 'Cuatrimestral' ? $meses_completos[$objetivos[$i]->valores[$j]->periodo * 4] : ($objetivos[$i]->frecuencia == 'Semestral' ? 'Julio' : ''))))}}</div>
									<input type="text" style="width: 43%; border-radius: 7px; border: 1px solid #A5A7A8; background-color: red; color: white" value="{{($objetivos[$i]->tipo == '$' ? '$' . $objetivos[$i]->valores[$j]->valor_rojo : ($objetivos[$i]->tipo == '%' ? $objetivos[$i]->valores[$j]->valor_rojo . '%' : $objetivos[$i]->valores[$j]->valor_rojo))}}" class="text-center" readonly> <input type="text" style="width: 10%; border-radius: 7px; border: 1px solid #A5A7A8; background-color: #FFDB58" readonly> <input type="text" style="width: 43%; border-radius: 7px; border: 1px solid #A5A7A8; background-color: green; color: white" value="{{($objetivos[$i]->tipo == '$' ? '$' . $objetivos[$i]->valores[$j]->valor_verde : ($objetivos[$i]->tipo == '%' ? $objetivos[$i]->valores[$j]->valor_verde . '%' : $objetivos[$i]->valores[$j]->valor_verde))}}" class="text-center" readonly>
								</div>
					  @endfor
					
					  	</div>
					  </div>
					  <div class="text-center" data-valores="{{count($objetivos[$i]->valores)}}" data-step="0">
					  	<span class="fas fa-arrow-left semaforos_arrow_left" style="cursor: pointer"></span> <span class="fas fa-arrow-right semaforos_arrow_right" style="cursor: pointer"></span>
					  </div>
					@endif
				@else

					@if (empty($objetivos[$i]->valores))

						<input type="text" class="valor_rojo form-control size text-right middle_size {{($objetivos[$i]->tipo != 'Si/No' ? ($objetivos[$i]->tipo == '$' ? 'currency' : ($objetivos[$i]->tipo == '#' ? 'numeric' : 'porcentaje')) : '')}}" value="{{$objetivos[$i]->valor_rojo}}" <?php echo (empty($objetivos[$i]->editar) ? 'readonly' : (empty($objetivos[$i]->valor_rojo_catalogo) ? '' : 'readonly'))?> <?php if ($objetivos[$i]->tipo == 'Si/No'){ ?>style="display: none"<?php } ?>>
						<input type="text" class="valor_verde form-control size text-right middle_size {{($objetivos[$i]->tipo != 'Si/No' ? ($objetivos[$i]->tipo == '$' ? 'currency' : ($objetivos[$i]->tipo == '#' ? 'numeric' : 'porcentaje')) : '')}}" value="{{$objetivos[$i]->valor_verde}}" <?php echo (empty($objetivos[$i]->editar) ? 'readonly' : (empty($objetivos[$i]->valor_verde_catalogo) ? '' : 'readonly'))?> <?php if ($objetivos[$i]->tipo == 'Si/No'){ ?>style="display: none"<?php } ?>>
						<select class="valor_rojo form-control middle_size" <?php echo (empty($objetivos[$i]->editar) ? 'disabled' : (empty($objetivos[$i]->valor_rojo_catalogo) ? '' : 'disabled'))?> <?php if ($objetivos[$i]->tipo != 'Si/No'){ ?>style="display: none"<?php } ?>>
							<option value="Si">Si</option>
							<option value="No" <?php if ($objetivos[$i]->valor_rojo == 'No'){ ?>selected="selected"<?php } ?>>No</option>
						</select>
						<select class="valor_verde form-control middle_size" <?php echo (empty($objetivos[$i]->editar) ? 'disabled' : (empty($objetivos[$i]->valor_verde_catalogo) ? '' : 'disabled'))?> <?php if ($objetivos[$i]->tipo != 'Si/No'){ ?>style="display: none"<?php } ?>>
							<option value="Si">Si</option>
							<option value="No" <?php if ($objetivos[$i]->valor_verde == 'No'){ ?>selected="selected"<?php } ?>>No</option>
						</select>

					@else

						<div style="width: 100%; overflow: hidden">
					  	<div style="position: relative; width: {{count($objetivos[$i]->valores) * 100}}%" class="slide text-right">
							
			<?php $espacio = 100 / count($objetivos[$i]->valores); ?>

					  @for ($j = 0;$j < count($objetivos[$i]->valores);$j++)

					  		<div style="width: {{$espacio}}%; float: left">
									<div class="text-center">{{($objetivos[$i]->frecuencia == 'Mensual' ? $meses_completos[$objetivos[$i]->valores[$j]->periodo] : ($objetivos[$i]->frecuencia == 'Trimestral' ? $meses_completos[$objetivos[$i]->valores[$j]->periodo * 3] : ($objetivos[$i]->frecuencia == 'Cuatrimestral' ? $meses_completos[$objetivos[$i]->valores[$j]->periodo * 4] : ($objetivos[$i]->frecuencia == 'Semestral' ? 'Julio' : ''))))}}</div>
									<input type="text" class="valor_rojo form-control middle_size {{($objetivos[$i]->tipo != 'Si/No' ? ($objetivos[$i]->tipo == '$' ? 'currency' : ($objetivos[$i]->tipo == '#' ? 'numeric' : 'porcentaje')) : '')}}" value="{{$objetivos[$i]->valores[$j]->valor_rojo}}" <?php if ($objetivos[$i]->tipo == 'Si/No'){ ?>style="display: none"<?php } ?>>
									<input type="text" class="valor_verde form-control middle_size text-right {{($objetivos[$i]->tipo != 'Si/No' ? ($objetivos[$i]->tipo == '$' ? 'currency' : ($objetivos[$i]->tipo == '#' ? 'numeric' : 'porcentaje')) : '')}}" value="{{$objetivos[$i]->valores[$j]->valor_verde}}" <?php if ($objetivos[$i]->tipo == 'Si/No'){ ?>style="display: none"<?php } ?>>
									<select class="valor_rojo form-control middle_size" <?php if ($objetivos[$i]->tipo != 'Si/No'){ ?>style="display: none"<?php } ?>>
										<option value="Si">Si</option>
										<option value="No" <?php if ($objetivos[$i]->valores[$j]->valor_rojo == 'No'){ ?>selected="selected"<?php } ?>>No</option>
									</select>
									<select class="valor_verde form-control middle_size" <?php if ($objetivos[$i]->tipo != 'Si/No'){ ?>style="display: none"<?php } ?>>
										<option value="Si">Si</option>
										<option value="No" <?php if ($objetivos[$i]->valores[$j]->valor_verde == 'No'){ ?>selected="selected"<?php } ?>>No</option>
									</select>
								</div>
					  @endfor
					
					  	</div>
					  </div>
					  <div class="text-center" data-valores="{{count($objetivos[$i]->valores)}}" data-step="0">
					  	<span class="fas fa-arrow-left semaforos_arrow_left" style="cursor: pointer"></span> <span class="fas fa-arrow-right semaforos_arrow_right" style="cursor: pointer"></span>
					  </div>
					@endif
				@endif
			@endif
					
					</td>
						
				@if (empty($status_plan) || $status_plan[0]->status == 1 || !empty($editar_objetivos))
						
					<td class="text-center">
						
				@if (!empty($objetivos[$i]->editar))
					
					@if (($alimentar_propuestas == 1 || ($alimentar_propuestas == 2 && auth()->user()->employee->idempleado == $user->jefe) || ($alimentar_propuestas == 4 && !empty(auth()->user()->employee->grado)) || auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin')) && !empty($editar_objetivos))
						<button class="btn btn-primary crear editar" id="{{$objetivos[$i]->id}}" style="display: none">✓</button> <button class="btn btn-primary borrar" id="{{$objetivos[$i]->id}}">X</button>
					@endif
				@endif
					</td>
				@endif

				<?php $total_peso += $objetivos[$i]->peso?>

				</tr>
			@endfor
				<tr class="fila_peso_total">
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="text-center" style="padding: 5px; color: white; background-color: #555759">Total:</td>
					<td class="text-right peso_total" style="padding-right: 25px; font-weight: bold; color: black')?>"><?php echo $total_peso?></td>
					<td></td>
					<td></td>
					<!--<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>-->
					
				@if ((empty($status_plan) || $status_plan[0]->status == 1) && !empty($editar_objetivos) && auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin'))
					
					<td></td>
				@endif
					
				</tr>
			</tbody>
		</table>
	<!--</div>-->
	</div>
</div>

@if ((empty($status_plan) || $status_plan[0]->status == 1) && !empty($editar_objetivos))

	@if ($alimentar_propuestas == 1 || ($alimentar_propuestas == 2 && auth()->user()->employee->idempleado == $user->jefe) || ($alimentar_propuestas == 4 && !empty(auth()->user()->employee->grado)) || auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin'))

<div class="row">

			@if (count($catalogo_objetivos))

	<div class="col-md-4">
		<button class="btn btn-primary agregar_propuesta_catalogo">1. Agregar objetivo de catálogo</button>
	</div>
	<div class="col-md-4">
		<button class="btn btn-primary agregar_propuesta">1. Agregar objetivo</button>
	</div>
	<div class="col-md-4 text-right">
		<button class="btn btn-primary guardar_objetivos">2. Guardar objetivos</button>
	</div>
			@else

	<div class="col-md-6">
		<button class="btn btn-primary agregar_propuesta">1. Agregar objetivo</button>
	</div>
	<div class="col-md-6 text-right">
		<button class="btn btn-primary guardar_objetivos">2. Guardar objetivos</button>
	</div>
			@endif

</div>
	@endif
@endif

<div class="col-md-12 text-center">

  @if (empty($status_plan) || $status_plan[0]->status == 1 || !empty($editar_objetivos))

  	@if ($alimentar_propuestas == 1 || ($alimentar_propuestas == 2 && auth()->user()->employee->idempleado == $user->jefe) || ($alimentar_propuestas == 4 && !empty(auth()->user()->employee->grado)) || auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin'))
		<div class="margin-top-20 solicitarAprobacionForm_container">
			<form action="solicitar-autorizacion" method="post" id="solicitarAprobacionForm">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_plan" value="{{$plan->id}}">
				<input type="hidden" name="periodo" value="{{(!empty($status_plan) ? $status_plan[0]->periodo : 1)}}">
				<input type="hidden" name="id_empleado" value="{{$id_user}}">
				<button class="btn btn-primary solicitar_validacion" type="submit" <?php if (count($objetivos) == 0 || (!empty($plan->total_peso_objetivos) && $total_peso != $plan->total_peso_objetivos)){ ?>style="display: none"<?php } ?>><?php if ($plan->alimentar_propuestas == $plan->autorizar_propuestas){ ?>3. Listo para registrar logros<?php } else{ ?>3. Solicitar validación<?php } ?></button>
			</form>
		</div>
		@else
	  		@if (!empty($status_plan))
		<div class="margin-top-20">

				@if ($status_plan[0]->status > 2)

				<p>Fecha de Autorización</p>
				<p>{{substr($status_plan[0]->fecha_autorizado, 8)}} de {{$meses_completos[substr($status_plan[0]->fecha_autorizado, 5, 2) * 1]}} del {{substr($status_plan[0]->fecha_autorizado, 0, 4)}}</p>
				@else

				<p>Fecha de Solicitud</p>
				<p>{{substr($status_plan[0]->fecha_solicitado, 8)}} de {{$meses_completos[substr($status_plan[0]->fecha_solicitado, 5, 2) * 1]}} del {{substr($status_plan[0]->fecha_solicitado, 0, 4)}}</p>
				@endif

		</div>
			@endif
		@endif
	@else
	  @if (!empty($status_plan))
			<div class="margin-top-20">

				@if ($status_plan[0]->status > 2)

				<p>Fecha de Autorización</p>
				<p>{{substr($status_plan[0]->fecha_autorizado, 8)}} de {{$meses_completos[substr($status_plan[0]->fecha_autorizado, 5, 2) * 1]}} del {{substr($status_plan[0]->fecha_autorizado, 0, 4)}}</p>
				@else

				<p>Fecha de Solicitud</p>
				<p>{{substr($status_plan[0]->fecha_solicitado, 8)}} de {{$meses_completos[substr($status_plan[0]->fecha_solicitado, 5, 2) * 1]}} del {{substr($status_plan[0]->fecha_solicitado, 0, 4)}}</p>
				@endif

			</div>
		@endif
	@endif
	</div>

</div>
</div>
@if (!empty($objetivos_corresponsables))


<div class="card mt-3">
	<h5 class="card-header bg-info text-white font-weight-bolder">
		Objetivos Corresponsable
		</h5>
	<div class="card-body">

 
<div style="overflow: auto">
	<table class="tabla_compromisos table table-striped table-bordered table-hover" style="margin-bottom: 0">
		<thead style="background-color: #222B64; color:white">
			<tr>
				<th class="cabeceras-evaluaciones text-center">Clave</th>
				<th class="cabeceras-evaluaciones text-center">Objetivo</th>
				<th class="cabeceras-evaluaciones text-center">Corresponsables</th>
				<th class="cabeceras-evaluaciones text-center">Frecuencia</th>
				<th class="cabeceras-evaluaciones text-center">Meta</th>
				<th class="cabeceras-evaluaciones text-center">Unidad de medida</th>
				<th class="cabeceras-evaluaciones text-center">
					<p style="font-size: 13px">Semáforo de Desempeño</p>
							<div>
								<span style="background-color: red; padding: 5px 10px"></span><span style="background-color: #FFDB58; color: #FFDB58; padding: 5px 10px"></span><span style="background-color: green; padding: 5px 10px"></span>
							</div>
						</th>
					</tr>
				</thead>
				<tbody>

	<?php foreach ($objetivos_corresponsables as $key => $value){
			
					$current_type = 0;
					$counter = 0; ?>
			
					<tr class="objetivo_corresponsable">
						<td class="text-center">
							<select class="text-center form-control size3">

		<?php foreach ($objetivos_corporativos_sin_responsable as $key2 => $objetivo_corporativo){

						if ($current_type != $objetivo_corporativo->id_tipo){

							$counter = 1;
							$current_type = $objetivo_corporativo->id_tipo;
						}

						if ($objetivo_corporativo->id == $value->id_objetivo_corporativo){ ?>
						
								<option value="{{$value->id_objetivo_corporativo}}">{{$objetivo_corporativo->letter}}{{$counter}}</option>
			<?php }

						$counter++;
					} ?>
							
							</select>
						</td>
						<td class="text-center">
							<textarea class="form-control size2" readonly>{{$value->nombre}}</textarea>
						</td>
						<td class="text-center">
							<div class="row">

		<?php if (count($users_groups) > 0){ ?>

								<div class="col-md-12">

			<?php if (!empty($value->grupos_corresponsables)){

							foreach ($users_groups as $key2 => $users_group){
						
								if (in_array($users_group->data, $value->grupos_corresponsables)){ ?>

									<div style="clear: both">
										<div style="float: left" class="size2">{{$users_group->value}}</div>
									</div>
					<?php }
							}
						} ?>

								</div>
		<?php } ?>

								<div class="col-md-12">
										
		<?php if (!empty($value->corresponsables)){

						foreach ($users as $key2 => $user){
						
							if (in_array($user->data, $value->corresponsables)){ ?>

									<div style="clear: both">
										<div style="float: left" class="size2">{{$user->value}}</div>
									</div>
				<?php }
						}
					} ?>

								</div>
							</div>
						</td>
						<td class="text-center">
							<select class="text-center form-control size">
								<option>{{$value->frecuencia}}</option>
							</select>
						</td>
						<td class="text-center">

					@if ($value->tipo != 'Si/No')

								<input type="text" class="form-control size text-right" value="{{$value->objetivo}}" readonly style="width: 90px">
					@else

								<select class="form-control size" style="width: 90px">
									<option>{{$value->objetivo}}</option>
								</select>
					@endif

						</td>
						<td class="text-center">
							<select class="text-center form-control size3 tipo">
								<option>{{($value->tipo == '#' ? '$' : ($value->tipo == '9' ? '0' : ($value->tipo == 'Si/No' ? 'Si / No' : '%')))}}</option>
							</select>
						</td>
						<td class="text-center">
							<div style="min-width: 190px">
								<button style="cursor: default" class="btn btn-danger">{{($value->tipo == '%' ? number_format($value->valor_rojo, 2, '.', ',') . '%' : ($value->tipo == '#' ? '$' . number_format($value->valor_rojo) : ($value->tipo == '9' ? number_format($value->valor_rojo) : $value->valor_rojo)))}}</button>
								<button style="cursor: default" class="btn btn-success">{{($value->tipo == '%' ? number_format($value->valor_verde, 2, '.', ',') . '%' : ($value->tipo == '#' ? '$' . number_format($value->valor_verde) : ($value->tipo == '9' ? number_format($value->valor_verde) : $value->valor_verde)))}}</button>
							</div>
						</td>
					</tr>
	<?php } ?>

				</tbody>
			</table>
		</div>
		</div>
		</div>
			@endif



			<div class="card mt-3">
				<h5 class="card-header bg-info text-white font-weight-bolder">
					Observaciones
					</h5>
				<div class="card-body">
			

<div class="row">
	<div class="col-md-12">

		<div style="max-height: 200px; overflow-y: auto">
			<table width="100%" style="font-size: 13px" border="1" class="table table-bordered table-hover table-striped notas">
				<thead style="background-color: #222B64; color:white">
					<tr>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Fecha</th>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Persona</th>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Nota</th>
					</tr>
				</thead>
				<tbody>

      	<?php $meses = array('0', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');?>

      	@for ($i = 0;$i < count($mensajes); $i++) 

					<tr>
						<td class="text-center" style="padding: 5px; border: 0">{{substr($mensajes[$i]->fecha, 8) * 1}}/{{$meses[substr($mensajes[$i]->fecha, 5, 2) * 1]}}/{{substr($mensajes[$i]->fecha, 0, 4)}}</td>
						<td class="text-center" style="padding: 5px; border: 0">{{$mensajes[$i]->nombre}} {{$mensajes[$i]->paterno}} {{$mensajes[$i]->materno}}</td>
						<td class="text-center" style="padding: 5px; border: 0">{{$mensajes[$i]->mensaje}}</td>
					</tr>
				@endfor

				</tbody>
			</table>
		</div>
		<h4 class="titulos-evaluaciones">Nota</h4>
		<div>
			<textarea style="width: 100%; height: 100px; border-radius: 7px" class="nota"></textarea>
		</div>
		<div class="text-right margin-top-10">
			<button class="btn btn-primary agregar_nota"><i class="fas fa-plus-circle"></i> Agregar nota</button>
		</div>
	</div>
</div>
</div>
</div>

@if (!empty($objetivos_corporativos))




<?php $cells = array();
	$counter = 0;
	$current_type = 0;
	$maximum = 0; ?>

	@foreach ($objetivos_corporativos as $key => $objetivo_corporativo)
			
		@if ($current_type != $objetivo_corporativo->id_tipo)

			@if ($current_type != 0)

				@if ($counter > $maximum)

		<?php $maximum = $counter; ?>
				@endif

	<?php $cells[] = $counter; ?>
			@endif

<?php $current_type = $objetivo_corporativo->id_tipo;
			$counter = 0; ?>
		@endif

<?php $counter++; ?>
	@endforeach
		
	@if ($current_type != 0)

		@if ($counter > $maximum)

<?php $maximum = $counter; ?>
		@endif

<?php $cells[] = $counter; ?>
	@endif

<?php $counter = 0; ?>
<div class="card mt-3">
	<h5 class="card-header bg-info text-white font-weight-bolder">
		Objetivos Corporativos
		</h5>
	<div class="card-body"> 

<div style="overflow-x: auto">
<table class="table">
	<thead>
		<tr>

	@foreach ($cells as $key => $cell)
		
			<th colspan="2" style="color: white; background-color: {{$objetivos_corporativos[$counter]->color}}">{{$objetivos_corporativos[$counter]->nombre_tipo}}</th>
<?php $counter = $counter + $cell; ?>
	@endforeach					

		</tr>
	</thead>
	<tbody>

<?php $counter = 0; ?>

	@for ($i = 0;$i < $maximum;$i++)

		<tr>

<?php $counter = 0; ?>

		@foreach ($cells as $key => $cell)

			@if ($cell > $i)

			<td style="color: white; background-color: {{$objetivos_corporativos[$counter]->color}}">{{$objetivos_corporativos[$counter + $i]->letter}}{{$i + 1}}</td>
			<td>{{$objetivos_corporativos[$counter + $i]->name}}</td>
			@else

			<td></td>
			<td></td>
			@endif

<?php $counter = $counter + $cell; ?>
		@endforeach

		</tr>
	@endfor

	</tbody>
</table>
</div>
</div>
</div>
@endif

@else

<div class="row">
				
	<div class="col-md-12 text-center">
		
		<div class="alert alert-warning h4" role="alert">
			<i class="fa fa-exclamation-triangle"></i>	No hay planes abiertos
		</div>

	</div>
</div>
 
@endif

@if (count($catalogo_objetivos) > 0)

<div class="card mt-3" style="display: none">
	<h5 class="card-header bg-info text-white font-weight-bolder">
		Objetivos Corporativos
		</h5>
	<div class="card-body"> 
<table style="display: none" class="propuesta_catalogo">
	<tr class="objetivos">
		<td class="text-center"></td>
		<td class="text-center">{{(!empty($plan) ? $plan->periodo : '')}}</td>
		<td class="text-center">
			<select class="id_catalogo form-control size2">
				<option value="0">--Selecciona--</option>

	@for ($i = 0;$i < count($catalogo_objetivos);$i++)

				<option value="{{$catalogo_objetivos[$i]->id}}">{{$catalogo_objetivos[$i]->objetivo}}</option>

	<?php $counter++; ?>
			@endfor

			</select>
		</td>
		<td class="text-center" >
			<select class="id_objetivo_corporativo form-control text-center size3" disabled>

<?php $current_type = $counter = 0; ?>

			@for ($j = 0;$j < count($objetivos_corporativos_sin_responsable); $j++)

				@if ($current_type != $objetivos_corporativos_sin_responsable[$j]->id_tipo)

		<?php $counter = 1;
					$current_type = $objetivos_corporativos_sin_responsable[$j]->id_tipo; ?>
				@endif

				<option value="{{$objetivos_corporativos_sin_responsable[$j]->id}}">{{$objetivos_corporativos_sin_responsable[$j]->letter}}{{$counter}}</option>

	<?php $counter++; ?>
			@endfor

			</select>
		</td>
		<td class="text-center">
			<textarea style="resize: none; overflow-x: hidden" class="formula form-control size2"></textarea>
		</td>
		<td class="text-center">
			<select class="tipo form-control size3" disabled>
				<option value="%">%</option>
				<option value="$">$</option>
				<option value="#">#</option>
				<option value="Si/No">Si/No</option>
			</select>
		</td>
		<td class="text-center">
			<input type="text" class="objetivo size text-right form-control porcentaje" readonly>
			<select class="objetivo form-control size" style="display: none" disabled>
				<option value="Si">Si</option>
				<option value="No">No</option>
			</select>
		</td>
		<td class="text-center">
			<input type="text" class="peso numeric periodo{{$periodo}} form-control size3 text-right" readonly>
		</td>
		<td class="text-center">
			<select class="frecuencia form-control size">
				<option value="Mensual">Mensual</option>
				<option value="Bimestral">Bimestral</option>
				<option value="Trimestral">Trimestral</option>
				<option value="Cuatrimestral">Cuatrimestral</option>
				<option value="Semestral">Semestral</option>
				<option value="Anual">Anual</option>
			</select>
		</td>
		<!--<td class="text-center">
			<div class="row">

<?php //if (count($users_groups) > 0){ ?>

				<div class="col-md-12">
					<input type="text" class="size2 grupos_corresponsables form-control" placeholder="Escribe el nombre del grupo" style="width: 260px; border-radius: 7px">
					<button type="button" class="btn btn-primary add_grupo" style="display: none">
						<i class="fas fa-plus-circle"></i> Agregar
					</button>
				</div>
<?php //} ?>

				<div class="col-md-12">
					<input type="text" class="size2 corresponsables form-control" placeholder="Escribe el nombre del usuario" style="width: 260px; border-radius: 7px">
					<button type="button" class="btn btn-primary add" style="display: none">
						<i class="fas fa-plus-circle"></i> Agregar
					</button>
				</div>
			</div>
		</td>-->
		<td class="text-center">
			<input type="text" class="valor_rojo text-right form-control middle_size porcentaje" readonly> <input type="text" class="valor_verde text-right form-control middle_size porcentaje" readonly>
			<select class="valor_rojo form-control middle_size" style="display: none" disabled>
				<option value="No">No</option>
				<option value="Si">Si</option>
			</select>
			<select class="valor_verde form-control middle_size" style="display: none" disabled>
				<option value="Si">Si</option>
				<option value="No">No</option>
			</select>
		</td>
		<td class="text-center">
			<button class="btn btn-primary crear" style="display: none">✓</button> <button class="btn btn-primary remover">X</button>
		</td>
	</tr>
</table>
</div>
</div>
@endif

<table style="display: none" class="propuesta">
	<tr class="objetivos">
		<td class="text-center"></td>
		<td class="text-center">{{(!empty($plan) ? $plan->periodo : '')}}</td>
		<td class="text-center">
			<textarea style="overflow-x: hidden; resize: none" class="nombre form-control size2"></textarea>
		</td>
		<td class="text-center" >
			<select class="id_objetivo_corporativo form-control text-center size3">

<?php $current_type = $counter = 0;
			$repetidos = array(); ?>

			@for ($j = 0;$j < count($objetivos_corporativos_sin_responsable); $j++)

				@if ($current_type != $objetivos_corporativos_sin_responsable[$j]->id_tipo)

		<?php $counter = 1;
					$current_type = $objetivos_corporativos_sin_responsable[$j]->id_tipo; ?>
				@endif

				@if (!in_array($objetivos_corporativos_sin_responsable[$j]->letter . $counter, $repetidos))

			<?php $repetidos[] = $objetivos_corporativos_sin_responsable[$j]->letter . $counter; ?>

				<option value="{{$objetivos_corporativos_sin_responsable[$j]->id}}">{{$objetivos_corporativos_sin_responsable[$j]->letter}}{{$counter}}</option>
				@endif

	<?php $counter++; ?>
			@endfor

			</select>
		</td>
		<td class="text-center">
			<textarea style="resize: none; overflow-x: hidden" class="formula form-control size2"></textarea>
		</td>
		<td class="text-center">
			<select class="tipo form-control size3">
				<option value="%">%</option>
				<option value="$">$</option>
				<option value="#">#</option>
				<option value="Si/No">Si/No</option>
			</select>
		</td>
		<td class="text-center">
			<input type="text" class="objetivo size text-right form-control porcentaje">
			<select class="objetivo form-control size" style="display: none">
				<option value="Si">Si</option>
				<option value="No">No</option>
			</select>
		</td>
		<td class="text-center">
			<input type="text" class="peso numeric periodo{{$periodo}} form-control size3 text-right">
		</td>
		<td class="text-center">
			<select class="frecuencia form-control size">
				<option value="Mensual">Mensual</option>
				<option value="Bimestral">Bimestral</option>
				<option value="Trimestral">Trimestral</option>
				<option value="Cuatrimestral">Cuatrimestral</option>
				<option value="Semestral">Semestral</option>
				<option value="Anual">Anual</option>
			</select>
		</td>
		<!--<td class="text-center">
			<div class="row">

<?php //if (count($users_groups) > 0){ ?>

				<div class="col-md-12">
					<input type="text" class="size2 grupos_corresponsables form-control" placeholder="Escribe el nombre del grupo" style="width: 260px; border-radius: 7px">
					<button type="button" class="btn btn-primary add_grupo" style="display: none">
						<i class="fas fa-plus-circle"></i> Agregar
					</button>
				</div>
<?php //} ?>

				<div class="col-md-12">
					<input type="text" class="size2 corresponsables form-control" placeholder="Escribe el nombre del usuario" style="width: 260px; border-radius: 7px">
					<button type="button" class="btn btn-primary add" style="display: none">
						<i class="fas fa-plus-circle"></i> Agregar
					</button>
				</div>
			</div>
		</td>-->
		<td class="text-center">
			<input type="text" class="valor_rojo text-right form-control middle_size porcentaje"> <input type="text" class="valor_verde text-right form-control middle_size porcentaje">
			<select class="valor_rojo form-control middle_size" style="display: none" disabled>
				<option value="No">No</option>
				<option value="Si">Si</option>
			</select>
			<select class="valor_verde form-control middle_size" style="display: none" disabled>
				<option value="Si">Si</option>
				<option value="No">No</option>
			</select>
		</td>
		<td class="text-center">
			<button class="btn btn-primary crear" style="display: none">✓</button> <button class="btn btn-primary remover">X</button>
		</td>
	</tr>
</table>
</div>
</div>
</div>
</div>
@endsection

@section('scripts')
	<script>

	  var id_plan = <?php echo (!empty($plan->id) ? $plan->id : 0)?>;
	  var total_peso_objetivos = <?php echo $plan->total_peso_objetivos?>;
	  var periodo = <?php echo (!empty($plan) ? $plan->periodo : 1)?>;
          var objetivos = <?php echo (!empty($objetivos) ? json_encode($objetivos) : 0)?>;
          var peso_acumulado = 0;
		  var status_plan = <?php echo (empty($status_plan) || $status_plan[0]->status == 1 || ($status_plan[0]->status == 3 && $peso_periodo == 0) || !empty($editar_objetivos) ? 0 : 1)?>;
		  var id_user = <?php echo $id_user?>;
		  var nueva_propuesta = '';
		  var nueva_propuesta_catalogo = '';
		  var guardar_objetivos = false;
		  var num_objetivos = 0;
		  var objetivos_guardados = 0;
		var nombre = '';
		var id = 0;
		var catalogo_objetivos = <?php echo json_encode($catalogo_objetivos)?>;

		// Lista de usuarios para el autocomplete
		var users_groups = <?php echo json_encode($users_groups)?>;

		var total_bono = 0;
		var id_plan_para_copiar = <?php echo (count($planes_para_copiar) > 0 ? $planes_para_copiar[0]->id : 0)?>;
		var users_plans = <?php echo json_encode($users_plans)?>;
		var users_with_objetives = <?php echo json_encode($users_with_objetives)?>;
		
		$(document).ready(function(){

			/*$.ajaxSetup({
    		headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
			});*/
			
			nueva_propuesta = $('table.propuesta tr');
			nueva_propuesta_catalogo = $('table.propuesta_catalogo tr');
			
			if (status_plan == 1){
			
				$('.tabla_objetivos').DataTable({
    			scrollX: true,
    			scrollCollapse: true,
    			paging: false,
    			searching: false,
    			ordering: false,
    			info: false
				});
			}

			/*$('.corresponsables').autocomplete({
    		lookup: autocomplete_users,
    		onSelect: function (suggestion){
        
        	nombre = suggestion.value;
    			id = suggestion.data;
    			$(this).next().show();
    		}
			});

			$('.grupos_corresponsables').autocomplete({
    		lookup: users_groups,
    		onSelect: function (suggestion){
        
        	nombre = suggestion.value;
    			id = suggestion.data;
    			$(this).next().show();
    		}
			});*/

			// Quita un usuario o un grupo
			/*$('body').on('click', 'a.remove_corresponsable', function(){

				$(this).parent().parent().remove();
			});*/

			// Agrega un nuevo corresponsable a un objetivo
			/*$('body').on('click', 'button.add', function(){

				$(this).prev().val('');
				$(this).parent().append('<div style="clear: both"><input type="hidden" value="' + id + '" class="corresponsable"><div style="float: left">' + nombre + '</div><div style="float: right; height: 20px"><a href="javascript:;" class="remove_corresponsable">X</a></div></div>');
				$(this).hide();
			});*/

			// Agrega un nuevo grupo corresponsable a una responsabilidad
			/*$('body').on('click', 'button.add_grupo', function(){

				$(this).prev().val('');
				$(this).parent().append('<div style="clear: both"><input type="hidden" value="' + id + '" class="grupo_corresponsable"><div style="float: left">' + nombre + '</div><div style="float: right; height: 20px"><a href="javascript:;" class="remove_corresponsable">X</a></div></div>');
				$(this).hide();
			});*/
			
			// Clic en la flecha izquierda debajo de los valores rojo y verde de un objetivo
			$('span.semaforos_arrow_left').click(function(){
				
				var total = $(this).parent().attr('data-valores');
				var step = $(this).parent().attr('data-step');
				
				if (step == 0){
					
					step = (total - 1) * -1;
				}
				
				else{
					
					step--;
					step = step * -1;
				}

				$(this).parent().attr('data-step', Math.abs(step));
				$(this).parent().prev().find('.slide').animate({"left":(step * 100) + '%'}, "slow");

				if (!slide){

					slide = true;
					//$('span.metas_arrow_left').click();
				}

				else{

					slide = false;
				}
			});

			// Clic en la flecha derecha debajo de los valores rojo y verde de un objetivo
			$('span.semaforos_arrow_right').click(function(){
				
				var total = $(this).parent().attr('data-valores');
				var step = $(this).parent().attr('data-step');
				
				if (step == (total - 1)){
					
					step = 0;
				}
				
				else{
					
					step++;
					step = step * -1;
				}

				$(this).parent().attr('data-step', Math.abs(step));
				$(this).parent().prev().find('.slide').animate({"left":(step * 100) + '%'}, "slow");

				if (!slide){

					slide = true;
					//$('span.metas_arrow_right').click();
				}

				else{

					slide = false;
				}
			});

			// Clic en la flecha izquierda debajo de las metas de un objetivo
			$('span.metas_arrow_left').click(function(){
				
				var total = $(this).parent().attr('data-valores');
				var step = $(this).parent().attr('data-step');
				
				if (step == 0){
					
					step = (total - 1) * -1;
				}
				
				else{
					
					step--;
					step = step * -1;
				}

				$(this).parent().attr('data-step', Math.abs(step));
				$(this).parent().prev().find('.slide').animate({"left":(step * 100) + '%'}, "slow");

				if (!slide){

					slide = true;
					//$('span.semaforos_arrow_left').click();
				}

				else{

					slide = false;
				}
			});

			// Clic en la flecha derecha debajo de las metas de un objetivo
			$('span.metas_arrow_right').click(function(){
				
				var total = $(this).parent().attr('data-valores');
				var step = $(this).parent().attr('data-step');
				
				if (step == (total - 1)){
					
					step = 0;
				}
				
				else{
					
					step++;
					step = step * -1;
				}

				$(this).parent().attr('data-step', Math.abs(step));
				$(this).parent().prev().find('.slide').animate({"left":(step * 100) + '%'}, "slow");

				if (!slide){

					slide = true;
					//$('span.semaforos_arrow_right').click();
				}

				else{

					slide = false;
				}
			});

			$('select.id_plan').change(function(){

				$('form#changePlanForm').submit();
			});

			$('body').on('change', 'select.id_catalogo', function(){

				var id_catalogo = $(this).val();

				for (var i = 0; i < catalogo_objetivos.length;i++){
					
					if (catalogo_objetivos[i].id == id_catalogo){

						if (catalogo_objetivos[i].id_objetivo_corporativo !== null && catalogo_objetivos[i].id_objetivo_corporativo !== undefined){

							$(this).parent().parent().find('select.id_objetivo_corporativo').val(catalogo_objetivos[i].id_objetivo_corporativo);
							$(this).parent().parent().find('select.id_objetivo_corporativo').change();
							$(this).parent().parent().find('select.id_objetivo_corporativo').attr('disabled', 'disabled');
						}

						else{

							$(this).parent().parent().find('select.id_objetivo_corporativo').removeAttr('disabled');
						}

						if (catalogo_objetivos[i].unidad !== null && catalogo_objetivos[i].unidad !== undefined && catalogo_objetivos[i].unidad.length > 0){

							$(this).parent().parent().find('select.tipo').val(catalogo_objetivos[i].unidad);
							$(this).parent().parent().find('select.tipo').change();
							$(this).parent().parent().find('select.tipo').attr('disabled', 'disabled');
						}

						else{

							$(this).parent().parent().find('select.tipo').removeAttr('disabled');
						}

						if (catalogo_objetivos[i].meta !== null && catalogo_objetivos[i].meta != ''){

							if (catalogo_objetivos[i].meta != 'Si' && catalogo_objetivos[i].meta != 'No'){

								$(this).parent().parent().find('input.objetivo').val(catalogo_objetivos[i].meta);
								$(this).parent().parent().find('input.objetivo').attr('readonly', 'readonly');
							}

							else{

								$(this).parent().parent().find('select.objetivo').val(catalogo_objetivos[i].meta);
								$(this).parent().parent().find('select.objetivo').attr('disabled', 'disabled');
							}
						}

						else{

							$(this).parent().parent().find('input.objetivo').removeAttr('readonly');
							$(this).parent().parent().find('select.objetivo').removeAttr('disabled');
						}

						if (catalogo_objetivos[i].peso !== null && catalogo_objetivos[i].peso !== undefined){

							$(this).parent().parent().find('input.peso').val(catalogo_objetivos[i].peso);
							$(this).parent().parent().find('input.peso').attr('readonly', 'readonly');
						}

						else{

							$(this).parent().parent().find('input.peso').removeAttr('readonly');
						}

						if (catalogo_objetivos[i].frecuencia !== null && catalogo_objetivos[i].frecuencia !== undefined){

							$(this).parent().parent().find('select.frecuencia').val(catalogo_objetivos[i].frecuencia);
							$(this).parent().parent().find('select.frecuencia').change();
							$(this).parent().parent().find('select.frecuencia').attr('disabled', 'disabled');
						}

						else{

							$(this).parent().parent().find('select.frecuencia').removeAttr('disabled');
						}

						if (catalogo_objetivos[i].valor_rojo !== null && catalogo_objetivos[i].valor_rojo != ''){

							if (catalogo_objetivos[i].valor_rojo != 'Si' && catalogo_objetivos[i].valor_rojo != 'No'){

								$(this).parent().parent().find('input.valor_rojo').val(catalogo_objetivos[i].valor_rojo);
								$(this).parent().parent().find('input.valor_rojo').attr('readonly', 'readonly');
							}

							else{

								$(this).parent().parent().find('select.valor_rojo').val(catalogo_objetivos[i].valor_rojo);
								$(this).parent().parent().find('select.valor_rojo').attr('disabled', 'disabled');
							}
						}

						else{

							$(this).parent().parent().find('input.valor_rojo').removeAttr('readonly');
							$(this).parent().parent().find('select.valor_rojo').removeAttr('disabled');
						}

						if (catalogo_objetivos[i].valor_verde !== null && catalogo_objetivos[i].valor_verde !== ''){

							if (catalogo_objetivos[i].valor_verde != 'Si' && catalogo_objetivos[i].valor_verde != 'No'){

								$(this).parent().parent().find('input.valor_verde').val(catalogo_objetivos[i].valor_verde);
								$(this).parent().parent().find('input.valor_verde').attr('readonly', 'readonly');
							}

							else{

								$(this).parent().parent().find('select.valor_verde').val(catalogo_objetivos[i].valor_verde);
								$(this).parent().parent().find('select.valor_verde').attr('disabled', 'disabled');
							}
						}

						else{

							$(this).parent().parent().find('input.valor_verde').removeAttr('readonly');
							$(this).parent().parent().find('select.valor_verde').removeAttr('disabled');
						}
					}
				};
			});

			$('button.agregar_propuesta, button.agregar_propuesta_catalogo').click(function(){
					
				if ($(this).hasClass('agregar_propuesta')){

					$('table.tabla_objetivos tr.fila_peso_total').before('<tr class="objetivos">' + nueva_propuesta.html() + '</tr>');
				}

				else{

					$('table.tabla_objetivos tr.fila_peso_total').before('<tr class="objetivos">' + nueva_propuesta_catalogo.html() + '</tr>');
				}

				var counter = 1;

               $('table.tabla_objetivos tr.objetivos').each(function(){

                 $(this).find('td:nth-child(1)').text(counter);
                 counter++;
               });
				
			   var filas = $('table.tabla_objetivos tbody tr.objetivos').length;

        	   if (filas > 1){

        			$('table.tabla_objetivos tbody tr.objetivos:nth-child(' + filas + ') select.frecuencia').val($('table.tabla_objetivos tbody tr.objetivos:nth-child(1) select.frecuencia').val());
        	   }

        /*$('.corresponsables').autocomplete({
    			lookup: autocomplete_users,
    			onSelect: function (suggestion){
        
        		nombre = suggestion.value;
    				id = suggestion.data;
    				$(this).next().show();
    			}
				});

				$('.grupos_corresponsables').autocomplete({
    			lookup: users_groups,
    			onSelect: function (suggestion){
        
        		nombre = suggestion.value;
    				id = suggestion.data;
    				$(this).next().show();
    			}
				});*/

				$('input.porcentaje').inputmask("numeric", {
    			radixPoint: ".",
    			groupSeparator: ",",
    			digits: 2,
    			autoGroup: true,
    			suffix: '%', //No Space, this will truncate the first character
    			rightAlign: false,
    			oncleared: function () { self.Value(''); }
				});

				$('input.currency').inputmask("numeric", {
    			radixPoint: ".",
    			groupSeparator: ",",
    			digits: 2,
    			autoGroup: true,
    			prefix: '$', //No Space, this will truncate the first character
    			rightAlign: false,
    			oncleared: function () { self.Value(''); }
				});

				$('input.numeric').inputmask("numeric", {
    			groupSeparator: ",",
    			autoGroup: true,
    			rightAlign: false,
    			oncleared: function () { self.Value(''); }
				});
			});

			$('body').on('click', 'button.remover', function(){

				$(this).parent().parent().remove();
				
				var counter = 1;

               $('table.tabla_objetivos tr.objetivos').each(function(){

                 $(this).find('td:nth-child(1)').text(counter);
                 counter++;
               });
			});

			$('body').on('change', '.tipo', function(){

				var padre = $(this).parent().parent();

				if ($(this).val() == 'Si/No'){

					padre.find('input.objetivo, input.valor_rojo, input.valor_verde').val('').hide();
					padre.find('select.objetivo, select.valor_rojo, select.valor_verde').show();
					//padre.find('select.tipo_semaforo option[value="Mas alto"]').remove();
					//padre.find('select.tipo_semaforo option[value="Mas bajo"]').remove();
					//padre.find('select.tipo_semaforo').append('<option value="Si/No">Si/No</option>');
				}

				else{

					padre.find('select.objetivo, select.valor_rojo, select.valor_verde').hide();
					padre.find('input.objetivo, input.valor_rojo, input.valor_verde').show();
					padre.find('input.objetivo, input.valor_rojo, input.valor_verde').removeClass('currency').removeClass('porcentaje').removeClass('numeric');

					if ($(this).val() == '$'){

						padre.find('input.objetivo, input.valor_rojo, input.valor_verde').addClass('currency');
					}

					else{

						if ($(this).val() == '%'){

							padre.find('input.objetivo, input.valor_rojo, input.valor_verde').addClass('porcentaje');
						}

						else{

							padre.find('input.objetivo, input.valor_rojo, input.valor_verde').addClass('numeric');
						}
					}

					$('input.porcentaje').inputmask("numeric", {
    				radixPoint: ".",
    				groupSeparator: ",",
    				digits: 2,
    				autoGroup: true,
    				suffix: '%', //No Space, this will truncate the first character
    				rightAlign: false,
    				oncleared: function () { self.Value(''); }
					});

					$('input.currency').inputmask("numeric", {
    				radixPoint: ".",
    				groupSeparator: ",",
    				digits: 2,
    				autoGroup: true,
    				prefix: '$', //No Space, this will truncate the first character
    				rightAlign: false,
    				oncleared: function () { self.Value(''); }
					});

					$('input.numeric').inputmask("numeric", {
    				groupSeparator: ",",
    				autoGroup: true,
    				rightAlign: false,
    				oncleared: function () { self.Value(''); }
					});
					//padre.find('select.tipo_semaforo option[value="Si/No"]').remove();

					/*if (padre.find('select.tipo_semaforo option[value="Mas alto"]').length == 0){

						padre.find('select.tipo_semaforo').append('<option value="Mas alto">Mas alto</option>');
						padre.find('select.tipo_semaforo').append('<option value="Mas bajo">Mas bajo</option>');
					}*/
				}
			});
			
			// Cambia el métrico de Si/No
			$('body').on('change', 'select.objetivo', function(){

				var padre = $(this).parent().parent();

				if ($(this).val() == 'Si'){

					padre.find('select.valor_verde').val('Si');
					padre.find('select.valor_rojo').val('No');
				}

				else{

					padre.find('select.valor_verde').val('No');
					padre.find('select.valor_rojo').val('Si');
				}
			});

			$('body').on('keyup', 'input.objetivo', function(){

				var padre = $(this).parent().parent();
				padre.find('input.valor_verde').val($(this).val());
				//padre.find('input.valor_rojo').val($(this).val());
			});

			$('body').on('click', 'button.crear', function(){

				var propuesta = $(this).parent().parent();
					var nombre = '';
					var formula = '';
					var tipo = propuesta.find('.tipo').val();
					var id = 0;
					var id_catalogo = 0;
					var button = $(this);
					var corresponsables = '';
					var grupos_corresponsables = '';
					var niveles_logro = '';
					var porcentajes_bono = '';

					if ($(this).attr('id')){

						id = $(this).attr('id');
						nombre = propuesta.find('.nombre').val();
					}

					else{

						if (propuesta.find('.id_catalogo').length > 0){

							id_catalogo = propuesta.find('.id_catalogo').val();

							if (id_catalogo == 0){

								alert('Selecciona un objetivo de catálogo');
								propuesta.find('.id_catalogo').focus();
								return false;
							}

							nombre = propuesta.find('.id_catalogo option[value="' + id_catalogo +'"]').text();
						}

						else{

							nombre = propuesta.find('.nombre').val();
						}
					}

					if (nombre.trim().length == 0){

						alert('Escribe el objetivo');
						propuesta.find('nombre').focus();
						return false;
					}

					var objetivo = '';

					if (tipo != 'Si/No'){

						propuesta.find('input.objetivo').each(function(){

							if ($(this).val().trim().length == 0){

								alert('Escribe la meta del objetivo');
								$(this).focus();
								return false;
							}
        			
        			objetivo += ',' + $(this).val().replace('$', '').replace('%', '').replace(/,/g, '');
        		});
					}

					else{

						propuesta.find('select.objetivo').each(function(){
        			
        			objetivo += ',' + $(this).val();
        		});
					}

					if (objetivo != ''){

        		objetivo = objetivo.substr(1);
        	}

          var peso = propuesta.find('.peso');

					if (peso.val().trim().length == 0){

						alert('Escribe el peso');
						peso.focus();
						return false;
					}

					var peso_total = 0;

					$('table.tabla_objetivos .peso.periodo' + periodo).each(function(){

						peso_total += $(this).val() * 1;
					});

					$('.peso_total').text(peso_total);

        	var id_objetivo_corporativo = propuesta.find('.id_objetivo_corporativo').val();
        	var frecuencia = propuesta.find('.frecuencia').val();

        	if (niveles_logro != ''){

        		niveles_logro = niveles_logro.substr(1);
        	}

        	if (porcentajes_bono != ''){

        		porcentajes_bono = porcentajes_bono.substr(1);
        	}

        	var valor_rojo = '';
        	var valor_verde = '';

        	if (tipo != 'Si/No'){

        		propuesta.find('input.valor_rojo').each(function(){

							if ($(this).val().trim().length == 0){

								alert('Escribe el valor rojo del semáforo del objetivo');
								$(this).focus();
								return false;
							}
        			
        			valor_rojo += ',' + $(this).val().replace('$', '').replace('%', '').replace(/,/g, '');
        		});

        		propuesta.find('input.valor_verde').each(function(){

							if ($(this).val().trim().length == 0){

								alert('Escribe el valor verde del semáforo del objetivo');
								$(this).focus();
								return false;
							}
        			
        			valor_verde += ',' + $(this).val().replace('$', '').replace('%', '').replace(/,/g, '');
        		});
					}

      		else{

      			propuesta.find('select.valor_verde').each(function(){

							if ($(this).val() == 'Si'){

								valor_rojo += ',No';
							}

							else{

								valor_rojo += ',Si';
							}
        			
        			valor_verde += ',' + $(this).val();
        		});
      		}

      		if (valor_verde != '' && valor_verde != ''){

        		valor_verde = valor_verde.substr(1);
        		valor_rojo = valor_rojo.substr(1);
        	}

        	formula = propuesta.find('.formula').val();

					$.ajax({
        		type:'POST',    
        		url: '/agregar-propuesta',
          	data:{
          		nombre: nombre,
          		id_objetivo_corporativo: id_objetivo_corporativo,
          		formula: formula,
          		tipo: tipo,
          		peso: peso.val(),
          		objetivo: objetivo,
          		frecuencia: frecuencia,
          		corresponsables: corresponsables,
          		grupos_corresponsables: grupos_corresponsables,
          		valor_rojo: valor_rojo,
          		valor_verde: valor_verde,
          		id_plan: id_plan,
          		periodo: periodo,
          		id: id,
          		id_empleado: id_user,
          		id_catalogo: id_catalogo,
          		niveles_logro: niveles_logro.replace(/%/g, ''),
          		porcentajes_bono: porcentajes_bono.replace(/%/g, ''),
          		total_bono: total_bono,
							'_token': '{{ csrf_token() }}'
          	},
          	success: function(data){
			  
							if (data != 0){

                button.next().attr('id', data);
            		//button.text('Guardar');
            		button.attr('id', data);
								button.addClass('editar');
                button.next().addClass('borrar');
								button.next().removeClass('remover');
            		//$('.solicitarAprobacionForm_container').show();
            	}

            	peso_acumulado = 0;
							var peso_parcial = 0;
			   			var correcto = true;
							var periodo_actual = 0;
							var periodos = 0;
							objetivos_guardados++;

							$('table.tabla_objetivos .peso').each(function(){

								periodos = $(this).attr('class').replace('peso periodo', '') * 1;

								if (periodos != periodo_actual){

									if (periodo_actual != 0){

										if (peso_parcial > 100){

											correcto = false;
										}
									}

									periodo_actual = periodos;
									peso_parcial = 0;
								}

								peso_parcial += $(this).val() * 1;
								peso_acumulado += $(this).val() * 1;
							});

							if (periodo_actual != 0){

								if (peso_parcial > 100){

									correcto = false;
								}
							}

							$('.peso_total').text(peso_acumulado);

							if (peso_acumulado == 0){

								$('.solicitarAprobacionForm_container').hide();
							}
							
							// Se solicito guardar un objetivo solamente
							if (!guardar_objetivos){

            		alert('El objetivo fue guardado');
            	}

            	// Se solicito guardar todos los objetivos
            	else{

            		// No se han guardado todos los objetivos
            		if ($('table.tabla_objetivos tbody tr td button.crear').length > objetivos_guardados){

            			// Se guarda el siguiente objetivo
            			num_objetivos++;

            			while ($('table.tabla_objetivos tbody tr.objetivos:nth-child(' + num_objetivos + ') td button.crear').length == 0){

										num_objetivos++;
									}

            			$('table.tabla_objetivos tbody tr.objetivos:nth-child(' + num_objetivos + ') td button.crear').click();
            		}

            		// Se guardaron todos los objetivos
            		else{

            			num_objetivos = 0;
            			guardar_objetivos = false;
            			objetivos_guardados = 0;
            			alert('Los objetivos fueron guardados');
            			$('form#changePlanForm').submit();
            		}
            	}
          	}
        	});
			});

      $('button.agregar_nota').click(function(){

				var nota = $('textarea.nota').val();

				$.ajax({
        	type:'POST',    
        	url: 'agregar-nota',
          data:{
          	mensaje: nota,
          	id_plan: id_plan,
          	id_empleado: id_user,
			'_token': '{{ csrf_token() }}'
          },
          success: function(data){
             
            var meses = [];
            meses[0] = 'Ene';
            meses[1] = 'Feb';
            meses[2] = 'Mar';
            meses[3] = 'Abr';
            meses[4] = 'May';
            meses[5] = 'Jun';
            meses[6] = 'Jul';
            meses[7] = 'Ago';
            meses[8] = 'Sep';
            meses[9] = 'Oct';
            meses[10] = 'Nov';
            meses[11] = 'Dic';
            var fecha = new Date();
            $('table.notas').prepend('<tr><td class="text-center" style="padding: 5px; border: 0">' + fecha.getDate() + '/' + meses[fecha.getMonth()] + '/' + fecha.getFullYear() + '</td><td class="text-center" style="padding: 5px; border: 0">' + data + '</td><td class="text-center" style="padding: 5px; border: 0">' + nota + '</td></tr>');
            $('textarea.nota').val('');
          }
        });
			});

			$('form#solicitarAprobacionForm').submit(function(){

				peso_acumulado = 0;
				var correcto = true;
				var periodo_actual = 0;
				var periodos = 0;

				$('table.tabla_objetivos .peso').each(function(){

					periodos = $(this).attr('class').replace('peso periodo', '') * 1;

					if (periodos != periodo_actual){

						if (periodo_actual != 0){

							if (peso_acumulado != 100){

								correcto = false;
							}
						}

						periodo_actual = periodos;
						peso_acumulado = 0;
					}

					peso_acumulado += $(this).val() * 1;
				});

				if (periodo_actual != 0){

					if (peso_acumulado != 100){

						correcto = false;
					}
				}
				
				/*if (correcto == false){

					alert('La suma de los pesos de los objetivos por periodo debe ser 100');
					return false;
				}*/
			});

                        $('body').on('click', 'button.borrar', function(){

                        if (confirm('¿De verdad quieres eliminar este objetivo?')){

                          var button = $(this) 
                          var id_propuesta = button.attr('id');

                        $.ajax({
        	type:'POST',    
        	url: 'borrar-propuesta',
          data:{
          	id_propuesta: id_propuesta,
          	id_plan: id_plan,
          	id_empleado: id_user,
          	periodo: periodo,
			'_token': '{{ csrf_token() }}'
          },
          success: function(data){

               button.parent().parent().remove();

               peso_acumulado = 0;
			   var peso_parcial = 0;
			   var correcto = true;
				var periodo_actual = 0;
				var periodos = 0;

				$('table.tabla_objetivos .peso').each(function(){

					periodos = $(this).attr('class').replace('peso periodo') * 1;

					if (periodos != periodo_actual){

						if (periodo_actual != 0){

							if (peso_parcial > 100){

								correcto = false;
							}
						}

						periodo_actual = periodos;
						peso_parcial = 0;
					}

					peso_parcial += $(this).val() * 1;
					peso_acumulado += $(this).val() * 1;
				});

				if (periodo_actual != 0){

					if (peso_parcial > 100){

						correcto = false;
					}
				}

				$('.peso_total').text(peso_acumulado);

				if (peso_acumulado == 0){

					$('.solicitarAprobacionForm_container').hide();
				}

			  // El peso de todos los objetivos suman menos o igual a 100
				/*if (correcto == true){

					// Mostramos en negro el peso
					$('.peso_total').css('color', 'black');
					$('.peso_total').css('font-weight', 'normal');
				}*/
			  
               var counter = 1;

               $('table.tabla_objetivos tr.objetivos').each(function(){

                 $(this).find('td:nth-child(1)').text(counter);
                 counter++;
               });             
            }
          });

          }
        });

				// Se dio click en guardar todos los objetivos
				$('.guardar_objetivos').click(function(){

					total_bono = 1;

					// Hay objetivos para guardar
					if ($('table.tabla_objetivos tbody tr td button.crear').length > 0){

						guardar_objetivos = true;
						num_objetivos = 1;
						$('.solicitar_validacion').hide();

						while ($('table.tabla_objetivos tbody tr.objetivos:nth-child(' + num_objetivos + ') td button.crear').length == 0){

							num_objetivos++;
						}
						
						// Se guarda el primer objetivo
						$('table.tabla_objetivos tbody tr.objetivos:nth-child(' + num_objetivos + ') td button.crear').click();
					}
				});

				// Se selecciono algún plan para copiar objetivos
				$('.guardar_objetivos').click(function(){

					total_bono = 1;

					// Hay objetivos para guardar
					if ($('table.tabla_objetivos tbody tr td button.crear').length > 0){

						guardar_objetivos = true;
						num_objetivos = 1;
						$('.solicitar_validacion').hide();

						while ($('table.tabla_objetivos tbody tr.objetivos:nth-child(' + num_objetivos + ') td button.crear').length == 0){

							num_objetivos++;
						}
						
						// Se guarda el primer objetivo
						$('table.tabla_objetivos tbody tr.objetivos:nth-child(' + num_objetivos + ') td button.crear').click();
					}
				});

				$('select.plan_para_copiar').change(function(){

					id_plan_para_copiar = $(this).val();
					var x = 0, y = 0;
					var autocomplete_users = [];

					for (x = 0;x < users_plans.length;x++){

						if (users_plans[x].id_plan == id_plan_para_copiar){

							for (y = 0;y < users_with_objetives.length;y++){

								if (users_with_objetives[y].data == users_plans[x].id_empleado){

									autocomplete_users.push(users_with_objetives[y]);
									break;
								}
							}
						}
					}

					// Autocomplete
					$('.colaborador').autocomplete({
    				lookup: autocomplete_users,
    				onSelect: function (suggestion){
        
        			nombre = suggestion.value;
    					id = suggestion.data;
    					$(this).next().html('<input type="hidden" value="' + id + '" name="colaborador"><div>' + nombre + '</div>');
							$(this).val('');
    				}
					});
				});

				var periodo_actual = 0;

                         if (objetivos != 0){

				for (var i = 0;i < objetivos.length;i++){

					if (periodo_actual != objetivos[i].periodo){

						peso_acumulado = 0;
						periodo_actual = objetivos[i].periodo;
					}

					peso_acumulado += objetivos[i].peso * 1;
				}
			}

			$('input.porcentaje').inputmask("numeric", {
    		radixPoint: ".",
    		groupSeparator: ",",
    		digits: 2,
    		autoGroup: true,
    		suffix: '%', //No Space, this will truncate the first character
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});

			$('input.currency').inputmask("numeric", {
    		radixPoint: ".",
    		groupSeparator: ",",
    		digits: 2,
    		autoGroup: true,
    		prefix: '$', //No Space, this will truncate the first character
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});

			$('input.numeric').inputmask("numeric", {
    		groupSeparator: ",",
    		autoGroup: true,
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});

			$('select.plan_para_copiar').change();
		});
	</script>
@endsection