@extends('layouts.app')

@section('title', 'Planes')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/banner_evaluacion_resultados.png') }}" alt="">
	
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Planes</h5>
			<div class="card-body">
				
			<div class="row margin-top-20 mb-3">
				<div class="col-md-12 text-center">
					<a href="/evaluacion-resultados/planes/create" class="btn btn-primary btn-finvivir-blue"><i class="fas fa-plus-circle"></i> Agregar Plan</a>
				</div>
				<!--<div class="col-md-6 text-center">
					<form action="/importar-regiones" method="post" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<<input type="file" name="file" required style="display: inline"> <button type="submit" class="btn btn-primary btn-finvivir-blue">Importar Regiones</button>
					</form>
				</div>-->
			</div>
			<div class="row margin-top-20 mb-3">
				<div class="col-md-12 text-center table-responsive">
		<table class="table table-striped table-bordered table-hover reportes">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th>Estado</th>
					<th>Código</th>
					<th>Nombre</th>
					<th>Año de inicio</th>
					<th>Tope de Peso de los objetivos</th>
					<th>Quien registra objetivos</th>
					<th>Quien autoriza objetivos</th>
					<th>Mes de inicio</th>
					<!--<th>Quien registra responsabilidades</th>-->
					<th>Quien registra logros</th>
					<th>Quien autoriza logros</th>
					<th>¿Registrar Logros?</th>
					<th>¿Cambiar valores?</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>

<?php $meses = array(0,'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'); ?>

			@for ($i = 0; $i < count($planes); $i++)

				<tr>
					<td>
						<h5 class="my-auto"><span class="badge" style="padding: 5px; background-color: <?php echo ($planes[$i]->estado == 2 ? '#97005e' : '#0078ad')?>; color: white"><?php echo ($planes[$i]->estado == 1 ? 'Preparatorio' : ($planes[$i]->estado == 2 ? 'Abierto' : ($planes[$i]->estado == 3 ? 'Cerrado (Solo reportes)' : ($planes[$i]->estado == 4 ? 'Historico' : 'Cancelado'))))?></span></h5>
					</td>
					<td>{{$planes[$i]->id}}</td>
					<td>{{$planes[$i]->nombre}}</td>
					<td>{{$planes[$i]->anio}}</td>
					<td>{{(!empty($planes[$i]->total_peso_objetivos) ? $planes[$i]->total_peso_objetivos : '')}}</td>
					<td>{{($planes[$i]->alimentar_propuestas == 1 ? 'Colaborador' : ($planes[$i]->alimentar_propuestas == 2 ? 'Jefe' : ($planes->alimentar_propuestas == 4 ? 'Director' : 'Administrador')))}}</td>
					<td>{{($planes[$i]->autorizar_propuestas == 2 ? 'Jefe' : ($planes[$i]->autorizar_propuestas == 4 ? 'Director' : 'Administrador'))}}</td>
					<td>{{$meses[$planes[$i]->periodo]}}</td>
					<!--<td>{{$planes[$i]->registrar_responsabilidades}}</td>-->
					<td>{{($planes[$i]->registrar_logros == 1 ? 'Colaborador' : ($planes[$i]->registrar_logros == 2 ? 'Jefe' : ($planes_registrar_logros == 4 ? 'Director' : 'Administrador')))}}</td>
					<td>{{($planes[$i]->autorizar_logros == 2 ? 'Jefe' : ($planes[$i]->autorizar_logros == 4 ? 'Director' : 'Administrador'))}}</td>
					<td>{{($planes[$i]->registrar_logros_abierto == 1 ? 'Si' : 'No')}}</td>
					<td>{{($planes[$i]->tipo == 1 ? 'No' : 'Si')}}</td>
					<td>
						<?php //if ($planes[$i]->estado == 3){ ?><!--<a href="calculo-bono/{{$planes[$i]->id}}" class="btn btn-primary btn-finvivir-purple">Calcular Bono</a>--><?php //} ?> <a href="/evaluacion-resultados/planes/{{$planes[$i]->id}}/edit/" class="btn btn-primary btn-finvivir-blue" title="Editar"><i class="fas fa-edit"></i> </a> <?php if ($planes[$i]->estado == 5){ ?><a href="/evaluacion-resultados/borrar-plan/{{$planes[$i]->id}}" class="btn btn-primary btn-finvivir-purple" title="Eliminar"><i class="fas fa-trash"></i></a><?php } ?>
					</td>
				</tr>
			@endfor
			</tbody>
		</table>

	</div>
	</div>
	</div>
</div>
</div>
</div>
@endsection
@section('scripts')

<script type="text/javascript">
	$('.reportes').DataTable({
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
   	});
</script>

@endsection
