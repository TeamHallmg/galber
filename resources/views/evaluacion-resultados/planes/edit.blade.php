@extends('layouts.app')

@section('title', 'Editar Plan')

@section('content')
<div class="row margin-top-20">
  <div class="col-md-2 text-right">
    @include('evaluacion-resultados/partials/sub-menu')
  </div>
  <div class="col-md-10">
    <img class="img-fluid" src="{{ asset('img/banner_evaluacion_resultados.png') }}" alt="">
  
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Editar Plan
        <a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
 
      </h5>
			<div class="card-body">
    @if ($plan->estado == 1)

    <!--<div class="row mb-3">
      <div class="col-md-12">
      
        <div class="card">
          <h5 class="card-header bg-primary text-white font-weight-bolder">Exportar / Importar Plantilla de Objetivos por Puestos</h5>
          <div class="card-body">
            
            <div class="row">
              <div class="col-md-3 d-flex align-items-end">										
                <form action="/evaluacion-resultados/exportar-plantilla-objetivos-puestos" method="post" class="exportar_resultados w-100">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="id_plan" value="{{$plan->id}}">
                  <button type="submit" class="btn btn-block btn-success font-weight-bold text-white">Exportar</button>
                </form>
              </div>

              <div class="col-md-1 text-center">
                |
              </div>
              <div class="col-md-8">

                <form action="/evaluacion-resultados/importar-plantilla-objetivos-puestos" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="id_plan" value="{{$plan->id}}">
                  
                  <div class="text-center">
                  <input type="checkbox" name="autorizados" value="1" class="mr-1" id="autorizados_0"> <label for="autorizados_0"> ¿Objetivos ya autorizados? </label>
                  </div>
                  <div class="custom-file w-75 mr-3">
                    <input type="file" name="file" class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Seleccionar Archivo a Importar</label>
                    
                  </div> 
                  <button type="submit" class="btn btn-primary font-weight-bold text-white">Importar</button>
              
                </form>  
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>-->

    <div class="row mb-3">
      <div class="col-md-12">
      
        <div class="card">
          <h5 class="card-header bg-primary text-white font-weight-bolder">Exportar / Importar Plantilla de Objetivos por RFC</h5>
          <div class="card-body">
            
            <div class="row">
              <div class="col-md-3 d-flex align-items-end">                   
                <form action="/evaluacion-resultados/exportar-plantilla-objetivos-rfc" method="post" class="exportar_resultados w-100">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="id_plan" value="{{$plan->id}}">
                  <button type="submit" class="btn btn-block btn-success font-weight-bold text-white">Exportar</button>
                </form>
              </div>

              <div class="col-md-1 text-center">
                |
              </div>
              <div class="col-md-8">

                <form action="/evaluacion-resultados/importar-plantilla-objetivos-rfc" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="id_plan" value="{{$plan->id}}">
                  
                  <!--<div class="text-center">
                  <input type="checkbox" name="autorizados" value="1" class="mr-1" id="autorizados_0"> <label for="autorizados_0"> ¿Objetivos ya autorizados? </label>
                  </div>-->
                  <div class="custom-file w-75 mr-3">
                    <input type="file" name="file" class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Seleccionar Archivo a Importar</label>
                    
                  </div> 
                  <button type="submit" class="btn btn-primary font-weight-bold text-white">Importar</button>
              
                </form>  
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>

    <!--<div class="row mb-3">
      <div class="col-md-12">
      
        <div class="card">
          <h5 class="card-header bg-primary text-white font-weight-bolder">Exportar / Importar Plantilla de Bonos</h5>
          <div class="card-body">
            
            <div class="row">
              <div class="col-md-3">										
                <form action="/evaluacion-resultados/exportar-plantilla-bonos" method="post" class="exportar_resultados">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="id_plan" value="{{$plan->id}}">
                  <button type="submit" class="btn btn-block btn-success font-weight-bold text-white">Exportar</button>
                </form>
              </div>

              <div class="col-md-1 text-center">
                |
              </div>
              <div class="col-md-8">

                <form action="/evaluacion-resultados/importar-plantilla-bonos" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="id_plan" value="{{$plan->id}}">
                  <div class="custom-file w-75 mr-3">
                    <input type="file" name="file" class="custom-file-input">
                    <label class="custom-file-label" for="customFile">Seleccionar Archivo a Importar</label>
                    
                  </div> 
                  <button  type="submit" class="btn btn-primary font-weight-bold text-white">Importar</button>
                  <a href="{{ url()->previous() }}" class="btn btn-danger">Regresar</a>

                </form>  
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>-->




    @endif

    {!! Form::open(['route' => ['planes.update', $plan->id], 'method' => 'PUT'])!!}
      <input type="hidden" name="id" value="{{$plan->id}}">
      
	<div class="row">
	  <div class="col-md-6">
      <div class="form-group plan_form">
        <label for="nombre">Nombre</label>
        <input type="text" class="form-control" name="nombre" required value="{{$plan->nombre}}">
      </div>
    </div>
      <div class="col-md-6">
      <div class="form-group">
        <label for="anio">Año de Inicio</label>
        <select class="form-control" name="anio">

  <?php if ($plan->estado != 1){ ?>
      
      <option value="{{$plan->anio}}">{{$plan->anio}}</option>

  <?php }

    else{
      
      $anio = date('Y') * 1;
      
      if ($plan->anio < $anio){ ?>

      <option value="{{$plan->anio}}">{{$plan->anio}}</option>
    <?php } ?>
      
          <option value="{{$anio}}" <?php if ($anio == $plan->anio){ ?>selected="selected"<?php } ?>>{{$anio}}</option>
          <option value="{{$anio + 1}}" <?php if (($anio + 1) == $plan->anio){ ?>selected="selected"<?php } ?>>{{$anio + 1}}</option>
  <?php } ?>
      
        </select>
      </div>
    </div>
      <div class="col-md-6">
      <div class="form-group">
        <label for="nombre">Tope de Peso de los objetivos</label>
        <input type="text" class="form-control numeric" name="total_peso_objetivos" value="{{(!empty($plan->total_peso_objetivos) ? $plan->total_peso_objetivos : '')}}">
      </div>
    </div>
      <div class="col-md-6">
      <div class="form-group">
        <label for="alimentar_propuestas">Registra objetivos</label>
        <select class="form-control alimentar_propuestas" name="alimentar_propuestas">
      
  <?php if ($plan->estado != 1){ ?>
      
      <option value="{{$plan->alimentar_propuestas}}">{{($plan->alimentar_propuestas == 2 ? 'Jefe' : ($plan->alimentar_propuestas == 3 ? 'Administrador' : ($plan->alimentar_propuestas == 4 ? 'Director' : 'Colaborador')))}}</option>
  <?php }

    else{ ?>
      
          <option value="2">Jefe</option>
          <option value="4" <?php if ($plan->alimentar_propuestas == 4){ ?>selected="selected"<?php } ?>>Director</option>
          <option value="3" <?php if ($plan->alimentar_propuestas == 3){ ?>selected="selected"<?php } ?>>Administrador</option>
          <option value="1" <?php if ($plan->alimentar_propuestas == 1){ ?>selected="selected"<?php } ?>>Colaborador</option>
  <?php } ?>
      
        </select>
      </div>
    </div>
      <div class="col-md-6">
      <div class="form-group">
        <label for="autorizar_propuestas">Autoriza objetivos</label>
        <select class="form-control autorizar_propuestas" name="autorizar_propuestas">
      
  <?php if ($plan->estado != 1){ ?>
      
      <option value="{{$plan->autorizar_propuestas}}">{{($plan->autorizar_propuestas == 2 ? 'Jefe' : ($plan->autorizar_propuestas == 3 ? 'Administrador' : 'Director'))}}</option>
  <?php }

    else{ ?>
      
          <option value="4">Director</option>
          <option value="2" <?php if ($plan->autorizar_propuestas == 2){ ?>selected="selected"<?php } ?>>Jefe</option>
          <option value="3" <?php if ($plan->autorizar_propuestas == 3){ ?>selected="selected"<?php } ?>>Administrador</option>
  <?php } ?>
      
        </select>
      </div>
    </div>
      <div class="col-md-6">
    <div class="form-group">
        <label for="periodo">Mes de inicio</label>
        <select class="form-control periodo" name="periodo" required>

  <?php $meses = array(0,'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');

        foreach ($meses as $key => $mes){
      
          if ($key > 0){ ?>

          <option value="{{$key}}" <?php if ($plan->periodo == $key){ ?>selected="selected"<?php } ?>>{{$mes}}</option>
    <?php }
        } ?>

        </select>
      </div>
    </div>
      <div class="col-md-6">
    <!--<div class="form-group">
          <label for="registrar_responsabilidades">Registra responsabilidades</label>
          <select class="form-control" name="registrar_responsabilidades" required>
            <option value="Responsable">Responsable</option>
            <option value="Administrador" <?php //if ($plan->registrar_responsabilidades == 'Administrador'){ ?>selected="selected"<?php //} ?>>Administrador</option>      
          </select>
        </div>-->
    <div class="form-group registrar_logros">
        <label for="registrar_logros">Registra logros</label>
        <select class="form-control" name="registrar_logros">
      
  <?php if ($plan->estado != 1){ ?>
      
      <option value="{{$plan->registrar_logros}}">{{($plan->registrar_logros == 2 ? 'Jefe' : ($plan->registrar_logros == 4 ? 'Director' : ($plan->registrar_logros == 3 ? 'Administrador' : 'Colaborador')))}}</option>
  <?php }

    else{ ?>
      
          <option value="2">Jefe</option>
          <option value="3" <?php if ($plan->registrar_logros == 3){ ?>selected="selected"<?php } ?>>Administrador</option>
          <option value="4" <?php if ($plan->registrar_logros == 4){ ?>selected="selected"<?php } ?>>Director</option>
          <option value="1" <?php if ($plan->registrar_logros == 1){ ?>selected="selected"<?php } ?>>Colaborador</option>

  <?php } ?>
      
        </select>
      </div>
    </div>
      <div class="col-md-6">
      <div class="form-group autorizar_logros">
        <label for="autorizar_logros">Autoriza logros</label>
        <select class="form-control" name="autorizar_logros">
      
  <?php if ($plan->estado != 1){ ?>
      
      <option value="{{$plan->autorizar_logros}}">{{($plan->autorizar_logros == 2 ? 'Jefe' : ($plan->autorizar_logros == 4 ? 'Director' : 'Administrador'))}}</option>
  <?php }

    else{ ?>
      
          <option value="4">Director</option>
          <option value="3" <?php if ($plan->autorizar_logros == 3){ ?>selected="selected"<?php } ?>>Administrador</option>
          <option value="2" <?php if ($plan->autorizar_logros == 2){ ?>selected="selected"<?php } ?>>Jefe</option>

  <?php } ?>
      
        </select>
      </div>
    </div>
      <div class="col-md-6">
    <div class="form-group registrar_logros_abierto">
      <input type="checkbox" name="registrar_logros_abierto" <?php if ($plan->registrar_logros_abierto == 1){ ?>checked="checked"<?php } ?> class="mr-2"><label for="registrar_logros_abierto">Registrar logros cuando este abierto</label> 
      </div>
    </div>
      <div class="col-md-6">
      <div class="form-group">
        <label for="tipo">¿Permitir cambiar los valores rojo y verde del semáforo cada mes/periodo?</label>
        <select class="form-control" name="tipo">
          <option value="2">Si</option>
          <option value="1" <?php if ($plan->tipo == 1){ ?>selected="selected"<?php } ?>>No</option>
        </select>
      </div>
    </div>
      <div class="col-md-6">
      <div class="form-group">
        <label for="estado">Estado</label>
        <select class="form-control" name="estado">
          
  <?php if ($plan->estado == 1){ ?>

          <option value="1">Preparatorio</option>
  <?php }

        if ($plan->estado < 4){ ?>

          <option value="2" <?php echo ($plan->estado == 2 ? 'selected="selected"' : '')?>>Abierto</option>
  <?php }

        if ($plan->estado == 2 || $plan->estado == 3){ ?>

          <option value="3" <?php echo ($plan->estado == 3 ? 'selected="selected"' : '')?>>Cerrado (Solo reportes)</option>
  <?php }

        if ($plan->estado != 3){ ?>

          <option value="5" <?php echo ($plan->estado == 5 ? 'selected="selected"' : '')?>>Cancelado</option>
  <?php } ?>

        </select>
      </div>
    </div>
      <div class="col-md-12">
      <div class="form-group">
        <button class="btn btn-success text-center" type="submit"><i class="fas fa-check-circle"></i> Guardar Cambios</button>
      </div>
      </div>
    </div>
    {!! Form::close() !!}

</div>
</div>
</div>
@endsection
@section('scripts')
  <script>
    
    $(document).ready(function(){

      $('div.plan_form').parent().attr('action', $('div.plan_form').parent().attr('action').replace('escalafon', 'evaluacion-resultados'));

      // Cambiar el dropdown para decidir quien alimenta las propuestas
      $('select.alimentar_propuestas').change(function(){
        
        var valor = $(this).val();
        
        if (valor == 1){
          
          $('div.registrar_logros').hide();
        }
        
        else{
          
          $('div.registrar_logros').show();
        }
      });

      $('input.numeric').inputmask("numeric", {
        groupSeparator: ",",
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
      });
    });
  </script>
@endsection