@extends('layouts.app')

@section('title', 'Borrar Plan')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_evaluacion_resultados.png" alt="">

	</div>
	<div class="col-md-12">
		<h4 class="margin-top-20 titulos-evaluaciones">Borrar Plan</h4>
	</div>
</div>
<div class="row margin-top-20">
	<div class="col-md-3"></div>
	<div class="col-md-6 text-center">
		<form action="/evaluacion-resultados/borrar-plan" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id" value="{{$plan[0]->id}}">
			<div class="form-group">
        <label for="nombre">Se borrarán todos los objetivos, logros, notas, etc, relacionados con este plan. ¿De verdad quieres eliminar el plan {{$plan[0]->nombre}}?</label>
      </div>
			<div class="form-group">
        <button class="btn btn-danger" type="submit">Borrar</button> <a href="/planes" class="btn btn-primary">Cancelar</a>
      </div>
    </form>
	</div>
	<div class="col-md-3"></div>
</div>
@endsection
