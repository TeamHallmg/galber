@extends('layouts.app')

@section('title', 'Crear Plan')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/banner_evaluacion_resultados.png') }}" alt="">
 
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Crear Plan
        <a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>


      </h5>
			<div class="card-body">
        {!! Form::open(['route' => 'planes.store', 'method' => 'POST'])!!}
	
        <div class="row">
        <div class="col-md-6">
          <div class="form-group plan_form">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" name="nombre" required>
          </div>
        </div>
        <div class="col-md-6">
  		<div class="form-group">
        <label for="anio">Año de Inicio</label>
        <select class="form-control" name="anio">

  <?php $anio = date('Y') * 1?>

          <option value="{{$anio}}">{{$anio}}</option>
          <option value="{{$anio + 1}}">{{$anio + 1}}</option>
        </select>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="periodo">Mes de inicio</label>
        <select class="form-control periodo" name="periodo" required>

  <?php $meses = array(0,'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');

        foreach ($meses as $key => $mes){

          if ($key > 0){ ?>
      
          <option value="{{$key}}">{{$mes}}</option>
    <?php }
        } ?>

        </select>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="nombre">Tope de Peso de los objetivos</label>
        <input type="text" class="form-control numeric" name="total_peso_objetivos">
      </div>
    </div>
    <div class="col-md-6">
      <!--<div class="form-group">
          <label for="registrar_responsabilidades">Registra responsabilidades</label>
          <select class="form-control" name="registrar_responsabilidades" required>
            <option value="Responsable">Responsable</option>
            <option value="Administrador">Administrador</option>
          </select>
        </div>-->
	  <div class="form-group">
      <label for="alimentar_propuestas">Registra objetivos</label>
      <select class="form-control alimentar_propuestas" name="alimentar_propuestas">
        <option value="2">Jefe</option>
        <option value="3">Administrador</option>
        <option value="4">Director</option>
        <option value="1">Colaborador</option>
      </select>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label for="autorizar_propuestas">Autoriza objetivos</label>
      <select class="form-control autorizar_propuestas" name="autorizar_propuestas">
        <option value="4">Director</option>
        <option value="3">Administrador</option>
        <option value="2">Jefe</option>
      </select>
    </div>
  </div>
  <div class="col-md-6">
	  <div class="form-group registrar_logros">
        <label for="registrar_logros">Registra logros</label>
        <select class="form-control" name="registrar_logros">
          <option value="2">Jefe</option>
          <option value="3">Administrador</option>
          <option value="4">Director</option>
          <option value="1">Colaborador</option>
        </select>
      </div>
    </div>
    <div class="col-md-6">
    <div class="form-group autorizar_logros">
        <label for="autorizar_logros">Autoriza logros</label>
        <select class="form-control" name="autorizar_logros">
          <option value="4">Director</option>
          <option value="3">Administrador</option>
          <option value="2">Jefe</option>
        </select>
      </div>
    </div>
    <div class="col-md-6">
	  <div class="form-group registrar_logros_abierto text-left">
      <input type="checkbox" name="registrar_logros_abierto" checked="checked" class="mr-2"><label for="registrar_logros_abierto">Registrar logros cuando este abierto</label> 
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="tipo">¿Permitir cambiar los valores rojo y verde del semáforo cada mes/periodo?</label>
        <select class="form-control" name="tipo">
          <option value="2" selected="selected">Si</option>
          <option value="1">No</option>
        </select>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="estado">Estado</label>
        <select class="form-control" name="estado">
        	<option value="1">Preparatorio</option>
        	<option value="2">Abierto</option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
			<div class="form-group">
        <button class="btn btn-success" type="submit"><span class="fas fa-check-circle"></span> Crear</button>
        <a href="{{ url()->previous() }}" class="btn btn-danger">Regresar</a>
      </div>
      </div>
	</div>
</form>
  </div>
  </div>
  </div>
</div>

@endsection
@section('scripts')
	<script>
		
		$(document).ready(function(){

      $('div.plan_form').parent().attr('action', $('div.plan_form').parent().attr('action').replace('escalafon', 'evaluacion-resultados'));

			// Cambiar el dropdown para decidir quien alimenta las propuestas
			$('select.alimentar_propuestas').change(function(){
				
				var valor = $(this).val();
				
				if (valor == 1){
					
					$('div.registrar_logros').hide();
				}
				
				else{
					
					$('div.registrar_logros').show();
				}
			});

      $('input.numeric').inputmask("numeric", {
        groupSeparator: ",",
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
      });	
		});
	</script>
@endsection
