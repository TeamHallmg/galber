@extends('layouts.app')

@section('title', 'Editar Objetivo de Catálogo')

@section('content')
<div class="row margin-top-20">
  <div class="col-md-2 text-right">
    @include('evaluacion-resultados/partials/sub-menu')
  </div>
  <div class="col-md-10">
    <img class="img-fluid" src="{{ asset('img/banner_evaluacion_resultados.png') }}" alt="">
  
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Editar Objetivo de Catálogo
        <a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>

      </h5>
			<div class="card-body">
        <form method="post" action="/evaluacion-resultados/catalogo-objetivos">
    
        <div class="row">
          <div class="col-md-6">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="id" value="{{$objetivo_catalogo->id}}">
              <div class="form-group">
                <label for="codigo">Código</label>
                <input type="text" class="form-control" name="codigo" required value="{{$objetivo_catalogo->codigo}}">
              </div>
          </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="objetivo">Objetivo</label>
            <textarea class="form-control" name="objetivo" required>{{$objetivo_catalogo->objetivo}}</textarea>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="descripcion">Descripción</label>
            <textarea class="form-control" name="descripcion">{{$objetivo_catalogo->descripcion}}</textarea>
          </div>
        </div>
        <div class="col-md-3">
      <div class="form-group">
        <label for="id_plan">Plan del Objetivo Corporativo</label>
        <select class="form-control planes">

      @foreach ($planes as $key => $plan)

          <option value="{{$key}}" <?php if ($id_plan == $key){ ?>selected="selected"<?php } ?>>{{$plan}}</option>
      @endforeach

        </select>
      </div>
    </div>
	  <div class="col-md-3">
      <div class="form-group">
        <label for="id_objetivo_corporativo">Objetivo Corporativo</label>
        <select class="form-control objetivos_corporativos" name="id_objetivo_corporativo">

<?php $current_type = $counter = 0; ?>

      @for ($j = 0;$j < count($objetivos_corporativos); $j++)

        @if ($id_plan == $objetivos_corporativos[$j]->id_plan)

          @if ($current_type != $objetivos_corporativos[$j]->id_tipo)

      <?php $counter = 1;
            $current_type = $objetivos_corporativos[$j]->id_tipo; ?>
          @endif

          <option value="{{$objetivos_corporativos[$j]->id}}" <?php if ($objetivos_corporativos[$j]->id == $objetivo_catalogo->id_objetivo_corporativo){ ?>selected="selected"<?php } ?>>{{$objetivos_corporativos[$j]->letter}}{{$counter}}</option>

    <?php $counter++; ?>
        @endif
      @endfor

        </select>
      </div>
    </div>
	  <div class="col-md-6">
      <div class="form-group">
        <label for="unidad">Unidad de medida</label>
        <select class="form-control" name="unidad">
          <option value="">-- Sin Unidad --</option>
          <option value="%" <?php if ($objetivo_catalogo->unidad == '%'){ ?>selected="selected"<?php } ?>>%</option>
          <option value="$" <?php if ($objetivo_catalogo->unidad == '$'){ ?>selected="selected"<?php } ?>>$</option>
          <option value="#" <?php if ($objetivo_catalogo->unidad == '#'){ ?>selected="selected"<?php } ?>>#</option>
        </select>
      </div>
    </div>
	  <div class="col-md-6">
      <div class="form-group">
        <label for="meta">Meta</label>
        <input type="text" class="form-control" name="meta" value="{{$objetivo_catalogo->meta}}">
      </div>
    </div>
	  <div class="col-md-6">
      <div class="form-group">
        <label for="peso">Peso</label>
        <input type="text" class="form-control" name="peso" value="{{$objetivo_catalogo->peso}}">
      </div>
    </div>
	  <div class="col-md-6">
      <div class="form-group">
        <label for="frecuencia">Frecuencia</label>
        <select class="form-control" name="frecuencia">
          <option value="Mensual">Mensual</option>
          <option value="Bimestral" <?php if ($objetivo_catalogo->frecuencia == 'Bimestral'){ ?>selected="selected"<?php } ?>>Bimestral</option>
          <option value="Trimestral" <?php if ($objetivo_catalogo->frecuencia == 'Trimestral'){ ?>selected="selected"<?php } ?>>Trimestral</option>
          <option value="Cuatrimestral" <?php if ($objetivo_catalogo->frecuencia == 'Cuatrimestral'){ ?>selected="selected"<?php } ?>>Cuatrimestral</option>
          <option value="Semestral" <?php if ($objetivo_catalogo->frecuencia == 'Semestral'){ ?>selected="selected"<?php } ?>>Semestral</option>
          <option value="Anual" <?php if ($objetivo_catalogo->frecuencia == 'Anual'){ ?>selected="selected"<?php } ?>>Anual</option>
        </select>
      </div>
    </div>
	  <div class="col-md-6">
      <div class="form-group">
        <label for="valor_rojo">Valor Rojo</label>
        <input type="text" class="form-control" name="valor_rojo" value="{{$objetivo_catalogo->valor_rojo}}">
      </div>
    </div>
	  <div class="col-md-6">
      <div class="form-group">
        <label for="valor_verde">Valor Verde</label>
        <input type="text" class="form-control" name="valor_verde" value="{{$objetivo_catalogo->valor_verde}}">
      </div>
    </div>
	  <div class="col-md-6">
      <div class="form-group">
        <button class="btn btn-success" type="submit"><span class="fas fa-check-circle"></span> Actualizar</button>
        <a href="{{ url()->previous() }}" class="btn btn-danger">Regresar</a>
      </div>
    </div>
    </div>
    </form>
  </div>
  </div>
</div>
</div>
 
@endsection
@section('scripts')
  <script>

    var objetivos_corporativos = <?php echo json_encode($objetivos_corporativos)?>;
    var j = 0;

    $(document).ready(function(){

      $('select.planes').change(function(){

        var plan = $(this).val();
        var html = '';
        var counter = 0;
        var current_type = 0;

        for (j = 0;j < objetivos_corporativos.length;j++){

          if (objetivos_corporativos[j].id_plan == plan){

            if (current_type != objetivos_corporativos[j].id_tipo){

              counter = 1;
              current_type = objetivos_corporativos[j].id_tipo;
            }

            html += '<option value="' + objetivos_corporativos[j].id + '">' + objetivos_corporativos[j].letter + counter + '</option>';
            counter++;
          }
        }

        $('select.objetivos_corporativos').html(html);
      });
    });
  </script>
@endsection