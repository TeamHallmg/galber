@extends('layouts.app')

@section('title', 'Detalle Bono')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-resultados/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="img/banner_evaluacion_resultados.png" alt="">
		<h3 class="titulos-evaluaciones mt-3 font-weight-bold">Detalle Bono</h3>
		<hr>
		
@if (!empty($planes))

	<div class="row">
	<div class="col-md-12">
    <!--<p class="margin-top-20 cabeceras-evaluaciones" style="padding: 5px 10px">Periodo Abierto: <?php //echo $plan->anio?></p>-->
		<div class="row margin-top-20">
			<div class="col-md-12 text-center">
				<form id="changePlanForm" action="revision-logros" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="id_empleado" value="{{$id_user}}">
					<div>Plan:  <select name="id_plan" style="border-radius: 7px; margin-bottom: 4px" class="id_plan">

											@for ($i = 0;$i < count($planes);$i++)

											 	<option value="{{$planes[$i]->id}}" <?php if ($plan->id == $planes[$i]->id){ ?>selected="selected"<?php } ?>>{{$planes[$i]->nombre}}</option>
											@endfor

											</select>
					</div>
					<input type="submit" style="display: none">
				</form>

			</div>
		</div>
		<div class="row mt-4">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<table width="100%">
					<tr>
						<td width="70">Persona:</td>
						<td>
							<input type="text" value="{{$user->nombre}} {{$user->paterno}} {{$user->materno}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
						</td>
					</tr>
					<tr>
						<td>Puesto:</td>
						<td>
							<input type="text" value="{{(!empty($user->jobPosition) ? $user->jobPosition->name : '')}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
						</td>
					</tr>
					<tr>
						<td>Área:</td>
						<td>
							<input type="text" value="{{(!empty($user->jobPosition) && !empty($user->jobPosition->area) && !empty($user->jobPosition->area->department) ? $user->jobPosition->area->department->name: '')}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
						</td>
					</tr>
					<tr>
						<td>Jefe:</td>
						<td>
							<input type="text" value="{{(!empty($user->boss) ? $user->boss->nombre . ' ' . $user->boss->paterno . ' ' . $user->boss->materno : '')}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
						</td>
					</tr>
				</table>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
</div>
<hr style="border-color: #A5A7A8">

@if (auth()->user()->isAdminOrHasRolePermission('evaluation_of_results_admin') || !empty($bono))

<div class="row">
	<div class="col-md-12">
		<strong>Total Bono</strong> <input type="text" value="{{(!empty($bono) ? $bono->total_bono : '')}}" class="form-control total_bono size currency text-right" style="display: inline-block; width: auto" readonly>
	</div>
</div>
@endif

<?php $meses = array('0', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');
            	$meses_completos = array('0', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'); ?>

<div class="row">
	<div class="col-md-12">
		<table style="width:100%" style="font-size: 13px" class="tabla_logros table-striped table-bordered">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th class="text-center">#</th>
					<th class="text-center">Objetivo</th>
					<th class="text-center">Clave</th>
					<th class="text-center">Meta</th>
					<th class="text-center">PESO</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Ene</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Feb</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Mar</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Abr</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>May</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Jun</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Jul</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Ago</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Sep</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Oct</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Nov</th>
					<th class="cabeceras-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">{{$plan->anio}}<br>Dic</th>
				</tr>
			</thead>
			<tbody>

<?php $actual_objetivo = 0;
			$contador_meses = 1;
			$frecuencias = array();
			$contador_objetivos = 1;
			$periodos = array();
			$meses_validos = array();
			$promedio = 0;
			$periodo_global = 0;
			$frecuencia_menor = 13;
			$contador_logros = 0;
			$porcentajes_bono = array();
			$contador_metas = 0; ?>

      @for ($i = 0;$i < count($objetivos); $i++)

  <?php $periodo_global = $objetivos[$i]->periodo; ?>

        @if ($actual_objetivo != $objetivos[$i]->id)

          @if ($actual_objetivo != 0)

      <?php $contador_objetivos++?>

            @for ($j = $contador_meses;$j <= 12; $j++)

          <td></td>
						@endfor

				</tr>
					@endif

				<tr>
					<td class="text-center" style="padding: 5px">{{$contador_objetivos}}</td>
					<td style="padding: 5px">
						<div style="width: 300px">{{$objetivos[$i]->nombre}}</div>
					</td>
					<td class="text-center" style="padding: 5px">

		<?php $counter = 0;
					$current_type = 0; ?>

					@for ($j = 0;$j < count($objetivos_corporativos_sin_responsable);$j++)

						@if ($current_type != $objetivos_corporativos_sin_responsable[$j]->id_tipo)

				<?php $counter = 1;
							$current_type = $objetivos_corporativos_sin_responsable[$j]->id_tipo; ?>
						@endif

						@if ($objetivos_corporativos_sin_responsable[$j]->id == $objetivos[$i]->id_objetivo_corporativo)

							{{$objetivos_corporativos_sin_responsable[$j]->letter}}{{$counter}}
				<?php break; ?>
						@endif
						
			<?php $counter++; ?>
					@endfor

					</td>

					@if ($objetivos[$i]->tipo_bono != 'Comision')

						@if (count($objetivos[$i]->valores) == 0)

					<td class="text-right" style="padding: 5px">{{($objetivos[$i]->tipo == 'Si/No' ? $objetivos[$i]->objetivo : ($objetivos[$i]->tipo == '$' ? '$' . number_format(str_replace(',', '', $objetivos[$i]->objetivo) * 1, 0, '.', ',') : ($objetivos[$i]->tipo == '%' ? str_replace(',', '', $objetivos[$i]->objetivo) . '%' : number_format(str_replace(',', '', $objetivos[$i]->objetivo), 0, '.', ','))))}}</td>
						@else

					<td class="text-right" style="padding: 5px">{{($objetivos[$i]->tipo == 'Si/No' ? $metas[$contador_metas] : ($objetivos[$i]->tipo == '$' ? '$' . number_format(str_replace(',', '', $metas[$contador_metas]) * 1, 0, '.', ',') : ($objetivos[$i]->tipo == '%' ? str_replace(',', '', $metas[$contador_metas]) . '%' : number_format(str_replace(',', '', $metas[$contador_metas]), 0, '.', ','))))}}</td>

				<?php $contador_metas++; ?>
						@endif
					@else

					<td></td>
					@endif

					<td class="text-right" style="padding: 5px">{{$objetivos[$i]->peso}}</td>

		<?php $actual_objetivo = $objetivos[$i]->id;
		      $contador_meses = 1;
		      $frecuencia = 1;
		      $periodo = 0;
		      $contador_logros = -1;
		      $porcentajes_bono = $objetivos[$i]->bonos; ?>

		      @if ($objetivos[$i]->frecuencia != 'Mensual')

    	   	 	@if ($objetivos[$i]->frecuencia == 'Anual')

    		<?php $frecuencia = 12;
    		      array_push($frecuencias, 12); ?>
    		    @endif

    				@if ($objetivos[$i]->frecuencia == 'Trimestral')

    		<?php $frecuencia = 3;
    		      array_push($frecuencias, 3); ?>
    		    @endif

    		    @if ($objetivos[$i]->frecuencia == 'Semestral')

    		<?php $frecuencia = 6;
    		      array_push($frecuencias, 6); ?>
    		    @endif
    		  @else

    		  	<?php array_push($frecuencias, 1)?>
    		  @endif
        @endif

        @if (!empty($objetivos[$i]->logro) || $objetivos[$i]->logro == '0')
				
	  <?php $periodo++;
	  			$contador_logros++;

	  			$temp_periodo = intval($objetivos[$i]->periodo_logro / $frecuencia);

      		if ($objetivos[$i]->periodo_logro % $frecuencia != 0){

        		$temp_periodo++;
      		}

      		$objetivos[$i]->periodo = $temp_periodo;
      		$periodo_global = $temp_periodo; ?>
				
					@while ($periodo < $objetivos[$i]->periodo_logro)

        		@for ($k = 0;$k < $frecuencia;$k++)

          	  <td></td>
						@endfor

	  	<?php $periodo++;
						$contador_meses = $contador_meses + $frecuencia;?>
					@endwhile
				
	  <?php if ($frecuencia < $frecuencia_menor){

	  				$frecuencia_menor = $frecuencia;
	  				$periodos = array();
	  			}

	  			if ($frecuencia == $frecuencia_menor){

	  				$periodos[] = $temp_periodo + $contador_logros;
	  			}

      		$meses_validos[] = $periodo * $frecuencia;?>

    		  @for ($j = 1;$j < $frecuencia;$j++)

    		  <td></td>
    		  @endfor

    	<?php $contador_meses = $contador_meses + $frecuencia;
    				$peso = $valor = 0;
    				$total_bono = 0;
    				$color = 'red';

    				// El logro del objetivo ya fue revisado
    				if ($objetivos[$i]->revisado == 1){

    					if ($objetivos[$i]->tipo_bono != 'Comision'){

    						if (count($objetivos[$i]->valores) > 0){

    							$found = false;

    							foreach ($objetivos[$i]->valores as $key => $valor_objetivo){
                      
                    if ($valor_objetivo->periodo == $objetivos[$i]->periodo_logro){

                      $objetivos[$i]->valor_rojo = $valor_objetivo->valor_rojo;
                      $objetivos[$i]->valor_verde = $valor_objetivo->valor_verde;
                      $found = true;
                      break;
                    }

                    else{

                      if ($valor_objetivo->periodo > $objetivos[$i]->periodo_logro){

                        if ($key > 0){

                          $index = $key - 1;
                          $objetivos[$i]->valor_rojo = $objetivos[$i]->valores[$index]->valor_rojo;
                      		$objetivos[$i]->valor_verde = $objetivos[$i]->valores[$index]->valor_verde;
                          $found = true;
                          break;
                        }
                      }
                    }
                  }

                  if (!$found){

                    $objetivos[$i]->valor_rojo = $objetivos[$i]->valores[0]->valor_rojo;
                    $objetivos[$i]->valor_verde = $objetivos[$i]->valores[0]->valor_verde;
                  }
    						}

    						$peso = $objetivos[$i]->peso;
								$porcentaje_bono = $peso * $bono->total_bono / 100;

    						// El objetivo es de tipo Si/No 
    						if ($objetivos[$i]->valor_verde == 'Si' || $objetivos[$i]->valor_verde == 'No'){

									// Se cumplió el objetivo
									if ($objetivos[$i]->logro == $objetivos[$i]->valor_verde){

										$color = 'green';
										$nivel_logro = 100;

										if (count($porcentajes_bono) > 0){

											foreach ($porcentajes_bono as $key => $value){
											
												if ($nivel_logro >= $value->nivel_logro){

													$porcentaje = $value->porcentaje_bono;

													if (empty($porcentaje)){

														$porcentaje = $nivel_logro;
													}

													$total_bono = $porcentaje * $porcentaje_bono / 100;
													break;
												}
											}
										}

										else{

											$total_bono = $porcentaje_bono;
										}
									}
								}

								// El objetivo es de tipo numérico
								else{

									if ($objetivos[$i]->valor_verde >= $objetivos[$i]->valor_rojo){

										$objetivos[$i]->valor_verde = str_replace(',', '', $objetivos[$i]->valor_verde) * 1;
										$objetivos[$i]->logro = str_replace(',', '', $objetivos[$i]->logro) * 1;

										// Se cumplió el objetivo
										if ($objetivos[$i]->logro >= $objetivos[$i]->valor_verde){

											$color = 'green';
											$nivel_logro = 100;

											if (count($porcentajes_bono) > 0){

												foreach ($porcentajes_bono as $key => $value){

													if ($value->porcentaje_bono > $value->nivel_logro){

														$porcentaje = $objetivos[$i]->logro * 100 / $objetivos[$i]->valor_verde;

														if ($porcentaje > $value->porcentaje_bono){

															$porcentaje = $value->porcentaje_bono;
														}

														$total_bono = $porcentaje * $porcentaje_bono / 100;
														break;
													}

													else{
											
														if ($nivel_logro >= $value->nivel_logro){

															$porcentaje = $value->porcentaje_bono;

															if (empty($porcentaje)){

																$porcentaje = $nivel_logro;
															}

															$total_bono = $porcentaje * $porcentaje_bono / 100;
															break;
														}
													}
												}
											}

											else{

												$total_bono = $porcentaje_bono;
											}
										}

										// Se cumplió a medias o no se cumplió
										else{

    									// Se calcula la calificación respecto al peso 
    									$valor = $objetivos[$i]->logro * 100 / $objetivos[$i]->valor_verde;
											$peso = $objetivos[$i]->peso * $valor / 100;

											if (count($porcentajes_bono) > 0){

												foreach ($porcentajes_bono as $key => $value){
											
													if ($valor >= $value->nivel_logro){

														$porcentaje = $value->porcentaje_bono;

														if (empty($porcentaje)){

															$porcentaje = $nivel_logro;
														}

														$total_bono = $porcentaje * $porcentaje_bono / 100;
														break;
													}
												}
											}

											else{

												$total_bono = $valor * $porcentaje_bono / 100;
											}

											if ($total_bono > 0){

												$color = '#FFDB58';
											}
										}
									}

									else{

										$objetivos[$i]->valor_verde = str_replace(',', '', $objetivos[$i]->valor_verde) * 1;
										$objetivos[$i]->logro = str_replace(',', '', $objetivos[$i]->logro) * 1;

										// Se cumplió el objetivo
										if ($objetivos[$i]->logro <= $objetivos[$i]->valor_verde){

											$color = 'green';
											$nivel_logro = 100;

											if (count($porcentajes_bono) > 0){

												foreach ($porcentajes_bono as $key => $value){
											
													if ($nivel_logro >= $value->nivel_logro){

														$porcentaje = $value->porcentaje_bono;

														if (empty($porcentaje)){

															$porcentaje = $nivel_logro;
														}

														$total_bono = $porcentaje * $porcentaje_bono / 100;
														break;
													}
												}
											}

											else{

												$total_bono = $porcentaje_bono;
											}
										}

										// Se cumplió a medias o no se cumplió
										else{

    									// Se calcula la calificación respecto al peso 
    									$valor = $objetivos[$i]->logro * 100 / $objetivos[$i]->valor_verde;
    									$resto = $valor - 100;
    									$valor = 100 - $resto;

    									if ($valor > 0){

												$peso = $objetivos[$i]->peso * $valor / 100;

												if (count($porcentajes_bono) > 0){

													foreach ($porcentajes_bono as $key => $value){
											
														if ($valor >= $value->nivel_logro){

															$porcentaje = $value->porcentaje_bono;

															if (empty($porcentaje)){

																$porcentaje = $nivel_logro;
															}

															$total_bono = $porcentaje * $porcentaje_bono / 100;
															break;
														}
													}
												}

												else{

													$total_bono = $valor * $porcentaje_bono / 100;
												}

												if ($total_bono > 0){

													$color = '#FFDB58';
												}
											}
										}
									}
								}
							}

							else{

								if (count($objetivos[$i]->valores) > 0){

									foreach ($objetivos[$i]->valores as $key => $value) {
										
										if ($value->periodo == $objetivos[$i]->periodo_logro){

											$total_bono = $value->meta;
											break;
										}
									}
								}

								else{

									$total_bono = $objetivos[$i]->objetivo;
								}
							}

							// Se suma la calificación del objetivo
							$promedio += $peso;
						} ?>

    		  <td class="text-right" style="padding: 5px">
    		    <input type="text" style="color: white; background-color: {{$color}}" value="{{'$' . number_format($total_bono, 2, '.', ',')}}" readonly class="form-control size text-right">
    		  </td>
    		@endif
			@endfor

			@for ($j = $contador_meses;$j <= 12; $j++)

          <td></td>
			@endfor

				</tr>
			</tbody>
		</table>
	</div>
</div>
<hr style="border-color: #A5A7A8">
<div class="row margin-top-20">
	<div class="col-md-12">
		<div style="max-height: 200px; overflow-y: auto">
			<h3 style="margin-top: 0">Observaciones</h3>
			<table width="100%" style="font-size: 13px" border="1" class="notas table-bordered table-striped table-hover">
				<thead style="background-color: #222B64; color:white;">
					<tr>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Fecha</th>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Persona</th>
						<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Nota</th>
					</tr>
				</thead>
				<tbody>

      	@for ($i = 0;$i < count($notas); $i++) 

					<tr>
						<td class="text-center" style="padding: 5px; border: 0">{{substr($notas[$i]->fecha, 8) * 1}}/{{$meses[substr($notas[$i]->fecha, 5, 2) * 1]}}/{{substr($notas[$i]->fecha, 0, 4)}}</td>
						<td class="text-center" style="padding: 5px; border: 0">{{$notas[$i]->nombre}} {{$notas[$i]->paterno}} {{$notas[$i]->materno}}</td>
						<td class="text-center" style="padding: 5px; border: 0">{{$notas[$i]->mensaje}}</td>
					</tr>
				@endfor

				</tbody>
			</table>
		</div>
		<h4 class="titulos-evaluaciones">Nota</h4>
		<div>
			<textarea style="width: 100%; height: 100px; border-radius: 7px"></textarea>
		</div>
	@if ((!empty($user->boss) && $user->boss->id == auth()->user()->employee_id) || $alimentar_propuestas == 2)
		<div class="text-right margin-top-10">
			<button class="btn btn-primary agregar_nota"><i class="fas fa-plus-circle"></i> Agregar nota</button>
		</div>
	@endif
	</div>
</div>
@else
				
<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="titulos-evaluaciones">No hay planes abiertos</h4>
	</div>
</div>
@endif
</div>
</div>
@endsection

@section('scripts')
	<script>

	  var id_plan = <?php echo (!empty($plan->id) ? $plan->id : 0)?>;
	  var id_empleado = <?php echo (!empty($id_user) ? $id_user : 0)?>;
		
		$(document).ready(function(){

			$.ajaxSetup({
    		headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
			});
				
			/*$('.tabla_logros').DataTable({
					scrollX: true,
    				scrollCollapse: true,
    				paging: false,
    				searching: false,
    				ordering: false,
    				info: false,
					//"sScrollX": "100%",
   					fixedColumns:   {
            			leftColumns: 5
        			}
				});*/
					$('.tabla_logros').DataTable({
					scrollX: true,
					scrollCollapse: true,
    				paging: false,
    				searching: false,
					ordering: false,
    				info: false
				});

			$('select.id_plan').change(function(){

				$('form#changePlanForm').submit();
			});

      $('button.agregar_nota').click(function(){

				var nota = $('textarea').val();

				$.ajax({
        	type:'POST',    
        	url: 'agregar-nota',
          data:{
          	mensaje: nota,
          	id_empleado: id_empleado,
          	id_plan: id_plan
          },
          success: function(data){
             
            var meses = [];
            meses[0] = 'Ene';
            meses[1] = 'Feb';
            meses[2] = 'Mar';
            meses[3] = 'Abr';
            meses[4] = 'May';
            meses[5] = 'Jun';
            meses[6] = 'Jul';
            meses[7] = 'Ago';
            meses[8] = 'Sep';
            meses[9] = 'Oct';
            meses[10] = 'Nov';
            meses[11] = 'Dic';
            var fecha = new Date();
            $('table.notas').prepend('<tr><td class="text-center" style="padding: 5px; border: 0">' + fecha.getDate() + '/' + meses[fecha.getMonth()] + '/' + fecha.getFullYear() + '</td><td class="text-center" style="padding: 5px; border: 0">' + data + '</td><td class="text-center" style="padding: 5px; border: 0">' + nota + '</td></tr>');
            $('textarea').val('');
          }
        });
			});

			$('input.porcentaje').inputmask("numeric", {
    		radixPoint: ".",
    		groupSeparator: ",",
    		digits: 2,
    		autoGroup: true,
    		suffix: '%', //No Space, this will truncate the first character
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});

			$('input.currency').inputmask("numeric", {
    		radixPoint: ".",
    		groupSeparator: ",",
    		digits: 2,
    		autoGroup: true,
    		prefix: '$', //No Space, this will truncate the first character
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});

			$('input.numeric').inputmask("numeric", {
    		groupSeparator: ",",
    		autoGroup: true,
    		rightAlign: false,
    		oncleared: function () { self.Value(''); }
			});
		});
	</script>
@endsection