@extends('layouts.app')

@section('title', 'Editar Usuario')

@section('content')

<div class="container my-5">
    <div class="row my-5">
        <div class="col">
                @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissible fade show card-1" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <strong>{{$error}}</strong>
                    </div>
                @endforeach
            @elseif(session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show card-1" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <strong>¡El usuario con el RFC: {{$usuario->employee->rfc}} fue creado con éxito!</strong>
                </div>
            @endif
            <div class="card card-3">
                <div class="card-header">
                    <h2 class="my-auto font-weight-bold">Crear Nuevo Usuario</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('admin-de-usuarios.store') }}" id="store">
                        {!! method_field('POST') !!}
                        {!! csrf_field() !!}	
                        <div class="form-row">

                            @foreach($fields as $key => $input)

                                @if($input['column'] == 'idempleado')
                                    <div class="form-group col-12 col-md-6">
                                        <label for="{{$input['column']}}" class="col-form-label font-weight-bold">{{$input['nameShow']}}:</label>
                                        <input type="text" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="" required>
                                    </div>
                                @elseif($input['column'] == 'extra2')
                                    <div class="form-group col-12 col-md-6">
                                        <label for="region_id" class="col-form-label font-weight-bold">Nacionalidad:</label>
                                        <input type="text" class="form-control" name="{{$input['column']}}"  id="{{$input['column']}}" value="" required>
                                    </div>
                                @endif

                            @endforeach

                            @foreach($fields as $key => $input)

                                @if($input['column'] == 'fechapuesto')
                                <div class="form-group col-12 col-md-6">
                                    <label for="{{$input['column']}}" class="col-form-label font-weight-bold">Fecha de asignación de puesto:</label>
                                    <input type="date" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="{{ old($input['column']) }}">
                                </div>
                                @elseif($input['column'] == 'nacimiento')
                                <div class="form-group col-12 col-md-6">
                                    <label for="{{$input['column']}}" class="col-form-label font-weight-bold">Fecha de Nacimiento:</label>
                                    <input type="date" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="" required>
                                </div>
                                @elseif($input['type'] == 'date')
                                <div class="form-group col-12 col-md-6">
                                    <label for="{{$input['column']}}" class="col-form-label font-weight-bold">{{ucfirst($input['column'])}}:</label>
                                    <input type="date" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="{{ old($input['column']) }}">
                                </div>
                                @elseif($input['type'] == 'curp')
                                <div class="form-group col-12 col-md-6">
                                    <label for="{{$input['column']}}" class="col-form-label font-weight-bold">CURP:</label>
                                    <input type="text" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="{{ old($input['column']) }}" required>
                                </div>
                                @elseif($input['column'] == 'idempresa')
                                <div class="form-group col-12 col-md-6">
                                    <label for="{{$input['column']}}" class="col-form-label font-weight-bold">{{ucfirst($input['column'])}}:</label>
                                    <input type="text" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="{{ old($input['column']) }}" readonly>
                                </div>
                                @elseif($input['column'] == 'password')
                                <div class="form-group col-12 col-md-6">
                                    <label for="password" class="col-form-label font-weight-bold">Contraseña:</label>
                                    <input type="password" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}">
                                </div>
                                @elseif($input['column'] == 'correoempresa')
                                <div class="form-group col-12 col-md-6">
                                    <label for="{{$input['column']}}" class="col-form-label font-weight-bold">Correo Empresarial:</label>
                                <input type="email" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="{{ old($input['column']) }}">
                                </div>
                                @elseif($input['column'] == 'correopersonal')
                                <div class="form-group col-12 col-md-6">
                                    <label for="{{$input['column']}}" class="col-form-label font-weight-bold">Correo Personal:</label>
                                <input type="email" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="{{ old($input['column']) }}">
                                </div>
                                @elseif($input['column'] == 'rol')
                                <div class="form-group col-12 col-md-6">
                                    <label for="{{$input['column']}}" class="col-form-label font-weight-bold">Rol:</label>
                                    <select class="custom-select" name="{{$input['column']}}">
                                        {{-- <option value="admin">Administrador</option> --}}
                                        <option value="employee">Empleado</option>
                                    </select>
                                </div>
                                @elseif($input['column'] == 'sexo')
                                <div class="form-group col-12 col-md-6">
                                    <label for="{{$input['column']}}" class="col-form-label font-weight-bold">Sexo:</label>
                                    <select class="custom-select" name="{{$input['column']}}">
                                        <option value="F">F</option>
                                        <option value="M">M</option>
                                    </select>
                                </div>
                                @elseif($input['column'] == 'direccion')
                                {{-- <div class="form-group col-12 col-md-6">
                                    <label for="direction" class="col-form-label font-weight-bold">Dirección:</label>
                                    <select name="{{$input['column']}}" id="direction" class="selectpicker form-control" data-live-search="true" title="Elije una Dirección...">
                                        @foreach($direcciones as $id => $direction)
                                            <option value="{{$id}}">{{$direction}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            
                                <div class="form-group col-12 col-md-6">
                                    <label for="division" class="col-form-label font-weight-bold">Departamento:</label>
                                    <select name="division" id="division" class="selectpicker form-control" data-live-search="true" title="Elije una departamento...">
                                        @foreach($departamentos as $id => $depto)
                                            <option value="{{ $id }}">{{ $depto }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="seccion" class="col-form-label font-weight-bold">Área:</label>
                                    <select name="seccion" id="seccion" class="selectpicker form-control" data-live-search="true" title="Elije una área...">
                                        @foreach($areas as $id => $area)
                                            <option value="{{ $id }}">{{$area}}</option>
                                        @endforeach
                                    </select>
                                </div> --}}
                                {{-- @elseif($input['column'] == 'job_position_id') --}}
                                <div class="form-group col-12 col-md-6">
                                    <label for="jobPosition" class="col-form-label font-weight-bold">Estructura <span class="text-muted">(Puesto)</span></label>
                                    <select name="job_position_id" id="jobPosition" class="selectpicker form-control" data-live-search="true" title="Elije un puesto por departamento..." required>
                                        @foreach($puestos as $puesto)
                                        <option value="{{$puesto->id}}">
                                            {{-- {{ $puesto->getDepartmentName() }} - {{ $puesto->name }} --}}
                                            {{ $puesto->getStructureNames() ?? 'SIN ESTRUCTURA' }} / {{ $puesto->name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                @elseif($input['column'] == 'jefe')
                                <div class="form-group col-12 col-md-6">
                                    <label for="{{$input['column']}}" class="col-form-label font-weight-bold">Jefe:</label>
                                    <select class="selectpicker form-control" data-live-search="true" title="Elije un Jefe..." name="{{$input['column']}}">
                                        @foreach($usuarios as $usuario)
                                            <option value="{{$usuario->employee->idempleado}}" data-subtext="{{ (isset($usuario->employee->jobPosition->area)?'('.$usuario->employee->jobPosition->area->name.')':'(N/A)') }}">{{$usuario->employee->nombre.' '.$usuario->employee->paterno.' '.$usuario->employee->paterno}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @elseif($input['column'] == 'enterprise_id')
                                <div class="form-group col-12 col-md-6">
                                    <label for="{{$input['column']}}" class="col-form-label font-weight-bold">Empresa:</label>
                                    
                                    <select class="selectpicker form-control" data-live-search="true" title="Elije una empresa..." name="{{$input['column']}}">
                                        @foreach($enterprises as $enterprise)
                                        <option value="{{$enterprise->id}}">{{$enterprise->name}}</option>
                                        @endforeach
                                    </select>
                                    
                                </div>
                                @elseif($input['column'] == 'region_id')
                                    <div class="form-group col-12 col-md-6">
                                        <label for="region_id" class="col-form-label font-weight-bold">{{$input['nameShow']}}:</label>
                                        <select name="{{$input['column']}}" id="region_id" class="selectpicker form-control" data-live-search="true" title="Elije una Región...">
                                            @foreach($regions as $region)
                                            <option value="{{$region->id}}">
                                                {{$region->name}}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                @elseif($input['column'] == 'extra2' || $input['column'] == 'idempleado'  || $input['column'] == 'seccion' || $input['column'] == 'division')
                                
                                @else
                                <div class="form-group col-12 col-md-6">
                                    <label for="{{$input['column']}}" class="col-form-label font-weight-bold">{{$input['nameShow']}}:</label>
                                    <input type="text" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="{{ old($input['column']) }}">
                                </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="form-group col col-md-6 mt-5">
                                <a href="{{asset('admin-de-usuarios')}}" class="btn btn-primary mb-md-0 card-1"><i class="fas fa-arrow-alt-circle-left"></i> Volver</a>
                                <button type="submit" class="btn btn-success card-1">Guardar Cambios <i class="fas fa-save"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
{{-- @section('scripts')
<script>

    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#direction').change(function(){
            var id = $(this).val();
            getDepartments(id);
        });

        function getDepartments(id){
            $.ajax({
                url: "{{url('departments')}}/"+id,
                Type:'GET',
                success: function(result){

                    $('#area').empty();
                    $('#area').selectpicker('refresh');

                    $('#department').empty();
                    $.each(result, function(i, item){
                        $('#department').append($('<option>', { 
                            value: item.id,
                            text : item.name 
                        })).selectpicker('refresh');
                    })
                    
                }
            });
        }

        function getAreas(id){
            $.ajax({
                url: "{{url('area')}}/"+id,
                Type:'GET',
                success: function(result){
                    $('#area').empty();
                    $.each(result, function(i, item){
                        $('#area').append($('<option>', { 
                            value: item.id,
                            text : item.name 
                        })).selectpicker('refresh');
                    })
                    
                }
            });
        }

        $('#department').change(function(){
            var id = $(this).val();
            getAreas(id);
        });

        function getJobs(id){
            $.ajax({
                url: "{{url('jobpositions')}}/"+id,
                Type:'GET',
                success: function(result){
                    $('#jobPosition').empty();
                    $.each(result, function(i, item){
                        $('#jobPosition').append($('<option>', { 
                            value: item.id,
                            text : item.name 
                        })).selectpicker('refresh');
                    })
                    
                }
            });
        }

        $('#area').change(function(){
            var id = $(this).val();
            getJobs(id);
        });

    });
</script>
@endsection --}}