
<html>
<head>

    <link href="{{ asset('css/pdf.css')}}" rel="stylesheet">
<style>
body {
font-family: "Century Gothic", serif !important;
font-size: 1rem !important;
margin: 8mm;
}


            /** Define the footer rules **/
            footer {
                position: fixed; 
                bottom: 2cm; 
                right: 0cm;
                float: right;
                height: 2cm;
            }
</style>
</head>
<body>

 <main class="py-4">
    <div class="row justify-content-center">
      <div class="col-md-8 text-center">
            <strong>
              MINISTERIO DE TRABAJO Y PREVISION SOCIAL <br/>
              DIRECCIÓN GENERAL DE TRABAJO <br/>
              CONTRATO INDIVIDUAL DE TRABAJO <br/>
             </strong>
        </div>
    </div>
    <br/><br/>
    <div class="row justify-content-center  mt-3">
      <div class="col-md-8 text-justify">
          
          <strong>{{ $enterprise->name }} </strong> quien actúa a través de 
          <strong>{{ $enterprise->legal->nombre }}, </strong> de 
          {{$employee->edad($enterprise->legal->fecha_nac)}}  años de edad, 
          {{ $enterprise->legal->nacionalidad }}, 
          {{ $enterprise->legal->edo_civil }},  
          {{ $enterprise->legal->profesion }},  de este domicilio, me identifico con el Documento Personal de Identificación con Código Único de Identificación número 
          
          <?php $countt = count(explode(' ', $enterprise->legal->dpi));
                    $ii = 1;
                   ?>
                  @foreach(explode(' ', $enterprise->legal->dpi) as $dpi) 
                    {{$employee->num2letras($dpi)}}
                    @if($ii < $countt)
                    ,
                    @endif
                    <?php $ii++; ?>
                @endforeach 

           ({{ $enterprise->legal->dpi }})  extendido por el Registro Nacional de las Personas de la República de Guatemala, en su calidad de 
          {{ $enterprise->legal->puesto }}  tal como lo acredita el Acta Notarial que contiene mi nombramiento, autorizada en la ciudad de Guatemala el 
          {{$employee->dateToSpanish($enterprise->legal->nombramiento)}} por el Notario 
          {{ $enterprise->legal->notario }},  debidamente inscrito en el Registro Mercantil General de la República bajo el número
           {{$employee->num2letras($enterprise->legal->numero_registro)}} ({{ $enterprise->legal->numero_registro }}),  
          folio {{$employee->num2letras($enterprise->legal->folio)}} ({{ $enterprise->legal->folio }}),  
          del libro {{$employee->num2letras($enterprise->legal->libro)}} ({{ $enterprise->legal->libro }})  de Auxiliares de Comercio y  por la parte trabajadora comparece 
          <strong>{!! $employee->nombre!!} {!! $employee->paterno!!} {!! $employee->materno!!}, </strong> de 
          {{$employee->edad($employee->nacimiento)}} años de edad, 
          {!! $employee->civil!!}, 
          {!! $employee->user->profile->contrato->nacionalidad!!},  
          {!! $employee->user->profile->contrato->profesion!!},  con domicilio en el departamento de 
          Suchitepéquez,  se identifica con el documento Personal de Identificación (DPI) con Código Único de Identificación (CUI) número 
          

        @foreach(explode(' ', $employee->curp) as $dpi) 
            {{$employee->num2letras($dpi)}}, 
          @endforeach 
           ({{ $employee->curp }})
          el cual le fue extendido por el Registro Nacional de las Personas (RENAP), con residencia en 
          COMUNICAD <span style="text-transform: uppercase">{!! $employee->user->profile->perfilAdicional->colony!!} DEL MUNICIPIO DE {!! $employee->user->profile->perfilAdicional->municipio->nombre!!} DEL DEPARTAMENTO DE {!! $employee->user->profile->perfilAdicional->estado->nombre!!}</span>.  Quienes en lo sucesivo nos denominaremos <strong>EMPLEADOR Y TRABAJADOR, </strong> respectivamente, consentimos en celebr/ar el <strong>CONTRATO INDIVIDUAL DE TRABAJO, </strong> contenido en las siguientes cláusulas:
        
        </div>
      </div>


            <div class="row justify-content-center  mt-3">

              <div class="col-md-8 text-justify">
                    
                <strong> PRIMERA:</strong> La relación de trabajo inició el  {{$employee->dateToSpanish($employee->ingreso)}}.

              <br/>
              <br/>
                  
                <strong>SEGUNDA: DESCRIPCIÓN DEL TRABAJO A REALIZAR: </strong><br/>
                EL TRABAJADOR prestará sus servicios laborales en forma exclusiva para la entidad contratante, en el puesto de 
                <strong>{!! $employee->jobPosition->name!!}, </strong> cuyo jefe inmediato es el 
                
                  @if(!isset($employee->jobPosition->bossJob))
                  {!! $employee->jobPosition->name!!}
                  @else
                  {!! $employee->jobPosition->bossJob->name!!}
                  @endif
                ,  teniendo como objetivo general: 
                {!! $employee->jobPosition->description!!}  y las siguientes atribuciones: 


              <br/>        

                <ul>
                  <li>
                    {!! $employee->jobPosition->knowledge!!}
                  </li>
                </ul>
              <br/>
              <br/>
                Y TODAS AQUELLAS RESPONSABILIDADES SEGÚN LOS REGLAMENTOS INTERNOS LABORALES DE LA ENTIDAD PATRONAL Y LOS QUE DISPONGA JUNTA DIRECTIVA.

              <br/>      
              <br/>      

                <strong>TERCERA:</strong> Por la naturaleza del trabajo, los servicios de El Trabajador serán prestados en distintos lugares tanto en la ciudad capital, en donde tiene la sede la sociedad contratante ubicada en: 
                {!! $employee->user->profile->contrato->sede_principal!!}.  Así mismo sus funciones pueden ser realizadas en el interior de la República de Guatemala.  entre los que se pueden mencionar son: 
                {!! $employee->user->profile->contrato->sede_secundaria!!} 

              <br/>
              <br/>

                <strong> CUARTA:</strong> La duración del presente contrato es: {!! $employee->user->profile->contrato->tipo_contrato!!}  

              <br/>
              <br/>

                 <strong> QUINTA:</strong> La jornada ordinaria de trabajo será diurna y discontinua, de cuarenta y cuatro (44) horas a la semana, fraccionada de la siguiente manera: 
                 {!! $employee->user->profile->contrato->horario!!}  

              <br/>
              <br/>

                <strong> SEXTA:</strong> El salario será de <span style="text-transform: uppercase;">
                  <?php $count = count(explode(',', $employee->user->profile->contrato->salario));
                    $i = 1; 
                    $salario = str_replace('.','-',$employee->user->profile->contrato->salario);
                    $salario = str_replace(',','.',$salario);
                    $salario = str_replace('-',',',$salario);
                    ?>

                  @foreach(explode(',', $salario) as $salario) 
                  {{$employee->num2letras(str_replace('.','',$salario))}} 
                    @if($i < $count)
                    con
                    @endif
                    <?php $i++; ?>
                @endforeach 
                </span> (Q.{!! $employee->user->profile->contrato->salario!!})   mensuales, cantidad que incluye la bonificación incentiva para los trabajadores del sector privado, de {{$employee->num2letras($employee->user->profile->contrato->bono)}} quetzales (Q.{!! $employee->user->profile->contrato->bono!!}) mensuales y le será pagado cada mes mediante depósito en una cuenta bancaria de El Trabajador o en las oficinas del empleador que son del pleno conocimiento del Trabajador. En el salario estipulado quedan comprendidos los pagos relativos a los días de descanso semanal, días de asueto, feriados locales y nacionales y cualquier otro estipulado por la ley. 

              <br/>
              <br/>

                <strong> SÉPTIMA:</strong> Las horas extras, el séptimo y los días de asueto, le serán pagados de conformidad con los artículos 121, 126, 127, del Código de Trabajo. Igualmente el presente contrato incluye pago de Viáticos, Seguro Médico, Teléfono móvil, sin constituir estos rubr/os ventajas económicas, ya que los mismos son para el desarrollo del trabajo. 

              <br/>
              <br/>

                <strong> OCTAVA:</strong> No Incluye Indemnización Universal, debiéndose regir al respecto según lo estipulado en el Código de Trabajo.

              <br/>
              <br/>
                <strong> NOVENA:</strong> Es entendido que de conformidad con el artículo 122 del Código de trabajo, la jornada ordinaria y extraordinaria no puede exceder de una suma total de 12 horas diarias.

              <br/>
              <br/>
                 <strong> DÉCIMA:</strong> Este contrato podrá darse por terminado con causa justa, además de las causales establecidas en el código de trabajo, por parte del empleador si El Trabajador no cumple con su trabajo a cabalidad, o bien con los objetivos requeridos por el puesto de trabajo, sin dar justificación para ello. 

              <br/>
              <br/>
                 <strong> DÉCIMA PRIMERA:</strong> No tiene permitido bajo ningún motivo o circunstancia El Trabajador a realizar funciones equivalentes o comparables a lo estipulado en este contrato aun de forma no remunerada para cualquier otra empresa, persona jurídica o individual o institución, así como que no tiene permitido realizar actos traducibles como competencia.

              <br/>
              <br/>
                 <strong> DÉCIMA SEGUNDA:</strong> En mi capacidad de empleado y en consideración de la relación laboral que mantengo con la empresa denominada <strong>{{ $enterprise->name }} </strong> así como del acceso que se me permite a sus distintas bases de Información, clientes, procedimientos, precios, know how, constato que, en mi calidad de trabajador:  

              <br/>
              <br/>
                1) Soy consciente de la importancia de mis responsabilidades en cuanto a no poner en peligro la integridad, disponibilidad y confidencialidad de la información que maneja mi empresa. Por tal razón me comprometo a cumplir con todas las obligaciones y procedimientos establecidos o que se establezcan en un futuro para el resguardo de la información que se considere como confidencial ya sea por la empresa o por ley.

              <br/>
              <br/>
                2) Me comprometo a cumplir, asimismo, todas las disposiciones relativas a la política de la empresa en materia de uso y divulgación de información, y a no divulgar la información que reciba a lo largo de mi relación con la empresa,  subsistiendo este deber de secreto, aun después de que finalice dicha relación por un periodo de cinco años y  tanto si esta información es de su propiedad, como si pertenece a un cliente de la misma, o a alguna otra Sociedad que nos proporcione el acceso a dicha información,  cualquiera que sea la forma de acceso a tales datos o información y el soporte en el que consten, quedando absolutamente prohibido obtener copias sin previa autorización. 
               
              <br/>
              <br/>
                 3)  Además declaro que todos los materiales producidos o adquiridos, durante la ejecución de ese contrato, ya sea que consten en documentos, gráficos, medios electrónicos de cualquier naturaleza, películas, cintas magnéticas, fotografías, o en cualquier otro medio, son y serán propiedad exclusiva de <strong>{{ $enterprise->name }} </strong> quien será el único autorizado para divulgarlos, por cualquier medio que estime necesario.
                   
              <br/>
              <br/>
                4) Entiendo que el incumplimiento de cualesquiera de las obligaciones que constan en el presente documento, intencionadamente o por negligencia, podrían implicar en su caso, las sanciones disciplinarias correspondientes por parte de la empresa y la posible reclamación por parte de la misma de los daños económicos causados, así como el poder iniciar los procesos legales correspondientes.  

              <br/>
              <br/>
                <strong> DÉCIMA TERCERA:</strong> El presente contrato se suscribe, en tres ejemplares, uno para cada una de las partes y el tercero que debe ser remitido al Registro Laboral de la Dirección General de Trabajo dentro de los 15 días siguientes a la suscripción, y se suscribe en la ciudad de Guatemala, 
                 {{$employee->dateToSpanish(date('Y-m-d'))}}

            </div>
        </div>
        <br><br><br><br>
        <div class="row justify-content-center  mt-5">
          <div class="col-md-8 text-justify mt-5"> 
               
            f) __________________________________________________

            <br/><br/>
            Firma del Patrono. 

          </div>
        </div>

        <div class="row justify-content-center  mt-5">
          <div class="col-md-8 text-justify mt-5"> 
               
            f) __________________________________________________

            <br/><br/>
            Firma del Trabajador. 

          </div>
        </div>

</body>
</html>
