@extends('bienes.app')
@section('content')

<div class="container-fluid">
	
		<div class="row mb-3">
				<div class="col-12 blue_header rounded py-3">
					<h2 class="font-italic font-weight-bold m-0"> Tipos de Recurso - Crear</h2>
				</div>
			</div>

	<div class="row">
		
		<div class="col-12">
			<h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid rgb(137, 135, 136);">Datos Generales</h3>
		</div>

		<div class="col-12 mt-4">
			<form action="{{ url('Tipobienes') }}" method="POST">
				@csrf
				
				<div class="form-row">
					<div class="form-group col-md-3">
						<label for="id_codigo">Código</label>
						<input type="text" class="form-control" name="codigo" id="id_codigo" placeholder="" required="">
					</div>
					<div class="form-group col-md-3">
						<label for="id_nombre">Nombre</label>
						<input type="text" class="form-control" name="nombre" id="id_nombre" placeholder="" required="">
					</div>
					<div class="form-group col-md-3">
						<label for="id_descripcion">Descripción</label>
						<input type="text" class="form-control" name="descripcion" id="id_descripcion" placeholder="">
					</div>
					<div class="col-md-3">
						<label for="id_estatus">Estado:</label>
						<select name="estatus" id="id_estatus" class="form-control" required="">
							<option disabled value="" >Seleccione una opción...</option>
							<option value="1" selected>Disponible</option>
							<option value="0">No Disponible</option>
						</select>
					</div>
				</div>

				<div class="row mt-4" id="Especificaciones">
					<div class="col-12">
						<h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid rgb(137, 135, 136);">Especificaciones</h3>
					</div>
				</div>
				<div class="form-row">
					@foreach ($bienesVariables as $variable)

						<div class="form-check col-md-2 form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mt-2 mb-md-0">
							<label class="form-check-label" for="{{ $variable->etiqueta }}">{{ $variable->etiqueta }}</label>
							<input class="form-check-input" type="checkbox" name="variables[{{ $variable->id }}]" id="{{ $variable->etiqueta }}" value="{{ $variable->id }}">
						</div>

					@endforeach
				</div>
				
				<hr style="border: 2px solid #000000;">
				<div class="form-row">
					<div class="form-group col col-md-4">
					</div>
					<div class="form-group col col-md-8 d-flex justify-content-end">
						
						<a href="{{url('Tipobienes')}}" class="btn btn-primary mr-3">Regresar</a>

						<button type="submit" class="btn btn-success mr-3">Guardar</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

	{{-- PARA MOSTRAR UNA VENTANA DE ERROR SINO SE ENCUENTRA EL ARCHIVO --}}
	@if (Session::has('flag'))
		<script type="text/javascript">
			$(function() {
				$('#modalError').modal('show');
			});
		</script>
	@endif

	<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					{!! Session::get('flag') !!}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<style type="text/css">
	.clase_error {
		display: none;
		color: red;
	}
</style>

<script type="text/javascript">
	$('form').submit(function(e){
    // si la cantidad de checkboxes "chequeados" es cero,
    // entonces se evita que se envíe el formulario y se
    // muestra una alerta al usuario
    if ($('input[type=checkbox]:checked').length === 0) {
        e.preventDefault();
        alert('Debe indicar al menos una especificación');

	    var target_offset = $("#Especificaciones").offset();
		var target_top = target_offset.top;
	    $('html,body').animate({scrollTop:target_top},{duration:"slow"});
    }
});

</script>


@endsection