@extends('bienes.app')
@section('content')

	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	<br>
	
	
<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"> Administrar Tipos de Recurso</h2>
    </div>
</div>



<div class="row justify-content-center mb-3">
    <div class="col-md-4">
        <a  href="{{ url('Tipobienes/create') }}" class="btn btn-primary btn-block"> Nuevo Tipo Recurso</a>
    </div>
</div>

<div class="table-responsive">
		<div class="col-md-12 tableWrapper">
		<table class="table table-hover table-bordered" id="tableBienes">
			<thead class="blue_header">

			<tr>
				<th>Código</th>
				<th>Nombre</th>
				<th>Descripción</th>
				<th>Estatus</th>
				<th>Opciones</th>
				{{-- <th>ruta</th> --}}
			</tr>
		</thead>
		<tbody>
			
			@foreach($bienes as $bien)
				<tr>
					<td>{{ $bien->codigo }}</td>
					<td>{{ $bien->nombre }}</td>
					<td>{{ $bien->descripcion }}</td>
					<td>
	 					@if($bien->estatus==1)
	                        Disponible
	                    @else
	                    	No Disponible
	                    @endif
					<td>
						<a class="btn btn-sm btn-success" href="{{ url('Tipobienes/' . $bien->id) }}">Ver</a>
						<a class="btn btn-sm btn-warning" href="{{ url('Tipobienes/' . $bien->id.'/edit') }}">Editar</a>
					</td>
				</tr>

			@endforeach

		</tbody>
	</table>
		</div>
	</div>
@endsection

@section('scripts')

<script type="text/javascript">
	$('#tableBienes').DataTable({
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
   	});
</script>
@endsection