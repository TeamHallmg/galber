@extends('bienes.app')
@section('content')

	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	<br>
	
	<div class="row mb-3">
			<div class="col-12 blue_header rounded py-3">
				<h2 class="font-italic font-weight-bold m-0"> Administrar Recurso - Tipos de detalles</h2>
			</div>
		</div>
		
		
		
		<div class="row justify-content-center mb-3">
			<div class="col-md-4">
				<a href="{{ url('TipoBienesDetalles/create') }}" class="btn btn-primary btn-block"> Nuevo Tipo detalle</a>
			</div>
		</div>
		
		<div class="table-responsive">
				<div class="col-md-12 tableWrapper">
				<table class="table table-hover table-bordered" id="tableBienes">
					<thead class="blue_header">

			<tr>
				<th>Nombre</th>
				<th>Descripción</th>
				<th>Tipo</th>
				<th>Estatus</th>
				<th>Opciones</th>
				{{-- <th>ruta</th> --}}
			</tr>
		</thead>
		<tbody>
			
			@foreach($Variable as $Detalle)
				<tr>
					<td>{{ $Detalle->etiqueta }}</td>
					<td>{{ $Detalle->descripcion }}</td>
					<td>{{ $Detalle->tipo }}</td>
					<td>
	 					@if($Detalle->estado==1)
	                        Disponible
	                    @else
	                    	No Disponible
	                    @endif
					<td>
						<a class="btn btn-sm btn-success" href="{{ url('TipoBienesDetalles/' . $Detalle->id) }}">Ver</a>
						<a class="btn btn-sm btn-warning" href="{{ url('TipoBienesDetalles/' . $Detalle->id.'/edit') }}">Editar</a>
					</td>
				</tr>

			@endforeach

		</tbody>
	</table>
				</div>
		</div>
@endsection

@section('scripts')

<script type="text/javascript">
	$('#tableBienes').DataTable({
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
   	});
</script>
@endsection