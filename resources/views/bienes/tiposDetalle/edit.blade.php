@extends('bienes.app')
@section('content')

<div class="container-fluid">
		<div class="row mb-3">
				<div class="col-12 blue_header rounded py-3">
					<h2 class="font-italic font-weight-bold m-0"> Tipos de detalles - Editar</h2>
				</div>
			</div>

	<form action="{{ route('TipoBienesDetalles.update', $TipoBienesDetalle->id) }}" method="POST" enctype="multipart/form-data">
	@csrf
	@method('PUT')
		<div class="row">
			
		<div class="col-12 mt-4">
				<h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid rgb(137, 135, 136);">Datos Generales</h3>
			</div>
			<div class="col-12 mt-4">
					<div class="form-row">
						<div class="form-group col-md-2">
							<label for="id_etiqueta">Etiqueta</label>
							<input type="text" class="form-control" name="etiqueta" id="id_etiqueta" placeholder="" value="{{ $TipoBienesDetalle->etiqueta }}"  required="" >
						</div>
						<div class="form-group col-md-2">
							<label for="id_descripcion">Descripción</label>
							<input type="text" class="form-control" name="descripcion" id="id_descripcion" placeholder="" value="{{ $TipoBienesDetalle->descripcion }}" required="" >
						</div>
						<div class="form-group col-md-2">
							<label for="id_longitud">Longitud</label>
							<input type="text" class="form-control" name="longitud" id="id_longitud" placeholder="" value="{{ $TipoBienesDetalle->longitud }}" value="191" required="" >
						</div>

						<div class="col-md-2">
							<label for="id_tipo">Tipo:</label>
							<select name="tipo" id="id_tipo" class="form-control" required="">
								<option disabled value="" >Seleccione una opción...</option>
								<option value="text" selected>Texto</option>
								<option disabled value="int">Entero</option>
								<option disabled value="float">Decimal</option>
								<option disabled value="bool">Verdadero/Falso</option>
								<option disabled value="date">Fecha</option>
								<option disabled value="time">Hora</option>
								<option disabled value="select">Lista Seleccionable</option>
								<option disabled value="multiple">Select Multiple</option>
							</select>
						</div>
						<div class="col-md-2">
							<label for="id_estado">Estado:</label>
							<select name="estado" id="id_estado" class="form-control" required="">
								<option disabled value="" >Seleccione una opción...</option>
								<option value="1" selected>Disponible</option>
								<option value="0">No Disponible</option>
							</select>
						</div>
					</div>
					
					<div class="form-row">
						<div class="col-md-2">
							<input type="checkbox" name="requerido" value="0" id="id_requerido" {{ $TipoBienesDetalle->requerido == 1 ? 'checked' : '' }}>
							<label for="id_requerido">Requerido</label>	
						</div>
						<div class="col-md-2">
							<input type="checkbox" name="multiple" value="0" id="id_multiple" {{ $TipoBienesDetalle->multiple == 1 ? 'checked' : '' }}>
							<label for="id_multiple">Multiple</label>	
						</div>
						<div class="col-md-2">
							<input type="checkbox" name="editable" value="0" id="id_editable" {{ $TipoBienesDetalle->editable == 1 ? 'checked' : '' }}>
							<label for="id_editable">Editable</label>	
						</div>
						<div class="col-md-2">
							<input type="checkbox" name="seleccionable" value="0" id="id_seleccionable" {{ $TipoBienesDetalle->seleccionable == 1 ? 'checked' : '' }}>
							<label for="id_seleccionable">Seleccionable</label>	
						</div>
					</div>
					
					<div class="form-row">

						<div class="form-group col col-md-4">
						</div>
						<div class="form-group col col-md-8 d-flex justify-content-end">
							<a class="btn btn-info mr-3" href="{{ url('TipoBienesDetalles') }}">Regresar</a>
							<button type="submit" class="btn btn-success mr-3">Actualizar</button>
						</div>
					</div>
			</div>
		</div>
	</form>
</div>

	{{-- PARA MOSTRAR UNA VENTANA DE ERROR SINO SE ENCUENTRA EL ARCHIVO --}}
	@if (Session::has('flag'))
		<script type="text/javascript">
			$(function() {
				$('#modalError').modal('show');
			});
		</script>
	@endif

	<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					{!! Session::get('flag') !!}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<style type="text/css">
	.clase_error {
		display: none;
		color: red;
	}
</style>
<script type="text/javascript">
	$(function() {

			$("#id_tipo option[value='{{ $TipoBienesDetalle->tipo }}']").prop('selected', true);
			$("#id_estado option[value='{{ $TipoBienesDetalle->estado }}']").prop('selected', true);

		$('form').submit(function() {
			$("button[type='submit']").prop('disabled',true);
		});
	});
</script>
@endsection