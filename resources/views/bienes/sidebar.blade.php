
<div class="row">
        <div class="col-10">
            <h3 class="text-blue font-weight-bold font-italic d-flex justify-content-around">
                <span>Menú</span>
            </h3>
        </div>
    </div>
    <hr>
    <a class="text-decoration-none" href="{{ url('Tipobienes') }}">
    <h5 class="right-menu-element @if(Request::url() === url('/Tipobienes/{id}')) right-menu @endif">
      Tipos de Recurso
    </h5>
</a>
<a class="text-decoration-none" href="{{ url('TipoBienesDetalles') }}">
    <h5 class="right-menu-element @if(Request::url() === url('/common/plea/create')) right-menu @endif">
      Tipo de Detalles
    </h5>
</a>