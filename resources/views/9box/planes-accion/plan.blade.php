@extends('layouts.app')

@section('title', 'Plan de Acción')

@section('content')
<?php function numberFormatPrecision($number, $precision = 2, $separator = '.'){

    $number = $number . '';
    $numberParts = explode($separator, $number);
    $response = $numberParts[0];

    if (count($numberParts) > 1 && $precision > 0){

      $response .= $separator;
      $response .= substr($numberParts[1], 0, $precision);
    }

    return $response;
  } ?>

	<style type="text/css">
		table.table.resultados tbody tr td {vertical-align: middle;text-align: center; position: relative; height: 57px}
		table.table.resultados tbody tr td b {position: absolute; right: 2px; bottom: 0; font-size: 20px}
		table.table.resultados tbody tr td span {position: absolute; left: 2px; top: 0; font-size: 25px; border-radius: 50%; border: 2px solid white; color: #004A91; background-color: white; cursor: pointer;}
		h3.rotate {transform: rotate(-90deg); float: right; position: relative; right: -64px; top: 117px}
		.table-striped tbody tr:nth-of-type(odd) {
    			background-color: #eee;
		}
	</style>

	<div class="row mt-3">
		<div class="col-md-2 text-right">
		    @include('/9box/partials/sub-menu')
		</div>
		<div class="col-md-10">
			<img class="img-fluid w-100" src="/img/9box/edr.png" alt="">
		</div>
		<div class="col-md-12 text-center">
			<h3 class="mt-3 titulos-evaluaciones" style="margin-bottom: 20px">Plan de Acción</h3>
			<hr style="border-top: 3px solid #273778;">
		</div>
	</div>

	<div class="row mt-3">
		<div class="offset-3 col-md-6">
			<table class="w-100">
				<tr>
					<td><b>Usuario:</b></td>
					<td>
						<input type="text" value="{{!empty($user) ? $user->Fullname : ''}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
					</td>
				</tr>
				<tr>
					<td><b>Puesto:</b></td>
					<td>
						<input type="text" value="{{(!empty($user->employee) ? $user->employee->getPuestoName() : '')}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
					</td>
				</tr>
    		</table>
		</div>
	</div>

<?php $meses = array(0,'Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic');
			$sumaPonderantes = 0;
			$actual_tipo_objetivo_corporativo = ''; ?>
    <div class="row mt-3">
		<div class="col-md-12">
			<table class="w-100 table">
                <thead style="background-color: #273778" class="text-white">
                    <th class="text-center">Objetivo</th>
                    <th class="text-center">Código</th>
                    <th class="text-center">Fórmula</th>
                    <th class="text-center">Peso Factor</th>
                    <th class="text-center">Mínimo</th>
                    <th class="text-center">Meta</th>

                    @for ($i = 0;$i < $total_logros;$i++)

                    <th class="text-center">{{$meses[$plan_start_month + $i]}}</th>
                    @endfor

                    <th class="text-center">Resultado</th>
                    <th class="text-center">Resultado Ponderado</th>
                </thead>
                <tbody>					
                    @foreach ($objetivos as $objetivo)
                <?php $counter = 0; ?>
                    	@if ($tipos_objetivos_corporativos[$objetivo->id_objetivo_corporativo] != $actual_tipo_objetivo_corporativo)
                    		<tr class="alert alert-info">
                    			<td colspan="{{8 + $total_logros}}">{{$tipos_objetivos_corporativos[$objetivo->id_objetivo_corporativo]}}</td>
                    		</tr>
                  <?php $actual_tipo_objetivo_corporativo = $tipos_objetivos_corporativos[$objetivo->id_objetivo_corporativo]; ?>
                  		@endif
                        <tr class="table-striped">
                            <td>{{ $objetivo->nombre }}</td>
                            <td>{{ (isset($catalogo_objetivos[$objetivo->id_catalogo]) ? $catalogo_objetivos[$objetivo->id_catalogo] : '') }}</td>
                            <td>{{ $objetivo->formula }}</td>
                            <td class="text-center">{{ $objetivo->peso }}</td>
                            <td class="text-center">{{ $objetivo->valor_rojo }}</td>
                            <td class="text-center">{{ $objetivo->objetivo }}</td>
							@foreach ($logros as $logro)
								@if ($logro->objetivo_id == $objetivo->id)
									<td class="text-center">{{ $logro->logro }}</td>
						<?php $counter++; ?>
								@endif
							@endforeach
							
							@if($counter != $total_logros)
								@for ($i = $counter; $i < $total_logros; $i++)
									<td></td>
								@endfor
							@endif
                            <td class="text-right">{{ $promedio[$loop->index] }}%</td>
                            <td class="text-right">{{ (($objetivo->peso * 1.2) >= $ponderante[$loop->index] ? numberFormatPrecision($ponderante[$loop->index]) : numberFormatPrecision($objetivo->peso * 1.2)) }}%</td>
                      <?php $sumaPonderantes += (($objetivo->peso * 1.2) >= $ponderante[$loop->index] ? $ponderante[$loop->index] : $objetivo->peso * 1.2); ?>
                        </tr>
                    @endforeach
					<tr class="alert alert-info">
						<td>Cuantitativa</td>
						<td></td>
						<td></td>
						<td class="text-center">{{ $sumaPeso }}%</td>
						<td></td>
						<td></td>

							@for ($i = 0;$i < $total_logros;$i++)

            <td></td>
              @endfor

						<td class="text-right font-weight-bold"> <small>TOTAL:</small></td>
						<td class="text-right">{{ numberFormatPrecision($sumaPonderantes) }}%</td>
					</tr>
					<tr class="alert alert-warning">
						<td>Cualitativa</td>
						<td></td>
						<td></td>
						<td class="text-center">{{ $cualitativa }}%</td>
						<td></td>
						<td></td>

							@for ($i = 0;$i < $total_logros;$i++)

            <td></td>
              @endfor

						<td class="text-right font-weight-bold"> <small>TOTAL:</small></td>
						<td class="text-right font-weight-bold">{{ $resultadoDesempeno }}%</td>
					</tr>
					<tr class="alert alert-success">
						<td>TOTAL</td>
						<td></td>
						<td></td>
						<td class="text-center">{{ $sumaTotal }}%</td>
						<td></td>
						<td></td>
						<td></td>
						{{-- <td></td> --}}
						{{-- <td></td> --}}
						{{-- <td></td> --}}
						<td class="text-right font-weight-bold text-nowrap" colspan="{{$total_logros}}">CALIFICACIÓN TOTAL:</td>
						<td class="text-right font-weight-bold">{{ $resultadoDesempeno + numberFormatPrecision($sumaPonderantes) }}%</td>
					</tr>
                </tbody>
    		</table>
		</div>
	</div>

	@if(($b9_periodo->status == 'Preparatorio' && auth()->user()->role == 'admin') || $b9_periodo->status == 'Abierto')
	<div class="row">
	<div class="col-md-12">
		<form action="/guardar-comentarios-reporte" method="post" target="response" class="comentarios_reporte">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id_evaluado" value="{{$user->id}}">
			<input type="hidden" name="id_periodo" value="{{$id_periodo}}">
			<div class="col-md-12 my-4 oculto2">
				<div class="text-center titulos-evaluaciones font-weight-bold" style="font-size: 20px">Retroalimentación</div>
				<textarea style="width: 100%; height: 100px; resize: none;" name="comentarios_adicionales" <?php if (empty($user->employee_wt->boss) || ($user->employee_wt->boss->user->id != auth()->user()->id) || !$evaluaciones_terminadas){ ?>readonly="readonly"<?php } ?> class="retro">{{(count($comentario) > 0 ? $comentario[0]->comentario : '')}}</textarea>

		@if (!empty($user->employee_wt->boss) && $user->employee_wt->boss->user->id == auth()->user()->id && $evaluaciones_terminadas)

				<div class="text-center oculto" style="font-size: 13px; color: red">
					<strong>* Este campo es obligatorio para poder imprimir el reporte.</strong>
				</div>
		@endif

			</div>
			<div class="col-md-12">
				<div class="text-center titulos-evaluaciones objetivos_entregables font-weight-bold" style="font-size: 20px">Competencias y acciones a realizar</div>
				<p class="text-center">¿Qué competencias y/o comportamientos relacionados con mi puesto de trabajo necesito mejorar? ¿Qué acciones específicas se pueden realizar para lograrlo?</p>
				<table class="table-fluid objetivos_entregables" border="1" width="100%">
					<thead>
						<tr>
							<th class="text-center" width="50%">Nombre de la competencia</th>
							<th class="text-center" width="50%">Acciones</th>
						</tr>
					</thead>
					<tbody>

<?php foreach ($objetivos_entregables_desempeno as $key => $value){ ?>

						<tr>
							<td>

			@if ($user->employee_wt->boss->user->id != auth()->user()->id || !$evaluaciones_terminadas)

								<textarea style="width: 100%;height: 100%;resize: none;border: none;overflow: auto;outline: none;" readonly="readonly" class="retro">{{$value->objetivo}}</textarea>
			@else

								<select class="form-control oculto" style="width: 100%" name="objetivo{{$key + 1}}">

									<option value="0">-- Selecciona Competencia --</option>

				@foreach ($competencias as $key2 => $competencia)
						
									<option value="{{$competencia->nombre}}" <?php if ($competencia->nombre == $value->objetivo){ ?>selected="selected"<?php } ?>>{{$competencia->nombre}}</option>
				@endforeach		

								</select>
								<textarea style="width: 100%;height: 100%;resize: none;border: none;overflow: auto;outline: none;display: none;padding-left: 10px" class="mostrar">{{$value->objetivo}}</textarea>
			@endif

							</td>
							<td>
								<textarea style="width: 100%;height: 100%;resize: none;border: none;overflow: auto;outline: none;padding-left: 10px" name="accion{{$key + 1}}" <?php if ($user->employee_wt->boss->user->id != auth()->user()->id || !$evaluaciones_terminadas){ ?>readonly="readonly"<?php } ?> class="retro">{{$value->accion}}</textarea>
							</td>
						</tr>
<?php }

		for ($i = count($objetivos_entregables_desempeno) + 1; $i < 6;$i++){ ?>

						<tr>
							<td>

			@if (empty($user->employee_wt->boss) || $user->employee_wt->boss->user->id != auth()->user()->id || !$evaluaciones_terminadas)

								<textarea style="width: 100%;height: 100%;resize: none;border: none;overflow: auto;outline: none;" readonly="readonly" class="retro"></textarea>
			@else

								<select class="form-control oculto" style="width: 100%" name="objetivo{{$i}}">

									<option value="0">-- Selecciona Competencia --</option>

				@foreach ($competencias as $key => $competencia)
						
									<option value="{{$competencia->nombre}}">{{$competencia->nombre}}</option>
				@endforeach		

								</select>
			@endif
						
							</td>
							<td>
								<textarea style="width: 100%;height: 100%;resize: none;border: none;overflow: auto;outline: none;" name="accion{{$i}}" <?php if (empty($user->employee_wt->boss) || $user->employee_wt->boss->user->id != auth()->user()->id || !$evaluaciones_terminadas){ ?>readonly="readonly"<?php } ?> class="retro"></textarea>
							</td>
						</tr>
<?php } ?>

					</tbody>
				</table>
			</div>

<?php if (!empty($user->employee_wt->boss) && $user->employee_wt->boss->user->id == auth()->user()->id  && $evaluaciones_terminadas){ ?>

			<div style="color: red; font-size: 13px" class="text-center oculto">
				<b>* Es necesario indicar mínimo una competencia a mejorar para poder imprimir el reporte.</b>
			</div>
			<div class="col-md-12 margin-top-20 text-center oculto">
				<input type="submit" class="btn btn-primary" value="Guardar Comentario y Objetivos">
			</div>
<?php } ?>

		</form>
	</div>
	</div>
	@endif

	<div class="row mt-3">
		<div class="col-md-12 d-flex justify-content-end">
			<a href="{{ url()->previous() }}" class="btn btn-primary">Regresar</a>
		</div>
	</div>
@endsection

@section('scripts')
	<script>

	$(document).ready(function(){
    });

	</script>
@endsection
