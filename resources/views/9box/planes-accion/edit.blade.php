@extends('layouts.app')

@if ($canEdit)

  @section('title', 'Editar Medida o Acción')
@else

  @section('title', 'Ver Medida o Acción')
@endif

@section('content')
<div class="row margin-top-20">
  <div class="col-md-2 text-right">
  @include('9box/partials/sub-menu')
  </div>
  <div class="col-md-10">
    <img class="img-responsive" src="{{ asset('img/9box/banner 9 box maver.png') }}" alt="">
  </div>
  <div class="col-md-12 text-center">

  @if ($canEdit)

    <h3 class="margin-top-20 titulos-evaluaciones" style="margin-bottom: 20px">Editar Medida o Acción</h3>
  @else

    <h3 class="margin-top-20 titulos-evaluaciones" style="margin-bottom: 20px">Medida o Acción</h3>
  @endif

  </div>
</div>
<div class="row margin-top-20">
  <form action="/9box/plan-accion/{{$plan->id}}" method="POST" enctype="multipart/form-data">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label>Medida o Acción</label>
          <textarea name="accion" class="form-control" rows="5" required disabled>{{$plan->accion}}</textarea>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label>Descripción</label>
          <textarea name="descripcion" class="form-control" rows="5" required disabled>{{$plan->descripcion}}</textarea>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label>Estatus</label>
          <select class="form-control" name="status">

  @if ($canEdit)

            <option value="Planeado">Planeado</option>
            <option value="Iniciado" <?php if ($plan->status == 'Iniciado'){ ?>selected="selected"<?php } ?>>Iniciado</option>
            <option value="Pausado" <?php if ($plan->status == 'Pausado'){ ?>selected="selected"<?php } ?>>Pausado</option>
            <option value="Terminado" <?php if ($plan->status == 'Terminado'){ ?>selected="selected"<?php } ?>>Terminado</option>
            <option value="Cancelado" <?php if ($plan->status == 'Cancelado'){ ?>selected="selected"<?php } ?>>Cancelado</option>
  @else

            <option>{{$plan->status}}</option>
  @endif

          </select>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <h2>Bitácora</h2>
          <div style="max-height: 200px; overflow-y: auto">
            <table width="100%" border="1" class="table table-bordered table-hover table-striped notas">
              <thead style="background-color: rgb(122, 117, 181); color: white;">
                <tr>
                  <th>Registro</th>
                  <th>Avance</th>
                  <th>Documento</th>
                  <th>Creado Por</th>
                </tr>
              </thead>
              <tbody>

  @if (!empty($bitacora))

    @for ($i = 0;$i < count($bitacora);$i++) 

                <tr>
                  <td>{{$bitacora[$i]->created_at}}</td>
                  <td>{{$bitacora[$i]->avances}}</td>
                    
      @if (!empty($bitacora[$i]->evidencia))
                            
                  <td>
                    <a class="btn btn-success" href="/documents/9box/plan_accion/{{$bitacora[$i]->id_plan}}/{{$bitacora[$i]->id}}/{{$bitacora[$i]->evidencia}}" style="background-color: #ebc200" download>
                      <span class="icon-span-filestyle glyphicon glyphicon-file"></span>
                    </a>
                  </td>
      @else
                              
                  <td></td>
      @endif
                            
                  <td>{{$users[$bitacora[$i]->created_by]}}</td>
                </tr>
    @endfor
  @endif

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

  @if ($canEdit)

    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label>Avance</label>
            <textarea name="avances" class="form-control" rows="3"></textarea>
        </div>
      </div>
    </div>
    <div class="text-right">
      <div class="row">
        <div class="col-md-8"></div>
        <div class="col-md-4">
          <input class="filestyle" data-buttontext="Explorar" data-badge="false" style="display: none;" data-buttonname="btn-custom-" name="file" type="file" id="file" accept="application/pdf">
          <div class="bootstrap-filestyle input-group">
            <input type="text" class="form-control " placeholder="Documento PDF" disabled="">
            <span class="group-span-filestyle input-group-btn" tabindex="0" style="background-color: rgb(122, 117, 181); font-weight: bold; color: white; height: 37px">
              <label for="file" class="btn btn-custom-guia" style="font-weight: bold; color: white; height: 27px">
                <span class="icon-span-filestyle fas fa-folder-open"></span> <span class="buttonText">Explorar</span>
              </label>
            </span>
          </div>
        </div>
      </div>
      <input type="submit" value="Guardar" class="btn btn-primary"> <a href="/9box/plan-accion" class="btn btn-success">Regresar</a>
    </div>
  @else

    <div class="text-right">
      <a href="/9box/plan-accion" class="btn btn-success">Regresar</a>
    </div>
  @endif

  </form>
</div>
<div class="col-md-2"></div>
@endsection

@section('scripts')
<script>

  $(document).ready(function(){

    $('.filestyle').change(function(){
      
      $('.bootstrap-filestyle input[type="text"]').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });
  });

</script>
@endsection