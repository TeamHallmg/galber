@extends('layouts.app')

@section('title', 'Crear Medida o Acción')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
	@include('9box/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-responsive" src="{{ asset('img/9box/banner 9 box maver.png') }}" alt="">
	</div>
	<div class="col-md-12 text-center">
		<h3 class="margin-top-20 titulos-evaluaciones" style="margin-bottom: 20px">Crear Medida o Acción</h3>
	</div>
</div>
<div class="row margin-top-20">
	<form action="/9box/plan-accion/create" method="POST" enctype="multipart/form-data">
    <div class="row">
      <input type="hidden" name="period_id" value="{{ $period_id }}">
      <div class="col-md-12">
        <div class="form-group">
          <label>Medida o Acción</label>
          <textarea name="accion" class="form-control" rows="5" required></textarea>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label>Descripción</label>
          <textarea name="descripcion" class="form-control" rows="5" required></textarea>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label>Estatus</label>
          <select class="form-control" name="status">
            <option value="Planeado">Planeado</option>
            <option value="Iniciado">Iniciado</option>
            <option value="Pausado">Pausado</option>
            <option value="Terminado">Terminado</option>
            <option value="Cancelado">Cancelado</option>
          </select>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <h2>Bitácora</h2>
        </div>
        <div class="form-group">
          <label>Avance</label>
            <textarea name="avances" class="form-control" rows="3"></textarea>
        </div>
      </div>
    </div>
    <div class="text-right">
      <div class="row">
        <div class="col-md-8"></div>
        <div class="col-md-4">
          <input class="filestyle" data-buttontext="Explorar" data-badge="false" style="display: none;" data-buttonname="btn-custom-" name="file" type="file" id="file" accept="application/pdf">
          <div class="bootstrap-filestyle input-group">
            <input type="text" class="form-control " placeholder="Documento PDF" disabled="">
            <span class="group-span-filestyle input-group-btn" tabindex="0" style="background-color: rgb(122, 117, 181); font-weight: bold; color: white; height: 37px">
              <label for="file" class="btn btn-custom-guia" style="font-weight: bold; color: white; height: 27px">
                <span class="icon-span-filestyle fas fa-folder-open"></span> <span class="buttonText">Explorar</span>
              </label>
            </span>
          </div>
        </div>
      </div>
      <input type="submit" value="Guardar" class="btn btn-primary"> <a href="/9box/plan-accion" class="btn btn-success">Regresar</a>
    </div>
  </form>
</div>
<div class="col-md-2"></div>
@endsection

@section('scripts')
<script>

  $(document).ready(function(){

    $('.filestyle').change(function(){
      
      $('.bootstrap-filestyle input[type="text"]').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });
  });

</script>
@endsection