@extends('layouts.app')

@section('title', 'Plan de Acción')

@section('content')
	<style type="text/css">
		table.table.resultados tbody tr td {vertical-align: middle;text-align: center; position: relative; height: 57px}
		table.table.resultados tbody tr td b {position: absolute; right: 2px; bottom: 0; font-size: 20px}
		table.table.resultados tbody tr td span {position: absolute; left: 2px; top: 0; font-size: 25px; border-radius: 50%; border: 2px solid white; color: #004A91; background-color: white; cursor: pointer;}
		h3.rotate {transform: rotate(-90deg); float: right; position: relative; right: -64px; top: 117px}
	</style>

	<div class="row mt-3">
		<div class="col-md-2 text-right">
		@include('/9box/partials/sub-menu')
		</div>
		<div class="col-md-10">
			<img class="img-fluid w-100" src="/img/9box/edr.png" alt="">
		</div>
		<div class="col-md-12 text-center">
			<h3 class="mt-3 titulos-evaluaciones" style="margin-bottom: 20px">Plan de Acción</h3>
			<hr style="border-top: 3px solid #273778;">
		</div>
	</div>

	<div class="row mt-3 text-center">
		<div class="col-md-12">
			<form id="changePlanForm" action="/plan-accion" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_empleado" value="{{$user->id}}">
				Periodo
				<select class="form-group period_id" name="period_id">
					@foreach ($periods as $key => $period)
						<option value="{{$period->id}}" <?php if ($period->id == $period_id){ ?>selected="selected"<?php } ?>>{{$period->name}}</option>
					@endforeach
				</select>
				<input type="submit" style="display: none">
			</form>
		</div>
	</div>

	<div class="row mt-3">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<table class="w-100">
				<tr>
					<td><b>Usuario:</b></td>
					<td>
						<input type="text" value="{{$user->first_name}} {{$user->last_name}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
					</td>
				</tr>
				<tr>
					<td><b>Puesto:</b></td>
					<td>
						<input type="text" value="{{(!empty($user->employee) ? $user->employee->getPuestoName() : '')}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
					</td>
				</tr>
				<tr>
					<td><b>Área:</b></td>
					<td>
						<input type="text" value="{{(!empty($user->division) ? $user->division: '')}}" readonly style="border-radius: 5px; font-size: 15px; padding: 1px 5px; border: 1px solid #A5A7A8; background-color: #E6E7E8; margin-bottom: 4px; width: 100%">
					</td>
				</tr>
			</table>
		</div>
	</div>

	<?php
		$actual_objetivo = 0;
		$periodo_anual = 0;
		$periodo = 0;
		$resultado_anual = 0;
		$porcentaje = 0;
		$peso = 0;
		$total_porcentaje = 0;
		$total_peso = 0;
		$total_desempeno = 0;
		$total_objetivos = 0;
		$desempeno_original = 0;
		$meses = array('0', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');

		if (!empty($desempeno)){
			$desempeno_original = $desempeno;
			$total_desempeno = $desempeno * 100 / 4;
			$desempeno = number_format($total_desempeno, 1);
			if ($desempeno > $total_desempeno){
				$total_desempeno = $desempeno;
			}
		}
	?>

	@for ($i = 0;$i < count($objetivos);$i++)
		@if ($objetivos[$i]->id != $actual_objetivo)
			@if ($actual_objetivo != 0)
				@if ((!empty($resultado_anual) || $resultado_anual == 0) && $periodo == $periodo_anual)
					<?php
						$porcentaje_peso = $porcentaje * $peso / 100;
						$total_peso += $porcentaje_peso; 
						$total_porcentaje += $porcentaje;
					?>
				@endif
			@endif
			<?php
				$actual_objetivo = $objetivos[$i]->id;
				$resultado_anual = 0;
				$porcentaje = 0;
			?>
			@if (!empty($objetivos[$i]->revisado))
				<?php
					$resultado_anual = $objetivos[$i]->logro;
					$porcentaje = 0;
					$periodo = $objetivos[$i]->periodo_logro;
				?>
				@if ($objetivos[$i]->valor_verde == 'Si' || $objetivos[$i]->valor_verde == 'No')
					@if ($resultado_anual == $objetivos[$i]->valor_verde)
						<?php $porcentaje = 100; ?>
					@endif
				@else
					@if ($objetivos[$i]->valor_verde >= $objetivos[$i]->valor_rojo)
						@if ($resultado_anual >= $objetivos[$i]->valor_verde)
							<?php $porcentaje = 100; ?>
						@else
							<?php $porcentaje = $resultado_anual * 100 / $objetivos[$i]->objetivo; ?>
							@if ($porcentaje > 100)
								<?php $porcentaje = 100; ?>
							@endif
						@endif
					@else
						@if ($resultado_anual <= $objetivos[$i]->valor_verde)
							<?php $porcentaje = 100; ?>
						@endif
					@endif
				@endif
			@endif

			<?php
				$frecuencia_captura = $objetivos[$i]->frecuencia;
				$periodo_anual = ($frecuencia_captura == 'Bimestral' ? 7 : ($frecuencia_captura == 'Trimestral' ? 5 : ($frecuencia_captura == 'Anual' ? 2 : 13)));
				$peso = $objetivos[$i]->peso;
			?>
		@endif
	@endfor

	@if ($actual_objetivo != 0)
		@if ((!empty($resultado_anual) || $resultado_anual == 0) && $periodo == $periodo_anual)
			<?php
				$porcentaje_peso = $porcentaje * $peso / 100;
				$total_peso += $porcentaje_peso;
				$total_porcentaje += $porcentaje;
			?>
		@endif
	@endif

	@if (!empty($objetivos))
		@if ($total_porcentaje != 0)
			<?php
				$total_objetivos = $total_peso;
				$total_peso = number_format($total_peso, 1);
				if ($total_peso > $total_objetivos) {
					$total_objetivos = $total_peso;
				}
			?>
		@endif
	@endif

	<div class="row mt-3 d-none">
		<div class="col-md-12">
			<div class="row">
				<div class="offset-3 col-md-1">
					<b>Desempeño:</b>
				</div>
				<div class="col-md-1">
					{{number_format($total_objetivos, 2)}}%
				</div>
				<div class="col-md-1">
					<b>Potencial:</b>
				</div>
				<div class="col-md-1">
					{{number_format($total_desempeno, 2)}}%
				</div>
			</div>
		</div>
	</div>

	<div class="row margin-top-40 d-none">
		<div class="col-md-offset-1 col-md-1 text-center">
			<h3 class="rotate">POTENCIAL</h3>
		</div>
		<div class="col-md-9">
			<table class="table resultados" width="100%">
				<tr class="border-left alto">
					<td class="texto-small" style="vertical-align: top">Alto</td>
					<td <?php if ($total_desempeno >= 90 && $total_desempeno <= 100 && $total_objetivos < 75) { ?>class="amarillo border"><span style="font-size:12px" class="glyphicon glyphicon-plus-sign" id="6" data-toggle="modal" data-target="#description"></span><?php } else{ ?>class="amarillo border"><?php } ?>DIAMANTE EN BRUTO<b>6</b></td>
					<td <?php if ($total_desempeno >= 90 && $total_desempeno <= 100 && $total_objetivos >= 75 && $total_objetivos < 90){ ?>class="verde-claro border"><span style="font-size:12px" class="glyphicon glyphicon-plus-sign" id="3" data-toggle="modal" data-target="#description"></span><?php } else{ ?>class="verde-claro border"><?php } ?>ESTRELLA POTENCIAL<b>3</b></td>
					<td <?php if ($total_desempeno >= 90 && $total_desempeno <= 100 && $total_objetivos >= 90 && $total_objetivos <= 100){ ?>class="verde border"><span style="font-size:12px" class="glyphicon glyphicon-plus-sign" id="1" data-toggle="modal" data-target="#description"></span><?php } else{ ?>class="verde border"><?php } ?>SUPER ESTRELLA<b>1</b></td>
				</tr>
				<tr class="border-left alto">
					<td></td>
					<td <?php if ($total_desempeno >= 75 && $total_desempeno < 90 && $total_objetivos < 75){ ?>class="rojo-claro border"><span style="font-size:12px" class="glyphicon glyphicon-plus-sign" id="8" data-toggle="modal" data-target="#description"></span><?php } else{ ?>class="rojo-claro border"><?php } ?>INCONSISTENTE<b>8</b></td>
					<td <?php if ($total_desempeno >= 75 && $total_desempeno < 90 && $total_objetivos >= 75 && $total_objetivos < 90){ ?>class="amarillo border"><span style="font-size:12px" class="glyphicon glyphicon-plus-sign" id="5" data-toggle="modal" data-target="#description"></span><?php } else{ ?>class="amarillo border"><?php } ?>CONTRIBUIDOR<br/> CONSISTENTE/JUGADOR CLAVE<b>5</b></td>
					<td <?php if ($total_desempeno >= 75 && $total_desempeno < 90 && $total_objetivos >= 90 && $total_objetivos <= 100){ ?>class="verde-claro border"><span style="font-size:12px" class="glyphicon glyphicon-plus-sign" id="2" data-toggle="modal" data-target="#description"></span><?php } else{ ?>class="verde-claro border"><?php } ?>FUTURA ESTRELLA<b>2</b></td>
				</tr>
				<tr class="border-left alto">
					<td class="texto-small" style="vertical-align: bottom">Bajo</td>
					<td <?php if ($total_desempeno < 75 && $total_objetivos < 75){ ?>class="rojo border"><span style="font-size:12px" class="glyphicon glyphicon-plus-sign" id="9" data-toggle="modal" data-target="#description"></span><?php } else{ ?>class="rojo border"><?php } ?>ESTRATEGIA DE DESVINCULACIÓN<b>9</b></td>
					<td <?php if ($total_desempeno < 75 && $total_objetivos >= 75 && $total_objetivos < 90){ ?>class="rojo-claro border"><span style="font-size:12px" class="glyphicon glyphicon-plus-sign" id="7" data-toggle="modal" data-target="#description"></span><?php } else{ ?>class="rojo-claro border"><?php } ?>TIBIO/CONTRIBUIDO MARGINAL<b>7</b></td>
					<td <?php if ($total_desempeno < 75 && $total_objetivos >= 90 && $total_objetivos <= 100){ ?>class="amarillo border"><span style="font-size:12px;" class="glyphicon glyphicon-plus-sign" id="4" data-toggle="modal" data-target="#description"></span><?php } else{ ?>class="amarillo border"><?php } ?>PROFESIONAL CON EXPERIENCIA<b>4</b></td>
				</tr>
				<tr>
					<td></td>
					<td class="texto-small border-bottom" style="vertical-align: top; text-align: left;">Bajo</td>
					<td class="border-bottom"></td>
					<td class="texto-small border-bottom" style="vertical-align: top; text-align: right;">Alto</td>
				</tr>
			</table>
			<div class="text-center">
				<h3>DESEMPEÑO</h3>
			</div>
		</div>
	</div>

	@if (!empty($plan_status))
		<div class="row mt-3">
			<div class="col-md-12 text-center">
				@if ($plan_status == 'Aprobado')
					<button class="btn btn-success" style="cursor: default">Aprobado</button>
				@else
					@if ($aprobador)
						<form action="/aprobar-plan" method="post">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="user_id" value="{{$user->id}}">
							<input type="hidden" name="period_id" value="{{$period_id}}">
							<button type="submit" class="btn btn-success">Aprobar Plan</button>
						</form>
					@else
						<button class="btn btn-warning" style="cursor: default">Esperando Aprobación</button>
					@endif
				@endif
			</div>
		</div>
	@else
		@if (count($planes_accion) > 0 && $canEdit)
			<div class="row mt-3">
				<div class="col-md-12 text-center">
					<form action="/solicitar-aprobacion" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="user_id" value="{{$user->id}}">
						<input type="hidden" name="period_id" value="{{$period_id}}">
						<button type="submit" class="btn btn-success">Solicitar Aprobación</button>
					</form>
				</div>
			</div>
		@endif
	@endif

	<div class="row mt-3" id="areaImprimir">
		<div class="col-md-5 d-none">
			<table class="table-responsive tabla_competencias" border="1" width="100%">
				<thead>
					<tr>
						<th>Competencias</th>
						<th class="text-center">Resultado</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($factors as $key => $value){ ?>
						<tr>
							<td>{{$value}}</td>
							<td class="text-center" style="color: white; background-color: {{($totales_factores[$key] >= 3.5 ? '#008000' : ($totales_factores[$key] >= 3 ? '#73CB77' : ($totales_factores[$key] >= 2.5 ? '#FFFF00; color: black' : ($totales_factores[$key] >= 2 ? '#FF4343' : '#C00000'))))}}">
								<b>{{number_format($totales_factores[$key], 2, '.', ',')}}</b>
							</td>
						</tr>
					<?php } ?>
						<tr>
							<td>RESULTADO GLOBAL</td>
							<td class="text-center" style="color: white; background-color: {{($desempeno_original >= 3.5 ? '#008000' : ($desempeno_original >= 3 ? '#73CB77' : ($desempeno_original >= 2.5 ? '#FFFF00; color: black' : ($desempeno_original >= 2 ? '#FF4343' : '#C00000'))))}}">
								<b>{{number_format($desempeno_original, 2, '.', ',')}}</b>
							</td>
						</tr>
				</tbody>
			</table>
		</div>
		
		<div class="col-md-12">
			<table class="table w-100">
				<thead>
					<tr>
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Clave</th>
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Descripción del Indicador</th>
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Peso</th>
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Meta</th>
						{{-- <th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Unidad de Medida</th> --}}
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Resultado anual</th>
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Resultado anual %</th>
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Resultado Anual % Peso</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$actual_objetivo = 0;
						$contador_periodos = 0;
						$periodo_anual = 0;
						$periodo = 0;
						$resultado_anual = 0;
						$porcentaje = 0;
						$peso = 0;
						$contador_objetivos = 0;
						$total_resultado_anual = 0;
						$total_porcentaje = 0;
						$total_peso = 0;
					?>

					@for ($i = 0;$i < count($objetivos);$i++)
						@if ($objetivos[$i]->id != $actual_objetivo)
							@if ($actual_objetivo != 0)
									@if ((!empty($resultado_anual) || $resultado_anual == 0) && $periodo == $periodo_anual)
										<td class="text-center" style="padding: 10px 5px">{{$resultado_anual}}</td>
										<td class="text-right" style="padding: 10px 5px; background-color: {{$color}}">
											<strong>{{number_format($porcentaje,1)}}</strong>
										</td>
										<?php
											$porcentaje_peso = $porcentaje * $peso / 100;
											$total_peso += $porcentaje_peso; 
											$total_resultado_anual += ($resultado_anual == 'Si' || $resultado_anual == 'No' ? 100 : $resultado_anual);
											$total_porcentaje += $porcentaje;
										?>
										<td class="text-center" style="padding: 10px 5px">
											<strong>{{number_format($porcentaje_peso,1)}}</strong>
										</td>
									@else
										<td></td>
										<td></td>
										<td></td>
									@endif
								</tr>
							@endif
							<?php
								$current_type = 0;
								$counter = 0;
								$clave = '';
								foreach ($objetivos_corporativos as $key => $objetivo_corporativo){			
									if ($current_type != $objetivo_corporativo->id_tipo){
										$counter = 1;
										$current_type = $objetivo_corporativo->id_tipo;
									}
									if ($objetivos[$i]->id_objetivo_corporativo == $objetivo_corporativo->id){
										$clave = $objetivo_corporativo->letter . $counter;
										break;
									}
									$counter++;
								}
							?>

							<tr class="filas">
							<td style="padding: 10px 5px" class="text-center">{{$clave}}</td>
							<td style="padding: 10px 5px" class="text-left">{{$objetivos[$i]->nombre}}</td>
							<td style="padding: 10px 5px" class="text-center">{{$objetivos[$i]->peso}}</td>
							<td style="padding: 10px 5px" class="text-center">{{$objetivos[$i]->objetivo}}</td>
							{{-- <td style="padding: 10px 5px" class="text-center">{{($objetivos[$i]->tipo == 'Si/No' ? 'Si / No' : ($objetivos[$i]->tipo == '#' ? '$' : ($objetivos[$i]->tipo == '9' ? 'Numérico' : '%')))}}</td> --}}

							<?php
								$actual_objetivo = $objetivos[$i]->id;
								$resultado_anual = 0;
								$porcentaje = 0;
								$contador_objetivos++;
							?>
							@if (!empty($objetivos[$i]->revisado))
								<?php
									$resultado_anual = $objetivos[$i]->logro;
									$porcentaje = 0;
									$color = '#d9534f';
									$periodo = $objetivos[$i]->periodo_logro;
								?>
								@if ($objetivos[$i]->valor_verde == 'Si' || $objetivos[$i]->valor_verde == 'No')
									@if ($resultado_anual == $objetivos[$i]->valor_verde)
										<?php
											$porcentaje = 100;
											$color = '#5cb85c';
										?>
									@endif
								@else
									@if ($objetivos[$i]->valor_verde >= $objetivos[$i]->valor_rojo)
										@if ($resultado_anual >= $objetivos[$i]->valor_verde)
											<?php
												$porcentaje = 100;
												$color = '#5cb85c';
											?>
										@else
											@if ($resultado_anual <= $objetivos[$i]->valor_rojo)
												<?php $color = '#d9534f'; ?>
											@else
												<?php $color = '#d58512'; ?>
											@endif
											<?php $porcentaje = $resultado_anual * 100 / $objetivos[$i]->objetivo; ?>
											@if ($porcentaje > 100)
												<?php
													$porcentaje = 100;
													$color = '#5cb85c';
												?>
											@endif
										@endif
									@else
										@if ($resultado_anual <= $objetivos[$i]->valor_verde)
											<?php
												$porcentaje = 100;
												$color = '#5cb85c';
											?>
										@else
											<?php $color = '#d9534f'; ?>
										@endif
									@endif
								@endif
							@endif
							<?php
								$contador_periodos = 0;
								$frecuencia_captura = $objetivos[$i]->frecuencia;
								$periodo_anual = ($frecuencia_captura == 'Bimestral' ? 7 : ($frecuencia_captura == 'Trimestral' ? 5 : ($frecuencia_captura == 'Anual' ? 2 : 13)));
								$peso = $objetivos[$i]->peso;
							?>
						@endif
						<?php $contador_periodos++; ?>
					@endfor

					@if ($actual_objetivo != 0)
							@if ((!empty($resultado_anual) || $resultado_anual == 0) && $periodo == $periodo_anual)
								<td class="text-center" style="padding: 10px 5px">{{$resultado_anual}}</td>
								<td class="text-right" style="padding: 10px 5px; background-color: {{$color}}">
									<strong>{{number_format($porcentaje,1)}}</strong>
								</td>
								<?php
									$porcentaje_peso = $porcentaje * $peso / 100;
									$total_peso += $porcentaje_peso;
									$total_resultado_anual += ($resultado_anual == 'Si' || $resultado_anual == 'No' ? 100 : $resultado_anual);
									$total_porcentaje += $porcentaje;
								?>
								<td class="text-center" style="padding: 10px 5px">
									<strong>{{number_format($porcentaje_peso,1)}}</strong>
								</td>
							@else
								<td></td>
								<td></td>
								<td></td>
							@endif
						</tr>
					@endif

					@if (!empty($objetivos))
						<tr>
							<td></td>
							<td class="text-right">Total:</td>
							<td class="text-center">
								<strong>100</strong>
							</td>
							<td></td>
							<td></td>
							<td class="text-center"></td>
							<td class="text-right">
								<!--<strong>{{($total_porcentaje != 0 ? number_format($total_porcentaje / $contador_objetivos, 1) : '')}}</strong>-->
								<strong>{{($total_porcentaje != 0 ? number_format($total_peso, 1) : '')}}</strong>
							</td>
							<td class="text-center">
								<strong>{{($total_porcentaje != 0 ? number_format($total_peso, 1) : '')}}</strong>
							</td>
						</tr>
					@endif
				</tbody>
			</table>
		</div>
	</div>

	@if ($canEdit)
		<div class="row mt-3 d-none">
			<div class="col-md-12 text-center">
				<a href="/plan-accion/create/{{$period_id}}" class="btn btn-primary">+ Agregar Medida o Acción</a>
			</div>
		</div>
	@endif

	<div class="row mt-3 d-none">
		<div class="col-md-12">
			<table width="100%" class="table table-striped">
				<thead class="cabeceras-tablas-evaluaciones">
					<tr>
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Acción</th>
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Descripción</th>
						<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Estatus</th>
						@if ($canEdit && (empty($plan_status) || $plan_status != 'Aprobado'))
							<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Editar</th>
							<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Eliminar</th>
						@else
							<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Ver</th>
						@endif
					</tr>
				</thead>
				<tbody>
					@for ($i = 0; $i < count($planes_accion); $i++)
						<tr>
							<td class="text-center" style="padding: 10px 5px">{{$planes_accion[$i]->accion}}</td>
							<td class="text-center" style="padding: 10px 5px">{{$planes_accion[$i]->descripcion}}</td>
							<td class="text-center" style="padding: 10px 5px">{{$planes_accion[$i]->status}}</td>
							<td class="text-center" style="padding: 10px 5px">
								<a href="/plan-accion/{{$planes_accion[$i]->id}}" class="btn btn-primary">{{($canEdit ? 'Editar' : 'Ver')}}</a>
							</td>
							@if ($canEdit && (empty($plan_status) || $plan_status != 'Aprobado'))
								<td class="text-center" style="padding: 10px 5px">
									<a class="btn btn-danger" href="/borrar-plan-accion/{{$planes_accion[$i]->id}}" onclick="return confirm('¿Esta seguro de querer eliminar esta acción?')">Eliminar</a>
								</td>
							@endif
						</tr>
					@endfor
				</tbody>
			</table>
		</div>
	</div>

	<div class="row mt-3 d-none">
		<div class="col-md-12">
			<div style="max-height: 200px; overflow-y: auto">
				<h3 style="margin-top: 0">Comentarios</h3>
				<table width="100%" style="font-size: 13px" border="1" class="notas">
					<thead>
						<tr>
							<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Fecha</th>
							<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Usuario</th>
							<th class="text-center cabeceras-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Comentario</th>
						</tr>
					</thead>
					<tbody>

			@for ($i = 0;$i < count($comentarios); $i++) 

						<tr>
							<td class="text-center" style="padding: 5px; border: 0">{{substr($comentarios[$i]->created_at, 8, 2) * 1}}/{{$meses[substr($comentarios[$i]->created_at, 5, 2) * 1]}}/{{substr($comentarios[$i]->created_at, 0, 4)}}</td>
							<td class="text-center" style="padding: 5px; border: 0">{{$users[$comentarios[$i]->created_by]}}</td>
							<td class="text-center" style="padding: 5px; border: 0">{{$comentarios[$i]->comentario}}</td>
						</tr>
					@endfor

					</tbody>
				</table>
			</div>
			<div class="mt-3">
				<textarea style="width: 100%; height: 100px; border-radius: 7px" placeholder="Comentario"></textarea>
			</div>
			<div class="text-right margin-top-10">
				<button class="btn btn-primary agregar_nota">Agregar comentario</button>
			</div>
		</div>
	</div>

	<div class="row mt-3 d-none">
		<div class="col-md-2"></div>
			<div class="col-md-8 text-center">
				<b>Estas acciones son parte del plan de preparación para obtener la oportunidad de ser partícipe en el proceso de selección de una vacante interna. Esto no garantiza la obtención del puesto, ya que deben completarse demás requisitos.</b>
			</div>
		<div class="col-md-2"></div>
	</div>

	<div class="modal" tabindex="-1" role="dialog" id="description">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-body">
					<table class="table">
						<thead>
							<tr>
								<th>Título</th>
								<th>Descripción</th>
								<th>Recomendación</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="titulo"></td>
								<td class="descripcion"></td>
								<td class="recomendacion"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script>

	var period_id = <?php echo (!empty($period_id) ? $period_id : 0)?>;
	var user_id = <?php echo $user->id?>;
	var textos_resultados = <?php echo json_encode($textos_resultados)?>;
		
		$(document).ready(function(){

			$.ajaxSetup({
			headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
			});

			$('select.period_id').change(function(){

				$('form#changePlanForm').submit();
			});

	$('button.agregar_nota').click(function(){

				var nota = $('textarea').val();

				$.ajax({
        	type:'POST',    
        	url: '/agregar-nota-box',
          data:{
          	mensaje: nota,
          	period_id: period_id,
          	id_empleado: user_id,
						'_token': '{{ csrf_token() }}'
		},
		success: function(data){
			
			var meses = [];
			meses[0] = 'Ene';
			meses[1] = 'Feb';
			meses[2] = 'Mar';
			meses[3] = 'Abr';
			meses[4] = 'May';
			meses[5] = 'Jun';
			meses[6] = 'Jul';
			meses[7] = 'Ago';
			meses[8] = 'Sep';
			meses[9] = 'Oct';
			meses[10] = 'Nov';
			meses[11] = 'Dic';
			var fecha = new Date();
			$('table.notas').prepend('<tr><td class="text-center" style="padding: 5px; border: 0">' + fecha.getDate() + '/' + meses[fecha.getMonth()] + '/' + fecha.getFullYear() + '</td><td class="text-center" style="padding: 5px; border: 0">' + data + '</td><td class="text-center" style="padding: 5px; border: 0">' + nota + '</td></tr>');
			$('textarea').val('');
		}
		});
			});

			$('.glyphicon-plus-sign').click(function(){

				var id_textos_resultados = $(this).attr('id') * 1;

				for (var i = 0;i < textos_resultados.length;i++){

					if (textos_resultados[i].id == id_textos_resultados){

						$('div#description table tbody tr td.titulo').text(textos_resultados[i].titulo);
						$('div#description table tbody tr td.descripcion').text(textos_resultados[i].descripcion);
						$('div#description table tbody tr td.recomendacion').text(textos_resultados[i].recomendacion);
					}
				}
			});
		});
	</script>
@endsection
