@extends('layouts.app')

@section('title', '9Box Avance de Colaboradores')

@section('content')
<style>div.dt-buttons {
	float: right;
	margin-left:10px;
	display: none;
	}</style>
<div class="row mt-1">
	<div class="col-md-2">
		@include('9box/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid w-100" src="/img/9box/edr.png" alt="">
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">AVANCE DE COLABORADORES
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
 
  
		@if (!empty($periods))

		

		<div class="row"> 
			<div class="col-12 offset-md-4 col-md-3 text-center">
				<div class="form-group">
					<form id="changePeriodoForm" action="/avance-colaboradores" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<label for="id_plan" class="font-weight-bolder mb-0">Periodo:</label>							
					<select class="form-control id_periodo" name="period_id">
						@foreach ($periods as $key => $value)
						<option value="{{$value->id}}" <?php if ($value->id == $period->id){ ?>selected="selected"<?php } ?>>{{$value->name}}</option>
					@endforeach
					</select>
				<input type="submit" style="display: none">
			</form>
				</div>
			</div> 
		</div>
		 
		@endif 

@if (!empty($periods))
	@if (count($users) > 0)
		@if (substr(auth()->user()->subdivision, 0, 8) == 'DIRECTOR' || auth()->user()->role == 'admin' || auth()->user()->hasRolePermission('9Box'))
			<div class="row my-2">
				<div class="col-md-12 text-left">
					{{-- <form action="/reporte-colaboradores" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="period_id" value="{{$period->id}}">
						<button type="submit" class="btn btn-primary mx-3">Reporte General</button>
					</form> --}}
					<form action="/listado-colaboradores" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="period_id" value="{{$period->id}}">
						<button type="submit" class="btn btn-primary mx-3">Listado Colaboradores</button>
					</form>
				</div>
			</div>
		@endif
			
		

		<div class="card border-0">
			<h5 class="card-header bg-primary text-white font-weight-bolder">Avances
				<button class="btn btn-success float-right text-left mr-2 btn-excel" title="Exportar a Excel">
					<i class="fa fa-file-excel"></i> Exportar a Excel
				</button>
			</h5>
			<div class="card-body px-0"> 

				<div class="row mt-2">
					<div class="col-md-12">
						<table class="table w-100" id="tablaCol">
							<thead>
								<tr>
									<th class="cabeceras-evaluaciones">ID</th>
									<th class="cabeceras-evaluaciones">Colaborador</th>
									<th class="cabeceras-evaluaciones">Puesto</th>
									<th class="cabeceras-evaluaciones">Departamento</th>
									<th class="detalle cabeceras-evaluaciones">Acciones</th>
								</tr>
							</thead>
							<tbody>
								@for ($i = 0;$i < count($users);$i++)
								<tr>
									<td class="text-left">{{isset($users[$i]->idempleado) ? $users[$i]->idempleado : $users[$i]->employee_id}}</td>
									<td class="text-left">{{$users[$i]->last_name}} {{$users[$i]->first_name}} </td>
									<td class="text-left">{{(!empty($users[$i]->job_position) ? $users[$i]->job_position : '')}}</td>
									<td class="text-left">{{(!empty($users[$i]->department) ? $users[$i]->department : '')}}</td>
									<td class="text-center detalle">
										<form method="post" action="/plan-accion-box" style="display: inline-block">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<input type="hidden" name="id_user" value="{{$users[$i]->id}}">
											<input type="hidden" name="id_periodo_9b" value="{{$period->id}}">
											@if (in_array($users[$i]->id, $planes_autorizados))
												<button class="btn btn-success" type="submit">Plan de Acción</button>
											@else
												@if (in_array($users[$i]->id, $planes_en_espera))
													@if ($aprobador)
														<button class="btn btn-danger" type="submit">Plan de Acción</button>
													@else
														<button class="btn btn-warning" type="submit">Plan de Acción</button>
													@endif
												@else
													<button class="btn btn-primary" type="submit">Plan de Acción</button>
												@endif
											@endif
										</form>
									</td>
								</tr>
								@endfor
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	@else
		<div class="row">
			<div class="col-md-12 text-center">
				<h4 class="mt-1 titulos-evaluaciones">Colaboradores con planes de accion no disponibles </h4>
			</div>
		</div>
	@endif
@else
	<div class="row">
		<div class="col-md-12 text-center">
			<h4 class="mt-1 titulos-evaluaciones">No hay información del usuario a mostrar</h4>
		</div>
	</div>
@endif
			</div>
			</div>
			</div>
			</div>
@endsection

@section('scripts')
	<script>
		
		$('.btn-excel').click(function(){ 
			$('.buttons-excel').trigger('click');
		});

		var period_id = <?php echo (!empty($period) ? $period->id : 0)?>	
		$(document).ready(function(){
			$('select.id_periodo').change(function(){
				$('form#changePeriodoForm').submit();
			});
			$('input.autorize_plan_accion').click(function(){
		        var user_id = $(this).val();
    			var checked = 0;
	    		if ($(this).prop('checked') == true){
	    			checked = 1;
    			}

				$.ajax({
					type:'POST',    
					url: '/9box/autorize-plan-accion',
					data:{
					user_id: user_id,
					period_id: period_id,
					checked: checked
					}
					/*success: function(data){

					alert('El cambio fue guardado correctamente');
					}*/
				});
    		});

			$.extend( jQuery.fn.dataTableExt.oSort, {
				"locale-compare-asc": function ( a, b ) {
					return a.localeCompare(b, 'es', { sensitivity: 'accent' })
				},
				"locale-compare-desc": function ( a, b ) {
					return b.localeCompare(a, 'es', { sensitivity: 'accent' })
				}
			});
			
			$('#tablaCol').DataTable({
					dom:"<'row'<'col-md-6'l><'col-md-6'Bf>>" +
"<'row'<'col-md-6'><'col-md-6'>>" +
"<'row'<'col-md-12't>><'row'<'col-md-12'ip>>",
   				order: [[1,'asc']],
   				language: {
		 			'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
      			},
				columnDefs : [
     				{ targets: 1, type: 'locale-compare' }
				],
   				buttons: [
					{
        				extend: 'excel',
						text: 'Exportar a Excel',
						titleAttr: 'Exportar a Excel',
						title: 'Galber - Avance de los Colaboradores',
						exportOptions: {
							columns: ':not(.detalle)'
						}
					}
        		],
			});
		});
	</script>
@endsection