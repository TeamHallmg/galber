@extends('layouts.app')

@section('title', 'Crear Periodo')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
	@include('9box/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-responsive" src="{{ asset('img/9box/banner 9 box maver.png') }}" alt="">
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Crear Periodo
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
 
 
		<form action="/desempeno-resultados/crear-periodo" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="row "> 
        <div class="col-md-6">

			<div class="form-group">
        <label for="name">Nombre</label>
        <input type="text" class="form-control" name="name" required>
      </div>
      </div>
      <div class="col-md-6">
  		<div class="form-group">
        <label for="start_date">Fecha de Inicio</label>
        <input type="date" class="form-control" name="start_date" required>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="end_date">Fecha de Cierre</label>
        <input type="date" class="form-control" name="end_date" required>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="plan_id">Evaluación Cuantitativa</label>
        <select class="form-control" name="plan_id">

  @foreach ($plans as $key => $value)
      
          <option value="{{$value->id}}">{{$value->name}}</option>
  @endforeach

        </select>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="period_id">Evaluación Cualitativa</label>
        <select class="form-control" name="period_id">

  @foreach ($periods as $key => $value)
      
          <option value="{{$value->id}}">{{$value->descripcion}}</option>
  @endforeach

        </select>
      </div>
    </div> 

  @if (!empty($users))
 
<div class="col-md-6">
      <div class="form-group elements_container">
        <label for="users">Aprobadores de los Planes de Acción</label>
        <input type="text" class="form-control users_autocomplete" name="users" placeholder="Escribe el nombre del aprobador">
        <button type="button" class="btn btn-primary add_user" style="display: none">Agregar</button>
      </div>
      </div>
  @endif
 
<div class="col-md-6">
      <div class="form-group">
        <label for="status">Status</label>
        <select class="form-control" name="status">
			    <option value="Preparatorio">Preparatorio</option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
			<div class="form-group">
        <button class="btn btn-primary" type="submit" style="color: white">Crear</button> 
        <a class="btn btn-danger" href="/desempeno-resultados/periodos">Regresar</a>
        </div>
      </div>
      </div>
    </form>
	</div>
 </div>
</div>
</div> 
@endsection

@section('scripts')
  <script>

    var users = <?php echo json_encode($users)?>;
    var user_name = '';
    var user_id = 0;
    
    $(document).ready(function(){

      // Autocomplete 
      $('.users_autocomplete').autocomplete({
      
        lookup: users,
        onSelect: function (suggestion){

          user_name = suggestion.value;
          user_id = suggestion.data;
          $('button.add_user').show();
        }
      });

      // Agrega un nuevo aprobador
      $('button.add_user').click(function(){

        $('.users_autocomplete').val('');
        $(this).parent().append('<div class="row"><div class="col-md-6 text-left"><input type="hidden" name="users[]" value="' + user_id + '">' + user_name + '</div><div class="col-md-6"><a href="javascript:;" class="remove_element">X</a></div></div>');
        $(this).hide();
      });

      // Quita un aprobador
      $('body').on('click', 'a.remove_element', function(){

        $(this).parent().parent().remove();
      });
    });
  </script>
@endsection
