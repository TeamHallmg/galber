@extends('layouts.app')

@section('title', 'Periodos')

@section('content')
<style>div.dt-buttons {
	float: right;
	margin-left:10px;
	display: none;
	}</style>
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('9box/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid w-100" src="/img/9box/edr.png" alt="">
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Periodos
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
 
 
<div class="row margin-top-20 mb-2">
	<div class="col-md-12 text-center">
		<a href="/desempeno-resultados/periodos/create" class="btn btn-primary">+ Agregar Periodo</a>
	</div>
</div>


<div class="card border-0">
	<h5 class="card-header bg-primary text-white font-weight-bolder">Detalles
		<button class="btn btn-success float-right text-left mr-2 btn-excel" title="Exportar a Excel">
			<i class="fa fa-file-excel"></i> Exportar a Excel
		</button>
	</h5>
	<div class="card-body px-0"> 

<div class="row margin-top-20">
	<div class="col-md-12">
		<table width="100%" style="font-size: 13px" class="table table-striped periodos">
			<thead class="cabeceras-tablas-evaluaciones">
				<tr>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Status</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Nombre</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Fecha de Inicio</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Fecha de Cierre</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Evaluación Cuantitativa</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Evaluación Cualitativa</th>
					<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Acciones</th>
				</tr>
			</thead>
			<tbody>
			@for ($i = 0; $i < count($periodos); $i++)

				<tr>
					<td class="text-center" style="padding: 10px 5px">
						<span style="padding: 5px; background-color: <?php echo ($periodos[$i]->status != 'Abierto' ? '#4B4B4D' : '#0090DA')?>; color: white">{{$periodos[$i]->status}}</span>
					</td>
					<td class="text-center" style="padding: 10px 5px">{{$periodos[$i]->name}}</td>
					<td class="text-center" style="padding: 10px 5px">{{$periodos[$i]->start_date}}</td>
					<td class="text-center" style="padding: 10px 5px">{{$periodos[$i]->end_date}}</td>
					<td class="text-center" style="padding: 10px 5px">{{$plans[$periodos[$i]->plan_id]}}</td>
					<td class="text-center" style="padding: 10px 5px">{{$periods[$periodos[$i]->period_id]}}</td>
					<td class="text-center" style="padding: 10px 5px">
						<a href="/desempeno-resultados/periodos/{{$periodos[$i]->id}}/edit" class="btn btn-primary">Editar</a> <?php if ($periodos[$i]->status == 'Cancelado'){ ?><a href="/desempeno-resultados/borrar-periodo/{{$periodos[$i]->id}}" class="btn btn-danger" onclick="return confirm('¿Esta seguro de querer eliminar este periodo?')">Eliminar</a><?php } ?>
					</td>
				</tr>
			@endfor
			</tbody>
		</table>
	</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$('.btn-excel').click(function(){ 
			$('.buttons-excel').trigger('click');
		});
		$(document).ready(function(){
			$('.periodos').DataTable({
					dom:"<'row'<'col-md-6'l><'col-md-6'Bf>>" +
"<'row'<'col-md-6'><'col-md-6'>>" +
"<'row'<'col-md-12't>><'row'<'col-md-12'ip>>",
				language: {
		 			'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
      			},
				buttons: [
					{
						extend: 'excel',
						text: '<span class="glyphicon glyphicon-file"></span> Exportar a excel',
						title: 'Periodos',
						// className: 'btn btn-strong_green',
					},
				],
			});
		});
	</script>
@endsection
