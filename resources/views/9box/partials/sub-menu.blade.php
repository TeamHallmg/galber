<div class="sub-menu">
	<?php function activeSubMenu($url){
		return request()->is($url) ? 'active' : '';
	} ?>
	<h4>Evaluación de Desempeño y Resultados</h4>
	<ul class="nav nav-pills flex-column">
		{{-- <li class="nav-item">
			<a class="nav-link {{ activeSubMenu('/que-es') }}" href="{{ url('/que-es') }}">¿Qué es?</a>
		</li> --}}

		@if (auth()->user()->role != 'admin' && !Auth::user()->isAdminOrHasRolePermission('9Box'))
			<li class="nav-item">
				<a class="nav-link {{ activeSubMenu('/avance-colaboradores') }}" href="{{ url('/avance-colaboradores') }}">Avance de Colaboradores</a>
			</li>
		@else
			{{-- <li class="nav-item"><h4 class="font-weight-bold mt-4">Administrador</h4><hr></li> --}}
			<li class="nav-item">
				<a class="nav-link {{ activeSubMenu('/avance-colaboradores') }}" href="{{ url('/avance-colaboradores') }}">Avance</a>
			</li>
			<li class="nav-item">
				<a class="nav-link {{ activeSubMenu('/periodos') }}" href="{{ url('/desempeno-resultados/periodos') }}">Periodos</a>
			</li>
			{{--<li>
				<a href="{{ url('/ponderaciones') }}">Ponderaciones</a>
			</li>--}}
		@endif
	</ul>
</div>

@if (auth()->user()->isSuperAdmin())

  <span class="font-weight-bold font-sepanka font-version-sepanka h4">20250130.YARM</span>
@endif