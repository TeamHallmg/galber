@extends('layouts.app')

@section('title', 'Listado Colaboradores')

@section('content')
<style>div.dt-buttons {
	float: right;
	margin-left:10px;
	display: none;
	}</style>
<?php function numberFormatPrecision($number, $precision = 2, $separator = '.'){

    $number = $number . '';
    $numberParts = explode($separator, $number);
    $response = $numberParts[0];

    if (count($numberParts) > 1 && $precision > 0){

      $response .= $separator;
      $response .= substr($numberParts[1], 0, $precision);
    }

    return $response;
  } ?>
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
	@include('9box/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid w-100" src="/img/9box/edr.png" alt="">
		
	</div>
</div>
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Listado de Colaboradores
<a href="{{ url()->previous() }}" class="btn btn-sm float-right btn-success">Regresar</a>
				 
</h5>
			<div class="card-body">
 


 
				<div class="row"> 
					<div class="col-12 offset-md-4 col-md-3 text-center">
						<div class="form-group">
							<form id="changePlanForm" action="/9box/reporte-colaboradores" method="post">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="listado" value="true">
							<label for="id_plan" class="font-weight-bolder mb-0">Periodo:</label>							
							<select class="form-control period_id" name="period_id">
								@foreach ($periodos as $key => $periodo)
									<option value="{{$periodo->id}}" <?php if ($periodo->id == $id_periodo_9b){ ?>selected="selected"<?php } ?>>{{$periodo->name}}</option>
								@endforeach
							</select>
						<input type="submit" style="display: none">
					</form>
						</div>
					</div> 
				</div>
 
				<div class="card border-0">
					<h5 class="card-header bg-primary text-white font-weight-bolder">Detalles
						<button class="btn btn-success float-right text-left mr-2 btn-excel" title="Exportar a Excel">
							<i class="fa fa-file-excel"></i> Exportar a Excel
						</button>
					</h5>
					<div class="card-body px-0"> 

<div class="row mt-3">
	<div class="col-md-12 col-md-offset-1">
		<table class="table listado">
			<thead class="bg-primary">
				<th class="text-center">Nombre</th>
				<th class="text-center">Puesto</th>
				<th class="text-center">Dirección</th>
				<th class="text-center">SubÁrea</th>
				<th class="text-center">Cuantitativa</th>
				<th class="text-center">Cualitativa</th>
				<th class="text-center">Total</th>
			</thead>
			<tbody>
				@foreach ($resultados as $resultado)
					@if ($resultado['cuantitativa'] != 0 || $resultado['cualitativa'] != null)
						<tr>	
							<td>{{ $resultado['nombre'] }}</td>
							<td>{{ $resultado['puesto'] }}</td>
							<td>{{ $resultado['direccion'] }}</td>
							<td>{{ $resultado['sub-area'] }}</td>
							<td class="text-right">{{ $resultado['cuantitativa']==0 ? '0.00' : numberFormatPrecision($resultado['cuantitativa']) }}%</td>			
							<td class="text-right">{{ is_null($resultado['cualitativa']) ? '0.00' : $resultado['cualitativa'] }}%</td>			
							<td class="text-right">{{ numberFormatPrecision($resultado['total']) }}%</td>	
						</tr>	
					@endif
				@endforeach
			</tbody>
		</table>
	</div>
</div>
</div>
</div>

<div class="row mt-3">
	<div class="col-md-12 text-right">
		<a href="{{ url('/avance-colaboradores') }}" role="button" class="btn btn-primary">Regresar</a>
	</div>
</div>
</div>
</div>
</div>
</div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function(){
			$('select.period_id').change(function(){
				$('form#changePlanForm').submit();
			});

			$('.btn-excel').click(function(){ 
			$('.buttons-excel').trigger('click');
		});

			$.extend( jQuery.fn.dataTableExt.oSort, {
				"locale-compare-asc": function ( a, b ) {
					return a.localeCompare(b, 'es', { sensitivity: 'accent' })
				},
				"locale-compare-desc": function ( a, b ) {
					return b.localeCompare(a, 'es', { sensitivity: 'accent' })
				}
			});

			$('.listado').DataTable({
					dom:"<'row'<'col-md-6'l><'col-md-6'Bf>>" +
"<'row'<'col-md-6'><'col-md-6'>>" +
"<'row'<'col-md-12't>><'row'<'col-md-12'ip>>",
				language: {
		 			'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
      			},
				columnDefs : [
     				{ targets: 0, type: 'locale-compare' }
				],
				buttons: [
					{
						extend: 'excel',
						customize: function( xlsx ) {
							var sSh = xlsx.xl['styles.xml'];
							//below could be replaced with use of .innerHtml but that doesn't work in IE
							var newPercentageFormat =sSh.childNodes[0].childNodes[0].childNodes[3].cloneNode(false);
							newPercentageFormat.setAttribute('formatCode','##0.00%');
							newPercentageFormat.setAttribute('numFmtId','180');
							sSh.childNodes[0].childNodes[0].appendChild(newPercentageFormat);
							
							$(sSh).find('numFmts').attr('count', '7');
							$(sSh).find('xf[numFmtId="9"]').attr('numFmtId', '180');
						}
						/* text: '<span class="glyphicon glyphicon-file"></span> Exportar a excel',
						title: 'Listado Colaboradores', */
						// className: 'btn btn-strong_green',
					},
				],
			});
		});
	</script>
@endsection
