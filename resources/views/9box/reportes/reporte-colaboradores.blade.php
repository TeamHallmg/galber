@extends('layouts.app')

@section('title', 'Reporte Colaboradores')

@section('content')
<style type="text/css">
	table.table.resultados tbody tr td {text-align: center; position: relative}
	table.table.resultados tbody tr td b {position: absolute; right: 2px; bottom: 0; font-size: 20px}
	h3.rotate {transform: rotate(-90deg); float: right; position: relative; right: -64px; top: 54px}
</style>
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
	@include('9box/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-responsive" src="{{ asset('img/9box/banner 9 box maver.png') }}" alt="">
	</div>
	<div class="col-md-12 text-center">
		<h3 class="margin-top-20 titulos-evaluaciones" style="margin-bottom: 20px">Reporte de Colaboradores</h3>
	</div>
</div>
<div class="row margin-top-20 text-center">
	<div class="col-md-12">
		<form id="changePlanForm" action="/9box/reporte-colaboradores" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
  		Periodo
  		<select class="form-group period_id" name="period_id">

  @foreach ($periods as $key => $period)
    
      	<option value="{{$period->id}}" <?php if ($period->id == $period_id){ ?>selected="selected"<?php } ?>>{{$period->name}}</option>
  @endforeach

    	</select>
    	<input type="submit" style="display: none">
		</form>
  </div>
</div>
<div class="row margin-top-20">
	<div class="col-md-offset-1 col-md-1 text-center">
		<h3 class="rotate">POTENCIAL</h3>
	</div>
	<div class="col-md-9">
		<table class="table resultados" width="100%">
			<tr class="border-left">
				<td class="texto-small" style="vertical-align: top">Alto</td>
				<td class="strong_blue_background">Colaboradores Total: <span id="v1"></span></td>
				<td class="strong_blue_background">Colaboradores Total: <span id="v2"></span></td>
				<td class="strong_blue_background">Colaboradores Total: <span id="v3"></span></td>
			</tr>
			<tr class="border-left">
				<td></td>
				<td class="amarillo medio-alto ancho">DIAMANTE EN BRUTO<b>6</b></td>
				<td class="verde-claro medio-alto ancho">ESTRELLA POTENCIAL<b>3</b></td>
				<td class="verde medio-alto ancho">SUPER ESTRELLA<b>1</b></td>
			</tr>
			<div style="display: none">
				{{ $v1 = 0 }}
				{{ $v2 = 0 }}
				{{ $v3 = 0 }}
			</div>
			<tr class="border-left">
				<td></td>
				<td class="amarillo-claro">
					@foreach ($users as $key => $value)
						@if (empty($totales_desempeno[$value->id]) || empty($totales_objetivos[$value->id]))
							<?php continue; ?>
						@endif
						@if ($totales_desempeno[$value->id] >= 90 && $totales_desempeno[$value->id] <= 100 && $totales_objetivos[$value->id] < 75)
							<div>{{$usuarios[$value->id]}}</div>
							<div style="display: none"> {{ $v1++ }}</div>
						@endif
					@endforeach
				</td>
				<td class="verde-claro-light">
					@foreach ($users as $key => $value)
						@if (empty($totales_desempeno[$value->id]) || empty($totales_objetivos[$value->id]))
							<?php continue; ?>
						@endif
						@if ($totales_desempeno[$value->id] >= 90 && $totales_desempeno[$value->id] <= 100 && $totales_objetivos[$value->id] >= 75 && $totales_objetivos[$value->id] < 90)
							<div>{{$usuarios[$value->id]}}</div>
							<div style="display: none"> {{ $v2++ }}</div>
						@endif
					@endforeach
				</td>
				<td class="verde-light">
					@foreach ($users as $key => $value)
						@if (empty($totales_desempeno[$value->id]) || empty($totales_objetivos[$value->id]))
							<?php continue; ?>
						@endif
						@if ($totales_desempeno[$value->id] >= 90 && $totales_desempeno[$value->id] <= 100 && $totales_objetivos[$value->id] >= 90 && $totales_objetivos[$value->id] <= 100)
							<div>{{$usuarios[$value->id]}}</div>
							<div style="display: none"> {{ $v3++ }}</div>
						@endif
					@endforeach
				</td>
			</tr>
			<tr><td class="border-left"></td></tr>
		{{-- </table>
		<table class="table resultados" width="100%"> --}}
			<tr class="border-left">
				<td class="medio-ancho"></td>
				<td class="strong_blue_background">Colaboradores Total: <span id="v4"></span></td>
				<td class="strong_blue_background">Colaboradores Total: <span id="v5"></span></td>
				<td class="strong_blue_background">Colaboradores Total: <span id="v6"></span></td>
			</tr>
			<tr class="border-left">
				<td></td>
				<td class="rojo-claro medio-alto ancho">INCONSISTENTE<b>8</b></td>
				<td class="amarillo medio-alto ancho">CONTRIBUIDOR CONSISTENTE/JUGADOR CLAVE<b>5</b></td>
				<td class="verde-claro medio-alto ancho">FUTURA ESTRELLA<b>2</b></td>
			</tr>
			<div style="display: none">
				{{ $v4 = 0 }}
				{{ $v5 = 0 }}
				{{ $v6 = 0 }}
			</div>
			<tr class="border-left">
				<td></td>
				<td class="rojo-claro-light">
					@foreach ($users as $key => $value)
						@if (empty($totales_desempeno[$value->id]) || empty($totales_objetivos[$value->id]))
							<?php continue; ?>
						@endif
						@if ($totales_desempeno[$value->id] >= 75 && $totales_desempeno[$value->id] < 90 && $totales_objetivos[$value->id] < 75)
							<div>{{$usuarios[$value->id]}}</div>
							<div style="display: none"> {{ $v4++ }}</div>
						@endif
					@endforeach
				</td>
				<td class="amarillo-claro">
					@foreach ($users as $key => $value)
						@if (empty($totales_desempeno[$value->id]) || empty($totales_objetivos[$value->id]))
							<?php continue; ?>
						@endif
						@if ($totales_desempeno[$value->id] >= 75 && $totales_desempeno[$value->id] < 90 && $totales_objetivos[$value->id] >= 75 && $totales_objetivos[$value->id] < 90)
							<div>{{$usuarios[$value->id]}}</div>
							<div style="display: none"> {{ $v5++ }}</div>
						@endif
					@endforeach
				</td>
				<td class="verde-claro-light">
					@foreach ($users as $key => $value)
						@if (empty($totales_desempeno[$value->id]) || empty($totales_objetivos[$value->id]))
							<?php continue; ?>
						@endif
						@if ($totales_desempeno[$value->id] >= 75 && $totales_desempeno[$value->id] < 90 && $totales_objetivos[$value->id] >= 90 && $totales_objetivos[$value->id] <= 100)
							<div>{{$usuarios[$value->id]}}</div>
							<div style="display: none"> {{ $v6++ }}</div>
						@endif
					@endforeach
				</td>
			</tr>
			<tr><td class="border-left"></td></tr>
		{{-- </table>
		<table class="table resultados" width="100%"> --}}
			<tr class="border-left">
				<td></td>
				<td class="strong_blue_background">Colaboradores Total: <span id="v7"></span></td>
				<td class="strong_blue_background">Colaboradores Total: <span id="v8"></span></td>
				<td class="strong_blue_background">Colaboradores Total: <span id="v9"></span></td>
			</tr>
			<tr class="border-left">
				<td></td>
				<td class="rojo medio-alto ancho">ESTRATEGIA DE DESVINCULACIÓN<b>9</b></td>
				<td class="rojo-claro medio-alto ancho">TIBIO/CONTRIBUIDO MARGINAL<b>7</b></td>
				<td class="amarillo medio-alto ancho">PROFESIONAL CON EXPERIENCIA<b>4</b></td>
			</tr>
			<div style="display: none">
				{{ $v7 = 0 }}
				{{ $v8 = 0 }}
				{{ $v9 = 0 }}
			</div>
			<tr class="border-left">
				<td class="texto-small" style="vertical-align: bottom">Bajo</td>
				<td class="rojo-light">
					@foreach ($users as $key => $value)
						@if (empty($totales_desempeno[$value->id]) || empty($totales_objetivos[$value->id]))
							<?php continue; ?>
						@endif
						@if ($totales_desempeno[$value->id] < 75 && $totales_objetivos[$value->id] < 75)
							<div>{{$usuarios[$value->id]}}</div>
							<div style="display: none"> {{ $v7++ }}</div>
						@endif
					@endforeach
				</td>
				<td class="rojo-claro-light">
					@foreach ($users as $key => $value)
						@if (empty($totales_desempeno[$value->id]) || empty($totales_objetivos[$value->id]))
							<?php continue; ?>
						@endif
						@if ($totales_desempeno[$value->id] < 75 && $totales_objetivos[$value->id] >= 75 && $totales_objetivos[$value->id] < 90)
							<div>{{$usuarios[$value->id]}}</div>
							<div style="display: none"> {{ $v8++ }}</div>
						@endif
					@endforeach
				</td>
				<td class="amarillo-claro">
					@foreach ($users as $key => $value)
						@if (empty($totales_desempeno[$value->id]) || empty($totales_objetivos[$value->id]))
							<?php continue; ?>
						@endif
						@if ($totales_desempeno[$value->id] < 75 && $totales_objetivos[$value->id] >= 90 && $totales_objetivos[$value->id] <= 100)
							<div>{{$usuarios[$value->id]}}</div>
							<div style="display: none"> {{ $v9++ }}</div>
						@endif
					@endforeach
				</td>
			</tr>
			<tr>
				<td></td>
				<td class="texto-small border-bottom" style="vertical-align: top; text-align: left;">Bajo</td>
				<td class="border-bottom"></td>
				<td class="texto-small border-bottom" style="vertical-align: top; text-align: right;">Alto</td>
			</tr>
		</table>
		<div class="text-center">
			<h3>DESEMPEÑO</h3>
		</div>
	</div>
	{{-- <div class="col-md-3"></div> --}}
</div>
<div class="row margin-top-20">
	<div class="col-md-12 text-right">
		<a href="{{ url('/avance-colaboradores') }}" role="button" class="btn btn-primary">Regresar</a>
	</div>
</div>
@endsection

@section('scripts')
	<script>
		
		$(document).ready(function(){
			$('select.period_id').change(function(){
				$('form#changePlanForm').submit();
			});

			var v1 = {{ $v1 }}
			var v2 = {{ $v2 }}
			var v3 = {{ $v3 }}
			$('#v1').html(v1);
			$('#v2').html(v2);
			$('#v3').html(v3);
			
			var v4 = {{ $v4 }}
			var v5 = {{ $v5 }}
			var v6 = {{ $v6 }}
			$('#v4').html(v4);
			$('#v5').html(v5);
			$('#v6').html(v6);

			var v7 = {{ $v7 }}
			var v8 = {{ $v8 }}
			var v9 = {{ $v9 }}
			$('#v7').html(v7);
			$('#v8').html(v8);
			$('#v9').html(v9);
		});
	</script>
@endsection
