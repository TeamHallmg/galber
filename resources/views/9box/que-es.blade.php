﻿@extends('layouts.app')

@section('title', '9Box')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('9box/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid w-100" src="/img/9box/edr.png" alt="">
		{{-- <p class="mt-2 text-justify">La metodología matriz de talento o evaluación 9 box, tiene como objetivo detectar fortalezas y áreas de oportunidad de los colaboradores, identificando con mayor eficiencia sus necesidades y así, lograr el cumplimiento de todas las metas y/o estrategias de la organización.</p>
		<p>Esta metodología de medición se realiza por medio de nueve cuadrantes, los cuales tomarán en cuenta el rendimiento del personal conforme a dos factores, potencial y desempeño. Estos son medidos a través de las evaluaciones de objetivos y competencias.</p>
		<p>Dicha evaluación permite formalizar un plan de desarrollo, tanto personal como profesional, para cada uno de los colaboradores evaluados.</p> --}}
	</div>
</div>
@endsection
