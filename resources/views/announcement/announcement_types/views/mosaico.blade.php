<style>
	.img-mosaico{
		padding: 0px;
		margin: 0px;
		width: 100%;
	}
</style>
	
@php
	$max = 3;
@endphp

<div class="container-fluid my-5" style="position: relative; display: inline-block;">
	{{-- <div style="position: relative; display: inline-block;"> --}}
		@if(Auth::user()->hasRolePermission('announcement_mosaic'))
			<div style="position: absolute; right: 0; z-index: 5;">
				<a href="{{ url('announcements?view=' . $display_announcements['mosaico']['view'] . '&type=' . $display_announcements['mosaico']['type']->id )}}" class="btn btn-primary btn-sm mt-2 mr-2" style="z-index:1;">
					<i class="nav-icon fas fa-cog fa-lg"></i> Editar
				</a>
			</div>
		@endif
		<div class="row">
			@foreach($announcements as $key => $announcement)
				{{--@if ($loop->index % $max === 0)
					
				@endif--}}
					<div class="col-4 px-0">
						@if(isset($announcement->data['link']))
							<a href="{{$announcement->data['link']}}">
						@endif
							<img class="img-mosaico img-fluid" src="{{ asset('/img/announcements/'.$announcement->data['image']) }}">
						@if(isset($announcement->data['link']))
							</a>
						@endif
					</div>
				{{--@if ($loop->iteration % $max === 0 && $loop->index !== 0 || $loop->last)
					
				@endif--}}
			@endforeach
		</div>
	{{-- </div> --}}
</div>