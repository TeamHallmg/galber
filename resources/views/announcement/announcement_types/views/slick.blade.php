<div class="row justify-content-center">
  @if(Auth::user()->hasRolePermission('announcement_carrousel'))
    <div class="col-md-12 text-right mb-3">
      <a href="{{ url('announcements?view=' . $display_announcements['slick']['view'] . '&type=' . $display_announcements['slick']['type']->id )}}" class="btn btn-sm btn-primary mt-2 mr-2" style="z-index:1;">
        <i class="nav-icon fas fa-cog fa-lg"></i> Editar
      </a>
    </div>
  @endif
  <div class="col-11 multiple-items d-none" data-slick='{"slidesToShow": 5, "slidesToScroll": 1}'>
      @foreach($announcements as $key => $announcement)
        <div>
            <div class="slide-item mx-3 mb-4 text-center text-dark">
              @if(isset($announcement->data['link']))
                <a href="{{ $announcement->data['link'] }}">
              @endif
              <img class="img-fluid" style="width: 100%" src="/img/announcements/{{$announcement->data['image']}}" alt="">
              @if(isset($announcement->data['link']))
                </a>
              @endif
            </div>
        </div>
      @endforeach
  </div>
</div>