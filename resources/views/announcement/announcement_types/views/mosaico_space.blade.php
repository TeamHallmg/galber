<div class="container-fluid px-0">
	<div class="row justify-content-left">
		@foreach($announcements as $key => $announcement)
			<div class="col-12 col-md-{{$columns}} my-3">
				@if(Route::current()->getName() === 'marcas')
					<a class="myModal" style="cursor: pointer;" data-image="{{ isset($announcement->data['image']) ? $announcement->data['image'] : '' }}" data-link="{{ isset($announcement->data['link']) ? $announcement->data['link'] : '' }}" data-description="{{ isset($announcement->data['description']) ? $announcement->data['description'] : '' }}" data-toggle="modal" data-target="#modalMarcas">
				@endif
				@if(isset($announcement->data['link']) && Route::current()->getName() !== 'marcas')
					<a href="{{$announcement->data['link']}}" target="_blank">
				@endif
				<div class="contenedor shadow-3">
					@if(isset($announcement->data['image']))
					<img class="image img-fluid" src="{{ asset('/img/announcements/'.$announcement->data['image']) }}">
						<div class="overlay shadow-1">
							@if(isset($announcement->data['description']))
								@if(Route::current()->getName() !== 'marcas')
									<div class="text col-12">
										{!! $announcement->data['description'] !!}
									</div>
								@endif
							@endif
						</div>
					@endif
				</div>
				@if(isset($announcement->data['link']))
					</a>
				@endif
			</div>
	    @endforeach
	</div>
</div>

<div id="modalMarcas" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header alert-secondary">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="img-modal" class="d-flex justify-content-center">

				</div>
				<div id="text-modal" class="d-flex justify-content-center">
				
				</div>
				<div id="link-modal" class="d-flex justify-content-center">

				</div>
			</div>
		</div>
	</div>
</div>

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
		$('.myModal').on('click', function(){
			var image = $(this).data('image');
			var link = $(this).data('link');
			var description = $(this).data('description');

			$('#img-modal').empty();
			$('#text-modal').empty();
			$('#link-modal').empty();
			$('#img-modal').append('<img class="img-fluid w-25 h-25" src="/img/announcements/'+image+'">');
			$('#text-modal').append('<div class="col-12 mt-4">'+description+'</div');
			if(link !== ''){
				$('#link-modal').append('<a href="http://'+link+'" class="btn btn-dark" target="_blank">Página Oficial</a>');
				$('#link-modal').click(function(){
					location.reload();
				})
			}
		});
    });
</script>
@endsection




