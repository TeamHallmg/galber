<style>
	.cols4-img{
		height: 20vh;
	}

	.cols4-title{
		background-color: #B8B8B8;
		text-align: center;
		padding: .5rem 0px;
	}
	
	.cols4-title h4{
		font-weight: bold;
	}
</style>
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-10">
		<div class="row justify-content-center">
		@foreach($announcements as $announcement)
			<div class="col-md-3" style="text-align: center;">	
				<img src="{{asset('img/announcements/'.$announcement->data['image'])}}" class="img-fluid cols4-img" alt="">
				<div class="cols4-title">
					<h4>{{$announcement->data['title']}}</h4>
				</div>
				<p>{{$announcement->data['description']}}</p>
			</div>
		@endforeach
		</div>
	</div>
	<div class="col-md-1"></div>
</div>