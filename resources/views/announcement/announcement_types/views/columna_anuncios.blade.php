{{-- @if(isset($columna_anuncios['columna_anuncios']) && Auth::user()->role === 'admin')
<div class="row mb-5">
	<div class="col">
		<a class="btn btn-secondary btn-sm" href="{{ url('announcements?view=' . $columna_anuncios['view'] . '&type=' . $columna_anuncios['type']->id )}}">
			<i class="fa fa-plus nav-icon"></i> <span>Administrar</span> {{ $columna_anuncios['type']->show_name ?? '' }}
		</a>
	</div>
</div>
@endif --}}

<div class="container">
@isset($columna_anuncios['columna_anuncios'])
	@guest
		@foreach($columna_anuncios['columna_anuncios'] as $announcement)
		<div class="row mb-2">
			<div class="col">
				<img class="mx-auto d-block w-100" src="/img/announcements/{{$announcement->data['image'] ?? ''}}" alt="">
				<div class="mt-4 text-justify">
					{!! $announcement->data['description'] ?? '' !!}
				</div>
			</div>
		</div>
		@endforeach
	@else
		@if(Auth::user()->hasRolePermission('announcement_columns'))
			<div class="row justify-content-end">
				<a href="{{ url('announcements?view=' . $columna_anuncios['view'] . '&type=' . $columna_anuncios['type']->id )}}" class="btn btn-primary btn-sm mt-2 mr-2" style="z-index:1;">
					<i class="nav-icon fas fa-cog fa-lg"></i> Editar
				</a>
			</div>
		@endif
		@foreach($columna_anuncios['columna_anuncios'] as $announcement)
		<div class="row mb-2">
			<div class="col">
				<img class="mx-auto d-block w-100" src="/img/announcements/{{$announcement->data['image'] ?? ''}}" alt="">
				<div class="mt-4 text-justify">
					{!! $announcement->data['description'] ?? '' !!}
				</div>
			</div>
		</div>
		@endforeach
	@endguest
@endif
</div>

{{-- @guest
@else
	@if(isset($columna_anuncios['columna_anuncios']) && Auth::user()->role === 'admin')
	<div class="row mb-3">
		<div class="col">
			<a class="btn btn-secondary btn-sm" href="{{ url('announcements?view=' . $columna_anuncios['view'] . '&type=' . $columna_anuncios['type']->id )}}">
				<i class="fa fa-plus nav-icon"></i> <span>Administrar</span> {{ $columna_anuncios['type']->show_name ?? '' }}
			</a>
		</div>
	</div>
	@endif
@endguest --}}