@php
    $cont = 1;
    $cont_new_convenios = 0;
@endphp
@foreach ($announcements as $announcement)
    <div id="{{ $announcement->id }}" class="col-md-3 col-sm-4 col-xs-6 mb-2 convenios-card c_{{ $announcement->announcement_category_id }} {{ $cont_new_convenios++ < config('announcements.ANNOUNCEMENTS_MAX_CONVENIOS_NUEVOS') ?'convenios_nuevos':'' }} {{ $announcement->favorite?'c_favorite':'' }} {{ $announcement->like?'c_like':'' }}">
        @if(isset($announcement->data['image']))
            <div class="convenio-container convenio-card" data-toggle="modal" data-target="#convenios_modal" src="{{ asset('img/announcements/'. $announcement->data['image']) }}" alt="{{ $announcement->data['title'] }}" data-description="{{ $announcement->data['description'] }}" data-link="{{ $announcement->data['link'] ?? '#' }}"
                style="
                    background-image:url('{{ asset('img/announcements/'. $announcement->data['image']) }}');
                    background-size:cover;
                    background-position:center;
            ">
            </div>
        @else
            <img class="img-responsive convenio-card" data-toggle="modal" data-target="#convenios_modal" src="https://via.placeholder.com/300x240/0078AD/FFFFFF?text={{ $announcement->data['title'] }}" alt="{{ $announcement->data['title'] }}" data-description="{{ $announcement->data['description'] }}" data-link="{{ $announcement->data['link'] ?? '#' }}">
        @endif
        <div style="display: none" class="convenio_text">{{ $announcement->data['title'] }} {{ $announcement->data['description'] }}</div>
        <div class="row mt-2">
            <div class="col-xs-6 text-center pr-0">
                <span class="ml-3 convenios-btn {{ $announcement->favorite?'convenios-favorite':'' }}" data-id="{{ $announcement->id }}" data-type="favorite"><i class="{{ $announcement->favorite?'fas':'far' }} fa-star"></i> <span class="inner-text">Favoritos</span></span>
            </div>
            <div class="col-xs-6 text-center p-0">
                <span class="ml-3 convenios-btn {{ $announcement->like?'convenios-like':'' }}" data-id="{{ $announcement->id }}" data-type="like"><i class="{{ $announcement->like?'fas':'far' }} fa-thumbs-up"></i> <span class="inner-text">Me interesa</span></span>
            </div>
        </div>
    </div>
@endforeach