<div class="row">
    @foreach($announcements->sortByDesc('created_at') as $announcement)
        <div class="col-md-3 encont my-4">
            <div class="container-fluid card-1 p-3">
                {{-- border-gallery --}}
                <div class="row">
                    <div class="col-12 col-md-12">
                        {{--dd($announcement)--}}
                        @if(isset($announcement->data['image']))
                        <div>
                            <div style="
                            padding: 30%;
                            background-image: url('/img/announcements/{{ $announcement->data['image'] }}');
                            background-position: center; 
                            background-repeat: no-repeat;
                            background-size: cover;" 
                            class="col-12 h-100 w-100 rounded"></div>
                            {{--<img style="max-height: 150px;" class="h-100 w-100 rounded" src="/img/announcements/{{ $announcement->data['image'] }}" alt="">--}}
                        </div>
                        @endif
                        <div>
                            @if(isset($announcement->data['gallery']))
                                <button type="button" class="btn btn-blue col-12 mt-3 card-1 font-weight-bold" data-toggle="modal" data-target="#exampleModal" data-name="{{ trim($announcement->data['gallery'], "[]" )}}">
                                    Ver Galería
                                </button>
                            @else
                                <button type="button" class="btn btn-blue col-12 mt-3 card-1 font-weight-bold disabled">
                                    Ver Galería
                                </button>
                            @endif
                        </div>
                    </div>
                    <div class="col-12 col-md-12 mt-2">
                        <div class="text-blue font-weight-bolder font-size-1">Nombre del Evento:</div>
                        @if(isset($announcement->data['title']))
                            <div for="">{{ $announcement->data['title'] }}</div>
                        @endif
                        <div class="text-blue font-weight-bolder font-size-1">Fecha:</div>
                        @if(isset($announcement->data['date']))
                            <div for="">{{ $announcement->data['date'] }}</div>
                        @endif
                        <div class="text-blue font-weight-bolder font-size-1">Lugar:</div>
                        @if(isset($announcement->data['place']))
                            <div for="">{{ $announcement->data['place'] }}</div>
                        @endif
                    </div>
                </div>
                
                @if(isset($announcement->data['link']))
                    <a href="http://{{$announcement->data['link']}}">
                @endif
                @if(isset($announcement->data['link']))
                    </a>
                @endif
            </div>
        </div>
    @endforeach
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header bg-blue text-white">
                <h5 class="modal-title" id="exampleModalLabel">Galería de Eventos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="slider-for col-12 col-md-12">
                            
                        </div>
                    </div>
                    <div class="row justify-content-center pr-5 pt-2">
                        <div class="slider-nav pl-5 justify-content-center col-10 col-md-10">
                                
                        </div>
                    </div>
                </div>
                <div class="modal-footer mt-2">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>