	@if(isset($announcements[0]->data['image']))
		<div style="position: relative; display: inline-block;">
			@guest
				<img class="mx-auto d-block" src="/img/announcements/{{$announcements[0]->data['image']}}" alt="" style="width: 100%">
			@else	
				@if(Auth::user()->hasRolePermission('announcement_banner'))
					<div style="position: absolute; right: 0; z-index: 5;">
						<a href="{{ url('announcements?view=' . $display_announcements['banner']['view'] . '&type=' . $display_announcements['banner']['type']->id )}}" class="btn btn-sm btn-primary mt-2 mr-2" style="z-index:1;">
							<i class="nav-icon fas fa-cog fa-lg"></i> Editar
						</a>
					</div>
				@endif
				<img class="mx-auto d-block" src="/img/announcements/{{$announcements[0]->data['image']}}" alt="" style="width: 100%">
			@endguest
		</div>
	@else
		<div style="position: relative; display: inline-block;">
			@guest
				<img class="mx-auto d-block" src="https://via.placeholder.com/1623x395/244395/FFFFFF?text=Banner" alt="" style="width: 100%">
			@else
				@if(Auth::user()->hasRolePermission('announcement_banner'))
					<div style="position: absolute; right: 0; z-index: 5;">
						<a href="{{ url('announcements?view=' . $display_announcements['banner']['view'] . '&type=' . $display_announcements['banner']['type']->id )}}" class="btn btn-sm btn-primary mt-2 mr-2" style="z-index:1;">
							<i class="nav-icon fas fa-cog fa-lg"></i> Editar
						</a>
					</div>
				@endif
				<img class="mx-auto d-block" src="https://via.placeholder.com/1623x395/244395/FFFFFF?text=Banner" alt="" style="width: 100%">
			@endguest
		</div>
	@endif
