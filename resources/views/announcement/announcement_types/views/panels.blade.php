@foreach($announcements as $announcement)
    <div>
    {{-- <div style="margin: 2rem"> --}}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-2 border-0 p-0">
                    {{-- <div class="card-header card-header-white-bg">
                        <h5 class="card-title text-white">-</h5>
        
                        <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        </div>
                    </div> --}}
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="row">
                            @if($loop->iteration % 2 == 1)

                            <div class="col-md-6 d-flex align-items-center justify-content-center p-0">
                                <div class="col-md-12">
                                    <h1 class="title-blue text-danger text-right">{{ isset($announcement->data['title'])?$announcement->data['title']:'' }}</h1>
                                    {{-- <br> --}}
                                    <p class="text-justify text-blue text-right">
                                        {!! isset($announcement->data['description'])?$announcement->data['description']:'' !!}
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6 d-flex align-items-center justify-content-center p-0">
                                <img class="w-100" src="img/announcements/{{$announcement->data['image']}}" alt="{{ isset($announcement->data['title'])?$announcement->data['title']:'' }}">
                            </div>
                            @else
                            <div class="col-md-6 d-flex align-items-center justify-content-center p-0">
                                <img class="w-100" src="img/announcements/{{$announcement->data['image']}}" alt="{{ isset($announcement->data['title'])?$announcement->data['title']:'' }}">
                            </div>
                            <div class="col-md-6 d-flex align-items-center justify-content-center p-0">
                                <div class="col-md-12">
                                    <h1 class="title-blue text-danger">{{ isset($announcement->data['title'])?$announcement->data['title']:'' }}</h1>
                                    {{-- <br> --}}
                                    <p class="text-justify text-blue">
                                        {!! isset($announcement->data['description'])?$announcement->data['description']:'' !!}
                                    </p>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach