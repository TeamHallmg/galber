@if(Auth::user()->hasRolePermission('announcement_images'))
    <div class="row d-flex justify-content-end mb-3">
        <a href="{{ url('announcements?view=' . $display_announcements['images']['view'] . '&type=' . $display_announcements['images']['type']->id )}}" class="btn btn-primary btn-sm mt-2 mr-2" style="z-index:1;">
            <i class="nav-icon fas fa-cog fa-lg"></i> Editar
        </a>
    </div>
@endif

<div class="row">
    @foreach($announcements as $announcement)
        <div class="col-12 px-0">
            <img class="mx-auto d-block w-100" src="/img/announcements/{{$announcement->data['image']}}" alt="{{ $announcement->data['title'] ?? '' }}">
        </div>
    @endforeach
</div>