<table class="table table-striped table-bordered eventos w-100" id="calendar">
    <thead class="bg-blue text-white">
        <tr>
            <th>Foto</th>
            <th>Nombre del Evento</th>
            {{-- <th class="d-none">Fecha fake</th> --}}
            <th>Fecha</th>
            <th class="d-none" style="background-color: #EBC200">Mes</th>
            <th>Hora y Lugar</th>
            {{-- <th>Descripción</th> --}}
        </tr>
        <tr class="bg-lightgray" id="thead_filters">
            <th class="bg-lightgray">
                {{-- <input type="text" class="form-control text-center" placeholder="&#xF002;" style="font-family:Arial, FontAwesome" data-index="0"> --}}
            </th>
            <th class="bg-lightgray">
                <input type="text" class="form-control text-center" placeholder="Nombre del Evento..." data-index="1">
            </th>
            <th class="bg-lightgray">
                <input type="text" class="form-control text-center" placeholder="Fecha..." data-index="2">
            </th>
            <th class="bg-lightgray d-none">
                <input type="text" class="form-control text-center" placeholder="Mes..." data-index="3">
            </th>
            <th class="bg-lightgray">
                <input type="text" class="form-control text-center" placeholder="Hora y Lugar..." data-index="4">
            </th>
            {{-- <th class="bg-lightgray">
                <input type="text" class="form-control text-center" placeholder="Descripción..." data-index="5">
            </th> --}}
        </tr>
    </thead>
    <tbody>
        @foreach($announcements as $announcement)
            <tr id="{{$announcement->id}}">
                <td class="text-center align-middle">
                    @if(isset($announcement->data['link']))
                        <a href="http://{{$announcement->data['link']}}">
                            @if(isset($announcement->data['image']))
                                <img style="max-height: 150px" src="/img/announcements/{{ $announcement->data['image'] }}" class="img-responsive rounded">
                            @else
                                -
                            @endif
                        </a>
                    @else
                        @if(isset($announcement->data['image']))
                            <img style="max-height: 150px" src="/img/announcements/{{ $announcement->data['image'] }}" class="img-responsive rounded">
                        @else
                            -
                        @endif
                    @endif
                    <a class="btn btn-blue btn-block btn-sm mt-3" href="{{ url('calendario/'.$announcement->id) }}">Ver Evento</a>
                </td>
                <td class="align-middle">{{ $announcement->data['title'] }}</td>
                {{-- <td class="align-middle d-none">{{ date('m/d/Y', strtotime($announcement->data['date'])) }}</td> --}}
                <td class="align-middle" data-sort='{{ $announcement->data['date'] }}'>{{ date('d/m/Y', strtotime($announcement->data['date'])) }}</td>
                <td class="align-middle text-capitalize d-none">{{ $dt = Carbon\Carbon::parse($announcement->data['date'])->formatLocalized("%B %Y")}}</td>
                <td class="align-middle">
                    {{ $announcement->getFormattedHour('time_start') }} - {{ $announcement->getFormattedHour('time_end') }}
                    <br>
                    {{ $announcement->data['place'] }}
                </td>
                {{-- <td class="align-middle">{!! $announcement->data['description'] !!}</td> --}}
            </tr>
        @endforeach
    </tbody>
</table>

@if(Auth::user()->hasRolePermission('announcement_calendar'))
    <div class="row d-flex justify-content-end">
        <a href="{{ url('announcements?view=' . $display_announcements['calendario']['view'] . '&type=' . $display_announcements['calendario']['type']->id )}}" class="btn btn-primary btn-sm mt-2 mr-2" style="z-index:1;">
            <i class="nav-icon fas fa-cog fa-lg"></i> Editar
        </a>
    </div>
@endif

@section('scripts')
<script type="text/javascript">
    !function(a){a.fn.datepicker.dates.es={days:["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],daysShort:["Dom","Lun","Mar","Mié","Jue","Vie","Sáb"],daysMin:["Do","Lu","Ma","Mi","Ju","Vi","Sa"],months:["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],monthsShort:["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],today:"Hoy",monthsTitle:"Meses",clear:"Borrar",weekStart:1,format:"dd/mm/yyyy"}}(jQuery);
    $('.datepicker').datepicker({
        todayBtn: "linked",
        todayHighlight: true,
        toggleActive: true,
        format: 'dd/mm/yyyy',
        autoclose: true,
        language: 'es'
    });
    

    $('#datepicker').on('changeDate', function() {
        var dt = $('#datepicker').datepicker('getFormattedDate');
        console.log(dt);
        table.column(2).search(dt).draw();
    });

    $(document).ready(function(){
        
        var values = table.columns(2).data();
        var min = new Date();
        function date(d) {
            return new Date(d) > min;
        }
        values = values[0].filter(date);

        var myArray = values.map(function(val){
            return val.slice(0,2) + val.slice(5);
        });

        let unique = [...new Set(myArray)];
        let search = unique.join('|');

        console.log(search);
        table.column(3).search(search,true,false,false,false).draw();
    });

</script>
<script type="text/javascript">

// Shorthand for $( document ).ready()
/*$(function() {
});*/

    let table = $('#calendar').DataTable({
        "bSortCellsTop": true,
        "rowGroup":{
            "dataSrc": 3,
            "className": "text-capitalize dtrg-group"
        },
        // "columnDefs": [
        //     { "width": "35%", "targets": 5 }
        // ],
        "order": [[ 2, "asc" ]],
        "paging": true,
        "pagingType": "numbers",
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     ">",
                "sPrevious": "<"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $( table.table().container() ).on( 'keyup', 'thead input', function () {
        table
            .column( $(this).data('index') )
            .search( this.value )
            .draw();
    } );

</script>
@endsection