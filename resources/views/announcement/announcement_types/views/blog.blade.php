<div class="row">
    @if(Auth::user()->hasRolePermission('announcement_blog'))
        <div class="col-md-12 text-right">
            <a href="{{ url('announcements?view=' . $display_announcements['blog']['view'] . '&type=' . $display_announcements['blog']['type']->id )}}" class="btn btn-sm btn-primary mt-2 mr-2" style="z-index:1;">
                <i class="nav-icon fas fa-cog fa-lg"></i> Editar
            </a>
        </div>
    @endif
    @foreach($announcements as $announcement)
    <div class="col-md-12">
        <div class="row">
            {{-- <div class="col-12 mt-4">
                <h2 class="text-blue font-weight-bold m-0"
                    style="border-bottom: 4px solid rgb(137, 135, 136);">
                    {{ isset($announcement->data['title'])?$announcement->data['title']:'' }}
                </h2>
            </div> --}}
            <div class="col-md-12 d-flex align-items-center justify-content-center px-0">
                <div class="col-md-12 px-0">
                    <p class="title-blue">{!! isset($announcement->data['description'])?$announcement->data['description']:'' !!}</p>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>