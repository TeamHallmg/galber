<div id="carouselExampleIndicators{{$type}}" class="carousel slide mx-auto row card-2" data-ride="carousel">
  {{-- <div class="row"> --}}
    
  {{-- </div> --}}
  <ol class="carousel-indicators">
    @foreach($announcements as $key => $announcement)
      <li data-target="#carouselExampleIndicators{{$type}}" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
    @endforeach
  </ol>
  {{-- <div style="position: relative; display: inline-block;"> --}}
    @if(Auth::user()->hasRolePermission('announcement_carrousel'))
      <div style="position: absolute; right: 0; z-index: 5;">
        <a href="{{ url('announcements?view=' . $display_announcements['carrousel']['view'] . '&type=' . $display_announcements['carrousel']['type']->id )}}" class="btn btn-primary btn-sm mt-2 mr-2" style="z-index:1;">
          <i class="nav-icon fas fa-cog fa-lg"></i> Editar
        </a>
      </div>
    @endif
    <div class="carousel-inner" role="listbox">
        {{-- <a href="{{ url('admin_banners') }}" class="nav-link" style="position:absolute; margin-left: 96%; z-index:999; color:#F6F914;">
            <i class="nav-icon fas fa-cog fa-lg"></i>
          </a> --}}
      @foreach ($announcements as $key => $announcement)
        <div class="carousel-item {{ $loop->first ? 'active' : '' }}" >
          <img class="w-100" src="img/announcements/{{$announcement->data['image']}}" alt="{{ isset($announcement->data['title'])?$announcement->data['title']:'' }}">
          <div class="carousel-caption d-none d-md-block d-sm-block">
          </div>
        </div>
      @endforeach
    {{-- </div> --}}
  </div>
  
   <!-- Controls -->
  @if(count($announcements) > 1)
  <a class="carousel-control-prev" href="#carouselExampleIndicators{{$type}}" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators{{$type}}" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  @endif
</div>