<div class="mx-0">
    <ul class="list-group list-group-flush p-0">
        @forelse($announcements as $announcement)
            <li class="list-group-item faq-border px-0">                    
                <h5><strong>{{ isset($announcement->data['title'])?$announcement->data['title']:'' }}</strong></h5>
                <p class="title-blue">{!! isset($announcement->data['description'])?$announcement->data['description']:'' !!}</p> 
            </li>
        @empty
            <li class="list-group-item px-0 text-center">                    
                <h5><strong>No hay preguntas activas</strong></h5>
            </li>
        @endforelse
    </ul>
</div>
@isset($display_announcements['simple_list'])
    @if(Auth::user()->hasRolePermission('announcement_simple_list'))
        <div class="row">
            <div class="col-md-12 text-right">
                <a class="btn btn-sm btn-primary" href="{{ url('announcements?view=' . $display_announcements['simple_list']['view'] . '&type=' . $display_announcements['simple_list']['type']->id )}}">
                    <i class="nav-icon fas fa-cog fa-lg"></i> Editar
                </a>
            </div>
        </div>
    @endif
@endif
