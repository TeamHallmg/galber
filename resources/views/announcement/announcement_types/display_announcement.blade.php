@inject('TPV', 'App\Models\Announcement\AnnouncementTypePerView')
@if($TPV::check($announcements['view'],$announcements['type']->id))
<a href="{{url('announcements?view='.$announcements['view'].'&type='.$announcements['type']->id)}}" class="btn btn-primary">Administrar {{$announcements['type']->show_name}}</a>
@include('announcement.announcement_types.views.'.key($announcements), ['announcements' => $announcements[key($announcements)],"type"=>$announcements['type']->id])
@endif