@extends('announcement.app')

@section('title', 'Crear Tipo de Anuncio')

@section('content')

<h1>Esta vista solo la deberia de ver el desarrollador por el momento</h1>
<h3>Para poder nosotros crear un tipo de anuncio es necesario crear la vista que lo tratara, es decir, hacer el archivo blade.php en el cual nosotros le daremos formato a nuestro anuncio, es por esto que <strong>No se puede</strong> crear tipos de anuncios de la nada</h3>

{{--
<div class="row margin-top-20">
	<div class="col-md-12">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			{!! Form::open(['route' => 'admin.announcement-types.store', 'method' => 'POST'])!!}

				@include('announcement-types.partials.form-fields')

				<div class="form-group">
					{!! Form::submit('Agregar', ['class' => 'btn btn-success'])!!}
				</div>
			{!! Form::close() !!}
		</div>
		<div class="col-md-3"></div>
	</div>
</div>
--}}
@endsection