@extends('announcement.app')

@section('content')

<div class="card">
	<div class="card-header">
		<h1 class="an-title">Tipos de anuncios para la vista: {{$view->name}}</h1>
	</div>
	<div class="card-body">
			<table class="table table-striped table-bordered w-100" id="myTable">
				<thead>
					<tr>
						<th>#</th>
						<th>Nombre</th>
						<th class="text-center">Activos</th>
						<th class="text-center">Total</th>
						<th>Ir</th>
					</tr>
				</thead>
				<tbody>
					@if ($announcement_types->isEmpty())
						<tr class="text-center">
							<td colspan="5">No hay anuncios disponibles para esta vista</td>
						</tr>
					@endif
					@foreach($announcement_types as $announcement_type)
						<tr>
							<td style="width: 10%">{{ $loop->iteration }}</td>
							<td style="width: 60%">{{$announcement_type->name}}</td>
							@if($announcement_type->countActiveAnnouncementsInView($view->id) > 0)
								<td style="width: 10%" class="text-center"><span class="active-box notification-box">{{$announcement_type->countActiveAnnouncementsInView($view->id)}}</span></td>
							@else
								<td></td>
							@endif
							@if($announcement_type->countAnnouncementsInView($view->id) > 0)
								<td style="width: 10%" class="text-center"><span class="total-box notification-box">{{$announcement_type->countAnnouncementsInView($view->id)}}</span></td>
							@else
								<td></td>
							@endif
							<td style="width: 10%">
							@if($announcement_type->hasFormCreated())
								<a class="btn btn-primary" href="{{url('announcements?view='.$view->id.'&type='.$announcement_type->id)}}">Ver</a>
							@else
								<a class="btn btn-secondary" href="{{url('announcement_forms/create')}}">Crear Formulario</a>
							@endif
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
	</div>
	<div class="card-footer text-muted">
	</div>
</div>
@endsection
@section('mainScripts')
<script>
	$(document).ready( function () {
    	$('#myTable').DataTable({
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			},
		});
	});
</script>
@endsection