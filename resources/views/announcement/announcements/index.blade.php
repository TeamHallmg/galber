@extends('announcement.app')

@section('title', 'Anuncios')

@section('content')
<div class="container-fluid">
		<div class="card card-2">
			<div class="card-header text-center">
				<h1 class="card-title">Anuncios para la vista de: {{$view->name}}</h1>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<table class="table table-striped table-bordered" id="myTable">
							<thead>
								<tr>
									<th>#</th>
									@foreach($announcement_types->headers as $head)
									<th> {{ $head }} </th>
									@endforeach
									<th>Pagina</th>
									<th>Inicia</th>
									<th>Termina</th>
									<th>Acciones</th>
								</tr>
							</thead>
							<tbody>
								@if ($announcements->count() == 0)
									<tr class="text-center">
										<td colspan="{{count($announcement_types->headers) + 6}}">No hay anuncios</td>
									</tr>
								@else
									@foreach ($announcements as $announcement)
										<tr>
											<td>{{ $loop->iteration }}</td>
											@foreach($announcement_types->headers as $key => $data)
												<td width="10%">
											@if(isset($announcement->data[$key]))
												@if($key == 'link')
													<a href="{{$announcement->data[$key]}}">{{$announcement->data[$key]}}</a>
												@elseif($key == 'image')
													<img src="{{asset('img/announcements/'.$announcement->data[$key])}}" alt="" class="img-fluid">
												@elseif($key == 'description')
													<div>{{ str_limit($announcement->data[$key],90) }}</div>
												@else
													<div>{!!str_limit($announcement->data[$key],90)!!}</div>
												@endif
											@endif
												</td>
											@endforeach
											<td>{{ $announcement->view->name }}</td>
											{{--<td>{{ isset($announcement->announcementRegion)?$announcement->announcementRegion->name:'' }}</td>--}}
											<td>{{ $announcement->starts }}</td>
											<td>{{ $announcement->ends }}</td>
											<td style="vertical-align:middle" nowrap>
												<form class="form" action="{{ url('announcements/'.$announcement->id.'/activate')}}" method="POST">
													{!! method_field('POST') !!}
													{!! csrf_field() !!}
													<input class="form-control d-none" type="text" name="view" value="{{$view->id}}">
													<input class="form-control d-none" type="text" name="type" value="{{$type}}">
													<div class="form-row justify-content-center">
														@if (! $announcement->active)
															<button type="submit" name="Activar" class="btn btn-success mr-2 card-1">Activar</button>
														@else
															<button type="submit" name="Desactivar" class="btn btn-danger mr-2 card-1">Desactivar</button>
														@endif
														<a class="btn btn-primary card-1" href="{{ url('announcements/' . $announcement->id . '/edit') }}">Editar</a>
													</div>
												</form>
											</td>
										</tr>
									@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="card-footer text-muted">
				<a class="btn btn-dark card-1" href="{{url($view->name)}}">Regresar</a>
				<a class="btn btn-success card-1" href="{{ url('announcements/create?view='.$view->id.'&type='.$announcement_types->id)}}"></i> Agregar</a>
			</div>
		</div>
</div>

@endsection

@section('mainScripts')
<script>
	$(document).ready( function () {
    	$('#myTable').DataTable({
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			},
		});
	});
</script>
@endsection
