@extends('announcement.app')

@section('content')


<div class="card">
	<div class="card-header text-center">
		<h1>Páginas con anuncios</h1>
	</div>
	<div class="card-body">
		<table class="table table-striped table-bordered" id="myTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Pagina</th>
					<th>Ir</th>
				</tr>	
			</thead>
			<tbody>
				@foreach($views as $view)
					<tr>
						<td>{{$loop->iteration}}</td>
						<td>{{$view->name}}</td>
						<td><a class="btn btn-primary" href="{{url('announcements?view='.$view->id)}}">Editar</a></td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="card-footer text-muted">
	</div>
</div>

@endsection
@section('mainScripts')
<script>
	$(document).ready( function () {
    	$('#myTable').DataTable({
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			},
		});
	});
</script>
@endsection