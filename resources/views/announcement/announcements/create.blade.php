@extends('announcement.app')

@section('title', 'Crear Anuncio')

@section('content')
<div class="container">
	<div class="row my-5">
		<div class="col text-center">
			<h1>Crear un nuevo anuncio</h1>
		</div>
	</div>
	<form method="POST" action="{{ route('announcements.store') }}" enctype="multipart/form-data">
		{!! method_field('POST') !!}
		{!! csrf_field() !!}
		<div class="form-group col-12 col-md-12 d-none">
			<input class="form-control" type="text" name="view" value="{{$view}}">
			<input class="form-control" type="text" name="type" value="{{$announcement_type->id}}">
		</div>
		<div class="form-group col-12 col-md-12">
			@include('announcement.announcement_forms.form',['form' => $form])
		</div>
		@if($regions)
			<div class="form-group col-12 col-md-12">
				<label for="region">Regiones</label>
				<select name="region" id="region">
					<option value="">...Seleccione una region</option>
					@foreach($regions as $key => $region)
						<option value="{{$key}}">{{$region}}</option>
					@endforeach
				</select>
			</div>
		@endif
		@if($categories)
			<div class="form-group col-12 col-md-12">
				<label for="region">Categorías</label>
				<select name="region" id="region">
					<option value="">...Seleccione una categoria</option>
					@foreach($categories as $key => $category)
						<option value="{{$key}}">{{$category}}</option>
					@endforeach
				</select>
			</div>
		@endif
		<div class="form-group col-12 col-md-12">
			<div class="form-row ">
				<div class="col-12 col-md-3">
					<label for="starts">Inicia</label>
					<input class="form-control" type="date" name="starts" id="starts" placeholder="dd/mm/aaaa">
				</div>
			</div>
			<div class="form-row my-3">
				<div class="col-12 col-md-1">
					<label for="starts_hour">Hora</label>
					<input class="form-control" type="number" name="starts_hour" id="starts_minute" min="00" max="23">
				</div>
				<div class="col-12 col-md-1">
					<label for="starts_minute">Minuto</label>
					<input class="form-control" type="number" name="starts_minute" id="starts_minute" min="00" max="59">
				</div>
			</div>
		</div>
		<div class="form-group col-12 col-md-12">
				<div class="form-row ">
					<div class="col-12 col-md-3">
						<label for="ends">Termina</label>
						<input class="form-control" type="date" name="ends" id="ends" placeholder="dd/mm/aaaa">
					</div>
				</div>
				<div class="form-row my-3">
					<div class="col-12 col-md-1">
						<label for="ends_hour">Hora</label>
						<input class="form-control" type="number" name="ends_hour" id="ends_minute" min="00" max="23">
					</div>
					<div class="col-12 col-md-1">
						<label for="starts_minute">Minuto</label>
						<input class="form-control" type="number" name="ends_minute" id="ends_minute" min="00" max="59">
					</div>
				</div>
			</div>
		<div class="form-group col-12 col-md-12">
			<button type="submit" class="btn btn-success card-1">Crear</button>
		</div>
	</form>
</div>
@endsection

@section('scripts')
<script>
    $('.sn-component').summernote({
      placeholder: '',
      tabsize: 2,
      height: 400,
      toolbar: [
        ['style', ['style']],
        ['hr', ['hr']],
        ['fontsize', ['fontsize']],
        ['font', ['bold', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']],
      ]
    });
</script>
@endsection