
<li class="nav-item has-treeview">
    <a href="#" class="nav-link">
            <i class="nav-icon fas fa-bullhorn"></i>
        <p>
        Comunicación
        <i class="right fa fa-angle-left"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a class="nav-link" href="{{ url('announcements') }}">
                <i class="fa fa-plus nav-icon"></i>
                <p><span>Principal</span></p>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('announcement_form') }}">
                <i class="fa fa-plus nav-icon"></i>
                <p><span>Formularios / Estructura</span></p>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('announcement_categories') }}">
                <i class="fa fa-plus nav-icon"></i>
                <p><span>Categorías</span></p>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('announcement_types_per_view') }}">
                <i class="fa fa-plus nav-icon"></i>
                <p><span>Anuncios / Vistas</span></p>
            </a>
        </li>
    </ul>
</li>