@extends('announcement.app')

@section('content')
<div class="row my-4">
	<div class="col-md-5">
		<h2 class="font-weight-bold">ADMINISTRAR FORMULARIOS</h2>
	</div>
	<div class="col-md-6">
	@if($canCreateForm)			
		<form action="{{route('announcement_forms.create')}}">
			<button class="btn btn-dark card-1"><i class="fas fa-plus-circle icon"></i> <span class="text">Crear Formulario</span></button>
		</form>
	@endif
	</div>
</div>

@foreach($forms as $keyForm => $form)
<div class="card collapsed-card card-2">
	<div class="card-header">
		<h4><i class="fab fa-wpforms"></i> Formulario para: {{$keyForm}}</h4>
		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-widget="collapse">
				<i class="fas fa-plus"></i>
			</button>
		</div>
	</div>
	<div class="card-body">
		<table class="table myTable table-striped table-bordered an-table">
			<thead>
				<tr>
					<th>#</th>
					<th>Nombre</th>
					<th>Vista en el Formulario</th>
					<th>Tipo de campo</th>
				</tr>
			</thead>
			<tbody>
				@foreach($form as $keyAttr => $attribute)
				{{--dd($attribute)--}}
					<tr>
						<td>{{$loop->iteration}}</td>
						<td>{{$attribute->announcementAttribute->name}}</td>
						<td>{{$attribute->announcementAttribute->form_name}}</td>
						<td>{{$attribute->announcementAttribute->attr_view}}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="card-footer text-muted">
		<div class="row">
			<div class="col-md-6 text-right">
				<a class="btn btn-success card-1 col" href="{{route('announcement_forms.edit',$form[key($form)]->announcement_type_id)}}"><i class="far fa-edit"></i> Editar</a>
			</div>
			<div class="col-md-6">
				<form class="form" action="{{ route('announcement_forms.destroy' , $form[key($form)]->announcement_type_id)}}" method="POST">
					<div class="form-row justify-content-end">
						<input name="_method" type="hidden" value="DELETE">
						{{ csrf_field() }}
						<button data-toggle="tooltip" title="Eliminar" type="submit" class="btn btn-danger card-1 col">Eliminar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endforeach

@endsection
@section('mainScripts')
<script>
	$(document).ready( function () {
    	$('.myTable').DataTable({
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			},
		});
	});
</script>
@endsection