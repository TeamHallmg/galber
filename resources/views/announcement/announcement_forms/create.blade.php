@extends('announcement.app')

@section('content')

<div class="container mt-5">
	<div class="card card-2">
		<div class="card-header">
			<h1>Crear Formulario</h1>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-12 col-md-12">
					<form method="POST" action="{{route('announcement_forms.store')}}">
							@method('POST')
							@csrf
							<div class="form-group col-12 col-md-12">
								<label class="an-title font-weight-bold" for="type">Tipo de Anuncio</label>
								<select class="form-control" name="type" id="type" required>
										<option value="">Elije un tipo de anuncio...</option>
									@foreach($types as $key => $type)
										<option value="{{$key}}">{{$type}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group col-12 col-md-12 my-5">
								<table class="table table-bordered table-striped">
									<thead>
										<tr class="text-center">
											<th>Atributos</th>
											<th>Obligatorio</th>
										</tr>
									</thead>
									<tbody>
										@foreach($attributes as $attr)
										<tr>
											<td>
												<input class="mr-5" type="checkbox" name="attr_list[]" id="attr_list[]" value="{{$attr->id}}" autocomplete="off">
												<label class="">{{$attr->name}}</label>
											</td>
											<td align="center">
												<input type="checkbox" name="attr_list_req[]" id="attr_list_req[]" value="{{$attr->id}}" autocomplete="off">
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							<div class="form-group col-12 col-md-12">
								<button type="submit" class="btn btn-success card-1">Crear Formulario</button>
							</div>
					</form>
				</div>
			</div>
		</div>
	</div>


</div>

@endsection