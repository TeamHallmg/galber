<div class="form-group">
	<label for="{{$attr->form_name}}" class="{{$required == 'required'?'requerido':''}}">{{$attr->name}}</label>
	<textarea class="form-control sn-component" id="{{$attr->form_name}}" name="{{$attr->form_name}}" value="{{$data}}" type="{{$attr->attr_view}}" {{$required}}>{{$data}}</textarea>
</div>