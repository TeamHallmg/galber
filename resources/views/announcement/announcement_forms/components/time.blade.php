<div class="form-group">
    <label for="{{$attr->form_name}}" class="{{$required == 'required'?'requerido':''}}">{{$attr->name}}</label>
    <input class="form-control" name="{{$attr->form_name}}" type="{{$attr->attr_view}}" value="{{$data}}" {{$required}}>
</div>