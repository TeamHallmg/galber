@extends('announcement.app')

@section('content')

@inject('TPV', 'App\Models\Announcement\AnnouncementTypePerView')

<div class="card">
	<div class="card-header">
		<h1 class="font-weight-bold">Anuncios Activos por Vista</h1>
	</div>
	<div class="card-body">
			<form method="POST" action="{{ route('announcement_types_per_view.store')}}">
					{!! csrf_field() !!}
					<div class="table-responsive">
						<table class="table table-bordered table-striped w-100" id="myTable">
							<thead>
								<tr>
									<th>Pagina</th>
									@foreach($types as $type)
										<th>{{ $type->name }}</th>
									@endforeach
								</tr>
							</thead>
							<tbody>
								@foreach($views as $view)
									<tr>
										<td>{{ $view->name }}</td>
										@foreach($types as $type)
											<th>
												<input type="checkbox" name="{{ $view->id }}[]" value="{{ $type->id }}" {{($TPV::check($view->id, $type->id))?"checked":""}}>
											</th>
										@endforeach
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="form-group col-12 col-md-12 my-4">
						<button type="submit" class="btn btn-success card-1">Guardar Cambios</button>
					</div>
				</form>
	</div>
	<div class="card-footer text-muted">
	</div>
</div>
@endsection
@section('mainScripts')
<script>
	$(document).ready( function () {
    	$('#myTable').DataTable({
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			},
		});
	});
</script>
@endsection