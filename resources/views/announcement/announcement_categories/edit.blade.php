@extends('layouts.app')

@section('title', 'Categorías')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-12">
		<div class="col-md-12">
			<div>
				<form action="{{ route('announcement_categories.update', $category->id) }}" method="post" enctype="multipart/form-data">
					@csrf
					@method('PUT')
					<div class="form-group">
						<label for="name">Nombre</label>
						<input class="form-control" type="text" name="name" id="name" value="{{ old('name', $category->name) }}" required>
					</div>

					<div class="form-group">
						<label for="description">Descripción</label>
						<textarea name="description" id="description" rows="5" class="form-control" required>{{ old('description', $category->description) }}</textarea>
					</div>

					<div class="form-group">
						<label for="image">Imagen</label>
						<input id="image" class="form-control" type="file" name="image">
					</div>

					<div class="form-group">
						<label for="announcement_type_id">Tipo de anuncio</label>
						<select name="announcement_type_id" id="announcement_type_id" class="form-control selectpicker">
							@foreach ($types as $id => $type)
								<option value="{{ $id }}">{{ $type }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-success">Actualizar</button>
						<a href="{{ route('announcement_categories.index') }}" class="btn btn-primary">Regresar</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection