@extends('layouts.app')

@section('title', 'Categorías')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-12">
		<div class="col-md-12">
			<div>
				<table class="table table-striped">
					<tr>
						<th>#</th>
						<th>Titulo</th>
						<th>Descripción</th>
						<th>Imagen</th>
						<th>Tipo</th>
						<th>Acciones</th>
					</tr>
					@if (count($categories) == 0)
						<tr class="text-center">
							<td colspan="5">No hay categorias</td>
						</tr>
					@else
						@foreach ($categories as $category)
							<tr>
								<td>{{ $category->id }}</td>
								<td>{{ $category->name }}</td>
								<td>{!! $category->description !!}</td>
								<td>{{ $category->announcement_type->name ?? '-' }}</td>
								<td>
									<img width="120" src="{{ asset('/img/announcements/categories/' . $category->image) }}" alt="{{ $category->image }}">
								</td>
								<td width="180">
									<a class="btn btn-primary" href="{{ url('announcement_categories/' . $category->id . '/edit') }}">Editar</a>
								</td>
							</tr>
						@endforeach
					@endif
				</table>
			</div>
			<div class="text-center">
				<a class="btn btn-primary" href="{{ url('convenios') }}">Regresar</a>
				<a class="btn btn-success" href="{{ url('announcement_categories/create') }}">Agregar</a>
			</div>
		</div>
		@if (! is_null($categories))
			{{-- {!! $categories->render() !!} --}}
		@endif
	</div>
</div>
@endsection