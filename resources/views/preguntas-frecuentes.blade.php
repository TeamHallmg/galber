@extends('layouts.app')

@section('content')

<div class="card border-0">
    <div class="card-body">
        <div class="p-2">
            <div class="row mb-4">
                <div class="col bg-blue rounded-xl pt-3 py-1 px-5 mx-3">
                    <p class="text-white font-weight-bold font-italic announcement-title">
                        <i class="fas fa-question-circle"></i> Preguntas Frecuentes
                    </p>
                </div>
            </div>

            @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner']])

            <div class="card my-3">
                <div class="card-body">
                    @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['simple_list']])  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection