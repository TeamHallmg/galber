@extends('layouts.app_ext')

@section('content')

<div class="container">
	<div class="row mt-3">
		{{-- <p>
			Grupo AKRON te agadece tu participación responsable, ética y honesta, muestra de responsabilidad y compromiso moral con la empresa.
			Te recordamos que la información proporcionada es estrictamente confidencial y será atendida por el Comité de Ética. Asi mismo, te invitamos a que ante cualquier duda referente a una posible 
			irregularidad o evento que afecte a Grupo Akron sigas utilizando nuestra herramienta "Línea de contacto".
		</p> --}}
		<div>
			<p class="text-justify">
				Gracias por usar este servicio, enseguida se te proporciona un numero de folio con el cual se le dará seguimiento a tu asunto.
			</p>
		</div>
	</div>
	<div class="row mt-3">
		<p class="text-justify mt-2 mr-4">No. de Folio:</p>
		<p class="border border-primary p-2">{{ $denuncia->id }}</p>
	</div>
	
	<div class="row border border-top my-3"></div>

	<div class="row d-flex justify-content-center">
		<a href="{{ url('/') }}" class="btn btn-info text-white mx-3">Regresar</a>
	</div>
</div>		

@endsection