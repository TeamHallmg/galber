@extends('layout')

@section('title', 'Linea de contacto interna')

@section('content')
<!-- We just give good looking to the image, text and we link the button to the denuncia form-->
<div class="row margin-top-20">
	<div class="col-md-6 col-md-offset-3">
		<img class="img-responsive" src="img/denuncia/main/banner-linea-contacto-2.png" alt="linea de contacto">
	</div>
	<div class="row">
		<div class="col-md-10 col-md-offset-1 text-denuncia-interna">
		<p>
		La Línea de Contacto es un mecanismo formal implementado que está abierto para informar sobre las violaciones al
		Código de Ética y Conducta o conductas inapropiadas detectadas en la empresa.
		</p>
		<p>
		Con base en lo establecido por la Política y Procedimiento de la línea de contacto, ésta es aplicable a todos los
		Colaboradores de Grupo Akron y todos sus grupos de interés.
		</p>
		<p>
		La línea está orientada a reportar denuncias como: hechos delictivos incurridos o por incurrir, fraudes,
		inconformidades, acoso, hostigamiento, maltrato, conflictos de interés, inadecuado clima laboral, robo, sugerencias, faltas al código
		de ética y/o reglamento interior de trabajo; es decir, todos aquellos asuntos en los que Grupo Akron pueda corregir, eliminar y/o mejorar sus procesos, la relación entre su personal y/o
		actividades.
		</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-md-offset-2 text2-denuncia-interna">
			<p>
				Te invitamos a ingresar al formulario de <span class="Color-Contacto-Word"> Línea de Contacto </span> dando clic en el siguiente botón.
			</p>
		</div>


		@if (Auth::user()->role == 'manager' || Auth::user()->role == 'adminlinea')
		<div class="col-md-4 col-md-offset-4 margin-top-20">
			<a  href="{{ url('denuncia-interna-admin') }}" >
				<img class="img-responsive" src="img/denuncia/main/boton-administrar.png" alt="linea de contacto">
			</a>
		</div>
		@else
		<div class="col-md-4 col-md-offset-4 margin-top-20">
			<a  href="{{ url('denuncia-form-interna') }}" >
				<img class="img-responsive" src="img/denuncia/main/boton-ingresar.png" alt="linea de contacto">
			</a>
		</div>
@endif





	</div>
</div>



@endsection
