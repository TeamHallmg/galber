@extends('layouts.app')

@section('content')

@if(Session::has('alert-danger'))
	<p class="alert alert-danger">{!! Session::get('alert-danger') !!} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
@endif

<div class="card shadow p-3 mb-5 bg-white rounded">
	<h3 class="card-title text-blue"><strong>Gestión de Mejora</strong></h3>
		<div class="card-body">
			<div class="row d-flex justify-content-between">
				<div class="form-group col-md-3">
					<label for="fecha_hechos">Fecha solicitud:</label>
					<input type="date" class="form-control" name="fecha_hechos" id="fecha_hechos" value="{{ $denuncia->fecha_hechos }}" readonly>
                </div>
				<div class="form-group col-md-3">
					<label for="folio">Folio:</label>
					<input type="text" class="form-control text-right" name="folio" id="folio" value="{{ $denuncia->id }}" readonly>
                </div>
            </div>
            
            <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="nombre_externo">Nombre</label>
						<input id="nombre_externo" class="form-control" type="text" name="nombre_externo" value="{{ $denuncia->nombre_externo }}" readonly>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="puesto_externo">Puesto</label>
						<input id="puesto_externo" class="form-control" type="text" name="puesto_externo" value="{{ $denuncia->puesto_externo }}" readonly>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="ciudad_externo">Ciudad</label>
						<input id="ciudad_externo" class="form-control" type="text" name="ciudad_externo" value="{{ $denuncia->ciudad_externo }}" readonly>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="email_externo">Correo</label>
						<input id="email_externo" class="form-control" type="text" name="email_externo" value="{{ $denuncia->email_externo }}" readonly>
					</div>
				</div>
			</div>
			<hr class="border border-bottom border-dark">

			<div class="row">
				<div class="form-group col-md-12">
					<label for="denuncia">Redactar Escenario</label>
					<textarea id="denuncia" class="form-control" name="denuncia" rows="5" style="resize: none;" readonly>{{ $denuncia->denuncia }}</textarea>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-6">
					<label for="afectan_a">Estos hechos afectan a:</label>
					<select id="afectan_a" class="form-control" name="afectan_a" disabled>
						<option value="Procedimiento" {{ $denuncia->afectan_a == 'Procedimiento' ? 'selected' : '' }}>Procedimiento</option>
						<option value="Proceso" {{ $denuncia->afectan_a == 'Proceso' ? 'selected' : '' }}>Proceso / Actividad</option>
						<option value="Formato" {{ $denuncia->afectan_a == 'Formato' ? 'selected' : '' }}>Formato</option>
						<option value="Perfil" {{ $denuncia->afectan_a == 'Perfil' ? 'selected' : '' }}>Perfil de puestos</option>
						<option value="Instrucciones" {{ $denuncia->afectan_a == 'Instrucciones' ? 'selected' : '' }}>Instrucciones de trabajo</option>
						<option value="Infraestructura" {{ $denuncia->afectan_a == 'Infraestructura' ? 'selected' : '' }}>Infraestructura (mobiliario, equipo de trabajo, software, maq...)</option>
					</select>
				</div>
				<div class="form-group col-md-6">
					<label for="afectan_a_categoria">Categoría correspondiente para esta propuesta:</label>
					<select id="afectan_a_categoria" class="form-control" name="afectan_a_categoria" disabled>
						<option value="Almacen" {{ $denuncia->afectan_a_categoria == 'Almacen' ? 'selected' : '' }}>Almacén</option>
						<option value="Ventas" {{ $denuncia->afectan_a_categoria == 'Ventas' ? 'selected' : '' }}>Ventas</option>
						<option value="Logistica" {{ $denuncia->afectan_a_categoria == 'Logistica' ? 'selected' : '' }}>Logística</option>
						<option value="Atencion" {{ $denuncia->afectan_a_categoria == 'Atencion' ? 'selected' : '' }}>Atención a cliente</option>
						<option value="Finanzas" {{ $denuncia->afectan_a_categoria == 'Finanzas' ? 'selected' : '' }}>Finanzas</option>
						<option value="Capital" {{ $denuncia->afectan_a_categoria == 'Capital' ? 'selected' : '' }}>Capital humano</option>
						<option value="Tecnologias" {{ $denuncia->afectan_a_categoria == 'Tecnologias' ? 'selected' : '' }}>Tecnologías de la información</option>
						<option value="Credito" {{ $denuncia->afectan_a_categoria == 'Credito' ? 'selected' : '' }}>Crédito y cobranza</option>
						<option value="Marketing" {{ $denuncia->afectan_a_categoria == 'Marketing' ? 'selected' : '' }}>Marketing</option>
					</select>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-12">
					<label for="debe_ser">¿Cómo dice que debe ser?</label>
					<textarea id="debe_ser" class="form-control" name="debe_ser" rows="5" style="resize: none;" readonly>{{ $denuncia->debe_ser }}</textarea>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-12">
					<label for="como_se_hace">¿Cómo se hace actualmente?</label>
					<textarea id="como_se_hace" class="form-control" name="como_se_hace" rows="5" style="resize: none;" readonly>{{ $denuncia->como_se_hace }}</textarea>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-12">
					<label for="que_sugiere">¿Qué sugiere para mejorar?</label>
					<textarea id="que_sugiere" class="form-control" name="que_sugiere" rows="5" style="resize: none;" readonly>{{ $denuncia->que_sugiere }}</textarea>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-12">
					<label for="otros">Describa el beneficio en tiempo, ahorro, calidad, ventas u otro:</label>
					<textarea id="otros" class="form-control" name="otros" rows="5" style="resize: none;" readonly>{{ $denuncia->otros }}</textarea>
				</div>
			</div>

			Documentos o archivos que respalden la queja/sugerencia (en caso de contar con fotografía o cualquier archivo electrónico).
            
            <div class="row col-md-12">
                <p class="w-100">Imágenes</p>
                <div class="row">
                    <div class="col-md-12">
                        @foreach($denuncia->images as $img) 
                            @if ($img->files_id == $denuncia->id)
                                <a href="{{ asset('/uploads/denuncias/'.$img->url()) }}" target="_blank">
                                    <img src="{{ asset('/uploads/denuncias/'.$img->url()) }}" class="img-fluid w-25 mx-2">
                                </a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row col-md-12">
                <p class="w-100">Documentos</p>
                <div class="row">
                    <div class="col-md-12">
                        @foreach($denuncia->documents as $doc)
                            @if ($doc->files_id == $denuncia->id)
                                <a href="{{ asset('/uploads/denuncias/'.$doc->url()) }}" class="btn btn-secondary mx-2" target="_blank">{{ $doc->name }}</a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>

            <hr class="border border-bottom border-dark">
            <h3 class="card-title text-blue"><strong>Gestión de Mejora</strong></h3>
        <form action="{{ url('denuncias/'.$denuncia->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-7">
                    <label for="importancia">Tipo de Sugerencia de Mejora</label>
                    <div class="form-group d-flex justify-content-start">
						<div class="btn-group btn-group-toggle w-25 px-3" data-toggle="buttons">
                            <label class="btn btn-secondary mejora">
                                <input type="radio" id="mejora" name="tipo" value="mejora" {{ $denuncia->tipo == 'mejora' ? 'checked' : '' }}> Mejora
                            </label>
                        </div>
                        <div class="btn-group btn-group-toggle w-25 px-3" data-toggle="buttons">
                            <label class="btn btn-secondary queja">
                                <input type="radio" id="queja" name="tipo" value="queja" {{ $denuncia->tipo == 'queja' ? 'checked' : '' }}> Queja
                            </label>
                        </div>
					</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <label for="estatus">Estatus</label>
                    <div class="form-group d-flex justify-content-between">
                        <div class="btn-group btn-group-toggle w-25 px-3" data-toggle="buttons">
                            <label class="btn btn-danger sin_procesar">
                                <input type="radio" id="sin_procesar" name="estatus" value="sin_procesar" {{ ($denuncia->estatus=="sin_procesar" || empty($denuncia->estatus)) ? "checked" : "" }}> Sin Procesar
                            </label>
                        </div>
                        <div class="btn-group btn-group-toggle w-25 px-3" data-toggle="buttons">
                            <label class="btn btn-primary en_proceso">
                                <input type="radio" id="en_proceso" name="estatus" value="en_proceso" {{ ($denuncia->estatus=="en_proceso") ? "checked" : "" }}> En Proceso
                            </label>
                        </div>
                        <div class="btn-group btn-group-toggle w-25 px-3" data-toggle="buttons">
                            <label class="btn btn-success procesada">
                                <input type="radio" id="procesada" name="estatus" value="procesada" {{ ($denuncia->estatus=="procesada") ? "checked" : "" }}> Procesada
                            </label>
                        </div>
                        <div class="btn-group btn-group-toggle w-25 px-3" data-toggle="buttons">
                            <label class="btn btn-warning cerrada">
                                <input type="radio" id="cerrada" name="estatus" value="cerrada" {{ ($denuncia->estatus=="cerrada") ? "checked" : "" }}> Cerrada
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
				<div class="col-md-7">
                    <label for="importancia">Importancia</label>
                    <div class="form-group d-flex justify-content-between">
						<div class="btn-group btn-group-toggle w-25 px-3" data-toggle="buttons">
                            <label class="btn btn-dark no_asignado">
                                <input type="radio" id="no_asignado" name="importancia" value="no_asignado" {{ ($denuncia->importancia=="no_asignado" || empty($denuncia->importancia)) ? "checked" : "" }}> No Asignado
                            </label>
                        </div>
                        <div class="btn-group btn-group-toggle w-25 px-3" data-toggle="buttons">
                            <label class="btn btn-danger critico">
                                <input type="radio" id="critico" name="importancia" value="critico" {{ ($denuncia->importancia=="critico") ? "checked" : "" }}> Crítico
                            </label>
                        </div>
                        <div class="btn-group btn-group-toggle w-25 px-3" data-toggle="buttons">
                            <label class="btn btn-warning moderado">
                                <input type="radio" id="moderado" name="importancia" value="moderado" {{ ($denuncia->importancia=="moderado") ? "checked" : "" }}> Moderado
                            </label>
                        </div>
                        <div class="btn-group btn-group-toggle w-25 px-3" data-toggle="buttons">
                            <label class="btn btn-primary bajo">
                                <input type="radio" id="bajo" name="importancia" value="bajo" {{ ($denuncia->importancia=="bajo") ? "checked" : "" }}> Bajo
                            </label>
                        </div>
					</div>
                </div>
            </div>
            <div class="row">
				<div class="form-group col-md-12">
					<label for="comentarios">Comentarios</label>
					<textarea id="comentarios" class="form-control comentarios" name="comentarios" rows="5" style="resize: none;">{{ $denuncia->comentarios }}</textarea>
				</div>
            </div>
            <div class="row">
				<div class="form-group col-md-12">
                    <label for="bitacora">Bitácora</label>
<textarea id="bitacora" class="form-control bitacora" name="bitacora" rows="10" style="resize: none;" readonly>
@foreach ($denuncia->bitacora as $bitacora)
{{ $bitacora->comentarios }}
@endforeach
</textarea>
				</div>
            </div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="responsable">Responsable</label>
                        <select id="responsable" class="form-control selectpicker border border-1" name="responsable_id" data-live-search="true">
                            <option value="" disabled selected hidden>Selecciona un responsable...</option>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}" {{ $user->id == $denuncia->responsable_id ? 'selected' : '' }}>{{ $user->FullName }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="fecha_seguimiento">Fecha de seguimiento</label>
                        <input id="fecha_seguimiento" class="form-control" type="date" name="fecha_seguimiento" value="{{ $denuncia->formatoFecha($denuncia->fecha_seguimiento) == '' ? date('Y-m-d') : $denuncia->fecha_seguimiento }}" readonly>
                    </div>
                </div>
            </div>
        </div>
		<div class="card-footer text-center">
			<a href="{{ url('denuncias_index') }}" class="btn btn-info text-white mx-3">Regresar</a>
			<button class="btn btn-success mx-3" type="submit" id="more">Actualizar</button>
		</div>
	</form>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(function() {
        //funciones para los check de mejora y queja
        if($('#mejora').is(':checked')) {
            $('.mejora').removeClass('btn btn-secondary').addClass('btn bg-blue text-white');
        } else if($('#queja').is(':checked')) {
            $('.queja').removeClass('btn btn-secondary').addClass('btn bg-blue text-white');
        } //else {}

        $('.mejora').on('click', function(){
            $('.mejora').removeClass('btn btn-secondary').addClass('btn bg-blue text-white');
            $('.queja').removeClass('btn bg-blue').addClass('btn btn-secondary');
        });
        $('#mejora').on('click', function(e){
            $.ajax({
                type: "POST",
                url: "{{ url('denunciasBitacora') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: '{{ $denuncia->id }}',
                    accion: 'mejora',
                    tipo: true
                },
                success: function (response) {
                    // console.log(response);
                    if(response!=undefined) {
$('.bitacora').append("\n"+response.comentarios);
                    }
                }
            });
        });
		$('.queja').on('click', function(){
            $('.queja').removeClass('btn btn-secondary').addClass('btn bg-blue text-white');
            $('.mejora').removeClass('btn bg-blue').addClass('btn btn-secondary');
        });
        $('#queja').on('click', function(e){
            $.ajax({
                type: "POST",
                url: "{{ url('denunciasBitacora') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: '{{ $denuncia->id }}',
                    accion: 'queja',
                    tipo: true
                },
                success: function (response) {
                    // console.log(response);
                    if(response!=undefined) {
$('.bitacora').append("\n"+response.comentarios);
                    }
                }
            });
        });


		// funciones de botones para el estatus de la denuncia
        if($('#sin_procesar').is(':checked')) {
            $('.en_proceso').removeClass('btn btn-primary').addClass('btn btn-secondary');
            $('.procesada').removeClass('btn btn-success').addClass('btn btn-secondary');
            $('.cerrada').removeClass('btn btn-warning').addClass('btn btn-secondary');
        }else if($('#en_proceso').is(':checked')){
            $('.sin_procesar').removeClass('btn btn-danger').addClass('btn btn-secondary');
            $('.procesada').removeClass('btn btn-success').addClass('btn btn-secondary');
            $('.cerrada').removeClass('btn btn-warning').addClass('btn btn-secondary');
        }else if($('#procesada').is(':checked')){
            $('.sin_procesar').removeClass('btn btn-danger').addClass('btn btn-secondary');
            $('.en_proceso').removeClass('btn btn-primary').addClass('btn btn-secondary');
            $('.cerrada').removeClass('btn btn-warning').addClass('btn btn-secondary');
        }else{
            $('.sin_procesar').removeClass('btn btn-danger').addClass('btn btn-secondary');
            $('.en_proceso').removeClass('btn btn-primary').addClass('btn btn-secondary');
            $('.procesada').removeClass('btn btn-success').addClass('btn btn-secondary');
        }

        $('.sin_procesar').on('click', function(){
            $('.sin_procesar').removeClass('btn btn-secondary').addClass('btn btn-danger');
            $('.en_proceso').removeClass('btn btn-primary').addClass('btn btn-secondary');
            $('.procesada').removeClass('btn btn-success').addClass('btn btn-secondary');
            $('.cerrada').removeClass('btn btn-warning').addClass('btn btn-secondary');
        });
        $('#sin_procesar').on('click', function(e){
            $.ajax({
                type: "POST",
                url: "{{ url('denunciasBitacora') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: '{{ $denuncia->id }}',
                    accion: 'Sin Procesar',
                },
                success: function (response) {
                    // console.log(response);
                    if(response!=undefined) {
$('.bitacora').append("\n"+response.comentarios);
                    }
                }
            });
        });
        $('.en_proceso').on('click', function(e){
            $('.sin_procesar').removeClass('btn btn-danger').addClass('btn btn-secondary');
            $('.en_proceso').removeClass('btn btn-secondary').addClass('btn btn-primary');
            $('.procesada').removeClass('btn btn-success').addClass('btn btn-secondary');
            $('.cerrada').removeClass('btn btn-warning').addClass('btn btn-secondary');
        });
        $('#en_proceso').on('click', function(e){
            $.ajax({
                type: "POST",
                url: "{{ url('denunciasBitacora') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: '{{ $denuncia->id }}',
                    accion: 'En Proceso',
                },
                success: function (response) {
                    // console.log(response);
                    if(response!=undefined) {
$('.bitacora').append("\n"+response.comentarios);
                    }
                }
            });
        });
        $('.procesada').on('click', function(){
            $('.sin_procesar').removeClass('btn btn-danger').addClass('btn btn-secondary');
            $('.en_proceso').removeClass('btn btn-primary').addClass('btn btn-secondary');
            $('.procesada').removeClass('btn btn-secondary').addClass('btn btn-success');
            $('.cerrada').removeClass('btn btn-warning').addClass('btn btn-secondary'); 
        });
        $('#procesada').on('click', function(e){
            $.ajax({
                type: "POST",
                url: "{{ url('denunciasBitacora') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: '{{ $denuncia->id }}',
                    accion: 'Procesada',
                },
                success: function (response) {
                    // console.log(response);
                    if(response!=undefined) {
$('.bitacora').append("\n"+response.comentarios);
                    }
                }
            });
        });
        $('.cerrada').on('click', function(){
            $('.sin_procesar').removeClass('btn btn-danger').addClass('btn btn-secondary');
            $('.en_proceso').removeClass('btn btn-primary').addClass('btn btn-secondary');
            $('.procesada').removeClass('btn btn-success').addClass('btn btn-secondary');
            $('.cerrada').removeClass('btn btn-secondary').addClass('btn btn-warning');
        });
        $('#cerrada').on('click', function(e){
            $.ajax({
                type: "POST",
                url: "{{ url('denunciasBitacora') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: '{{ $denuncia->id }}',
                    accion: 'Cerrada',
                },
                success: function (response) {
                    // console.log(response);
                    if(response!=undefined) {
$('.bitacora').append("\n"+response.comentarios);
                    }
                }
            });
        });

		// funciones de botones para la importancia de la denuncia
		if($('#no_asignado').is(':checked')) {
            $('.critico').removeClass('btn btn-danger').addClass('btn btn-secondary');
            $('.moderado').removeClass('btn btn-warning').addClass('btn btn-secondary');
            $('.bajo').removeClass('btn btn-primary').addClass('btn btn-secondary');
        }else if($('#critico').is(':checked')){
            $('.no_asignado').removeClass('btn btn-dark').addClass('btn btn-secondary');
            $('.moderado').removeClass('btn btn-warning').addClass('btn btn-secondary');
            $('.bajo').removeClass('btn btn-primary').addClass('btn btn-secondary');
        }else if($('#moderado').is(':checked')){
            $('.no_asignado').removeClass('btn btn-dark').addClass('btn btn-secondary');
            $('.critico').removeClass('btn btn-danger').addClass('btn btn-secondary');
            $('.bajo').removeClass('btn btn-primary').addClass('btn btn-secondary');
        }else{
            $('.no_asignado').removeClass('btn btn-dark').addClass('btn btn-secondary');
            $('.critico').removeClass('btn btn-danger').addClass('btn btn-secondary');
            $('.moderado').removeClass('btn btn-warning').addClass('btn btn-secondary');
        }

        $('.no_asignado').on('click', function(){
            $('.no_asignado').removeClass('btn btn-secondary').addClass('btn btn-dark');
            $('.critico').removeClass('btn btn-danger').addClass('btn btn-secondary');
            $('.moderado').removeClass('btn btn-warning').addClass('btn btn-secondary');
            $('.bajo').removeClass('btn btn-primary').addClass('btn btn-secondary');
        });
        $('#no_asignado').on('click', function(e){
            $.ajax({
                type: "POST",
                url: "{{ url('denunciasBitacora') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: '{{ $denuncia->id }}',
                    accion: 'No Asignado',
                    importancia: true,
                },
                success: function (response) {
                    // console.log(response);
                    if(response!=undefined) {
$('.bitacora').append("\n"+response.comentarios);
                    }
                }
            });
        });
        $('.critico').on('click', function(){
            $('.no_asignado').removeClass('btn btn-dark').addClass('btn btn-secondary');
            $('.critico').removeClass('btn btn-secondary').addClass('btn btn-danger');
            $('.moderado').removeClass('btn btn-warning').addClass('btn btn-secondary');
            $('.bajo').removeClass('btn btn-primary').addClass('btn btn-secondary');
        });
        $('#critico').on('click', function(e){
            $.ajax({
                type: "POST",
                url: "{{ url('denunciasBitacora') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: '{{ $denuncia->id }}',
                    accion: 'No Asignado',
                    importancia: true,
                },
                success: function (response) {
                    // console.log(response);
                    if(response!=undefined) {
$('.bitacora').append("\n"+response.comentarios);
                    }
                }
            });
        });
        $('.moderado').on('click', function(){
            $('.no_asignado').removeClass('btn btn-dark').addClass('btn btn-secondary');
            $('.critico').removeClass('btn btn-danger').addClass('btn btn-secondary');
            $('.moderado').removeClass('btn btn-secondary').addClass('btn btn-warning');
            $('.bajo').removeClass('btn btn-primary').addClass('btn btn-secondary'); 
        });
        $('#moderado').on('click', function(e){
            $.ajax({
                type: "POST",
                url: "{{ url('denunciasBitacora') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: '{{ $denuncia->id }}',
                    accion: 'Moderado',
                    importancia: true,
                },
                success: function (response) {
                    // console.log(response);
                    if(response!=undefined) {
$('.bitacora').append("\n"+response.comentarios);
                    }
                }
            });
        });
        $('.bajo').on('click', function(){
            $('.no_asignado').removeClass('btn btn-dark').addClass('btn btn-secondary');
            $('.critico').removeClass('btn btn-danger').addClass('btn btn-secondary');
            $('.moderado').removeClass('btn btn-warning').addClass('btn btn-secondary');
            $('.bajo').removeClass('btn btn-secondary').addClass('btn btn-primary');
        });
        $('#bajo').on('click', function(e){
            $.ajax({
                type: "POST",
                url: "{{ url('denunciasBitacora') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: '{{ $denuncia->id }}',
                    accion: 'Bajo',
                    importancia: true,
                },
                success: function (response) {
                    // console.log(response);
                    if(response!=undefined) {
$('.bitacora').append("\n"+response.comentarios);
                    }
                }
            });
        });

        //accion para cuando cambia el responsable
        $('#responsable').on('change', function(e){
            var id = $(this).val();
            // console.log(id);
            $.ajax({
                type: "POST",
                url: "{{ url('denunciasBitacora') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: '{{ $denuncia->id }}',
                    accion: 'Cambio Responsable',
                    responsable_id: id,
                    responsable: true,
                },
                success: function (response) {
                    // console.log(response);
                    if(response!=undefined) {
$('.bitacora').append("\n"+response.comentarios);
                    }
                }
            });
        });
	});
</script>
@endsection