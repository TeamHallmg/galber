@extends($view)

@section('content')

@if(Session::has('alert-danger'))
	<p class="alert alert-danger">{!! Session::get('alert-danger') !!} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
@endif

@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner']])
@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $columna_anuncios])


<div class="card shadow p-3 mb-5 bg-white rounded">
	<h3 class="card-title text-blue"><strong>Sugerencia de Mejora</strong></h3>

	<form action="{{ url('denuncias') }}" method="post" enctype="multipart/form-data" id="form">
		@csrf
		<div class="card-body">
			<div class="row">
				<div class="form-group col-md-3">
					<label for="fecha_hechos">Fecha:</label>
					<input type="date" class="form-control" name="fecha_hechos" id="fecha_hechos" value="{{ date('Y-m-d') }}" readonly>
				</div>
			
				<div class="col-md-9 mt-auto">
                    {{-- <label for="importancia"></label> --}}
                    <div class="form-group d-flex justify-content-end">
						<div class="btn-group btn-group-toggle w-25 pr-3" data-toggle="buttons">
                            <label class="btn btn-secondary mejora">
                                <input type="radio" id="mejora" name="tipo" value="mejora" {{ old('tipo') == 'mejora' ? 'checked' : '' }} required> Mejora
                            </label>
                        </div>
                        <div class="btn-group btn-group-toggle w-25 pl-3" data-toggle="buttons">
                            <label class="btn btn-secondary queja">
                                <input type="radio" id="queja" name="tipo" value="queja" {{ old('tipo') == 'queja' ? 'checked' : '' }} required> Queja
                            </label>
                        </div>
					</div>
                </div>
            </div>
			<hr class="border border-bottom border-dark">

			{{-- <div class="row">
				<div class="form-group col-md-4">
					<label for="nombre_externo">Nombre: </label>
					<input id="nombre_externo" class="form-control" type="text" name="nombre_externo" value="{{ old('nombre_externo') }}">
				</div>
				<div class="form-group col-md-4">
					<label for="puesto_externo">Puesto: </label>
					<input id="puesto_externo" class="form-control" type="text" name="puesto_externo" value="{{ old('puesto_externo') }}">
				</div>
				<div class="form-group col-md-4">
					<label for="email_externo">Correo: </label>
					<input id="email_externo" class="form-control" type="text" name="email_externo" value="{{ old('email_externo') }}">
				</div>
			</div> --}}

			<div class="row">
				<div class="form-group col-md-12">
					<label for="denuncia">Redactar Escenario</label>
					<textarea id="denuncia" class="form-control" name="denuncia" rows="5" style="resize: none;">{{ old('denuncia') }}</textarea>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-6">
					<label for="afectan_a">Estos hechos afectan a:</label>
					<select id="afectan_a" class="form-control" name="afectan_a">
						<option value="Formato" {{ old('afectan_a') == 'Formato' ? 'selected' : '' }}>Formato</option>
						<option value="Infraestructura" {{ old('afectan_a') == 'Infraestructura' ? 'selected' : '' }}>Infraestructura (mobiliario, equipo de trabajo, software, maq...)</option>
						<option value="Instrucciones" {{ old('afectan_a') == 'Instrucciones' ? 'selected' : '' }}>Instrucciones de trabajo</option>
						<option value="Perfil" {{ old('afectan_a') == 'Perfil' ? 'selected' : '' }}>Perfil de puestos</option>
						<option value="Procedimiento" {{ old('afectan_a') == 'Procedimiento' ? 'selected' : '' }}>Procedimiento</option>
						<option value="Proceso" {{ old('afectan_a') == 'Proceso' ? 'selected' : '' }}>Proceso / Actividad</option>
					</select>
				</div>
				<div class="form-group col-md-6">
					<label for="afectan_a_categoria">Categoría correspondiente para esta propuesta:</label>
					<select id="afectan_a_categoria" class="form-control" name="afectan_a_categoria">
						<option value="Almacen" {{ old('afectan_a_categoria') == 'Almacen' ? 'selected' : '' }}>Almacén</option>
						<option value="Atencion" {{ old('afectan_a_categoria') == 'Atencion' ? 'selected' : '' }}>Atención a cliente</option>
						<option value="Capital" {{ old('afectan_a_categoria') == 'Capital' ? 'selected' : '' }}>Capital humano</option>
						<option value="Credito" {{ old('afectan_a_categoria') == 'Credito' ? 'selected' : '' }}>Crédito y cobranza</option>
						<option value="Finanzas" {{ old('afectan_a_categoria') == 'Finanzas' ? 'selected' : '' }}>Finanzas</option>
						<option value="Logistica" {{ old('afectan_a_categoria') == 'Logistica' ? 'selected' : '' }}>Logística</option>
						<option value="Marketing" {{ old('afectan_a_categoria') == 'Marketing' ? 'selected' : '' }}>Marketing</option>
						<option value="Tecnologias" {{ old('afectan_a_categoria') == 'Tecnologias' ? 'selected' : '' }}>Tecnologías de la información</option>
						<option value="Ventas" {{ old('afectan_a_categoria') == 'Ventas' ? 'selected' : '' }}>Ventas</option>
					</select>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-12">
					<label for="debe_ser">¿Cómo dice que debe ser?</label>
					<textarea id="debe_ser" class="form-control" name="debe_ser" rows="5" style="resize: none;">{{ old('debe_ser') }}</textarea>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-12">
					<label for="como_se_hace">¿Cómo se hace actualmente?</label>
					<textarea id="como_se_hace" class="form-control" name="como_se_hace" rows="5" style="resize: none;">{{ old('como_se_hace') }}</textarea>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-12">
					<label for="que_sugiere">¿Qué sugiere para mejorar?</label>
					<textarea id="que_sugiere" class="form-control" name="que_sugiere" rows="5" style="resize: none;">{{ old('que_sugiere') }}</textarea>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-12">
					<label for="otros">Describa el beneficio en tiempo, ahorro, calidad, ventas u otro:</label>
					<textarea id="otros" class="form-control" name="otros" rows="5" style="resize: none;">{{ old('otros') }}</textarea>
				</div>
			</div>

			Documentos o archivos que respalden la queja/sugerencia (en caso de contar con fotografía o cualquier archivo electrónico).
			{{-- <div class="form-group">
				<label for="imagenes">Subir archivo</label>
				<input id="imagenes" class="form-control-file" type="file" name="imagenes">
			</div> --}}
			<div class="form-group w-100">
				<label for="document">Images</label>
				<div class="dropzone" id="document-dropzone">
		
				</div>
			</div>

			<div class="form-group col-sm-12">
				<label for="file_documents">Documentos (sólo pdf)</label>
				<input type="file" name="file_documents[]" class="form-control" id="file_documents" accept="application/pdf" enctype="multipart/form-data" multiple>
			</div>

			<hr class="border border-bottom border-dark">

			<h3 class="card-title text-blue"><strong>Información no indispensable</strong></h3>
			<p class="mb-3 text-justify">Le sugerimos proporcionar los siguientes datos a fin de llevar a cabo una investigación más profunda, te recordamos que la información es de carácter confidencial</p>

			@auth
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="nombre_externo">Nombre</label>
							<input id="nombre_externo" class="form-control" type="text" name="nombre_externo" value="{{ auth()->user()->employee->FullName ?? '' }}" readonly>
							<input id="denunciante_id" type="hidden" name="denunciante_id" value="{{ auth()->user()->id }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="puesto_externo">Puesto</label>
							<input id="puesto_externo" class="form-control" type="text" name="puesto_externo" value="{{ auth()->user()->employee ? auth()->user()->employee->getPuestoName() : '' }}" readonly>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="ciudad_externo">Ciudad</label>
							<input id="ciudad_externo" class="form-control" type="text" name="ciudad_externo" value="{{ auth()->user()->employee->sucursal ?? '' }}" readonly>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="email_externo">Correo</label>
							<input id="email_externo" class="form-control" type="text" name="email_externo" value="{{ auth()->user()->employee->correoempresa ?? '' }}" readonly>
						</div>
					</div>
				</div>
			@else
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="nombre_externo">Nombre</label>
							<input id="nombre_externo" class="form-control" type="text" name="nombre_externo" value="{{ old('nombre_externo') }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="puesto_externo">Puesto</label>
							<input id="puesto_externo" class="form-control" type="text" name="puesto_externo" value="{{ old('puesto_externo') }}">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="ciudad_externo">Ciudad</label>
							<input id="ciudad_externo" class="form-control" type="text" name="ciudad_externo" value="{{ old('ciudad_externo') }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="email_externo">Correo</label>
							<input id="email_externo" class="form-control" type="email" name="email_externo" value="{{ old('email_externo') }}">
						</div>
					</div>
				</div>
			@endauth
		</div>

		<div class="card-footer text-center">
			<a href="{{ url('/') }}" class="btn btn-info text-white mx-3">Regresar</a>
			<button class="btn btn-success mx-3" type="submit" id="more">Enviar</button>
		</div>
	</form>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	var uploadedDocumentMap = {}
	Dropzone.options.documentDropzone = {
		url: '{{ route('denuncias.images') }}',
		maxFilesize: 2, // MB
		maxFiles: 5,
		dictDefaultMessage: "Arrastra los archivos aquí para subirlos",
		addRemoveLinks: true,
		acceptedFiles: ".jpeg,.jpg,.png,.JPEG,.JPG,.PNG",
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		},
		success: function (file, response) {
			$('#form').append('<input type="hidden" name="file_images[]" value="' + response.name + '">')
			uploadedDocumentMap[file.name] = response.name
		},
		removedfile: function (file) {
			file.previewElement.remove()
			var name = ''
			if (typeof file.file_name !== 'undefined') {
				name = file.file_name
			} else {
				name = uploadedDocumentMap[file.name]
			}
			$('#form').find('input[name="file_images[]"][value="' + name + '"]').remove()
		},
		init: function () {
			this.on("maxfilesexceeded", function(file) {
				alert("MAX_FILES_EXCEEDED");
			});
			@if(isset($project) && $project->file_images)
				var files = {!! json_encode($project->file_images) !!}
				for (var i in files) {
					var file = files[i]
					this.options.addedfile.call(this, file)
					file.previewElement.classList.add('dz-complete')
					$('#form').append('<input type="hidden" name="file_images[]" value="' + file.file_name + '">')
				}
			@endif
		}
	}
	$(function() {
		@if(old('tipo') == 'mejora')
			$('.mejora').removeClass('btn btn-secondary').addClass('btn bg-blue text-white');
			$('.queja').removeClass('btn bg-blue').addClass('btn btn-secondary');
		@elseif(old('tipo') == 'queja')
			$('.queja').removeClass('btn btn-secondary').addClass('btn bg-blue text-white');
			$('.mejora').removeClass('btn bg-blue').addClass('btn btn-secondary');
		@endif

		$('.mejora').on('click', function(){
            $('.mejora').removeClass('btn btn-secondary').addClass('btn bg-blue text-white');
            $('.queja').removeClass('btn bg-blue').addClass('btn btn-secondary');
        });
		$('.queja').on('click', function(){
            $('.queja').removeClass('btn btn-secondary').addClass('btn bg-blue text-white');
            $('.mejora').removeClass('btn bg-blue').addClass('btn btn-secondary');
        });
	});
</script>
@endsection