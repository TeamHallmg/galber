@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row mb-3">
        <div class="col-12 blue_header rounded py-3">
            <h2 class="font-italic font-weight-bold m-0"><i class="fa fa-headset"></i> Administrar Mejoras</h2>
        </div>
    </div>
    <table class="table" id="denuncias">
        <thead >
            <tr>
                <th>Folio</th>
                <th>Fecha de Creación</th>
                <th>Sugerencia/Mejora</th>
                <th>Fecha de Seguimiento</th>
                <th>Responsable</th>
                <th>Estatus</th>
                <th>Importancia</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($denuncias as $denuncia)
                <tr>
                    <td>{{ $denuncia->id }}</td>
                    <td>{{ $denuncia->formatoFecha($denuncia->fecha_hechos) }}</td>
                    <td>{{ $denuncia->denuncia }}</td>
                    <td>{{ $denuncia->formatoFecha($denuncia->fecha_seguimiento) }}</td>
                    <td>{{ ($denuncia->user) ? $denuncia->user->FullName : '' }}</td>
                    <td class="alert alert-{{ $estado[$loop->index] }} w-auto">{{ $txt_estado[$loop->index] }}</td>
                    <td class="alert alert-{{ $importancia[$loop->index] }} w-auto">{{ $txt_importancia[$loop->index] }}</td>
                    <td>
                        <a href="{{ url('denuncias_gestion/'.$denuncia->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="mt-5"></div>
    @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner']])
    @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $columna_anuncios])
</div>


@endsection

@section('scripts')
<script type="text/javascript">
	$(function() {
        // Setup - add a text input to each footer cell
        $('#denuncias thead tr').clone(true).appendTo( '#denuncias thead' );
        $('#denuncias thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            var vacio = '';
			if (i != 7 )
                $(this).html('<input type="text" class="form-control col-md-12" />');
            else
                $(this).html(vacio);
    
            $('input', this).on('keyup change', function () {
                if (table.column(i).search() !== this.value) {
                    table.column(i).search( this.value ).draw();
                }
            });
        });

        var table = $('#denuncias').DataTable( {
            responsive: true,
            orderCellsTop: true,
            // fixedHeader: true,
            language: {
                url: "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            order: [[ 0, "desc" ]]
        } );
	});
</script>
@endsection
