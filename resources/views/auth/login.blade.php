@extends('layouts.login')

@section('content')
<div class="container">
    <div class="row d-flex justify-content-center align-content-center">
        <div class="card card-size my-5 mb-5 text-white rounded-xl border-0 animated zoomIn card-5">
            <!--<img class="card-img" src="{{asset('/img/fondo logotipo.png')}}" alt="Card image">
            <div class="card-img-overlay d-flex justify-content-center align-content-center">-->
                <img class="img-fluid card-logo-login" src="{{asset('/img/logo login.png')}}" alt="">
            <!--</div>-->
            <div class="card-body card-body-login">
                @php
                    $moodle = config('app.moodle_login'); 
                @endphp

                @if($moodle)
                    <form method="POST" action="/moodle/login/index.php" aria-label="{{ __('Login') }}">
                @else
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                @endif
                    @csrf

                    <div class="form-group row">
                        @if($moodle)
                            <div class="col-md-12">
                                <input id="username" type="email" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" placeholder="Nombre de usuario" value="{{ old('username') }}" required autofocus>
                                @if ($errors->has('username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        @else
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Nombre de usuario" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        @endif
                    </div>

                    <div class="form-group row">
                        <!--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña:') }}</label>-->

                        <div class="col-md-12">
                            <input id="password" placeholder="Contraseña" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                            <div class="d-flex justify-content-end mt-2">
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">Recordar</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <div class="col text-center">
                            <button type="submit" class="btn btn-primary col-12 font-weight-bold card-1">
                                {{ __('Iniciar ') }}<i class="fas fa-sign-in-alt"></i>
                            </button>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <div class="col text-center">
                            <a class="btn btn-link text-white" href="{{ route('password.request') }}">
                                {{ __('¿Olvidaste tu contraseña?') }}
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

