
@extends('layouts.login')

@section('content')
    {{--@if (session('status'))
    <div id="message">
        <div class="alert alert-success alert-dismissible" role="alert">
            {{ session('status') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
    @endif--}}
    <div class="container">
    <div class="row d-flex justify-content-center align-items-center">
        <div class="card-container col-10 col-xs-10 col-sm-10 col-md-7 col-lg-6">
            <div class="card login-card animated zoomIn card-3">
                <div class="card-header card-header-log border-0 d-flex justify-content-center">
                    <img class="img-fluid" src="{{asset('/img/logo login.png')}}" alt="">
                </div>
                <div class="card-body card-body-login">
                    @if (session('status'))
                        <div class="row">
                            <div class="col">
                                <h5 class="text-white font-weight-bold">¡ÉXITO!</h5>
                                <p class="text-white">
                                    Se ha enviado un correo electrónico para restablecer su contraseña.
                                </p>
                                <p>
                                    Si el correo electrónico no llega pronto, revise su carpeta de spam(correo no deseado).
                                </p>
                                <div class="text-center">
                                    <a href="{{route('password.request')}}" type="button" class="text-white font-weight-bold text-decoration-none mb-4">
                                        <span>{{ __('Reenviar Correo') }}</span>
                                    </a>
                                </div>
                                <div class="text-center">
                                    <a href="{{url('login')}}" type="button" class="btn btn-primary font-weight-bold card-1">
                                        <span>{{ __('Regresar al Inicio') }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @else
                        <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                            @csrf

                            <div class="row">
                                <div class="col">
                                    <h5 class="text-white font-weight-bold">RECUPERA TU CONTRASEÑA</h5>
                                    <p class="text-white">
                                        ¿Tienes problemas para ingresar? Puedes recuperar tu contraseña utilizando el correo electrónico asociado a tu cuenta.
                                    </p>
                                    <p>
                                        Se enviará un mensaje de recuperación a tu correo.
                                    </p>
                                </div>
                            </div>
                            <div class="form-group row">

                                <div class="col-12 col-md-12">
                                    <input id="email" type="email" placeholder="@" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 d-flex justify-content-between">
                                    <a href="{{url('login')}}" type="button" class="d-flex align-items-center text-white font-weight-bold text-decoration-none">
                                        <span>{{ __('Regresar al Inicio') }}</span>
                                    </a>
                                    <button type="submit" class="btn btn-primary font-weight-bold card-1">
                                        {{ __('Recuperar') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('footer')
<footer class="footer mt-auto py-1 fixed-bottom footer-bg">
    <div class="container">
        <div class="d-flex justify-content-center">
            <div class="flex-column">
                <div class="p-1">
                    Grupo Guía {{ date('Y') }} &copy;
                </div>
            </div>
        </div>
    </div>
</footer>
@endsection
