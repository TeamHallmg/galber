@extends('layouts.app')

@section('content')
<div class="card card-2 border-0">
    <div class="card-body">
		<div class="container-fluid">
			<div class="row mb-4">
                <div class="col bg-blue rounded-xl pt-3 py-1 px-5">
                    <p class="text-white font-weight-bold font-italic announcement-title">Desarrollo Interno</p>
                </div>
			</div>
			
			<div class="row justify-content-center">
				@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner']])
			</div>

			<div class="row justify-content-center my-3">
				<div class="col-md-12 px-0">
					<h4 class="font-weight-bold my-4">Promociones</h4>
				</div>
				@forelse($promotions as $promotion)
					{{-- <div class="col-12 col-md-6 col-lg-4"> --}}
					{{-- <div class="col-12 col-md-12 col-lg-6 col-xl-4"> --}}
					<div class="col-auto mb-4" style="width: 480px;">
						<div class="row px-3">
							<div class="col-4 p-0 mb-3" style="background-image:url('{{asset($promotion->user->getProfileImage())}}');
								background-size:cover;
								background-position:center;">
							</div>
							{{-- <div class="col-12 col-sm-8 col-md-8 pl-0 pr-0 mb-3 bg-client-gray"> --}}
							<div class="col-8 pl-0 pr-0 mb-3 bg-client-gray">
								<p class="text-uppercase bg-blue text-white font-weight-bold pl-2"> {{isset($promotion->user->employee->nombre)?$promotion->user->employee->nombre:''}} {{isset($promotion->user->employee->paterno)?$promotion->user->employee->paterno:''}} {{isset($promotion->user->employee->materno)?$promotion->user->employee->materno:''}}</p>
								<span class="pl-2 d-flex">
									<p>
										<i class="fas fa-angle-double-up" style="color: #244395"></i>
										@if(isset($promotion->user->employee->jobPosition->name))
											{{ $promotion->user->employee->jobPosition->name }} <small class="text-muted"> ({{ $promotion->user->employee->jobPosition->area->department->name ?? 'Sin Departamento' }}) </small>
										@else
											-
										@endif
										<br>
										@if(isset($promotion->old_job_position->name))
											{{ $promotion->old_job_position->name }} <small class="text-muted"> ({{ $promotion->old_job_position->area->department->name ?? 'Sin Departamento' }}) </small>
										@else
											-
										@endif
										<br>
										{{ $promotion->user->employee->getSucursal() ?? 'N/A' }}<br>
										{{isset($promotion->user->employee->correoempresa)?$promotion->user->employee->correoempresa:'-'}}<br>
										@if($promotion->user->employee->telefono)
											Ofic. {{ $promotion->user->employee->telefono }}<br>
										@endif
										@if($promotion->user->employee->celular)
											Cel. {{ $promotion->user->employee->celular }}<br>
										@endif
										@if(!$promotion->user->employee->telefono)
											<br>
										@endif
										@if(!$promotion->user->employee->celular)
											<br>
										@endif
										{{-- <br> --}}
									</p>
								</span>
							</div>
						</div>
					</div>
				@empty
					<div class="col-12 col-md-12 col-lg-12 text-center">                    
						<h4 style="color: #343A40;"><strong>No hay promociones de personal registradas</strong></h4>
					</div>
				@endforelse
			</div>

			<div class="row">
				<div class="col-md-12 px-0">
					<hr class="scale-hr">
				</div>
			</div>

			<div class="row justify-content-center my-3">
				<div class="col-md-12 px-0">
					<h4 class="font-weight-bold my-4">Transferencias o cambios</h4>
				</div>
				@forelse($movements as $movement)
					{{-- <div class="col-12 col-sm-4 col-md-4 col-lg-4"> --}}
					{{-- <div class="col-12 col-md-12 col-lg-6 col-xl-4"> --}}
					<div class="col-auto mb-4" style="width: 480px;">
						<div class="row px-3">
							<div class="col-4 p-0 mb-3" style="background-image:url('{{asset($movement->user->getProfileImage())}}');
								background-size:cover;
								background-position:center;">
							</div>
							{{-- <div class="col-12 col-sm-8 col-md-8 pl-0 pr-0 mb-3 bg-client-gray"> --}}
							<div class="col-8 pl-0 pr-0 mb-3 bg-client-gray">
								<p class="text-uppercase bg-blue text-white font-weight-bold pl-2"> {{isset($movement->user->employee->nombre)?$movement->user->employee->nombre:''}} {{isset($movement->user->employee->paterno)?$movement->user->employee->paterno:''}} {{isset($movement->user->employee->materno)?$movement->user->employee->materno:''}}</p>
								<span class="pl-2 d-flex">
									<p>
										@if(isset($movement->old_job_position->name))
											{{ $movement->old_job_position->name }} <small class="text-muted"> ({{ $movement->old_job_position->area->department->name ?? 'Sin Departamento' }}) </small>
										@else
											-
										@endif
										<i class="fas fa-angle-double-right" style="color: #244395"></i> 
										@if(isset($movement->user->employee->jobPosition->name))
											{{ $movement->user->employee->jobPosition->name }} <small class="text-muted"> ({{ $movement->user->employee->jobPosition->area->department->name ?? 'Sin Departamento' }}) </small>
										@else
											-
										@endif
										<br>
										{{ $movement->user->employee->getSucursal() ?? 'N/A' }}<br>
										{{isset($movement->user->employee->correoempresa)?$movement->user->employee->correoempresa:'-'}}<br>
										@if($movement->user->employee->telefono)
											Ofic. {{ $movement->user->employee->telefono }}<br>
										@endif
										@if($movement->user->employee->celular)
											Cel. {{ $movement->user->employee->celular }}<br>
										@endif
										@if(!$movement->user->employee->telefono)
											<br>
										@endif
										@if(!$movement->user->employee->celular)
											<br>
										@endif
										{{-- <br> --}}
									</p>
								</span>
							</div>
						</div>
					</div>
				@empty
					<div class="col-12 col-md-12 col-lg-12 text-center">                 
						<h4 style="color: #343A40;"><strong>No hay movimientos de personal registrados</strong></h4>
					</div>
				@endforelse
			</div>

		</div>
    </div>
</div>
@endsection
@section('mainScripts')
<script type="text/javascript">
</script>
@endsection