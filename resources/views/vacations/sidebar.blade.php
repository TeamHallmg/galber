<h3 class="text-blue font-weight-bold font-italic mb-5 d-flex justify-content-around">
    <span>Gestión de Incidencias (vacaciones)</span>
    @if(Auth::user()->isAdminOrHasRolePermission('incidents_admin') || Auth::user()->hasPermissionOfModule('Incidencias'))
        <a data-toggle="collapse" href="#collapseVacationMenu" role="button" aria-expanded="false" aria-controls="collapseVacationMenu"><i class="fas fa-cog"></i></a>
    @endif
</h3>
<div class="collapse {{ is_url_of_hidden_menu(Route::currentRouteName())?'show':'' }}" id="collapseVacationMenu">
    @if(Auth::user()->hasAccess('admin_export_vacations') || Auth::user()->isAdminOrHasRolePermission('incidents_admin'))
        <a class="text-decoration-none" href="{{ route('admin.exports.index', 'vacations') }}">
            <h5 class="right-menu-element @if(request()->is('admin/export/vacations'))right-menu @endif">
                Exportar Vacaciones
            </h5>
        </a>
    @endif
    @if(Auth::user()->hasAccess('admin_export_incidents') || Auth::user()->isAdminOrHasRolePermission('incidents_admin'))
        <a class="text-decoration-none" href="{{ route('admin.exports.index', 'incidents') }}">
            <h5 class="right-menu-element @if(request()->is('admin/export/incidents'))right-menu @endif">
                Exportar Incidencias
            </h5>
        </a>
    @endif
    @if(Auth::user()->hasAccess('admin_balances_history') || Auth::user()->isAdminOrHasRolePermission('incidents_admin'))
    <a class="text-decoration-none" href="{{ route('admin.incidents.balances') }}">
        <h5 class="right-menu-element @if(request()->is('admin/balances')) right-menu @endif">
            Balances
        </h5>
    </a>
    @endif
    @if(Auth::user()->hasAccess('admin_balances_create') || Auth::user()->isAdminOrHasRolePermission('incidents_admin'))
    <a class="text-decoration-none" href="{{ route('admin.incidents.balances.create') }}">
        <h5 class="right-menu-element @if(request()->is('admin/balances/user')) right-menu @endif">
            Nuevo Balance Extraordinario
        </h5>
    </a>
    @endif
    @if(Auth::user()->hasAccess('admin_balances_history') || Auth::user()->isAdminOrHasRolePermission('incidents_admin'))
    <a class="text-decoration-none" href="{{ route('admin.view.employee_balances') }}">
        <h5 class="right-menu-element @if(request()->is('admin/view/balances*')) right-menu @endif">
            Resumen de Saldos
        </h5>
    </a>
    @endif
    @if(Auth::user()->hasAccess('admin_balances_create') || Auth::user()->isAdminOrHasRolePermission('incidents_admin'))
    <a class="text-decoration-none" href="{{ route('admin.incidents.balances.create') }}">
        <h5 class="right-menu-element @if(request()->is('admin/balances/user')) right-menu @endif">
            Nuevo Balance Extraordinario
        </h5>
    </a>
    @endif
    @if(Auth::user()->hasAccess('admin_clocklog_history') || Auth::user()->isAdminOrHasRolePermission('incidents_admin'))
        <a class="text-decoration-none" href="{{ route('admin.clocklog.history') }}">
            <h5 class="right-menu-element @if(request()->is('admin/clocklog/history')) right-menu @endif">
                Historico de Checadas
            </h5>
        </a>
    @endif
    @if(Auth::user()->hasAccess('admin_incidents_history') || Auth::user()->isAdminOrHasRolePermission('incidents_admin'))
        <a class="text-decoration-none" href="{{ route('admin.incidents.history') }}">
            <h5 class="right-menu-element @if(request()->is('admin/incidents/history')) right-menu @endif">
                Historico de Incidencias
            </h5>
        </a>
    @endif
    @if(Auth::user()->hasAccess('crud_events') || Auth::user()->isAdminOrHasRolePermission('incidents_admin'))
        <a class="text-decoration-none" href="{{ route('super.events.index') }}">
            <h5 class="right-menu-element @if(request()->is('super/events*')) right-menu @endif">
                Administrar Eventos
            </h5>
        </a>
    @endif
    @if(Auth::user()->hasAccess('admin_benefits') || Auth::user()->isAdminOrHasRolePermission('incidents_admin'))
        <a class="text-decoration-none" href="{{ route('admin.incidents.index') }}">
            <h5 class="right-menu-element @if(request()->is('admin/incidents*') && !request()->is('admin/incidents/history')) right-menu @endif">
                Administrar Incidencias
            </h5>
        </a>
    @endif
    @if(Auth::user()->hasAccess('admin_schedules') || Auth::user()->isAdminOrHasRolePermission('incidents_admin'))
        <a class="text-decoration-none" href="{{ route('admin.schedule.index') }}">
            <h5 class="right-menu-element @if(request()->is('admin/schedule*')) right-menu @endif">
                Administrar Horarios
            </h5>
        </a>
    @endif
    @if(Auth::user()->hasAccess('admin_benefits_from_job') || Auth::user()->isAdminOrHasRolePermission('incidents_admin'))
        <a class="text-decoration-none" href="{{ route('admin.benefitjobs.index') }}">
            <h5 class="right-menu-element @if(request()->is('admin/benefitjobs*')) right-menu @endif">
                Administrar Puesto - Beneficios
            </h5>
        </a>
    @endif
    @if(Auth::user()->hasAccess('admin_approve_requests') || Auth::user()->isAdminOrHasRolePermission('incidents_admin'))
        <a class="text-decoration-none" href="{{ route('admin.approve', 'requests') }}">
            <h5 class="right-menu-element @if(request()->is('admin/approve/requests')) right-menu @endif">
                Administrar Solicitudes
            </h5>
        </a>
    @endif
    <hr>
</div>
<a class="text-decoration-none" href="{{ route('common.plea.create') }}">
    <h5 class="right-menu-element @if(request()->is('common/plea/create')) right-menu @endif">
        Nueva solicitud
    </h5>
</a>
<a class="text-decoration-none" href="{{ route('common.plea.index') }}">
    <h5 class="right-menu-element @if(request()->is('common/plea')) right-menu @endif">
        Solicitudes Activas
    </h5>
</a>
<a class="text-decoration-none" href="{{ route('common.plea.inactive') }}">
    <h5 class="right-menu-element @if(request()->is('common/inactive')) right-menu @endif">
        Historial
    </h5>
</a>
<a class="text-decoration-none" href="{{ route('common.balances') }}">
    <h5 class="right-menu-element @if(request()->is('common/balances')) right-menu @endif">
        Mis Saldos
    </h5>
</a>
<a class="text-decoration-none" href="{{ route('common.clocklog.index') }}">
    <h5 class="right-menu-element @if(request()->is('common/clocklog')) right-menu @endif">
        Reloj Checador
    </h5>
</a>
@if(Auth::user()->isBoss())
<hr>
<a class="text-decoration-none" href="{{ route('super.approve', 'requests') }}">
    <h5 class="right-menu-element @if(request()->is('super/approve/requests')) right-menu @endif">
        Autorizar solicitudes
    </h5>
</a>
<a class="text-decoration-none" href="{{ route('super.user.incidents.index') }}">
    <h5 class="right-menu-element @if(request()->is('super/user/incidents*')) right-menu @endif">
        Crear Incidencias
    </h5>
</a>
<a class="text-decoration-none" href="{{ route('super.view.employee_balances') }}">
    <h5 class="right-menu-element @if(request()->is('super/view/balances*')) right-menu @endif">
        Saldos colaboradores
    </h5>
</a>
<a class="text-decoration-none" href="{{ route('super.view.requests') }}">
    <h5 class="right-menu-element @if(request()->is('super/view/requests')) right-menu @endif">
        Historial colaboradores
    </h5>
</a>
<a class="text-decoration-none" href="{{ route('super.view.schedule') }}">
    <h5 class="right-menu-element @if(request()->is('super/view/schedule*')) right-menu @endif">
        Agenda colaboradores
    </h5>
</a>
<a class="text-decoration-none" href="{{ route('super.clocklog.supervisor.today') }}">
    <h5 class="right-menu-element @if(request()->is('super/clocklog/today')) right-menu @endif">
        Checadas del Día
    </h5>
</a>
<a class="text-decoration-none" href="{{ route('super.clocklog.supervisor.review_problems') }}">
    <h5 class="right-menu-element @if(request()->is('super/clocklog/review_problems')) right-menu @endif">
        Ver Retardos / Salidas Temprano
    </h5>
</a>
<a class="text-decoration-none" href="{{ route('super.clocklog.supervisor.missing_logs') }}">
    <h5 class="right-menu-element @if(request()->is('super/clocklog/missing_logs')) right-menu @endif">
        Administrar faltas
    </h5>
</a>
<a class="text-decoration-none" href="{{ route('super.clocklog.supervisor.history') }}">
    <h5 class="right-menu-element @if(request()->is('super/clocklog/history')) right-menu @endif">
        Historial checadas
    </h5>
</a>
@endif