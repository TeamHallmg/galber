@extends('vacations.app')

@section('content')
<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Eventos</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Crear evento</h3>
    </div>
</div>
<form action="{{ route('super.events.update', $event->id) }}" method="POST">
    @csrf
    @method('PUT')
    <input type="hidden" name="type" value="global">
    <input type="hidden" name="status" value="global">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="title"> @lang('pages.ecf_title') </label>                        
                <input type="text" name="title" class="form-control" value="{{ $event->title }}" required>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label for="start">Fecha Inicio</label>
                        <input type="date" id="start" name="start" value="{{ $event->getStartDate('Y-m-d') }}" class="form-control" required>
                    </div>
                    <div class="col-md-6">
                        <label for="end">Fecha Final </label>
                        <input type="date" id="end" name="end" value="{{ $event->getStartDate('Y-m-d') }}" class="form-control" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="benefit_id"> @lang('pages.ecf_incident')</label>
                <input type="text" class="form-control" value="{{ $event->benefit->name }}" disabled>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="benefit_id"> Horarios</label>
                <input type="text" class="form-control" value="{{ $event->region->name }}" disabled>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="custom-control custom-checkbox ">
                    <input type="checkbox" name="blocked" value="1" class="custom-control-input" id="cbox_block_days" {{ $event->blocked?'checked':'' }}>
                    <label class="custom-control-label" for="cbox_block_days">Bloquear Dias</label>
                </div>
            </div>                    
        </div>                
    </div>
    <hr>
    
    <div class="row justify-content-center">
        <div class="col-md-4">
            <input type="submit" value="@lang('pages.create')" class="btn btn-block btn-primary">
        </div>
    </div>
</form>
@endsection
