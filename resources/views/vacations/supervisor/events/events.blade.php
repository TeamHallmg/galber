@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Eventos</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Eventos activos</h3>
    </div>
</div>

<div class="text-right mb-3">
    <a href="{{ route('super.events.create') }}" class="btn btn-success">Crear Evento</a>
</div>
<div class="table-responsive">
    <table id="eventsTable" class="table table-bordered">
        <thead class="blue_header">
            <tr>
                <th>Titulo</th>
                <th>Inicio</th>
                <th>Fin</th>
                <th>Bloquea</th>
                <th>Region</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($events as $event)
                <tr>
                    <td>{{ $event->title }}</td>
                    <td>{{ date('Y-m-d', $event->start) }}</td>
                    <td>{{ date('Y-m-d', $event->end) }}</td>
                    <td>{{ ($event->blocked)?'Sí':'No' }}</td>
                    <td>{{ $event->region->name }}</td>
                    <td>
                        <a href="{{ route('super.events.edit', $event->id) }}" class="btn btn-primary"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a>
                        <button type="button" id="{{ $event->id }}" class="btn btn-danger remove-event"><i class="fa fa-times-circle" aria-hidden="true"></i></button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@section('scripts')
<script>
    let table = $('#eventsTable').DataTable({
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			},
		});
    const deleteUrl = "{{ route('super.events.index') }}";
    $(function (){
        $('tbody').on('click', '.remove-event', function(){
            if(confirm('Estas seguro de borrar el evento?')){
                const btn = $(this);
                const event_id = btn.attr('id');
                axios.delete(deleteUrl + '/' + event_id)
                .then(function(response){
                    if(response.data.success){
                        btn.closest('tr').fadeOut();
                    }else{
                        alert(respose.data.error);
                    }
                })
                .catch(function(error){

                });
            }
        });
    });
</script>
@endsection