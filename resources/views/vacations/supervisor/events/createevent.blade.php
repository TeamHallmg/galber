@extends('vacations.app')

@section('content')
<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Eventos</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Crear evento</h3>
    </div>
</div>
<form action="{{ url('super/events') }}" method="POST">
    @csrf
    <input type="hidden" name="type" value="global">
    <input type="hidden" name="status" value="global">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="title"> @lang('pages.ecf_title') </label>                        
                <input type="text" name="title" class="form-control" required>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label for="start">Fecha Inicio</label>
                        <input type="date" id="start" name="start" class="form-control" required>
                    </div>
                    <div class="col-md-6">
                        <label for="end">Fecha Final </label>
                        <input type="date" id="end" name="end" class="form-control" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="benefit_id"> @lang('pages.ecf_incident')</label>
                <select name="benefit_id" required="required" class="form-control">
                    <option selected="selected" value="">@lang('pages.ecf_select')</option>
                    @foreach ($benefits as $benefit)
                        <option value="{{ $benefit->id}}">{{ $benefit->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="custom-control custom-checkbox ">
                    <input type="checkbox" name="blocked" value="1" class="custom-control-input" id="cbox_block_days">
                    <label class="custom-control-label" for="cbox_block_days">Bloquear Dias</label>
                </div>
            </div>                    
        </div>                
    </div>
    <hr>
    @if(Auth::user()->role == 'supervisor')
        <div class="row">
            <div class="col-md-6">                        
                <strong>@lang('pages.ecf_users', ['number' => $total])</strong>
            </div>
        </div>
        <div class="row my-3">
            @if($total > 1)
                <div class="col-md-6">
                    @for($i = 0; $i < $total / 2; $i++)
                        <div class="custom-control custom-checkbox ">
                            <input type="checkbox" name="users[]" value="{{ $users[$i]->id }}" class="custom-control-input" id="cbox_users_{{ $i }}">
                            <label class="custom-control-label" for="cbox_users_{{ $i }}">{{ $users[$i]->firstname}}</label>
                        </div>
                    @endfor
                </div>
                <div class="col-md-6">
                    @for($i; $i < $total; $i++)
                        <div class="custom-control custom-checkbox ">
                            <input type="checkbox" name="users[]" value="{{ $users[$i]->id }}" class="custom-control-input" id="cbox_users_{{ $i }}">
                            <label class="custom-control-label" for="cbox_users_{{ $i }}">{{ $users[$i]->firstname}}</label>
                        </div>
                    @endfor
                </div>
            @else
                <div class="col-md-6">
                    @for($i=0 ;$i < $total ; $i++)
                        <div class="custom-control custom-checkbox ">
                            <input type="checkbox" name="users[]" value="{{ $users[$i]->id }}" class="custom-control-input" id="cbox_users_{{ $i }}">
                            <label class="custom-control-label" for="cbox_users_{{ $i }}">{{ $users[$i]->firstname}}</label>
                        </div>
                    @endfor
                </div>
            @endif
        </div>
    @elseif(Auth::user()->role == 'admin')
        <h5 class="text-center">Horarios</h5>
        <div class="row justify-content-center mt-2 mb-4">
            <div class="col-md-10">
                <div class="row justify-content-center">
                    @foreach($regions as $region)
                        <div class="col-xs-6 col-md-4">
                            <div class="custom-control custom-checkbox ">
                                <input type="checkbox" name="regions[]" value="{{ $region->id }}" class="custom-control-input" id="cbox_regions_{{ $loop->index }}">
                                <label class="custom-control-label" for="cbox_regions_{{ $loop->index }}">{{ $region->name }}</label>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif            
    <div class="row justify-content-center">
        <div class="col-md-4">
            <input type="submit" value="@lang('pages.create')" class="btn btn-block btn-primary">
        </div>
    </div>
</form>
@endsection
