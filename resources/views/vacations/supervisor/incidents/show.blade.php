@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Solicitudes</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Colaborador</h3>
    </div>
</div>
<form action="{{ route('super.user.incidents.store', $user->id) }}" method="POST">
    @csrf 
    <div class="form-group">
        <label for="benefit_id">Concepto</label>
        <select name="benefit_id" class="form-control" id="select_benefit" required="required">
            <option value="">Seleccione...</option>
            @foreach ($balances as $balance)
                @if($balance->benefit->type == 'day')
                <option {{ old('benefit_id') == $balance->benefit_id?'selected':'' }} value="{{ $balance->benefit_id }}">{{ $balance->benefit->name }}</option>
                @elseif($balance->benefit->type == 'pending')
                    @if(($balance->pending - $balance->amount) <= 0)
                        <option {{ old('benefit_id') == $balance->benefit_id?'selected':'' }} value="{{ $balance->benefit_id }}">
                            {{ $balance->benefit->name }} (Días restantes: {{ $balance->diff }})
                        </option>
                    @else
                        <option {{ old('benefit_id') == $balance->benefit_id?'selected':'' }} value="{{ $balance->benefit_id }}">{{ $balance->benefit->name }} @if($balance->pending>0)(Días restantes: {{ $balance->pending - $balance->amount }})@endif</option>
                    @endif
                @elseif($balance->benefit->type == 'time')
                    <option {{ old('benefit_id') == $balance->benefit_id?'selected':'' }} class="time" data-time="{{ $balance->getMaxTimePerRequest() }}" value="{{ $balance->benefit_id }}">{{ $balance->benefit->name }}</option>
                @endif
            @endforeach
        </select>
    </div>       
    <div class="form-group">
        <label for="title">Descripción</label>
        <input type="text" name="title" value="{{ old('title') }}" class="form-control form-date">
    </div>
    <div class="row">
        <div class="col-md-6">
            <label for="start">Fecha Inicio</label>
            <input type="date" id="start" name="start" class="form-control form-date" value="{{ old('start') }}" required>
        </div>
        <div class="col-md-6">
            <label for="end" id="label_title">Fecha Final </label>
            <input type="number" id="time" name="time" min="1" max="8" class="form-control" value="{{ old('time') }}" style="display: none">
            <input type="date" id="end" name="end" class="form-control" value="{{ old('end') }}" required>
        </div>

    </div>
    <div class="my-4">
        <input type="submit" value="Crear" class="btn btn-primary">
    </div>
</form>
@include('vacations.common.calendar', ['user_id' => $user->id])

@endsection

@push('scripts')
<script>
    $('#select_benefit').on('change', function(){
        show_time_fields()
    });

    function show_time_fields(){
        const select = $('#select_benefit');
        const val = select.val();
        if(val){
            const opt = $(select).find('option:selected');
            const opt_class = opt.attr('class');
            const opt_time = opt.data('time');
            const stmt = opt_class === 'time';
            $('#time').prop('disabled', !stmt).prop('required', stmt);
            if(stmt){
                $('#label_title').text('Horas');
                $('#time').attr('max', opt_time?opt_time:8).show();
                $('#end').val($('#start').val()).hide();
            }else{
                $('#label_title').text('Fecha Final');
                $('#time').hide();
                $('#end').show();
            }
        }
    }

    show_time_fields()

    $(function(){
        $('.form-date').on('change', function(){
            const opt = $('#select_benefit').find('option:selected');
            const opt_class = opt.attr('class');
            const stmt = opt_class === 'time';
            var dateBegin = new Date($('#start').val());
            if(!stmt){
                var dateEnd = new Date($('#end').val());
                if(!isNaN(dateBegin) && !isNaN(dateEnd)){
                    if(dateEnd < dateBegin){
                        $('#end').val($('#start').val());
                    }
                }else if(!isNaN(dateBegin) && isNaN(dateEnd)){
                    $('#end').val($('#start').val());

                }else if(isNaN(dateBegin) && !isNaN(dateEnd)){
                    $('#start').val($('#end').val());
                }
            }else{
                $('#end').val($('#start').val());
            }
        })
    });
</script>
@endpush