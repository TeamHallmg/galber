@extends('vacations.app')

@section('content')
<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> CONTROL DE INCIDENCIAS</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Resumen de saldos</h3>
    </div>
</div>

<div class="row justify-content-center mb-2">
    <div class="col-7 text-center">
        <select name="user" id="user" class="selectpicker w-100" data-live-search="true" title="Selecciona un usuario para consultar su resumen de saldos" required>
            @foreach ($users as $usuario)
                <option value="{{ $usuario->id }}" {{ !is_null($user)?$usuario->id == $user->id?'selected':'':'' }}>{{ $usuario->getFullNameAttribute() }}</option>
                {{-- <i class="fas fa-search"></i> --}}
            @endforeach
        </select>
    </div>
</div>

@if(!empty($data))
    @include('vacations.supervisor.incidents.ub')
@endif

@endsection

@section('scripts')
<script>

$('select[name=user]').change(function() {
    var user_id = $(this).val();
    var url = '{!! $url !!}/' + user_id;
    // alert(url);
    window.location.replace(url);
});


</script>
@endsection
