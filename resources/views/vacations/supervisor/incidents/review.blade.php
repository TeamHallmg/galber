@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Solicitudes</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Mis colaboradores</h3>
    </div>
</div>

<form action="{{ route('super.user.incidents.update', $user->id) }}" method="POST">
    @csrf
    @method('PUT')
    <table class="table" id="table">
        <thead class="blue_header">
            <tr>
                <th>Empleado</th>
                <th>Titulo</th>
                <th>Inicio</th>
                <th>Fin</th>
                <th>Incidencia</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($incidents as $incident)
                <tr>
                    <td>{{ $incident->from->FullName }}</td>
                    <td>{{ $incident->info }}</td>
                    <td>{{ $incident->event->getStartDate('d/m/Y') }}</td>
                    <td>{{ $incident->event->getEndDate('d/m/Y') }}</td>
                    <td>{{ $incident->benefit->name }}</td>
                    <td>
                        <select name="incidents[{{ $incident->id }}]" class="form-control" style="min-width: 150px" data-container="body" data-style="btn-info">
                            <option value="-1" style="background: #f0ad4e; color: #fff;">Posponer</option>
                            <option value="0" style="background: #d9534f; color: #fff;">Rechazar</option>
                            @if($incident->status === "pending")
                                <option value="1" style="background: #5cb85c; color: #fff;">Aprobar</option>
                            @endif
                        </select>
                    </td>
                </tr>

            @endforeach
        </tbody>
    </table>
    <div class="row justify-content-center mt-3">
        <div class="col-md-4">
            <input type="submit" value="Guardar" class="btn btn-primary btn-block">
        </div>
    </div>
</form>

@endsection

@section('scripts')
<script>
    let table = $('#table').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
    });
    $(function(){
        $('#table').on('page.dt', function () {
            setTimeout(function(){
                $('.selectpicker').selectpicker('refresh');
            }, 0)
        });
    });
</script>
@endsection