@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Solicitudes</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Mis colaboradores</h3>
    </div>
</div>

<table class="table" id="table">
    <thead class="blue_header">
        <tr>
            <th># Empleado</th>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
            <tr>
                <td>{{ $user->employee->idempleado }}</td>
                <td>{{ $user->FullName }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    <a href="{{ route('super.user.incidents.show', $user->id) }}" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i></a>
                    <a href="{{ route('super.user.incidents.review', $user->id) }}" class="btn btn-info"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>


@endsection

@section('scripts')
<script>
    let table = $('#table'){
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			},
		}
</script>
@endsection