<div class="card my-4">
    <div class="blue_header card-header">
        <h3 class="m-0">Vacaciones</h3>
    </div>
    <div class="card-body">
    <h4>Fecha de ingreso: @if(isset($user)) {{ $user->employee?$user->employee->getIngreso('d/m/Y'):'' }} @endif </h4>
        <div class="table-responsive d-none">
            <table class="table table-hover table-bordered">
                <thead class="blue_header">
                    <tr>
                        <th>Periodo</th>
                        <th>Fecha Inicio</th>
                        <th>Vigencia</th>
                        <th>Saldo</th>
                        <th>Autorizados</th>
                        <th>Solicitados</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data['vacations']['rows'] as $vacation)
                        @if($vacation->pending != 0)
                            <tr>
                            <td>{{$vacation->year - 1}} - {{$vacation->year}}</td>
                            <td>{{ date('d-m-Y', strtotime($vacation->created_at)) }}</td>
                            <td>{{ $vacation->until?date('d-m-Y', strtotime($vacation->until)):'' }}</td>
                            <td>{{$vacation->pending}}</td>
                            <td>{{$vacation->amount-$vacation->solicited}}</td>
                            <td>{{$vacation->solicited}}</td>
                            <td>{{$vacation->pending - $vacation->amount}}</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead class="blue_header">
                    <tr>
                        <th>Periodo</th>
                        <th>Fecha Inicio</th>
                        <th>Vigencia</th>
                        <th>Inicial</th>
                        <th>Procesadas</th>
                        <th>Autorizados</th>
                        <th>Solicitados</th>
                        <th>Disponibles</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($data['vacations']['rows'] as $vacation)
                        @if($vacation->pending != 0)
                            <tr>
                            <td>{{$vacation->year - 1}} - {{$vacation->year}}</td>
                            <td>{{ date('d-m-Y', strtotime($vacation->created_at)) }}</td>
                            <td>{{ $vacation->until?date('d-m-Y', strtotime($vacation->until)):'' }}</td>
                            <td>{{$vacation->pending}}</td>
                            <td>{{$vacation->getProcessed()}}</td>
                            <td>{{$vacation->amount-$vacation->solicited-$vacation->getProcessed()}}</td>
                            <td>{{$vacation->solicited}}</td>
                            <td>{{$vacation->pending - $vacation->amount}}</td>
                            </tr>
                        @endif
                    @empty
                    @endforelse
                </tbody>
            </table>
        </div>
        @if(count($data['balances']))
            <h4 class="pull-right">Total: {{$data['vacations']['diff']}} </h4>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="blue_header card-header">
                <h5 class="m-0">Vacaciones</h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                            <th>Concepto</th>
                            <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['balances'] as $balance)
                                @if($balance->benefit->type == 'day')
                                    <tr>
                                    <td>{{$balance->benefit->name}}</td>
                                    <td>{{$balance->pending}}</td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    @if(isset($data['time']))
        <div class="col-md-6">
            <div class="card">
                <div class="blue_header card-header">
                    <h5 class="m-0">Tiempo</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Concepto</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data['time'] as $benefit)
                                    <tr>
                                    <td>{{ $benefit->name }}</td>
                                    <td>{{ $benefit->time }} H</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>