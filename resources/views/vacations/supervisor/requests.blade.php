@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> APROBAR SOLICITUDES</h2>
    </div>
</div>
{{--<div class="card mb-5">
    <div class="card-body">
        Mis Solicitudes Inactivas
    </div>
</div>--}}

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Solicitudes de los empleados</h3>
    </div>
</div>
    
<form action="{{ route('super.requests') }}" method="POST">
    @csrf
    <div class="table-responsive">
        <table class="table table-hover table-bordered" id="incidentsTable">
            <thead class="blue_header">
                <tr>
                    <th>Empleado</th>
                    <th>Descripción</th>
                    <th>Inicio</th>
                    <th>Fin</th>
                    <th>Estatus</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($requests as $request)
                    <tr>
                        <td>{{ $request->from->FullName }}</td>
                        <td>{!! $request->info !!}</td>
                        <td>{{ $request->event->getStartDate('d/m/Y') }}</td>
                        <td>{{ $request->event->getEndDate('d/m/Y') }}</td>
                        <td>
                            @if($request->status == 'pending')
                                Pendiente
                            @elseif($request->status == 'complete')
                                Autorizada
                            @endif
                        </td>
                        <td>
                            @if($request->status == 'pending')
                                <select name="b[{{ $request->id }}][v]" class="selectpicker" data-container="body" data-style="btn-info">
                                    <option value="-1" style="background: #f0ad4e; color: #fff;">Posponer</option>
                                    <option value="0" style="background: #d9534f; color: #fff;">Rechazar</option>
                                    <option value="1" style="background: #5cb85c; color: #fff;">Aprobar</option>
                                </select>
                            @elseif($request->status == 'complete')
                                <button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#deleteRequest" data-id="{{ $request->id }}" data-min="{{ date('Y-m-d', $request->time) }}" title="Eliminar"><i class="fas fa-trash"></i></button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <input type="submit" value="Guardar" class="btn btn-primary">
</form>

<div class="modal fade" id="deleteRequest" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form" action="{{ url('super/delete_approved_request') }}" method="POST" id="form_delete">
				<input name="request_id" type="hidden">
				{{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar Solicitud Aprobada</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
						<label class="requerido" for="delete_date">Fecha de Solicitud Cancelación</label>
						<input type="date" class="form-control" name="delete_date" id="delete_date" required>
                    </div>
                    <div class="form-group">
						<label id="cnt_comment" for="delete_comments" class="requerido">Comentarios (0/200)</label>
						<textarea name="delete_comments" class="form-control noresize" maxlength="200" id="delete_comments" onkeyup="countMotive(this.value)" rows="2" required></textarea>
					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    var incidentsTable = $('#incidentsTable').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
    });

    function countMotive(str) {
        var lng = str.length;
        $("#cnt_comment").html('Comentarios (' + lng + '/200)');
    }

    $(document).ready(function() {
        $("#deleteRequest").on('show.bs.modal', function (e) {
            var triggerLink = $(e.relatedTarget);
            var id = triggerLink.data("id");
            var min_date = triggerLink.data("min");
            $('input[name=request_id]').val(id);
            $('input[name=delete_date]').attr('min', min_date);
        });

        $("#deleteRequest").on('hide.bs.modal', function (e) {
            $("#cnt_comment").html('Comentarios (0/200)');
            $('#del_reason').val('');
        });
    });
</script>
@endsection
