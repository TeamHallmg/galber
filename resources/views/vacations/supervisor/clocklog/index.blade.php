@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Reloj Checador</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Autorizar checadas</h3>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-bordered" id="logsTable">
        <thead class="blue_header">
            <tr>
                <th>Nombre</th>
                <th>Hora de checada</th>
                <th>Hora comparativa</th>
                <th>Tipo de checada</th>
                <th>Plataforma</th>
                <th>Estatus</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($logs as $log)
                <tr id="tr-{{ $log->id }}">
                    <td>{{ $log->user->FullName }}</td>
                    <td>{{ $log->date }}</td>
                    <td>{{ $log->compare_time }}</td>
                    <td>{{ $log->type === 'in'? 'Llegada' : 'Salida' }}</td>
                    <td>{{ $log->from }}</td>
                    <td>{{ __('bd.' . $log->status) }}</td>
                    <td>
                        <button class="btn btn-primary log_status" data-status="accepted" data-name="{{ $log->user->FullName }}" data-date="{{ $log->date }}" data-log="{{ $log->id }}" data-toggle="modal" data-target="#modal_log">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </button>
                        <button type="button" class="btn btn-danger log_status" data-status="rejected" data-name="{{ $log->user->FullName }}" data-date="{{ $log->date }}" data-log="{{ $log->id }}" data-toggle="modal" data-target="#modal_log">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

{{-- <div class="row mb-2 mt-3">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Ausencias detectadas</h3>
    </div>
</div>


<div class="table-responsive">
    <table class="table table-bordered" id="logsTable">
        <thead class="blue_header">
            <tr>
                <th>Nombre</th>
                <th>Hora de checada</th>
                <th>Hora comparativa</th>
                <th>Tipo de checada</th>
                <th>Plataforma</th>
                <th>Estatus</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($absentLogs as $log)
                <tr>
                    <td>{{ $log->user->FullName }}</td>
                    <td>{{ $log->date }}</td>
                    <td>{{ $log->compare_time }}</td>
                    <td>{{ $log->type === 'in'? 'Llegada' : 'Salida' }}</td>
                    <td>{{ $log->from }}</td>
                    <td>{{ __('bd.' . $log->status) }}</td>
                    <td>
                        <button class="btn btn-primary log_status" data-status="accepted" data-name="{{ $log->user->FullName }}" data-date="{{ $log->date }}" data-log="{{ $log->id }}" data-toggle="modal" data-target="#modal_log">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </button>
                        <button type="button" class="btn btn-danger log_status" data-status="rejected" data-name="{{ $log->user->FullName }}" data-date="{{ $log->date }}" data-log="{{ $log->id }}" data-toggle="modal" data-target="#modal_log">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div> --}}
<!-- Modal -->
<div class="modal fade" id="modal_log" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_title">Revisión de checada</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <p>Ingresa una explicación del porque rechazas el registro, sea claro</p>
                    <hr>
                    <input type="hidden" id="txt-log_id">
                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="text" class="form-control" id="txt-log_name" disabled>
                    </div>
                    <div class="form-group">
                        <label>Fecha</label>
                        <input type="text" class="form-control" id="txt-log_date" disabled>
                    </div>
                    <hr>
                    <div class="alert alert-warning fade show" role="alert" id="alert">
                      <strong>Información requerida</strong> 
                    </div>
                    <textarea class="form-control" id="txt-log_comment" rows="3" placeholder="Explicación..."></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-save_log_comment">Guardar</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    const URL_LOG_CHANGE_STATUS = "{{ url('api/incidents/change_log_status') }}";

    let logsTable = $('#logsTable').DataTable({
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			},
		});

    $(function(){
        $('tbody').on('click', '.log_status', function(){
            $('#txt-log_comment').val('');
            const name = $(this).data('name');
            const date = $(this).data('date');
            const log_id = $(this).data('log');
            const status = $(this).data('status');
            const btn = $(this);
            $('#txt-log_name').val(name);
            $('#txt-log_date').val(date);
            $('#txt-log_id').val(log_id);
            $('#txt-log_id').data('status', status);
            if(status === 'rejected'){
                $('#alert').show();
            }else{
                $('#alert').hide();
            }
        });

        $('#btn-save_log_comment').on('click', function(){
            const modal = $('#modal_log');
            const comment = $('#txt-log_comment').val().trim();
            const log_id = $('#txt-log_id').val();
            const status = $('#txt-log_id').data('status');
            console.log('#tr-' + log_id);
            const rowEl = $('#tr-' + log_id);
            if((!!comment && status === 'rejected') || status === 'accepted'){
                axios.post(URL_LOG_CHANGE_STATUS, {
                    log_id: log_id,
                    status: status,
                    comment: comment,
                })
                .then(function(response){
                    if(response.data.success){
                        modal.modal('hide');
                        rowEl.closest('tr').fadeOut();
                    }
                });
            }else{
                //Alerta el campo es requerido
            }
        });

    });
</script>
@endsection