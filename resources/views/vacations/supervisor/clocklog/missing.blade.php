@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Reloj Checador</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Faltas generadas por el sistema</h3>
    </div>
</div>

<div>
    <div class="form-group">
        <select id="chooseDateSelect" class="selectpicker">
            <option value="">Todas las Fechas</option>
            @foreach ($dates as $date)
                <option value="{{ $date }}">{{ $date }}</option>
            @endforeach
        </select>
    </div>
</div>

@if(!!$absentBenefit)
    <div class="table-responsive">
        <table class="table table-bordered" id="logsTable">
            <thead class="blue_header">
                <tr>
                    <th>Nombre</th>
                    <th>Fecha de falta</th>
                    <th>Información</th>
                    <th>Justificar falta</th>
                    <th>Aceptar falta</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($incidents as $incident)
                    <tr id="{{ $incident->id }}" data-user="{{ $incident->from->id }}">
                        <td>{{ $incident->from->FullName }}</td>
                        <td data-sort="{{ $incident->getTimeOnFormat('YmdHis') }}">{{ $incident->getTimeOnFormat('d/m/Y') }}  </td>
                        <td>{{ $incident->info }}</td>
                        <td>
                            <button type="button" class="btn btn-success btnAuthorizeCheck" data-toggle="modal" data-target="#modalAuthorize">
                                <span><i class="fa fa-check-circle" aria-hidden="true"></i></span>
                            </button>
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger btnRejectCheck"  data-toggle="modal" data-target="#modalReject">
                                <span><i class="fa fa-times-circle" aria-hidden="true"></i></span>
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@else
    <div class="alert alert-warning alert-important" role="alert">
      <strong>No se encontro incidencía para faltas</strong>
    </div>
@endif

<!-- Modal Authorize -->
<div class="modal fade" id="modalAuthorize" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Justicar falta | Generar asistencia del día: <span class="modalAuthorizeDate"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" id="modalAuthorizeForm">
                    <div class="container-fluid" id="modalAuthorizeBody">
                        <p>
                            Al aceptar usted justfica la falta para este día: <span class="modalAuthorizeDate"></span>
                        </p>
                        <p> 
                            Esto generará las checadas del usuario como si hubiera checado y asistido normalmente.
                        </p>
                        <input type="hidden" name="modalAuthorizeLogID" id="modalAuthorizeID" class="form-control">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="">Información:</label>
                                    <textarea id="modalAuthorizeCheck" class="form-control" disabled></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Entrada</h5>
                                <ul id="modalAuthorize-in">

                                </ul>
                            </div>
                            <div class="col-md-6">
                                <h5>Salida</h5>
                                <ul id="modalAuthorize-out">

                                </ul>
                            </div>
                        </div>
                        <hr>
                        <h5>Motivo</h5>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <textarea name="reason" id="modalAuthorizeReason" rows="3" class="form-control" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="text-danger">
                            <ul id="modalAuthorizeProblems"></ul>
                        </div>
                    </div>
                    <div class="container-fluid" id="modalAuthorizeImg" style="display: none">
                        <div class="text-center">
                            <h3>Guardado correctamente</h3>
                            <img src="{{ asset('img/check_mark.png') }}" class="img-fluid my-3" style="max-height: 250px">
                        </div>
                    </div>
                    <div class="modal-footer" id="modalAuthorizeFooter">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Justificar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Reject -->
<div class="modal fade" id="modalReject" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmar la falta el día <span class="modalRejectDate"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" id="modalRejectForm">
                    <div class="container-fluid" id="modalRejectBody">
                        <p>
                            Al aceptar usted confirmará la falta generada por el sistema del día <span class="modalRejectDate"></span> para este empleado.
                        </p>
                        <p>
                            Favor de válidar bien la información antes de continuar.
                        </p>
                        <input type="hidden" class="modalRejectLogsAll" name="modalRejectLogID" id="modalRejectID" class="form-control">
                        <div id="modalRejectLogsID"> </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="">Información:</label>
                                    <textarea id="modalRejectCheck" class="form-control" disabled></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Entrada</h5>
                                <ul id="modalReject-in">

                                </ul>
                            </div>
                            <div class="col-md-6">
                                <h5>Salida</h5>
                                <ul id="modalReject-out">

                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="collapse" id="collapseExample">
                                <div class="col-md-8">
                                    <h5 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Listado de Incidencias</h5>
                                </div>
                                <div class="w-100"></div>
                                <div class="col-12">
                                    <p class="mt-1">Si la incidencia no es una falta. En este apartado usted puede generar una incidencia a partir del siguiente listado.</p>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <select id="modalRejectSelect" class="form-control">
                                            <option value=""></option>
                                            @foreach ($benefits as $benefit)
                                                <option value="{{ $benefit->id }}">{{ $benefit->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h5>Motivo</h5>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <textarea name="reason" id="modalRejectReason" rows="3" class="form-control" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="text-danger">
                            <ul id="modalRejectProblems"></ul>
                        </div>
                    </div>
                    <div class="container-fluid" id="modalRejectImg" style="display: none">
                        <div class="text-center">
                            <h3>Guardado correctamente</h3>
                            <img src="{{ asset('img/check_mark.png') }}" class="img-fluid my-3" style="max-height: 250px">
                        </div>
                    </div>
                    <div class="modal-footer" id="modalRejectFooter">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-warning" id="btn_change_incident" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Cambiar incidencia
                        </button>
                        <button type="submit" class="btn btn-danger" id="modalRejectBtnSubmit">Confirmar Falta</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
    
@section('scripts')
<script> 
    $(document).ready(function() {
        let groupColumn = 0;
        let table = $('#logsTable').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
            },
            "columnDefs": [
                { "visible": false, "targets": groupColumn }
            ],
            "order": [[ groupColumn, 'asc' ]],
            "drawCallback": function ( settings ) {
                let api = this.api();
                let rows = api.rows( {page:'current'} ).nodes();
                let last=null;
                api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="dt_group"><td colspan="8">'+group+'</td></tr>'
                        );
                        last = group;
                    }
                } );
            }
        } );
    
        // Order by the grouping
        $('#logsTable tbody').on( 'click', 'tr.group', function () {
            let currentOrder = table.order()[0];
            if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
                table.order( [ groupColumn, 'desc' ] ).draw();
            }
            else {
                table.order( [ groupColumn, 'asc' ] ).draw();
            }
        } );

        $('#logsTable tbody').on( 'click', '.btnAuthorizeCheck', function () {
            const tr = $(this).closest('tr');
            const id = tr.attr('id');
            const user = tr.data('user');
            let date = tr.find('td:eq(0)').text().trim();
            let splitDate = date.split('/');
            date = splitDate[2] + '-' + splitDate[1] + '-' + splitDate[0];
            const info = tr.find('td:eq(1)').text();
            $('#modalAuthorizeCheck').val(info);
            $('#modalAuthorizeID').val(id);
            $('.modalAuthorizeDate').html(date);

            $('#modalAuthorize-in').empty();
            $('#modalAuthorize-out').empty();

            getClockLog(user, date.trim(), 'in', 'Authorize');
            getClockLog(user, date.trim(), 'out', 'Authorize');
        });

        $('#logsTable tbody').on( 'click', '.btnRejectCheck', function () {
            const tr = $(this).closest('tr');
            const id = tr.attr('id');
            const user = tr.data('user');
            let date = tr.find('td:eq(0)').text().trim();
            $('#modalRejectID').val(id);
            $('.modalRejectDate').html(date);
            let splitDate = date.split('/');
            date = splitDate[2] + '-' + splitDate[1] + '-' + splitDate[0];
            $('#modalReject-in').empty();
            $('#modalReject-out').empty();
            $('#modalRejectHiddenInputs').empty();
            getClockLog(user, date.trim(), 'in', 'Reject');
            getClockLog(user, date.trim(), 'out', 'Reject');
        });

        $('#chooseDateSelect').on('change', function(){
            table.draw();
        });

        function getClockLog(user, date, type, classModal){
            const url = `{{ url('api/incidents/check_logs/${user}/${date}/${type}') }}`;
            let logs = [];
            axios.get(url, {
                user_id: user,
                date: date,
                type: type,
            })
            .then(function (response){
                if(response.data.success){
                    if (response.data.logs.length === 0 ){
                        $('#modal' + classModal + '-' + type).append(`<li>No hay checada</li>`);
                    }else{
                        for (const log of response.data.logs) {
                            $('#modal' + classModal + '-' + type).append(`<li>${log.date}</li>`);
                        }
                    }
                }
            })
            .catch(function (response){
                const errors = response.response.data.errors
                for (const key in errors) {
                    if (errors.hasOwnProperty(key)) {
                        for (const iterator of errors[key]) {
                            $('#modal' + classModal + 'Problems').append(`<li>${iterator}</li>`);
                        }
                    }
                }
            });
        }

        $('#modalAuthorizeForm').on('submit', function(e){
            e.preventDefault();
            const btn_submit = $(this).find(':submit')
            btn_submit.attr('disabled', true);
            const id = $('#modalAuthorizeID').val();
            const reason = $('#modalAuthorizeReason').val();
            const url = "{{ route('super.clocklog.supervisor.manage_absents') }}";
            $('#modalAuthorizeProblems').empty();

            axios.post(url, {
                incident_id: id,
                incident_reason: reason,
                incident_status: 'canceled',
            }).then(function (response) {
                if(response.data.success){
                    table.row('#' + id).remove();
                    $('#modalAuthorizeBody').addClass('animated fadeOut faster')
                    setTimeout(() => {
                        $('#modalAuthorizeBody').hide();
                        $('#modalAuthorizeImg').show().addClass('animated fadeIn faster');
                        $('#modalAuthorizeFooter').hide();
                    }, 500);
                    setTimeout(() => {
                        $('#modalAuthorize').modal('hide');
                    }, 2000);
                    setTimeout(() => {
                        setupModal()
                    }, 300);
                }else{
                    alert(response.data.msg);
                }
            }).catch(function (error){
                const errors = error.response.data.errors
                for (const key in errors) {
                    if (errors.hasOwnProperty(key)) {
                        for (const iterator of errors[key]) {
                            $('#modalAuthorizeProblems').append(`<li>${iterator}</li>`);
                        }
                    }
                }
            });
            btn_submit.attr('disabled', false);
        })

        $('#modalRejectForm').on('submit', function(e){
            e.preventDefault();
            const btn_submit = $(this).find(':submit')
            btn_submit.attr('disabled', true);
            const id = $('#modalRejectID').val();
            const reason = $('#modalRejectReason').val();
            const url = "{{ route('super.clocklog.supervisor.manage_absents') }}";
            const benefit = $('#modalRejectSelect').val();
            $('#modalRejectProblems').empty();

            let allLogs = $('.modalRejectLogsAll').map(function() {
                return this.value;
            }).get();
            allLogs = new Set(allLogs);

            axios.post(url, {
                incident_id: id,
                incident_reason: reason,
                incident_status: 'accepted',
                incident_benefit: benefit,
            }).then(function (response) {
                if(response.data.success){
                    for (const logID of allLogs) {
                        table.row('#' + logID).remove();
                    }
                    $('#modalRejectBody').addClass('animated fadeOut faster')
                    setTimeout(() => {
                        $('#modalRejectBody').hide();
                        $('#modalRejectImg').show().addClass('animated fadeIn faster');
                        $('#modalRejectFooter').hide();
                    }, 500);
                    setTimeout(() => {
                        $('#modalReject').modal('hide');
                    }, 2000);
                    setTimeout(() => {
                        setupModal()
                    }, 300);
                }else{
                    alert(response.data.msg);
                }
            }).catch(function (error){
                const errors = error.response.data.errors
                for (const key in errors) {
                    if (errors.hasOwnProperty(key)) {
                        for (const iterator of errors[key]) {
                            $('#modalRejectProblems').append(`<li>${iterator}</li>`);
                        }
                    }
                }
            });
            btn_submit.attr('disabled', false);
        })

        $('#modalAuthorize').on('hidden.bs.modal', function (e) {
            setupModal('Authorize');
        });

        $('#modalReject').on('hidden.bs.modal', function (e) {    
            setupModal('Reject');
        });

        function setupModal(modal){
            $('#modal' + modal + 'Img').removeClass('animated fadeIn faster').hide();
            $('#modal' + modal + 'Body').removeClass('animated fadeOut faster').show();
            $('#modal' + modal + 'Footer').show();
            $('#modal' + modal + 'Reason').val('');
            if(modal === 'Reject'){
                $('#collapseExample').removeClass('show');
                $('#modalRejectSelect').removeClass('required').prop('required', false).prop('disabled', true).prop("selectedIndex", 0);
                $('#modalRejectBtnSubmit').text('Confirmar Falta');
                $('#btn_change_incident').text('Cambiar incidencia')
            }
            clearEmptyRowGroups();
        }

        function clearEmptyRowGroups(){
            setTimeout(function() {
                table.draw();
            }, 0);
        }

        $.fn.dataTableExt.afnFiltering.push(
            function(oSettings, aData, iDataIndex) {
                const date = $('#chooseDateSelect').val().trim();
                if(!date){
                    return true;
                }else{
                    if (typeof aData[1] === "string" && typeof date === "string"){
                        if(aData[1] === date){
                            return true;
                        }
                    }
                }
                return false;
            }
        );

        $('#btn_change_incident').on('click', function(){
            const visible = !$('#collapseExample').hasClass('show');
            $('#modalRejectBtnSubmit').text(visible ? 'Confirmar' : 'Confirmar Falta');
            $('#btn_change_incident').text(visible ? 'Regresar a Falta' : 'Cambiar incidencia')
            $('#modalRejectSelect').toggleClass('required', visible).prop('required', visible).prop('disabled', !visible);
        });

    } );
</script>
@endsection