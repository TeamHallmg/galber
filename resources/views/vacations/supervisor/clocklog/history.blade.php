@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Reloj Checador</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Historial de checadas</h3>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-bordered" id="logsTable">
        <thead class="blue_header">
            <tr>
                <th>Empleado</th>
                <th>Departamento</th>
                <th>dept_id</th>
                <th>Puesto</th>
                <th>job_id</th>
                <th>Fecha</th>
                <th>Hora de checada</th>
                <th>Hora comparativa</th>
                <th>Tipo de checada</th>
                <th>Plataforma</th>
                <th>Ubicación</th>
                <th>active</th>
                {{-- <th>Nombre</th>
                <th>Hora de checada</th>
                <th>Hora comparativa</th>
                <th>Tipo de checada</th>
                <th>Plataforma</th>
                <th>Ubicación</th>
                <th>Info</th> --}}
                <th>Comentarios</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($logs as $log)
                <tr>
                    <td>{{ $log->user->FullName }}</td>
                    <td>{{ $log->user->employee_wt->getDepartmentName() }}</td>
                    <td>{{ $log->user->employee_wt->getDepartmentId() }}</td>
                    <td>{{ $log->user->employee_wt->getPuestoName() }}</td>
                    <td>{{ $log->user->employee_wt->getPuestoId() }}</td>
                    <td data-sort="{{ $log->getDateOnFormat('YmdHis') }}">{{ $log->getDateOnFormat('d-m-Y') }}</td>
                    <td>{{ $log->getDateOnFormat('H:i') }}</td>
                    <td>{{ $log->compare_time }}</td>
                    <td>{{ $log->getTypeInfo() }}</td>
                    <td>{{ $log->from }}</td>
                    <td>{{ $log->address }}</td>
                    <td>{{ $log->user->trashed()?'sd_inactive':'sd_active' }}</td>
                    {{-- <td>{{ $log->user->FullName }}</td>
                    <td data-sort="{{ $log->getDateOnFormat('YmdHis') }}">{{ $log->getDateOnFormat('d/m/Y') }}</td>
                    <td>{{ $log->compare_time }}</td>
                    <td>{{ $log->type === 'in'? 'Llegada' : 'Salida' }}</td>
                    <td>{{ $log->from }}</td>
                    <td>{{ $log->address }}</td>
                    <td>{{ $log->getTypeInfo() }}</td> --}}
                    <td>
                        @if ($log->comments->isNotEmpty())
                            <button type="button" class="btn btn-success btn-comment" data-comments="{{ $log->comments->toJson() }}" data-toggle="modal" data-target="#modelId">
                                Ver
                            </button>
                        @else
                            -
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Comentarios</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <ul id="modal_body">

                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>

    let logsTable = $('#logsTable').DataTable({
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			},
            dom: '<"row display-content-between"<"col"l><"col"f>>rtip',
            columnDefs: [
                {
                    targets: [2, 4, 11],
                    visible: false,
                }
            ]
		});

    $(function(){
        $('tbody').on('click', '.btn-comment', function(){
            const comment_json = $(this).data('comments');
            if(typeof(comment_json) === "object"){
                $('#modal_body').empty();
                for (const comment of comment_json) {
                    $('#modal_body').append(`<li>${comment.comment} - ${comment.created_at}</li>`)
                }
            }
        });
    })
</script>
@endsection
