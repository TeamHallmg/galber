@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Reloj Checador</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Checadas del Día</h3>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-bordered" id="logsTable">
        <thead class="blue_header">
            <tr>
                <th>Nombre</th>
                <th>Hora de llegada</th>
                <th>Horario llegada</th>
                <th>Hora de Salida</th>
                <th>Horario Salida</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($clocklogs as $log)
                <tr>
                    <td>
                        {{ $log['user']->FullName }}
                        @if($log['user']->tooltip)
                            <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="{{ trim($log['user']->not_require_msg) }}">
                                <i class="fa fa-info-circle fa-bg-info" aria-hidden="true"></i>
                            </span>
                        @endif
                    </td>
                    @if($log['user']->workable)
                        @if(isset($log['in']))
                            <td class="bg-clocklog-in_{{ $log['in']->arrive_status }}">{{ $log['in']->getDateOnFormat('H:i') }}</td>
                            <td>{{ $log['in']->compare_time }}</td>
                        @else
                            <td class="bg-clocklog-none">Sin checada</td>
                            <td>{{ $workshifts[$log['user']->employee->region_id]['in'] }}</td>
                        @endif
                        @if(isset($log['out']))
                            <td class="bg-clocklog-out_{{ $log['out']->arrive_status }}"> {{ $log['out']->getDateOnFormat('H:i')   }}</td>
                            <td>{{ $log['out']->compare_time }}</td>
                        @else
                            <td class="bg-clocklog-none">Sin checada</td>
                            <td>{{ $workshifts[$log['user']->employee->region_id]['out'] }}</td>
                        @endif
                    @else
                        <td colspan="4" class="text-center bg-clocklog-none">{{ $log['user']->not_require_msg }}</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Información</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
</script>
@endsection