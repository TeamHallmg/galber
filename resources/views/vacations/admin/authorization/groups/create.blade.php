@extends('layouts.app')

@section('content')

<div class="card card-3">
	<div class="card-header">
		<h3>Ruta de autorizacion</h3>
	</div>
	<div class="card-body">
		<div class="text-center">
			<h3 for="">Añadir Nivel de autorización <button id="addLevel" class="btn btn-warning">Añadir</button></h3>
		</div>
		<form action="{{ route('authorization_groups.store', $group->id) }}" method="POST">
			@csrf
			<div class="row justify-content-center my-4">
				<div class="col-md-4">
					<input type="submit" value="Guardar" class="btn btn-primary btn-block" id="btnSubmit" disabled>
				</div>
			</div>
			<div class="card">
				<div class="card-body" id="body">
				
				</div>
			</div>
			
		</form>
		<div id="divLevel" style="display: none">
			<h3>Nivel #</h3>
			<div class="row">
				<div class="col-md-5">
					<input type="text" name="name[]" class="form-control">	
				</div>
				<div class="col-md-5">
					<select name="users[]" class="form-control" data-live-search="true">
						@foreach ($users as $key => $user)
							<option value="{{ $key }}"> {{ $user }} </option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-danger removeLevel">Quitar Nivel</button>
				</div>
			</div>
			<hr>
		</div>
	</div>
</div>

@endsection

@section('scripts')
	<script>
		$(document).ready( function () {
			$("#addLevel").on('click', function (){
				var divLevel = $('#divLevel').clone();
				divLevel.find('select').selectpicker();
				divLevel.show();
				divLevel.addClass('levels');
				$('#body').append(divLevel);
				reprintLevels();
				activeFormButton()
			});

			$("#body").on('click', '.removeLevel', function (event) {
				event.preventDefault();
				$(this).closest('.levels').remove();
				reprintLevels();
				activeFormButton();
			});

			function activeFormButton(){
				var numItems = $('.levels').length;
				console.log(numItems);
				if(numItems > 0){
					$("#btnSubmit").removeAttr('disabled');
				}else{
					$("#btnSubmit").attr('disabled','disabled');
				}
			}


			function reprintLevels(){
				var cont = 1;
				$('.levels').each( function () {
					$(this).attr('id', 'divLevel-' + cont);
					var h3Level = $(this).find('h3');
					var text = h3Level.html();
					text = text.substring(0 ,text.indexOf("#") + 1);;
					h3Level.html(text + cont);
					cont++;
				});
			}
		});
	</script>
@endsection