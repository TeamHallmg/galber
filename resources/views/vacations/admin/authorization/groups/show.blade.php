@extends('layouts.app')


@section('content')

<div class="card card-3">
	<div class="card-header">
		<h3>Grupo: {{$group->name}}</h3>
	</div>
	@if(count($group->authorizationRoutes))
	<div class="card-body">
		<div class="row justify-content-end">
			<div class="col-sm-4">
				<a href="{{url('authorization_groups/'.$group->id.'/edit')}}" class="btn btn-primary btn-block">Editar Lineas</a>
			</div>
		</div>
		<div id="accordion" class="my-3">
			@foreach($group->authorizationRoutes as $level)
			<div class="card">
				<div class="card-header" id="heading{{ $level->id }}">
					<h5 class="mb-0">
					<button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{ $level->id }}" aria-expanded="true" aria-controls="collapse{{ $level->id }}">
						<h4>#{{ $level->level }} {{ $level->name }}</h4>
					</button>
					</h5>
				</div>

				<div id="collapse{{ $level->id }}" class="collapse {{ $loop->first?'show':'' }}" aria-labelledby="headingOne" data-parent="#accordion">
					<div class="card-body">
						@foreach($level->usersInRoute as $userRoute)
							<p>Nombre: <strong>{{ $userRoute->user->FullName }} </strong></p>
							<p>No. de Empleado: <strong>{{ $userRoute->user->number }} </strong></p>
							<p>Correo: <strong>{{ $userRoute->user->email }} </strong></p>
						@endforeach
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
	@else
	<div class="row justify-content-center mt-3">
		<div class="col-sm-6">
			<div class="alert alert-warning">
				<h3 class="text-center">No tiene una ruta de autorizacion creada</h3>	
			</div>			
		</div>
	</div>
	<div class="row justify-content-center mb-3">
			<div class="col-sm-4">
				<a href="{{url('authorization_groups/'.$group->id.'/create')}}" class="btn btn-primary btn-block">Crear</a>
			</div>
		</div>
	
	@endif
</div>


@endsection