@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Balances</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Saldos</h3>
    </div>
</div>

<div class="table-responsive">
    <table id="table" class="table table-bordered">
        <thead class="blue_header">
            <tr>
                <th>Empleado</th>
                <th>No. Empleado</th>
                <th>Periodo</th>
                <th>Beneficio</th>
                <th>Clave Beneficio</th>
                <th>Saldo</th>
                <th>Utilizados</th>
            </tr>
        </thead>
        <tbody>
            @foreach($balances as $balance)
                @if($balance->benefit_id != 5 || $balance->pending != 0)
                    <tr>
                        <td>{{ $balance->user->Fullname }}</td>
                        <td>{{ $balance->user->employee->idempleado }}</td>
                        <td>{{ $balance->year - 1 }} - {{ $balance->year }}</td>
                        <td>{{ $balance->benefit->name }}</td>
                        <td>{{ $balance->benefit->code }}</td>
                        <td>{{ $balance->getBalance() }}</td>
                        <td>{{ $balance->getAmount() }}</td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>

@endsection

@section('scripts')
<script>
    let table = $('#table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                title: 'Reporte_Balances',
            }
        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
    });
</script>
@endsection
