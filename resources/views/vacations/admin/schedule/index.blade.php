@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Horarios</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Administrar Horarios</h3>
    </div>
</div>

<div class="text-right">
    <a href="{{ url('admin/schedule/create') }}" class="btn btn-primary">Crear Horario</a>
</div>

<div>
    <div class="form-group">
        <label>Horario</label>
        <select id="select_schedule" class="form-control" data-live-search="true">
            <option value="" selected disabled>...Seleccione un horario</option>
            @foreach ($regions as $region)
                <option value="{{ $region->id }}">{{ $region->name }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="card card-3 mt-4">
    <div class="card-body">
        <div class="tab-content" id="pills-tabContent">
            @foreach($regions as $region)                
                <div class="tab-pane fade" id="pills-{{ $region->id }}" role="tabpanel" aria-labelledby="pills-{{ $region->id }}-tab">
                    <div class="row justify-content-center mb-4">
                        <div class="col-md-4">
                            <a href="{{ url('admin/schedule/' . $region->id . '/edit') }}" class="btn btn-primary btn-block">Editar</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="blue_header">
                            <tr>
                                <th>Día</th>
                                <th>Entrada</th>
                                <th>Salida</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($region->workshiftFixed as $day)
                                    <tr class="{{ $day->labor?'':'table-secondary' }}">
                                        <td>{{ __('bd.l-'.$day->day) }}</td>
                                        <td>{{ $day->getInTime() }}</td>
                                        <td>{{ $day->getOutTime() }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pull-right">
                        <a title="Ver" alt="Ver" href="{{App::make('url')->to('/')}}/admin/schedule/{{$region->id}}">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a> &nbsp;&nbsp;
                        <a title="Editar" alt="Editar" href="{{App::make('url')->to('/')}}/admin/schedule/{{$region->id}}/edit">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $('#select_schedule').selectpicker();

    $(function(){
        $('#select_schedule').on('change', function(){
            const id = $('#select_schedule').selectpicker().val();
            $('.tab-pane').removeClass('show active');
            $('#pills-' + id).addClass('show active')
        });
    });
</script>    
@endsection