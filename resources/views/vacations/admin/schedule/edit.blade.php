@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Horarios</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Crear Horario</h3>
    </div>
</div>

<form action="{{ route('admin.schedule.update', $region->id) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label for="name">Nombre</label>
                <input type="text" name="name" value="{{ old('name', $region->name) }}" class="form-control" required>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="workshift">Jornada Laboral</label>
                <select name="workshift" class="form-control" required>
                    <option value="5x2" {{ old('workshift', $region->workshift) === '5x2'?'selected':'' }}>Semana de 7 días</option>
                </select>
            </div>
        </div>
    </div>
    <hr>
    @foreach ($days as $key => $day)
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="day" class="w-25 d-inline mx-2">Día</label>
                    <input type="text" class="form-control w-75 d-inline" value="{{ $day }}" disabled>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <div class="custom-control custom-checkbox ">
                        <input type="checkbox" name="labor[{{ $key }}]" value="1" class="custom-control-input cbox-labor" id="cbox_labor[{{ $key }}]" {{ in_old_input(session()->getOldInput(), 'labor', $region->getScheduleIdByDay($key), $key)?'checked':'' }}>
                        <label class="custom-control-label" for="cbox_labor[{{ $key }}]">Laboral</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <label for="schedule" class="w-25 d-inline mx-2">Horario</label>
                <select name="schedule[{{ $key }}]" class="form-control w-75 d-inline select-schedule" {{ in_old_input(session()->getOldInput(), 'schedule', $region->getScheduleIdByDay($key), $key)?:'disabled' }}>
                    <option value="">Eliga un horario</option>
                    @foreach ($schedules as $schedule)
                        <option value="{{ $schedule->id }}" {{ in_old_input(session()->getOldInput(), 'schedule', $region->getScheduleIdByDay($key), $key, in_old_input(session()->getOldInput(), 'schedule', $region->getScheduleIdByDay($key), $key)) == $schedule->id?'selected':'' }}>
                            {{ $schedule->shift }}  /  {{ $schedule->in }} - {{ $schedule->out }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
    @endforeach
    <div class="text-right">
        <input type="submit" value="Guardar" class="btn btn-primary">
        <a href="{{ route('admin.schedule.index') }}" class="btn btn-info">Regresar</a>
    </div>
</form>

@endsection

@section('scripts')
<script>
    $(function(){
        $('.cbox-labor').on('change', function(){
            const isChecked = $(this).is(":checked");
            const row = $(this).closest('.row');
            const select = row.find('.select-schedule');
            if(!isChecked){
                select.prop('selectedIndex', 0);
            }
            select.attr('required', isChecked).attr('disabled', !isChecked);
        });
    });
</script>
@endsection