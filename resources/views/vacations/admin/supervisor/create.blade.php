@extends('layouts.app')

@section('content')

<div class="panel panel-default">  
    <div class="panel-heading panel-heading-custom-{{config('config.theme')}}"><h3  class="panel-title">Crear Supervisor</h3></div>
    <div class="panel-body">
    	<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Numero</th>
						<th>Area</th>
						<th>Departamento</th>
						<th>Puesto</th>
						<th></th>
						<th>Area a supervisar</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr>
						<td>{{$user->getName()}}</td>
						<td>{{$user->number}}</td>
						<td>{{$user->area->name}}</td>
						<td>{{$user->department->name}}</td>
						<td>{{$user->position->name}}</td>
						<td><button class="btn btn-primary">Crear</button></td>
						<td>{!! Form::select('area',$areas,null,['class' => 'form-control'])!!}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
    </div>
</div>

@endsection