@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> ADMINISTRACIÓN</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Beneficios por Puesto</h3>
    </div>
</div>

<div class="text-right mb-3">
    <a href="{{ route('admin.benefitjobs.create') }}" class="btn btn-primary">Crear</a>
</div>

<div class="table-responsive">
    <table class="table table-hover table-bordered" id="incidentsTable">
        <thead class="blue_header">
            <tr>
                <th>Puesto</th>
                <th>Departamento</th>
                <th>Dirección</th>
                <th>Beneficio(s)</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody id="tbody">
            @foreach($jobs as $job)
                <tr>
                    <td>{{ $job->name }}</td>
                    <td>{{ $job->getDepartmentName() }}</td>
                    <td>{{ $job->getDirectionName() }}</td>
                    <td>
                        <ul>
                            @foreach ($job->jobBenefits as $benefit)
                                <li>{{ $benefit->name }}</li>
                            @endforeach
                        </ul>
                    </td>
                    <td>
                        <a href="{{ route('admin.benefitjobs.edit', $job->id) }}" class="btn btn-success">
                            Editar
                        </a>
                        <button class="btn btn-danger btn-remove" data-id="{{ $job->id }}">
                            Quitar
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@section('scripts')
<script>
    let table = $('#incidentsTable').DataTable({
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			},
		});
    $(function(){
        $('tbody').on('click', '.btn-remove', function(){
            if(confirm('¿Estas seguro de remover los beneficios del puesto?')){
                const URL_DELETE = "{{ route('admin.benefitjobs.index') }}";
                const job_id = $(this).data('id');
                const url = URL_DELETE + '/' + job_id;
                const parent_row = $(this).closest('tr');
                axios.delete(url)
                .then(function(response){
                    if(response.data.success){
                        parent_row.fadeOut();
                    }
                });
            }
        });
    });
</script>
@endsection