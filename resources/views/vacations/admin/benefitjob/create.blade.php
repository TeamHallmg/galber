@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> ADMINISTRACIÓN</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Añadir beneficios</h3>
    </div>
</div>

<form action="{{ route('admin.benefitjobs.store') }}" method="POST">
    @csrf
    <div class="row justify-content-center mb-3">
        <div class="col-md-4">
            <input type="submit" value="Guardar" class="btn btn-primary btn-block">
        </div>
    </div>

    <div>
        <div class="form-group">
            <label for="">Puesto </label>
            <select name="job_position_id" class="form-control selectpicker" data-live-search="true">
                <option value="" selected disabled>...Eliga un puesto</option>
                @foreach ($jobs as $job)
                    <option data-subtext="{{ $job->getStructureNames() }}" value="{{ $job->id }}" {{ old('job_position_id') == $job->id?'selected':'' }}>{{ $job->name }} </option>
                @endforeach
            </select>
            <small id="helpId" class="text-muted">Puestos sin ningun beneficio asignado</small>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-bordered">
            <thead class="blue_header">
                <tr>
                    <th>#</th>
                    <th>Beneficio</th>
                    <th>Nombre Corto</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($benefits as $benefit)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $benefit->name }}</td>
                        <td>{{ $benefit->shortname }}</td>
                        <td>
                            <input type="checkbox" name="benefits[]" value="{{ $benefit->id }}" {{ old('benefits.'.$loop->index) == $benefit->id?'checked':'' }}>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</form>

    
@endsection