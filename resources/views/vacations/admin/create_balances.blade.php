@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Balances</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Crear Saldos</h3>
    </div>
</div>
<form action="{{ route('admin.incidents.balances.store') }}" method="POST">
    @csrf
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Empleados</label>
                <select name="user_id" id="select_users" class="form-control selectpicker" data-live-search="true" required>
                    <option value="">Eliga un empleado</option>
                    @foreach ($users as $key => $user)
                        <option value="{{ $key }}" {{ old('user_id') == $key?'selected':'' }}>{{ $user }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-6"> 
            <div class="form-group">
                <label>Beneficios</label>
                <select name="benefit_id" id="" class="form-control" required>
                    <option value="{{ $benefit->id }}">{{ $benefit->name }}</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="year">Periodo</label>
                <input type="number" name="year" value="{{ old('year') }}" id="txt-year" readonly class="form-control" required>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="pending">Días</label>
                <input type="number" name="pending" value="{{ old('pending', 1) }}" min="1" class="form-control" required>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="until">Vigencía</label>
                <input type="date" name="until" value="{{ old('until') }}" class="form-control" required>
            </div>
        </div>
    </div>
    <div class="text-right my-3">
        <input type="submit" value="Guardar" class="btn btn-primary">
    </div>
</form>


@endsection

@section('scripts')
<script>
    $(function(){
        $('#select_users').on('change', function(){
            const user_id = $(this).val();
            const url = "{{ url('admin/balances/user_year') }}";
            $('#txt-year').val('').prop('readonly', 'true');

            if(user_id){
                const getUrl = url + "/" + user_id;
                axios.get(getUrl)
                .then(function(response){
                    if(response.data.success){
                        $('#txt-year').val(response.data.year);
                    }
                })
                .catch(function(error){

                });
            }
        });
    });
</script>
@endsection
