@extends('vacations.app')

@section('content')
<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Exportación de archivos</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Previsualización del archivo Vacaciones (Hasta la fecha: {{ $endDate }})</h3>
    </div>
</div>

<div class="table-responsive">
    <table class="table" id="tablevac">
        <thead>
            <tr>
                <td>EMP_VACAC</td>
                <td>FEDE_VACAC</td>
                <td>DD_VACAC</td>
                <td>PM_VACAC</td>
            </tr>
        </thead>
        <tbody>
            @foreach($events as $event)
                @foreach ($event->eventDays as $day)
                    <tr>
                        <td>{{ $event->user->employee->idempleado }}</td>
                        <td>{{ $day->date }}</td>
                        <td>1</td>
                        <td>0.25</td>
                    </tr>
                @endforeach
            @endforeach

        </tbody>
    </table>
</div>
<hr>
<form action="{{ route('admin.exports.vacations') }}" method="POST">
    @csrf
    <input type="hidden" name="end" value="{{ $endDate }}">
    <div class="custom-control custom-checkbox text-right">
        <input type="checkbox" name="process" class="custom-control-input" id="defaultUnchecked" {{ $process?'checked':'' }}>
        <label class="custom-control-label" for="defaultUnchecked">Procesar información</label>
    </div>
    <div class="text-center">
        <input type="submit" value="Exportar" class="btn btn-primary">
    </div>
</form>

@endsection

@section('scripts')
<script>
    let table = $('#tablevac').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
    });
</script>
@endsection