@extends('vacations.app')

@section('content')
<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Exportación de archivos</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Exportar {{ $title }}</h3>
    </div>
</div>

<div class="alert alert-warning alert-important alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        <span class="sr-only">Close</span>
    </button>
    Si duda sobre la información a procesar, exporte sin marcar la casilla
</div>

<form action="{{ $url }}" method="POST">
    @csrf
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="start">Fecha Inicio</label>
                <input type="date" id="start" name="start" value="{{ $date }}" class="form-control" readonly>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="end">Fecha Final </label>
                <input type="date" id="end" name="end" class="form-control" required>
            </div>
        </div>
    </div>
    <div class="custom-control custom-checkbox text-right">
        <input type="checkbox" name="process" class="custom-control-input" id="defaultUnchecked">
        <label class="custom-control-label" for="defaultUnchecked">Procesar información</label>
    </div>
    <div class="text-center">
        <input type="submit" value="Exportar" class="btn btn-primary">
    </div>
</form>


@endsection
