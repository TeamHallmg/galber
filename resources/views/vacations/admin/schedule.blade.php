@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> VER SOLICITUDES</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">
            Solicitudes por semana
        </h3>
    </div>
</div>

<div class="row justify-content-center mb-2">
    <div class="col-12 text-center">
        <div class="input-group d-flex justify-content-center mb-3">
            <div class="input-group-prepend">
                <a class="btn btn-outline-secondary" href="{{ route('super.view.schedule', ['last_date' => $week['last_date'], 'direction' => 'backward']) }}" id="button-addon1"><i class="fas fa-angle-left"></i></a>
            </div>
            <span class="input-group-text"><strong>{{ $week['readeable_start'] }}</strong> - <strong>{{ $week['readeable_end'] }}</strong></span>
            <div class="input-group-append">
                <a class="btn btn-outline-secondary" href="{{ route('super.view.schedule', ['last_date' => $week['last_date'], 'direction' => 'forward']) }}" id="button-addon2"><i class="fas fa-angle-right"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="table-responsive">
    <table id="tableRequest" class="table table-bordered w-100">
        <thead class="blue_header">
            <tr class="text-center">
                {{-- <th>Jefe</th> --}}
                <th class="d-none">Empleado</th>
                @foreach($week_days as $day)
                    <th>{{ $day['name'] }}<br> {{ $day['readeable'] }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach ($employees_week as $employee)
                <tr>
                    {{-- <td>{{ $request->user->FullName }}</td> --}}
                    <td class="d-none">{{ $employee['name'] .' - '. $employee['jobPosition'] }}</td>
                    @foreach($week_days as $day)
                        @if(isset($employee[$day['name']]))
                            <td class="{{ $employee[$day['name']]['class'] }}" colspan="{{ $employee[$day['name']]['colspan'] }}" data-eventday="{{$employee[$day['name']]['eventDay']->id}}">{{ $employee[$day['name']]['event']->title }}</td>
                        @else
                            <td class="bg-secondary"></td>
                        @endif
                    @endforeach
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    var tableRequest = $('#tableRequest').DataTable({
        order: [],
        rowGroup: {
            dataSrc: 0
        }
    });
</script>

@endsection
