@extends('layouts.app')

@section('content')

<div class="card card-3">
    <div class="card-header">
        REGIONES
    </div>
    <div class="card-body">    
        <div class="row justify-content-center">
            <div class="col-md-4">
                <a href="{{ url('regions/create') }}" class="btn btn-primary btn-block">Crear</a>
            </div>
        </div>
        <div class="table-responsive mt-4">
            <table class="table" id="regionsTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Tipo de Jornada</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($regions as $region)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $region->name }}</td>
                            <td>{{ $region->workshift }}</td>
                            <td>
                                <a href="{{ url('regions/' . $region->id . '/edit') }}" class="btn btn-primary">Editar</a>
                                {{-- <form action="{{ url('regions/' . $region->id) }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="submit" value="Eliminar" class="btn btn-danger" onclick="return confirm('Estas seguro de eliminar la región??')">
                                </form> --}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
    
@endsection

@section('scripts')
<script>
    var regionsTable = $('#regionsTable').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
    });
</script>
@endsection