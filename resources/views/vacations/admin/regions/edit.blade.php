@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card card-3">
            <div class="card-header">
                Editar región
            </div>
            <div class="card-body">
                <form action="{{ url('regions/' . $region->id) }}" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" name="name" class="form-control" placeholder="Nombre de la region" value="{{ $region->name }}" required>
                    </div>
                    <div class="form-group">
                        <label for="workshift">Tipo de Jornada</label>
                        <select name="workshift" class="form-control selectpicker" required>
                            <option value="5x2" {{ $region->workshift == "5x2"?'selected':'' }}>5 x 2</option>
                            <option value="6x2" {{ $region->workshift == "6x2"?'selected':'' }}>6 x 2</option>
                        </select>
                    </div>
                    <div class="text-right">
                        <input type="submit" value="Guardar" class="btn btn-success">
                        <a href="{{ url('regions') }}" class="btn btn-primary">Regresar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection