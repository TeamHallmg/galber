@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Reloj Checador</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Historial de checadas</h3>
    </div>
</div>

<div class="row mb-2">
    <div class="col-md-5">
        <div class="form-group">
            <label>Departamento</label>
            <select id="departmentSelect" class="selectpicker form-control" data-live-search="true">
                <option value="">Todos los departamentos</option>
                @foreach ($departments as $department)
                    <option value="{{ $department->id }}" data-subtext="{{ $department->getDirectionName() }}"> {{ $department->name }} </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group" id="divJobs">
            <label>Puesto</label>
            <select id="jobSelect" class="selectpicker form-control" data-live-search="true" disabled>
                <option value="">Seleccione el departamento</option>
            </select>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label>Mostrar Inactivos</label>
            <input type="checkbox" id="cboxActive">
        </div>
    </div>
</div>

<div class="row mb-4">
    <div class="col-md-10">
        <div class="input-daterange input-group" id="datepicker">
            <input type="date" class="form-control date_users datepicker-class" id="date_ubegin" name="start" required="required" />
            <span class="input-group-text">al</span>
            <input type="date" class="form-control date_users datepicker-class" id="date_uend" name="end" required="required" />
            <button class="btn btn-primary" onclick="cleanDateInputs()">Limpiar</button>
        </div>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-bordered" id="logsTable">
        <thead class="blue_header">
            <tr>
                <th>Empleado</th>
                <th>Departamento</th>
                <th>dept_id</th>
                <th>Puesto</th>
                <th>job_id</th>
                <th>Fecha</th>
                <th>Hora de checada</th>
                <th>Hora comparativa</th>
                <th>Tipo de checada</th>
                <th>Plataforma</th>
                <th>Ubicación</th>
                <th>active</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($logs as $log)
                <tr>
                    <td>{{ $log->user->FullName }}</td>
                    <td>{{ $log->user->employee_wt->getDepartmentName() }}</td>
                    <td>{{ $log->user->employee_wt->getDepartmentId() }}</td>
                    <td>{{ $log->user->employee_wt->getPuestoName() }}</td>
                    <td>{{ $log->user->employee_wt->getPuestoId() }}</td>
                    <td data-sort="{{ $log->getDateOnFormat('YmdHis') }}">{{ $log->getDateOnFormat('d-m-Y') }}</td>
                    <td>{{ $log->getDateOnFormat('H:i') }}</td>
                    <td>{{ $log->compare_time }}</td>
                    <td>{{ $log->getTypeInfo() }}</td>
                    <td>{{ $log->from }}</td>
                    <td>{{ $log->address }}</td>
                    <td>{{ $log->user->trashed()?'sd_inactive':'sd_active' }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Comentarios</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <ul id="modal_body">

                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    const jobUrl = "{{ url('jobsFromDepartment') }}";
    let logsTable = $('#logsTable').DataTable({
        "language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        dom: 'B<"row display-content-between mt-3"<"col"l><"col"f>>rtip',
        buttons: [
            {
                extend: 'excel',
                text: 'Exportar Excel',
                title: 'Historico de Checadas',
            }
        ],
        columnDefs: [
            {
                targets: [2, 4, 11],
                visible: false,
            }
        ]
    });

    function cleanDateInputs() {
		document.getElementById('date_ubegin').value = '';
		document.getElementById('date_uend').value = '';
		logsTable.draw();
    }
    
    $(function(){
        $('tbody').on('click', '.btn-comment', function(){
            const comment_json = $(this).data('comments');
            if(typeof(comment_json) === "object"){
                $('#modal_body').empty();
                for (const comment of comment_json) {
                    $('#modal_body').append(`<li>${comment.comment} - ${comment.created_at}</li>`)
                }
            }
        });

        $('#departmentSelect').on('change', function(){
            const departmentID = $(this).val();
            if(departmentID){
                axios.get(jobUrl, {
                    params:{ 
                        department: departmentID,
                    }
                })
                .then(response => {
                    const jobs = response.data;
                    var sel = $('<select>');
                        sel.append($("<option>").attr('value','').text('Todos los puestos del departamento'));
                    $(jobs).each(function() {
                        sel.append($("<option>").attr('value',this.id).text(this.name));
                    });
                    $('#jobSelect').selectpicker('destroy').remove();
                    sel.attr('id', 'jobSelect').addClass('selectpicker form-control').attr('data-live-search', "true");
                    $('#divJobs').append(sel);
                    sel.selectpicker();
                })
                .catch(error => {

                });
            }else{
                $('#jobSelect').selectpicker('destroy').remove();
                var sel = $('<select>');
                sel.append($('<option>').attr('value', '').text('Seleccione el departamento'));
                sel.attr('id', 'jobSelect').addClass('selectpicker form-control').attr('data-live-search', "true").attr('disabled', true);
                $('#divJobs').append(sel);
                    sel.selectpicker();
            }
        });

        $('#departmentSelect').on('change', refreshTable);
        $('#divJobs').on('change', '#jobSelect', refreshTable);
        $('#cboxActive').on('change', refreshTable);

        function refreshTable(){
            logsTable.draw();
        }        

        $('.date_users').on('change', function () {
			var dateBegin = new Date($('#date_ubegin').val());
			var dateEnd = new Date($('#date_uend').val());
			if(!isNaN(dateBegin) && !isNaN(dateEnd)){
				if(dateEnd < dateBegin){
					$('#date_uend').val($('#date_ubegin').val());
				}
			}else if(!isNaN(dateBegin) && isNaN(dateEnd)){
				$('#date_uend').val($('#date_ubegin').val());

			}else if(isNaN(dateBegin) && !isNaN(dateEnd)){
				$('#date_ubegin').val($('#date_uend').val());
			}
			logsTable.draw();
		});

        $.fn.dataTableExt.afnFiltering.push(
            function(oSettings, aData, iDataIndex) {
                const department = $('#departmentSelect').val().trim();
                const job = $('#jobSelect').val().trim();
                const isActive = $('#cboxActive').prop("checked");

                let min = new Date( $('#date_ubegin').val() );
                let max = new Date( $('#date_uend').val() );
                const dateArray = aData[5].split('-');
                let date = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0];
		        date = new Date( date ) || 0; // Fecha

                if(isActive && aData[11] === "sd_inactive" || aData[11] === 'sd_active'){
                    if(!department){
                        if( !isNaN(min) && !isNaN(max)){
                            return min <= date && date <= max;
                        }
                        return true;  
                    }else{
                        if(!job){
                            if (typeof aData[2] === "string" && typeof department === "string"){
                                if(aData[2] === department){
                                    if( !isNaN(min) && !isNaN(max)){
                                        return min <= date && date <= max;
                                    }
                                    return true;
                                }
                            }
                        }else{
                            if (typeof aData[2] === "string" && typeof department === "string" && (typeof aData[4] === "string" && typeof job === "string")){
                                if(aData[2] === department && aData[4] === job){
                                    if( !isNaN(min) && !isNaN(max)){
                                        return min <= date && date <= max;
                                    }
                                    return true;
                                }
                            }
                        }
                    }
                }
                return false;
            }
        );
    });
</script>
@endsection
