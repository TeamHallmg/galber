@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> VER SOLICITUDES</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Solicitudes</h3>
    </div>
</div>

<div class="table-responsive">
    <table id="tableRequest" class="table table-bordered">
        <thead class="blue_header">
            <tr>
                <th>Jefe</th>
                <th>Empleado</th>
                <th>Tipo</th>
                <th>Estatus</th>
                <th>Nota</th>
                <th>Inicio del Evento</th>
                <th>Fin del Evento</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($requests as $request)
                <tr>
                    <td>{{ $request->user->FullName }}</td>
                    <td>{{ $request->from->FullName }}</td>
                    <td>{{ $request->benefit->name }}</td>
                    <td>{{ __('incidents.' . $request->status) }}</td>
                    <td>{{ $request->info }}</td>
                    <td>{{ $request->event->getStartDate('d/m/Y') }}</td>
                    <td>{{ $request->event->getEndDate('d/m/Y') }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    function cleanDateInputs() {
		document.getElementById('date_ubegin').value = '';
		document.getElementById('date_uend').value = '';
		tableRequest.draw();
	}

    var tableRequest = $('#tableRequest').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        order: [],
        colReorder: true,
        dom: 'B<"row"<"col-md-3"><"col-md-6"<"#toolbar">><"col-md-3"f>>rtip',
        buttons: [
            {
                extend: 'csvHtml5',
                title: 'Listado_incidencias',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                }
            },
        ]
    });

    $(function(){
        $("div#toolbar").html('<div class="input-daterange input-group" id="datepicker">' +
            '<input type="date" class="form-control date_users datepicker-class" id="date_ubegin" name="start" required="required" />' +
            '<span class="input-group-text" id="basic-addon1">al</span>' +
            '<input type="date" class="form-control date_users datepicker-class" id="date_uend" name="end" required="required" />' +
            '<button class="btn btn-primary" onclick="cleanDateInputs()">Limpiar</button>'+
            '</div>'
        );


        $.fn.dataTable.ext.search.push(
		    function( settings, data, dataIndex ) {
		        var min = new Date( $('#date_ubegin').val() );
                var max = new Date( $('#date_uend').val() );
                var start = data[5].split('/');
                var end = data[6].split('/');
                start = start[2] + '-' + start[1] + '-' + start[0];
                end = end[2] + '-' + end[1] + '-' + end[0];
		        var start = new Date( start ) || 0; // Fecha de Inicio
                var end = new Date( end ) || 0; // Fecha de Fin
		 		if( isNaN(min) && isNaN(max) ||
                    min <= start && start <= max ||
                    min <= end && end <= max ||
                    (start <=  min && max <= end && start <= max && min <= max)){
		 			return true;
		 		}
		        return false;
		    }
		);

        $('.date_users').on('change', function () {
			var dateBegin = new Date($('#date_ubegin').val());
			var dateEnd = new Date($('#date_uend').val());
			if(!isNaN(dateBegin) && !isNaN(dateEnd)){
				if(dateEnd < dateBegin){
					$('#date_uend').val($('#date_ubegin').val());
				}
			}else if(!isNaN(dateBegin) && isNaN(dateEnd)){
				$('#date_uend').val($('#date_ubegin').val());

			}else if(isNaN(dateBegin) && !isNaN(dateEnd)){
				$('#date_ubegin').val($('#date_uend').val());
			}
			tableRequest.draw();
		});
    });
    
    function destroy(button,id){

        var r = confirm('¿Esta seguro de eliminar la solicitud?');
        if(r){
            $.ajax({
                headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
                url: "/common/plea/"+id,
                async: false,
                method: "DELETE",
                dataType: "json"
            }).done(function( data ) {
                if(data.success == 1) {
                    $('#alert').html(
                        '<div class="alert alert-warning alert-dismissible fade in" role="alert">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                            '<span aria-hidden="true">&times;</span>'+
                        '</button>'+
                        '<strong>Registro cancelado.</strong>'+
                        '</div>'
                    );
                } else {
                    $('#alert').html(
                        '<div class="alert alert-warning alert-dismissible fade in" role="alert">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                            '<span aria-hidden="true">&times;</span>'+
                        '</button>'+
                        '<strong>'+data.error+'</strong>'+
                        '</div>'
                    );
                }
            });
        }
    }
</script>

@endsection
