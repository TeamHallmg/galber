@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Incidencias</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Administrar Incidencias</h3>
    </div>
</div>

<div class="text-right mb-3">
    <a href="{{ url('admin/incidents/create') }}" class="btn btn-primary">Crear Incidencia</a>
</div>
<div class="table-responsive">
    <table class="table table-hover table-bordered" id="tableIncidents">
        <thead class="blue_header">
            <tr>
            <th>Código</th>
            <th>Nombre corto</th>
            <th>Descripción</th>
            <th>Grupo</th>
            <th>Operaciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($benefits as $benefit)
                <tr>
                    <td>{{ $benefit->code }}</td>
                    <td>{{ $benefit->shortname }}</td>
                    <td>{{ $benefit->name }}</td>
                    <td>{{ __('bd.benefit-group-' . $benefit->group) }}</td>
                    <td>
                        <a title="Editar" alt="Editar" class="btn btn-success" href="{{ url('admin/incidents/' . $benefit->id . '/edit') }}">
                            <i class="fa fa-pencil-alt" aria-hidden="true"></i>
                        </a>
                        {{-- <a title="Eliminar" alt="Eliminar" class="btn btn-danger" href="{{ url('admin/incidents/' . $benefit->id) }}">
                            Eliminar
                        </a> --}}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@section('scripts')
<script>
    let table = $('#tableIncidents').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
    });
</script>
@endsection
