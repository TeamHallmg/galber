<div class="row my-3">
    <div class="col-md-12">
        <label for='name'>Texto</label>
        <input type="text" name='name' class='form-control' value="{{ isset($benefit)?$benefit->name:old('name') }}" required>
        <input type="hidden" name='relationship' value="1">
    </div>
</div>
<div class="row my-3">
    <div class="col-md-3">
        <label for='code'>Código</label>
        <input type="text" name='code' class='form-control' value="{{ isset($benefit)?$benefit->code:old('code') }}" required maxlength='10'>
    </div>
    <div class="col-md-3">
        <label for='shortname'>Nombre corto</label>
        <input type="text" name='shortname' class='form-control' value="{{ isset($benefit)?$benefit->shortname:old('shortname') }}" required maxlength='10'>
    </div>
    <div class="col-md-3">
        <label for='context'>Contexto</label>
        @if(isset($benefit))
            <select name="context" class="form-control">
                <option value="admin" {{ old('context', $benefit->context) == 'admin'?'selected':'' }}>Administrador</option>
                <option value="user" {{ old('context', $benefit->context) == 'user'?'selected':'' }}>Usuario</option>
                <option value="super" {{ old('context', $benefit->context) == 'super'?'selected':'' }}>Supervisor</option>
            </select>
        @else
            <select name="context" class="form-control">
                <option value="admin" {{ old('context') == 'admin'?'selected':'' }}>Administrador</option>
                <option value="user" {{ old('context') == 'user'?'selected':'' }}>Usuario</option>
                <option value="super" {{ old('context') == 'super'?'selected':'' }}>Supervisor</option>
            </select>
        @endif
    </div>
    <div class="col-md-3">
        <label for='type'>Comportamiento</label>
        @if(isset($benefit))
            <select name="type" class="form-control">
                <option value="time" {{ old('type', $benefit->type) == 'time'?'selected':'' }}>Tiempo</option>
                <option value="day" {{ old('type', $benefit->type) == 'day'?'selected':'' }}>Día</option>
                <option value="pending" {{ old('type', $benefit->type) == 'pending'?'selected':'' }}>Pendiente</option>
            </select>
        @else
            <select name="type" class="form-control">
                <option value="time" {{ old('type') == 'time'?'selected':'' }}>Tiempo</option>
                <option value="day" {{ old('type') == 'day'?'selected':'' }}>Día</option>
                <option value="pending"{{ old('type') == 'pending'?'selected':'' }} >Pendiente</option>
            </select>
        @endif

    </div>
</div>
<div class="row mt-4 justify-content-center">
    @if(isset($benefit))
        <div class="col-md-2">
            <input type="hidden" name='continuous' value="0">
            <div class="custom-control custom-checkbox ">
                <input type="checkbox" name="continuous" value="1" class="custom-control-input" id="cbox_continuous" {{ old('continuous', $benefit->continuous) == '1'?'checked':'' }}>
                <label class="custom-control-label" for="cbox_continuous">Continuo</label>
            </div>
        </div>
        <div class="col-md-2">
            <input type="hidden" name='cronjob' value="0">   
            <div class="custom-control custom-checkbox ">
                <input type="checkbox" name="cronjob" value="1" class="custom-control-input" id="cbox_cronjob" {{ old('cronjob', $benefit->cronjob) == '1'?'checked':'' }}>
                <label class="custom-control-label" for="cbox_cronjob">Cron</label>
            </div>
        </div>
        <div class="col-md-2">
            <input type="hidden" name='remove' value="0">
            <div class="custom-control custom-checkbox ">
                <input type="checkbox" name="remove" value="1" class="custom-control-input" id="cbox_remove" {{ old('remove', $benefit->remove) == '1'?'checked':'' }}>
                <label class="custom-control-label" for="cbox_remove">Vence</label>
            </div>
        </div>
        <div class="col-md-2">
            <input type="hidden" name='blocked' value="0">
            <div class="custom-control custom-checkbox ">
                <input type="checkbox" name="blocked" value="1" class="custom-control-input" id="cbox_blocked" {{ old('blocked', $benefit->blocked) == '1'?'checked':'' }}>
                <label class="custom-control-label" for="cbox_blocked">Bloquea</label>
            </div>
        </div>
        <div class="col-md-2"> 
            <input type="hidden" name='report' value="0">
            <div class="custom-control custom-checkbox ">
                <input type="checkbox" name="report" value="1" class="custom-control-input" id="cbox_report" {{ old('report', $benefit->report) == '1'?'checked':'' }}>
                <label class="custom-control-label" for="cbox_report">Reporta</label>
            </div>
            @if(config('config.export') == 'flexiform')
                <label for='retype'>Tipo</label>
                <select name="retype">
                    <option value="sum" {{ $benefit->retype == 'sum'?'selected':'' }}>Suma</option>
                    <option value="week" {{ $benefit->retype == 'week'?'selected':'' }}>Semana</option>
                </select>
            @endif
        </div>
    @else
        <div class="col-md-2">
            <input type="hidden" name='continuous' value="0">
            <div class="custom-control custom-checkbox ">
                <input type="checkbox" name="continuous" value="1" class="custom-control-input" id="cbox_continuous" {{ old('continuous') == '1'?'checked':'' }}>
                <label class="custom-control-label" for="cbox_continuous">Continuo</label>
            </div>
        </div>
        <div class="col-md-2">
            <input type="hidden" name='cronjob' value="0">
            <div class="custom-control custom-checkbox ">
                <input type="checkbox" name="cronjob" value="1" class="custom-control-input" id="cbox_cronjob" {{ old('cronjob') == '1'?'checked':'' }}>
                <label class="custom-control-label" for="cbox_cronjob">Cron</label>
            </div>
        </div>
        <div class="col-md-2">
            <input type="hidden" name='remove' value="0">
            <div class="custom-control custom-checkbox ">
                <input type="checkbox" name="remove" value="1" class="custom-control-input" id="cbox_remove" {{ old('remove') == '1'?'checked':'' }}>
                <label class="custom-control-label" for="cbox_remove">Cron</label>
            </div>
        </div>
        <div class="col-md-2">
            <input type="hidden" name='blocked' value="0">
            <div class="custom-control custom-checkbox ">
                <input type="checkbox" name="blocked" value="1" class="custom-control-input" id="cbox_blocked" {{ old('blocked') == '1'?'checked':'' }}>
                <label class="custom-control-label" for="cbox_blocked">Bloquea</label>
            </div>
        </div>
        <div class="col-md-2"> 
            <input type="hidden" name='report' value="0">
            <div class="custom-control custom-checkbox ">
                <input type="checkbox" name="report" value="1" class="custom-control-input" id="cbox_report" {{ old('report') == '1'?'checked':'' }}>
                <label class="custom-control-label" for="cbox_report">Reporta</label>
            </div>
            @if(config('config.export') == 'flexiform')
                <label for='retype'>Tipo</label>
                <select name="retype">
                    <option value="sum" {{ old('retype') == 'sum'?'selected':'' }}>Suma</option>
                    <option value="week" {{ old('retype') == 'week'?'selected':'' }}>Semana</option>
                </select>
            @endif
        </div>
    @endif
</div>
<div class="row my-3">
    <div class="col-md-3">
        <label for='days'>Días</label>
        <input type="text" name='days' class='form-control' value="{{ isset($benefit)?$benefit->days:old('days') }}" required maxlength='10'>
    </div>
    <div class="col-md-3">
        <label for='group'>Grupo</label>
        @if(isset($benefit))
            <select name="group" class="form-control">
                <option value="incident" {{ old('group', $benefit->group) == 'incident'?'selected':'' }}>Incidencia</option>
                <option value="benefit" {{ old('group', $benefit->group) == 'benefit'?'selected':'' }}>Prestación</option>
                <option value="incapacity" {{ old('group', $benefit->group) == 'incapacity'?'selected':'' }}>Incapacidad</option>
            </select>
        @else
            <select name="group" class="form-control">
                <option value="incident" {{ old('group') == 'incident'?'selected':'' }}>Incidencia</option>
                <option value="benefit" {{ old('group') == 'benefit'?'selected':'' }}>Prestación</option>
                <option value="incapacity" {{ old('group') == 'incapacity'?'selected':'' }}>Incapacidad</option>
            </select>
        @endif
    </div>
    <div class="col-md-3">
        <label for='frequency'>Frecuencia</label>
        @if(isset($benefit))
            <select name="frequency" class="form-control">
                <option value="yearly" {{ old('frequency', $benefit->frequency) == 'yearly'?'selected':'' }}>Anual</option>
                <option value="monthly" {{ old('frequency', $benefit->frequency) == 'monthly'?'selected':'' }}>Mensual</option>
                <option value="weekly" {{ old('frequency', $benefit->frequency) == 'weekly'?'selected':'' }}>Semanal</option>
                <option value="daily" {{ old('frequency', $benefit->frequency) == 'daily'?'selected':'' }}>Diario</option>
            </select>
        @else
            <select name="frequency" class="form-control">
                <option value="yearly" {{ old('frequency') == 'yearly'?'selected':'' }}>Anual</option>
                <option value="monthly" {{ old('frequency') == 'monthly'?'selected':'' }}>Mensual</option>
                <option value="weekly" {{ old('frequency') == 'weekly'?'selected':'' }}>Semanal</option>
                <option value="daily" {{ old('frequency') == 'daily'?'selected':'' }}>Diario</option>
            </select>
        @endif
    </div>
    <div class="col-md-3">
        <label for='gender'>Sexo</label>
        @if(isset($benefit))
            <select name="gender" class="form-control">
                <option value="x" {{ old('gender', $benefit->gender) == 'x'?'selected':'' }}>Indistinto</option>
                <option value="m" {{ old('gender', $benefit->gender) == 'm'?'selected':'' }}>Masculino</option>
                <option value="f" {{ old('gender', $benefit->gender) == 'f'?'selected':'' }}>Femenino</option>
            </select>
        @else
            <select name="gender" class="form-control">
                <option value="x" {{ old('gender') == 'x'?'selected':'' }}>Indistinto</option>
                <option value="m" {{ old('gender') == 'm'?'selected':'' }}>Masculino</option>
                <option value="f" {{ old('gender') == 'f'?'selected':'' }}>Femenino</option>
            </select>
        @endif
    </div>
</div>
<div class="row mt-5">
    <div class="col-12 mb-4">
        <h5 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Antigüedad</h5>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <select name="select_seniority" class="form-control select_input" id="seniority">
                <option value="">Elija un tipo...</option>
                <option value="day" {{ set_selected('day', isset($benefit)?$benefit->timeDescription:'', 'seniority_type_of_time') }}>Días</option>
                <option value="week" {{ set_selected('week', isset($benefit)?$benefit->timeDescription:'', 'seniority_type_of_time') }}>Semanas</option>
                <option value="month" {{ set_selected('month', isset($benefit)?$benefit->timeDescription:'', 'seniority_type_of_time') }}>Meses</option>
                <option value="year" {{ set_selected('year', isset($benefit)?$benefit->timeDescription:'', 'seniority_type_of_time') }}>Años</option>
            </select>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <input type="number" name="number_seniority" id="number_seniority" data-value="{{ isset($benefit)?optional($benefit->timeDescription)->seniority_quantity:'' }}" class="form-control" min="1" disabled>
        </div>
    </div>
    <div class="col-12 mb-4">
        <h5 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Periodo de uso</h5>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <select name="select_period" class="form-control select_input" id="period">
                <option value="">Elija un tipo...</option>
                <option value="month" {{ set_selected('month', isset($benefit)?$benefit->timeDescription:'', 'period_use_type_of_time') }}>Mes (Días)</option>
                <option value="month-hour" {{ set_selected('month-hour', isset($benefit)?$benefit->timeDescription:'', 'period_use_type_of_time') }}>Mes (Horas)</option>
                <option value="year" {{ set_selected('year', isset($benefit)?$benefit->timeDescription:'', 'period_use_type_of_time') }}>Año (Días)</option>
                <option value="year-hour" {{ set_selected('year-hour', isset($benefit)?$benefit->timeDescription:'', 'period_use_type_of_time') }}>Año (Horas)</option>
                
            </select>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <input type="number" name="number_period" class="form-control" data-value="{{ isset($benefit)?optional($benefit->timeDescription)->period_use_quantity:'' }}" id="number_period" min="1" disabled>
        </div>
    </div>
    <div class="col-12 mb-4">
        <h5 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Máximo por solicitud</h5>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <select name="select_max" class="form-control select_input" id="max">
                <option value="">Elija un tipo...</option>
                <option value="hour" {{ set_selected('hour', isset($benefit)?$benefit->timeDescription:'', 'use_max_type_of_time') }}>Hora</option>
                <option value="day" {{ set_selected('day', isset($benefit)?$benefit->timeDescription:'', 'use_max_type_of_time') }}>Día</option>
            </select>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <input type="number" name="number_max" class="form-control" data-value="{{ isset($benefit)?optional($benefit->timeDescription)->use_max_quantity_per_time:'' }}" id="number_max" min="1" disabled>
        </div>
    </div>
</div>

@section('scripts')
<script>
    $('.select_input').on('change', function(){
        const value = $(this).val();
        const id = $(this).attr('id');
        console.log('number_'+id);
        $('#number_' + id).prop('disabled', !value).prop('required', !!value).val(!value?'':1);
    });

    function onload(){
        $('.select_input').each((index, elem) => {
            const val = $(elem).val();
            const id = $(elem).attr('id');
            const value = $('#number_' + id).data('value');
            if(val){
                $('#number_' + id).prop('disabled', false).prop('required', true).val(value);
            }
        });
    }

    onload();
</script>
@endsection