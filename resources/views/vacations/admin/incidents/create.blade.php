@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Incidencias</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Crear Incidencias</h3>
    </div>
</div>

<form action="{{ url('admin/incidents') }}" method="POST">
    @csrf
    @include('vacations.admin.incidents.form')
    <div class="text-right">
        <input type="submit" value="Crear" class="btn btn-success">
        <a href="{{ url('admin/incidents') }}" class="btn btn-primary">Regresar</a>
    </div>
</form>

@endsection
