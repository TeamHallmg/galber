@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <span class="panel-custom-first-{{config('config.theme')}} incidents-icon"><h3 class="panel-title"> Incidencias</h3></span><span class="panel-custom-second-{{config('config.theme')}}"></span>
    </div>

    <div class="card-body">
        <p class="lead">Info</p>
        <div class="row">
            <form action="{{ url('admin/incidents' . $benefit->id) }}" method="POST">
                @csrf
                @method('DELETE')
                <div class="col-md-4 col-md-offset-4">
                    <input type="submit" value="Eliminar" class="btn btn-block btn-danger">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading panel-heading-custom-{{config('config.theme')}}"><h3  class="panel-title">Registro</h3></div>
    <div class="panel-body">
        <form action="{{ url('admin/incidents' . $benefit->id) }}" method="GET">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <label for="name">Texto</label>
                    <input type="text" name="name" class="form-control" readonly>
                    {!! Form::label('name', 'Texto') !!}<br />
                </div>
            </div>
            <div class="row"><br /></div>
            <div class="row">

                <div class="col-md-3">
                    {!! Form::label('code', 'Código') !!}<br />
                    {!! Form::text('code', null, ['class' => 'form-control ','maxlength'=>'10','readonly'=>'readonly']) !!}
                </div>
                <div class="col-md-3">
                    {!! Form::label('shortname', 'Nombre corto') !!}<br />
                    {!! Form::text('shortname', null, ['class' => 'form-control','maxlength'=>'10','readonly'=>'readonly']) !!}
                </div>
                <div class="col-md-3">
                    {!! Form::label('context', 'Contexto') !!}<br />
                    {!! Form::text('context',null,['class' => 'form-control','maxlength'=>'10','readonly'=>'readonly']) !!}
                </div>
                <div class="col-md-3">
                    {!! Form::label('type', 'Comportamiento') !!}<br />
                    {!! Form::text('type',null,['class' => 'form-control','maxlength'=>'10','readonly'=>'readonly']) !!}
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
