@extends('layouts.app')

@section('content')

<div class="card">
	<div class="card-header">
		<h4>Grupos</h4>		
	</div>
	<div class="card-body">
		<div class="row justify-content-center mb-4">
			<div class="col-md-4">
				<a href="{{url('groups/create')}}" class="btn btn-primary btn-block">Crear Grupo <i class="fas fa-users fa-lg ml-1"></i></a>
			</div>
		</div>
		<table class="table" id="groupsTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Nombre</th>
					<th>Personal Recomendado</th>
					<th>Personal Activo</th>
					<th>Ruta de Autorización</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($groups as $group)
					<tr>
						<td>{{$loop->iteration}}</td>
						<td>{{$group->name}}</td>
						<td>{{$group->recommended_staff}}</td>
						<td>{{$group->users->count()}}</td>
						<td>{{($group->authorizationRoutes->count() > 0)?'Si':'No'}}</td>
						<td>
							<a href="{{url('groups/'.$group->id.'/edit')}}" class="btn btn-primary"><i class="fas fa-edit"></i> Editar</a>
							<form class="my-0" action="{{ url('groups/' . $group->id) }}" method="POST">
								@csrf
								<input type="hidden" name="_method" value="DELETE">
								<button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt fa-lg"></i> Eliminar</button>
							</form>							
							<a href="{{url('authorization_groups/'.$group->id)}}" class="btn btn-success"> <i class="fas fa-check-double"></i> Autorizaciones</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

@endsection

@section('scripts')
	<script>
		var groupsTable = $('#groupsTable'){
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			},
		}

	</script>
@endsection