@extends('layouts.app')

@section('content')

<div class="flash-message" id="mensaje">
    @foreach ($errors->all() as $message)
    <p class="alert alert-warning">{{ $message }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endforeach
</div>

<div class="card">    
    <div class="card-header">
        <h3>Importar Grupos</h3>
    </div>
    <div class="card-body">
        <form action="{{ route('group.import.process') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="">Archivo .csv</label>
                <input type="file" name="file" class="form-control" accept=".csv" placeholder="Eliga un Archivo" aria-describedby="helpId">
                <small id="helpId" class="text-muted">Archivo .csv con los grupos y sus usuarios</small>
            </div>
            <input type="submit" value="Importar" class="btn btn-primary">
        </form>
    </div>
</div>

@endsection