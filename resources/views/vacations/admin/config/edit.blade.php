@extends('layouts.app')

@section('content')

<div class="panel panel-default panel-custom">
    <div class="panel-heading panel-heading-custom">
        <span class="panel-custom-first-{{config('config.theme')}} flexiform-icon"><h3 class="panel-title"> Editar configuración</h3></span><span class="panel-custom-second-{{config('config.theme')}}"></span>

    </div>
    <div class="panel-body panel-body-custom">
        {!! Form::open(['url' => "admin/config", 'method' => 'POST']) !!}
        <div class="row"><div class="container"><h3>Personalización</h3></div></div>
        <div class="row">
            <div class="col-md-3">
                {!! Form::label('startdate', 'Número de días para solicitudes') !!}<br />
                {!! Form::text('startdate',null, ['class' => 'form-control','required'=>'required']) !!}
            </div>
            <div class="col-md-3">
                {!! Form::label('firstday', 'Inicio semana (calendario)') !!}<br />
                {!! Form::select('firstday', [1=>'Lunes',2=>'Domingo'], null,['class' => 'form-control','required'=>'required']) !!}
            </div>
        </div>
        <div class="row"><br /></div>
        
        <div class="row"><br /></div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                {!!Form::submit('Guardar',['class'=>'btn btn-block btn-custom-'.config('config.theme')])!!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    // var i = 
    function add() {
        $('#container').append(
            '<div class="row" id="r'+i+'">'+
            '<div class="col-md-3 form-group">'+
                '<input placeholder="Id" class="form-control" required="required" name="companies['+i+'][id]" type="text">'+
            '</div>'+
            '<div class="col-md-3">'+
                '<input placeholder="Nombre" class="form-control" required="required" name="companies['+i+'][name]" type="text">'+
            '</div>'+
            '<div class="col-md-1">'+
                '<button id="minus" onclick="remove('+i+')" class="btn btn-block btn-custom-{{config('config.theme')}}" type="button">-</button>'+
            '</div'+
            '</div>'
        );
        i++;
    }
    function remove(id) {
        $('#r'+id).remove();
    }
</script>
@endsection
