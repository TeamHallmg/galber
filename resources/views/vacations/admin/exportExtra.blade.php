@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <h3> Exportar Nominpaq</h3>
    </div>

    <div class="card-body">
        <form action="{{ url('admin/export/extras') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <rangedate></rangedate>
                </div>
            </div>
            <div class="row my-3">
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="checkbox" name="mark" id="">
                        <label for="mark" value='1'>¿Desea marcar como procesado?</label>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <input type="submit" value="Procesar" class="btn btn-block btn-primary">
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
