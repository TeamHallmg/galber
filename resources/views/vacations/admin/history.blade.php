@extends('vacations.app')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> VER SOLICITUDES</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Solicitudes</h3>
    </div>
</div>

<div class="row mb-2">
    <div class="col-md-5">
        <div class="form-group">
            <label>Departamento</label>
            <select id="departmentSelect" class="selectpicker form-control" data-live-search="true">
                <option value="">Todos los departamentos</option>
                @foreach ($departments as $department)
                    <option value="{{ $department->id }}" data-subtext="{{ $department->getDirectionName() }}"> {{ $department->name }} </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group" id="divJobs">
            <label>Puesto</label>
            <select id="jobSelect" class="selectpicker form-control" data-live-search="true" disabled>
                <option value="">Seleccione el departamento</option>
            </select>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label>Mostrar Inactivos</label>
            <input type="checkbox" id="cboxActive">
        </div>
    </div>
</div>
<div class="row mb-2">
    <div class="col-md-5">
        <div class="form-group" id="divJobs">
            <label>Incidencias</label>
            <select id="incidentSelect" class="selectpicker form-control" data-live-search="true">
                <option value="">Seleccione la incidencia</option>
                @foreach($benefits as $benefit)
                    <option value="{{ $benefit->id }}" data-subtext="{{ $benefit->shortname }}">
                        {{ $benefit->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="table-responsive">
    <table id="tableRequest" class="table table-bordered">
        <thead class="blue_header">
            <tr>
                <th>Jefe</th>
                <th>Empleado</th>
                <th></th>
                <th>Departamento</th>
                <th></th>
                <th>Puesto</th>
                <th></th>
                <th>Tipo</th>
                <th>Estatus</th>
                <th>Nota</th>
                <th>Inicio Evento</th>
                <th>Fin Evento</th>
                <td></td>
            </tr>
        </thead>
        <tbody>
            @foreach ($requests as $request)
                <tr>
                    <td>{{ $request->user_wt->FullName }}</td>
                    <td>{{ $request->from_wt->FullName }}</td>
                    <td>{{ $request->from_wt->employee_wt->getDepartmentId(true) }}</td>
                    <td>{{ $request->from_wt->employee_wt->getDepartmentName(true) }}</td>
                    <td>{{ $request->from_wt->employee_wt->getPuestoId(true) }}</td>
                    <td>{{ $request->from_wt->employee_wt->getPuestoName(true) }} </td>
                    <td>{{ $request->benefit->id }}</td>
                    <td>{{ $request->benefit->name }}</td>
                    <td>{{ __('incidents.' . $request->status) }}</td>
                    <td>{{ $request->info }}</td>
                    <td data-sort="{{ $request->event->getStartDate('YmdHis') }}">{{ $request->event->getStartDate('d/m/Y') }}</td>
                    <td data-sort="{{ $request->event->getEndDate('YmdHis') }}">{{ $request->event->getEndDate('d/m/Y') }}</td>
                    <td>{{ $request->user_wt->trashed()?'sd_inactive':'sd_active' }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    
    const jobUrl = "{{ url('jobsFromDepartment') }}";
    function cleanDateInputs() {
		document.getElementById('date_ubegin').value = '';
		document.getElementById('date_uend').value = '';
		tableRequest.draw();
	}

    var tableRequest = $('#tableRequest').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        order: [],
        colReorder: true,
        dom: 'B' +
            '<"row mt-1"<"col-md-3"l><"col-md-6"<"#toolbar">><"col-md-3"f>>rtip',
            "columnDefs": [
            {
                "targets": [ 2, 4, 6, 12 ],
                "visible": false,
            }
        ],
        buttons: [
            {
                extend: 'csvHtml5',
                title: 'Listado_incidencias',
                exportOptions: {
                    columns: [0, 1, 3, 5, 7, 8, 9, 10, 11]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [0, 1, 3, 5, 7, 8, 9, 10, 11]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [0, 1, 3, 5, 7, 8, 9, 10, 11]
                }
            },
        ]
    });

    $(function(){
        $("div#toolbar").html('<div class="input-daterange input-group" id="datepicker">' +
            '<input type="date" class="form-control date_users datepicker-class" id="date_ubegin" name="start" required="required" />' +
            '<span class="input-group-text">al</span>' +
            '<input type="date" class="form-control date_users datepicker-class" id="date_uend" name="end" required="required" />' +
            '<button class="btn btn-primary" onclick="cleanDateInputs()">Limpiar</button>'+
            '</div>'
        );

        $.fn.dataTable.ext.search.push(
		    function( settings, data, dataIndex ) {
                const department = $('#departmentSelect').val().trim();
                const job = $('#jobSelect').val().trim();
                const isActive = $('#cboxActive').prop("checked");
                const incidentID = $('#incidentSelect').val();

		        const min = new Date( $('#date_ubegin').val() );
                const max = new Date( $('#date_uend').val() );

                console.log(department);

                const incidentIdPos = 6;
                const startDatePos = 10;
                const endDatePos = 11;
                const activeFlagPos = 12;

                let start = data[startDatePos].split('/');
                let end = data[endDatePos].split('/');
                start = start[2] + '-' + start[1] + '-' + start[0];
                end = end[2] + '-' + end[1] + '-' + end[0];
		        start = new Date( start ) || 0; // Fecha de Inicio
                end = new Date( end ) || 0; // Fecha de Fin
                let dateval = false;
		 		if( isNaN(min) && isNaN(max) ||
                    min <= start && start <= max ||
                    min <= end && end <= max ||
                    (start <=  min && max <= end && start <= max && min <= max)){
                    dateval = true;
                }

                let incidentval = (!incidentID || incidentID === data[incidentIdPos])

                let val = false;
                if(isActive && data[activeFlagPos] === "sd_inactive" || data[activeFlagPos] === 'sd_active'){
                    if(!department){
                        val = true;  
                    }else{
                        if(!job){
                            if (typeof data[2] === "string" && typeof department === "string"){
                                if(data[2] === department){
                                    val = true;
                                }
                            }
                        }else{
                            if (typeof data[2] === "string" && typeof department === "string" && (typeof data[4] === "string" && typeof job === "string")){
                                if(data[2] === department && data[4] === job){
                                    val = true;
                                }
                            }
                        }
                    }
                }
                return val && dateval && incidentval;
		    }
		);

        $('.date_users').on('change', function () {
			const dateBegin = new Date($('#date_ubegin').val());
			const dateEnd = new Date($('#date_uend').val());
			if(!isNaN(dateBegin) && !isNaN(dateEnd)){
				if(dateEnd < dateBegin){
					$('#date_uend').val($('#date_ubegin').val());
				}
			}else if(!isNaN(dateBegin) && isNaN(dateEnd)){
				$('#date_uend').val($('#date_ubegin').val());

			}else if(isNaN(dateBegin) && !isNaN(dateEnd)){
				$('#date_ubegin').val($('#date_uend').val());
			}
			tableRequest.draw();
		});
    });

    $('#departmentSelect').on('change', function(){
            const departmentID = $(this).val();
            if(departmentID){
                axios.get(jobUrl, {
                    params:{ 
                        department: departmentID,
                    }
                })
                .then(response => {
                    const jobs = response.data;
                    var sel = $('<select>');
                        sel.append($("<option>").attr('value','').text('Todos los puestos del departamento'));
                    $(jobs).each(function() {
                        sel.append($("<option>").attr('value',this.id).text(this.name));
                    });
                    $('#jobSelect').selectpicker('destroy').remove();
                    sel.attr('id', 'jobSelect').addClass('selectpicker form-control').attr('data-live-search', "true");
                    $('#divJobs').append(sel);
                    sel.selectpicker();
                })
                .catch(error => {

                });
            }else{
                $('#jobSelect').selectpicker('destroy').remove();
                var sel = $('<select>');
                sel.append($('<option>').attr('value', '').text('Seleccione el departamento'));
                sel.attr('id', 'jobSelect').addClass('selectpicker form-control').attr('data-live-search', "true").attr('disabled', true);
                $('#divJobs').append(sel);
                    sel.selectpicker();
            }
        });

        $('#departmentSelect').on('change', refreshTable);
        $('#divJobs').on('change', '#jobSelect', refreshTable);
        $('#cboxActive').on('change', refreshTable);
        $('#incidentSelect').on('change', refreshTable);

        function refreshTable(){
            tableRequest.draw();
        }
    
    function destroy(button,id){

        var r = confirm('¿Esta seguro de eliminar la solicitud?');
        if(r){
            $.ajax({
                headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
                url: "/common/plea/"+id,
                async: false,
                method: "DELETE",
                dataType: "json"
            }).done(function( data ) {
                if(data.success == 1) {
                    $('#alert').html(
                        '<div class="alert alert-warning alert-dismissible fade in" role="alert">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                            '<span aria-hidden="true">&times;</span>'+
                        '</button>'+
                        '<strong>Registro cancelado.</strong>'+
                        '</div>'
                    );
                } else {
                    $('#alert').html(
                        '<div class="alert alert-warning alert-dismissible fade in" role="alert">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                            '<span aria-hidden="true">&times;</span>'+
                        '</button>'+
                        '<strong>'+data.error+'</strong>'+
                        '</div>'
                    );
                }
            });
        }
    }
</script>

@endsection
