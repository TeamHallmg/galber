<style>
    .logout{
        background-color: #F14810;
        border: none;
        color: white;
        border-radius: 0 0 17px 17px;
        margin: 0px 2% 0px 0px;
        padding: 5px 15px 10px 15px;
    }
    .divnav{
        padding: 0 5% 0 0;
        background-color: #F14810;
        font-weight: bold;
        font-size: 120%;
    }
    ul{
        text-align: center;
    }
    a{
        color: black;
    }
</style>

@auth
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="{{ url('/') }}">
        <img class="logo-flex pull-left " src="{{asset('img/logo_flexfilm_rojo.png')}}" style="height: 3.5rem">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            @if(Auth::user()->role != 'employee') 
                @include('layouts.menus.' . Auth::user()->role) 
            @endif
            @include('layouts.menus.employee')
        </ul>
        <ul class="navbar-nav ml-auto mr-3">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navbarDropdownAdmin" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    {{ Auth::user()->FullName }}
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownAdmin">
                    <a class="dropdown-item" href="{{ url('/logout') }}"> Cerrar sesion</a>
                </div>
            </li>
        </ul>
        {{-- <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> --}}
        </form>
    </div>
</nav>
{{-- <nav class="navbar text-right w-100 p-3">
    <div class="collapse navbar-collapse divnav">
        <a href="{{url('/')}}">
            <img class="logo-flex pull-left" src="{{asset('img/logo_flexfilm_rojo.png')}}" style="height: 40px; margin: 5px 10px 5px 20px">
        </a>
        <ul class="nav navbar-nav">

            <ul class="nav navbar-nav">
                @if(Auth::user()->role != 'employee') @include('layouts.menus.' . Auth::user()->role) 
                    @include('layouts.menus.employee')
                @endif
            </ul>
            <ul class="nav navbar-nav navbar-right pull-right">
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> {{ Auth::user()->firstname }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            @if(App::isLocale('en'))
                            <a href="{{ url('/set/lang/es') }}">
                                <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>  Es
                            </a>
                            @else
                            <a href="{{ url('/set/lang/en') }}">
                                <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>  En
                            </a>
                            @endif
                        </li>
                        <li>
                            <a href="{{ url('/logout') }}" >
                                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>  @lang('menus.logout')
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </ul>
    </div>
</nav> --}}
@endauth