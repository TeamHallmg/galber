{{--   @if(Auth()->User()->isSubstituting())
    <li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> @lang('menus.administration')<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="{{App::make('url')->to('/')}}/super/approve/requests"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> @lang('menus.appruverequest')</a></li>
        </ul>
    </li>
  @endif --}}
  {{-- Menu de balances --}}

  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navbarDropdownAdmin" role="button" aria-haspopup="true" aria-expanded="false">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
        @lang('menus.timerequest')
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownAdmin">
      @if(Auth::user()->authorizatorIn->isNotEmpty())
        <a class="dropdown-item" href="{{ url('common/time/create') }}">
            Crear
        </a>
      @endif
      @if(Auth::user()->isAuthorizatorOfLevel(2))
        <a class="dropdown-item" href="{{ route('common.time.approve') }}">
          Autorizar
        </a>
      @endif
      <a class="dropdown-item" href="{{ url('common/time') }}">
          Historial
      </a>
    </div>
  </li>

  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navbarDropdownAdmin" role="button" aria-haspopup="true" aria-expanded="false">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
        Cambios de Turno
    </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdownAdmin">
        @if(Auth::user()->authorizatorIn->isNotEmpty())
        {{--  @if(Auth::user()->role != 'employee')  --}}
          <a class="dropdown-item" href="{{ url('swapshift/create') }}">
            Crear
          </a> 
        @endif  
        <a class="dropdown-item" href="{{ url('swapshift') }}">
          Historial
        </a>
    </div>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{App::make('url')->to('/')}}/common/balances">
      <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
      @lang('menus.balance')
    </a>
  </li>
    {{-- Menu sobre las solicitudes --}}
  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navbarDropdownAdmin" role="button" aria-haspopup="true" aria-expanded="false">
      <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      Vacaciones
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownAdmin">
      <a class="dropdown-item" href="{{ url('common/plea/create') }}">
        @lang('menus.createnew')
      </a>
      <a class="dropdown-item" href="{{ url('common/plea') }}">
        @lang('menus.cancel')
      </a>
      <a class="dropdown-item" href="{{ url('common/inactive') }}">
        @lang('menus.requesthistory')
      </a>
    </div>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="{{ url('clocklog') }}">
      <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
      Reloj Checador
    </a>
  </li>
  
    {{-- Cambios de Turno --}}
    {{--  <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navbarDropdownAdmin" role="button" aria-haspopup="true" aria-expanded="false">
          <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
          Cambios de Turno
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownAdmin">
          <a class="dropdown-item" href="{{ url('swapshift/create') }}">
            Crear
          </a>        
          <a class="dropdown-item" href="{{ url('swapshift') }}">
            Historial
          </a>
        </div>
    </li>  --}}
    {{-- li><a href="{{App::make('url')->to('/')}}/common/incidents">Incidencias</a></li --}}
    {{-- <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-time" aria-hidden="true"></span> @lang('menus.clock')<span class="caret"></span></a>
      <ul class="dropdown-menu">
        <li><a href="{{App::make('url')->to('/')}}/common/clock"> @lang('menus.arrive_leave')</a></li>
        <li role="separator" class="divider"></li>
        <li><a href="{{App::make('url')->to('/')}}/common/clock/absens"> @lang('menus.absence')</a></li>
        <li><a href="{{App::make('url')->to('/')}}/common/clock/delays"> @lang('menus.delay')</a></li>
        <li><a href="{{App::make('url')->to('/')}}/common/clock/overtimes"> @lang('menus.overtime')</a></li>
      </ul>
    </li> --}}
    {{-- <li><a href="{{App::make('url')->to('/')}}/common/calendar"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> @lang('menus.calendar')</a></li> --}}
    
  