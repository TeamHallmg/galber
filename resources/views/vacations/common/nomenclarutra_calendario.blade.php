<h4>Eventos</h4>
<div class="row">
    <div class="col-md-6">
        <p class="vac-bg-text vac-bg-event">Eventos por ley o por parte de la empresa</p>
    </div>
</div>
<h4>Solicitudes</h4>
<div class="row">
    <div class="col-md-6">
        <p class="vac-bg-text vac-bg-status-pending">Solicitudes pendientes de autorización</p>
    </div>
    <div class="col-md-6">
        <p class="vac-bg-text vac-bg-status-accepted">Solicitudes autorizadas por el jefe</p>
    </div>
    <div class="col-md-6">
        <p class="vac-bg-text vac-bg-status-rejected">Solicitudes rechazadas por el jefe</p>
    </div>
    <div class="col-md-6">
        <p class="vac-bg-text vac-bg-status-processed">Solicitudes exportadas para la nónima</p>
    </div>
    <div class="col-md-6">
        <p class="vac-bg-text vac-bg-status-canceled">Solicitudes canceladas por cuenta propia</p>
    </div>
</div>
<hr>