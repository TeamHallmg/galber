@extends('vacations.app')

@section('content')
<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> CONTROL DE INCIDENCIAS</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Mi Historial</h3>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-hover table-bordered" id="incidentsTable">
    <thead class="blue_header">
        <tr>
            <th>Solicitud</th>
            <th>Tipo</th>
            <th>Inicio</th>
            <th>Fin</th>
            <th>Total</th>
            <th>Estado</th>
        </tr>
    </thead>
    <tbody>
        @foreach($events as $event)
            <tr>
                <td>{{$event->title}}</td>
                <td>{{ $event->benefit->name }}</td>
                <td>{{ $event->getStartDate('d/m/Y') }}</td>
                <td>{{ $event->getEndDate('d/m/Y') }}</td>
                <td>{{ $event->eventDays->count() }}</td>
                <td>{{$event->status}}</td>
            </tr>
        @endforeach
    </tbody>
    </table>
</div>

@endsection

@section('scripts')
<script>
    var incidentsTable = $('#incidentsTable').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        order: [],
    });
</script>
@endsection