@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h3> Crear solicitud</h3>
    </div>

    <div class="card-body">
        <form action="{{ url('common/time') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-4">
                    @if(empty($users))
                        <div class="alert alert-warning">
                    @endif
                        <label for="user">Empleado</label>
                        <select name="user" class="form-control" placeholder="Eliga a un usuario..." id="userSelect">
                            <option value="">..Eliga a un usuario...</option>
                            @foreach ($users as $key => $user)
                                <option value="{{ $key }}">{{ $user }}</option>
                            @endforeach
                        </select>
                    @if(empty($users))
                            <strong>Advertencia!</strong> No eres supervisor actualmente de algun empleado.
                        </div>
                    @endif
                </div>
            </div>
            <div class="row my-2">
                <div class="col-md-4">
                    <label for="">Nombre</label>
                    <input type="text" name="name" id="nameid" class="form-control" disabled>                        
                </div>
                <div class="col-md-4">
                    <label for="">Número</label>
                    <input type="number" name="number" id="numid" class="form-control" disabled>                        
                </div>
                <div class="col-md-4">
                    <label for="">Tipo</label>
                    <input type="text" name="type" class="form-control" id="typeid" disabled>                        
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Categoría</label>
                    <input type="text" name="category" id="cateid" class="form-control" disabled>
                </div>
                <div class="col-md-4">
                    <label for="">Departamento</label>
                    <input type="text" name="department" class="form-control" id="deptid" disabled>                        
                </div>
                <div class="col-md-4"></div>
            </div>
            <hr>
            <div class="row my-2">
                <div class="col-md-4">
                    <label for="date">Fecha</label>
                    <input type="date" name="date" id="datePick" class="form-control" required>
                </div>
            </div>
            <div class="row my-2">
                <div class="col-md-2">
                    <label for="">Hora de Entrada</label>
                    <input type="text" class="form-control" id="myTurnIn" disabled>
                </div>
                <div class="col-md-2">
                    <label for="">Hora de Salida</label>
                    <input type="text" class="form-control" id="myTurnOut" disabled>
                </div>
                <div class="col-md-3">
                    <label for="">Laboral / No Laboral</label>
                    <input type="text" class="form-control" id="myLabor" disabled>
                </div>
                <div class="col-md-2">
                    <label for="">Turno actual</label>
                    <input type="text" class="form-control" id="myTurn" disabled>
                </div>
                <div class="col-md-3">
                    <label for="">Turno a laborar</label>
                    <select name="shift" class="form-control" id="turns" required></select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label for="time">Hora entrada</label>
                    <input type="time" name="timeIn" value="{{ old('timeIn','00:00') }}" class="form-control timeVal" id="inTime" required>
                </div>
                <div class="col-md-4">
                    <label for="time">Hora salida</label>
                    <input type="time" name="timeOut" value="{{ old('timeOut','00:00') }}" class="form-control timeVal" id="outTime" required>
                </div>
                <div class="col-md-4">
                    <label for="time">Horas a laborar</label>
                    <input type="time" name="timeResult" value="" class="form-control" id="timeResult" aria-describedby="helpId" readonly>
                    <small id="helpId" class="text-muted">No puede hacer más de 8 horas seguidas</small>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-6">
                    <label for="">Motivos</label>
                    <textarea name="description" class="form-control" required></textarea>                        
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <input type="submit" value="Crear" class="btn btn-block btn-primary">                    
                </div>
            </div>
        </form>
    </div>
</div>

@section('scripts')

    <script>
        $(function(){
            loadLaborTime();
        });
        $('#datePick').change(function () {
            var userid = $('#userSelect').find(":selected").val();
            var date = $(this).val();
            if(userid){
                $.ajax({
                headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
                type: 'POST',
                url: '{{url('userInfo')}}',
                async: true,
                data: {userid, date},
                success: function(data){
                    var val = JSON.parse(data);
                    if (val.success) {
                        $('#nameid').val(val.name);
                        $('#numid').val(val.number);
                        $('#typeid').val(val.type);
                        $('#cateid').val(val.area);
                        $('#deptid').val(val.department);
                        $('#myLabor').val((val.labor)?'Laboral':'No laboral');
                        $('#myTurn').val(val.myShift);
                        $('#myTurnIn').val(val.inTime);
                        $('#myTurnOut').val(val.outTime);
                        $('#turns').empty();
                        $.each(val.shifts ,function(key,value){
                            $('#turns').append('<option value=' + key + '>' + value + '</option>')
                        });
                    }else{
                        alert(val.error);
                    }
                }
            });
            }
        });
        $('#userSelect').change(function () {
            var userid = $(this).find(":selected").val();
            var date = $('#datePick').val();
            $.ajax({
                headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
                type: 'POST',
                url: '{{url('userInfo')}}',
                async: true,
                data: {userid, date},
                success: function(data){
                    var val = JSON.parse(data);
                    if (val.success) {
                        $('#nameid').val(val.name);
                        $('#numid').val(val.number);
                        $('#typeid').val(val.type);
                        $('#cateid').val(val.area);
                        $('#deptid').val(val.department);
                        $('#myTurn').val(val.myShift);
                        $('#turns').empty();
                        $.each(val.shifts ,function(key,value){
                            $('#turns').append('<option value=' + key + '>' + value + '</option>')
                        });
                    }else{
                        alert(val.error);
                    }
                }
            });
        });

        $('.timeVal').on('change input', function(){
            loadLaborTime();
        });

        function loadLaborTime(){
            var inTime = $('#inTime').val();
            var outTime = $('#outTime').val();

            var inHour = parseInt(inTime.substring(0, inTime.indexOf(':')));
            var inMinutes = parseInt(inTime.substring(inTime.indexOf(':') + 1, inTime.length));
            var outHour = parseInt(outTime.substring(0, outTime.indexOf(':')));
            var outMinutes = parseInt(outTime.substring(outTime.indexOf(':') + 1, outTime.length));
            var inTimeNumber = parseFloat(inTime.replace(':','.'));
            var outTimeNumber = parseFloat(outTime.replace(':','.'));
            
            var resultHour = 0;
            var resulMinuts = 0;
            if(inTimeNumber <= outTimeNumber){
                resultHour = outHour - inHour;
                resulMinuts = outMinutes - inMinutes;
            }else{
                resultHour = Math.abs(inHour - outHour - 24);
                resulMinuts = outMinutes - inMinutes;
            }
            if(resulMinuts < 0){
                resultHour -= 1;
                resulMinuts = 60 + resulMinuts;
            }
            if(resultHour > 8 || (resultHour == 8 && resulMinuts > 0)){
                $('#timeResult').val('');
            }else{
                var time = (resultHour < 10)?'0':'';
                time += resultHour.toString();
                time += ':';
                time += (resulMinuts < 10)?'0':'';
                time += resulMinuts.toString();
                $('#timeResult').val(time);
            }
        }
    </script>

@endsection

@endsection
