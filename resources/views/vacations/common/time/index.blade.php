@extends('layouts.app')

@section('content')

@if($incidents->isNotEmpty())
<div class="card">
    <div class="card-header">
        <h3 class="panel-title">Solicitudes de Tiempo Extra</h3>
    </div>

    <div class="card-body">        
        <table class="table" id="incidents">
            <thead>
                <tr>
                    <th># Folio</th>
                    <th>Empleado</th>
                    <th>Dia</th>
                    <th>Inicio</th>
                    <th>Tiempo Extra</th>
                    <th>Turno</th>
                    <th>Motivo</th>
                    <th>Estatus</th>
                    <th>Reporte</th>
                    {{--  <th>Acciones</th>  --}}
                </tr>
            </thead>
            <tbody>
            @foreach($incidents as $incident)
                <tr>
                    <td> {{ $incident->id }} </td>
                    <td> {{ $incident->from->FullName }} </td>
                    <td> {{ $incident->incidentOvertime->date }} </td>
                    <td> {{ $incident->incidentOvertime->timeIn }} </td>
                    <td> {{ $incident->incidentOvertime->timeResult }} </td>
                    <td> {{ $incident->incidentOvertime->schedule->shift }} </td>
                    <td> {{ $incident->comment }} </td>
                    <td> {{ __('incidents.'.$incident->status) }} </td>
                    <td> 
                        <a href="{{url('common/times/print/'. $incident->id)}}" class="btn btn-warning">Imprimir</a>
                    </td>
                    {{--  <td>
                        @if($incident->status == 'pending')
                        <select name="b[{{$incident->id}}][v]" data-container="body" class="selectpicker" data-style="btn-info">
                            <option value="-1" style="background: #f0ad4e; color: #fff;">Posponer</option>
                            <option value="0" style="background: #d9534f; color: #fff;">Rechazar</option>
                            <option value="1" style="background: #5cb85c; color: #fff;">Aprobar</option>
                        </select>
                        @endif
                    </td>  --}}
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endif

<div class="card my-4">
    <div class="card-header">
        <h3 class="panel-title">Mis tiempos Extra</h3>
    </div>

    <div class="card-body">
        <table class="table" id="myIncidents">
            <thead>
                <tr>
                    <th># Folio</th>
                    <th>Empleado</th>
                    <th>Dia</th>
                    <th>Turno</th>
                    <th>Motivo</th>
                    <th>Estatus</th>
                    <th>Reporte</th>
                </tr>
            </thead>
            <tbody>
            @foreach($myIncidents as $incident)
                <tr>
                    <td> {{ $incident->id }} </td>
                    <td> {{ $incident->from->FullName }} </td>
                    <td> {{ $incident->incidentOvertime->date }} </td>
                    <td> {{ $incident->incidentOvertime->schedule->shift }} </td>
                    <td> {{ $incident->comment }} </td>
                    <td> {{ __('incidents.'.$incident->status) }} </td>
                    <td> 
                        <a href="{{url('common/times/print/'. $incident->id)}}" class="btn btn-warning">Imprimir</a>
                    </td>                    
                </tr>
            @endforeach
            </tbody>
        </table>        
    </div>
</div>

@endsection

@section('scripts')
<script>
    $('#incidents').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        colReorder: true,
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
        ]
    })

    $('#myIncidents').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        colReorder: true,
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
        ]
    });
</script>
@endsection