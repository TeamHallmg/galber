<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Calendario de eventos</h3>
    </div>
</div>

@include('vacations.common.nomenclarutra_calendario')

<div id="calendar" data-user_id="{{ isset($user_id)?$user_id:'' }}"></div>

@section('scripts')
<script>
    const url = "{{ route('common.getCalendarEvents') }}";
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        const user_id = calendarEl.dataset['user_id'];
        var calendar = new FullCalendar.Calendar(calendarEl, {
            titleFormat: { year: 'numeric', month: 'long' },
            locale: 'es',
            firstDay: 1,
            plugins: [ 'dayGrid', 'bootstrap' ],
            themeSystem: 'bootstrap',
            eventLimit: 5,
            events: {
                url: url,
                extraParams: {
                    user_id: user_id,
                },
                failure: function() {
                    alert('there was an error while fetching events!');
                },
            }
        });

        calendar.render();
    });
</script>
@endsection