@extends('vacations.app')

@section('content')
<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> CONTROL DE INCIDENCIAS</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Crear Solicitud</h3>
    </div>
</div>
<form action="{{ url('common/plea') }}" method="POST">
    @csrf 
    <div class="form-group">
        <label for="benefit_id">Concepto</label>
        <select name="benefit_id" class="form-control" id="select_benefit" required="required">
            <option value="">Seleccione...</option>
            @foreach ($balances as $balance)
                @if($balance->benefit->context == 'user')
                    @if($balance->benefit->type == 'day')
                    <option {{ old('benefit_id') == $balance->benefit_id?'selected':'' }} value="{{ $balance->benefit_id }}" data-week="{{ $balance->benefit->week_calendar }}">{{ $balance->benefit->name }}</option>
                    @elseif($balance->benefit->type == 'pending')
                        @if(($balance->pending - $balance->amount) <= 0)
                            <option {{ old('benefit_id') == $balance->benefit_id?'selected':'' }} value="{{ $balance->benefit_id }}">
                                {{ $balance->benefit->name }} (Días restantes: {{ $balance->diff }})
                            </option>
                        @else
                            <option {{ old('benefit_id') == $balance->benefit_id?'selected':'' }} value="{{ $balance->benefit_id }}">{{ $balance->benefit->name }} @if($balance->pending>0)(Días restantes: {{ $balance->pending - $balance->amount }})@endif</option>
                        @endif
                    @elseif($balance->benefit->type == 'time')
                        <option {{ old('benefit_id') == $balance->benefit_id?'selected':'' }} class="time" data-time="{{ $balance->getMaxTimePerRequest() }}" value="{{ $balance->benefit_id }}">{{ $balance->benefit->name }}</option>
                    @endif
                @endif
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="title">Descripción</label>
        <input type="text" name="title" value="{{ old('title') }}" class="form-control form-date">
    </div>
    <div class="row" id="dates">
        <div class="col-md-6">
            <label for="start">Fecha Inicio</label>
            <input type="date" id="start" name="start" class="form-control form-date" value="{{ old('start') }}" required>
        </div>
        <div class="col-md-6">
            <label for="end" id="label_title">Fecha Final </label>
            <input type="number" id="time" name="time" min="1" max="8" class="form-control" value="{{ old('time') }}" style="display: none">
            <input type="date" id="end" name="end" class="form-control" value="{{ old('end') }}" required>
        </div>
    </div>
    <div id="week" class="pt-3" style="display: none">
        <input type="hidden" name="date" id="off_date">
        <div id="weekCanCreate" style="display: none">
            <p class="alert alert-danger">No puedes crear un evento de semana comprimida para esta semana ya que tienes eventos activos</p>
        </div>
        <div id="weekCan" style="display: none">
            <p class="alert alert-info" id="weekMsg"></p>
        </div>
        <div id="weekCalendar" data-user_id="{{ isset($user_id)?$user_id:'' }}"></div>
    </div>
    <div class="my-4">
        <input type="submit" value="Crear" class="btn btn-primary">
    </div>
</form>
@include('vacations.common.calendar')

@endsection

@push('scripts')
<script>
    document.addEventListener('DOMContentLoaded', function() {
        let weekCalendar = document.getElementById('weekCalendar');
        let w_user_id = weekCalendar.dataset['user_id'];
        let wCalendar = new FullCalendar.Calendar(weekCalendar, {
            contentHeight: 120,
            columnHeaderFormat:{ 
                weekday: 'short', month: 'short', day: '2-digit', omitCommas: true 

            },
            titleFormat: { year: 'numeric', month: 'long' },
            locale: 'es',
            firstDay: 1,
            plugins: [ 'dayGrid', 'bootstrap', 'interaction' ],
            selectable: true,
            defaultView: 'dayGridWeek',
            duration: {weeks:1},
            themeSystem: 'bootstrap',
            eventLimit: 5,
            events: {
                url: url,
                extraParams: {
                    user_id: w_user_id,
                    onlyActive: true,
                },
                success: function(rawEvents, xhr) {
                    const select = $('#select_benefit');
                    const val = select.val();
                    const opt = $(select).find('option:selected');
                    const week = opt.data('week');
                    if(!!week){
                        $(':submit').prop('disabled', !!rawEvents.length);
                        $('#weekCan').fadeOut();
                        if(rawEvents.length){
                            $('#weekCanCreate').fadeIn();
                        }else{
                            $('#weekCanCreate').fadeOut();
                        }
                    }
                },
                failure: function() {
                    alert('there was an error while fetching events!');
                },
            },
            dateClick: function(dateClickInfo){
                let dayOfWeek = dateClickInfo.date.getDay();
                const eventCount = wCalendar.getEvents().length;
                const maxDays = 5;
                if(!eventCount && (dayOfWeek < 6 && dayOfWeek > 0)){
                    $('#weekCalendar tbody .extra-work-day').empty();
                    $('#weekCalendar .extra-work-day').removeClass('extra-work-day');
                    $('#weekCalendar tbody .rest-day').empty();
                    $('#weekCalendar .rest-day').removeClass('rest-day');
                    $('#weekCalendar [data-date="'+dateClickInfo.dateStr+'"]').addClass('rest-day');
                    $('#weekCalendar tbody [data-date="'+dateClickInfo.dateStr+'"]').append('<i class="fa fa-coffee" aria-hidden="true"></i>');
                    dayOfWeek--;
                    let today = getMonday(dateClickInfo.date);
                    $('#start').val(dateTo_Ymd_Format(today));
                    $('#off_date').val(dateTo_Ymd_Format(dateClickInfo.date));
                    let text = `En la semana del ${dateTo_dmy_Format(today)} al `;
                    for (let i = 0; i < maxDays; i++) {
                        if(i !== dayOfWeek){
                            let todayDate = today.toISOString().slice(0,10);
                            $('#weekCalendar [data-date="'+todayDate+'"]').addClass('extra-work-day');
                            $('#weekCalendar tbody [data-date="'+todayDate+'"]').append('<i class="fa fa-briefcase" aria-hidden="true"></i>');
                        }
                        let tomorrow = new Date(today)
                        today.setDate(tomorrow.getDate() + 1)
                    }
                    today.setDate(today.getDate() - 1);
                    $('#end').val(dateTo_Ymd_Format(today));
                    text += `${dateTo_dmy_Format(today)}, no laborara el día ${dateTo_dmy_Format(dateClickInfo.date)} acomodando las horas en el resto de días`;
                    $('#weekMsg').text('').text(text);
                    $('#weekCan').fadeIn();
                }
            }
        });

        wCalendar.render();
    });

    function dateTo_dmy_Format(date){
        if(typeof(date) === 'object'){
            let day = date.getDate().toString().padStart(2, '0');
            let month = (date.getMonth() + 1).toString().padStart(2, '0');
            return `${day}/${month}/${date.getFullYear()}`;
        }
    }

    function dateTo_Ymd_Format(date){
        if(typeof(date) === 'object'){
            let day = date.getDate().toString().padStart(2, '0');
            let month = (date.getMonth() + 1).toString().padStart(2, '0');
            return `${date.getFullYear()}-${month}-${day}`;
        }
    }

    function getMonday(d) {
        d = new Date(d);
        var day = d.getDay(),
            diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
        return new Date(d.setDate(diff));
    }

    $('#select_benefit').on('change', function(){
        const select = $('#select_benefit');
        const val = select.val();
        if(val){
            const opt = $(select).find('option:selected');
            const week = opt.data('week');
            if(week){
                show_week(select, opt);
            }else{
                show_time_fields(select, opt)
            }
        }
    });

    function show_time_fields(select, opt){
        const opt_class = opt.attr('class');
        const opt_time = opt.data('time');
        const stmt = opt_class === 'time';
        $('#time').prop('disabled', !stmt).prop('required', stmt);
        $('#week').hide();
        $('#dates').show();
        $(':submit').prop('disabled', false);
        if(stmt){
            $('#label_title').text('Horas');
            $('#time').attr('max', opt_time?opt_time:8).show();
            $('#end').val($('#start').val()).hide();
        }else{
            $('#label_title').text('Fecha Final');
            $('#time').hide();
            $('#end').show();
        }
    }

    function show_week(select, opt){
        $('#dates').hide();
        $('#week').show();
    }

    function show_fields(){
        const select = $('#select_benefit');
        const val = select.val();
        if(!!val){
            const opt = $(select).find('option:selected');
            const week = opt.data('week');
            if(week){
                show_week(select, opt);
            }else{
                show_time_fields(select, opt)
            }
        }
    }

    show_fields();

    $(function(){
        $('.form-date').on('change', function(){
            const opt = $('#select_benefit').find('option:selected');
            const opt_class = opt.attr('class');
            const stmt = opt_class === 'time';
            var dateBegin = new Date($('#start').val());
            if(!stmt){
                var dateEnd = new Date($('#end').val());
                if(!isNaN(dateBegin) && !isNaN(dateEnd)){
                    if(dateEnd < dateBegin){
                        $('#end').val($('#start').val());
                    }
                }else if(!isNaN(dateBegin) && isNaN(dateEnd)){
                    $('#end').val($('#start').val());

                }else if(isNaN(dateBegin) && !isNaN(dateEnd)){
                    $('#start').val($('#end').val());
                }
            }else{
                $('#end').val($('#start').val());
            }
        })
    });
</script>
@endpush