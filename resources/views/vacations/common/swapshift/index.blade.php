@extends('layouts.app')

@section('content')

@if(count($exchanges) > 0)
<div class="card">
	<div class="card-header">
		<h4>Cambios de mi Personal</h4>
	</div>
	<div class="card-body">
		<table class="table" id="exchangesTable">
			<thead>
				<tr>
					<th>#Folio</th>
					<th>Solicitante</th>
					<th>Turno a trabajar</th>
					<th>Fecha</th>
					<th>Solicitado</th>
					<th>Turno cambiado</th>
					<th>Fecha</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($exchanges as $exchange)
					<tr>
						<td> {{ $exchange->id }} </td>
						<td> {{ $exchange->applicant->FullName }} </td>
						<td> {{ $exchange->applicantWorkshift->schedule->shift }} </td>
						<td> {{ $exchange->requestedWorkshift->date }} </td>
						<td> {{ $exchange->requested->FullName }} </td>
						<td> {{ $exchange->requestedWorkshift->schedule->shift }} </td>
						<td> {{ $exchange->applicantWorkshift->date }} </td>
						<td>
							<form action="{{ url('swapshift/' . $exchange->id) }}" method="POST" style="display: inline">
								@csrf
								<input type="hidden" name="_method" value="DELETE">			
								<input type="submit" value="Borrar" class="btn btn-danger">			
							</form>
							<a href="{{ url('swapshift_download/'.$exchange->id) }}" class="btn btn-info">Descargar Permiso</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endif

<div class="card my-4">
		<div class="card-header">
			<h4>Mis intercambios</h4>
		</div>
		<div class="card-body">
			<table class="table" id="myExchangesTable">
				<thead>
					<tr>
						<th>#Folio</th>
						<th>Solicitante</th>
						<th>Turno a trabajar</th>
						<th>Fecha</th>
						<th>Solicitado</th>
						<th>Turno cambiado</th>
						<th>Fecha</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($myExchanges as $exchange)
						<tr>
							<td> {{ $exchange->id }} </td>
							<td> {{ $exchange->applicant->FullName }} </td>
							<td> {{ $exchange->applicantWorkshift->schedule->shift }} </td>
							<td> {{ $exchange->requestedWorkshift->date }} </td>
							<td> {{ $exchange->requested->FullName }} </td>
							<td> {{ $exchange->requestedWorkshift->schedule->shift }} </td>
							<td> {{ $exchange->applicantWorkshift->date }} </td>
							<td>
								<a href="{{ url('swapshift_download/'.$exchange->id) }}" class="btn btn-info">Descargar Permiso</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

@endsection

@section('scripts')
<script>
	var exchangesTable = $('#exchangesTable').DataTable({
		"language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
		colReorder: true,
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                }
            },
        ]
	});

	var myExchangesTable = $('#myExchangesTable').DataTable({
		"language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
		colReorder: true,
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                }
            },
        ]
	});
</script>
@endsection