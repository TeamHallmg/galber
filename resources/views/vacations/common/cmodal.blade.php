<!-- Modal -->
<div id="mymodal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nuevo</h4>
      </div>
      <div class="modal-body">
          {!! Form::open(['url' => 'common/plea', 'method' => 'POST']) !!}
          <input type="hidden" name="type" value="user" />
          <input type="hidden" name="status" value="pending" />
          <input type="hidden" name="user_id" value="{{Auth::user()->id}}" />
          <input type="hidden" name="region_id" value="{{Auth::user()->region_id}}" />
          <input type="hidden" name="blocked" value="0" />
          <input id="start" type="hidden" name="start" value="0" />
          <input id="end" type="hidden" name="end" value="0" />
          {!! Form::label('title', 'Descripción') !!}
          {!! Form::text('title', null, ['class' => 'form-control']) !!}
          <div class="row"><br /></div>
          {!! Form::label('title', 'Concepto') !!}
          {!! Form::label('title', 'Concepto') !!}
          <select name="benefit_id" required="required">
              <option selected="selected" value="">Seleccione...</option>
              @if($vacations['diff'] > 0)
                  <option value="{{$vacations['benefit']->id}}">{{$vacations['benefit']->name}} (Días restantes: {{$vacations['diff']}})</option>
              @else
                  <option value="{{$vacations['benefit']->id}}">{{$vacations['benefit']->name}}</option>
              @endif
              @foreach ($balances as $balance)
                  @if($balance->benefit->context == 'user')
                      @if($balance->benefit->type=='day')
                          <option value="{{$balance->benefit_id}}">{{$balance->benefit->name}}</option>
                      @elseif($balance->benefit->type=='pending' && $balance->benefit->id != 5)
                          @if(($balance->pending - $balance->amount)<=0)<option disabled value="{{$balance->benefit_id}}">{{$balance->benefit->name}}</option>
                          @else <option value="{{$balance->benefit_id}}">{{$balance->benefit->name}} @if($balance->pending>0)(Días restantes: {{$balance->pending - $balance->amount}})@endif</option>
                          @endif
                      @endif
                  @endif
              @endforeach
          </select>
          <div class="row"><br /></div>
          <div class="col-md-4 col-md-offset-4">
              {!!Form::submit('Crear',['class'=>'btn btn-block btn-custom-'.config('config.theme')])!!}
          </div>
          <div class="row"><br /></div>
          {!! Form::close() !!}
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<div id="myerror" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Error</h4>
      </div>
      <div class="modal-body">
          ¡No puede solicitar una fecha pasada!
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
