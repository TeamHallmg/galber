@extends('vacations.app')

@section('content')
<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> CONTROL DE INCIDENCIAS</h2>
    </div>
</div>
<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Mis Solicitudes</h3>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-hover table-bordered" id="incidentsTable">
        <thead class="blue_header">
            <tr>
            <th>Solicitud</th>
            <th>Tipo</th>
            <th>Inicio</th>
            <th>Fin</th>
            <th>Total</th>
            <th>Estado</th>
            <th>Cancelar</th>
            </tr>
        </thead>
        <tbody id="tbody">
            @foreach($events as $event)
                <tr>
                    <td>{{ $event->title }}</td>
                    <td>{{ $event->benefit->name }}</td>
                    <td>{{ $event->getStartDate('d/m/Y') }}</td>
                    <td>{{ $event->getEndDate('d/m/Y') }}</td>
                    <td>{{ $event->eventDays->count() }}</td>
                    <td>{{ $event->status }}</td>
                    <td>
                        <button data-id="{{ $event->id }}" data-title="{{ $event->title }}" class="btn btn-outline-danger rmvIncident">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Aprobadas</h3>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-hover table-bordered" id="approvedIncidentsTable">
        <thead class="blue_header">
            <tr>
            <th>Solicitud</th>
            <th>Tipo</th>
            <th>Inicio</th>
            <th>Fin</th>
            <th>Total</th>
            <th>Estado</th>
            <th>Cancelar</th>
            </tr>
        </thead>
        <tbody id="tbody">
            @foreach($approved_events as $event)
                <tr>
                    <td>{{ $event->title }}</td>
                    <td>{{ $event->benefit->name }}</td>
                    <td>{{ $event->getStartDate('d/m/Y') }}</td>
                    <td>{{ $event->getEndDate('d/m/Y') }}</td>
                    <td>{{ $event->eventDays->count() }}</td>
                    <td>{{ $event->status }}</td>
                    <td>
                        <button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#deleteRequest" data-id="{{ $event->incident->id }}" data-min="{{ date('Y-m-d', $event->incident->time) }}" title="Eliminar"><i class="fas fa-trash"></i></button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="modal fade" id="deleteRequest" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form" action="{{ url('super/delete_approved_request') }}" method="POST" id="form_delete">
				<input name="request_id" type="hidden">
				{{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar Solicitud Aprobada</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
						<label class="requerido" for="delete_date">Fecha de Solicitud Cancelación</label>
                        <input type="date" class="form-control" name="delete_date" id="delete_date" value="{{ date('Y-m-d') }}" required>
                    </div>
                    <div class="form-group">
						<label id="cnt_comment" for="delete_comments" class="requerido">Comentarios (0/200)</label>
						<textarea name="delete_comments" class="form-control noresize" maxlength="200" id="delete_comments" onkeyup="countMotive(this.value)" rows="2" required></textarea>
					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    var incidentsTable = $('#incidentsTable').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        order: [],
    });

    var incidentsTable = $('#approvedIncidentsTable').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        order: [],
    });
    var destroyUrl = '{{ url("common/plea/") }}';
    $(document).ready(function () {
        $('#tbody').on('click', '.rmvIncident', function(){
            var r = confirm('¿Esta seguro de eliminar: ' + $(this).data('title') + '?');
            if(r){
                var id = $(this).data('id');
                var self = this;
                axios
                    .delete(destroyUrl + '/' + id)
                    .then( function(res) {
                        if(res.data.success){
                            incidentsTable
                                .row( $(self).parents('tr') )
                                .remove()
                                .draw();
                        }else{
                            if(!!res.data.message)
                                alert(res.message);
                        }
                    })
                    .catch( function(error){
                        alert('No se pudo borar la incidencia');
                    });
            }            
        });
    });

    function destroy(id){
        var title = $('#row'+id+' td:first-child').text();
        var r = confirm('¿Esta seguro de eliminar: '+title+'?');
        if(r){
            $.ajax({
                headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
                url: "/common/plea/"+id,
                async: false,
                method: "DELETE",
                dataType: "json"
            }).done(function( data ) {
                if(data.success == 1) {
                    $('#alert').html(
                        '<div class="alert alert-warning alert-dismissible fade in" role="alert">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                            '<span aria-hidden="true">&times;</span>'+
                        '</button>'+
                        '<strong>Registro'+data.error+' cancelado.</strong>'+
                        '</div>'
                    );
                    $("#row"+id).css('display','none');
                } else {
                    $('#alert').html(
                        '<div class="alert alert-warning alert-dismissible fade in" role="alert">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                            '<span aria-hidden="true">&times;</span>'+
                        '</button>'+
                        '<strong>Hubo un error al calcelar el registro.'+data.error+'</strong>'+
                        '</div>'
                    );
                }
            });
        }
    }

    function countMotive(str) {
        var lng = str.length;
        $("#cnt_comment").html('Comentarios (' + lng + '/200)');
    }

    $(document).ready(function() {
        $("#deleteRequest").on('show.bs.modal', function (e) {
            var triggerLink = $(e.relatedTarget);
            var id = triggerLink.data("id");
            var min_date = triggerLink.data("min");
            $('input[name=request_id]').val(id);
            $('input[name=delete_date]').attr('min', min_date);
        });

        $("#deleteRequest").on('hide.bs.modal', function (e) {
            $("#cnt_comment").html('Comentarios (0/200)');
            $('#del_reason').val('');
        });
    });
</script>
@endsection
