@extends('layouts.app')

@section('content')

<div class="card card-3">
	<div class="card-body">
		<form action="{{ url('workshift') }}" method="POST">
			@csrf
			<div class="row">
			@foreach($regions as $region)
				<div class="col">
					<div class="square" id="div-{{ $loop->index }}">
						Grupo {{ $region->name }}
						<input type="text" name="{{ $region->name }}" class="invisible" id="{{ $loop->index }}" required>
					</div>
				</div>
			@endforeach
			</div>
			<div class="row justify-content-center">
				<div class="col-md-4">
					<input type="submit" value="Crear Turnos" class="btn btn-block btn-primary">
				</div>
			</div>
		</form>
	</div>
</div>

<div class="row mt-5">
	@foreach($months as $monthName => $month)
		<div class="card card-3 w-100 mb-3">
			<div class="card-header">
				<h3>{{$listMonths[intval($monthName)-1]}}</h3>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table" id="drawCalendar">
						<thead>
							<tr class="drawTr">
								@foreach($month as $dayName => $day)
									<th>{{$dayName}}</td>
								@endforeach
							</tr>
						</thead>
						<tbody>
							@for($i = 0; $i < 4; $i++)
								<tr class="role{{ $i }} drawTr" data-days="{{count($month)}}">
									@foreach($month as $dayName => $day)
										<td class="drawCell" id = '{{intval($monthName).'-'.$i.'-'.intval($dayName)}}' data-month='{{$monthName}}' data-day='{{$day}}' data-row='{{$i}}' ></td>
									@endforeach
								</tr>
							@endfor
						</tbody>
					</table>
				</div>
			</div>
		</div>
	@endforeach	
</div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function() {			
			// var colors = ['green','blue','red','orange'];
			var colors = ['#76d33d','#23b4ce','#ff7e5e','#f3ff75'];

			$(".table").on("click", "td", function() {
	     		hola($(this).attr('id'));
	   		});

	   		function hola(id){
	   			var td = $('#' + id);	   			
	   			var tdID = $(td).attr('id');
	   			var tdInfo = tdID.split('-');
	   			var tr = td.closest('tr');
	   			$('#'+tdInfo[1]).val(tdID);
	   			var trDays = $(tr).data('days');
				paintYear(tdInfo,trDays);
				console.log('#div-' + td.data('row'), colors[td.data('row')]);

				$('#div-' + td.data('row')).css('background-color', colors[td.data('row')]);
	   		}

	   		function paintYear(tdInfo,trDays){
	   			var month = parseInt(tdInfo[0]);
	   			var lastPos = [0, 1];
	   			var tdInfoAux = tdInfo;
	   			var monthAux = month;
	   			var trDaysAux = trDays;
	   			// console.log({month, tdInfoAux, monthAux, trDaysAux});
	   			for(var i = month; i <= 12; i++){
	   				// console.log({tdInfoAux,trDaysAux,lastPos,numOrder});
	   				var lastPos = fowardPainting(tdInfoAux,trDaysAux,lastPos[0], lastPos[1]);
	   				monthAux = monthAux + 1;
	   				newtd = "#"+monthAux+"-"+tdInfo[1]+"-"+1;
	   				trDaysAux = $(newtd).closest('tr').data('days');
	   				tdInfoAux = [monthAux, tdInfo[1], 1];
	   			}
	   			
	   			var tdInfoAux = tdInfo;
	   			tdInfoAux[2] = tdInfoAux[2] - 1;
	   			var monthAux = month;
	   			var trDaysAux = trDays - 1;
	   			lastPos[0] = 1;
	   			lastPos[1] = ((lastPos[1] + 1) > 1)?1: lastPos[1] + 1;
	   			console.log({tdInfoAux, trDaysAux, lastPos});
	   			for(var i = month; i > 0; i--){ 
	   				lastPos = backwardPainting(tdInfoAux,trDaysAux,lastPos[0], lastPos[1]);
	   				monthAux = monthAux - 1;
	   				newtd = "#"+monthAux+"-"+tdInfo[1]+"-"+1;
	   				trDaysAux = $(newtd).closest('tr').data('days');
	   				tdInfoAux = [monthAux,tdInfo[1],trDaysAux];
	   				
	   			}
	   		}
	   		function fowardPainting(tdInfo,trDays,lastPos, num){
	   			var month = tdInfo[0];
	   			var row = tdInfo[1];
	   			var day = tdInfo[2];
	   			var pos = lastPos;
	   			for (var i = day; i <= trDays; i++) {
	   				var cell = $("#"+month+"-"+row+"-"+i);
	   				if(pos == 6 || pos == 7){
	   					cell.css("background-color", "#99a394");
	   					cell.html('D');
	   					if(pos == 7){
	   						pos = 0;
	   						if(num == 1){
	   							num = 3;
	   						}else{
	   							num--;
	   						}
	   					}else{
	   						pos++;
	   					}
	   				}
	   				else{
	   					cell.css("background-color", colors[row]);
	   					cell.html(num);
	   					pos++;
	   				}
	   			}
	   			return [pos, num];
	   		}

	   		function backwardPainting(tdInfo,trDays,lastPos,num){
	   			var month = tdInfo[0];
	   			var row = tdInfo[1];
	   			var day = tdInfo[2];
	   			var pos = lastPos;
	   			for (var i = day; i > 0; i--) {
	   				var cell = $("#"+month+"-"+row+"-"+i);
	   				if(pos == 0 || pos == 1){
	   					cell.css("background-color", "#99a394");
	   					cell.html('D');
	   					if(pos == 0){
	   						pos = 7;
	   						if(num == 3){
	   							num = 1;
	   						}else{
	   							num++;
	   						}
	   					}else{
	   						pos--;
	   					}
	   				}
	   				else{
	   					cell.css("background-color", colors[row]);
	   					cell.html(num);
	   					pos--;
	   				}
	   			}
	   			return [pos, num];
	   		}
		});


	</script>
@endsection