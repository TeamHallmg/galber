@extends('vacations.app')

@section('content')
<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Reloj Checador</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Horario de Trabajo</h3>
    </div>
</div>

@if($workshift)
    <div class="row">
        @foreach ($workshift as $day)
            <div class="col {{ $day->labor ? 'bg-light':'bg-secondary' }} py-3 px-2">
                <h4 class="font-weight-bold text-blue">{{ __('bd.l-'.$day->day) }}</h4>
                <div class="form-group">
                    <input type="text" class="form-control bg-info" value="{{ $day->labor ? 'Día laboral':'Día no laboral' }}" disabled>
                    {{ $day->labor? $day->schedule->in . ' - ' . $day->schedule->out:'' }}
                </div>
            </div>        
        @endforeach
    </div>
@else
    <h4 class="text-danger m-0">No tienes un horario asignado...</h4>
@endif

<div class="row mt-3 mb-4">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Mis checadas</h3>
    </div>
</div>

<div class="my-2">
    @if(!!count($clocklogs))
        <table class="table" id="clocklog">
            <thead>
                <tr>
                    <th>Hora de marcado</th>
                    <th>Hora de comparación</th>
                    <th>Tipo de checada</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clocklogs as $clocklog)
                    @if($clocklog->arrive_status === 'late')
                        <tr class="table-danger">
                    @elseif($clocklog->arrive_status === 'early')
                        <tr class="{{ $clocklog->type === 'in' ? 'table-success' : 'table-danger'}}">
                    @elseif($clocklog->arrive_status === 'ok')
                        <tr class="table-success">
                    @elseif($clocklog->arrive_status === 'tolerance')
                        <tr class="table-warning">
                    @else
                        <tr>
                    @endif
                        <td data-sort="{{ $clocklog->getDateOnFormat('YmdHis') }}">{{ $clocklog->getDateOnFormat('d/m/Y - H:i') }}</td>
                        <td>{{ $clocklog->compare_time }}</td>
                        <td>{{ $clocklog->type === 'in'? 'Llegada' : 'Salida' }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <p>No hay registros en el reloj checador...</p>
    @endif
</div>

@endsection

@section('scripts')
<script>
    var clocklog = $('#clocklog').DataTable({
        language: {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
        },
    });
</script>
@endsection