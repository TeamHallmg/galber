<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
    <p>Estimado colaborador</p>
    <p>El colaborador a su cargo "{{ $event->user->getFullNameAttribute() ?? 'N/A' }}" canceló una incidencia previamente autorizada.</p>
</body>
</html>