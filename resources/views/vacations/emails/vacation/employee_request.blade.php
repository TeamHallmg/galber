<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
    {{-- <p><img style="width:100%" src="{{App::make('url')->to(config('config.mailhead'))}}"></p> --}}
    <p>Se ha recibido una solicitud de {{ $event->user->FullName }}.</p>
    <h2>Solicitud</h2>
    <table style="border: 1px solid #244395; border-collapse: collapse; text-align: center;">
        <tr style="background-color: #244395; border: 1px solid black;">
            <th style="padding:15px">Incidencia</th>
            <th style="padding:15px">Fecha Inicio</th>
            <th style="padding:15px">Fecha Fin</th>
            <th style="padding:15px">Descripción</th>
        </tr>
        <tr>
            <td style="padding:15px">{{ $event->benefit->name }}</td>
            <td style="padding:15px">{{ $event->getStartDate('d/m/Y') }}</td>
            <td style="padding:15px">{{ $event->getEndDate('d/m/Y') }}</td>
            <td style="padding:15px">{{ $event->title }}</td>
        </tr>
    </table>
<p>Haga clic en el siguiente link para ingresar a la herramienta de autorización <a href="{{ route('super.approve', 'requests') }}">Solicitudes</a></p>
</body>
</html>
