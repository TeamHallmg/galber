<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
</head>
<body>
<style>
	.center{
		text-align: center;
	}

	.underline{
		text-decoration: underline;
	}

	.firma{
		margin: 0 25%;
		border-top: 3px solid black;
	}

	.firma2{
		border-bottom: 3px solid black;
	}

</style>

<div>
	<div class="center">
		<h1 class="">Solicitud de cambio</h1>
	</div>
	<div>
		<div>
			<h3>Altamira, Tamps.  <span class="underline">{{date('d')}}</span> de <span class="underline">{{date('m')}}</span> de <span class="underline">{{date('Y')}}</span> </h3>
		</div>
		<div>
			<p>Yo <span class="underline"> {{$exchange->applicant->FullName}} </span> </p>
			<p>Por medio de la presente me comprometo a cubrir el turno de <span class="underline"> {{ $exchange->requested->FullName }} </span>
			</p>
			<p>El dia <span class="underline"> {{ $exchange->requestedWorkshift->date }} </span> en el turno <span class="underline"> {{ $exchange->requestedWorkshift->schedule->shift }} </span>, queda por entendido que esta persona me pagara el turno ?? </p>
		</div>
		<div style="margin: 5% 0">
			<h2>Solicitante:</h2>
			<div class="center">
				<p class="firma">{{$exchange->applicant->FullName}} </p>
				<p>Nombre y Firma</p>
			</div>
		</div>
		<div style="margin: 5% 0">
			<h2>Acepta:</h2>
			<div class="center">
				<p class="firma">{{$exchange->requested->FullName}} </p> 
				<p>Nombre y Firma</p>
			</div>
		</div>
		<div class="center"style="margin: 5% 0; padding-right: 25%">
			<span>Firma del supervisor</span>
			<span class="firma2"> _________________________________ </span>
			<div>
				<p>Tachar lo que se solicita</p>
				<table align="center">
					<thead>
						<tr>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td align="left">CAMBIO DE TURNO</td>
							<td>_______</td>
						</tr>
						<tr>
							<td align="left">TURNO POR TURNO</td>
							<td>_______</td>
						</tr>
						<tr>
							<td align="left">CASTIGO </td>
							<td>_______</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
	window.print();
</script>
</body>
</html>
