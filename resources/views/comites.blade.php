@extends('layouts.app')

@section('content')
<div class="card card-2 border-0">
    <div class="card-body">
        <div class="container-fluid"> 
            <div class="flash-message" id="mensaje">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-'.$msg))
                        <p class="alert alert-{{ $msg }}">{!! Session::get('alert-'.$msg) !!} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                @endforeach
            </div>
            <div class="row mb-4">
                <div class="col bg-blue rounded-xl pt-3 py-1 px-5">
                    <p class="text-white font-weight-bold font-italic announcement-title">Comités</p>
                </div>
            </div>
            <div class="row mb-5 justify-content-center">
                @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner']])
            </div>
            <div class="row">
                {{-- <div class="col-12 col-md-12 table-responsive">
                    @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['committees']])
                </div> --}}
                <div class="col-12 col-md-12">
                    @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['images']])
                </div>
            </div>
        </div>
        
    </div>
</div>
@endsection