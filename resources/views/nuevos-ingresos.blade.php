@extends('layouts.app')

@section('content')
<div class="card card-2 border-0">
    {{-- <div class="card-header bg-white text-white p-3">
    </div> --}}
    <div class="card-body">
		<div class="container-fluid">
			<div class="row mb-4">
                <div class="col bg-blue rounded-xl pt-3 py-1 px-5">
                    <p class="text-white font-weight-bold font-italic announcement-title">Nuevos Ingresos</p>
                </div>
			</div>

			<div class="row justify-content-center">
				@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner']])
			</div>

			<div class="row justify-content-center my-5">
				@foreach($users as $user)
					{{-- <div class="col-12 col-md-6 col-lg-4"> --}}
					{{-- <div class="col-12 col-md-12 col-lg-6 col-xl-4"> --}}
					<div class="col-auto mb-4 " style="width: 480px;">
						<div class="row px-3">
							<div class="col-4 p-0" style="background-image:url('{{asset($user->photo)}}');
								background-size:cover;
								background-position:center;">
								{{--<img class="img-fluid w-100 rounded" src="{{asset('/img/'.$user->photo)}}" alt="">--}}
							</div>
							<div class="col-8 pl-0 pr-0 bg-client-gray">
								{{-- Nombre Completo --}}
								<p class="text-uppercase bg-blue text-white font-weight-bold pl-2"> {{isset($user->nombre)?$user->nombre:''}} {{isset($user->paterno)?$user->paterno:''}} {{isset($user->materno)?$user->materno:''}}</p>
								{{-- Puesto --}}
								<span class="pl-2">{{isset($user->job_position_id)?$user->job_position_id:'-'}}</span><br>
								{{-- Sede --}}
								<span class="pl-2">{{$user->getSucursal() ?? 'N/A'}}</span><br>
								{{-- Correo Empresa --}}
								<span class="pl-2">{{ $user->correoempresa }}</span><br>
								@if($user->telefono)
									<span class="pl-2">Ofic. {{ $user->telefono }}</span><br>
								@endif
								@if($user->celular)
									<span class="pl-2">Cel. {{ $user->celular }}</span><br>
								@endif
								<span class="pl-2">Fecha: {{Carbon\Carbon::parse(isset($user->ingreso)?$user->ingreso:'')->formatLocalized('%d de %B del %Y')}}</span><br>
								@if(!$user->telefono)
									<br>
								@endif
								@if(!$user->celular)
									<br>
								@endif
								<br>
								{{-- <span class="pl-2">{{isset($user->employee->JobPosition->area->department)?$user->employee->JobPosition->area->department->name:'-'}}</span><br> --}}
								{{-- @if(isset($user->user->profile->hobbies))
                                    @if($user->user->profile->hobbies != null)
                                        <span class="pl-2 d-flex">{{ $user->user->profile->hobbies }}</span><br>
                                    @else
                                        <span class="pl-2">-</span><br>
                                    @endif
								@endif --}}
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
    </div>
</div>
@endsection
@section('mainScripts')
<script type="text/javascript">
</script>
@endsection