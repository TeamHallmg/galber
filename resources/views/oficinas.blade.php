@extends('layouts.app')

@section('content')
	<style>
		.img-mosaico{
			padding: 0px;
			margin: 0px;
			width: 100%;
		}	
		
		.image {
		display: block;
		width: 100%;
		height: auto;
		}

		@media (max-width: 1920px) {
			.overlay {
				position: absolute;
				bottom: 0;
				left: 0;
				padding: .5rem;
				right: 0;
				background-color: #244395;
				overflow: hidden;
				margin-right: 15px;
				margin-left: 15px;
				width: auto;
				height: 30%;
				opacity: 0.8;
				transition: .5s ease;
			}
			
			.contenedor:hover .overlay {
				height: 100%;
				opacity: 0.8;
			}
			
			.text {
				color: white;
				font-size: 14px;
				position: absolute;
				top: 160%;
				left: 90%;
				display: contents;
				right: 50%
				-webkit-transform: translate(-90%, -90%);
				-ms-transform: translate(-90%, -90%);
				transform: translate(-90%, -90%);
				transition: .5s ease;
			}
			.contenedor:hover .overlay .text{
				top: 50%;
			}
		}
		@media (max-width: 1366px) {
			.overlay {
				position: absolute;
				bottom: 0;
				left: 0;
				right: 0;
				background-color: #244395;
				overflow: hidden;
				margin-right: 15px;
				margin-left: 15px;
				width: auto;
				height: 40%;
				opacity: 0.8;
				transition: .5s ease;
			}
			
			.contenedor:hover .overlay {
				height: 100%;
				opacity: 0.8;
			}
			
			.text {
				color: white;
				font-size: 14px;
				position: absolute;
				top: 210%;
				left: 90%;
				right: 50%
				-webkit-transform: translate(-90%, -90%);
				-ms-transform: translate(-90%, -90%);
				transform: translate(-90%, -90%);
				transition: .5s ease;
			}
			.contenedor:hover .overlay .text{
				top: 90%;max
			}
		}
		@media (max-width: 480px) {
			.overlay {
				position: absolute;
				bottom: 0;
				left: 0;
				right: 0;
				background-color: #244395;
				overflow: hidden;
				margin-right: 15px;
				margin-left: 15px;
				width: auto;
				height: 40%;
				opacity: 0.8;
				transition: .5s ease;
			}
			
			.contenedor:hover .overlay {
				height: 100%;
				opacity: 0.8;
			}
			
			.text {
				color: white;
				font-size: 14px;
				position: absolute;
				top: 180%;
				left: 90%;
				right: 50%
				-webkit-transform: translate(-90%, -90%);
				-ms-transform: translate(-90%, -90%);
				transform: translate(-90%, -90%);
				transition: .5s ease;
			}
			.contenedor:hover .overlay .text{
				top: 80%;max
			}
		}
		@media (max-width: 0px) {
			
		}
	</style>

	<div class="card card-2 border-0">
		<div class="card-body">
			<div class="container-fluid">
				{{-- <div class="row mb-4">
					<div class="col bg-blue rounded-xl pt-3 py-1 px-5">
						<p class="text-white font-weight-bold font-italic announcement-title">Sucursales</p>
					</div>
				</div> --}}
				
				<div class="row justify-content-center">
					@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner']])
				</div>
				
				@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['mosaico_space'], 'columns' => $display_announcements['columns']])

				@if(Auth::user()->hasRolePermission('announcement_mosaic_space'))
					<div class="row d-flex justify-content-end">
						<a href="{{ url('announcements?view=' . $display_announcements['mosaico_space']['view'] . '&type=' . $display_announcements['mosaico_space']['type']->id )}}" class="btn btn-primary btn-sm mt-2 mr-2" style="z-index:1;">
							<i class="nav-icon fas fa-cog fa-lg"></i> Editar
						</a>
					</div>
				@endif
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script type="text/javascript">
	</script>
@endsection