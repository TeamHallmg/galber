@extends('layouts.app')

@section('content')
<div class="card card-2 border-0">
    <div class="card-body">
        <div class="container-fluid"> 
            <div class="flash-message" id="mensaje">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-'.$msg))
                        <p class="alert alert-{{ $msg }}">{!! Session::get('alert-'.$msg) !!} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                @endforeach
            </div>
            <div class="row mb-4">
                <div class="col bg-blue rounded-xl pt-3 py-1 px-5">
                    <p class="text-white font-weight-bold font-italic announcement-title">Calendario</p>
                </div>
            </div>
            <div class="row mb-5 justify-content-center">
                @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner']])
            </div>
            <div class="row">
                {{-- Esta bandera sólo sirve para saber si es el show del anuncio o la tabla de calendario --}}
                @if(isset($form_data))
                    @if(isset($form_data['image']))
                        <div class="col-md-4 col-sm-12 mt-1 mb-3">
                            <h5 class="text-white font-weight-bold m-0 rounded-pill bg-info px-3 py-1 mb-3">
                                Imagen del Evento
                            </h5>
                            <img src="/img/announcements/{{ $form_data['image'] }}" class="img-fluid rounded px-3">
                        </div>
                    @endif

                    <div class="col-md-4 col-sm-12 mt-1 mb-3">
                        <h5 class="text-white font-weight-bold m-0 rounded-pill bg-info px-3 py-1 mb-3">
                            Título del Evento
                        </h5>
                    {{-- </div>
                    <div class="col-12 mt-1 mb-3"> --}}
                        <h1 class="text-blue px-3">{{ $form_data['title'] ?? 'N/A' }}</h1>
                    </div>

                    <div class="col-md-4 col-sm-12 mt-1 mb-3">
                        <h5 class="text-white font-weight-bold m-0 rounded-pill bg-info px-3 py-1 mb-3">
                            Detalle del Evento
                        </h5>

                        <p class="px-3"><span class="text-blue font-weight-bold">Lugar:</span> {{ $form_data['place'] ?? 'N/A' }}</p>
                        <p class="px-3"><span class="text-blue font-weight-bold">Hora de Inicio:</span> {{ $announcement->getFormattedHour('time_start') }}</p>
                        <p class="px-3"><span class="text-blue font-weight-bold">Hora de Fin:</span> {{ $announcement->getFormattedHour('time_end') }}</p>
                    </div>

                    <div class="col-12 mt-1 mb-3">
                        <h5 class="text-white font-weight-bold m-0 rounded-pill bg-info px-3 py-1">
                            Descripción del Evento
                        </h5>
                    </div>
                    <div class="col-12 mt-1 mb-3 px-4">
                        {!! $form_data['description'] ?? 'N/A' !!}
                    </div>


                    <div class="col-md-12 text-right">
                        <a href="{{ url('calendario')}}" class="btn btn-blue btn-sm mt-2 mr-2" style="z-index:1;">
                            <i class="fas fa-arrow-circle-left"></i> Regresar
                        </a>
                    </div>
                @else
                    <div class="col-12 col-md-3">
                        <div class="card">
                            <div class="card-header bg-blue text-white">
                                <h3 class="font-weight-bold m-0">Filtros</h3>
                            </div>
                            <div class="card-body">
                                <div id="datepicker" class="datepicker col-12 d-flex justify-content-center"></div>
                            </div>
                            <div class="card-footer text-muted">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-9 table-responsive">
                        {{-- <div class="row mb-3">
                            <div class="col-12 d-flex justify-content-end">
                                <a href="{{ url('gcalendar') }}" class="btn btn-danger btn-md float-right"><i class="fab fa-google"></i> Sincronizar con Google</a>
                            </div>
                        </div> --}}
                        @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['calendario']])
                    </div>
                @endif
            </div>
        </div>
        
    </div>
</div>
@endsection