@extends('layouts.app')

@section('content')
<div class="card card-2 border-0">
    <div class="card-body">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col bg-blue rounded-xl pt-3 py-1 px-5">
                    <p class="text-white font-weight-bold font-italic announcement-title">Galería de Eventos</p>
                </div>
            </div>
    
            <div class="row justify-content-center mb-5">
                @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner']])
            </div>
    
            <div class="input-group mb-3 card-2">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                <input type="text" id="buscador" class="form-control" placeholder="Buscar Evento" aria-label="Recipient's username" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon2"><i class="fas fa-search"></i></span>
                </div>
            </div>
            <div id="accordionExample">
                @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['gallery']])
            </div>
    
            @if(Auth::user()->hasRolePermission('announcement_gallery'))
                <div class="row d-flex justify-content-end">
                    <a href="{{ url('announcements?view=' . $display_announcements['gallery']['view'] . '&type=' . $display_announcements['gallery']['type']->id )}}" class="btn btn-primary btn-sm mt-2 mr-2" style="z-index:1;">
                        <i class="nav-icon fas fa-cog fa-lg"></i> Editar
                    </a>
                </div>
            @endif
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $("#buscador").on("keyup", function() {
        var value = $(this).val().toLowerCase();

        $("#accordionExample .encont").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
          

        });
          
        if($(".encont").filter(":visible").length == 0){
                $("#no_resultado").css("display", "block");
        }else{
                $("#no_resultado").css("display", "none");
         }


      });

    });

    $('#exampleModal').on('hidden.bs.modal', function(event) {
        $("img").remove('.sli');
        $("div").remove('.slick-list');
        $("div").removeClass('slick-initialized','slick-slider');
        $('.slider-for').empty();
        $('.slider-nav').empty();
    });

    $('#exampleModal').on('shown.bs.modal', function(event) {
        var button = $(event.relatedTarget);
        var filenames = button.data('name').split(",");
        var title = button.data('title');
        var modal = $(this);
        modal.find('.modal-title').text(title);
        
        $.each(filenames, function(index,value){   
            modal.find('.slider-for').append('<div class="gallery-box" style="background-image: url('+'\''+'img/announcements/' + filenames[index].replace(/"/g,'') +'\''+');background-position: center;background-repeat: no-repeat;background-size: contain;"></div>');
            modal.find('.slider-nav').append('<div class="gallery-nav" style="background-image: url('+'\''+'img/announcements/' + filenames[index].replace(/"/g,'') +'\''+');background-position: center;background-repeat: no-repeat;background-size: cover;"></div>');
        });

        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav',
        });

        $('.slider-nav').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            centerMode: true,
            focusOnSelect: true,
            arrows: true,
            adaptiveHeight:true,
            responsive: [
                {
                    breakpoint: 415,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        asNavFor: '.slider-for',
                        centerMode: true,
                        focusOnSelect: true,
                        arrows: true,
                        infinite: false,
                        dots: false
                    }
                }
            ]
        });
    });
</script>
@endsection