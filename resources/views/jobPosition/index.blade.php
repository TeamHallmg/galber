@extends('layouts.app')
@section('content')

<div class="container-fluid">
	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{!! Session::get('alert-'.$msg) !!} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	<div class="row mb-3">
		<div class="col-12 blue_header rounded py-3">
			<h2 class="font-italic font-weight-bold m-0"> Administrar Puestos</h2>
		</div>

		<div class="col-12 mt-4">
			<h3 class="text-blue font-weight-bold m-0" style="border-bottom: 4px solid rgb(137, 135, 136);">
				Listado de Puestos
				<a class="btn btn-sm btn-info float-right text-white" href="{{ url('jobPosition/create') }}">Nuevo</a>
			</h3>
		</div>
	</div>
		
	<div class="card">
		<div class="card-body">
			<div class="table-responsive">
				<div class="col-md-12 tableWrapper">
					<table class="table table-hover table-bordered w-100" id="tablejob">
						<thead class="blue_header">
							<tr>
								<th>Código</th>
								<th>Puesto</th>
								<th>Departamento</th>
								<th>Dirección</th>
								<th>Descripción</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
						@foreach($jobs as $job)
							<tr class="soft_blue_background">
								<td>{{ $job->id }}</td>
								<td>{{ $job->name }}</td>
								<td>{{ $job->getDepartmentName() }}</td>
								<td>{{ $job->getDirectionName() }}</td>
								<td>{{ ($job->description?$job->description:'N/A') }}</td>
								<td>
									<a class="btn btn-success btn-sm" href="{{ url('jobPosition/' . $job->id) }}"><span class="fa fa-eye"></span></a>
									<a class="btn btn-info btn-sm" href="{{ url('jobPosition/' . $job->id . '/edit') }}"><span class="fa fa-pen"></span></a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>
@endsection
	
@section('scripts')
	<script type="text/javascript">
	$(document).ready( function () {
		$('#tablejob').DataTable({
			"language": {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     '<span class="fa fa-arrow-right" aria-hidden="true"></span>',
					"sPrevious": '<span class="fa fa-arrow-left" aria-hidden="true"></span>',
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		});
	});
	</script>
@endsection