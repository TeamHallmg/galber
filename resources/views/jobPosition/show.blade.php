@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <div class="flash-message" id="mensaje">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if (Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{!! Session::get('alert-' . $msg) !!} <a href="#" class="close"
                            data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
            @endforeach
        </div>

        <div class="row mb-3">
            <div class="col-12 blue_header rounded py-3">
                <h2 class="font-italic font-weight-bold m-0"> Administrar Puestos</h2>
            </div>

            <div class="col-12 mt-4">
                <h3 class="text-blue font-weight-bold m-0" style="border-bottom: 4px solid rgb(137, 135, 136);">
                    Consultar Puesto

                    <a class="btn float-right mb-2 btn-info btn-sm text-white" href="{{ url('jobPosition') }}">Regresar</a>
                </h3>
            </div>
        </div>

        <div class="card">
            <div class="card-body">

                <div class="row">

                    <div class="col-md-4 col-sm-12">
                        <label for="name" class="col-form-label font-weight-bold">Nombre:</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $job->name }}" readonly>
                        <span class="text-danger"><strong>{{ $errors->first('name') }}</strong></span>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label class="col-form-label font-weight-bold" for="job_position_boss_id">Puesto Jefe:</label>
                        <select name="job_position_boss_id" id="job_position_boss_id" class="form-control selectpicker"
                            data-live-search="true" disabled>

                            <option value="" {{ $job->job_position_boss_id == '' ? 'selected' : null }}>Sin Puesto
                                Jefe
                            </option>

                            @foreach ($jobs as $jobP)

                                <option value="{{ $jobP->id }}"
                                    {{ $jobP->id == $job->job_position_boss_id ? 'selected' : null }}>
                                    {{ $jobP->name }}
                                </option>

                            @endforeach

                        </select>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <label for="area_id" class="col-form-label font-weight-bold">Departamento /
                            Dirección</label>
                        <select name="area_id" id="area_id" class="selectpicker form-control" data-live-search="true"
                            title="Elije la estructura del puesto..." disabled>
                            @foreach ($areas as $area)
                                <option value="{{ $area->id }}" {{ $job->area_id == $area->id ? 'selected' : '' }}>
                                    {{ $area->getStructureNames() ?? 'SIN ESTRUCTURA' }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <label class="col-form-label font-weight-bold" for="benefits">Beneficios:</label>
                        <select name="benefits" id="benefits" class="form-control" disabled>
                            <option value="" {{ is_null($job->benefits) ? 'hidden selected disabled' : '' }}>Sin beneficios</option>
                            <option value="A" {{ $job->benefits == 'A' ? 'selected' : '' }}>A</option>
                            <option value="B" {{ $job->benefits == 'B' ? 'selected' : '' }}>B</option>
                            <option value="C" {{ $job->benefits == 'C' ? 'selected' : '' }}>C</option>
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label class="col-form-label font-weight-bold" for="file">Archivo:</label>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 d-flex">
                                @if (!is_null($job->file))
                                    <a class="btn btn-outline-secondary btn-sm mr-1"
                                        href="{{ asset('puestos/' . $job->file) }}" target="_blank">
                                        <i class="fa fa-file-alt"></i>
									</a>
								@else
									Sin Archivo
                                @endif
                                <span class="text-danger"><strong>{{ $errors->first('file') }}</strong></span>
                            </div>
                        </div>
                    </div>

                    {{-- <div class="col-4">
                        <label for="file">Beneficios:</label>
                        <div class="row">
                            <div class="col-3">
                                @if (!is_null($job->benefits))
                                    <a class="btn btn-outline-secondary" href="{{ asset('puestos/' . $job->benefits) }}"
                                        target="_blank">
                                        <i class="fa fa-file-alt"></i> VER
                                    </a>
                                    @else
                                    <label for="" class="form-control">Sin archivo a mostrar</label>
                                @endif
                            </div>
                            <div class="col-9">
                                <input type="file" name="benefits" id="benefits">
                                <span class="text-danger"><strong>{{ $errors->first('benefits') }}</strong></span>
                            </div>
                        </div>
                    </div> --}}
                </div>

                <div class="row mt-3">
                    <div class="col-md-6">
                        <label class="col-form-label font-weight-bold" for="knowledge">Conocimientos:</label>
                        <textarea name="knowledge" id="knowledge" cols="15" rows="5" class="form-control"
                            style="resize: none;" readonly>{{ $job->knowledge }}</textarea>
                    </div>
                    <div class="col-md-6">
                        <label class="col-form-label font-weight-bold" for="experience">Funciones:</label>
                        <textarea name="experience" id="experience" cols="15" rows="5" class="form-control"
                            style="resize: none;" readonly>{{ $job->experience }}</textarea>
                    </div>
                </div>

                <div class="row mt-3">

                    <div class="col-6">
                        <label class="col-form-label font-weight-bold" for="comments">Objetivos:</label>
                        <textarea name="comments" id="comments" cols="15" rows="5" class="form-control"
                            style="resize: none;" readonly>{{ $job->comments }}</textarea>
                    </div>

                    <div class="col-6">
                        <label class="col-form-label font-weight-bold" for="description">Habilidades Requeridas:</label>
                        <textarea name="description" id="description" cols="15" rows="5" class="form-control"
                            style="resize: none;" readonly>{{ $job->description }}</textarea>
                    </div>

                </div>

                <div class="row mt-4">
                    <div class="col-12 mt-4">
                        <h3 class="text-blue font-italic font-weight-bold m-0"
                            style="border-bottom: 4px solid rgb(137, 135, 136);">Tipo de Recursos Disponibles para el puesto
                        </h3>
                    </div>
                </div>

                <div class="row">

                    @if ($TipoBienes->isEmpty())
                        <div class="col-md-12 text-center">

                            <p>
                                <strong>Este Puesto No Tiene Recursos Asociados</strong>
                            </p>

                        </div>
                    @endif

                    @foreach ($TipoBienes as $TipoBiene)

                        <div class="col-md-3 mt-2 ">

                            <label class="form-check-label" for="{{ $TipoBiene->nombre }}">{{ $TipoBiene->nombre }}</label>

                            <input type="text" name="" id="" class="form-control" readonly>


                        </div>

                    @endforeach

                </div>

                <div class="row mt-4">
                    <div class="col-12 mt-4">
                        <h3 class="text-blue font-italic font-weight-bold m-0"
                            style="border-bottom: 4px solid rgb(137, 135, 136);">
                            Cursos
                        </h3>
                    </div>
                    <div class="col-md-12 tableWrapper mt-4">
                        <table id="courses" class="table table-striped table-bordered w-100">
                            <thead style="background-color: #002C49; color:white;">
                                <tr>
                                    <th style="background-color: #005C99">ID</th>
                                    <th style="background-color: #005C99">Código</th>
                                    <th>Curso</th>
                                    <th>Nombre</th>
                                    <th>Calificación Mínima</th>
                                    <th>Prioridad</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($job_courses as $key => $course)
                                    <tr>
                                        <td style="vertical-align:middle" nowrap class="text-center">
                                            {{ $course->id }}
                                        </td>
                                        <td style="vertical-align:middle" nowrap class="text-center">
                                            {{ $course->code ?? 'N/A' }}
                                        </td>
                                        <td style="vertical-align:middle" nowrap class="text-center">
                                            {{ $course->getFrom() }}
                                        </td>
                                        <td style="vertical-align:middle" nowrap class="text-center">
                                            {{ $course->name }}
                                        </td>
                                        <td style="vertical-align:middle" nowrap class="text-center">
                                            {{ $course->job_min_grade ?? 'N/A' }}
                                        </td>
                                        <td style="vertical-align:middle" nowrap class="text-center">
                                            {{ $course->job_priority ?? 'N/A' }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>




            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        var jobposition_id = '{!!  $job->id !!}';

        $('#courses').DataTable({
            "order": [
                [0, "asc"]
            ],
            "paging": true,
            "pagingType": "numbers",
            "scrollX": true,
            "fixedColumns": {
                "leftColumns": 2
                // "rightColumns": 1,
            },
            language: {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": ">",
                    "sPrevious": "<"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });

    </script>
@endsection
