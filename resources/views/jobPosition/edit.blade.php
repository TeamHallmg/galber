@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <div class="flash-message" id="mensaje">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if (Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{!! Session::get('alert-' . $msg) !!} <a href="#" class="close"
                            data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
            @endforeach
        </div>

        <div class="row mb-3">
            <div class="col-12 blue_header rounded py-3">
                <h2 class="font-italic font-weight-bold m-0"> Administrar Puestos</h2>
            </div>

            <div class="col-12 mt-4">
                <h3 class="text-blue font-weight-bold m-0" style="border-bottom: 4px solid rgb(137, 135, 136);">
                    Editar Puesto

                    <a class="btn float-right mb-2 btn-info btn-sm text-white" href="{{ url('jobPosition') }}">Regresar</a>
                </h3>
            </div>
        </div>

        <div class="card">
            <div class="card-body">

                <form action="{{ route('jobPosition.update', $job->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="row">

                        <div class="col-md-4 col-sm-12">
                            <label for="name" class="col-form-label font-weight-bold requerido">Nombre:</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{ $job->name }}">
                            <span class="text-danger"><strong>{{ $errors->first('name') }}</strong></span>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <label class="col-form-label font-weight-bold" for="job_position_boss_id">Puesto Jefe:</label>
                            <select name="job_position_boss_id" id="job_position_boss_id" class="form-control selectpicker"
                                data-live-search="true">

                                <option value="" {{ $job->job_position_boss_id == '' ? 'selected' : null }}>Sin Puesto
                                    Jefe
                                </option>

                                @foreach ($jobs as $jobP)

                                    <option value="{{ $jobP->id }}"
                                        {{ $jobP->id == $job->job_position_boss_id ? 'selected' : null }}>
                                        {{ $jobP->name }}
                                    </option>

                                @endforeach

                            </select>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <label for="area_id" class="col-form-label font-weight-bold requerido">Departamento /
                                Dirección</label>
                            <select name="area_id" id="area_id" class="selectpicker form-control" data-live-search="true"
                                title="Elije la estructura del puesto..." required>
                                @foreach ($areas as $area)
                                    <option value="{{ $area->id }}" {{ $job->area_id == $area->id ? 'selected' : '' }}>
                                        {{ $area->getStructureNames() ?? 'SIN ESTRUCTURA' }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <label class="col-form-label font-weight-bold" for="benefits">Beneficios:</label>
                            <select name="benefits" id="benefits" class="form-control">
                                <option value="" {{ is_null($job->benefits) ? 'hidden selected disabled' : '' }}>Selecciona
                                    un beneficio</option>
                                <option value="A" {{ $job->benefits == 'A' ? 'selected' : '' }}>A</option>
                                <option value="B" {{ $job->benefits == 'B' ? 'selected' : '' }}>B</option>
                                <option value="C" {{ $job->benefits == 'C' ? 'selected' : '' }}>C</option>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <label class="col-form-label font-weight-bold" for="file">Archivo:</label>
                            <div class="row">
                                <div class="col-md-3 col-sm-12 d-flex">
                                    @if (!is_null($job->file))
                                        <a class="btn btn-outline-secondary btn-sm mr-1"
                                            href="{{ asset('puestos/' . $job->file) }}" target="_blank">
                                            <i class="fa fa-file-alt"></i>
                                        </a>
                                    @endif
                                    <input type="file" name="file" id="file">
                                    <span class="text-danger"><strong>{{ $errors->first('file') }}</strong></span>
                                </div>
                            </div>
                        </div>

                        {{-- <div class="col-4">
                            <label for="file">Beneficios:</label>
                            <div class="row">
                                <div class="col-3">
                                    @if (!is_null($job->benefits))
                                        <a class="btn btn-outline-secondary" href="{{ asset('puestos/' . $job->benefits) }}"
                                            target="_blank">
                                            <i class="fa fa-file-alt"></i> VER
                                        </a>
                                        @else
                                        <label for="" class="form-control">Sin archivo a mostrar</label>
                                    @endif
                                </div>
                                <div class="col-9">
                                    <input type="file" name="benefits" id="benefits">
                                    <span class="text-danger"><strong>{{ $errors->first('benefits') }}</strong></span>
                                </div>
                            </div>
                        </div> --}}
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-6">
                            <label class="col-form-label font-weight-bold" for="knowledge">Conocimientos:</label>
                            <textarea name="knowledge" id="knowledge" cols="15" rows="5" class="form-control"
                                style="resize: none;">{{ $job->knowledge }}</textarea>
                        </div>
                        <div class="col-md-6">
                            <label class="col-form-label font-weight-bold" for="experience">Funciones:</label>
                            <textarea name="experience" id="experience" cols="15" rows="5" class="form-control"
                                style="resize: none;">{{ $job->experience }}</textarea>
                        </div>
                    </div>

                    <div class="row mt-3">

                        <div class="col-6">
                            <label class="col-form-label font-weight-bold" for="comments">Objetivos:</label>
                            <textarea name="comments" id="comments" cols="15" rows="5" class="form-control"
                                style="resize: none;">{{ $job->comments }}</textarea>
                        </div>

                        <div class="col-6">
                            <label class="col-form-label font-weight-bold" for="description">Habilidades Requeridas:</label>
                            <textarea name="description" id="description" cols="15" rows="5" class="form-control"
                                style="resize: none;">{{ $job->description }}</textarea>
                        </div>

                    </div>

                    <div class="row mt-4">
                        <div class="col-12 mt-4">
                            <h3 class="text-blue font-weight-bold m-0" style="border-bottom: 4px solid rgb(137, 135, 136);">
                                Tipo de Recursos Disponibles para el
                                puesto
                            </h3>
                        </div>
                    </div>

                    <div class="form-row px-1">

                        @foreach ($TipoBienes as $TipoBiene)

                            <div
                                class="form-check col-md-3 form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mt-2 mb-md-0">

                                <label class="form-check-label"
                                    for="{{ $TipoBiene->nombre }}">{{ $TipoBiene->nombre }}</label>
                                <input class="form-check-input" type="checkbox" name="TipoBiene[]"
                                    id="{{ $TipoBiene->nombre }}" value="{{ $TipoBiene->id }}"
                                    {{ count($TipoBiene->jobpositionsbienes) > 0 ? 'checked' : '' }}>

                            </div>

                        @endforeach

                    </div>

                    <div class="row mt-4">
                        <div class="col-12 mt-4">
                            <h3 class="text-blue font-italic font-weight-bold m-0"
                                style="border-bottom: 4px solid rgb(137, 135, 136);">
                                Cursos
                                <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal"
                                    data-target="#ligarCursoModal">
                                    Ligar Curso
                                </button>
                            </h3>
                        </div>

                        <div class="col-md-12 tableWrapper mt-4">
                            <table id="courses" class="table table-striped table-bordered w-100">
                                <thead style="background-color: #002C49; color:white;">
                                    <tr>
                                        <th style="background-color: #005C99">ID</th>
                                        <th style="background-color: #005C99">Código</th>
                                        <th>Curso</th>
                                        <th>Nombre</th>
                                        <th>Calificación Mínima</th>
                                        <th>Prioridad</th>
                                        <th style="background-color: #005C99">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($job_courses as $key => $course)
                                        <tr>
                                            <td style="vertical-align:middle" nowrap class="text-center">
                                                {{ $course->id }}
                                            </td>
                                            <td style="vertical-align:middle" nowrap class="text-center">
                                                {{ $course->code ?? 'N/A' }}
                                            </td>
                                            <td style="vertical-align:middle" nowrap class="text-center">
                                                {{ $course->getFrom() }}
                                            </td>
                                            <td style="vertical-align:middle" nowrap class="text-center">
                                                {{ $course->name }}
                                            </td>
                                            <td style="vertical-align:middle" nowrap class="text-center">
                                                {{ $course->job_min_grade ?? 'N/A' }}
                                            </td>
                                            <td style="vertical-align:middle" nowrap class="text-center">
                                                {{ $course->job_priority ?? 'N/A' }}
                                            </td>
                                            <td style="vertical-align:middle" nowrap>
                                                <div class="form-row justify-content-center">
                                                    {{-- <button type="button"
                                                        class="btn btn-primary course_toogle add_course_{{ $course->id }} {{ in_array($course->id, $job->getIDCoursesArray()) ? 'd-none' : '' }}"
                                                        data-id="{{ $course->id }}" data-action="add">
                                                        <i class="fas fa-plus-circle"></i> Ligar
                                                    </button> --}}
                                                    {{-- <button type="button"
                                                        class="btn btn-danger course_toogle delete_course_{{ $course->id }} {{ !in_array($course->id, $job->getIDCoursesArray()) ? 'd-none' : '' }}"
                                                        data-id="{{ $course->id }}" data-action="delete">
                                                        <i class="fas fa-minus-circle"></i> Desligar
                                                    </button> --}}
                                                    <a href="{{ url('escalafon/cursos/eliminar_puesto/' . $course->id . '/' . $job->id) }}"
                                                        class="btn btn-danger">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>

                    <div class="row mt-3 float-right px-3">
                        <button type="submit" class="btn btn-success mr-3">Actualizar</button>
                        <a class="btn btn-info text-white" href="{{ url('jobPosition') }}">Regresar</a>
                    </div>

                </form>

            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="ligarCursoModal" tabindex="-1" role="dialog" aria-labelledby="ligarCursoModal"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST" action="{{ url('escalafon/ligar_puesto_curso') }}">
                    @csrf
                    <input type="hidden" name="job_position_id" value="{{ $job->id }}">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ligar Curso al Puesto</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <label for="course_id" class="col-form-label font-weight-bold requerido">Curso:</label>
                                    <select name="course_id" id="course_id" class="selectpicker form-control border cursos"
                                        data-live-search="true" title="Selecciona un curso..." required>
                                        @foreach ($courses as $id => $course)
                                            <option value="{{ $course->id }}" data-calificacion="{{ $course->min_grade }}"
                                                data-prioridad="{{ $course->priority }}">{{ $course->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <label for="calificacion"
                                        class="col-form-label font-weight-bold requerido text-wrap">Calificación
                                        Mínima:</label>
                                    <input type="number" min="0" max="100" step="1" name="calificacion" id="calificacion"
                                        class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <label for="prioridad"
                                        class="col-form-label font-weight-bold requerido text-wrap">Prioridad:</label>
                                    <input type="number" min="0" max="99" step="1" name="prioridad" id="prioridad"
                                        class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        var jobposition_id = '{!!  $job->id !!}';

        $('#courses').DataTable({
            "order": [
                [0, "asc"]
            ],
            "paging": true,
            "pagingType": "numbers",
            "scrollX": true,
            "fixedColumns": {
                "leftColumns": 2,
                // "rightColumns": 1
            },
            language: {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": ">",
                    "sPrevious": "<"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });


        $(function() {

            $('select[name=course_id]').change(function() {
                var calificacion = $(this).children("option:selected").data('calificacion');
                var prioridad = $(this).children("option:selected").data('prioridad');

                $('input[name=calificacion]').val(calificacion);
                $('input[name=prioridad]').val(prioridad);
            });



            $('#courses tbody').on('click', '.course_toogle', function() {
                var action = $(this).data('action');
                var course_id = $(this).data('id');
                console.log(action, course_id);
                $.ajax({
                    type: 'GET',
                    url: "{{ url('save_jobposition_course_data') }}/" + jobposition_id + '/' +
                        course_id + '/' + action,
                    dataType: 'json'
                }).done(function(data) {
                    // console.log(data);
                    if (data.fail !== undefined) {
                        alert('Error al procesar guardado de información...');
                    } else {
                        if (action == 'add') {
                            $('.delete_course_' + data.course_id).removeClass('d-none');
                            $('.add_course_' + data.course_id).addClass('d-none');
                        } else if (action == 'delete') {
                            $('.add_course_' + data.course_id).removeClass('d-none');
                            $('.delete_course_' + data.course_id).addClass('d-none');
                        }
                    }
                }).fail(function(httpObj, textStatus) {
                    if (httpObj.status == 401) { // Sesión invalida
                        alert('Tu sesión ha expirado, serás redirigido al inicio de sesión...');
                        window.location.replace("/login");
                    } else {
                        alert('Error al procesar guardado de información...');
                    }
                });
            });

        });

    </script>
@endsection