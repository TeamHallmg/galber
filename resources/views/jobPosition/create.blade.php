@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="flash-message" id="mensaje">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if (Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{!! Session::get('alert-' . $msg) !!} <a href="#" class="close"
                            data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
            @endforeach
        </div>

        <div class="row mb-3">
            <div class="col-12 blue_header rounded py-3">
                <h2 class="font-italic font-weight-bold m-0"> Administrar Puestos</h2>
            </div>
            <div class="col-12 mt-4">
                <h3 class="text-blue font-weight-bold m-0" style="border-bottom: 4px solid rgb(137, 135, 136);">
                    Crear Nuevo Puesto
                    <a class="btn float-right mb-2 btn-info btn-sm text-white" href="{{ url('jobPosition') }}">Regresar</a>
                </h3>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <form action="{{ url('jobPosition') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <label for="name" class="col-form-label font-weight-bold requerido">Nombre:</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                            <span class="text-danger"><strong>{{ $errors->first('name') }}</strong></span>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <label class="col-form-label font-weight-bold" for="job_position_boss_id">Puesto Jefe:</label>
                            <select name="job_position_boss_id" id="job_position_boss_id" class="form-control selectpicker"
                                data-live-search="true">
                                <option value="" selected>Sin Puesto Jefe</option>
                                @foreach ($jobs as $job)
                                    <option value="{{ $job->id }}">{{ $job->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    {{-- </div>
                    <div class="row"> --}}
                        <div class="col-md-4 col-sm-12">
                            <label for="area_id" class="col-form-label font-weight-bold requerido">Departamento /
                                Dirección</label>
                            <select name="area_id" id="area_id" class="selectpicker form-control" data-live-search="true"
                                title="Elije la estructura del puesto..." required>
                                @foreach ($areas as $area)
                                    <option value="{{ $area->id }}">
                                        {{ $area->getStructureNames() ?? 'SIN ESTRUCTURA' }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <label class="col-form-label font-weight-bold" for="benefits">Beneficios:</label>
                            <select name="benefits" id="benefits" class="form-control">
                                <option value="" selected hidden disabled>Selecciona un beneficio</option>
                                <option value="A" {{ old('benefits') == 'A' ? 'selected' : '' }}>A</option>
                                <option value="B" {{ old('benefits') == 'B' ? 'selected' : '' }}>B</option>
								<option value="C" {{ old('benefits') == 'C' ? 'selected' : '' }}>B</option>
                            </select>
                        </div>
						<div class="col-md-4 col-sm-12">
                            <label class="col-form-label font-weight-bold" for="file">Archivo:</label>
                            <div class="row">
                                <div class="col-md-3 col-sm-12 d-flex">
                                    <input type="file" name="file" id="file">
                                    <span class="text-danger"><strong>{{ $errors->first('file') }}</strong></span>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="col-4">
                            <label for="benefits">Beneficios:</label>
                            <input type="file" name="benefits" id="benefits">
                            <span class="text-danger"><strong>{{ $errors->first('benefits') }}</strong></span>
                        </div> --}}
                    </div>
                    <div class="row mt-3">
                        <div class="col-6">
                            <label class="col-form-label font-weight-bold" for="knowledge">Conocimientos:</label>
                            <textarea name="knowledge" id="knowledge" cols="15" rows="5" class="form-control"
                                style="resize: none;">{{ old('knowledge') }}</textarea>
                        </div>
                        <div class="col-6">
                            <label class="col-form-label font-weight-bold" for="experience">Funciones:</label>
                            <textarea name="experience" id="experience" cols="15" rows="5" class="form-control"
                                style="resize: none;">{{ old('experience') }}</textarea>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-6">
                            <label class="col-form-label font-weight-bold" for="comments">Objetivos:</label>
                            <textarea name="comments" id="comments" cols="15" rows="5" class="form-control"
                                style="resize: none;">{{ old('comments') }}</textarea>
                        </div>
                        <div class="col-6">
                            <label class="col-form-label font-weight-bold" for="description">Habilidades Requeridas:</label>
                            <textarea name="description" id="description" cols="15" rows="5" class="form-control"
                                style="resize: none;">{{ old('description') }}</textarea>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-12">
                            <h4 class="font-weight-bold text-blue ">Tipo de Recursos Disponibles para el puesto</h4>
                            <hr style="border: 2px solid grey;">
                        </div>
                    </div>
                    <div class="form-row px-1">
                        @foreach ($TipoBienes as $TipoBiene)
                            <div
                                class="form-check col-md-3 form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mt-2 mb-md-0">
                                <label class="form-check-label"
                                    for="{{ $TipoBiene->nombre }}">{{ $TipoBiene->nombre }}</label>
                                <input class="form-check-input" type="checkbox" name="TipoBiene[{{ $TipoBiene->id }}]"
                                    id="{{ $TipoBiene->nombre }}" value="{{ $TipoBiene->id }}">
                            </div>
                        @endforeach
                    </div>

                    <div class="row justify-content-end mt-3 px-3">
                        <button type="submit" class="btn btn-success mr-3">Guardar</button>
                        <a class="btn btn-info text-white" href="{{ url('jobPosition') }}">Regresar</a>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
