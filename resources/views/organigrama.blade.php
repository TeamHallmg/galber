@extends('layouts.app')

@section('title', 'Administración de Usuarios')

@section('content')

    <div class="row my-5">
        <div class="col">
            <div class="card card-3">
                <div class="card-header">
                    <h2 class="my-auto font-weight-bold">Organigrama</h2>
                </div>
                <div class="card-body" id="accordionExample">
                    <div class="row justify-content-end">
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control" name="buscador" id="buscador" placeholder="Buscar">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row mb-5 scroll" id="accordionExample">
                        {!! $list !!}
                    </div>
                    <div class="row mb-5 scroll ml-3" style="display: none;" id="no_resultado">
                        <h1 class="text-blue">Sin resultados de búsqueda...</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-yellow">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="modal-photo col-12 col-md-6">

                            </div>
                            <div class="modal-info col-12 col-md-6">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#buscador").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#accordionExample .encont").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
                });
                if ($(".encont").filter(":visible").length == 0) {
                    $("#no_resultado").css("display", "block");
                    console.log('if');
                } else {
                    $("#no_resultado").css("display", "none");
                    console.log('else');
                }
            });
        });

        $('.info').click(function() {
            var img = $(this).data('img');
            var name = $(this).data('name');
            var jobPosition = $(this).data('jobposition');
            var office = $(this).data('office');
            var email = $(this).data('email');
            var phone = $(this).data('phone');
            var mobile = $(this).data('mobile');
            var hobbies = $(this).data('hobbies');
            var department = $(this).data('department');

            $('.modal-header').empty();
            $('.modal-photo').empty();
            $('.modal-info').empty();

            $('.modal-header').append('<h2 class="m-0">' + name + ' - ' + jobPosition + ' - ' + department +
                '</h2>');
            $('.modal-photo').append('<img class="img-fluid" src=' + img + ' alt="' + img + '">');
            $('.modal-info').append(
                '<p>' + office + '</p>' +
                '<p>' + email + '</p>' +
                '<p>' + phone + '</p>' +
                '<p>' + hobbies + '</p>' +
                '<p>' + department + '</p>' +
                ''
            );
        });

        var genericCloseBtnHtml =
            '<a onclick="$(this).closest(\'div.popover\').popover(\'hide\');" type="button" class="close" aria-hidden="true">&times;</a>';
        $(function() {
            $('[data-toggle="popover"]').popover({
                // trigger:'focus',
                trigger: 'click',
                html: true,
                title: genericCloseBtnHtml,
                callback: function() {
                    $("#img_user").addClass("img-rounded img-responsive");
                }
            });
            $(document).on("click", ".popover .close", function() {
                $(this).parents(".popover").popover('hide');
            });
        });

        $('body').on('click', function(e) {
            //did not click a popover toggle, or icon in popover toggle, or popover
            if ($(e.target).data('toggle') !== 'popover' &&
                $(e.target).parents('[data-toggle="popover"]').length === 0 &&
                $(e.target).parents('.popover.in').length === 0) {
                $('[data-toggle="popover"]').popover('hide');
            }
        });

        $.fn.extend({
            treeview: function() {
                return this.each(function() {
                    // Initialize the top levels;
                    var tree = $(this);

                    tree.addClass('treeview-tree');
                    tree.find('li').each(function() {
                        var stick = $(this);
                    });

                    tree.find('li').each(function() {


                        var branch = $(this); //li with children ul
                        //console.log($(this).children('ul').children('li').attr('class'));
                        if ($(this).children('ul').length > 0) {

                            // branch.prepend("<i class='tree-indicator glyphicon glyphicon-folder-open'></i>");
                            branch.prepend("<i class='tree-indicator fas fa-users'></i>");
                            branch.addClass('tree-branch');
                            branch.on('click', function(e) {
                                if (this == e.target) {
                                    var icon = $(this).children('i:first');
                                    // icon.toggleClass("fa-users");
                                    // icon.toggleClass("fa-chevron-down");
                                    icon.toggleClass('expaded-tree');
                                    // $(this).children().children().toggle();
                                    if (icon.hasClass('expaded-tree')) {
                                        $(this).children().each(function(i, v) {
                                            $(this).children(i).hide();
                                        });
                                    }
                                    $(this).children().each(function(i, v) {
                                        $(this).children(i).toggle();
                                    });
                                }

                            });
                            branch.children().children().toggle();

                            /**
                             *  The following snippet of code enables the treeview to
                             *  function when a button, indicator or anchor is clicked.
                             *
                             *  It also prevents the default function of an anchor and
                             *  a button from firing.
                             */
                            branch.children('.tree-indicator, button, i').click(function(e) {
                                branch.click();
                                e.preventDefault();
                            });

                        } else {
                            branch.prepend(
                                "<i class='tree-indicator ' style='color: blue !important'></i>"
                                );
                        }

                    });

                });
            }
        });

        /**
         *  The following snippet of code automatically converst
         *  any '.treeview' DOM elements into a treeview component.
         */
        $(window).on('load', function() {

            $('.treeview').each(function() {
                var tree = $(this);
                tree.treeview();
            });
            $(document).ready(function() {
                $("#open").trigger("click");
            });
        });

    </script>

    <style type="text/css">
        [hidden] {
            display: none !important;
        }

        /*ORGANIGRAMA*/
        .scroll {
            max-width: auto;
            overflow-x: auto;
        }

        .popover-title .close {
            position: relative;
            bottom: 3px;
        }

        .close:hover {
            cursor: pointer;
        }

        div.treeview ul:first-child:before {
            display: none;
        }

        .treeview,
        .treeview ul {
            margin: 0;
            padding: 0;
            list-style: none;
            color: #369;
        }

        .treeview ul {
            margin-left: 1em;
            position: relative
        }

        .treeview ul ul {
            margin-left: .5em
        }

        .treeview ul:before {
            content: "";
            display: block;
            width: 0;
            position: absolute;
            top: 0;
            left: 0;
            border-left: 1px solid;

            /* creates a more theme-ready standard for the bootstrap themes */
            bottom: 15px;
        }

        .treeview li {
            margin: 0;
            padding: 0 1em;
            line-height: 2em;
            font-weight: 700;
            position: relative
        }

        .treeview ul .c1:before {
            width: 10px;
        }

        .treeview ul .c2:before {
            width: 32px;
        }

        .treeview ul .c3:before {
            width: 55px;
        }

        .treeview ul .c4:before {
            width: 85px;
        }

        .treeview ul .c5:before {
            width: 125px;
        }

        .treeview ul .c6:before {
            width: 175px;
        }

        .treeview ul .c7:before {
            width: 228px;
        }

        .treeview ul .c8:before {
            width: 250px;
        }

        .treeview ul li:before {
            content: "";
            display: block;
            height: 0;
            border-top: 1px dashed;
            border-width: 3px;
            margin-top: -1px;
            position: absolute;
            top: 1em;
            left: 0
        }

        .tree-indicator {
            margin-right: 5px;

            cursor: pointer;
        }

        .treeview li a {
            text-decoration: none;
            /*color:inherit;*/

            cursor: pointer;
        }

        .treeview li button,
        .treeview li button:active,
        .treeview li button:focus {
            text-decoration: none;
            color: inherit;
            border: none;
            background: transparent;
            margin: 0px 0px 0px 0px;
            padding: 0px 0px 0px 0px;
            outline: 0;
        }

        .orgChart {
            margin-bottom: 50px;
        }

        .img-orgChart {
            display: block;
            max-width: 50% !important;
            height: auto;
            float: left;
            padding-right: 1rem;
        }

        .popover {
            display: inline-block;
            /*white-space: nowrap;*/
            min-width: 500px;
            font-size: 1rem;
            box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
            border: 0;
            border-radius: 0;
        }

        .popover-body {
            padding: 0;
        }

        .popover-header {
            background-color: #EBC200;
            color: #002C48;
            border-bottom: 0px;
            padding-top: 0px;
            padding-bottom: 0px;
        }

        .bg-yellow {
            background-color: #244395;
            color: white;
        }

    </style>
@endsection
