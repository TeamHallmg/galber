@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            {{-- @if(isset($display_announcements) && Auth::user()->role === 'admin')
                @foreach ($display_announcements as $announcement)
                        <a class="btn btn-secondary btn-sm" href="{{ url('announcements?view=' . $announcement['view'] . '&type=' . $announcement['type']->id )}}">
                            <i class="fa fa-plus nav-icon"></i> <span>Administrar</span> {{ $announcement['type']->show_name }}
                        </a>
                @endforeach
            @endif --}}
            @foreach ($display_announcements as $announcement)
				@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $announcement])
            @endforeach
        </div>
    </div>
</div>
@endsection
