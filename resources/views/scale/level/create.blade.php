@extends('layouts.app')

@section('title', 'Administración de Niveles')

@section('content')
<div class="container">
<div class="row my-5">
    <div class="col">
        <div class="card card-3">
            <div class="card-header">
                <h2 class="my-auto font-weight-bold text-center">Escalafón | Crear Nuevo Nivel</h2>
            </div>
            @if($errors->any())
            <div class="container mt-4">
                <div class="alert alert-danger alert-dismissible fade show card-1" role="alert">
                    <strong>{{$errors->first()}}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            @endif
            <div class="card-body">
                <form method="POST" action="{{ url('escalafon/niveles') }}" id="store">
                    {!! method_field('POST') !!}
                    {!! csrf_field() !!}	
                    <div class="row">
                        <div class="form-group col-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label for="name" class="col-form-label font-weight-bold requerido">Nombre</label>
                                <input class="form-control" name="name" required value="{{old('name')}}" placeholder="Nombre...">
                            </div>
                        </div>
                        <div class="form-group col-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label for="virtues_score" class="col-form-label font-weight-bold requerido">Puntaje Virtudes</label>
                                <div class="input-group">
                                    <input type="number" step="1" class="form-control" name="virtues_score_from" value="{{old('virtues_score_from')}}" placeholder="Min..." required>
                                    <div class="input-group-prepend input-group-append">
                                        <div class="input-group-text"> - </div>
                                    </div>
                                    <input type="number" step="1" class="form-control" name="virtues_score_to" value="{{old('virtues_score_to')}}" placeholder="Max..." required>
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-group col-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label for="courses_score" class="col-form-label font-weight-bold requerido">Puntaje Cursos</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" name="courses_score_from" value="{{old('courses_score_from')}}" placeholder="Min..." min="0" required>
                                    <div class="input-group-prepend input-group-append">
                                        <div class="input-group-text"> - </div>
                                    </div>
                                    <input type="number" class="form-control" name="courses_score_to" value="{{old('courses_score_to')}}" placeholder="Max..." min="0" required>
                                </div>
                                
                            </div>
                        </div>
                        <div class="form-group col-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label for="commission" class="col-form-label font-weight-bold requerido">Meta Comisiones</label>
                                <div class="input-group">
                                    <input type="text" class="form-control currency" name="commission_from" value="{{old('commission_from')}}" placeholder="Min..." maxlength="14" required>
                                    <div class="input-group-prepend input-group-append">
                                        <div class="input-group-text"> - </div>
                                    </div>
                                    <input type="text" class="form-control currency" name="commission_to" value="{{old('commission_to')}}" placeholder="Max..." maxlength="14" required>
                                </div>
                            </div>
                        </div>


                        <div class="form-group col-12 col-md-12 mt-5">
                            <a href="{{url('escalafon/niveles')}}" class="btn btn-secondary mb-md-0 card-1"><i class="fas fa-arrow-alt-circle-left"></i> Volver</a>
                            <button type="submit" class="btn btn-success card-1">Guardar <i class="fas fa-save"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
</div>
@endsection
@section('mainScripts')
<script>

$( document ).ready(function() {
        function formatNumber(n) {
            return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function formatCurrency(input, blur) {
            var input_val = input.val();
            if (input_val === "") { return; }
            var original_len = input_val.length;
            var caret_pos = input.prop("selectionStart");
            
            if (input_val.indexOf(".") >= 0) {
                var decimal_pos = input_val.indexOf(".");
                var left_side = input_val.substring(0, decimal_pos);
                var right_side = input_val.substring(decimal_pos);

                left_side = formatNumber(left_side);
                right_side = formatNumber(right_side);

                if (blur === "blur") {
                    right_side += "00";
                }
                right_side = right_side.substring(0, 2);
                input_val = "$" + left_side + "." + right_side;

            } else {
                input_val = formatNumber(input_val);
                input_val = "$" + input_val;
        
                if (blur === "blur") {
                    input_val += ".00";
                }
            }
            input.val(input_val);
            var updated_len = input_val.length;
            caret_pos = updated_len - original_len + caret_pos;
            input[0].setSelectionRange(caret_pos, caret_pos);
        }

        $(".currency").each(function( index ) {
            formatCurrency($(this));
        });

        $(document).on('keyup', '.currency', function(){  
            formatCurrency($(this));
        });
    });

</script>
@endsection