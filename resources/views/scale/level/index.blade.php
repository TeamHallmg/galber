@extends('layouts.app')

@section('title', 'Administración de Niveles')

@section('content')

<div class="row my-5">
	<div class="col">
		<div class="card card-3">
			<div class="card-header">
				<h2 class="my-auto font-weight-bold text-center">Escalafón | Administración de Niveles</h2>
			</div>
			<div class="card-body">
				<div class="row mb-5">
					<div class="col col-md-12 my-3 text-center">
						<a href="{{url('escalafon/niveles/create')}}" class="btn btn-success mx-1 deleteBtn card-1">
							<i class="fas fa-plus-circle"></i> Crear Nuevo Nivel
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 tableWrapper">
						<table id="levels" class="table table-striped table-bordered w-100">
							<thead style="background-color: #002C49; color:white;">
								<tr>
									{{-- <th style="background-color: #005C99">ID</th> --}}
									<th style="background-color: #005C99">Nivel</th>
									<th>Nombre</th>
									<th>Virtudes <span class="text-muted">(Puntos)</span></th>
									<th>Cursos <span class="text-muted">(Puntos)</span></th>
									<th>Metas <span class="text-muted">(Comisiones)</span></th>
									<th style="background-color: #005C99">Opciones</th>
								</tr>
							</thead>
							<tbody>
								@foreach($levels as $key => $level)
									<tr>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{$level->id}}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{$level->name}}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{$level->virtues_score_from}} - {{$level->virtues_score_to}}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{$level->courses_score_from}} - {{$level->courses_score_to}}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ ($level->commission_from?'$ '.number_format($level->commission_from, 2):'N/A') }} - {{ ($level->commission_to?'$ '.number_format($level->commission_to, 2):'N/A') }}
										</td>
										<td style="vertical-align:middle" nowrap>
											<div class="form-row justify-content-center">
												<a data-toggle="tooltip" title="Editar" href="{{url('escalafon/niveles/'.$level->id.'/edit')}}" class="btn btn-dark mx-1 editBtn card-1">
													<i class="fas fa-pencil-alt"></i>
												</a>
												<button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#modelId" data-id="{{ $level->id }}">
													<i class="fas fa-trash"></i>
												</button>
											</div>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Eliminar -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form class="form" action="#" method="POST" id="form_delete">
				<input name="_method" type="hidden" value="DELETE">
				{{ csrf_field() }}
				<div class="modal-header">
					<h5 class="modal-title font-weight-bold">Eliminar Nivel</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
				</div>
				<div class="modal-body">
					<h2 class="text-center"><span class="text-warning"><i class="fas fa-exclamation-triangle"></i></span> ¡Advertencia!</h2><br>
					<h4 class="text-center" id="message">
						No se recomienda realizar esta acción.
					</h4>
					<div class="form-group">
						<label id="cnt_motive" for="del_reason">Motivo (0/200)</label>
						<textarea name="del_reason" class="form-control noresize" maxlength="200" id="del_reason" onkeyup="countMotive(this.value)" rows="4" required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-danger confirm-delete">Eliminar de todos modos</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('mainScripts')
<script type="text/javascript">

	function countMotive(str) {
		var lng = str.length;
		$("#cnt_motive").html('Motivo (' + lng + '/200)');
	}
		
	$('#levels').DataTable({
		"order": [[ 0, "asc" ]],
		"paging": true,
		"pagingType": "numbers",
		//"scrollX":true,
		//"scrollCollapse": true,
		"scrollX": true,
		"fixedColumns":{
			"leftColumns": 1,
			"rightColumns": 1,
		},
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     ">",
				"sPrevious": "<"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
	});

	$("#modelId").on('show.bs.modal', function (e) {
		var triggerLink = $(e.relatedTarget);
		var id = triggerLink.data("id");
		var action = '{{ url('escalafon/niveles') }}' + '/' + id;
		$("#form_delete").attr('action', action);
	});

	$("#modelId").on('hide.bs.modal', function (e) {
		$("#cnt_motive").html('Motivo (0/200)');
		$('#del_reason').val('');
	});

</script>
@endsection