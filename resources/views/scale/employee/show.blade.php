@extends('layouts.app')

@section('title', 'Administración de Virtudes')

@section('content')

@if($errors->any())
    <div class="container mt-4">
        <div class="alert alert-danger alert-dismissible fade show card-1" role="alert">
            <strong>{{$errors->first()}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endif

{{-- Mi escalafón --}}

{{-- <div class="container">
    <div class="row my-5">
        <div class="col"> --}}

<div class="card card-3">
    <div class="card-header" style="background-color: #002C49; color:white;">
        <h5 class="my-auto font-weight-bold">MI DESEMPEÑO</h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="form-group col text-center">
                @if ($user->profile != null && !is_null($user->profile->image))
                    <img src="{{ asset('uploads/'.$user->profile->image) }}" class="mb-3 img-fluid" id="profile_picture">
                @else
                    <img src="https://via.placeholder.com/250x250.png?text=Foto" class="mb-3" style="max-width: 300px; max-height: 183px" id="profile_picture">
                @endif
            </div>
            <div class="col-md-9">
                <div class="row mb-2">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control" value="{{ $user->employee->nombre }}" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Apellidos</label>
                            <input type="text" class="form-control" value="{{ $user->employee->paterno. ' ' .$user->employee->materno }}" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Puesto</label>
                            <input type="text" class="form-control" value="{{ ($user->employee->jobPosition?$user->employee->jobPosition->name:'N/A') }}" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Departamento</label>
                            <input type="text" class="form-control" value="{{ $user->nombre_departamento }}" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Área</label>
                            <input type="text" class="form-control" value="{{ $user->nombre_area }}" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Dirección</label>
                            <input type="text" class="form-control" value="{{ $user->nombre_direccion }}" readonly>
                        </div>
                    </div>
                    {{-- <div class="col-md-7">
                        <div class="form-group">
                            <label for="name">Unidad de Negocio</label>
                            <input type="text" class="form-control" value="N/A" readonly>
                        </div>
                    </div> --}}
                </div>
            </div>
            <div class="col-12 col-md-12 text-center">
                <div class="d-flex justify-content-center text-white px-3 pt-2 rounded-lg" style="background-color: #002C49;">
                <div class="{{$user->global_level['color']}} mr-3 rounded-lg align-middle h1 pt-2" style="width:60px; height: 60px;">{{ $user->global_level['code'] }}</div><h3>Nivel<br><strong>{{$user->global_level['name']}}</strong></h3>
                </div>
            </div>

            <div class="col-md-12">
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5 tableWrapper">
                <div class="card-header" style="margin-bottom:-20px;">
                    <h3 style="color: #343A40"><strong>Virtudes</strong></h3>
                </div>
                <table id="virtues" class="table table-striped table-bordered w-100 m0">
                    <thead style="background-color: #343A40; color:white;">
                        <tr>
                            {{-- <th>ID</th> --}}
                            <th>Valor</th>
                            {{-- <th>Descripción</th> --}}
                            <th>Calificación</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($virtues as $key => $virtue)
                            <tr>
                                {{-- <td nowrap class="text-center">
                                    {{$virtue->id}}
                                </td> --}}
                                <td nowrap class="text-center">
                                    {{$virtue->name}}
                                </td>
                                {{-- <td nowrap class="text-center">
                                    {{$virtue->description}}
                                </td> --}}
                                <td class="text-center">
                                    {{ $user->employee->getVirtueValue($virtue->id) }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="p-3 my-3 rounded-lg text-center" style="background-color: #EBC200; color:color: #343A40;">
                    <h5><strong>TOTAL: {{ $user->virtues_scale['score'] }}</strong></h5>
                </div>

                <div class="p-2 my-2 rounded-lg text-center" style="background-color: #002C49; color: white;">
                    <div class="d-flex justify-content-start text-white px-3 pt-2 rounded-lg" style="background-color: #002C49;">
                        <div class="{{$user->virtues_scale['color']}} mr-3 rounded-lg align-middle h1 pt-2" style="width:60px; height: 60px;">
                            {{ $user->virtues_scale['code'] }}
                        </div>
                        <h5 class="pt-2 text-left">
                            Mi nivel alcanzado | VIRTUDES<br><strong>{{ strtoupper($user->virtues_scale['name'])}}</strong>
                        </h5>
                    </div>
                </div>
            </div>
        
            <div class="col-md-7 tableWrapper">
                <div class="card-header" style="margin-bottom:-20px;">
                    <h3 style="color: #343A40"><strong>Cursos</strong></h3>
                </div>
                <table id="courses" class="table table-striped table-bordered w-100">
                    <thead style="background-color: #343A40; color:white;">
                        <tr>
                            {{-- <th>ID</th> --}}
                            <th>Nombre</th>
                            <th>Vigencia</th>
                            <th>Cursado</th>
                            <th>Valor</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($courses as $key => $course)
                            <tr>
                                {{-- <td nowrap class="text-center">
                                    {{$course->id}}
                                </td> --}}
                                <td nowrap class="text-center">
                                    {{$course->name}}
                                </td>
                                <td class="text-center">
                                    {{$course->validity}}
                                </td>
                                <td class="text-center">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" {{ $user->employee->checkIfHasCourseAndItsValid($course->id)?'checked':'' }} disabled>
                                        <label class="form-check-label">
                                        </label>
                                    </div>
                                </td>
                                <td style="vertical-align:middle" class="text-center">
                                    {{$course->value}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="p-3 my-3 rounded-lg text-center" style="background-color: #EBC200; color:color: #343A40;">
                    <h5><strong>TOTAL: {{ $user->courses_scale['score'] }}</strong></h5>
                </div>

                <div class="p-2 my-2 rounded-lg text-center" style="background-color: #002C49; color: white;">
                    <div class="d-flex justify-content-start text-white px-3 pt-2 rounded-lg" style="background-color: #002C49;">
                        <div class="{{$user->courses_scale['color']}} mr-3 rounded-lg align-middle h1 pt-2" style="width:60px; height: 60px;">
                            {{ $user->courses_scale['code'] }}
                        </div>
                        <h5 class="pt-2 text-left">
                            Mi nivel alcanzado | CURSOS<br><strong>{{ strtoupper($user->courses_scale['name'])}}</strong>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('mainScripts')
<script>

$( document ).ready(function() {

    $('.btn-image').on('click',function(){
        $('#image').click();
    });

    $('#virtues').DataTable({
		"order": [[ 0, "asc" ]],
		// "paging": true,
		// "pagingType": "numbers",
		"scrollX":true,
		"scrollCollapse": true,
		"scrollX": true,
        "searching": false,
        "bLengthChange": false,
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "",
			"sInfoEmpty":      "",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     ">",
				"sPrevious": "<"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
    });

    $('#courses').DataTable({
		"order": [[ 0, "asc" ]],
		// "paging": true,
		// "pagingType": "numbers",
		"scrollX":true,
		"scrollCollapse": true,
		"scrollX": true,
        "searching": false,
        "bLengthChange": false,
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "",
			"sInfoEmpty":      "",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     ">",
				"sPrevious": "<"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
    });

});

</script>
@endsection