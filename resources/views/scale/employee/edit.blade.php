@extends('layouts.app')

@section('title', 'Administración de Virtudes')

@section('content')
<div class="row my-5">
    <div class="col">
        <div class="card card-3">
            <div class="card-header" style="background-color: #002C49; color:white;">
                <h5 class="my-auto font-weight-bold">ADMINISTRAR DESEMPEÑO</h5>
            </div>
            @if($errors->any())
                <div class="container mt-4">
                    <div class="alert alert-danger alert-dismissible fade show card-1" role="alert">
                        <strong>{{$errors->first()}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif
            <form method="post" action="{{ url('escalafon/actualizar/'. $user->employee_id) }}" enctype="multipart/form-data" autocomplete="off">
                @csrf
                <input type="hidden" name="coursed_courses" value="">
                <input type="hidden" name="coursed_courses_dates" value="">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col text-center">
                            @if ($user->profile != null && !is_null($user->profile->image))
                                <img src="{{ asset('uploads/'.$user->profile->image) }}" class="mb-3 img-fluid" id="profile_picture">
                            @else
                                <img src="https://via.placeholder.com/250x250.png?text=Foto" class="mb-3" style="max-width: 300px; max-height: 183px" id="profile_picture">
                            @endif
                        </div>
                        <div class="col-md-9">
                            <div class="row mb-2">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Nombre</label>
                                        <input type="text" class="form-control" value="{{ $user->employee->nombre }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Apellidos</label>
                                        <input type="text" class="form-control" value="{{ $user->employee->paterno. ' ' .$user->employee->materno }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Puesto</label>
                                        <input type="text" class="form-control" value="{{ ($user->employee->jobPosition?$user->employee->jobPosition->name:'N/A') }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Departamento</label>
                                        <input type="text" class="form-control" value="{{ $user->nombre_departamento }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Área</label>
                                        <input type="text" class="form-control" value="{{ $user->nombre_area }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Dirección</label>
                                        <input type="text" class="form-control" value="{{ $user->nombre_direccion }}" readonly>
                                    </div>
                                </div>
                                {{-- <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="name">Unidad de Negocio</label>
                                        <input type="text" class="form-control" value="N/A" readonly>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                        <div class="col-12 col-md-12 text-center">
                            <div class="d-flex justify-content-center text-white px-3 pt-2 rounded-lg" style="background-color: #002C49;">
                                <div class="{{$user->global_level['color']}} mr-3 rounded-lg align-middle h1 pt-2" style="width:60px; height: 60px;">
                                    {{ $user->global_level['code'] }}
                                </div>
                                <h3>
                                    Nivel<br><strong>{{$user->global_level['name']}}
                                </h3>
                            </div>
                            {{-- <h3 class="text-white px-3 py-2 rounded-lg" style="background-color: #002C49;"><div class="bg-white" style="width:25px; height:25px;">T</div></strong></h3> --}}
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h4 style="color: #343A40">Virtudes</h4>
                            <hr style="border-bottom: 2px solid #ECC100;">
                        </div>
                        <div class="col-md-12 tableWrapper">                        
                            <table id="virtues" class="table table-striped table-bordered w-100 nowrap">
                                <thead style="background-color: #002C49; color:white; white-space: nowrap;">
                                    <tr>
                                        <th style="background-color: #005C99">Empleado</th>
                                        @foreach($virtues as $key => $virtue)
                                            <th data-toggle="tooltip" data-placement="bottom" title="{{ $virtue->description }}">{{ $virtue->name }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td nowrap class="text-center">
                                            {{ $user->employee->getFullNameAttribute() }}
                                        </td>
                                        @foreach($virtues as $key => $virtue)
                                            <td class="text-center">
                                                <div class="form-group">
                                                    <input type="number" name="virtue[{{$virtue->id}}]" value="{{ $virtue->score }}" min="0" max="10" step="1" class="form-control">
                                                </div>
                                            </td>
                                        @endforeach
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-12">
                            <h4 style="color: #343A40">Cursos</h4>
                            <hr style="border-bottom: 2px solid #ECC100;">
                        </div>
                        <div class="col-md-12 tableWrapper">                        
                            <table id="coursesv2" class="table table-striped table-bordered w-100 nowrap">
                                <thead style="background-color: #002C49; color:white; white-space: nowrap;">
                                    <tr>
                                        <th style="background-color: #005C99">Empleado</th>
                                        @foreach($courses as $key => $course)
                                            <th>{{ $course->name }} </th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td nowrap class="text-center">
                                            {{ $user->employee->getFullNameAttribute() }}
                                        </td>
                                        @foreach($courses as $key => $course)
                                            <td class="text-center">
                                                <div class="form-group">
                                                    <input type="date" name="course[{{$course->id}}]" value="{{$course->date_start}}" class="form-control">
                                                </div>
                                            </td>
                                        @endforeach
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    
                    <div class="col-md-12 text-right mt-3">
                        <hr>
                        <a class="btn btn-secondary" href="{{ url('escalafon') }}">Regresar</a>
                        <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Actualizar</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>
@endsection
@section('mainScripts')
<script>

    $(document).ready( function () {

        $('[data-toggle="tooltip"]').tooltip();

        $('#virtues, #coursesv2').DataTable({
            "fixedColumns":   {
                "leftColumns": "1",
                // "rightColumns": "1",
            },
            "order": [[ 0, "asc" ]],
            "paging": false,
            "scrollX":true,
            "searching": false,
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });

    });
</script>
@endsection