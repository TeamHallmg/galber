@extends('layouts.app')

@section('content')
<div class="row my-5">
		<div class="col">
			@if(session()->has('errors'))
				<div class="alert alert-danger alert-dismissible fade show card-1" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<strong>{{$errors->first()}}</strong>
				</div>
			@endif
			<div class="card card-3">
				<div class="card-header">
					<h2 class="my-auto font-weight-bold">Escalafón Usuarios</h2>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-12 tableWrapper">
						<table id="userAdmin" class="table table-striped table-bordered w-100">
								<thead style="background-color: #002C49; color:white;">
									<tr>
										<th style="background-color: #005C99">ID de Usuario</th>
										<th style="background-color: #005C99">Nivel</th>
										<th style="background-color: #005C99">No. de Empleado</th>
										<th style="background-color: #005C99">Nombre</th>
										<th style="background-color: #005C99">Apellidos</th>
										<th>Email Empresarial</th>
										<th>Email Personal</th>
										<th>Fecha de Ingreso</th>
										<th>Cumpleaños</th>
										<th>Fuente</th>
										<th>RFC</th>
										<th>CURP</th>
										<th>NSS</th>
										<th>Sexo</th>
										<th>Civil</th>
										<th>Teléfono</th>
										<th>Ext.</th>
										<th>Celular</th>
										<th>Rol</th>
										<th>Jefe</th>
										<th>Dirección</th>
										<th>Puesto</th>
										{{-- <th>Grado</th>
										<th>Región</th> --}}
										<th>Sucursal</th>
										<th>Nivel Virtudes</th>
										<th>Nivel Cursos</th>
										<th>Puntaje Virtudes</th>
										<th>Puntaje Cursos</th>
										{{-- <th>División</th>
										<th>Marca</th>
										<th>Centro</th>
										<th>Checador</th>
										<th>Turno</th>
										<th>Tipo Nómina</th>
										<th>Clave Nómina</th>
										<th>Nombre Nómina</th>
										<th>Generalista</th>
										<th>Relación</th>
										<th>Contrato</th>
										<th>Horario</th>
										<th>Jornada</th>
										<th>Cálculo</th>
										<th>Vacaciones</th>
										<th>Flotante</th>
										<th>Base</th>
										<th>Extra1</th>
										<th>Extra2</th>
										<th>Extra3</th>
										<th>Extra4</th>
										<th>Extra5</th> --}}

										<th style="background-color: #005C99">Acciones</th>
									</tr>
								</thead>
								<tbody>
									@forelse ($usuarios as $usuario)
									<tr @if($usuario->deleted_at != null) class="bg-warning" @endif>
										<td class="text-center">
											{{$usuario->employee->id}}
										</td>
										<td>
											<div class="d-flex justify-content-center text-dark px-3 rounded-lg text-center" >
												<div class="{{$usuario->global_level['color']}} mr-3 rounded-lg align-middle h5 pt-2 text-white" style="width:34px; height: 34px;">
													{{ $usuario->global_level['code'] }}
												</div>
												<h6 class="mt-2"><strong>{{$usuario->global_level['name']}}</strong></h6>
											</div>
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->idempleado}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->nombre}} 
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->paterno}} {{$usuario->employee->materno}} 
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->correoempresa}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->correopersonal}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->ingreso}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->nacimiento}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->fuente}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->rfc}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->curp}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->nss}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->sexo}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->civil}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->telefono}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->extension}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->celular}}
										</td>
										<td nowrap class="text-center">
											@if($usuario->employee->rol == 'admin')
											Administrador
											@elseif($usuario->employee->rol == 'employee')
											Empleado
											@endif
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->jefe}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->direccion}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->job_position_id}}
										</td>
										{{-- <td nowrap class="text-center">
											{{$usuario->employee->grado}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->region}}
										</td> --}}
										<td nowrap class="text-center">
											{{$usuario->employee->sucursal}}
										</td>

										{{-- <div class="d-flex justify-content-center text-dark px-3 pt-2 rounded-lg text-center" >
											<div class="{{$usuario->global_level['color']}} mr-3 rounded-lg align-middle h5 pt-2 text-white" style="width:34px; height: 34px;">
												{{ $usuario->global_level['code'] }}
											</div>
											<h6 class="mt-2"><strong>{{$usuario->global_level['name']}}</strong></h6>
										</div>
										<div class="d-flex justify-content-center text-dark px-3 pt-2 rounded-lg text-center" >
											<div class="{{$usuario->global_level['color']}} mr-3 rounded-lg align-middle h5 pt-2 text-white" style="width:34px; height: 34px;">
												{{ $usuario->global_level['code'] }}
											</div>
											<h6 class="mt-2"><strong>{{$usuario->global_level['name']}}</strong></h6>
										</div> --}}
										<td>
											<div class="d-flex justify-content-center text-dark px-3 rounded-lg text-center" >
												<div class="{{$usuario->virtues_scale['color']}} mr-3 rounded-lg align-middle h5 pt-2 text-white" style="width:34px; height: 34px;">
													{{ $usuario->virtues_scale['code'] }}
												</div>
												<h6 class="mt-2"><strong>{{$usuario->virtues_scale['name']}}</strong></h6>
											</div>
										</td>
										<td>
											<div class="d-flex justify-content-center text-dark px-3 rounded-lg text-center" >
												<div class="{{$usuario->courses_scale['color']}} mr-3 rounded-lg align-middle h5 pt-2 text-white" style="width:34px; height: 34px;">
													{{ $usuario->courses_scale['code'] }}
												</div>
												<h6 class="mt-2"><strong>{{$usuario->courses_scale['name']}}</strong></h6>
											</div>
										</td>
										<td nowrap class="text-center">
											{{ $usuario->virtues_scale['score'] }}
										</td>
										<td nowrap class="text-center">
											{{ $usuario->courses_scale['score'] }}
										</td>
										{{-- <td nowrap class="text-center">
											{{$usuario->employee->division}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->marca}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->centro}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->checador}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->turno}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->tiponomina}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->clavenomina}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->nombrenomina}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->generalista}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->relación}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->contrato}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->horario}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->jornada}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->calculo}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->vacaciones}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->Flotante}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->Base}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->extra1}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->extra2}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->extra3}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->extra4}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->extra5}}
										</td> --}}
										<td style="vertical-align:middle" nowrap>
											{{-- @if(!$usuario->deleted_at)
												<div class="form-row d-inline justify-content-center align-content-center"> --}}
													<a href="{{url('escalafon/editar/'.$usuario->employee->id)}}" class="btn btn-info mx-1 editBtn card-1 text-white">
														<i class="fas fa-layer-group"></i>
													</a>
												{{-- </div>
											@endif --}}
										</td>
									</tr>
								
									@empty
									<tr>
										<td colspan="8" class="text-center">No se encontraron usuarios.</td>
									</tr>
									@endforelse
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
@endsection
@section('mainScripts')
<script type="text/javascript">
		$('#userAdmin').DataTable({
			"order": [[ 0, "asc" ]],
			"paging": true,
			"pagingType": "numbers",
			//"fixedColumns": false,
			"scrollX": true,
			"fixedColumns":{
				"leftColumns": 5,
				"rightColumns": 1,
			},
			language: {
		 		"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     ">",
					"sPrevious": "<"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
	  });
		
</script>
@endsection