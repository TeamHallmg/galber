@extends('layouts.app')

@section('title', 'Administración de Virtudes')

@section('content')
<div class="container">
<div class="row my-5">
    <div class="col">
        <div class="card card-3">
            <div class="card-header">
                <h2 class="my-auto font-weight-bold text-center">Escalafón | Crear Nueva Virtud</h2>
            </div>
            @if($errors->any())
            <div class="container mt-4">
                <div class="alert alert-danger alert-dismissible fade show card-1" role="alert">
                    <strong>{{$errors->first()}}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            @endif
            <div class="card-body">
                <form method="POST" action="{{ url('escalafon/virtudes') }}">
                    {!! method_field('POST') !!}
                    {!! csrf_field() !!}	
                    <div class="row">
                        <div class="form-group col-12 col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-form-label font-weight-bold requerido">Nombre</label>
                                <input class="form-control" name="name" required value="{{old('name')}}">
                            </div>
                        </div>
                        <div class="form-group col-12 col-md-12">
                            <div class="form-group">
                                <label for="description" class="col-form-label font-weight-bold requerido">Descripción</label>
                                <textarea class="form-control" name="description" rows="3" required>{{old('description')}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-12 col-md-12 mt-3">
                            <a href="{{url('escalafon/virtudes')}}" class="btn btn-secondary mb-md-0 card-1"><i class="fas fa-arrow-alt-circle-left"></i> Volver</a>
                            <button type="submit" class="btn btn-success card-1">Guardar <i class="fas fa-save"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
</div>
@endsection
@section('mainScripts')
<script>

</script>
@endsection