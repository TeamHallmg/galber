@extends('layouts.app')

@section('title', 'Administración de Cursos')

@section('content')
<div class="container">
<div class="row my-5">
    <div class="col">
        <div class="card card-3">
            <div class="card-header bg-blue text-white">
                <h2 class="my-auto font-weight-bold text-center">Escalafón | Crear Nuevo Curso</h2>
            </div>
            @if($errors->any())
            <div class="container mt-4">
                <div class="alert alert-danger alert-dismissible fade show card-1" role="alert">
                    <strong>{{$errors->first()}}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            @endif
            <div class="card-body">
                <form method="POST" action="{{ url('escalafon/cursos') }}">
                    {!! method_field('POST') !!}
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="form-group col-md-2 col-sm-12">
                            <label for="code" class="col-form-label font-weight-bold requerido">Código</label>
                            <input type="text" class="form-control" id="code" name="code" value="{{old('code')}}" required>
                        </div>
                    {{-- </div>
                    <div class="row"> --}}
                        <div class="form-group col-md-2 col-sm-12">
                            <label for="course" class="col-form-label font-weight-bold">Curso</label>
                            <select id="course" name="course" class="selectpicker form-control" data-live-search="true" readonly>
                                <option value="M" {{(old('course') == 'M'?'selected':'')}}>
                                    Universidad Ucin
                                </option>
                                {{-- <option value="N" {{(old('course') == 'N'?'selected':'')}}>
                                    Nuevo
                                </option> --}}
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-12 d-moodle">
                            <label for="moodle_category" class="col-form-label font-weight-bold requerido">Categoría</label>
                            <select id="moodle_category" name="moodle_category" class="selectpicker form-control" data-live-search="true" title="Selecciona una opción..." required>
                                @foreach($moodle_categories as $key => $category)
                                    <option value="{{$category->id}}" {{(old('moodle_category') == $category->id?'selected':'')}}>
                                        {{$category->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-12 d-moodle">
                            <label for="moodle_course" class="col-form-label font-weight-bold requerido">Nombre</label>
                            <select id="moodle_course" name="moodle_course" class="selectpicker form-control" data-live-search="true" title="Selecciona una opción..." required>
                                {{-- @foreach($moodle_courses as $key => $course)
                                    <option value="{{$course->id}}" {{(old('moodle_course') == $course->id?'selected':'')}}>
                                        {{$course->fullname}}
                                    </option>
                                @endforeach --}}
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-12 d-name">
                            <label for="category" class="col-form-label font-weight-bold requerido">Categoría</label>
                            <input class="form-control" id="category" name="category" value="{{old('category')}}">
                        </div>
                        <div class="form-group col-md-4 col-sm-12 d-name">
                            <label for="name" class="col-form-label font-weight-bold requerido">Nombre</label>
                            <input class="form-control" id="name" name="name" value="{{old('name')}}">
                        </div>
                        <div class="form-group col-md-2 col-sm-12">
                            <label for="type" class="col-form-label font-weight-bold">Tipo</label>
                            <select id="type" name="type" class="selectpicker form-control" data-live-search="true">
                                <option value="I" {{(old('course') == 'I'?'selected':'')}}>
                                    Interno
                                </option>
                                <option value="E" {{(old('course') == 'E'?'selected':'')}}>
                                    Externo
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-2 col-sm-12">
                            <label for="modality" class="col-form-label font-weight-bold requerido">Modalidad</label>
                            <select id="modality" name="modality" class="selectpicker form-control" data-live-search="true" required>
                                <option value="virtual" {{(old('modality') == 'virtual'?'selected':'')}}>
                                    E-Learning
                                </option>
                                <option value="face-to-face" {{(old('modality') == 'face-to-face'?'selected':'')}}>
                                    Presencial
                                </option>
                                <option value="blended" {{(old('modality') == 'blended'?'selected':'')}}>
                                    Blended Learning
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="min_grade" class="col-form-label font-weight-bold requerido text-wrap">Calificación Mínima</label><small class="text-secondary ml-1">(0-100)</small>
                                <input type="number" min="0" max="100" step="1" name="min_grade" id="min_grade" class="form-control" value="0" required>
                            </div>
                        </div>
                        <div class="form-group col-md-4 col-sm-12">
                            <label for="priority" class="col-form-label font-weight-bold requerido">Prioridad</label><small class="text-secondary ml-1">(0-99)</small>
                            <input type="number" min="0" max="99" class="form-control" name="priority" value="{{old('priority', 0)}}" required>
                        </div>
                        <div class="form-group col-md-4 col-sm-12">
                            <label for="trainer" class="col-form-label font-weight-bold">Capacitador</label>
                            <select id="trainer" name="trainer[]" class="selectpicker form-control" data-live-search="true" title="Selecciona una o varias opciones..." multiple>
                                @foreach($employees as $key => $employee)
                                    <option value="{{$employee->id}}" data-subtext="({{ $employee->getPuestoName() }})">
                                        {{$employee->getFullNameAttribute()}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-12">
                            <label for="institution" class="col-form-label font-weight-bold requerido">Institución</label>
                            <input type="text" class="form-control" id="institution" name="institution" value="{{old('institution')}}" required>
                        </div>
                        <div class="form-group col-md-4 col-sm-12">
                            <label for="competence" class="col-form-label font-weight-bold">Competencias que desarrolla</label>
                            <select id="competence" name="competence[]" class="selectpicker form-control" data-live-search="true" title="Selecciona una o varias opciones..." multiple>
                                @foreach($factors as $key => $factor)
                                    <option value="{{$factor->id}}">
                                        {{$factor->nombre}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3 col-sm-12">
                            <label for="validity" class="col-form-label font-weight-bold">Vigencia<span class="text-secondary ml-1">(Días)</span></label>
                            <input type="number" min="0" class="form-control" name="validity" value="{{old('validity')}}">
                        </div>
                        <div class="form-group col-md-3 col-sm-12">
                            <label for="expire_at" class="col-form-label font-weight-bold">Caducidad</label>
                            <input type="date" class="form-control" name="expire_at" value="{{old('expire_at')}}">
                        </div>
                        <div class="form-group col-md-3 col-sm-12">
                            <label for="duration" class="col-form-label font-weight-bold">Duración<span class="text-secondary ml-1">(Horas)</span></label>
                            <input type="number" min="0" class="form-control" name="duration" value="{{old('duration')}}" readonly>
                        </div>
                        {{-- <div class="form-group col-md-2 col-sm-12">
                            <label for="budget" class="col-form-label font-weight-bold">Presupuesto</label>
                            <input type="text" id="budget" name="budget" class="form-control currency">
                        </div> --}}
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 col-sm-6">
                            <label for="extern_rfc" class="col-form-label font-weight-bold">RFC Agente Externo</label>
                            <input type="text" class="form-control" id="extern_rfc" name="extern_rfc[]" value="{{old('extern_rfc')}}">
                        </div>
                        <div class="form-group col-md-5 col-sm-6">
                            <label for="instructor_name" class="col-form-label font-weight-bold">Nombre Instructor</label>
                            <input type="text" class="form-control" id="instructor_name" name="instructor_name[]" value="{{old('instructor_name')}}">
                        </div>
                        <div class="form-group col-md-1 col-sm-1 align-self-end">
                            <button type="button" class="btn btn-lightblue" id="add_rfc">
                                <span class="fa fa-plus"></span>
                            </button>
                        </div>
                        <div class="col-md-12">
                            <div class="row" id="rfc_div">
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="observations" class="col-form-label font-weight-bold">Observaciones</label>
                            <textarea class="form-control" id="observations" name="observations">{{old('observations')}}</textarea>
                        </div>


                        {{-- <div class="form-group col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="value" class="col-form-label font-weight-bold requerido">Valor</label>
                                <input type="number" min="0" class="form-control" name="value" value="{{old('value')}}" required>
                            </div>
                        </div> --}}

                        <div class="form-group col-12 col-md-12 mt-3">
                            <a href="{{url('escalafon/cursos')}}" class="btn btn-primary mb-md-0 card-1"><i class="fas fa-arrow-alt-circle-left"></i> Volver</a>
                            <button type="submit" class="btn btn-success card-1">Guardar <i class="fas fa-save"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
</div>
@endsection
@section('scripts')
<script>
    $('.d-name').hide();
    $('#course').change(function() {
        var qwerty = $(this).val();
        if(qwerty == 'M') {
            $('.d-name').hide();
            $('.d-moodle').show();
            $('#name').prop('required', false);
            $('#moodle_course').prop('required', true);
            $('#moodle_category').prop('required', true);
        } else if(qwerty == 'N') {
            $('.d-name').show();
            $('.d-moodle').hide();
            $('#name').prop('required', true);
            $('#moodle_course').prop('required', false);
            $('#moodle_category').prop('required', false);
        }
        $('#moodle_course').selectpicker('refresh');
        $('#moodle_category').selectpicker('refresh');
    });

    $('#type').change(function() {
        var type = $(this).val();
        if(type == 'I') {
            $('input[name=duration]').val('');
            $('input[name=duration]').attr('readonly', true);
        } else if(type == 'E') {
            $('input[name=duration]').attr('readonly', false);
        }
    });

    $('#moodle_category').change(function(){
        var category_id = $(this).val();
        $('#moodle_course').empty();
        $('#moodle_course').selectpicker('refresh');
        getMoodleCoursesByCategory(category_id);
    });

    function getMoodleCoursesByCategory(category_id) {
        $.ajax({
            url: "{{url('escalafon/get_moodle_courses_by_category')}}/"+category_id,
            Type:'GET',
            success: function(result){
                $.each(result, function(i, item){
                    $('#moodle_course').append($('<option>', { 
                        value: item.id,
                        text : item.fullname 
                    })).selectpicker('refresh');
                });
            }
        });
    }

    /*acciones al presionar el boton de guardar*/
    $('#add_rfc').on('click',  function(e) {
        e.preventDefault();
        nameInput = '<input type="text" class="form-control" name="instructor_name[]">';
        rfcInput = '<input type="text" class="form-control" name="extern_rfc[]">';
        deleteRow = '<div class="form-group col-md-1 col-sm-1 align-self-end"><button type="button" class="btn btn-blue borrar"><span class="fa fa-minus"></span></button></div>';
        $('#rfc_div').append('<div class="form-group col-md-6 col-sm-5 line">'+rfcInput+'</div>'+'<div class="form-group col-md-5 col-sm-5 line">'+nameInput+'</div>'+deleteRow);
    });
        /*eliminar renglón */
    $("#rfc_div").on('click', '.borrar',function(e) {
        e.preventDefault();
        $(this).parent().prev().prev().remove();
        $(this).parent().prev().remove();
        $(this).parent().remove();
    });

    function formatNumber(n) {
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function formatCurrency(input, blur) {
        var input_val = input.val();
        if (input_val === "") { return; }
        var original_len = input_val.length;
        var caret_pos = input.prop("selectionStart");
        
        if (input_val.indexOf(".") >= 0) {
            var decimal_pos = input_val.indexOf(".");
            var left_side = input_val.substring(0, decimal_pos);
            var right_side = input_val.substring(decimal_pos);

            left_side = formatNumber(left_side);
            right_side = formatNumber(right_side);

            if (blur === "blur") {
                right_side += "00";
            }
            right_side = right_side.substring(0, 2);
            input_val = "$ " + left_side + "." + right_side;

        } else {
            input_val = formatNumber(input_val);
            input_val = "$ " + input_val;
    
            if (blur === "blur") {
                input_val += ".00";
            }
        }
        input.val(input_val);
        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
    }

    $(".currency").each(function( index ) {
        formatCurrency($(this));
    });

    $(document).on('keyup', '.currency', function(){  
        formatCurrency($(this));
    });
</script>
@endsection