@extends('layouts.app')

@section('title', 'Administración de Cursos')

@section('content')
<div class="container">
<div class="row my-5">
    <div class="col">
        <div class="card card-3">
            <div class="card-header bg-blue text-white">
                <h2 class="my-auto font-weight-bold text-center">Escalafón | Cursos por Puesto</h2>
            </div>
            @if($errors->any())
            <div class="container mt-4">
                <div class="alert alert-danger alert-dismissible fade show card-1" role="alert">
                    <strong>{{$errors->first()}}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            @endif
            <div class="card-body">
                <form method="POST" action="{{ url('escalafon/cursos/actualizar_puestos/'.$course->id) }}">
                    @csrf

                    <div class="row">
                        <div class="col-12 mt-1 mb-3">
                            <h5 class="text-white font-weight-bold m-0 rounded-pill bg-lightblue px-3 py-1">
                                {{ $course->name }}
                            </h5>
                        </div>
                        <div class="form-group col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="jobposition_id" class="col-form-label font-weight-bold requerido">Puesto(s):</label>
                                <select name="jobposition_id[]" id="jobposition_id" class="selectpicker form-control border puestos" data-live-search="true" title="Selecciona uno o varios puestos..." multiple required>
                                    @foreach ($jobpositions as $id => $jobposition)
                                        <option value="{{ $id }}">{{ $jobposition }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3 col-sm-12">
                            <div class="form-group">
                                <label for="calificacion" class="col-form-label font-weight-bold requerido text-wrap">Calificación Mínima:</label>
                                <input type="number" min="0" max="100" step="1" name="calificacion" id="calificacion" class="form-control" value="{{ $course->min_grade }}" required>
                            </div>
                        </div>
                        <div class="form-group col-md-3 col-sm-12">
                            <div class="form-group">
                                <label for="prioridad" class="col-form-label font-weight-bold requerido text-wrap">Prioridad:</label>
                                <input type="number" min="0" max="99" step="1" name="prioridad" id="prioridad" class="form-control" value="{{ $course->priority }}" required>
                            </div>
                        </div>
                        <div class="form-group col-md-2 col-sm-2 d-flex m-auto justify-content-center">
                            <button type="submit" class="btn btn-success btn-sm">
                                <span class="fas fa-save"></span> Guardar
                            </button>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12 tableWrapper mt-4">
                            <table id="jobs" class="table table-striped table-bordered w-100">
                                <thead class="bg-blue text-white">
                                    <tr>
                                        {{-- <th style="background-color: #005C99">ID</th> --}}
                                        <th class="bg-lightblue">Puesto</th>
                                        <th>Calificación Mínima</th>
                                        <th>Prioridad</th>
                                        <th class="bg-lightblue">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($course->jobPositionData as $data)
                                        <tr>
                                            {{-- <td style="vertical-align:middle" nowrap class="text-center">
                                                {{ $jobposition->job_position_id }}
                                            </td> --}}
                                            <td style="vertical-align:middle" nowrap class="text-center">
                                                {{ $data->getJobName() ?? 'N/A' }}
                                            </td>
                                            <td style="vertical-align:middle" nowrap class="text-center">
                                                {{ $data->score }}
                                            </td>
                                            <td style="vertical-align:middle" nowrap class="text-center">
                                                {{ $data->priority }}
                                            </td>
                                            <td style="vertical-align:middle" nowrap>
                                                <div class="form-row justify-content-center">
                                                    <a href="{{ url('escalafon/cursos/eliminar_puesto/'.$course->id.'/'.$data->model_id) }}" class="btn btn-danger">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
            
                    </div>
                    {{-- <div id="cursos_div"></div> --}}

                    <div class="form-group col-12 col-md-12 mt-3">
                        <a href="{{url('escalafon/cursos')}}" class="btn btn-primary mb-md-0 card-1"><i class="fas fa-arrow-alt-circle-left"></i> Volver</a>
                        {{-- <button type="submit" class="btn btn-success card-1">Guardar <i class="fas fa-save"></i></button> --}}
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
</div>
@endsection
@section('scripts')
<script>

    $('#jobs').DataTable({
		"order": [[ 0, "asc" ]],
		"paging": true,
		"pagingType": "numbers",
		//"scrollX":true,
		//"scrollCollapse": true,
		"scrollX": true,
		// "fixedColumns":{
		// 	"leftColumns": 1,
		// 	"rightColumns": 1,
		// },
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     ">",
				"sPrevious": "<"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
	});

    /*creamos arreglo */
    var puesto_id = [];
    var calificacion = [];
    var priority = [];
    var default_priority = '{!! $course->priority !!}';
    
    skills = {!! json_encode($course->puestos) !!};
    $.each(skills, function(i, item) {
        inputPuestoId = '<input type="hidden" class="form-control" name="puesto_id[]" value="'+item.pivot.job_position_id+'">';
        inputPuestoId1 = '<div class="form-group col-5 col-md-5"><div class="form-group"><input type="text" class="form-control" value="'+item.name+'" readonly></div></div>';
        InputCalificacion = '<div class="form-group col-2 col-md-2"><div class="form-group"><input type="text" class="form-control" name="calificacion[]" value="'+item.pivot.calificacion+'" readonly></div></div>';
        deleteRow = '<div class="form-group col-1 col-md-1"><button type="button" class="btn btn-danger" id="borrar"><span class="fa fa-minus"></span></button></div>';
        $('#cursos_div').append('<div class="row line">'+inputPuestoId+inputPuestoId1+InputCalificacion+deleteRow+'</div>');
    });

    
    /*acciones al presionar el boton de guardar*/
    $('#add_cursos').on('click',  function(e) {
        e.preventDefault();
        // console.log($('#puesto_id').val(), $('#calificacion').val(), $('#priority').val());
        if($('#puesto_id').val() == '') {
            alert('No se ha seleccionado ningún puesto');
            return false;
        }
        if($('#calificacion').val() == '') {
            alert('No se ha asignado una calificación mínima');
            return false;
        }
        if($('#prioridad').val() == '') {
            alert('No se ha asigando una prioridad');
            return false;
        }
        if($.inArray($('#puesto_id').val(), puesto_id) != -1) {
            alert('Puesto ya asignado');
            $('#puesto_id option:first').prop("selected", true);
            $('#puesto_id').trigger('change');
            return false;
        }
        /*agregamos el valor del id del select a un arreglo */
        puesto_id.push($('#puesto_id').val());
        console.log(puesto_id);
        calificacion.push($('#calificacion').val());
        priority.push($('#prioridad').val());
        var cursoNombre = $('#name').val();
        var puestoNombre = $('#puesto_id option:selected').html();
        /*obtenemos el último valor agregado del arreglo*/
        var lastPuestoId = puesto_id[puesto_id.length-1];
        var lastCalificacion = calificacion[calificacion.length-1];
        var lastPriority = priority[priority.length-1];
        /*asignamos los valores a campos input para mostrarlos */
        inputPuestoId = '<input type="hidden" class="form-control" name="puesto_id[]" value="'+lastPuestoId+'">';
        inputPuestoId1 = '<div class="form-group col-md-5 col-sm-12"><div class="form-group"><input type="text" class="form-control" value="'+puestoNombre+'" readonly></div></div>';
        InputCalificacion = '<div class="form-group col-md-3 col-sm-12"><div class="form-group"><input type="text" class="form-control" name="calificacion[]" value="'+lastCalificacion+'" readonly></div></div>';
        InputPriority = '<div class="form-group col-md-3 col-sm-12"><div class="form-group"><input type="text" class="form-control" name="prioridad[]" value="'+lastPriority+'" readonly></div></div>';
        deleteRow = '<div class="form-group col-md-1 col-sm-2"><button type="button" class="btn btn-danger" id="borrar"><span class="fa fa-minus"></span></button></div>';
        /*asignamos las variables con campos input al div y mostrarlos en pantalla*/
        $('#cursos_div').append('<div class="row line">'+inputPuestoId+inputPuestoId1+InputCalificacion+InputPriority+deleteRow+'</div>');
        /*limpiamos el id*/
        $('#puesto_id option:first').prop("selected", true);
        $('#puesto_id').trigger('change');
        $('#calificacion').val('0');
        $('#prioridad').val(default_priority);
    });
        /*eliminar renglón */
    $("#cursos_div").on('click', '#borrar',function(e) {
        e.preventDefault();
        console.log(puesto_id, calificacion, priority);
        // $(this).closest('.line').remove();
    });
</script>
@endsection