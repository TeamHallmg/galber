@extends('layouts.app')

@section('title', 'Administración de Cursos')

@section('content')

<div class="row my-5">
	<div class="col">
		<div class="flash-message" id="mensaje">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
				@if(Session::has($msg))
					<p class="alert alert-{{ $msg }}">{{ Session::get($msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
				@endif
			@endforeach
		</div>
		<div class="card card-3">
			<div class="card-header bg-blue">
				<h1 class="my-auto font-weight-bold text-center text-white">Escalafón | Administración de Cursos</h1>
			</div>
			<div class="card-body">
				<div class="row mb-5 justify-content-center">
					<div class="col col-md-4 my-3 text-center">
						<a href="{{url('escalafon/cursos/create')}}" class="btn btn-success btn-block mx-1 deleteBtn card-1">
							<i class="fas fa-plus-circle"></i> Crear Nuevo Curso
						</a>
					</div>
				</div>
				{{-- <div class="row loading">
					<div class="col-md-12 text-center">
						<div class="spinner-border text-blue" style="width: 6rem; height: 6rem;" role="status">
							<span class="sr-only">Loading...</span>
						</div>
						<h3 class="text-blue mt-2"><strong>Cargando...</strong></h3>
					</div>
				</div> --}}
				<div class="row">
					<div class="col-md-12">
						<table id="courses" class="table table-striped table-bordered w-100 nowrap">
							<thead class="bg-blue text-white">
								<tr>
									<th class="bg-lightblue pr-5">ID</th>
									<th class="bg-lightblue">Código</th>
									<th class="bg-lightblue">Nombre</th>
									<th class="pr-5">Curso</th>
									<th class="pr-5">Tipo</th>
									<th>Categoría</th>
									<th>Puestos</th>
									<th class="bg-lightblue">Opciones</th>
								</tr>
								<tr class="bg-lightgray" id="thead_filters">
                                    <th class="bg-lightgray">
                                        <input type="text" class="form-control text-center" placeholder="ID..." data-index="0">
                                    </th>
                                    <th class="bg-lightgray">
                                        <input type="text" class="form-control text-center" placeholder="Código..." data-index="1">
                                    </th>
									<th class="bg-lightgray">
                                        <input type="text" class="form-control text-center" placeholder="Nombre..." data-index="2">
                                    </th>
									<th>
                                        <input type="text" class="form-control text-center" placeholder="Curso..." data-index="3">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control text-center" placeholder="Tipo..." data-index="4">
									</th>
									<th>
                                        <input type="text" class="form-control text-center" placeholder="Categoría..." data-index="5">
									</th>
									<th>
                                        <input type="text" class="form-control text-center" placeholder="Puestos..." data-index="6">
									</th>
									<th class="bg-lightgray">
										{{-- <input type="text" class="form-control text-center" placeholder="Opc..." data-index="7"> --}}
                                    </th>
								</tr>
							</thead>
							<tbody>
								@foreach($courses as $key => $course)
									<tr class="text-center">
										<td>
											{{ $course->id }}
										</td>
										<td>
											{{ $course->code ?? 'N/A' }}
										</td>
										<td>
											{{ $course->name }}
										</td>
										<td>
											{{ $course->getFrom() }}
										</td>
										<td>
											{{ $course->getCourseType() }}
										</td>
										<td>
											{{ $course->getCategory() }}
										</td>
										<td>
											@if($course->job_positions->count() > 0)
												<ul style="list-style-type: none;">
													@foreach($course->job_positions as $job_position)
														<li>{{ $job_position->name }}</li>
													@endforeach
												</ul>
											@else
												N/A
											@endif
										</td>
										<td>
											<div class="form-row justify-content-center">
												<a data-toggle="tooltip" title="Calificación" href="{{ url('escalafon/cursos/'.$course->id.'/puestos') }}" class="btn btn-secondary">
													<i class="fas fa-briefcase"></i>
												</a>
												<a data-toggle="tooltip" title="Editar" href="{{url('escalafon/cursos/'.$course->id.'/edit')}}" class="btn btn-dark mx-1 editBtn card-1">
													<i class="fas fa-pencil-alt"></i>
												</a>
												<button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#modelId" data-id="{{ $course->id }}">
													<i class="fas fa-trash"></i>
												</button>
											</div>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Eliminar -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form class="form" action="#" method="POST" id="form_delete">
				<input name="_method" type="hidden" value="DELETE">
				{{ csrf_field() }}
				<div class="modal-header bg-blue text-white">
					<h5 class="modal-title font-weight-bold">Eliminar Curso</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
				</div>
				<div class="modal-body">
					<h2 class="text-center"><span class="text-warning"><i class="fas fa-exclamation-triangle"></i></span> ¡Advertencia!</h2><br>
					<h4 class="text-center" id="message">
						No se recomienda realizar esta acción.
					</h4>
					<div class="form-group">
						<label id="cnt_motive" for="del_reason">Motivo (0/200)</label>
						<textarea name="del_reason" class="form-control noresize" maxlength="200" id="del_reason" onkeyup="countMotive(this.value)" rows="4" required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-lightblue confirm-delete">Eliminar de todos modos</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	var courses = $('#courses').DataTable({
		"bSortCellsTop": true,
		"order": [[ 0, "asc" ]],
		"paging": true,
		"pagingType": "numbers",
		"scrollX": true,
		"fixedColumns":{
			"leftColumns": 3,
			"rightColumns": 1
		},
		"language": {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     ">",
				"sPrevious": "<"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
	});

	$( courses.table().container() ).on( 'keyup', 'thead input', function () {
        courses
            .column( $(this).data('index') )
            .search( this.value )
            .draw();
    } );

	$(function() {
		redrawTableWrapper();
	});


	function redrawTableWrapper() {
		setTimeout(() => {
			$('.loading').addClass('d-none');
			$('.tableWrapper').removeClass('d-none');
			$($.fn.dataTable.tables(true)).DataTable()
			.columns.adjust()
			.fixedColumns().relayout();
		}, 1000);
	}

	function countMotive(str) {
		var lng = str.length;
		$("#cnt_motive").html('Motivo (' + lng + '/200)');
	}

	$("#modelId").on('show.bs.modal', function (e) {
		var triggerLink = $(e.relatedTarget);
		var id = triggerLink.data("id");
		var action = '{{ url('escalafon/cursos') }}' + '/' + id;
		$("#form_delete").attr('action', action);
	});

	$("#modelId").on('hide.bs.modal', function (e) {
		$("#cnt_motive").html('Motivo (0/200)');
		$('#del_reason').val('');
	});

</script>
@endsection