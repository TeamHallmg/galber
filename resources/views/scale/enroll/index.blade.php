@extends('layouts.app')

@section('title', 'Administración de Cursos')

@section('content')

<div class="row my-5">
	<div class="col">
		<div class="flash-message" id="mensaje">
			@foreach (['danger', 'warning', 'success', 'info', 'important'] as $msg)
				@if(Session::has($msg))
					<p class="alert alert-{{ $msg }}">{{ Session::get($msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
				@endif
			@endforeach
		</div>
		<div class="card card-3">
			<div class="card-header bg-blue">
				<h2 class="my-auto font-weight-bold text-center text-white">Escalafón | Matriculación Masiva</h2>
			</div>
			<div class="card-body">
                <div class="row loading">
					<div class="col-md-12 text-center">
						<div class="spinner-border text-blue" style="width: 6rem; height: 6rem;" role="status">
							<span class="sr-only">Loading...</span>
						</div>
						<h3 class="text-blue mt-2"><strong>Cargando...</strong></h3>
					</div>
				</div>
				<div class="row tableWrapper d-none">
                    <div class="col-12 mt-1 mb-3">
                        <h5 class="text-white font-weight-bold m-0 rounded-pill bg-secondary px-3 py-1">
                            1. Selecciona curso para la matriculación
                        </h5>
                    </div>
                    <div class="col-md-12">
                        {{-- <h3 class="text-blue font-italic font-weight-bold title-border">
                            1. Selecciona curso para la matriculación
                        </h3> --}}
                        <div class="form-row">
                            <div class="form-group col-md-3 col-sm-12">
                                <label for="use_plan" class="col-form-label font-weight-bold">¿Matricular de acuerdo a plan de capacitación?</label>
                                <select id="use_plan" class="selectpicker form-control" data-live-search="true">
                                    <option value="1">
                                        Si
                                    </option>
                                    <option value="0" selected>
                                        No
                                    </option>
                                </select>
                            </div>
                            <div class="form-group col-md-3 col-sm-12 d-plan">
                                <label for="plan" class="col-form-label font-weight-bold">Plan</label>
                                <select id="plan" class="selectpicker form-control" data-live-search="true" title="Selecciona una opción...">
                                    @foreach($plans as $key => $row)
                                        <option value="{{$row->id}}">
                                            {{ $row->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-3 col-sm-12 d-plan">
                                <label for="course_plan" class="col-form-label font-weight-bold requerido">Curso</label>
                                <select id="course_plan" class="selectpicker form-control" data-live-search="true" title="Selecciona una opción..." required>
                                </select>
                            </div>
                            <div class="form-group col-md-3 col-sm-12 d-no-plan">
                                <label for="course" class="col-form-label font-weight-bold requerido">Curso</label>
                                <select id="course" class="selectpicker form-control" data-live-search="true" title="Selecciona una opción..." data-size="10" required>
                                    <optgroup label="Internos">
                                        @foreach($courses['moodle'] as $key => $course)
                                            <option value="{{$course->id}}" {{(old('moodle_course') == $course->id?'selected':'')}}>
                                                {{-- data-subtext="({{ $course->getFrom() }})" --}}
                                                {{$course->name}}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                    <optgroup label="Externos">
                                        @foreach($courses['extern'] as $key => $course)
                                            <option value="{{$course->id}}" {{(old('moodle_course') == $course->id?'selected':'')}}>
                                                {{-- data-subtext="({{ $course->getFrom() }})" --}}
                                                {{$course->name}}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                            <div class="form-group col-md-3 col-sm-12">
                                <label for="price" class="col-form-label font-weight-bold">Costo por persona:</label>
                                <input type="text" id="price" placeholder="Costo..." class="form-control currency">
                            </div>
                            <div class="form-group col-md-3 col-sm-12">
                                <label for="program" class="col-form-label font-weight-bold requerido">Programa de Capacitación</label>
                                <select id="program" class="selectpicker form-control" data-live-search="true" title="Selecciona una opción..." required>
                                    @foreach($programs as $key => $row)
                                        <option value="{{$row->id}}">
                                            {{ $row->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mt-1 mb-3">
                        <hr class="scale-hr">
                    </div>
                    <div class="col-12 mb-3">
                        <h5 class="text-white font-weight-bold m-0 rounded-pill bg-secondary px-3 py-1">
                            2. Selecciona usuarios a matricular
                        </h5>
                    </div>
					<div class="col-md-12">
                        {{-- <h3 class="text-blue font-italic font-weight-bold mb-3 title-border">
                            2. Selecciona usuarios a matricular
                        </h3> --}}
						<table id="employees" class="table table-striped table-bordered table-hover w-100 display">
							<thead class="bg-blue text-white">
								<tr>
                                    <th>
                                        <input type="checkbox" id="select_all">
                                    </th>
                                    <th class="bg-lightblue">No. de Empleado</th>
                                    <th class="bg-lightblue">Nombre</th>
                                    <th class="d-none">ID</th>
									<th>Departamento</th>
									<th>Puesto</th>
                                    <th>Sucursal</th>
								</tr>
								<tr class="bg-lightgray" id="thead_filters">
                                    <th></th>
                                    <th>
                                        <input type="text" class="form-control text-center" placeholder="No. de Empleado...">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control text-center" placeholder="Nombre...">
                                    </th>
                                    <th class="d-none"></th>
									<th>
                                        <input type="text" class="form-control text-center" placeholder="Departamento...">
                                    </th>
									<th>
                                        <input type="text" class="form-control text-center" placeholder="Puesto...">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control text-center" placeholder="Sucursal...">
                                    </th>
								</tr>
                            </thead>
							<tbody>
								@foreach($employees as $key => $employee)
                                    <tr id="user{{$employee->user->id}}">
                                        <td class="text-center"></td>
                                        <td style="vertical-align:middle" nowrap class="text-center">
											{{ $employee->idempleado }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $employee->getFullNameAttribute() }}
                                        </td>
                                        <td class="d-none"> {{ $employee->id }} </td>
                                        <td style="vertical-align:middle" nowrap class="text-center">
											{{ $employee->getDepartmentName(true) }}
                                        </td>
                                        <td style="vertical-align:middle" nowrap class="text-center">
											{{ $employee->getPuestoName(true) }}
                                        </td>
                                        <td style="vertical-align:middle" nowrap class="text-center">
											{{ $employee->getSucursal() }}
                                        </td>
									</tr>
								@endforeach
							</tbody>
						</table>
                    </div>
                    <div class="col-md-12 mt-2 mb-2">
                        <hr class="scale-hr">
                    </div>
                    <div class="col-12 mt-1 mb-3">
                        <h5 class="text-white font-weight-bold m-0 rounded-pill bg-secondary px-3 py-1">
                            3. Visualiza resumen de matriculación
                        </h5>
                    </div>
                    <div class="col-md-12 text-left">
                        {{-- <h3 class="text-blue font-italic font-weight-bold my-3 title-border text-left">
                            3. Visualiza resumen de matriculación
                        </h3> --}}
                        {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalPreview">
                            Watch me nene
                        </button> --}}
                        <form method="POST" action="{{ url('escalafon/matriculacion/resumen') }}">
                            {!! method_field('POST') !!}
                            {!! csrf_field() !!}
                            <input type="hidden" name="use_plan">
                            <input type="hidden" name="plan">
                            <input type="hidden" name="course">
                            <input type="hidden" name="employees">
                            <input type="hidden" name="price">
                            <input type="hidden" name="program">
                            <button type="submit" class="btn btn-success preview" disabled="disabled">
                                Continuar al resumen de matriculación
                                <i class="fas fa-tasks"></i>
                            </button>
                        </form>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    var sel_employees = [];
    var resume = null;

    $('#select_all').click(function() {
        var rows_filtered = employees.rows({ search: 'applied' });
        if($(this).is(':checked')) {
            rows_filtered.select();
        } else {
            rows_filtered.deselect();
        }
    });

    $('#employees thead tr:eq(1) th').each( function (i) {
        $( 'input[type="text"]', this ).on( 'keyup change', function () {
            if ( employees.column(i).search() !== this.value ) {
                employees
                    .column(i)
                    .search( this.value )
                    .draw();
            }
            validateSelectAllCheck();
        } );
    } );

    function validateSelectAllCheck() {
        var rows_filtered = employees.rows({ search: 'applied' });
        var selected = employees.rows('.selected', { search: 'applied' }).count(); // Total de datos seleccionados
        // console.log(rows_filtered.count(), selected);
        if(rows_filtered.count() == selected) {
            $('#select_all').prop('checked', true);
        } else {
            $('#select_all').prop('checked', false);
        }

        var data = employees.rows('.selected').data().toArray();

        sel_employees = [];
        $.each(data, function(index, value) {
            sel_employees.push(data[index][3]);
        });
        // checkSubmitButton(sel_employees);
    }

    $('.d-plan').hide();
    $('#use_plan').change(function() {
        var qwerty = $(this).val();
        if(qwerty == 1) {
            $('.d-plan').show();
            $('.d-no-plan').hide();
            $('#course_plan').prop('required', true);
            $('#course').prop('required', false);
        } else if(qwerty == 0) {
            $('.d-plan').hide();
            $('.d-no-plan').show();
            $('#course_plan').prop('required', false);
            $('#course').prop('required', true);
        }
        $('#course', '#course_plan').selectpicker('refresh');
        checkSubmitButton(sel_employees);
    });

    $('#plan').change(function(){
        var plan_id = $(this).val();
        $('#course_plan').empty();
        $('#course_plan').selectpicker('refresh');
        getMoodleCoursesByPlan(plan_id);
    });

    $('#course_plan').change(function() {
        checkSubmitButton(sel_employees);
        // checkUserEnrollements($(this).val());
    });
    $('#course').change(function() {
        checkSubmitButton(sel_employees);
        // checkUserEnrollements($(this).val());
    });
    $('#program').change(function() {
        checkSubmitButton(sel_employees);
    });

    function getMoodleCoursesByPlan(plan_id) {
        $.ajax({
            url: "{{url('escalafon/get_moodle_courses_by_plan')}}/"+plan_id,
            Type:'GET',
            success: function(result){
                $.each(result, function(i, item) {
                    if(item.type == 'I') {
                        additional_text = ' <span class="text-muted small">(Interno)</span>';
                    } else {
                        additional_text = ' <span class="text-muted small">(Externo)</span>';
                    }
                    $('#course_plan').append($('<option>', { 
                        value: item.id,
                        'data-content': item.name + additional_text
                    })).selectpicker('refresh');
                });
            }
        });
    }

	var employees = $('#employees').DataTable({
        "bSortCellsTop": true,
		"order": [[ 1, "asc" ]],
		"paging": true,
		"pagingType": "numbers",
		"scrollX": true,
		// "fixedColumns":{
		// 	"leftColumns": 3,
		// 	"rightColumns": 1,
		// },
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     ">",
				"sPrevious": "<"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		},
        columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            style: 'background: white',
            targets:   0
        } ],
        select: {
            style:    'multi',
            selector: 'td:first-child'
        }
	});

    employees.on( 'select', function ( e, dt, type, indexes ) {
        var rowData = employees.rows( indexes ).data().toArray();
        sel_employees.push(rowData[0][3]);
        // console.log(sel_employees);
        validateSelectAllCheck();
        checkSubmitButton(sel_employees);
    }).on( 'deselect', function ( e, dt, type, indexes ) {
        var rowData = employees.rows( indexes ).data().toArray();
        sel_employees.remove(rowData[0][3]);
        // console.log(sel_employees);
        validateSelectAllCheck();
        checkSubmitButton(sel_employees);
    });

    function checkSubmitButton(sel_employees) {
        // console.log(sel_employees);
        $("input[name=employees]").val(sel_employees);
        $("input[name=price]").val($('#price').val());
        bool = isCourseSelected();
        bool2 = isProgramSelected();
        // console.log(bool, bool2);
        if(!bool || !bool2) {
            $('.preview').attr('disabled', true);
        } else {
            if(sel_employees.length === 0) {
                $('.preview').attr('disabled', true);
            } else {
                setCourseAndPlanValues();
                $('.preview').attr('disabled', false);
            }
        }
    }

    function isProgramSelected() {
        if($('#program').val()) {
            return true;
        }
        return false;
    }

    function isCourseSelected() {
        use_plan = $('#use_plan').val();
        if(use_plan == 1) {
            if($('#course_plan').val()) {
                return true;
            }
        } else {
            if($('#course').val()) {
                return true;
            }
        }
        return false;
    }

    function setCourseAndPlanValues() {
        use_plan = $('#use_plan').val();
        if(use_plan == 1) {
            $("input[name=plan]").val($("#plan").val());
            $("input[name=course]").val($("#course_plan").val());
        } else {
            $("input[name=plan]").val(null);
            $("input[name=course]").val($("#course").val());
        }
        $("input[name=use_plan]").val($("#use_plan").val());
        $("input[name=program]").val($("#program").val());
    }

    Array.prototype.remove = function() {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };

    function formatNumber(n) {
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function formatCurrency(input, blur) {
        var input_val = input.val();
        if (input_val === "") { return; }
        var original_len = input_val.length;
        var caret_pos = input.prop("selectionStart");
        
        if (input_val.indexOf(".") >= 0) {
            var decimal_pos = input_val.indexOf(".");
            var left_side = input_val.substring(0, decimal_pos);
            var right_side = input_val.substring(decimal_pos);

            left_side = formatNumber(left_side);
            right_side = formatNumber(right_side);

            if (blur === "blur") {
                right_side += "00";
            }
            right_side = right_side.substring(0, 2);
            input_val = "$ " + left_side + "." + right_side;

        } else {
            input_val = formatNumber(input_val);
            input_val = "$ " + input_val;
    
            if (blur === "blur") {
                input_val += ".00";
            }
        }
        input.val(input_val);
        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
    }

    $(".currency").each(function( index ) {
        formatCurrency($(this));
    });

    $(document).on('keyup', '.currency', function(){  
        formatCurrency($(this));
    });

    $(function() {
		redrawTable();
	});

	function redrawTable() {
		setTimeout(() => {
			$('.loading').addClass('d-none');
			$('.tableWrapper').removeClass('d-none');
			$($.fn.dataTable.tables(true)).DataTable()
			.columns.adjust()
			.fixedColumns()
			.relayout();
		}, 1000);
	}
</script>
@endsection