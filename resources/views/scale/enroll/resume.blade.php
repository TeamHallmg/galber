@extends('layouts.app')

@section('title', 'Administración de Cursos')

@section('content')

<div class="row my-5">
	<div class="col">
		<div class="flash-message" id="mensaje">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
				@if(Session::has($msg))
					<p class="alert alert-{{ $msg }}">{{ Session::get($msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
				@endif
			@endforeach
		</div>
		<div class="card card-3">
			<div class="card-header bg-blue">
				<h2 class="my-auto font-weight-bold text-center text-white">Escalafón | Matriculación Masiva</h2>
			</div>
			<div class="card-body">
                <div class="row loading">
					<div class="col-md-12 text-center">
						<div class="spinner-border text-blue" style="width: 6rem; height: 6rem;" role="status">
							<span class="sr-only">Loading...</span>
						</div>
						<h3 class="text-blue mt-2"><strong>Cargando...</strong></h3>
					</div>
				</div>
				<div class="row tableWrapper d-none">
					<div class="col-12 mt-1 mb-3">
                        <h5 class="text-white font-weight-bold m-0 rounded-pill bg-secondary px-3 py-1">
                            4. Confirma datos de matriculación
                        </h5>
                    </div>
                    <div class="col-md-12 mt-3">
                        {{-- <h3 class="text-blue font-italic font-weight-bold mb-3 title-border">
                            4. Confirma datos de matriculación
                        </h3> --}}
                        <div class="row justify-content-between mb-5">
                            <div class="col-md-3 col-sm-12">
                                <h6 class="font-weight-bold text-blue">
                                    ¿Matricular de acuerdo a plan de capacitación?
								</h6>
								<p class="m-0 rounded px-3 py-2 bg-lightgray font-weight-bold">
                                    @if($data['use_plan'])
                                        Si
                                    @else
                                        No
                                    @endif
                                </p>
							</div>
							@if($data['use_plan'])
								<div class="col-md-3 col-sm-12">
									<h6 class="font-weight-bold text-blue">
										Plan
									</h6>
									<p class="m-0 rounded px-3 py-2 bg-lightgray font-weight-bold">
										@if($data['use_plan'])
											{{ $data['plan_name'] }}
										@else
											-
										@endif
									</p>
								</div>
							@endif
                            <div class="col-md-3 col-sm-12">
                                <h6 class="font-weight-bold text-blue">
                                    Curso
                                </h6>
                                <p class="m-0 rounded px-3 py-2 bg-lightgray font-weight-bold">
                                    {{ $data['course_name'] }}
								</p>
							</div>
							<div class="col-md-3 col-sm-12">
                                <h6 class="font-weight-bold text-blue">
                                    Precio por persona
                                </h6>
                                <p class="m-0 rounded px-3 py-2 bg-lightgray font-weight-bold">
                                    {{ $data['formated_price']  ?? '-' }}
								</p>
							</div>
							<div class="col-md-3 col-sm-12">
                                <h6 class="font-weight-bold text-blue">
                                    Programa de Capacitación
                                </h6>
                                <p class="m-0 rounded px-3 py-2 bg-lightgray font-weight-bold">
                                    {{ $data['program_name']  ?? '-' }}
								</p>
                            </div>
                        </div>
						<table id="employees" class="table table-striped table-bordered table-hover w-100 display">
							<thead class="bg-blue text-white">
								<tr>
                                    <th class="bg-lightblue">No. de Empleado</th>
                                    <th class="bg-lightblue">Nombre</th>
									<th>Departamento</th>
									<th>Puesto</th>
                                    <th>Sucursal</th>
                                    <th class="bg-lightblue">Estatus</th>
								</tr>
                            </thead>
							<tbody>
								@foreach($employees as $key => $employee)
									<tr id="user{{$employee->user->id}}">
										{{-- class="{{$employee->enrolled?'bg-warning':''}}" --}}
                                        <td style="vertical-align:middle" nowrap class="text-center">
											{{ $employee->idempleado }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $employee->getFullNameAttribute() }}
                                        </td>
                                        <td style="vertical-align:middle" nowrap class="text-center">
											{{ $employee->getDepartmentName(true) }}
                                        </td>
                                        <td style="vertical-align:middle" nowrap class="text-center">
											{{ $employee->getPuestoName(true) }}
                                        </td>
                                        <td style="vertical-align:middle" nowrap class="text-center">
											{{ $employee->getSucursal() }}
                                        </td>
                                        <td style="vertical-align:middle" nowrap class="text-center">
											@if($employee->enrolled)
												<p class="font-weight-bold m-0 rounded-pill px-3 py-1 bg-transparent border border-danger text-danger">
													Ya matriculado
												</p>
                                                {{-- <span class="text-danger">Ya matriculado</span> --}}
                                            @else
												<p class="font-weight-bold m-0 rounded-pill px-3 py-1 bg-transparent border border-success text-success">
													Puede matricularse
												</p>
                                            @endif
                                        </td>
									</tr>
								@endforeach
							</tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-12 text-right p-3">
							</div>
							<div class="col-12 mt-1 mb-3">
								<p class="font-weight-bold m-0 rounded bg-lightgray px-3 py-1 text-center">
									<span class="text-danger mr-2">*</span>Las personas ya matriculadas serán excluidas y no afectarán el proceso de matriculación
								</p>
							</div>
                            <div class="col-md-12 text-right">
                                <form method="POST" action="{{ url('escalafon/matriculacion') }}">
                                    {!! method_field('POST') !!}
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="use_plan" value="{{$data['use_plan']}}">
                                    <input type="hidden" name="plan_id" value="{{$data['plan']}}">
                                    <input type="hidden" name="course_id" value="{{$data['course']}}">
									<input type="hidden" name="employees_id" value="{{$data['employees']}}">
									<input type="hidden" name="price" value="{{$data['price']}}">
									<input type="hidden" name="program_id" value="{{$data['program']}}">
                                    <a href="{{ url('escalafon/matriculacion') }}" class="btn btn-primary">
                                        Cancelar
                                    </a>
                                    <button type="submit" class="btn btn-success">
                                        <i class="fas fa-check-circle"></i>
                                        Confirmar matriculación
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
	var employees = $('#employees').DataTable({
		"order": [[ 0, "asc" ]],
		"paging": true,
		"pagingType": "numbers",
		"scrollX": true,
		"fixedColumns":{
			"leftColumns": 2,
			"rightColumns": 1,
		},
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     ">",
				"sPrevious": "<"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
	});

    $(function() {
		redrawTable();
	});

	function redrawTable() {
		setTimeout(() => {
			$('.loading').addClass('d-none');
			$('.tableWrapper').removeClass('d-none');
			$($.fn.dataTable.tables(true)).DataTable()
			.columns.adjust()
			.fixedColumns()
			.relayout();
		}, 1000);
	}
</script>
@endsection