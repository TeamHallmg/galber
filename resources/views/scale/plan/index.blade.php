@extends('layouts.app')

@section('title', 'Administración de Planes')

@section('content')

<div class="row my-5">
	<div class="col">
		<div class="card card-3">
			<div class="card-header bg-blue text-white">
				<h1 class="my-auto font-weight-bold text-center">Escalafón | Administración de Programas</h1>
			</div>
			<div class="card-body">
				<div class="row mb-5 justify-content-center">
					<div class="col col-md-4 my-3 text-center">
						<a href="{{url('escalafon/planes/create')}}" class="btn btn-success btn-block mx-1 deleteBtn card-1">
							<i class="fas fa-plus-circle"></i> Crear Programa
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 tableWrapper">
						<table id="plans" class="table table-striped table-bordered w-100">
							<thead class="bg-blue text-white">
								<tr>
									<th class="bg-lightblue">ID</th>
									<th class="bg-lightblue">Nombre</th>
									<th>Tipo</th>
									<th>Descripción</th>
									<th>Estatus</th>
									<th>Fecha Inicio</th>
									<th>Fecha Fin</th>
									<th>Presupuesto</th>
									<th>Cursos</th>
									<th class="bg-lightblue">Opciones</th>
								</tr>
							</thead>
							<tbody nowrap class="text-center">
								@foreach($plans as $key => $plan)
									<tr>
										<td>
											{{$plan->id}}
										</td>
										<td>
											{{$plan->name}}
										</td>
										<td>
											{{$plan->getType()}}
										</td>
										<td>
											{{$plan->description ?? 'N/A'}}
										</td>
										<td>
											{{$plan->getStatus()}}
										</td>
										<td>
											{{ $plan->getFormattedDate('start_date') }}
										</td>
										<td>
											{{ $plan->getFormattedDate('end_date') }}
										</td>
										<td>
											{{ $plan->getFormattedBudget() }}
										</td>
										<td>
											@if($plan->courses->count() > 0)
												<ul style="list-style-type: none;">
													@foreach($plan->courses as $course)
														<li>{{ $course->name }}</li>
													@endforeach
												</ul>
											@else
												N/A
											@endif
										</td>
										<td>
											<div class="form-row justify-content-center">
												<a href="{{url('escalafon/planes/'.$plan->id.'/edit')}}" class="btn btn-dark mx-1 editBtn card-1">
													<i class="fas fa-pencil-alt"></i>
												</a>
												<button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#modelId" data-id="{{ $plan->id }}">
													<i class="fas fa-trash"></i>
												</button>
											</div>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Eliminar -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form class="form" action="#" method="POST" id="form_delete">
				<input name="_method" type="hidden" value="DELETE">
				{{ csrf_field() }}
				<div class="modal-header bg-blue text-white">
					<h5 class="modal-title font-weight-bold">Eliminar Plan</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
				</div>
				<div class="modal-body">
					<h2 class="text-center"><span class="text-warning"><i class="fas fa-exclamation-triangle"></i></span> ¡Advertencia!</h2><br>
					<h4 class="text-center" id="message">
						No se recomienda realizar esta acción.
					</h4>
					<div class="form-group">
						<label id="cnt_motive" for="del_reason">Motivo (0/200)</label>
						<textarea name="del_reason" class="form-control noresize" maxlength="200" id="del_reason" onkeyup="countMotive(this.value)" rows="4" required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-lightblue confirm-delete">Eliminar de todos modos</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">

	function countMotive(str) {
		var lng = str.length;
		$("#cnt_motive").html('Motivo (' + lng + '/200)');
	}
		
	$('#plans').DataTable({
		"order": [[ 0, "asc" ]],
		"paging": true,
		"pagingType": "numbers",
		//"scrollX":true,
		//"scrollCollapse": true,
		"scrollX": true,
		"fixedColumns":{
			"leftColumns": 1,
			"rightColumns": 1,
		},
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     ">",
				"sPrevious": "<"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
	});

	$("#modelId").on('show.bs.modal', function (e) {
		var triggerLink = $(e.relatedTarget);
		var id = triggerLink.data("id");
		var action = '{{ url('escalafon/planes') }}' + '/' + id;
		$("#form_delete").attr('action', action);
	});

	$("#modelId").on('hide.bs.modal', function (e) {
		$("#cnt_motive").html('Motivo (0/200)');
		$('#del_reason').val('');
	});

</script>
@endsection