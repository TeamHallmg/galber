@extends('layouts.app')

@section('title', 'Administración de Planes')

@section('content')
<div class="container">
<div class="row my-5">
    <div class="col">
        <div class="card card-3">
            <div class="card-header bg-blue text-white">
                <h2 class="my-auto font-weight-bold text-center">Escalafón | Editar Programa</h2>
            </div>
            @if($errors->any())
            <div class="container mt-4">
                <div class="alert alert-danger alert-dismissible fade show card-1" role="alert">
                    <strong>{{$errors->first()}}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            @endif
            <div class="card-body">
                <form method="POST" action="{{ url('escalafon/planes/'.$plan->id) }}" id="edit">
                    {!! method_field('PUT') !!}
                    {!! csrf_field() !!}	
                    <div class="row">
                        <div class="form-group col-md-3 col-sm-12">
                            <label for="type" class="col-form-label font-weight-bold">Tipo</label>
                            <select id="type" name="type" class="selectpicker form-control" data-live-search="true" required>
                                <option value="program" {{(old('type', $plan->type) == 'program'?'selected':'')}}>
                                    Programa de Capacitación
                                </option>
                                <option value="training" {{(old('type', $plan->type) == 'training'?'selected':'')}}>
                                    Plan de Capacitación
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-3 col-sm-12">
                            <label for="status" class="col-form-label font-weight-bold">Tipo</label>
                            <select id="status" name="status" class="selectpicker form-control" data-live-search="true" required>
                                <option value="open" {{(old('status', $plan->status) == 'open'?'selected':'')}}>
                                    Abierto
                                </option>
                                <option value="closed" {{(old('status', $plan->status) == 'closed'?'selected':'')}}>
                                    Cerrado
                                </option>
                                <option value="canceled" {{(old('status', $plan->status) == 'canceled'?'selected':'')}}>
                                    Cancelado
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-3 col-sm-12">
                            <label for="start_date" class="col-form-label font-weight-bold">Fecha Inicio</label>
                            <input type="date" class="form-control" name="start_date"  value="{{old('start_date', $plan->start_date)}}" placeholder="Fecha Inicio...">
                        </div>
                        <div class="form-group col-md-3 col-sm-12">
                            <label for="end_date" class="col-form-label font-weight-bold">Fecha Fin</label>
                            <input type="date" class="form-control" name="end_date"  value="{{old('end_date', $plan->end_date)}}" placeholder="Fecha Fin...">
                        </div>
                        <div class="form-group col-md-3 col-sm-12">
                            <label for="name" class="col-form-label font-weight-bold requerido">Nombre</label>
                            <input type="text" class="form-control" name="name"  value="{{old('name', $plan->name)}}" placeholder="Nombre..." required>
                        </div>
                        <div class="form-group col-md-3 col-sm-12">
                            <label for="budget" class="col-form-label font-weight-bold">Presupuesto</label>
                            <input type="text" id="budget" name="budget" class="form-control currency" value="{{old('budget', $plan->budget)}}">
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label for="courses" class="col-form-label font-weight-bold">Cursos</label>
                            <select id="courses" name="courses[]" class="selectpicker form-control" data-live-search="true" title="Selecciona uno o varios cursos..." multiple>
                                @foreach($courses as $key => $course)
                                    <option value="{{$course->id}}" {{ in_array($course->id, $plan->getCoursesIDs())?'selected':'' }}>
                                        {{$course->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12 col-sm-12">
                            <label for="description" class="col-form-label font-weight-bold">Descripción</label>
                            <textarea class="form-control" name="description">{{old('description', $plan->description)}}</textarea>
                        </div>

                        <div class="form-group col-12 col-md-12 mt-5">
                            <a href="{{url('escalafon/planes')}}" class="btn btn-primary mb-md-0 card-1"><i class="fas fa-arrow-alt-circle-left"></i> Volver</a>
                            <button type="submit" class="btn btn-success card-1">Guardar <i class="fas fa-save"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
</div>
@endsection
@section('scripts')
<script>
    function formatNumber(n) {
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function formatCurrency(input, blur) {
        var input_val = input.val();
        if (input_val === "") { return; }
        var original_len = input_val.length;
        var caret_pos = input.prop("selectionStart");
        
        if (input_val.indexOf(".") >= 0) {
            var decimal_pos = input_val.indexOf(".");
            var left_side = input_val.substring(0, decimal_pos);
            var right_side = input_val.substring(decimal_pos);

            left_side = formatNumber(left_side);
            right_side = formatNumber(right_side);

            if (blur === "blur") {
                right_side += "00";
            }
            right_side = right_side.substring(0, 2);
            input_val = "$ " + left_side + "." + right_side;

        } else {
            input_val = formatNumber(input_val);
            input_val = "$ " + input_val;
    
            if (blur === "blur") {
                input_val += ".00";
            }
        }
        input.val(input_val);
        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
    }

    $(".currency").each(function( index ) {
        formatCurrency($(this));
    });

    $(document).on('keyup', '.currency', function(){  
        formatCurrency($(this));
    });
</script>
@endsection