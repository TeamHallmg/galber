@extends('layouts.app')

@section('title', 'Escalafón')

@section('content')

<div class="row my-5">
	<div class="col">
		<div class="flash-message" id="mensaje">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
				@if(Session::has($msg))
					<p class="alert alert-{{ $msg }}">{{ Session::get($msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
				@endif
			@endforeach
		</div>
		<div class="card card-3">
			<div class="card-header">
				<h2 class="my-auto font-weight-bold text-center">Escalafón | Administración Reportes</h2>
			</div>
			<div class="card-body">
				<div class="row loading">
					<div class="col-md-12 text-center">
						<div class="spinner-border text-blue" style="width: 6rem; height: 6rem;" role="status">
							<span class="sr-only">Loading...</span>
						</div>
						<h3 class="text-blue mt-2"><strong>Cargando...</strong></h3>
					</div>
				</div>
				<div class="row tableWrapper d-none">
					<div class="col-md-4 col-xs-12">
						<h1 class="report-big-number text-primary text-center">{{ $courses['visible']->count() + $courses['nonvisible']->count() }}</h1>
						<h4 class="text-primary text-center">Total de Cursos</h4>
					</div>
					<div class="col-md-4 col-xs-6">
						<h1 class="report-big-number text-info text-center">{{ $courses['visible']->count() }}</h1>
						<h4 class="text-info text-center">Visibles</h4>
					</div>
					<div class="col-md-4 col-xs-6">
						<h1 class="report-big-number text-secondary text-center">{{ $courses['nonvisible']->count() }}</h1>
						<h4 class="text-secondary text-center">Ocultos</h4>
					</div>
					<div class="col my-2">
						<hr>
					</div>
					<div class="col-md-12">
						<table id="r1" class="table table-striped table-bordered w-100 moodle-report">
							<thead style="background-color: #002C49; color:white;">
								<tr>
									<th style="background-color: #005C99">Nombre de Usuario</th>
									<th style="background-color: #005C99">Curso</th>
									<th>Categoría</th>
									<th>Nombre de Item</th>
									<th>Puntaje</th>
									<th>Máximo Puntaje Posible</th>
									<th>Porcentaje</th>
									<th>Estado</th>
								</tr>
							</thead>
							<tbody>
								@foreach($data as $key => $row)
									<tr>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->name.' '.$row->lastname }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->course }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->category }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->item_name }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->score ?? 'N/A' }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->max_grade ?? 'N/A' }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->percentage ?? 'N/A' }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->status?'Aprobado':'No Aprobado' }}
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="col my-2">
						<hr>
					</div>
					<div class="col-md-12">
						<table id="r2" class="table table-striped table-bordered w-100 moodle-report">
							<thead style="background-color: #002C49; color:white;">
								<tr>
									<th style="background-color: #005C99">Nombre de Usuario</th>
									<th style="background-color: #005C99">Curso</th>
									<th>Fecha de Matriculación</th>
									<th>Primer Acceso</th>
									<th>Estado</th>
									<th>Último Acceso</th>
									<th>Días Accedidos</th>
									<th>Actividades Completadas</th>
									<th>Actividades Asignadas</th>
									<th>Porcentaje Avance Curso</th>
									<th>Calidad</th>
									<th>Calificación Final</th>
								</tr>
							</thead>
							<tbody>
								@foreach($datav2 as $key => $row)
									<tr>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->first_name.' '.$row->last_name }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->course }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->enroll_date }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->first_access ?? 'Nunca' }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											@if($row->withdrawn)
												<span class="text-danger">Retirado</span>
											@else
												<span class="text-success">Activo</span>
											@endif
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->last_access ?? 'Nunca' }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->days_accessed }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->activities_completed }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->activities_assigned }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											@if($row->percentage_completed)
												<div class="progress">
													@if($row->percentage_completed <= 40)
														<div class="progress-bar progress-bar-striped bg-warning text-dark" role="progressbar" style="width: {{ $row->percentage_completed }}%" aria-valuenow="{{ $row->percentage_completed }}" aria-valuemin="0" aria-valuemax="100">{{ $row->percentage_completed }} %</div>
													@elseif($row->percentage_completed <= 80)
														<div class="progress-bar progress-bar-striped bg-info text-dark" role="progressbar" style="width: {{ $row->percentage_completed }}%" aria-valuenow="{{ $row->percentage_completed }}" aria-valuemin="0" aria-valuemax="100">{{ $row->percentage_completed }} %</div>
													@elseif($row->percentage_completed <= 100)
														<div class="progress-bar progress-bar-striped bg-success text-dark" role="progressbar" style="width: {{ $row->percentage_completed }}%" aria-valuenow="{{ $row->percentage_completed }}" aria-valuemin="0" aria-valuemax="100">{{ $row->percentage_completed }} %</div>
													@endif
												</div>
											@else
												N/A
											@endif
											{{-- {{ $row->percentage_completed ?? 'N/A'}} --}}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->quality }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row->final_score }}
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="col my-2">
						<hr>
					</div>
					<div class="col-md-12">
						<table id="r3" class="table table-striped table-bordered w-100 moodle-report">
							<thead style="background-color: #002C49; color:white;">
								<tr>
									<th style="background-color: #005C99">Nombre</th>
									<th style="background-color: #005C99">Apellido</th>
									<th># Cursos Encontrados</th>
									<th># Cursos Aprobados</th>
									<th># Cursos No Aprobados</th>
									<th># Cursos No Iniciados</th>
									<th># Porcentaje Aprobados</th>
								</tr>
							</thead>
							<tbody>
								@foreach($user_courses as $key => $row)
									<tr>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row['m_first_name'] }}
											{{-- .$row['user_moodle_id'] --}}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row['m_last_name'] }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ count($row['courses']) }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row['approved_courses'] }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row['not_approved_courses'] }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											{{ $row['not_initiated_courses'] }}
										</td>
										<td style="vertical-align:middle" nowrap class="text-center">
											@if($row['percentage'] == 0)
												S/P
											@else
												<div class="progress position-relative">
													<div class="progress-bar progress-bar-striped {{ $row['percentage_bg'] }} text-dark" role="progressbar" style="width: {{ $row['percentage'] }}%" aria-valuenow="{{ $row['percentage'] }}" aria-valuemin="0" aria-valuemax="100"></div>
													<span class="justify-content-center d-flex position-absolute w-100">
														<span class="badge badge-pill badge-secondary" style="font-size: 11px;">{{ $row['percentage'] }} %</span>
													</span>
												</div>
											@endif
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">

	$('.moodle-report').DataTable({
		"order": [[ 0, "asc" ]],
		"paging": true,
		"pagingType": "numbers",
		"scrollX": true,
		"fixedColumns":{
			"leftColumns": 2
		},
		"language": {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     ">",
				"sPrevious": "<"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
	});

	$(function() {
		redrawTableWrapper();
	});


	function redrawTableWrapper() {
		setTimeout(() => {
			$('.loading').addClass('d-none');
			$('.tableWrapper').removeClass('d-none');
			$($.fn.dataTable.tables(true)).DataTable()
			.columns.adjust()
			.fixedColumns().relayout();
		}, 1000);
	}

</script>
@endsection