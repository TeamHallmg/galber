@extends('layouts.app')

@section('title', 'Escalafón')

@section('content')

<div class="row my-5">
	<div class="col">
		<div class="flash-message" id="mensaje">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
				@if(Session::has($msg))
					<p class="alert alert-{{ $msg }}">{{ Session::get($msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
				@endif
			@endforeach
		</div>
		<div class="card card-3">
			<div class="card-header bg-blue">
                <h2 class="my-auto font-weight-bold text-center text-white">
                    <a class="btn rounded-circle" href="{{ url('escalafon/overview') }}"><i class="fas fa-chevron-circle-left text-white fa-lg"></i></a>
                    Escalafón | Reporte Personal
                </h2>
			</div>
			<div class="card-body">
				<div class="row justify-content-center">
					<div class="col-lg-5 col-md-12">
						<div class="card px-2 border-0 bg-lightgray h-100">
							<div class="row no-gutters h-100">
								<div class="col-md-3 d-flex justify-content-center">
									<div class="photo-container-history align-self-center" 
											style="
											background-image:url('{{  asset($user->getProfileImagen()) }}');
											background-size:cover;
											background-position:center;
											">
									</div>
								</div>
								<div class="col-md-9">
									<div class="card-body pl-1 pr-0">
										<h3 class="card-title font-weight-bold text-blue">{{ $user->getFullNameAttribute() }}</h3>
										<p class="card-text text-secondary"><span class="font-weight-bold">{{ $user->employee->getPuestoName() }}</span> | {{ $user->email }}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-7 col-md-12 align-self-center">
						<div class="row">
							<div class="col-md-6">
								<div class="card p-0 rounded-lg">
									<div class="row no-gutters bg-lightgray">
										<div class="col-md-4 d-flex justify-content-center bg-lightblue rounded-lg">
											<i class="fas fa-star text-white align-self-center fa-4x"></i>
										</div>
										<div class="col-md-6">
											<div class="card-body">
												<h2 class="card-title font-weight-bold text-blue">
													{{ $data['user']['global_average'] }}
												</h2>
												<p class="card-text text-secondary h4">Promedio</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="card p-0 rounded-lg">
									<div class="row no-gutters bg-lightgray">
										<div class="col-md-4 d-flex justify-content-center bg-blue rounded-lg">
											<i class="fas fa-chart-line text-white align-self-center fa-4x"></i>
										</div>
										<div class="col-md-6">
											<div class="card-body">
												<h2 class="card-title font-weight-bold text-blue">
													{{ $data['user']['global_completion'] }}%
												</h2>
												<p class="card-text text-secondary h4">Progreso</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr>
				{{-- <div class="row">
					<div class="col">
						<hr>
					</div>
				</div> --}}
				<div class="row loading">
					<div class="col-md-12 text-center">
						<div class="spinner-border text-blue" style="width: 6rem; height: 6rem;" role="status">
							<span class="sr-only">Loading...</span>
						</div>
						<h3 class="text-blue mt-2"><strong>Cargando...</strong></h3>
					</div>
				</div>
				<div class="row tableWrapper d-none">
					<div class="col-md-12">
						{{-- <form action="{{ url('escalafon/overview') }}" method="GET" enctype="multipart/form-data" id="filters"> --}}
							<div class="form-row mb-3 justify-content-center">
								<div class="form-group col-md-3 col-sm-12">
									<label for="reportrange" class="col-form-label font-weight-bold">Fechas</label>
									<div id="reportrange" class="form-control" style="cursor: pointer;">
										<i class="fa fa-calendar"></i>&nbsp;
										<span></span> <i class="fa fa-caret-down"></i>
									</div>
								</div>
								<div class="form-group col-md-2 text-left my-auto pt-3 pl-3">
									<button class="btn btn-primary btn-sm" type="button">
										<i class="fas fa-filter"></i>
										Filtrar
									</button>
									{{-- @if($custom_filters['department'] || $custom_filters['branch_office'])
										<a class="btn btn-danger btn-sm ml-2" href="{{ url('escalafon/overview') }}">
											<i class="fas fa-times-circle"></i>
											Remover Filtrado
										</a>
									@endif --}}
								</div>
							</div>
						{{-- </form> --}}
					</div>
				</div>
				<div class="row tableWrapper d-none">
					<div class="col-md-12">
						<table id="history" class="table table-bordered w-100">
							<thead class="bg-blue text-white">
								<tr class="bg-lightblue text-center">
									<th style="background-color: #005C99">Nombre del Curso</th>
									<th>Categoría</th>
									<th>Tipo</th>
                                    <th>Finalización</th>
                                    <th>Estatus</th>
                                    <th>Promedio</th>
								</tr>
							</thead>
							<tbody class="text-center">
								@foreach($data['courses'] as $row)
                                    <tr>
                                        <td>{{ $row['course'] }}</td>
                                        <td>{{ $row['category'] }}</td>
                                        <td>{{ $row['modality'] }}</td>
                                        <td>
											@if(count($row['items']) > 0)
												{{ $row['with_grade'].'/'.count($row['items']) }}
												<span class="text-muted">({{  number_format(($row['with_grade']/count($row['items']))*100, 2) }}%)</span>
											@else
												<span class="text-muted">-</span>
											@endif
										</td>
                                        <td>
											@if(count($row['items']) > 0)
												@if($row['completed'])
													<span class="text-success">Completado</span>
												@else
													<span class="text-info">En Progreso</span>
												@endif
											@else
												<span class="text-muted">Por Definir</span>
											@endif
                                        </td>
                                        <td>
											@if($row['total_grades'] > 0)
												{{ number_format(($row['score']/$row['max_score'])*100, 2) }}
                                            @else
                                                <span class="text-muted">-</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')

<script type="text/javascript">

    $('#history').DataTable({
		"order": [[ 0, "asc" ]],
		"paging": true,
		// "pagingType": "numbers",
		//"scrollX":true,
		//"scrollCollapse": true,
		"scrollX": true,
		"fixedColumns":{
			"leftColumns": 1
			// "rightColumns": 1,
		},
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     ">",
				"sPrevious": "<"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
	});

	$(function() {
		redrawTable();
	});

	function redrawTable() {
		setTimeout(() => {
			$('.loading').addClass('d-none');
			$('.tableWrapper').removeClass('d-none');
			$($.fn.dataTable.tables(true)).DataTable()
			.columns.adjust()
			.fixedColumns()
			.relayout();
		}, 1000);
	}

	$(function() {
		var start = moment().subtract(29, 'days');
		var end = moment();

		function cb(start, end) {
			$('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
		}

		$('#reportrange').daterangepicker({
			startDate: start,
			endDate: end,
			ranges: {
				'Hoy': [moment(), moment()],
				'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
				'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
				'Este Mes': [moment().startOf('month'), moment().endOf('month')],
				'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			locale: {
                "format": "DD/MM/YYYY",
                "separator": " a ",
                "applyLabel": "Aceptar",
                "cancelLabel": "Cancelar",
                "fromLabel": "De",
                "toLabel": "Hasta",
                "customRangeLabel": "Personalizar",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
				"monthNames": [
					"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
				]
            }
		}, cb);

		// cb(start, end);
	});
</script>
@endsection