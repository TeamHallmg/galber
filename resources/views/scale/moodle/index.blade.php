@extends('layouts.app')

@section('title', 'Escalafón')

@section('content')

<div class="row my-5">
	<div class="col">
		<div class="flash-message" id="mensaje">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
				@if(Session::has($msg))
					<p class="alert alert-{{ $msg }}">{{ Session::get($msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
				@endif
			@endforeach
		</div>
		<div class="card card-3">
			<div class="card-header bg-blue text-white">
				<h1 class="my-auto font-weight-bold text-center">Escalafón | Reporte Programa de Capacitación</h1>
			</div>
			<div class="card-body">
				<div class="row loading">
					<div class="col-md-12 text-center">
						<div class="spinner-border text-blue" style="width: 6rem; height: 6rem;" role="status">
							<span class="sr-only">Loading...</span>
						</div>
						<h3 class="text-blue mt-2"><strong>Cargando...</strong></h3>
					</div>
				</div>
				<div class="row justify-content-center tableWrapper d-none">
					<div class="col-12 mb-3">
                        <h5 class="text-white font-weight-bold m-0 rounded-pill bg-lightblue px-3 py-1">
                            Resultados
                        </h5>
                    </div>
					<div class="col-md-4 col-xs-12">
						<h1 class="report-big-number text-primary text-center">{{ $courses['visible'] + $courses['nonvisible'] }}</h1>
						<h4 class="text-primary text-center">Total de Cursos</h4>
					</div>
					<div class="col-md-4 col-xs-6">
						<h1 class="report-big-number text-info text-center">{{ $courses['visible'] }}</h1>
						<h4 class="text-info text-center">Visibles</h4>
					</div>
					<div class="col-md-4 col-xs-6">
						<h1 class="report-big-number text-secondary text-center">{{ $courses['nonvisible'] }}</h1>
						<h4 class="text-secondary text-center">Ocultos</h4>
					</div>
					<div class="col-md-12 my-2">
						<hr>
					</div>
					<div class="col-md-8">
						<div class="accordion" id="accordionExample">
							<button class="btn btn-blue btn-block" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								<i class="fas fa-filter"></i>
								Filtros
							</button>  
							<div id="collapseOne" class="collapse {{$filtered_data?'show':''}}" aria-labelledby="headingOne" data-parent="#accordionExample">
								<div class="card-body px-3 pt-2">
									<form action="{{ url('escalafon/moodle') }}" method="GET" enctype="multipart/form-data" id="filters">
										<div class="form-row mb-3 justify-content-center">
											<div class="form-group col-md-4 col-sm-12">
												<label for="courses" class="col-form-label font-weight-bold text-blue">Cursos</label>
												<select id="courses" name="courses[]" class="selectpicker form-control" data-live-search="true" data-size="10" title="Selecciona una opción..." multiple>
													@foreach($courses['all'] as $key => $row)
														<option value="{{ $row['fullname'] }}" {{ in_array($row['fullname'], $customFilters['courses']) }}>
															{{ $row['fullname'] }}
														</option>
													@endforeach
												</select>
											</div>
											<div class="form-group col-md-4 col-sm-12">
												<label for="categories" class="col-form-label font-weight-bold text-blue">Categorías</label>
												<select id="categories" name="categories[]" class="selectpicker form-control" data-live-search="true" data-size="10" title="Selecciona una opción..." multiple>
													@foreach($filters['categories'] as $row)
														<option value="{{ $row['id'] }}" {{ in_array($row['id'], $customFilters['categories']) }}>
															{{ $row['name'] }}
														</option>
													@endforeach
												</select>
											</div>
											<div class="form-group col-md-4 col-sm-12">
												<label for="users" class="col-form-label font-weight-bold text-blue">Usuarios</label>
												<select id="users" name="users[]" class="selectpicker form-control" data-live-search="true" data-size="10" title="Selecciona una opción..." multiple>
													@foreach($filters['users'] as $row)
														<option value="{{ $row->id }}" {{ in_array($row->id, $customFilters['users']) }}>
															{{ $row->getFullNameAttribute() }}
														</option>
													@endforeach
												</select>
											</div>
											<div class="form-group col-md-4 col-sm-12">
												<label for="branch_offices" class="col-form-label font-weight-bold text-blue">Sucursales</label>
												<select id="branch_offices" name="branch_offices[]" class="selectpicker form-control" data-live-search="true" data-size="10" title="Selecciona una opción..." multiple>
													@foreach($filters['branch_offices'] as $row)
														<option value="{{ $row }}" {{ in_array($row, $customFilters['branch_offices']) }}>
															{{ $row }}
														</option>
													@endforeach
												</select>
											</div>
											<div class="form-group col-md-4 col-sm-12">
												<label for="jobs" class="col-form-label font-weight-bold text-blue">Puestos</label>
												<select id="jobs" name="jobs[]" class="selectpicker form-control" data-live-search="true" data-size="10" title="Selecciona una opción..." multiple>
													@foreach($filters['jobs'] as $row)
														<option value="{{ $row }}" {{ in_array($row, $customFilters['jobs']) }}>
															{{ $row }}
														</option>
													@endforeach
												</select>
											</div>
											<div class="form-group col-md-4 col-sm-12">
												<label for="departments" class="col-form-label font-weight-bold text-blue">Unidad de Negocio</label>
												<select id="departments" name="departments[]" class="selectpicker form-control" data-live-search="true" data-size="10" title="Selecciona una opción..." multiple>
													@foreach($filters['departments'] as $key => $row)
														<option value="{{$row}}" {{ in_array($row, $customFilters['departments']) }}>
															{{ $row }}
														</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="form-row mt-3 justify-content-center">
											<div class="form-group col text-left my-auto pt-3 pl-3 text-center">
												@if($filtered_data)
													<a class="btn btn-blue btn-sm ml-2" href="{{ url('escalafon/moodle') }}">
														<i class="fas fa-times-circle"></i>
														Remover Filtrado
													</a>
												@endif
												<button class="btn btn-lightblue btn-sm" type="submit">
													<i class="fas fa-filter"></i>
													Filtrar
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 my-2">
						<hr>
					</div>
					<div class="col-md-12">
						<table id="moodle_report" class="table table-striped table-bordered w-100 nowrap">
							<thead class="bg-blue text-white">
								<tr>
									<th class="bg-lightblue pr-5">ID</th>
									<th class="bg-lightblue pr-5">Plan</th>
									<th class="bg-lightblue">Curso</th>
									<th class="bg-lightblue">Categoría</th>
									<th>Modalidad</th>
									<th>Usuario</th>
									<th>Tipo Usuario</th>
									<th>Estatus Curso</th>
									<th>Calificación</th>
									<th>Duración</th>
									<th>Unidad de Negocio</th>
									<th>Puesto</th>
									<th>Sucursal</th>
									<th>Planeado</th>
									<th>Costo</th>
									<th>Prioridad</th>
									<th>Nombre Instructor</th>
									<th>RFC Instructor</th>
								</tr>
								<tr class="bg-lightgray" id="thead_filters">
									@for ($i = 0; $i <= 17; $i++)
										<th class="bg-lightgray">
											<input type="text" class="form-control text-center" placeholder="&#xF002;" style="font-family:Arial, FontAwesome" data-index="{{$i}}">
										</th>
									@endfor
								</tr>
							</thead>
							<tbody>
								@foreach($courses['all'] as $key => $row)
									@foreach($row['enrolled'] as $user)
										<tr class="text-center" nowrap>
											<td>
												{{ $key }}
											</td>
											<td>
												{!! $user->getScalePlanName($key) ?? '<small><span class="text-muted">(N/A)</span></small>' !!}
											</td>
											<td>
												{{ $row['fullname'] }}
											</td>
											<td>
												{{ $row['category'] }}
											</td>
											<td>
												{{ $row['modality'] }}
											</td>
											<td>
												{{ $user->getFullNameAttribute() }}
											</td>
											<td>
												{{ $user->employee->external?'Externo':'Interno' }}
											</td>
											<td>
												@if($user->course['status'] == 2)
													<span class="text-success">Completado</span>
												@elseif($user->course['status'] == 1)
													<span class="text-info">En Progreso</span>
												@else
													<span class="text-muted">Sin Iniciar</span>
												@endif
											</td>
											<td>
												@if($user->course['grade'])
													{{ $user->course['grade'] }}
												@else
													<small><span class="text-muted">(S/C)</span></small>	
												@endif
											</td>
											<td>
												{{-- Duración --}}
												@if($row['type'] == 'E')
													{{ $row['duration'] ?? 'N/A' }} horas
												@else
													<small><span class="text-muted">(N/A)</span></small>
												@endif
											</td>
											<td>
												{{ $user->employee->getDepartmentName(true) }}
											</td>
											<td>
												{{ $user->employee->getPuestoName(true) }}
											</td>
											<td>
												{{ $user->employee->getSucursal() }}
											</td>
											<td>
												{!! $user->course_priority ?? '<small><span class="text-muted">(N/A)</span></small>' !!}
											</td>
											<td>
												{!! $user->getScaleCoursePrice($key) ?? '<small><span class="text-muted">(N/A)</span></small>' !!}
											</td>
											<td>
												{!! $user->course_priority ?? '<small><span class="text-muted">(N/A)</span></small>' !!}
											</td>
											<td>
												@if($row['hasTrainer'])
													{{ $row['trainer_name'] }}
												@else
													<small><span class="text-muted">(N/A)</span></small>												
												@endif
											</td>
											<td>
												@if($row['hasTrainer'])
													{{ $row['trainer_rfc'] }}
												@else
													<small><span class="text-muted">(N/A)</span></small>												
												@endif
											</td>
										</tr>
									@endforeach
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	var moodle_report = $('#moodle_report').DataTable({
		"bSortCellsTop": true,
		"order": [[ 0, "asc" ]],
		"paging": true,
		"pagingType": "numbers",
		"scrollX": true,
		"fixedColumns":{
			"leftColumns": 4
		},
		"language": {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     ">",
				"sPrevious": "<"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
	});
	
    $( moodle_report.table().container() ).on( 'keyup', 'thead input', function () {
        moodle_report
            .column( $(this).data('index') )
            .search( this.value )
            .draw();
    } );

	$(function() {
		redrawTableWrapper();
	});


	function redrawTableWrapper() {
		setTimeout(() => {
			$('.loading').addClass('d-none');
			$('.tableWrapper').removeClass('d-none');
			$($.fn.dataTable.tables(true)).DataTable()
			.columns.adjust()
			.fixedColumns().relayout();
		}, 1000);
	}

</script>
@endsection