@extends('layouts.app')

@section('title', 'Escalafón')

@section('content')

<div class="row my-5">
	<div class="col">
		<div class="flash-message" id="mensaje">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
				@if(Session::has($msg))
					<p class="alert alert-{{ $msg }}">{{ Session::get($msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
				@endif
			@endforeach
		</div>
		<div class="card card-3">
			<div class="card-header bg-blue">
				<h2 class="my-auto font-weight-bold text-center text-white">Reportes Universidad Ucin</h2>
			</div>
			<div class="card-body">
				<div class="row loading">
					<div class="col-md-12 text-center">
						<div class="spinner-border text-blue" style="width: 6rem; height: 6rem;" role="status">
							<span class="sr-only">Loading...</span>
						</div>
						<h3 class="text-blue mt-2"><strong>Cargando...</strong></h3>
					</div>
				</div>
				<div class="row tableWrapper d-none">
					<div class="col-md-12">
						<form action="{{ url('escalafon/overview') }}" method="GET" enctype="multipart/form-data" id="filters">
							<div class="form-row mb-3 justify-content-center">
								<div class="form-group col-md-3 col-sm-12">
									<label for="department" class="col-form-label font-weight-bold">Unidad de Negocio</label>
									<select id="department" name="department" class="selectpicker form-control" data-live-search="true" title="Selecciona una opción...">
										@foreach($filters['departments'] as $key => $row)
											<option value="{{$row}}" {{ $custom_filters['department'] == $row?'selected':'' }}>
												{{ $row }}
											</option>
										@endforeach
									</select>
								</div>
								<div class="form-group col-md-3 col-sm-12">
									<label for="branch_office" class="col-form-label font-weight-bold">Sucursal</label>
									<select id="branch_office" name="branch_office" class="selectpicker form-control" data-live-search="true" title="Selecciona una opción...">
										@foreach($filters['branch_offices'] as $key => $row)
											<option value="{{$row}}" {{ $custom_filters['branch_office'] == $row?'selected':'' }}>
												{{ $row }}
											</option>
										@endforeach
									</select>
								</div>
								<div class="form-group col-md-3 col-sm-12">
									<label for="reportrange" class="col-form-label font-weight-bold">Fechas</label>
									<div id="reportrange" class="form-control" style="cursor: pointer;">
										<i class="fa fa-calendar"></i>&nbsp;
										<span></span> <i class="fa fa-caret-down"></i>
									</div>
								</div>
								<div class="form-group col-md-2 text-left my-auto pt-3 pl-3">
									<button class="btn btn-lightblue btn-sm" type="submit">
										<i class="fas fa-filter"></i>
										Filtrar
									</button>
									@if($custom_filters['department'] || $custom_filters['branch_office'])
										<a class="btn btn-blue btn-sm ml-2" href="{{ url('escalafon/overview') }}">
											<i class="fas fa-times-circle"></i>
											Remover Filtrado
										</a>
									@endif
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="row justify-content-end tableWrapper d-none">
					<div class="col-md-3">
						<div class="form-group">
							<input type="text" class="form-control" name="buscador" id="buscador" placeholder="Buscar">
						</div>
					</div>
				</div>
				<div class="row tableWrapper d-none">
					<div class="col-md-12">
						<table id="tree-table" class="table table-bordered table-hover w-100">
							<thead class="bg-blue text-white">
								<tr {{-- class="text-center" --}}>
									<th class="bg-lightblue">Nombre</th>
									<th>Unidad de Negocio</th>
									<th>Puesto</th>
									<th>Sucursal</th>
									<th>Finalización</th>
									<th>Finalizados</th>
									<th>Promedio</th>
									{{-- <th>Estado</th> --}}
								</tr>
							</thead>
							<tbody class="treegrid-body text-center">
								{!! $data !!}
								<tr>
									<td colspan="7" id="no_resultado" class="text-center {{ empty($data)?'':' d-none' }}">
										<h1 class="text-blue">Sin resultados de búsqueda...</h1>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')

<script type="text/javascript">

	$(document).ready(function(){
        $("#buscador").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".encont").filter(function() {
                var tr = $(this).parent();
				tr.toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
            if($(".encont").filter(":visible").length == 0) {
                $("#no_resultado").removeClass("d-none");
            } else {
                $("#no_resultado").addClass("d-none");
            }
        });
    });

	$(function () {
   		var table = $('#tree-table'), rows = table.find('tr');

		rows.each(function (index, row) {
			var
				row = $(row),
				level = row.data('level'),
				id = row.data('id'),
				columnName = row.find('td[data-column="name"]'),
				children = table.find('tr[data-parent="' + id + '"]'),
				dot = '<i class="far fa-circle lighblue"></i>';

			if (children.length) {
				var expander = columnName.prepend('' + '<span class="treegrid-expander fas fa-chevron-right lighblue"></span>' + '');

				children.hide();

				expander.on('click', function (e) {
					var target = $(e.target);
					if (target.hasClass('fa-chevron-right')) {
						target
							.removeClass('fa-chevron-right')
							.addClass('fa-chevron-down');

						children.show();
					} else if(target.hasClass('fa-chevron-down')) {
						target
							.removeClass('fa-chevron-down')
							.addClass('fa-chevron-right');

						reverseHide(table, row);
					}
				});
			} else {
				var rawr = columnName.prepend('<span class="treegrid-nochild lighblue"></span>');
			}

			columnName.prepend('' + '<span class="treegrid-indent lighblue" style="width:' + 15 * level + 'px"></span>' + '');
		});

		// Reverse hide all elements
		reverseHide = function (table, element) {
			var
				element = $(element),
				id = element.data('id'),
				children = table.find('tr[data-parent="' + id + '"]');

			if (children.length) {
				children.each(function (i, e) {
					reverseHide(table, e);
				});

				element
					.find('.fa-chevron-down')
					.removeClass('fa-chevron-down')
					.addClass('fa-chevron-right');

				children.hide();
			}
		};
	});

	$(function() {
		redrawTable();
	});


	function redrawTable() {
		setTimeout(() => {
			$('.loading').addClass('d-none');
			$('.tableWrapper').removeClass('d-none');
			// $($.fn.dataTable.tables(true)).DataTable()
			// .columns.adjust()
			// // .fixedColumns()
			// .relayout();
		}, 1000);
	}

	$(function() {
		var start = moment().subtract(29, 'days');
		var end = moment();

		function cb(start, end) {
			$('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
		}

		$('#reportrange').daterangepicker({
			startDate: start,
			endDate: end,
			ranges: {
				'Hoy': [moment(), moment()],
				'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
				'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
				'Este Mes': [moment().startOf('month'), moment().endOf('month')],
				'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			locale: {
                "format": "DD/MM/YYYY",
                "separator": " a ",
                "applyLabel": "Aceptar",
                "cancelLabel": "Cancelar",
                "fromLabel": "De",
                "toLabel": "Hasta",
                "customRangeLabel": "Personalizar",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
				"monthNames": [
					"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
				]
            }
		}, cb);

		// cb(start, end);
	});
</script>
@endsection