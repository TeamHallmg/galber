<?php
	
return [
	//Days of the week (short)
	'Mon' => 'Lu',
	'Tue' => 'Ma',
	'Wed' => 'Mi',
	'Thu' => 'Ju',
	'Fri' => 'Vi',
	'Sat' => 'Sa',
	'Sun' => 'Do',
	//Days of the week 
	'l-Mon' => 'Lunes',
	'l-Tue' => 'Martes',
	'l-Wed' => 'Miércoles',
	'l-Thu' => 'Jueves',
	'l-Fri' => 'Viernes',
	'l-Sat' => 'Sábado',
	'l-Sun' => 'Domingo',
	//
	'complete' => 'completado',
	'accepted' => 'aceptado',
	'processed' => 'procesado',
	'rejected' => 'rechazado',
	'pending' => 'pendiente', 
	'canceled' => 'cancelado',
	'global' => 'global',
	'time' => 'tiempo',
	'day' => 'dia',

	//benefits
	'benefit-group-incident' => 'Incidencia',
	'benefit-group-benefit' => 'Beneficio',
	'benefit-group-incapacity' => 'Incapacidad',
	'benefit-group-' => 'Sin grupo',
];
?>