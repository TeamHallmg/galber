<?php

return [
    //Common
        'createnew' => 'Crear nuevo',
        'save' => 'Guardar',
        'cancel' => 'Cancelar',
		'loggedin' => 'Has iniciado sesión!',
    //Menus
        //Common menu
        'timerequest' => 'Tiempo extra',
        'login' => 'Ingresar',
        'config' => 'Configuración',
        'changepassword' => 'Cambiar contraseña',
        'logout' => 'Salir',
        'calendar' => 'Calendario',
        'receipts' => 'Recibos',
        'balance' => 'Saldo',
        //Clock menu
        'clock' => 'Reloj',
        'arrive_leave' => 'Entradas y salidas',
        'absence' => 'Faltas',
        'delay' => 'Retardos',
        'overtime' => 'Horas extras',
        //Requests menu
        'requests' => 'Solicitudes',
        'requesthistory' => 'Historial de solicitudes',
        //Super menu
        'supervise' => 'Supervisar',
        'events' => 'Eventos',
        'appruverequest' => 'Aprobar solicitudes',
        'appruveincident' => 'Aprobar incidencias',
        'substitute' => 'Nombrar un sustituto',
        //Admin menu
        'administration' => 'Administrar',
        'export' => 'Exportar',
        'incapacityexport' => 'Exportar incapacidades',
        'incapacityimport' => 'Importar incapacidades',
        'importusers' => 'Importar usuarios',
        'importreceipts' => 'Importar recibos',
        'viewschedules' => 'Ver horarios',
        'viewrules' => 'Ver reglas',
        'viewincidents' => 'Ver incidencias',
        'viewrequests' => 'Ver solicitudes',
        'users' => 'Usuarios',
        'masive' => 'Masivos',
        'incidents' => 'Incidencias',
        'generalists' => 'Generalistas',
];
