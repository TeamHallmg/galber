<?php

return [

    'use_max_type_of_time' => [
        'day' => '{1} Solo puedes solicitar un día por solicitud de :benefit|[2.*] No puedes solicitar más de :max días disponibles por solitud de :benefit',
        'hour' => '{1} Solo puedes solicitar una hora por solicitud de :benefit|[2.*] No puedes solicitar más de :max horas por solictud de :benefit'
    ],

    'period_use_type_of_time' => [
        'day' => 'día(s) por solicitud',
        'month' => '{1} Solo puedes solicitar un día por mes para :benefit|[2.*] No puedes solicitar más de :max días por mes para :benefit, intenta solicitar menos.',
        'month-hour' => '{1} Ya has utilizado el máximo de horas de un total de :max por mes para :benefit|[2,*] No puedes solicitar :count horas, intenta solicitar menos. El máximo de horas por mes es :max para :benefit',
        'year' => '{1} Ya has solicitado el máximo de días de un total de :max por año para :benefit',
        'year-hour' => '{1} Ya has utilizado el máximo de horas de un total de :max por año para :benefit|[2,*] No puedes solicitar :count horas, intenta solicitar menos. El máximo de horas por año es :max para :benefit',
        'half-year' => '{1} Ya has solicitado el máximo de días de un total de :max en esta mitad del año para :benefit',
        'half-year-hour' => '{1} Ya has utilizado el máximo de horas de un total de :max en esta mitad del año para :benefit|[2,*] No puedes solicitar :count horas, intenta solicitar menos. El máximo de horas pora esta mitad de año es :max para :benefit',
    ]
];
