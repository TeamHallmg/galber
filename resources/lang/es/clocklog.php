<?php

return [

    'on-time' => 'A tiempo',
    'early' => 'Llegada temprano',
    'tolerance' => 'Con tolerancia',
    'late' => 'Llegada tarde',
    'leave_early' => 'Salida temprano',
    'ok' => 'Salida',
    'none' => 'Estatus no encontrado',
];