<?php
	
return [
	//Days of the week (short)
	'Mon' => 'Mon',
	'Tue' => 'Tue',
	'Wed' => 'Wed',
	'Thu' => 'Thu',
	'Fri' => 'Fri',
	'Sat' => 'Sat',
	'Sun' => 'Sun',
	//Days of the week 
	'l-Mon' => 'Monday',
	'l-Tue' => 'Tuesday',
	'l-Wed' => 'Wednesday',
	'l-Thu' => 'Thursday',
	'l-Fri' => 'Friday',
	'l-Sat' => 'Saturday',
	'l-Sun' => 'Sunday',
	//
	'complete' => 'complete',
	'accepted' => 'accepted',
	'processed' => 'processed',
	'rejected' => 'rejected',
	'pending' => 'pending', 
	'canceled' => 'canceled',
	'global' => 'global',
	'time' => 'time',
	'day' => 'day',

	//benefits
	'benefit-group-incident' => 'Incident',
	'benefit-group-benefit' => 'Benefit',
	'benefit-group-incapacity' => 'Incapacity',
	'benefit-group-' => 'None',
];
?>