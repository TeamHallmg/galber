<?php

return [
    //Common
        'createnew' => 'Create new',
        'save' => 'Save',
        'cancel' => 'Cancel',
		'loggedin' => 'You are logged in!',
    //Menus
        //Common menu
        'timerequest' => 'Overtime',
        'login' => 'Login',
        'config' => 'Configuration',
        'changepassword' => 'Change password',
        'logout' => 'Logout',
        'calendar' => 'Calendar',
        'receipts' => 'Receipts',
        'balance' => 'Balance',
        //Clock menu
        'clock' => 'Clock',
        'arrive_leave' => 'Arrive & leave',
        'absence' => 'Absence',
        'delay' => 'Delay',
        'overtime' => 'Overtime',
        //Requests menu
        'requests' => 'Requests',
        'requesthistory' => 'Request history',
        //Super menu
        'supervise' => 'Supervise',
        'events' => 'Events',
        'appruverequest' => 'Approve Requests',
        'appruveincident' => 'Approve incidents',
        'substitute' => 'Set a commission',
        //Admin menu
        'administration' => 'Administration',
        'export' => 'Export',
        'incapacityexport' => 'Export Incapacities',
        'incapacityimport' => 'Import Incapacities',
        'importusers' => 'Import users',
        'importreceipts' => 'Import receipts',
        'viewschedules' => 'View schedules',
        'viewrules' => 'View rules',
        'viewincidents' => 'View incidents',
        'viewrequests' => 'View requests',
        'users' => 'Users',
        'masive' => 'Masive',
        'incidents' => 'Incidents',
        'generalists' => 'Generalists',
];
