<?php

return [

    'on-time' => 'On Time',
    'early' => 'Arrive Early',
    'tolerance' => 'In Tolerance',
    'late' => 'Arrive Late',
    'leave_early' => 'Leave Early',
    'ok' => 'Leave',
    'none' => 'Status not Found',
];