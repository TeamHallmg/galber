<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('main')->middleware('guest');

/*Login Moodle*/
Route::get('mdlLogin/{email}/{token}', '\App\Http\Controllers\Auth\LoginController@mdlLogin');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

/*******************FAKELOGIN********************/
Route::get('checklogin', function () {
    return view('employee_login.login');
})->name('checklogin');
Route::post('check_employee', 'Auth\LoginController@check_employee')->name('check_employee');
Route::post('confirm_employee', 'Auth\LoginController@confirm_employee')->name('confirm_employee');
Route::get('success_check/{name}/{number}', 'Auth\LoginController@success_check')->name('success_check');

/*
 *	Rutas de Importacion y de Moodle
 */
Route::group(['prefix' => 'cron'], function () {
    Route::get('import', 'CronAutomatico\CronController@mainFunction');
    Route::get('run/{cronType}/{date?}', 'CronAutomatico\CronRunController@run');
    Route::get('current_balance', 'CronAutomatico\CronRunController@generateCurrentBalanceLine');

    Route::post('manual_import', 'CronAutomatico\CronController@manualImport');
    Route::get('process_import/{filename}', 'CronAutomatico\CronController@processImport');
});

Route::group(['middleware' => 'auth'], function () {
    

/**
 *  ================ Announcement Routes ================ 
 */
Route::resource('announcements', 'Announcement\AnnouncementController');
Route::resource('announcement_types', 'Announcement\AnnouncementTypeController');
Route::resource('announcement_categories','Announcement\AnnouncementCategoryController');
Route::resource('views', 'Announcement\ViewController');
Route::resource('announcement_forms', 'Announcement\AnnouncementFormController');
Route::resource('announcement_types_per_view', 'Announcement\AnnouncementTypePerViewController');
Route::post('announcements/{announcement}/activate', 'Announcement\AnnouncementController@activate');

Route::get('announcement_actions_report', 'Announcement\AnnouncementController@actionsReport');
Route::post('announcement_user_action', 'Announcement\AnnouncementController@setAction')->name('announcement_user_action');
Route::post('announcement_mark_as_watched', 'Announcement\AnnouncementController@markAsWatched')->name('announcement_mark_as_watched');
Route::get('announcement_in_area', 'Announcement\AnnouncementController@announcementInArea')->name('announcement_in_area');

Route::get('home', 'Announcement\AnnouncementController@home');
Route::get('organigrama', 'HomeController@organigrama');
Route::get('proposito-mision-vision', 'Announcement\AnnouncementController@valores');

Route::get('announcements_seed', 'Announcement\AnnouncementController@seed');
Route::get('cumpleaños', 'BirthdayController@index')->name('cumpleaños');
Route::get('movimientos-personal', 'HomeController@employeesMovement');
Route::get('eventos', 'HomeController@events');
Route::get('oficinas', 'HomeController@offices');
Route::get('nuevos-ingresos', 'HomeController@newEmployees');
Route::get('calendario', 'HomeController@calendario')->name('calendario');
Route::get('calendario/{id}', 'HomeController@showCalendarEvent');
Route::get('mis_beneficios', 'HomeController@benefits');
// Route::get('convenios', 'HomeController@convenios')->name('convenios');
Route::get('preguntas-frecuentes', 'HomeController@faq');
Route::get('objetivos-calidad', 'HomeController@qualityObjectives');
// Route::get('comites', 'HomeController@committees');
// Route::get('comisiones', 'HomeController@commissions');
/**
 *  =============== /Announcement Routes ================
 */

 /**
 *   ================ Admin de Usuarios ================ 
 */
Route::get('initializePermissionsData', 'PermissionController@initialize');
// Route::get('initializePermissionsData', 'UserController@initializePermissionsData');
Route::get('setMoodleUsersLanguage','UserController@setMoodleUsersLanguage');
Route::get('initializeMoodleData', 'UserController@initializeMoodleData');
Route::resource('admin-de-usuarios','UserController');
Route::resource('admin-form','EmployeeFormController');

Route::post('admin-de-usuarios/{id}/activate','UserController@activate')->name('activate-user');
Route::get('admin-de-usuarios/{id}/balances','UserController@balances')->name('admin_balances');
Route::get('generar-contrato/{id}','UserController@generarContrato');
Route::get('debugUserEmails','UserController@debugUserEmails');
Route::get('authMeWith/{user_email}', 'UserController@authMeWith');
Route::resource('direcciones','DirectionController');
Route::resource('departamentos','DepartmentController');
Route::resource('areas','AreaController');

Route::get('trashedDepartments/{id}','DepartmentController@getTrashed')->name('trashed-departments');
Route::get('trashedAreas/{id}','AreaController@trashedAreas')->name('trashed-areas');

Route::get('departments/{id}','PuestoController@departments')->name('departments');
Route::get('area/{id}','PuestoController@areas')->name('area');
Route::get('trashedJobs/{id}','PuestoController@trashedJobs')->name('trashed-jobs');
Route::get('jobpositions/{id}','PuestoController@jobpositions')->name('jobpositions');
Route::get('jobboss','PuestoController@jobboss')->name('jobboss');
Route::get('levels','PuestoController@levels')->name('levels');

Route::get('jobsFromDepartment', 'JobPositionController@getJobsFromDepartment');


 /**
 *   =============== /Admin de Usuarios ================ 
 */



//*********************************************************************************
//rutas para el perfil de personal
//inicio
Route::get('profile/{u}/{p}/{v}/consulta', 'Profile\ProfileController@consulta');
Route::get('profile/{p}/{v}/generar', 'Profile\ProfileController@generar');
Route::post('createBasica', 'Profile\ProfileController@createBasica');
Route::put('updateBasica', 'Profile\ProfileController@updateBasica');
Route::post('createComplementaria', 'Profile\ProfileController@createComplementaria');
Route::post('createRegistro', 'Profile\ProfileController@createRegistro');
Route::put('updateRegistro', 'Profile\ProfileController@updateRegistro');
Route::post('createSalud', 'Profile\ProfileController@createSalud');
Route::put('updateSalud', 'Profile\ProfileController@updateSalud');
Route::post('createBeneficiarios', 'Profile\ProfileController@createBeneficiarios');
Route::put('updateBeneficiarios', 'Profile\ProfileController@updateBeneficiarios');

Route::get('curriculum/{idVi?}', 'Profile\ProfileController@curriculum');
Route::get('profile/{id}/{idVi}/externo', 'Profile\ProfileController@externo');
Route::get('profile/{id}/showExternal', 'Profile\ProfileController@showExternal');
Route::get('listsprofile', 'Profile\ProfileController@listsprofile');
Route::get('listStates/{id}', 'Entidades\EntidadesController@listadoMunicipios');
 
Route::resource('profile', 'Profile\ProfileController');


Route::resource('Tipobienes', 'Bienes\TipoBienesController')->middleware('permission:profile_admin');

Route::resource('TipoBienesDetalles', 'Bienes\VariablesController')->middleware('permission:profile_admin');

Route::post('createBienes', 'Profile\ProfileController@createBienes');
Route::put('updateBienes', 'Profile\ProfileController@updateBienes');

Route::post('createContrato', 'Profile\ProfileController@createContrato');
Route::put('updateContrato', 'Profile\ProfileController@updateContrato');


Route::post('AsignarBienes', 'Profile\AdminBienesController@AsignarBienes');
Route::get('NotaEntregaBien/{profile}/{id}', 'Profile\AdminBienesController@entregaBienes');
Route::post('adjuntar_comprobante_bienes', 'Profile\AdminBienesController@adjuntar_comprobante');
Route::get('/descargar_comprobante/{id}', 'Profile\AdminBienesController@descargar_documento');
Route::post('desincorporar_bien', 'Profile\AdminBienesController@desincorporar_bien');


//fin
//*********************************************************************************



//rutas para el puestos de la empresa
//inicio ***************
Route::resource('jobPosition', 'JobPositionController');
Route::get('get_jobposition_data/{id?}', 'JobPositionController@getJobPositionData');
//Se agrega esta ruta, ya que es una consulta a la tabla puestos y obtener
//el beneficio del puesto de la persona logueada
Route::get('beneficios', 'JobPositionController@beneficios');
//fin



//*************************************************************************
//rutas para el modulo de Vacaciones
//inicio ***************
Route::group(['prefix' => 'common', 'as' => 'common.'], function () {
    Route::get('balances', 'EmployeeController@balances')->name('balances');
    Route::get('inactive', 'Vacations\PleaController@getInactive')->name('plea.inactive');
    Route::resource('plea', 'Vacations\PleaController');
    Route::get('clock', 'ClockController@clock');
    Route::resource('clocklog', 'Vacations\ClockLogController');
    
    Route::get('getCalendarEvents/{id?}', 'Vacations\CalendarController@getCalendarEvents')->name('getCalendarEvents');
    Route::get('authMeWith/{user_email}', 'EmployeeController@authMeWith');
    Route::get('debugToolForBalanceCheck/{user_id?}/{date?}', 'EmployeeController@debugToolForBalanceCheck');
});

Route::group(['prefix' => 'super', 'as' => 'super.'], function () {
    Route::get('user/incidents', 'Vacations\IncidentController@index')->name('user.incidents.index');
    Route::get('user/incidents/{user}', 'Vacations\IncidentController@show')->name('user.incidents.show');
    Route::post('user/incidents/{user}', 'Vacations\IncidentController@store')->name('user.incidents.store');
    Route::get('user/incidents/{user}/review', 'Vacations\IncidentController@review')->name('user.incidents.review');
    Route::put('user/incidents/{user}', 'Vacations\IncidentController@update')->name('user.incidents.update');
    Route::get('approve/{request}', 'Vacations\SuperController@approve')->name('approve');
    Route::get('eventstable', 'Vacations\EventsController@table');
    Route::post('requests', 'Vacations\SuperController@requests')->name('requests');
    Route::post('delete_approved_request', 'Vacations\SuperController@deleteApprovedRequest');
    Route::post('incidents', 'Vacations\SuperController@incidents');
    Route::post('incidents/{id}', 'Vacations\SuperController@update');
    Route::resource('events', 'Vacations\EventsController');
    Route::get('clocklog/today', 'Vacations\ClockLogController@today')->name('clocklog.supervisor.today');
    Route::get('clocklog/review_problems', 'Vacations\ClockLogController@review_problems')->name('clocklog.supervisor.review_problems');
    Route::get('clocklog/missing_logs', 'Vacations\ClockLogController@missing_logs')->name('clocklog.supervisor.missing_logs');
    Route::post('clocklog/approve_clock_incidents', 'Vacations\ClockLogController@approve_clock_incidents')->name('clocklog.supervisor.approve_clock_incidents');
    Route::post('clocklog/create_abscent_incident', 'Vacations\ClockLogController@create_abscent_incident')->name('clocklog.supervisor.create_abscent_incident');
    Route::post('clocklog/manage_absents', 'Vacations\ClockLogController@manage_absents')->name('clocklog.supervisor.manage_absents');
    Route::get('clocklog', 'Vacations\ClockLogController@supervisor')->name('clocklog.supervisor');
    Route::get('clocklog/history', 'Vacations\ClockLogController@history')->name('clocklog.supervisor.history');
    // Route::get('userstable', 'UsersController@table');
    // Route::get('users/{id}/add', 'UsersController@addBenefit');
    // Route::post('users/{id}/add', 'UsersController@setBenefit');
    // Route::resource('users', 'UsersController');
    // Route::get('excel/requests', 'Export\ExcelController@requests');
    // Route::get('excel/incidents', 'Export\ExcelController@incidents');
    Route::get('view/incidents', 'Vacations\AdminController@incidents');
    Route::get('view/requests', 'Vacations\AdminController@requests')->name('view.requests');
    Route::get('view/schedule/{last_date?}/{direction?}', 'Vacations\AdminController@schedule')->name('view.schedule');
    Route::get('view/balances/{user_id?}', 'Vacations\AdminController@employee_balances')->name('view.employee_balances');
    Route::get('tableincidents', 'Vacations\AdminController@tableIncidents');
    Route::post('tableincidents', 'Vacations\AdminController@tableIncidents');
    Route::get('tablerequests', 'Vacations\AdminController@tableRequests');
    Route::post('tablerequests', 'Vacations\AdminController@tableRequests'); 
});

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::resource('benefitjobs', 'Vacations\BenefitJobController');
    Route::get('clocklog/history', 'Vacations\AdminClocklogController@history')->name('clocklog.history');
    Route::get('incidents/history', 'Vacations\AdminController@history')->name('incidents.history');

    Route::get('balances', 'Vacations\AdminController@balances')->name('incidents.balances');
    Route::get('approve/{request}', 'Vacations\AdminController@approve')->name('approve');
    Route::get('view/balances/{user_id?}', 'Vacations\AdminController@admin_employee_balances')->name('view.employee_balances');
    /** Rutas para crear lineas de balances extraordinarias **/
    Route::get('balances/user', 'Vacations\BalanceUserController@index')->name('incidents.balances.create');
    Route::get('balances/user_year/{id}', 'Vacations\BalanceUserController@getYear');
    Route::post('balances/user', 'Vacations\BalanceUserController@store')->name('incidents.balances.store');
    /** Fin Rutas para crear lineas de balances extraordinarias **/

    // Route::resource('balances', 'AdminBalanceController');

    // Route::get('view/rules', 'AdminController@rules');
    // Route::get('masivetable', 'MasiveUsersController@table');

    // Route::get('config', 'AdminController@config');
    // Route::post('config', 'AdminController@setconfig');

    Route::get('export/{type}', 'Vacations\ExportController@index')->name('exports.index');
    Route::get('unprocessVacations/{date?}', 'Vacations\ExportController@unprocessVacations');
    Route::post('export_vacations', 'Vacations\ExportController@exportVacations')->name('exports.vacations');
    Route::post('export_incidents', 'Vacations\ExportController@exportIncidents')->name('exports.incidents');

    // Route::get('export', 'ExportController@export');
    // Route::get('getcsv', 'ExportController@getcsv');
    // Route::get('getxls', 'ExportController@getxls');

    // Route::get('users/{id}/add', 'UsersController@addBenefit');
    // Route::post('users/{id}/add', 'UsersController@setBenefit');
    // Route::get('users/{id}/login', 'UsersController@loginAs');

    // Route::post('masiveInfo', 'MasiveUsersController@userInfo');
    // Route::resource('masive', 'MasiveUsersController');
    Route::resource('incidents', 'Vacations\BenefitsController');
    Route::resource('schedule', 'ScheduleController');
    // Route::resource('generalist', 'GeneralistController');

    if(config('config.clockimport')) {
        Route::get('clock/import', 'ClockController@import');
        Route::post('clock/process', 'ClockController@manualprocess');
    }
});
 
//fin
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/*Route::get('/home', function(){
  return view('evaluacion-desempeno/que-es-evaluacion-desempeno');
})->middleware('auth');*/


/********************************************************************************** */
//rutas para codigos que nos ayuden a actualizar información y/o parchar ciertos errores
//inicio ***************
Route::get('turnRegionIntoForeign', 'Fixes\FixingCronDataController@turnRegionIntoForeign');
Route::get('crons','CronController@index');

//fin
//*************************************************************************

/*
Empieza evaluacion de desempeño
*/
//Route::group([ 'middleware' => 'admin'], function () {
    Route::get('lista-empleados', 'EvaluacionDesempeno\UsersController@lista_usuarios');
    Route::post('cambiar-jefe/{id_user}', 'EvaluacionDesempeno\UsersController@cambiar_jefe');
    Route::get('cambiar-jefe/{id_user}', 'EvaluacionDesempeno\UsersController@cambiar_jefe');
    Route::get('estadisticas', 'EvaluacionDesempeno\Admin\EstadisticasController@estadisticas');
    Route::post('estadisticas', 'EvaluacionDesempeno\Admin\EstadisticasController@estadisticas');
    Route::get('estadisticasAP', 'EvaluacionDesempeno\Admin\EstadisticasController@estadisticasConAreasYPuestos');
    Route::resource('areas', 'EvaluacionDesempeno\Admin\GruposAreasController');
    Route::resource('puestos', 'EvaluacionDesempeno\Admin\NivelesPuestosController');
    Route::resource('periodos', 'EvaluacionDesempeno\Admin\PeriodosController');
    Route::post('crear-periodo', 'EvaluacionDesempeno\Admin\PeriodosController@create');
    Route::post('editar-periodo', 'EvaluacionDesempeno\Admin\PeriodosController@edit');
    Route::get('borrar-periodo/{id}', 'EvaluacionDesempeno\Admin\PeriodosController@destroy');
    Route::post('borrar-periodo', 'EvaluacionDesempeno\Admin\PeriodosController@destroy');
    Route::resource('tipos-factores', 'EvaluacionDesempeno\Admin\TiposFactoresController');
    Route::post('crear-tipo-factor', 'EvaluacionDesempeno\Admin\TiposFactoresController@create');
    Route::post('editar-tipo-factor', 'EvaluacionDesempeno\Admin\TiposFactoresController@edit');
    Route::get('borrar-tipo-factor/{id}', 'EvaluacionDesempeno\Admin\TiposFactoresController@destroy');
    Route::post('borrar-tipo-factor', 'EvaluacionDesempeno\Admin\TiposFactoresController@destroy');
    Route::resource('etiquetas', 'EvaluacionDesempeno\Admin\EtiquetasController');
    Route::post('crear-etiqueta', 'EvaluacionDesempeno\Admin\EtiquetasController@create');
    Route::post('editar-etiqueta', 'EvaluacionDesempeno\Admin\EtiquetasController@edit');
    Route::get('borrar-etiqueta/{id}', 'EvaluacionDesempeno\Admin\EtiquetasController@destroy');
    Route::post('borrar-etiqueta', 'EvaluacionDesempeno\Admin\EtiquetasController@destroy');
    Route::resource('factores', 'EvaluacionDesempeno\Admin\FactoresController');
    Route::resource('familias-factores', 'EvaluacionDesempeno\Admin\FamiliasFactoresController');
    Route::post('editar-familia-factores', 'EvaluacionDesempeno\Admin\FamiliasFactoresController@edit');
  Route::get('tipos-evaluacion', 'EvaluacionDesempeno\Admin\MatrizEvaluacionController@tipos_evaluacion');
  Route::post('editar-tipo-evaluacion', 'EvaluacionDesempeno\Admin\MatrizEvaluacionController@edit_tipo_evaluacion');
    Route::resource('Matriz-Evaluacion', 'EvaluacionDesempeno\Admin\MatrizEvaluacionController');
    Route::resource('Matriz-Evaluacion/{id}/edit', 'EvaluacionDesempeno\Admin\MatrizEvaluacionController@edit');
    Route::resource('importacion-evaluadores-csv', 'EvaluacionDesempeno\Admin\ImportacionEvaluadoresController');
    Route::get('create-red/{period_id}', 'EvaluacionDesempeno\Admin\ImportacionEvaluadoresController@create_red');
    Route::get('export-interaction-red/{period_id}', 'EvaluacionDesempeno\EvaluadoresEvaluadosController@export_interaction_red');
    Route::get('ponderaciones', 'EvaluacionDesempeno\ReportesController@ponderaciones');
    Route::post('ponderaciones', 'EvaluacionDesempeno\ReportesController@ponderaciones');
//});
/*
 * Fin de ruta para administrador
 */
Route::post('cambiar-contrasena/{id_user}', 'EvaluacionDesempeno\UsersController@cambiar_contrasena');
Route::get('cambiar-contrasena/{id_user}', 'EvaluacionDesempeno\UsersController@cambiar_contrasena');
Route::get('evaluaciones', 'EvaluacionDesempeno\EvaluadoresEvaluadosController@listaEvaluados');
Route::get('reporte-desempeno', 'EvaluacionDesempeno\ReportesController@reporte_desempeno');
Route::post('reporte-desempeno', 'EvaluacionDesempeno\ReportesController@reporte_desempeno');
Route::get('reporte-consolidado', 'EvaluacionDesempeno\ReportesController@reporte_consolidado');
Route::post('reporte-consolidado', 'EvaluacionDesempeno\ReportesController@reporte_consolidado');
Route::get('rol-colaborador-jefe', 'EvaluacionDesempeno\ReportesController@rol_colaborador_jefe');
Route::post('rol-colaborador-jefe', 'EvaluacionDesempeno\ReportesController@rol_colaborador_jefe');
Route::get('reporte-desempeno/{id_evaluado}', 'EvaluacionDesempeno\ReportesController@reporte_desempeno_individual');
Route::post('reporte-desempeno/{id_evaluado}', 'EvaluacionDesempeno\ReportesController@reporte_desempeno_individual');
Route::get('reporte-consolidado/{id_evaluado}', 'EvaluacionDesempeno\ReportesController@reporte_consolidado_individual');
Route::post('reporte-consolidado/{id_evaluado}', 'EvaluacionDesempeno\ReportesController@reporte_consolidado_individual');
Route::get('rol-colaborador-jefe/{id_evaluado}', 'EvaluacionDesempeno\ReportesController@rol_colaborador_jefe_individual');
Route::post('rol-colaborador-jefe/{id_evaluado}', 'EvaluacionDesempeno\ReportesController@rol_colaborador_jefe_individual');
Route::get('reporte-graficas/{id_periodo}', 'EvaluacionDesempeno\ReportesController@reporte_graficas');
Route::post('reporte-graficas/{id_periodo}', 'EvaluacionDesempeno\ReportesController@reporte_graficas');
Route::get('reporte-graficas-competencias/{id_periodo}', 'EvaluacionDesempeno\ReportesController@reporte_graficas_competencias');
Route::post('reporte-graficas-competencias/{id_periodo}', 'EvaluacionDesempeno\ReportesController@reporte_graficas_competencias');
Route::get('reporte-detalle/{id_periodo}', 'EvaluacionDesempeno\ReportesController@reporte_desempeno_completo');
Route::get('reporte-sabana/{id_periodo}', 'EvaluacionDesempeno\ReportesController@reporte_desempeno_sabana');
Route::get('reporte-objetivos/{id_periodo}', 'EvaluacionDesempeno\ReportesController@reporte_objetivos');
Route::get('reporte-usuarios-competencias-acciones/{id_periodo}', 'EvaluacionDesempeno\ReportesController@reporte_usuarios_competencias_acciones');
Route::get('reporte-competencias-acciones/{id_periodo}', 'EvaluacionDesempeno\ReportesController@reporte_competencias_acciones');
Route::get('reporte-retroalimentacion/{id_periodo}', 'EvaluacionDesempeno\ReportesController@reporte_retroalimentacion');
Route::post('guardar-comentarios-reporte', 'EvaluacionDesempeno\ReportesController@guardar_comentarios_jefe');
Route::get('evaluado/{id_evaluado}', 'EvaluacionDesempeno\EvaluadoresEvaluadosController@formatoEvaluacion');
Route::post('guardar-respuesta', 'EvaluacionDesempeno\EvaluadoresEvaluadosController@guardar_respuesta');
Route::post('guardar-respuestas', 'EvaluacionDesempeno\EvaluadoresEvaluadosController@guardar_respuestas');
Route::get('que-es-evaluacion-desempeno', function(){
    return view('evaluacion-desempeno/que-es-evaluacion-desempeno');
})->middleware('auth');
Route::get('niveles-puesto', function(){
    return view('evaluacion-desempeno/niveles-puesto');
});
Route::get('matriz-evaluaciones/{id_periodo}', 'EvaluacionDesempeno\EvaluadoresEvaluadosController@matriz_evaluaciones');
Route::get('generar-matriz', 'EvaluacionDesempeno\EvaluadoresEvaluadosController@generar_matriz');
Route::post('generar-matriz', 'EvaluacionDesempeno\EvaluadoresEvaluadosController@generar_matriz');
/*
    Fin de Routes para la Evaluacion de Desempeño
 */

/*
    Inicio de Routes para la Evaluacion de Resultados
 */
Route::resource('ejes-estrategicos', 'EvaluacionResultados\EjesEstrategicosController');
Route::get('crear-eje-estrategico', 'EvaluacionResultados\EjesEstrategicosController@create');
Route::post('crear-eje-estrategico', 'EvaluacionResultados\EjesEstrategicosController@create');
Route::get('editar-eje-estrategico/{id}', 'EvaluacionResultados\EjesEstrategicosController@edit');
Route::post('editar-eje-estrategico', 'EvaluacionResultados\EjesEstrategicosController@edit');
Route::get('borrar-eje-estrategico/{id}', 'EvaluacionResultados\EjesEstrategicosController@destroy');
Route::post('borrar-eje-estrategico', 'EvaluacionResultados\EjesEstrategicosController@destroy');
Route::post('agregar-propuesta', 'EvaluacionResultados\PropuestasController@create');
Route::post('agregar-valores-propuesta', 'EvaluacionResultados\PropuestasController@guardar_valores');
Route::post('borrar-propuesta', 'EvaluacionResultados\PropuestasController@delete');
Route::post('agregar-nota', 'EvaluacionResultados\PropuestasController@agregar_nota');
Route::post('solicitar-autorizacion', 'EvaluacionResultados\PropuestasController@solicitar_autorizacion');
Route::post('autorizar', 'EvaluacionResultados\PropuestasController@autorizar');
Route::get('importar-objetivos', 'EvaluacionResultados\PropuestasController@importar_objetivos');
Route::post('importar-objetivos', 'EvaluacionResultados\PropuestasController@importar_objetivos');
Route::post('exportar-plantilla-resultados', 'EvaluacionResultados\LogrosController@exportar_plantilla_resultados');
Route::post('importar-plantilla-resultados', 'EvaluacionResultados\LogrosController@importar_plantilla_resultados');
Route::post('guardar-logro', 'EvaluacionResultados\LogrosController@create');
Route::post('resetear-logros', 'EvaluacionResultados\LogrosController@resetear_logros');
Route::post('resetear-autorizacion', 'EvaluacionResultados\PropuestasController@resetear_autorizacion');
Route::post('solicitar-revision', 'EvaluacionResultados\LogrosController@solicitar_revision');
Route::post('revisar', 'EvaluacionResultados\LogrosController@revisar');
Route::get('reportes', 'EvaluacionResultados\ReportesController@reporte_global');
Route::post('reportes', 'EvaluacionResultados\ReportesController@reporte_global');
Route::post('reporte_individual', 'EvaluacionResultados\ReportesController@reporte_individual');
Route::post('save-promedio', 'EvaluacionResultados\ReportesController@guardar_promedio');
Route::post('importar-regiones', 'EvaluacionResultados\ReportesController@mostrar_regiones_empleados');
Route::get('importar-regiones-empleados', 'EvaluacionResultados\ReportesController@importar_regiones_empleados');
Route::get('reporte-bonos/{id_plan}', 'EvaluacionResultados\ReportesController@reporte_bonos');
Route::get('que-es-evaluacion-resultados', 'EvaluacionResultados\EvaluacionResultadosController@que_es');
Route::get('propuesta', 'EvaluacionResultados\EvaluacionResultadosController@propuesta');
Route::post('propuesta', 'EvaluacionResultados\EvaluacionResultadosController@propuesta');
Route::get('autorizacion', 'EvaluacionResultados\EvaluacionResultadosController@autorizacion');
Route::post('autorizacion', 'EvaluacionResultados\EvaluacionResultadosController@autorizacion');
Route::get('registro-logros', 'EvaluacionResultados\EvaluacionResultadosController@registroLogros');
Route::post('registro-logros', 'EvaluacionResultados\EvaluacionResultadosController@registroLogros');
Route::get('revision-logros', 'EvaluacionResultados\EvaluacionResultadosController@revisionLogros');
Route::post('revision-logros', 'EvaluacionResultados\EvaluacionResultadosController@revisionLogros');
Route::get('informe-consolidado', 'EvaluacionResultados\EvaluacionResultadosController@informe_consolidado');
Route::get('informe-consolidado/{id_empleado}', 'EvaluacionResultados\EvaluacionResultadosController@informe_consolidado_individual');
Route::post('informe-consolidado/{id_empleado}', 'EvaluacionResultados\EvaluacionResultadosController@informe_consolidado_individual');
Route::get('calculo-bono/{id_plan}', 'EvaluacionResultados\EvaluacionResultados\ReportesController@calculo_bono');
Route::get('matriz-objetivos', function(){
    return view('evaluacion-resultados.matriz-objetivos');
});
Route::get('estadistica-avance', 'EvaluacionResultados\EvaluacionResultadosController@estadisticaAvance');
Route::post('estadistica-avance', 'EvaluacionResultados\EvaluacionResultadosController@estadisticaAvance');
Route::get('bonos', 'EvaluacionResultados\EvaluacionResultadosController@bonos');
Route::post('bonos', 'EvaluacionResultados\EvaluacionResultadosController@bonos');
Route::get('detalle-bono', 'EvaluacionResultados\EvaluacionResultadosController@detalle_bono');
Route::post('detalle-bono', 'EvaluacionResultados\EvaluacionResultadosController@detalle_bono');
Route::get('ultimo-bono', 'EvaluacionResultados\EvaluacionResultadosController@ultimo_bono');
Route::post('ultimo-bono', 'EvaluacionResultados\EvaluacionResultadosController@ultimo_bono');
Route::post('agregar-recomendacion', 'EvaluacionResultados\EvaluacionResultadosController@agregar_recomendacion');
Route::get('configuracion', 'EvaluacionResultados\EvaluacionResultadosController@configuracion');
Route::post('configuracion', 'EvaluacionResultados\EvaluacionResultadosController@configuracion');
Route::post('cambiar-periodos-pasados', 'EvaluacionResultados\EvaluacionResultadosController@cambiar_valores_periodos');
Route::post('copiar-objetivos', 'EvaluacionResultados\EvaluacionResultadosController@copiar_objetivos');
Route::post('aceptar-objetivos-resultados', 'EvaluacionResultados\EvaluacionResultadosController@aceptar_objetivos_resultados');
/*
    Fin de Routes para la Evaluacion de Resultados
 */

/* BSC (Evaluación de Reultados con Objetivos Corporativos*/ 
Route::group(['prefix' => 'evaluacion-resultados'], function (){
  Route::resource('planes', 'EvaluacionResultados\PlanesController');
  Route::get('crear-plan', 'EvaluacionResultados\PlanesController@create');
  Route::post('crear-plan', 'EvaluacionResultados\PlanesController@create');
  Route::get('editar-plan/{id}', 'EvaluacionResultados\PlanesController@edit');
  Route::post('editar-plan', 'EvaluacionResultados\PlanesController@edit');
  Route::get('borrar-plan/{id}', 'EvaluacionResultados\PlanesController@destroy');
  Route::post('borrar-plan', 'EvaluacionResultados\PlanesController@destroy');
  Route::get('tipos-objetivos-corporativos', 'EvaluacionResultados\BSCController@tipos_objetivos_corporativos');
  Route::post('tipos-objetivos-corporativos', 'EvaluacionResultados\BSCController@tipos_objetivos_corporativos');
  Route::get('objetivos-corporativos', 'EvaluacionResultados\BSCController@objetivos_corporativos');
  Route::post('objetivos-corporativos', 'EvaluacionResultados\BSCController@objetivos_corporativos');
  Route::get('responsabilidades-y-compromisos', 'EvaluacionResultados\BSCController@responsabilidades_y_compromisos');
  Route::post('responsabilidades-y-compromisos', 'EvaluacionResultados\BSCController@responsabilidades_y_compromisos');
  Route::get('cumplimiento-de-compromisos', 'EvaluacionResultados\BSCController@cumplimiento_de_compromisos');
  Route::post('cumplimiento-de-compromisos', 'EvaluacionResultados\BSCController@cumplimiento_de_compromisos');
  Route::post('iniciativas', 'EvaluacionResultados\BSCController@iniciativas');
  Route::get('iniciativas', 'EvaluacionResultados\BSCController@iniciativas');
  Route::get('reporte-semaforizado', 'EvaluacionResultados\BSCController@reporte_semaforizado');
  Route::get('reporte-semaforizado/{id_plan}', 'EvaluacionResultados\BSCController@reporte_semaforizado');
  Route::get('reporte-objetivo-corporativo/{id_plan}/{id_objetivo_corporativo}', 'EvaluacionResultados\BSCController@reporte_objetivo_corporativo');
  Route::get('reporte-objetivo-responsabilidad/{id_plan}/{id_responsabilidad}', 'EvaluacionResultados\BSCController@reporte_objetivo_responsabilidad');
  Route::post('save-tipos-objetivos-corporativos', 'EvaluacionResultados\BSCController@save_tipos_objetivos_corporativos');
  Route::post('save-objetivos-corporativos', 'EvaluacionResultados\BSCController@save_objetivos_corporativos');
  Route::post('copiar-objetivos-corporativos', 'EvaluacionResultados\BSCController@copiar_objetivos_corporativos');
  Route::post('save-responsabilities', 'EvaluacionResultados\BSCController@save_responsabilities');
  Route::post('copiar-responsabilidades', 'EvaluacionResultados\BSCController@copiar_responsabilidades');
  Route::post('save-cumplimiento', 'EvaluacionResultados\BSCController@save_cumplimiento');
  Route::post('save-iniciative', 'EvaluacionResultados\BSCController@save_iniciative');
  Route::post('save-iniciative-state', 'EvaluacionResultados\BSCController@save_iniciative_state');
  Route::post('autorize-cumplimientos', 'EvaluacionResultados\BSCController@autorize_cumplimientos');
  Route::post('save-note', 'EvaluacionResultados\BSCController@save_note');
  Route::get('catalogo-objetivos', 'EvaluacionResultados\BSCController@catalogo_objetivos');
  Route::post('exportar-plantilla-catalogo-objetivos', 'EvaluacionResultados\BSCController@exportar_plantilla_catalogo_objetivos');
  Route::post('importar-catalogo-objetivos', 'EvaluacionResultados\BSCController@importar_catalogo_objetivos');
  Route::post('exportar-plantilla-objetivos-puestos', 'EvaluacionResultados\BSCController@exportar_plantilla_objetivos_puestos');
  Route::post('importar-plantilla-objetivos-puestos', 'EvaluacionResultados\BSCController@importar_plantilla_objetivos_puestos');
  Route::post('exportar-plantilla-objetivos-rfc', 'EvaluacionResultados\BSCController@exportar_plantilla_objetivos_rfc');
  Route::post('importar-plantilla-objetivos-rfc', 'EvaluacionResultados\BSCController@importar_plantilla_objetivos_rfc');
  Route::post('exportar-plantilla-bonos', 'EvaluacionResultados\BSCController@exportar_plantilla_bonos');
  Route::post('importar-plantilla-bonos', 'EvaluacionResultados\BSCController@importar_plantilla_bonos');
  Route::post('delete-tipo-objetivo-corporativo', 'EvaluacionResultados\BSCController@delete_tipo_objetivo_corporativo');
  Route::post('delete-objetivo-corporativo', 'EvaluacionResultados\BSCController@delete_objetivo_corporativo');
  Route::post('delete-responsability', 'EvaluacionResultados\BSCController@delete_responsability');
  Route::post('delete-iniciative', 'EvaluacionResultados\BSCController@delete_iniciative');
  Route::resource('users_groups', 'EvaluacionResultados\UsersGroupsController');
  Route::post('revision-director', 'EvaluacionResultados\LogrosController@revision_director');
  Route::post('rechazo-director', 'EvaluacionResultados\LogrosController@rechazo_director');
  Route::get('catalogo-objetivos/create', 'EvaluacionResultados\BSCController@crear_objetivo_catalogo');
  Route::post('catalogo-objetivos/create', 'EvaluacionResultados\BSCController@guardar_objetivo_catalogo');
  Route::get('catalogo-objetivos/{id_catalogo}/edit', 'EvaluacionResultados\BSCController@editar_objetivo_catalogo');
  Route::post('catalogo-objetivos', 'EvaluacionResultados\BSCController@actualizar_objetivo_catalogo');
  Route::get('catalogo-objetivos/{id}', 'EvaluacionResultados\BSCController@borrar_objetivo_catalogo');
  Route::get('reporte-resultados-mensuales', 'EvaluacionResultados\ReportesController@reporte_resultados_mensuales');
  Route::post('reporte-resultados-mensuales', 'EvaluacionResultados\ReportesController@reporte_resultados_mensuales');
});
/* BSC */

/*Route::group(['prefix' => 'escalafon'], function () {
    // Route::resource('/', 'Scale\ScaleController')->middleware('permission:scale_admin');
    // Route::resource('niveles', 'Scale\LevelController')->middleware('permission:scale_admin');
    // Route::resource('virtudes', 'Scale\VirtueController')->middleware('permission:scale_admin');
    Route::resource('planes', 'Scale\PlanController'); // ->middleware('permission:scale_admin');
    Route::resource('cursos', 'Scale\CourseController'); // ->middleware('permission:scale_admin');
    Route::resource('moodle', 'Scale\MoodleController'); // ->middleware('permission:scale_admin');
    Route::resource('matriculacion', 'Scale\EnrollController'); // ->middleware('permission:scale_admin')
    Route::get('overview', 'Scale\MoodleController@overview');
    Route::get('overview/history/{id?}', 'Scale\MoodleController@userHistory');
    Route::get('cursos/{id}/puestos', 'Scale\CourseController@adminCourseJobPositions');
    Route::post('cursos/actualizar_puestos/{course_id}/{job_position_id?}', 'Scale\CourseController@updateCourseJobPositions');
    Route::get('cursos/eliminar_puesto/{course_id}/{job_position_id}', 'Scale\CourseController@deleteCourseJobPositions');
    Route::post('matricular_usuario_cursos', 'Scale\CourseController@enrollUserToCourse');
    Route::post('ligar_puesto_curso', 'Scale\CourseController@storeCourseJobPosition');
    Route::post('matriculacion/resumen', 'Scale\EnrollController@summary');

    Route::get('get_moodle_courses_by_category/{category_id}/{course_id?}', 'Scale\CourseController@getMoodleCoursesByCategory');
    Route::get('get_moodle_courses_by_plan_and_job/{plan_id}/{job_id}/{user_id?}', 'Scale\PlanController@getMoodleCoursesByPlanAndJob');
    Route::get('get_moodle_courses_by_plan/{plan_id}', 'Scale\PlanController@getMoodleCoursesByPlan');
    Route::get('get_users_enrolled_by_course/{course_id}', 'Scale\MoodleController@getUsersEnrolledByCourse');
    Route::get('get_courses_by_plan/{plan_id}', 'Scale\PlanController@getCoursesByPlan');
    // Route::get('get_users_status_by_course', 'Scale\EnrollController@getUsersStatusByCourse');

    // Route::get('test', 'Scale\CourseController@test');
    
    // Route::get('mi_desempeño', 'Scale\ScaleController@userScale');
    // Route::get('mis_asesores', 'Scale\ScaleController@myEmployeesScale');
    // Route::get('obtener_cursos/{table}/{ids?}','Scale\CourseController@getCourses');
    // Route::get('editar/{id}','Scale\ScaleController@editEmployee')->middleware('permission:scale_admin');

    // Route::post('actualizar/{id}','Scale\ScaleController@updateEmployee')->middleware('permission:scale_admin');
    // Route::post('actualizar_mis_asesores','Scale\ScaleController@updateEmployees');
});*/

/******************************/
/*RUTAS PARA DENUNCIAS */
Route::resource('denuncias', 'Denuncias\DenunciasController');
Route::get('denuncias_index', 'Denuncias\DenunciasController@adminIndex');
Route::get('denuncias_gestion/{id}', 'Denuncias\DenunciasController@gestionar');
Route::post('denuncias/images', 'Denuncias\DenunciasController@images')->name('denuncias.images');
Route::post('denunciasBitacora', 'Denuncias\DenunciasController@bitacora');
/* FIN RUTAS RENUNCIAS */
/******************************/


/******************************/
/*RUTAS PARA 9BOX */
   Route::group([ 'prefix' => 'desempeno-resultados'], function () {
    Route::resource('periodos', 'NueveBox\Admin\PeriodosController');
    Route::post('crear-periodo', 'NueveBox\Admin\PeriodosController@create');
    Route::post('editar-periodo', 'NueveBox\Admin\PeriodosController@edit');
    Route::resource('borrar-periodo', 'NueveBox\Admin\PeriodosController@destroy');
    Route::resource('periodos', 'NueveBox\Admin\PeriodosController');
    Route::get('ponderaciones', 'NueveBox\ReportesController@ponderaciones');
    Route::post('ponderaciones', 'NueveBox\ReportesController@ponderaciones');
   });
  
  /*
   * Fin de ruta para administrador
   */
  
//   Route::group([ 'prefix' => '9box'], function () {
    
    Route::get('que-es', function(){
      return view('9box/que-es');
    })->middleware('auth');
    Route::get('avance-colaboradores', 'NueveBox\NueveBoxController@avance_colaboradores');
    Route::post('avance-colaboradores', 'NueveBox\NueveBoxController@avance_colaboradores');
    Route::get('plan-accion', 'NueveBox\Admin\PlanAccionController@indexAccion');
    Route::post('plan-accion', 'NueveBox\Admin\PlanAccionController@indexAccion');
    Route::post('plan-accion-box', 'NueveBox\Admin\PlanAccionController@index');
    Route::get('plan-accion/create/{period_id}', 'NueveBox\Admin\PlanAccionController@createAccion');
    Route::post('plan-accion/create', 'NueveBox\Admin\PlanAccionController@storeAccion');
    Route::get('plan-accion/{id}', 'NueveBox\Admin\PlanAccionController@editAccion');
    Route::post('plan-accion/{id}', 'NueveBox\Admin\PlanAccionController@updateAccion');
    Route::get('borrar-plan-accion/{id}', 'NueveBox\Admin\PlanAccionController@destroyAccion');
    Route::post('agregar-nota-box', 'NueveBox\Admin\PlanAccionController@agregar_comentario');
    Route::post('solicitar-aprobacion', 'NueveBox\Admin\PlanAccionController@solicitar_aprobacion');
    Route::post('aprobar-plan', 'NueveBox\Admin\PlanAccionController@aprobar_plan');
    Route::post('autorize-plan-accion', 'NueveBox\Admin\PlanAccionController@autorize_plan_accion');
    Route::post('reporte-colaboradores', 'NueveBox\Admin\PlanAccionController@reporte_colaboradores');
    Route::post('listado-colaboradores', 'NueveBox\Admin\PlanAccionController@listado_colaboradores');
//   });
  /*
      Fin de Routes para 9Box
   */