<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('checklogin/{u}/{p}', 'ApiController\LoginApiController@checklogin');
Route::post('mark_notification/{n}', 'ApiController\NotificationController@mark_notification');

Route::group(['prefix' => 'incidents'], function () {
    Route::get('save_check/{u}/{d}/{h}/{lo}/{la}/{ad}/{co?}', 'ApiController\ClockLogController@saveCheckLog');
    Route::get('check_logs/{u}/{date?}/{type?}/{until?}', 'ApiController\ClockLogController@getChecklogs');
    Route::get('check_incidents/{u}/{date?}', 'ApiController\ClockLogController@getIncidents');
    Route::get('get_hours_labor/{u}/{date?}', 'ApiController\ClockLogController@getHoursLabor');
    Route::post('change_log_status', 'ApiController\ClockLogController@changeLogStatus');
});

Route::get('img_path/{id}', 'ApiController\LoginApiController@img_path');
