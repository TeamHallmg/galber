<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnnouncementTypeIDToAnnouncementCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('announcement_categories', function (Blueprint $table) {
            $table->integer('announcement_type_id')->unsigned()->nullable()->after('announcement_category_id');
            $table->foreign('announcement_type_id')->references('id')->on('announcement_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('announcement_categories', function (Blueprint $table) {            
            $table->dropForeign(['puesto_id']);
            $table->dropColumn('puesto_id');
        });
    }
}
