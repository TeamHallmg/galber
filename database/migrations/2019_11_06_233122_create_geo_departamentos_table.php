<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoDepartamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('entidad');

        Schema::create('geo_departamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('entidad', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('abbrev');
            $table->string('country');

            $table->timestamps();
        });
        
        Schema::dropIfExists('geo_departamentos');
    }
}
