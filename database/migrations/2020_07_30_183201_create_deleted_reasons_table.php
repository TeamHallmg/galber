<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeletedReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deleted_reasons', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->nullable();
            $table->longText('reason');
            $table->longText('comment')->nullable();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('deleted_reason_id');
            $table->string('deleted_reason_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deleted_reasons');
    }
}
