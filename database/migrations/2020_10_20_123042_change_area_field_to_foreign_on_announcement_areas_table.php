<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAreaFieldToForeignOnAnnouncementAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('announcement_areas', function (Blueprint $table) {
            $table->dropColumn('area');
            $table->integer('announcement_state_id')->unsigned()->after('announcement_id');
            $table->foreign('announcement_state_id')->references('id')->on('announcement_states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('announcement_areas', function (Blueprint $table) {
            $table->string('area')->nullable();
        });
    }
}
