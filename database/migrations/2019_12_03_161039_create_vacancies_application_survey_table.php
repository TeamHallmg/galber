<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacanciesApplicationSurveyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancies_application_survey', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('postulante_id')->unsigned();
            $table->foreign('postulante_id')->references('id')->on('vacancies_postulants');

            $table->text('question1')->nullable();            
            $table->text('question2')->nullable();            
            $table->text('question3')->nullable();            
            $table->text('question4')->nullable();            
            $table->text('question5')->nullable();            
            $table->text('question6')->nullable();            
            $table->text('question7')->nullable();            
            $table->text('question8')->nullable();            
            $table->text('question9')->nullable();            
            $table->text('question10')->nullable();            

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancies_application_survey');
    }
}
