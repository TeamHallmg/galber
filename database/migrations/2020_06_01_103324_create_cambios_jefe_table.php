<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCambiosJefeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cambios_jefe', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_user', 10)->nullable();
            $table->string('rfc', 15)->nullable();
            $table->string('anterior_jefe', 10)->nullable();
            $table->string('nuevo_jefe', 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cambios_jefe');
    }
}
