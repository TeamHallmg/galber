<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BenefitTimeDescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefit_time_description', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('seniority_type_of_time', ['day', 'week', 'month', 'year'])->nullable()->comment('Tipo para el tiempo en el que se buscara que el empleado tenga más antigüedad en la empresa');
            $table->integer('seniority_quantity')->unsigned()->nullable()->comment('Total que necesita del tipo para poder utilizar la incidencia, ejemplo, al menos 6 meses en la empresa');
            $table->enum('period_use_type_of_time', ['hour', 'day', 'week', 'month', 'year'])->nullable()->comment('El periodo en el que se podan usar');
            $table->integer('period_use_quantity')->unsigned()->nullable()->comment('Total de veces que se va a solicitar por periodo, Ejemplo, 2 goces de sueldo al año');
            //Estos dos campos de abajo en combinación deben de ser iguales o menor que la combinación de "period_use_type_of_time" y "period_use_quantity"
            $table->enum('use_max_type_of_time', ['hour', 'day', 'week', 'month', 'year'])->nullable()->comment('Periodo maximo a usar dentro del periodo definido por "period_use_type_of_time", debe de ser menor');
            $table->integer('use_max_quantity_per_time')->unsigned()->nullable()->comment('Total de veces que se pueden solicitar, debe de ser menor a "period_use_quantity"');
            $table->integer('benefit_id')->unsigned();
            $table->foreign('benefit_id')->references('id')->on('benefits');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benefit_time_description');
    }
}
