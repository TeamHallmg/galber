<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePonderacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ponderaciones', function (Blueprint $table) {
            $table->integer('id_periodo');
            $table->integer('id_nivel_puesto');
            $table->integer('jefe');
            $table->integer('autoevaluacion');
            $table->integer('colaborador');
            $table->integer('par');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ponderaciones');
    }
}
