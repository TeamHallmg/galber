<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnumFieldsToClocklogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE clock_logs MODIFY COLUMN `status` ENUM('pending','accepted','rejected','canceled','processed')");
        DB::statement("ALTER TABLE clock_logs MODIFY COLUMN `from` ENUM('web','movil', 'system')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE clock_logs MODIFY COLUMN `status` ENUM('pending','accepted','rejected','canceled')");
        DB::statement("ALTER TABLE clock_logs MODIFY COLUMN `from` ENUM('web','movil')");
    }
}
