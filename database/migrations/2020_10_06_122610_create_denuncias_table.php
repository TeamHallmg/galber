<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDenunciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('denuncias', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('denunciante_id')->unsigned()->nullable();
            $table->foreign('denunciante_id')->references('id')->on('users');

            $table->string('tipo', 100)->nullable();
            $table->string('empresa_hechos', 100)->nullable();
            $table->string('tipo_usuario', 100)->nullable();
            $table->string('lugar_hechos', 100)->nullable();
            $table->date('fecha_hechos')->nullable();
            $table->text('personas_involucradas')->nullable();
            $table->text('denuncia')->nullable();

            $table->string('nombre_externo', 100)->nullable();
            $table->string('email_externo', 100)->nullable();
            $table->string('ciudad_externo', 100)->nullable();
            $table->string('puesto_externo', 100)->nullable();
            $table->string('clasificacion', 255)->nullable();
			$table->string('area_corresponde',255)->nullable();
            $table->string('estatus', 100)->nullable();
            $table->string('importancia', 100)->nullable();
			$table->text('comentarios')->nullable();
            
            $table->integer('responsable_id')->unsigned()->nullable();
            $table->foreign('responsable_id')->references('id')->on('users');

			$table->date('fecha_seguimiento');
            $table->date('fecha_cierre');

            $table->string('afectan_a', 100)->nullable();
            $table->string('afectan_a_categoria', 100)->nullable();
            $table->text('debe_ser')->nullable();
            $table->text('como_se_hace')->nullable();
            $table->text('que_sugiere')->nullable();
            $table->text('otros')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('denuncias');
    }
}
