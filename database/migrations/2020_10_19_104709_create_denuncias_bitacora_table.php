<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDenunciasBitacoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('denuncias_bitacora', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('denuncia_id')->unsigned()->nullable();
            $table->foreign('denuncia_id')->references('id')->on('denuncias');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->text('comentarios')->nullable();
            $table->timestamp('fecha_hora')->nullable();
            $table->string('accion', 100)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('denuncias_bitacora');
    }
}
