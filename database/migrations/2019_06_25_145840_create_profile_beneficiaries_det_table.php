<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileBeneficiariesDetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_beneficiaries_det', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('profile_beneficiaries_id')->unsigned();
            $table->foreign('profile_beneficiaries_id')->references('id')->on('profile_beneficiaries');

            $table->string('beneficiarie_name', 100)->nullable();
            $table->date('date_birth_beneficiarie')->nullable();
            $table->string('file_birth_certificate_beneficiarie', 255)->nullable();
            $table->string('type_beneficiarie', 10)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_beneficiaries_det');
    }
}
