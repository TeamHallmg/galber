<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacanciesPostulantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancies_postulants', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned()->default('0');
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('vacante_id')->unsigned();
            $table->foreign('vacante_id')->references('id')->on('vacancies');
            $table->integer('profile_id')->unsigned()->default('0');
            $table->foreign('profile_id')->references('id')->on('profile');

            $table->string('intentos_postulacion', 2)->nullable();
            $table->string('aceptacion_postulacion', 2)->nullable();
            $table->text('motivo')->nullable();
            $table->text('motivo_cierre_vacante')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancies_postulants');
    }
}
