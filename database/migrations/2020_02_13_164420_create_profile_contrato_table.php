<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileContratoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_contrato', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('profile_id')->unsigned();
            $table->foreign('profile_id')->references('id')->on('profile');

            $table->string('nacionalidad', 191)->nullable();
            $table->string('profesion', 191)->nullable();
            $table->text('sede_principal')->nullable();
            $table->text('sede_secundaria')->nullable();
            $table->string('tipo_contrato', 191)->nullable();
            $table->string('horario', 191)->nullable();
            $table->string('salario', 191)->nullable();
            $table->string('salario_base', 191)->nullable();
            $table->string('bono', 191)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_contrato');
    }
}
