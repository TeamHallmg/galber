<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDenunciasFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('denuncias_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('denuncia_id')->unsigned()->nullable();
            $table->foreign('denuncia_id')->references('id')->on('denuncias');
            $table->string('type');
            $table->string('filename')->nullable();
            $table->string('name')->nullable();
            $table->string('extension')->nullable();
            $table->string('files_type');
            $table->unsignedInteger('files_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('denuncias_files');
    }
}
