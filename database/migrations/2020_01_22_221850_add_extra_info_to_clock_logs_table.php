<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraInfoToClockLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clock_logs', function (Blueprint $table) {
            $table->string('request_ip', 50)->nullable()->after('comment');
            $table->enum('from', ['web', 'movil'])->nullable()->after('request_ip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clock_logs', function (Blueprint $table) {
            $table->dropColumn(['request_ip', 'from']);
        });
    }
}
