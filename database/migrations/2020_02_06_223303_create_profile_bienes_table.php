<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileBienesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_bienes', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('id_tipo_bien')->unsigned();
            $table->foreign('id_tipo_bien')->references('id')->on('tipo_bienes');

            $table->integer('profile_id')->unsigned();
            $table->foreign('profile_id')->references('id')->on('profile');

            $table->integer('estatus')->unsigned();
            $table->date('fecha_entrega')->nullable();
            $table->string('codigo', 191)->nullable();
            $table->string('url_comprobante', 191)->nullable();
            $table->string('motivo_desincorporacion', 191)->nullable();
            $table->date('fecha_desincorporacion')->nullable();
            $table->text('detalle_desincorporacion')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_bienes');
    }
}
