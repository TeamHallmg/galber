<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToClocklogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clock_logs', function (Blueprint $table) {
            $table->string('compare_time')->after('from')->nullable();
            $table->enum('arrive_status', ['early', 'on-time', 'tolerance', 'late', 'ok'])->after('compare_time')->nullable();
            $table->enum('type', ['in', 'out'])->after('arrive_status')->nullable();
            $table->enum('status', ['pending', 'accepted', 'rejected'])->default('pending')->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clock_logs', function (Blueprint $table) {
            $table->dropColumn(['compare_time', 'arrive_status', 'type', 'status']);
        });
    }
}
