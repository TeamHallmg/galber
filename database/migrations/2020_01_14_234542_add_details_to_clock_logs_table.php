<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDetailsToClockLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clock_logs', function (Blueprint $table) {
            $table->decimal('longitude', 11, 8)->after('user_id')->nullable();
            $table->decimal('latitude', 10, 8)->after('longitude')->nullable();
            $table->string('address')->after('latitude')->nullable();
            $table->string('comment')->after('address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clock_logs', function (Blueprint $table) {
            $table->dropColumn(['longitude','latitude','address','comment']);
        });
    }
}
