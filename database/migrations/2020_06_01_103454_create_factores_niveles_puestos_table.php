<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactoresNivelesPuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factores_niveles_puestos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_factor');
            $table->integer('id_nivel_puesto');
            $table->integer('nivel_esperado');
            $table->string('peso', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factores_niveles_puestos');
    }
}
