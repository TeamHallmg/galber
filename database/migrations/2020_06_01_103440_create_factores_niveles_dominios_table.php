<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactoresNivelesDominiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factores_niveles_dominios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_factor');
            $table->text('nivel_dominio');
            $table->integer('calificacion');
            $table->string('etiqueta', 4);
            $table->enum('status', ['Activado','Desactivado']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factores_niveles_dominios');
    }
}
