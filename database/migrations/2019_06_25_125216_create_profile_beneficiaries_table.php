<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileBeneficiariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_beneficiaries', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('profile_id')->unsigned();
            $table->foreign('profile_id')->references('id')->on('profile');

            $table->string('spouse_name', 100)->nullable();
            $table->date('date_marriage')->nullable();
            $table->string('file_marriage', 255)->nullable();
            $table->string('father_name', 100)->nullable();
            $table->date('date_birth_father')->nullable();
            $table->string('file_birth_certificate_father', 255)->nullable();
            $table->string('mother_name', 100)->nullable();
            $table->date('date_birth_mother')->nullable();
            $table->string('file_birth_certificate_mother', 255)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_beneficiaries');
    }
}
