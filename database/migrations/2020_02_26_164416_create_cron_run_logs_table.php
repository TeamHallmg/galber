<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCronRunLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cron_run_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->comment('Día en el que el cron se ejecuto');
            $table->enum('cron_type', ['vacation_balance', 'missing_clocklog', 'create_absents'])
            ->comment('Tipos de crones que se pueden ejecutar');
            $table->enum('frequence', ['daily', 'weekly', 'monthly', 'yearly'])
            ->comment('Tipo de frecuencia en la que el cron se ejecuto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cron_run_logs');
    }
}
