<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScaleModelDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scale_model_data', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('model_name', ['JobPosition', 'User', 'Course']);
            $table->integer('model_id')->unsigned()->nullable();
            $table->enum('scale_data_type', ['virtue', 'course', 'moodle_course', 'plan']);
            $table->integer('scale_data_id')->unsigned()->nullable();
            $table->double('score', 8, 2)->nullable()->comment('Puntaje');
            $table->date('date_start')->nullable(); // para después
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scale_model_data');
    }
}
