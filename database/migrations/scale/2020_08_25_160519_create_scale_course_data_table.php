<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScaleCourseDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scale_course_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scale_course_id')->unsigned()->nullable();
            $table->foreign('scale_course_id')->references('id')->on('scale_courses');
            $table->enum('type', ['trainer', 'competence', 'extern_rfc']);
            $table->integer('type_id')->unsigned()->nullable();
            $table->string('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scale_course_data');
    }
}
