<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryToScaleCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scale_courses', function (Blueprint $table) {
            $table->integer('category_id')->unsigned()->nullable()->comment('!= null, si es de Moodle')->after('id');
            $table->string('category')->nullable()->after('category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scale_courses', function (Blueprint $table) {
            $table->dropColumn(['category_id', 'category']);
        });
    }
}
