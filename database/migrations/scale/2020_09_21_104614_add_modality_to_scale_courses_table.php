<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModalityToScaleCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scale_courses', function (Blueprint $table) {
            $table->enum('modality', ['face-to-face', 'virtual', 'blended'])->default('virtual')->after('type')->comment('Modalidad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scale_courses', function (Blueprint $table) {
            $table->dropColumn(['modality']);
        });
    }
}
