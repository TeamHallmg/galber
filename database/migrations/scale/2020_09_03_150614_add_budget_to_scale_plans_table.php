<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBudgetToScalePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scale_plans', function (Blueprint $table) {
            $table->double('budget', 16, 2)->nullable()->after('end_date')->comment('Presupuesto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scale_plans', function (Blueprint $table) {
            $table->dropColumn(['budget']);
        });
    }
}
