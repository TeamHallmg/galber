<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScaleJobpositionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scale_jobposition', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scale_course_id')->unsigned()->nullable();
            $table->foreign('scale_courses_id')->references('id')->on('scale_courses');
            $table->integer('job_position_id')->unsigned()->nullable();
            $table->foreign('job_position_id')->references('id')->on('job_positions');
            $table->integer('calificacion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scale_jobposition');
    }
}
