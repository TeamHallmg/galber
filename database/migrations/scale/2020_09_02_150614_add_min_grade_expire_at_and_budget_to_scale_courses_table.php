<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinGradeExpireAtAndBudgetToScaleCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scale_courses', function (Blueprint $table) {
            $table->integer('min_grade')->nullable()->after('name');
            $table->date('expire_at')->nullable()->after('validity');
            $table->double('budget', 16, 2)->nullable()->after('expire_at')->comment('Presupuesto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scale_courses', function (Blueprint $table) {
            $table->dropColumn(['min_grade','expire_at', 'budget']);
        });
    }
}
