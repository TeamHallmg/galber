<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDurationToScaleCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scale_courses', function (Blueprint $table) {
            $table->integer('duration')->nullable()->after('validity')->comment('Duración');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scale_courses', function (Blueprint $table) {
            $table->dropColumn(['duration']);
        });
    }
}
