<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtrasToScaleModelDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scale_model_data', function (Blueprint $table) {
            $table->integer('extra1')->nullable()->after('date_start');
            $table->integer('extra2')->nullable()->after('extra1');
            $table->string('extra3')->nullable()->after('extra2');
            $table->string('extra4')->nullable()->after('extra3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scale_model_data', function (Blueprint $table) {
            $table->dropColumn(['extra1', 'extra2', 'extra4', 'extra4']);
        });
    }
}
