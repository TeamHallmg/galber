<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScaleCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scale_courses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('moodle_id')->unsigned()->nullable()->comment('!= null, si es de Moodle');
            $table->string('name');
            $table->integer('value')->nullable();
            $table->integer('validity')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scale_courses');
    }
}
