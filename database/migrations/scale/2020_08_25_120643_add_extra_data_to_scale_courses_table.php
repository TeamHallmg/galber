<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraDataToScaleCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scale_courses', function (Blueprint $table) {
            $table->string('code')->nullable()->after('moodle_id');
            $table->enum('type', ['I', 'E'])->default('I')->after('code'); // Distintivo de Interno y Externo
            $table->integer('priority')->default(0)->after('name');
            $table->string('institution')->nullable()->after('priority');
            $table->string('instructor_name')->nullable()->after('institution');
            $table->longText('observations')->nullable()->after('instructor_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scale_courses', function (Blueprint $table) {
            $table->dropColumn(['code', 'type', 'priority', 'institution', 'instructor_name', 'observations']);
        });
    }
}
