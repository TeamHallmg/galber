<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompressedWeekInCronRunLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE cron_run_logs MODIFY COLUMN `cron_type` ENUM('vacation_balance','missing_clocklog','create_absents','clocklogs_with_time_incidents','compressed_week')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE cron_run_logs MODIFY COLUMN `cron_type` ENUM('vacation_balance','missing_clocklog','create_absents','clocklogs_with_time_incidents')");
    }
}
