<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancies', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('reclutador_id')->unsigned();
            $table->foreign('reclutador_id')->references('id')->on('vacancies_recruiters');
            $table->integer('requisicion_id')->unsigned();
            $table->foreign('requisicion_id')->references('id')->on('requisitions');
            $table->integer('paquete_id');

            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_fin')->nullable();
            $table->date('fecha_cierre')->nullable();
            $table->text('comentarios')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancies');
    }
}
