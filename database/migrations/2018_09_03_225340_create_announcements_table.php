<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcements', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('title');
            // $table->text('description')->nullable();
            // $table->string('link');
            // $table->string('file');
            $table->dateTime('starts');
            $table->dateTime('ends');
            $table->boolean('active');
            $table->integer('approved_by_user_id')->unsigned()->nullable();
            $table->foreign('approved_by_user_id')->references('id')->on('users');
            $table->integer('created_by_user_id')->unsigned();
            $table->foreign('created_by_user_id')->references('id')->on('users');
            $table->integer('announcement_type_id')->unsigned();
            $table->foreign('announcement_type_id')->references('id')->on('announcement_types');
            $table->integer('announcement_category_id')->unsigned()->nullable();
            $table->foreign('announcement_category_id')->references('id')->on('announcement_categories');
            $table->integer('region_id')->unsigned()->nullable();
            $table->foreign('region_id')->references('id')->on('regions');
            $table->integer('view_id')->unsigned();
            $table->foreign('view_id')->references('id')->on('views');
            $table->timestamps();
        });

        // Schema::table('announcements', function($table) {
        //     $table->foreign('approved_by_user_id')->references('id')->on('users');
        //     $table->foreign('created_by_user_id')->references('id')->on('users');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcements');
    }
}
