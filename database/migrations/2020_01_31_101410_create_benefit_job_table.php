<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenefitJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefit_job', function (Blueprint $table) {
            $table->integer('benefit_id')->unsigned();
            $table->foreign('benefit_id')->references('id')->on('benefits');
            $table->integer('job_position_id')->unsigned();
            $table->foreign('job_position_id')->references('id')->on('job_positions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benefit_job');
    }
}
