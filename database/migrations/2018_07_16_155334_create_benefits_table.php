<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',10);
            $table->string('shortname',10);
            $table->string('context',10);
            $table->string('name',255);
            $table->enum('type', ['time', 'pending', 'day']);
            $table->boolean('continuous')->default(0);
            $table->boolean('cronjob')->default(0);
            $table->boolean('report')->default(0);
            $table->boolean('remove')->default(0);
            $table->boolean('blocked')->default(0);
            $table->integer('relationship')->default(0);
            $table->integer('days')->default(0);
            $table->string('frequency',10)->default("");
            $table->string('group',10)->default("");
            $table->string('retype',10)->default("");
            $table->date('exec')->nullable();
            $table->string('gender',1)->default('x');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benefits');
    }
}
