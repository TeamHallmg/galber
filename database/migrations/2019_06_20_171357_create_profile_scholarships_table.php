<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileScholarshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_scholarships', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('profile_id')->unsigned();
            $table->foreign('profile_id')->references('id')->on('profile');

            $table->string('studio', 45)->nullable();
            $table->string('career', 60)->nullable();
            $table->string('school', 60)->nullable();
            $table->date('date_begin')->nullable();
            $table->date('date_end')->nullable();
            $table->string('speciality', 60)->nullable();
            $table->boolean('voucher')->nullable()->default(false);
            $table->string('file_studio', 100)->nullable();
            $table->string('type', 100)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_scholarships');
    }
}
