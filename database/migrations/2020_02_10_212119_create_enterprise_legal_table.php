<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterpriseLegalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise_legal', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('id_empresa')->unsigned();
            $table->foreign('id_empresa')->references('id')->on('enterprises');

            $table->string('dpi', 191)->nullable();
            $table->string('nombre', 191)->nullable();
            $table->string('nacionalidad', 191)->nullable();
            $table->date('fecha_nac')->nullable();
            $table->string('sexo', 191)->nullable();
            $table->string('edo_civil', 191)->nullable();
            $table->string('profesion', 191)->nullable();
            $table->string('puesto', 191)->nullable();
            $table->string('notario', 191)->nullable();
            $table->date('nombramiento')->nullable();
            $table->string('numero_registro', 191)->nullable();
            $table->string('folio', 191)->nullable();
            $table->string('libro', 191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise_legal');
    }
}
