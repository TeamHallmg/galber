<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimePeriodsToBenefitTimeDescriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('benefit_time_description', function (Blueprint $table) {
            DB::statement("ALTER TABLE benefit_time_description MODIFY COLUMN `period_use_type_of_time` ENUM('hour','day','week','week-hour','month','month-hour','year','year-hour','half-year','half-year-hour')");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('benefit_time_description', function (Blueprint $table) {
            DB::statement("ALTER TABLE benefit_time_description MODIFY COLUMN `period_use_type_of_time` ENUM('hour','day','week','month','year','half-year')");
        });
    }
}
