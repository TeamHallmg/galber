<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoBienesDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_bienes_detalle', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('id_tipo_bien')->unsigned();
            $table->foreign('id_tipo_bien')->references('id')->on('tipo_bienes');

            $table->integer('id_variable')->unsigned();
            $table->foreign('id_variable')->references('id')->on('variables');

            $table->integer('estatus')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_bienes_detalle');
    }
}
