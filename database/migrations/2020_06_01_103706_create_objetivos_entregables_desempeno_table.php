<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjetivosEntregablesDesempenoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objetivos_entregables_desempeno', function (Blueprint $table) {
            $table->integer('id_evaluado');
            $table->integer('id_jefe');
            $table->integer('id_periodo');
            $table->text('objetivo');
            $table->text('accion');
            $table->text('entregable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objetivos_entregables_desempeno');
    }
}
