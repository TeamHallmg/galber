<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWatchedToEnumFieldOnAnnouncementUserInteractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE announcement_user_interactions CHANGE COLUMN action action ENUM('like','favorite', 'watched') NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE announcement_user_interactions CHANGE COLUMN action action ENUM('like','favorite') NOT NULL");
    }
}
