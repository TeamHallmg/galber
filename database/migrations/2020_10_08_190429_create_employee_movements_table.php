<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('type')->comment('Tipo Movimiento');
            $table->integer('old_job_position_id')->unsigned()->comment('Puesto Anterior');
            $table->foreign('old_job_position_id')->references('id')->on('job_positions');
            $table->integer('new_job_position_id')->unsigned()->comment('Nuevo Puesto');
            $table->foreign('new_job_position_id')->references('id')->on('job_positions');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_movements');
    }
}
