<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEditableFlagToBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('benefits', function (Blueprint $table) {
            $table->boolean('week_calendar')->default(false)->after('file_required')->comment('Bandera para especificar si se trabajara con el calendario semanal');
            $table->boolean('editable')->default(true)->after('week_calendar')->comment('Bandera para permitir que editen el beneficio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('benefits', function (Blueprint $table) {
            $table->dropColumn('editable');
            $table->dropColumn('week_calendar');
        });
    }
}
