<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluadorEvaluadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluador_evaluado', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_evaluador');
            $table->integer('id_evaluado');
            $table->tinyInteger('tipo_evaluacion')->default(0);
            $table->enum('status', ['No Iniciada','Iniciada','Terminada']);
            $table->integer('id_periodo');
            $table->timestamps();
            $table->unique(['id_evaluador', 'id_evaluado', 'id_periodo']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluador_evaluado');
    }
}
