<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileHealthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_health', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('profile_id')->unsigned();
            $table->foreign('profile_id')->references('id')->on('profile');

            $table->string('imss', 100)->nullable();
            $table->string('policy', 255)->nullable();
            $table->string('blood_type', 3)->nullable();
            $table->string('allergies', 255)->nullable();
            $table->string('current_condition', 255)->nullable();
            $table->text('family_background')->nullable();
            $table->text('cx')->nullable();
            $table->text('comments')->nullable();
            $table->string('contact1', 255)->nullable();
            $table->string('phone1', 25)->nullable();
            $table->string('relationship1', 60)->nullable();
            $table->string('contact2', 255)->nullable();
            $table->string('phone2', 25)->nullable();
            $table->string('relationship2', 60)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_health');
    }
}
