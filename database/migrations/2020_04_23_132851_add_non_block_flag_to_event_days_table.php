<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNonBlockFlagToEventDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_days', function (Blueprint $table) {
            $table->boolean('block')->default(true)->after('date')->comment('Esta bandera es para utilizarla como referencia para ver si bloquea días evitando/permitiendo crear solicitud y checadas en el sistema');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_days', function (Blueprint $table) {
            $table->dropColumn('block');
        });
    }
}
