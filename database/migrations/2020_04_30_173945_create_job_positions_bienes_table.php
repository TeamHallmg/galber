<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobPositionsBienesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_positions_bienes', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('id_tipo_bien')->unsigned();
            $table->foreign('id_tipo_bien')->references('id')->on('tipo_bienes');

            $table->integer('id_job_positions')->unsigned();
            $table->foreign('id_job_positions')->references('id')->on('job_positions');

            $table->integer('estatus')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_positions_bienes');
    }
}
