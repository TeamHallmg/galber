<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('application')->nullable();

            $table->string('name', 60)->nullable();
            $table->string('surname_father', 30)->nullable();
            $table->string('surname_mother', 30)->nullable();
            $table->string('cellphone', 25)->nullable();
            $table->string('phone', 25)->nullable();
            $table->string('email', 45)->nullable();
            $table->string('gender', 10)->nullable();
            $table->date('date_birth')->nullable();
            $table->string('rfc', 45)->nullable();
            $table->string('know_vacancy', 45)->nullable();
            $table->text('know_vacancy_other')->nullable();
            $table->string('image', 255)->nullable();
            $table->string('file_cv', 255)->nullable();
            $table->string('irtra', 255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile');
    }
}
