<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ViewsSeeder::class,
            AnnouncementTypesSeeder::class,
            AnnouncementAttributesSeeder::class,
            AnnouncementFormSeeder::class,
            AnnouncementTypesPerViewSeeder::class,
        ]);

        $this->call([
            BenefitsSeeder::class,
        ]);

        $this->call([
            ScheduleSeeder::class,
            RegionsSeeder::class,
            WorkshiftFixedSeeder::class,
        ]);
    }
}
