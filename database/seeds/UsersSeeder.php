<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	[
				'first_name' => 'Admin',
				'last_name' => 'Soporte',
				'email' => 'soporte@hallmg.com',
				'role' => 'admin',
	            'password' => Hash::make('admin')
        	]
    	]);
    }
}
