<?php

use Illuminate\Database\Seeder;
use App\Models\Announcement\AnnouncementType;

class AnnouncementTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AnnouncementType::firstOrCreate([
            'name' => 'banner',
            'show_name' => 'Banner',
            'view_file' => 'banner',
            'max_quantity_showed' => 1,
            'max_quantity_stored' => 5
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'carrousel',
            'show_name' => 'Carrousel',
            'view_file' => 'carrousel',
            'max_quantity_showed' => 5,
            'max_quantity_stored' => 10
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'mosaico',
            'show_name' => 'Mosaico',
            'view_file' => 'mosaico',
            'max_quantity_showed' => 9,
            'max_quantity_stored' => 20
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'tabla_anuncios',
            'show_name' => 'Tabla anuncios',
            'view_file' => 'tabla_anuncios',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null,
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'columna_anuncios',
            'show_name' => 'Columna anuncios',
            'view_file' => 'columna_anuncios',
            'max_quantity_showed' => 5,
            'max_quantity_stored' => 10
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'cuatro_columnas',
            'show_name' => 'Cuatro columnas',
            'view_file' => 'cuatro_columnas',
            'max_quantity_showed' => 4,
            'max_quantity_stored' => 10,
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'dynamic_categories',
            'show_name' => 'Por categorias',
            'view_file' => 'dynamic_categories',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null,
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'gallery',
            'show_name' => 'Galería',
            'view_file' => 'gallery',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null,
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'carrousel_secondary',
            'show_name' => 'Carrousel Secundario',
            'view_file' => 'carrousel_secondary',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null,
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'banner_panel',
            'show_name' => 'Banner Panel',
            'view_file' => 'banner_panel',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'cards',
            'show_name' => 'Tarjetas',
            'view_file' => 'cards',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'panels',
            'show_name' => 'Paneles ',
            'view_file' => 'panels',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'prestaciones_sindicalizados',
            'show_name' => 'Prestaciones Sindicalizado',
            'view_file' => 'prestaciones',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'prestaciones_confianza',
            'show_name' => 'Prestaciones personal de Confianza',
            'view_file' => 'prestaciones',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'prestaciones',
            'show_name' => 'Prestaciones',
            'view_file' => 'prestaciones',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'convenios',
            'show_name' => 'Convenios',
            'view_file' => 'convenios',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'operaciones',
            'show_name' => 'Formatos en General',
            'view_file' => 'operaciones',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'mosaico_espacios',
            'show_name' => 'Panel de Imagenes',
            'view_file' => 'mosaico_espacios',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'image',
            'show_name' => 'Imagen',
            'view_file' => 'image',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'dos_columnas',
            'show_name' => 'Dos Columnas',
            'view_file' => 'dos_columnas',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'imagenes_organigrama',
            'show_name' => 'Organigramas',
            'view_file' => 'imagenes_organigrama',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'notificaciones',
            'show_name' => 'Eventos / Notificaciones',
            'view_file' => 'notificaciones',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'festividades',
            'show_name' => 'Eventos / Festividades',
            'view_file' => 'festividades',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'reconocimientos',
            'show_name' => 'Eventos / Reconocimientos',
            'view_file' => 'reconocimientos',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'aseguramiento_calidad_formatos',
            'show_name' => 'Formatos en General',
            'view_file' => 'aseguramiento_calidad_formatos',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'tabla_programas',
            'show_name' => 'Programas',
            'view_file' => 'tabla_programas',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'tabla_diagramas',
            'show_name' => 'Diagramas',
            'view_file' => 'tabla_diagramas',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
        AnnouncementType::firstOrCreate([
            'name' => 'formatos_general',
            'show_name' => 'Formatos en General',
            'view_file' => 'formatos_general',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);
    }
}
