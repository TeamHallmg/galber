<?php

use Illuminate\Database\Seeder;
use App\Models\Schedule;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schedule::updateOrCreate(['shift' => 'Central'], ['in' => '09:00', 'out' => '18:00']);
        Schedule::updateOrCreate(['shift' => 'FT2-01'], ['in' => '08:30', 'out' => '17:30']);
        Schedule::updateOrCreate(['shift' => 'FT2-02'], ['in' => '09:00', 'out' => '16:30']);
        Schedule::updateOrCreate(['shift' => 'FT2-03'], ['in' => '09:30', 'out' => '18:30']);
        Schedule::updateOrCreate(['shift' => 'PT'], ['in' => '09:00', 'out' => '14:00']);
        Schedule::updateOrCreate(['shift' => 'PT2-01'], ['in' => '08:30', 'out' => '13:30']);
        Schedule::updateOrCreate(['shift' => 'PT2-02'], ['in' => '09:00', 'out' => '14:00']);
        Schedule::updateOrCreate(['shift' => 'PT2-03'], ['in' => '09:30', 'out' => '14:30']);
        Schedule::updateOrCreate(['shift' => 'PT2-04'], ['in' => '10:00', 'out' => '15:00']);
        Schedule::updateOrCreate(['shift' => 'PT2-05'], ['in' => '10:30', 'out' => '15:30']);
        Schedule::updateOrCreate(['shift' => 'PT2-06'], ['in' => '11:00', 'out' => '16:00']);
        Schedule::updateOrCreate(['shift' => 'PT2-07'], ['in' => '11:30', 'out' => '16:30']);
        Schedule::updateOrCreate(['shift' => 'PT2-08'], ['in' => '12:00', 'out' => '17:00']);
        Schedule::updateOrCreate(['shift' => 'PT2-09'], ['in' => '12:30', 'out' => '17:30']);
        Schedule::updateOrCreate(['shift' => 'PT2-10'], ['in' => '13:00', 'out' => '18:00']);
        Schedule::updateOrCreate(['shift' => 'PT2-11'], ['in' => '13:30', 'out' => '18:30']);
        Schedule::updateOrCreate(['shift' => 'TC'], ['in' => '09:00', 'out' => '18:00']);
        Schedule::updateOrCreate(['shift' => 'FT1'], ['in' => '09:00', 'out' => '18:00']);
    }
}
