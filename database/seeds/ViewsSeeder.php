<?php

use Illuminate\Database\Seeder;
use App\Models\Announcement\View;

class ViewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        View::firstOrCreate(['name' => 'home']);
        View::firstOrCreate(['name' => 'example']);
        View::firstOrCreate(['name' => 'home']);
        View::firstOrCreate(['name' => 'eventos']);
        View::firstOrCreate(['name' => 'tablero']);
        View::firstOrCreate(['name' => 'mistica-guia']);
        View::firstOrCreate(['name' => 'filosofia']);
        View::firstOrCreate(['name' => 'cumpleaños']);
        View::firstOrCreate(['name' => 'proposito-mision-vision']);
    }
}

