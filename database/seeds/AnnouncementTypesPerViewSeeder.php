<?php

use Illuminate\Database\Seeder;
use App\Models\Announcement\View;
use App\Models\Announcement\AnnouncementType;
use App\Models\Announcement\AnnouncementTypePerView;

class AnnouncementTypesPerViewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = AnnouncementType::get()->pluck('id', 'name')->toArray();
        $views = View::get()->pluck('id', 'name')->toArray();
        
        AnnouncementTypePerView::firstOrCreate([
            'announcement_type_id' => $types['carrousel'],
            'view_id' => $views['home'],
        ]);
        AnnouncementTypePerView::firstOrCreate([
            'announcement_type_id' => $types['mosaico'],
            'view_id' => $views['home'],
        ]);
        AnnouncementTypePerView::firstOrCreate([
            'announcement_type_id' => $types['panels'],
            'view_id' => $views['filosofia'],
        ]);
        AnnouncementTypePerView::firstOrCreate([
            'announcement_type_id' => $types['banner'],
            'view_id' => $views['cumpleaños'],
        ]);
        AnnouncementTypePerView::firstOrCreate([
            'announcement_type_id' => $types['columna_anuncios'],
            'view_id' => $views['proposito-mision-vision'],
        ]);
    }
}
