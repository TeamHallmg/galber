<?php

use App\User;
use Illuminate\Database\Seeder;
use App\Models\Vacations\Balances;

class BalancesForAllFakeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::has('employee')
        ->doesntHave('balances')
        ->pluck('id')
        ->toArray();

        foreach($users as $user){
            Balances::create([
                'year' => date('Y'),
                'amount' => 0,
                'pending' => 10,
                'benefit_id' => 5,
                'user_id' => $user,
                'until' => null,
            ]);
        }
    }
}
