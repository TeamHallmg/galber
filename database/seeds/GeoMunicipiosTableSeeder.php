<?php

use Illuminate\Database\Seeder;

class GeoMunicipiosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('geo_municipios')->insert([
			'id' => '1',
            'nombre' => 'Chahal',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
		]);
        DB::table('geo_municipios')->insert([
			'id' => '2',
            'nombre' => 'Chisec',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
		]);
        DB::table('geo_municipios')->insert([
			'id' => '3',
            'nombre' => 'Cobán',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
		]);
        DB::table('geo_municipios')->insert([
			'id' => '4',
            'nombre' => 'Fray Bartolomé de las Casas',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
		]);
        DB::table('geo_municipios')->insert([
			'id' => '5',
            'nombre' => 'La Tinta',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
		]);
        DB::table('geo_municipios')->insert([
			'id' => '6',
            'nombre' => 'Lanquín',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
		]);
        DB::table('geo_municipios')->insert([
			'id' => '7',
            'nombre' => 'Panzós',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
		]);
        DB::table('geo_municipios')->insert([
			'id' => '8',
            'nombre' => 'Raxruhá',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
		]);
        DB::table('geo_municipios')->insert([
			'id' => '9',
            'nombre' => 'San Cristóbal Verapaz',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
		]);
        DB::table('geo_municipios')->insert([
			'id' => '10',
            'nombre' => 'San Juan Chamelco',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
		]);
        DB::table('geo_municipios')->insert([
			'id' => '11',
            'nombre' => 'San Pedro Carchá',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
		]);
        DB::table('geo_municipios')->insert([
			'id' => '12',
            'nombre' => 'Santa Cruz Verapaz',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
		]);
        DB::table('geo_municipios')->insert([
			'id' => '13',
            'nombre' => 'Santa María Cahabón',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
		]);
        DB::table('geo_municipios')->insert([
			'id' => '14',
            'nombre' => 'Senahú',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
		]);
        DB::table('geo_municipios')->insert([
			'id' => '15',
            'nombre' => 'Tamahú',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
		]);
        DB::table('geo_municipios')->insert([
			'id' => '16',
            'nombre' => 'Tactic',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
		]);
        DB::table('geo_municipios')->insert([
			'id' => '17',
            'nombre' => 'Tucurú',
            'id_geo_departamento' => '1',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '18',
            'nombre' => 'Cubulco',
            'id_geo_departamento' => '2',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '19',
            'nombre' => 'Granados',
            'id_geo_departamento' => '2',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '20',
            'nombre' => 'Purulhá',
            'id_geo_departamento' => '2',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '21',
            'nombre' => 'Rabinal',
            'id_geo_departamento' => '2',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '22',
            'nombre' => 'Salamá',
            'id_geo_departamento' => '2',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '23',
            'nombre' => 'San Jerónimo',
            'id_geo_departamento' => '2',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '24',
            'nombre' => 'San Miguel Chicaj',
            'id_geo_departamento' => '2',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '25',
            'nombre' => 'Santa Cruz el Chol',
            'id_geo_departamento' => '2',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '26',
            'nombre' => 'Acatenango',
            'id_geo_departamento' => '3',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '27',
            'nombre' => 'Chimaltenango',
            'id_geo_departamento' => '3',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '28',
            'nombre' => 'El Tejar',
            'id_geo_departamento' => '3',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '29',
            'nombre' => 'Parramos',
            'id_geo_departamento' => '3',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '30',
            'nombre' => 'Patzicía',
            'id_geo_departamento' => '3',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '31',
            'nombre' => 'Patzún',
            'id_geo_departamento' => '3',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '32',
            'nombre' => 'Pochuta',
            'id_geo_departamento' => '3',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '33',
            'nombre' => 'San Andrés Itzapa',
            'id_geo_departamento' => '3',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '34',
            'nombre' => 'San José Poaquíl',
            'id_geo_departamento' => '3',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '35',
            'nombre' => 'San Juan Comalapa',
            'id_geo_departamento' => '3',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '36',
            'nombre' => 'San Martín Jilotepeque',
            'id_geo_departamento' => '3',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '37',
            'nombre' => 'Santa Apolonia',
            'id_geo_departamento' => '3',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '38',
            'nombre' => 'Santa Cruz Balanyá',
            'id_geo_departamento' => '3',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '39',
            'nombre' => 'Tecpán',
            'id_geo_departamento' => '3',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '40',
            'nombre' => 'Yepocapa',
            'id_geo_departamento' => '3',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '41',
            'nombre' => 'Zaragoza',
            'id_geo_departamento' => '3',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '42',
            'nombre' => 'Camotán',
            'id_geo_departamento' => '4',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '43',
            'nombre' => 'Chiquimula',
            'id_geo_departamento' => '4',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '44',
            'nombre' => 'Concepción Las Minas',
            'id_geo_departamento' => '4',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '45',
            'nombre' => 'Esquipulas',
            'id_geo_departamento' => '4',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '46',
            'nombre' => 'Ipala',
            'id_geo_departamento' => '4',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '47',
            'nombre' => 'Jocotán',
            'id_geo_departamento' => '4',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '48',
            'nombre' => 'Olopa',
            'id_geo_departamento' => '4',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '49',
            'nombre' => 'Quezaltepeque',
            'id_geo_departamento' => '4',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '50',
            'nombre' => 'San Jacinto',
            'id_geo_departamento' => '4',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '51',
            'nombre' => 'San José la Arada',
            'id_geo_departamento' => '4',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '52',
            'nombre' => 'San Juan Ermita',
            'id_geo_departamento' => '4',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '53',
            'nombre' => 'El Jícaro',
            'id_geo_departamento' => '5',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '54',
            'nombre' => 'Guastatoya',
            'id_geo_departamento' => '5',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '55',
            'nombre' => 'Morazán',
            'id_geo_departamento' => '5',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '56',
            'nombre' => 'San Agustín Acasaguastlán',
            'id_geo_departamento' => '5',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '57',
            'nombre' => 'San Antonio La Paz',
            'id_geo_departamento' => '5',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '58',
            'nombre' => 'San Cristóbal Acasaguastlán',
            'id_geo_departamento' => '5',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '59',
            'nombre' => 'Sanarate',
            'id_geo_departamento' => '5',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '60',
            'nombre' => 'Sansare',
            'id_geo_departamento' => '5',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '61',
            'nombre' => 'Escuintla',
            'id_geo_departamento' => '6',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '62',
            'nombre' => 'Guanagazapa',
            'id_geo_departamento' => '6',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '63',
            'nombre' => 'Iztapa',
            'id_geo_departamento' => '6',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '64',
            'nombre' => 'La Democracia',
            'id_geo_departamento' => '6',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '65',
            'nombre' => 'La Gomera',
            'id_geo_departamento' => '6',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '66',
            'nombre' => 'Masagua',
            'id_geo_departamento' => '6',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '67',
            'nombre' => 'Nueva Concepción',
            'id_geo_departamento' => '6',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '68',
            'nombre' => 'Palín',
            'id_geo_departamento' => '6',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '69',
            'nombre' => 'San José',
            'id_geo_departamento' => '6',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '70',
            'nombre' => 'San Vicente Pacaya',
            'id_geo_departamento' => '6',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '71',
            'nombre' => 'Santa Lucía Cotzumalguapa',
            'id_geo_departamento' => '6',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '72',
            'nombre' => 'Siquinalá',
            'id_geo_departamento' => '6',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '73',
            'nombre' => 'Tiquisate',
            'id_geo_departamento' => '6',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '74',
            'nombre' => 'Amatitlán',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '75',
            'nombre' => 'Chinautla',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '76',
            'nombre' => 'Chuarrancho',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '77',
            'nombre' => 'Guatemala',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '78',
            'nombre' => 'Fraijanes',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '79',
            'nombre' => 'Mixco',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '80',
            'nombre' => 'Palencia',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '81',
            'nombre' => 'San José del Golfo',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '82',
            'nombre' => 'San José Pinula',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '83',
            'nombre' => 'San Juan Sacatepéquez',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '84',
            'nombre' => 'San Miguel Petapa',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '85',
            'nombre' => 'San Pedro Ayampuc',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '86',
            'nombre' => 'San Pedro Sacatepéquez',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '87',
            'nombre' => 'San Raymundo',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '88',
            'nombre' => 'Santa Catarina Pinula',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '89',
            'nombre' => 'Villa Canales',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '90',
            'nombre' => 'Villa Nueva',
            'id_geo_departamento' => '7',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '91',
            'nombre' => 'Aguacatán',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '92',
            'nombre' => 'Chiantla',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '93',
            'nombre' => 'Colotenango',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '94',
            'nombre' => 'Concepción Huista',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '95',
            'nombre' => 'Cuilco',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '96',
            'nombre' => 'Huehuetenango',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '97',
            'nombre' => 'Jacaltenango',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '98',
            'nombre' => 'La Democracia',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '99',
            'nombre' => 'La Libertad',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '100',
            'nombre' => 'Malacatancito',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '101',
            'nombre' => 'Nentón',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '102',
            'nombre' => 'San Antonio Huista',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '103',
            'nombre' => 'San Gaspar Ixchil',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '104',
            'nombre' => 'San Ildefonso Ixtahuacán',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '105',
            'nombre' => 'San Juan Atitán',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '106',
            'nombre' => 'San Juan Ixcoy',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '107',
            'nombre' => 'San Mateo Ixtatán',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '108',
            'nombre' => 'San Miguel Acatán',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '109',
            'nombre' => 'San Pedro Nécta',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '110',
            'nombre' => 'San Pedro Soloma',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '111',
            'nombre' => 'San Rafael La Independencia',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '112',
            'nombre' => 'San Rafael Pétzal',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '113',
            'nombre' => 'San Sebastián Coatán',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '114',
            'nombre' => 'San Sebastián Huehuetenango',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '115',
            'nombre' => 'Santa Ana Huista',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '116',
            'nombre' => 'Santa Bárbara',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '117',
            'nombre' => 'Santa Cruz Barillas',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '118',
            'nombre' => 'Santa Eulalia',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '119',
            'nombre' => 'Santiago Chimaltenango',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '120',
            'nombre' => 'Tectitán',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '121',
            'nombre' => 'Todos Santos Cuchumatán',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '122',
            'nombre' => 'Unión Cantinil',
            'id_geo_departamento' => '8',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '123',
            'nombre' => 'El Estor',
            'id_geo_departamento' => '9',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '124',
            'nombre' => 'Livingston',
            'id_geo_departamento' => '9',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '125',
            'nombre' => 'Los Amates',
            'id_geo_departamento' => '9',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '126',
            'nombre' => 'Morales',
            'id_geo_departamento' => '9',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '127',
            'nombre' => 'Puerto Barrios',
            'id_geo_departamento' => '9',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '128',
            'nombre' => 'Jalapa',
            'id_geo_departamento' => '10',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '129',
            'nombre' => 'Mataquescuintla',
            'id_geo_departamento' => '10',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '130',
            'nombre' => 'Monjas',
            'id_geo_departamento' => '10',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '131',
            'nombre' => 'San Carlos Alzatate',
            'id_geo_departamento' => '10',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '132',
            'nombre' => 'San Luis Jilotepeque',
            'id_geo_departamento' => '10',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '133',
            'nombre' => 'San Manuel Chaparrón',
            'id_geo_departamento' => '10',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '134',
            'nombre' => 'San Pedro Pinula',
            'id_geo_departamento' => '10',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '135',
            'nombre' => 'Agua Blanca',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '136',
            'nombre' => 'Asunción Mita',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '137',
            'nombre' => 'Atescatempa',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '138',
            'nombre' => 'Comapa',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '139',
            'nombre' => 'Conguaco',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '140',
            'nombre' => 'El Adelanto',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '141',
            'nombre' => 'El Progreso',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '142',
            'nombre' => 'Jalpatagua',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '143',
            'nombre' => 'Jerez',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '144',
            'nombre' => 'Jutiapa',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '145',
            'nombre' => 'Moyuta',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '146',
            'nombre' => 'Pasaco',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '147',
            'nombre' => 'Quesada',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '148',
            'nombre' => 'San José Acatempa',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '149',
            'nombre' => 'Santa Catarina Mita',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '150',
            'nombre' => 'Yupiltepeque',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '151',
            'nombre' => 'Zapotitlán',
            'id_geo_departamento' => '11',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '152',
            'nombre' => 'Dolores',
            'id_geo_departamento' => '12',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '153',
            'nombre' => 'El Chal',
            'id_geo_departamento' => '12',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '154',
            'nombre' => 'Ciudad Flores',
            'id_geo_departamento' => '12',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '155',
            'nombre' => 'La Libertad',
            'id_geo_departamento' => '12',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '156',
            'nombre' => 'Las Cruces',
            'id_geo_departamento' => '12',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '157',
            'nombre' => 'Melchor de Mencos',
            'id_geo_departamento' => '12',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '158',
            'nombre' => 'Poptún',
            'id_geo_departamento' => '12',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '159',
            'nombre' => 'San Andrés',
            'id_geo_departamento' => '12',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '160',
            'nombre' => 'San Benito',
            'id_geo_departamento' => '12',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '161',
            'nombre' => 'San Francisco',
            'id_geo_departamento' => '12',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '162',
            'nombre' => 'San José',
            'id_geo_departamento' => '12',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '163',
            'nombre' => 'San Luis',
            'id_geo_departamento' => '12',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '164',
            'nombre' => 'Santa Ana',
            'id_geo_departamento' => '12',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '165',
            'nombre' => 'Sayaxché',
            'id_geo_departamento' => '12',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '166',
            'nombre' => 'Almolonga',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '167',
            'nombre' => 'Cabricán',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '168',
            'nombre' => 'Cajolá',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '169',
            'nombre' => 'Cantel',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '170',
            'nombre' => 'Coatepeque',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '171',
            'nombre' => 'Colomba Costa Cuca',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '172',
            'nombre' => 'Concepción Chiquirichapa',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '173',
            'nombre' => 'El Palmar',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '174',
            'nombre' => 'Flores Costa Cuca',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '175',
            'nombre' => 'Génova',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '176',
            'nombre' => 'Huitán',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '177',
            'nombre' => 'La Esperanza',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '178',
            'nombre' => 'Olintepeque',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '179',
            'nombre' => 'Palestina de Los Altos',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '180',
            'nombre' => 'Quetzaltenango',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '181',
            'nombre' => 'Salcajá',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '182',
            'nombre' => 'San Carlos Sija',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '183',
            'nombre' => 'San Francisco La Unión',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '184',
            'nombre' => 'San Juan Ostuncalco',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '185',
            'nombre' => 'San Martín Sacatepéquez',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '186',
            'nombre' => 'San Mateo',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '187',
            'nombre' => 'San Miguel Sigüilá',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '188',
            'nombre' => 'Sibilia',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '189',
            'nombre' => 'Zunil',
            'id_geo_departamento' => '13',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '190',
            'nombre' => 'Canillá',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '191',
            'nombre' => 'Chajul',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '192',
            'nombre' => 'Chicamán',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '193',
            'nombre' => 'Chiché',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '194',
            'nombre' => 'Chichicastenango',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '195',
            'nombre' => 'Chinique',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '196',
            'nombre' => 'Cunén',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '197',
            'nombre' => 'Ixcán Playa Grande',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '198',
            'nombre' => 'Joyabaj',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '199',
            'nombre' => 'Nebaj',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '200',
            'nombre' => 'Pachalum',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '201',
            'nombre' => 'Patzité',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '202',
            'nombre' => 'Sacapulas',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '203',
            'nombre' => 'San Andrés Sajcabajá',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '204',
            'nombre' => 'San Antonio Ilotenango',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '205',
            'nombre' => 'San Bartolomé Jocotenango',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '206',
            'nombre' => 'San Juan Cotzal',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '207',
            'nombre' => 'San Pedro Jocopilas',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '208',
            'nombre' => 'Santa Cruz del Quiché',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '209',
            'nombre' => 'Uspantán',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '210',
            'nombre' => 'Zacualpa',
            'id_geo_departamento' => '14',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '211',
            'nombre' => 'Champerico',
            'id_geo_departamento' => '15',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '212',
            'nombre' => 'El Asintal',
            'id_geo_departamento' => '15',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '213',
            'nombre' => 'Nuevo San Carlos',
            'id_geo_departamento' => '15',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '214',
            'nombre' => 'Retalhuleu',
            'id_geo_departamento' => '15',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '215',
            'nombre' => 'San Andrés Villa Seca',
            'id_geo_departamento' => '15',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '216',
            'nombre' => 'San Felipe Reu',
            'id_geo_departamento' => '15',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '217',
            'nombre' => 'San Martín Zapotitlán',
            'id_geo_departamento' => '15',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '218',
            'nombre' => 'San Sebastián',
            'id_geo_departamento' => '15',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '219',
            'nombre' => 'Santa Cruz Muluá',
            'id_geo_departamento' => '15',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '220',
            'nombre' => 'Alotenango',
            'id_geo_departamento' => '16',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '221',
            'nombre' => 'Ciudad Vieja',
            'id_geo_departamento' => '16',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '222',
            'nombre' => 'Jocotenango',
            'id_geo_departamento' => '16',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '223',
            'nombre' => 'Antigua Guatemala',
            'id_geo_departamento' => '16',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '224',
            'nombre' => 'Magdalena Milpas Altas',
            'id_geo_departamento' => '16',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '225',
            'nombre' => 'Pastores',
            'id_geo_departamento' => '16',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '226',
            'nombre' => 'San Antonio Aguas Calientes',
            'id_geo_departamento' => '16',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '227',
            'nombre' => 'San Bartolomé Milpas Altas',
            'id_geo_departamento' => '16',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '228',
            'nombre' => 'San Lucas Sacatepéquez',
            'id_geo_departamento' => '16',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '229',
            'nombre' => 'San Miguel Dueñas',
            'id_geo_departamento' => '16',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '230',
            'nombre' => 'Santa Catarina Barahona',
            'id_geo_departamento' => '16',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '231',
            'nombre' => 'Santa Lucía Milpas Altas',
            'id_geo_departamento' => '16',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '232',
            'nombre' => 'Santa María de Jesús',
            'id_geo_departamento' => '16',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '233',
            'nombre' => 'Santiago Sacatepéquez',
            'id_geo_departamento' => '16',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '234',
            'nombre' => 'Santo Domingo Xenacoj',
            'id_geo_departamento' => '16',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '235',
            'nombre' => 'Sumpango',
            'id_geo_departamento' => '16',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '236',
            'nombre' => 'Ayutla',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '237',
            'nombre' => 'Catarina',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '238',
            'nombre' => 'Comitancillo',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '239',
            'nombre' => 'Concepción Tutuapa',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '240',
            'nombre' => 'El Quetzal',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '241',
            'nombre' => 'El Tumbador',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '242',
            'nombre' => 'Esquipulas Palo Gordo',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '243',
            'nombre' => 'Ixchiguán',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '244',
            'nombre' => 'La Blanca',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '245',
            'nombre' => 'La Reforma',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '246',
            'nombre' => 'Malacatán',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '247',
            'nombre' => 'Nuevo Progreso',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '248',
            'nombre' => 'Ocós',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '249',
            'nombre' => 'Pajapita',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '250',
            'nombre' => 'Río Blanco',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '251',
            'nombre' => 'San Antonio Sacatepéquez',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '252',
            'nombre' => 'San Cristóbal Cucho',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '253',
            'nombre' => 'San José El Rodeo',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '254',
            'nombre' => 'San José Ojetenam',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '255',
            'nombre' => 'San Lorenzo',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '256',
            'nombre' => 'San Marcos',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '257',
            'nombre' => 'San Miguel Ixtahuacán',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '258',
            'nombre' => 'San Pablo',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '259',
            'nombre' => 'San Pedro Sacatepéquez',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '260',
            'nombre' => 'San Rafael Pie de la Cuesta',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '261',
            'nombre' => 'Sibinal',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '262',
            'nombre' => 'Sipacapa',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '263',
            'nombre' => 'Tacaná',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '264',
            'nombre' => 'Tajumulco',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '265',
            'nombre' => 'Tejutla',
            'id_geo_departamento' => '17',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '266',
            'nombre' => 'Barberena',
            'id_geo_departamento' => '18',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '267',
            'nombre' => 'Casillas',
            'id_geo_departamento' => '18',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '268',
            'nombre' => 'Chiquimulilla',
            'id_geo_departamento' => '18',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '269',
            'nombre' => 'Cuilapa',
            'id_geo_departamento' => '18',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '270',
            'nombre' => 'Guazacapán',
            'id_geo_departamento' => '18',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '271',
            'nombre' => 'Nueva Santa Rosa',
            'id_geo_departamento' => '18',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '272',
            'nombre' => 'Oratorio',
            'id_geo_departamento' => '18',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '273',
            'nombre' => 'Pueblo Nuevo Viñas',
            'id_geo_departamento' => '18',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '274',
            'nombre' => 'San Juan Tecuaco',
            'id_geo_departamento' => '18',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '275',
            'nombre' => 'San Rafael las Flores',
            'id_geo_departamento' => '18',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '276',
            'nombre' => 'Santa Cruz Naranjo',
            'id_geo_departamento' => '18',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '277',
            'nombre' => 'Santa María Ixhuatán',
            'id_geo_departamento' => '18',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '278',
            'nombre' => 'Santa Rosa de Lima',
            'id_geo_departamento' => '18',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '279',
            'nombre' => 'Taxisco',
            'id_geo_departamento' => '18',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '280',
            'nombre' => 'Concepción',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '281',
            'nombre' => 'Nahualá',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '282',
            'nombre' => 'Panajachel',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '283',
            'nombre' => 'San Andrés Semetabaj',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '284',
            'nombre' => 'San Antonio Palopó',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '285',
            'nombre' => 'San José Chacayá',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '286',
            'nombre' => 'San Juan La Laguna',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '287',
            'nombre' => 'San Lucas Tolimán',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '288',
            'nombre' => 'San Marcos La Laguna',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '289',
            'nombre' => 'San Pablo La Laguna',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '290',
            'nombre' => 'San Pedro La Laguna',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '291',
            'nombre' => 'Santa Catarina Ixtahuacán',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '292',
            'nombre' => 'Santa Catarina Palopó',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '293',
            'nombre' => 'Santa Clara La Laguna',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '294',
            'nombre' => 'Santa Cruz La Laguna',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '295',
            'nombre' => 'Santa Lucía Utatlán',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '296',
            'nombre' => 'Santa María Visitación',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '297',
            'nombre' => 'Santiago Atitlán',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '298',
            'nombre' => 'Sololá',
            'id_geo_departamento' => '19',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '299',
            'nombre' => 'Chicacao',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '300',
            'nombre' => 'Cuyotenango',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '301',
            'nombre' => 'Mazatenango',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '302',
            'nombre' => 'Patulul',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '303',
            'nombre' => 'Pueblo Nuevo',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '304',
            'nombre' => 'Río Bravo',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '305',
            'nombre' => 'Samayac',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '306',
            'nombre' => 'San Antonio Suchitepéquez',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '307',
            'nombre' => 'San Bernardino',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '308',
            'nombre' => 'San Francisco Zapotitlán',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '309',
            'nombre' => 'San Gabriel',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '310',
            'nombre' => 'San José El Idolo',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '311',
            'nombre' => 'San José La Maquina',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '312',
            'nombre' => 'San Juan Bautista',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '313',
            'nombre' => 'San Lorenzo',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '314',
            'nombre' => 'San Miguel Panán',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '315',
            'nombre' => 'San Pablo Jocopilas',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '316',
            'nombre' => 'Santa Bárbara',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '317',
            'nombre' => 'Santo Domingo Suchitepéquez',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '318',
            'nombre' => 'Santo Tomás La Unión',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '319',
            'nombre' => 'Zunilito',
            'id_geo_departamento' => '20',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '320',
            'nombre' => 'Momostenango',
            'id_geo_departamento' => '21',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '321',
            'nombre' => 'San Andrés Xecul',
            'id_geo_departamento' => '21',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '322',
            'nombre' => 'San Bartolo',
            'id_geo_departamento' => '21',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '323',
            'nombre' => 'San Cristóbal Totonicapán',
            'id_geo_departamento' => '21',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '324',
            'nombre' => 'San Francisco El Alto',
            'id_geo_departamento' => '21',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '325',
            'nombre' => 'Santa Lucía La Reforma',
            'id_geo_departamento' => '21',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '326',
            'nombre' => 'Santa María Chiquimula',
            'id_geo_departamento' => '21',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '327',
            'nombre' => 'Totonicapán',
            'id_geo_departamento' => '21',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '328',
            'nombre' => 'Cabañas',
            'id_geo_departamento' => '22',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '329',
            'nombre' => 'Estanzuela',
            'id_geo_departamento' => '22',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '330',
            'nombre' => 'Gualán',
            'id_geo_departamento' => '22',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '331',
            'nombre' => 'Huité',
            'id_geo_departamento' => '22',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '332',
            'nombre' => 'La Unión',
            'id_geo_departamento' => '22',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '333',
            'nombre' => 'Río Hondo',
            'id_geo_departamento' => '22',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '334',
            'nombre' => 'San Diego',
            'id_geo_departamento' => '22',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '335',
            'nombre' => 'San Jorge',
            'id_geo_departamento' => '22',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '336',
            'nombre' => 'Teculután',
            'id_geo_departamento' => '22',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '337',
            'nombre' => 'Usumatlán',
            'id_geo_departamento' => '22',
            'nombre_guatecompras' => ''
        ]);
        DB::table('geo_municipios')->insert([
			'id' => '338',
            'nombre' => 'Zacapa',
            'id_geo_departamento' => '22',
            'nombre_guatecompras' => ''
        ]);    
    }
}
