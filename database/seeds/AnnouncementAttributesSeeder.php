<?php

use Illuminate\Database\Seeder;
use App\Models\Announcement\AnnouncementAttribute;

class AnnouncementAttributesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AnnouncementAttribute::firstOrCreate([
			'name' => 'Título',
			'form_name' => 'title',
			'attr_view' => 'text'
				
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Descripción',
			'form_name' => 'description',
			'attr_view' => 'textarea'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Enlace',
			'form_name' => 'link',
			'attr_view' => 'text'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Imagen',
			'form_name' => 'image',
			'attr_view' => 'file'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Autor',
			'form_name' => 'author',
			'attr_view' => 'text'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Etiqueta',
			'form_name' => 'tag',
			'attr_view' => 'textarea'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Documento',
			'form_name' => 'document',
			'attr_view' => 'file'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Galería',
			'form_name' => 'gallery',
			'attr_view' => 'multi-file'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Fecha',
			'form_name' => 'date',
			'attr_view' => 'date'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Lugar',
			'form_name' => 'place',
			'attr_view' => 'text'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Descuento',
			'form_name' => 'discount',
			'attr_view' => 'text'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Vigencia',
			'form_name' => 'validity',
			'attr_view' => 'date'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Observaciones',
			'form_name' => 'observations',
			'attr_view' => 'textarea'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Concepto',
			'form_name' => 'concept',
			'attr_view' => 'text'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Prestación',
			'form_name' => 'benefit',
			'attr_view' => 'textarea'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Política / Procedimiento',
			'form_name' => 'politica_procedimiento',
			'attr_view' => 'text'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Clasificación',
			'form_name' => 'clasificacion',
			'attr_view' => 'text'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Proceso',
			'form_name' => 'proceso',
			'attr_view' => 'text'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Responsable',
			'form_name' => 'responsable',
			'attr_view' => 'select'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Campus',
			'form_name' => 'campus',
			'attr_view' => 'text'
		]);
		AnnouncementAttribute::firstOrCreate([
			'name' => 'Archivo / Documento',
			'form_name' => 'file',
			'attr_view' => 'file'
		]);
    }
}