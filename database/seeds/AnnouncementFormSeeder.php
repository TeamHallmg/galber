<?php

use Illuminate\Database\Seeder;
use App\Models\Announcement\AnnouncementForm;
use App\Models\Announcement\AnnouncementType;
use App\Models\Announcement\AnnouncementAttribute;

class AnnouncementFormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = AnnouncementType::get()->pluck('id', 'name')->toArray();
        $attributes = AnnouncementAttribute::get()->pluck('id', 'form_name')->toArray();

        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['banner'],
            'announcement_attribute_id' => $attributes['link'],
        ],[
            'required' => 0,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['banner'],
            'announcement_attribute_id' => $attributes['image'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['panels'],
                'announcement_attribute_id' => $attributes['title']
            ],
            [ 'required' => 0 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['panels'],
                'announcement_attribute_id' => $attributes['description']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['prestaciones_sindicalizados'],
                'announcement_attribute_id' => $attributes['description']
            ],
            [ 'required' => 0 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['prestaciones_confianza'],
                'announcement_attribute_id' => $attributes['description']
            ],
            [ 'required' => 0 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['carrousel'],
                'announcement_attribute_id' => $attributes['link']
            ],
            [ 'required' => 0 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['carrousel'],
                'announcement_attribute_id' => $attributes['image']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['mosaico'],
                'announcement_attribute_id' => $attributes['link']
            ],
            [ 'required' => 0 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['mosaico'],
                'announcement_attribute_id' => $attributes['image']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['panels'],
                'announcement_attribute_id' => $attributes['link']
            ],
            [ 'required' => 0 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['panels'],
                'announcement_attribute_id' => $attributes['image']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['prestaciones_sindicalizados'],
                'announcement_attribute_id' => $attributes['image']
            ],
            [ 'required' => 0 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['prestaciones_confianza'],
                'announcement_attribute_id' => $attributes['image']
            ],
            [ 'required' => 0 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['convenios'],
                'announcement_attribute_id' => $attributes['image']
            ],
            [ 'required' => 0 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['convenios'],
                'announcement_attribute_id' => $attributes['discount']
            ],
            [ 'required' => 0 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['convenios'],
                'announcement_attribute_id' => $attributes['validity']
            ],
            [ 'required' => 0 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['convenios'],
                'announcement_attribute_id' => $attributes['observations']
            ],
            [ 'required' => 0 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['prestaciones_sindicalizados'],
                'announcement_attribute_id' => $attributes['concept']
            ],
            [ 'required' => 0 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['prestaciones_confianza'],
                'announcement_attribute_id' => $attributes['concept']
            ],
            [ 'required' => 0 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['prestaciones_sindicalizados'],
                'announcement_attribute_id' => $attributes['benefit']
            ],
            [ 'required' => 0 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['prestaciones_confianza'],
                'announcement_attribute_id' => $attributes['benefit']
            ],
            [ 'required' => 0 ]
        );
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['convenios'],
            'announcement_attribute_id' => $attributes['image']
        ],[
            'required' => 0,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['convenios'],
            'announcement_attribute_id' => $attributes['discount']
        ],[
            'required' => 0,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['convenios'],
            'announcement_attribute_id' => $attributes['validity']
        ],[
            'required' => 0,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['convenios'],
            'announcement_attribute_id' => $attributes['observations']
        ],[
            'required' => 0,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['operaciones'],
            'announcement_attribute_id' => $attributes['title']
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['operaciones'],
            'announcement_attribute_id' => $attributes['file']
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['mosaico_espacios'],
            'announcement_attribute_id' => $attributes['link'],
        ],[
            'required' => 0,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['mosaico_espacios'],
            'announcement_attribute_id' => $attributes['image'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['mosaico_espacios'],
            'announcement_attribute_id' => $attributes['title'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['image'],
            'announcement_attribute_id' => $attributes['image'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['dos_columnas'],
            'announcement_attribute_id' => $attributes['title'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['dos_columnas'],
            'announcement_attribute_id' => $attributes['description'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['imagenes_organigrama'],
            'announcement_attribute_id' => $attributes['image'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['imagenes_organigrama'],
            'announcement_attribute_id' => $attributes['title'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['notificaciones'],
            'announcement_attribute_id' => $attributes['image'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['notificaciones'],
            'announcement_attribute_id' => $attributes['title'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['notificaciones'],
            'announcement_attribute_id' => $attributes['description'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['gallery'],
            'announcement_attribute_id' => $attributes['gallery'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['gallery'],
            'announcement_attribute_id' => $attributes['title'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['gallery'],
            'announcement_attribute_id' => $attributes['date'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['gallery'],
            'announcement_attribute_id' => $attributes['place'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['festividades'],
            'announcement_attribute_id' => $attributes['image'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['festividades'],
            'announcement_attribute_id' => $attributes['title'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['festividades'],
            'announcement_attribute_id' => $attributes['date'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['festividades'],
            'announcement_attribute_id' => $attributes['description'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['festividades'],
            'announcement_attribute_id' => $attributes['campus'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['prestaciones'],
                'announcement_attribute_id' => $attributes['description']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['prestaciones'],
                'announcement_attribute_id' => $attributes['image']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['prestaciones'],
                'announcement_attribute_id' => $attributes['concept']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['prestaciones'],
                'announcement_attribute_id' => $attributes['benefit']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['aseguramiento_calidad_formatos'],
                'announcement_attribute_id' => $attributes['title']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['aseguramiento_calidad_formatos'],
                'announcement_attribute_id' => $attributes['file']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['tabla_programas'],
                'announcement_attribute_id' => $attributes['title']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['tabla_programas'],
                'announcement_attribute_id' => $attributes['file']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['tabla_diagramas'],
                'announcement_attribute_id' => $attributes['title']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['tabla_diagramas'],
                'announcement_attribute_id' => $attributes['file']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['tabla_diagramas'],
                'announcement_attribute_id' => $attributes['file']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['formatos_general'],
                'announcement_attribute_id' => $attributes['title']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['formatos_general'],
                'announcement_attribute_id' => $attributes['file']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['reconocimientos'],
                'announcement_attribute_id' => $attributes['title']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['reconocimientos'],
                'announcement_attribute_id' => $attributes['image']
            ],
            [ 'required' => 1 ]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['reconocimientos'],
                'announcement_attribute_id' => $attributes['description']
            ],
            [ 'required' => 1 ]
        );

        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['tabla_anuncios'],
                'announcement_attribute_id' => $attributes['title']
            ],[ 'required' => 1]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['tabla_anuncios'],
                'announcement_attribute_id' => $attributes['description']
            ],[ 'required' => 1]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['tabla_anuncios'],
                'announcement_attribute_id' => $attributes['link']
            ],[ 'required' => 0]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['tabla_anuncios'],
                'announcement_attribute_id' => $attributes['image']
            ],[ 'required' => 1]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['tabla_anuncios'],
                'announcement_attribute_id' => $attributes['author']
            ],[ 'required' => 0]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['tabla_anuncios'],
                'announcement_attribute_id' => $attributes['tag']
            ],[ 'required' => 0]
        );

        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['columna_anuncios'],
                'announcement_attribute_id' => $attributes['image'],
            ],[ 'required' => 1]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['columna_anuncios'],
                'announcement_attribute_id' => $attributes['link'],
            ],[ 'required' => 0]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['image'],
                'announcement_attribute_id' => $attributes['image'],
            ],[ 'required' => 1]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['aseguramiento_calidad_formatos'],
                'announcement_attribute_id' => $attributes['title'],
            ],[ 'required' => 1]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['aseguramiento_calidad_formatos'],
                'announcement_attribute_id' => $attributes['file'],
            ],[ 'required' => 1]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['tabla_programas'],
                'announcement_attribute_id' => $attributes['title'],
            ],[ 'required' => 1]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['tabla_programas'],
                'announcement_attribute_id' => $attributes['file'],
            ],[ 'required' => 1]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['tabla_diagramas'],
                'announcement_attribute_id' => $attributes['title'],
            ],[ 'required' => 1]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['tabla_diagramas'],
                'announcement_attribute_id' => $attributes['file'],
            ],[ 'required' => 1]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['formatos_general'],
                'announcement_attribute_id' => $attributes['title'],
            ],[ 'required' => 1]
        );
        AnnouncementForm::updateOrCreate(
            [
                'announcement_type_id' => $types['formatos_general'],
                'announcement_attribute_id' => $attributes['file'],
            ],[ 'required' => 1]
        );
    }
}