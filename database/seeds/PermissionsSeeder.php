<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Permission::updateOrCreate([
			'action' => 'see_vacancies',
		],[
			'name' => 'Ver Vacantes',
			'module' => 'Vacantes',
			'description' => 'Ver Vacantes',
		]);
		Permission::updateOrCreate([
			'action' => 'see_postulates',
		],[
			'name' => 'Ver Postulados',
			'module' => 'Vacantes',
			'description' => 'Ver Postulados',
		]);
		Permission::updateOrCreate([
			'action' => 'generate_vacant',
		],[
			'name' => 'Generar Vacante',
			'module' => 'Vacantes',
			'description' => 'Generar Vacante',
		]);
		Permission::updateOrCreate([
			'action' => 'add_remove_recruiter',
		],[
			'name' => 'Agregar/Eliminar Reclutador',
			'module' => 'Vacantes',
			'description' => 'Agregar/Eliminar Reclutador',
		]);
		Permission::updateOrCreate([
			'action' => 'vacancies_admin',
		],[
			'name' => 'Admin',
			'module' => 'Vacantes',
			'description' => 'Admin Vacantes',
		]);



		/* PERMISOS PARA VACACIONES */

		Permission::updateOrCreate([
			'action' => 'admin_clocklog_history',
		],[
			'name' => 'Historico de Checadas',
			'module' => 'Incidencias',
			'description' => 'Ver el catálogo completo de checadas de la empresa',
		]);
		Permission::updateOrCreate([
			'action' => 'admin_incidents_history',
		],[
			'name' => 'Historico de Incidencias',
			'module' => 'Incidencias',
			'description' => 'Ver el catálogo completo de incidencias de la empresa',
		]);
		Permission::updateOrCreate([
			'action' => 'crud_events',
		],[
			'name' => 'Administrar Eventos',
			'module' => 'Incidencias',
			'description' => 'Administrar Eventos de calendario para los trabajadores',
		]);
		Permission::updateOrCreate([
			'action' => 'admin_benefits',
		],[
			'name' => 'Administrar Beneficios/Incidencias',
			'module' => 'Incidencias',
			'description' => 'Administrar el tipo de Incidencias que puede solicitar el empleado',
		]);
		Permission::updateOrCreate([
			'action' => 'admin_schedules',
		],[
			'name' => 'Administar Horarios',
			'module' => 'Incidencias',
			'description' => 'Administrar Horarios y regiones de la empresa',
		]);
		Permission::updateOrCreate([
			'action' => 'admin_benefits_from_job',
		],[
			'name' => 'Administrar Beneficios de un Puesto',
			'module' => 'Incidencias',
			'description' => 'Administar los beneficios que asignados a los puestos',
		]);
    }
}
