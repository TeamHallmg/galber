<?php

use App\Models\Region;
use App\Models\Schedule;
use App\Models\WorkshiftFixed;
use Illuminate\Database\Seeder;

class WorkshiftFixedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$regions = Region::get()->pluck('id', 'name')->toArray();
		$schedules = Schedule::get()->pluck('id', 'shift')->toArray();

		WorkshiftFixed::updateOrCreate(['region_id' => $regions['Central'], 'schedule_id' => $schedules['Central'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['Central'], 'schedule_id' => $schedules['Central'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['Central'], 'schedule_id' => $schedules['Central'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['Central'], 'schedule_id' => $schedules['Central'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['Central'], 'schedule_id' => $schedules['Central'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['Central'], 'schedule_id' => $schedules['Central'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['Central'], 'schedule_id' => $schedules['Central'], 'day' => 'Sun'], ['labor' => false]);
		
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-01'], 'schedule_id' => $schedules['FT2-01'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-01'], 'schedule_id' => $schedules['FT2-01'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-01'], 'schedule_id' => $schedules['FT2-01'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-01'], 'schedule_id' => $schedules['FT2-01'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-01'], 'schedule_id' => $schedules['FT2-01'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-01'], 'schedule_id' => $schedules['FT2-01'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-01'], 'schedule_id' => $schedules['FT2-01'], 'day' => 'Sun'], ['labor' => false]);
	
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-02'], 'schedule_id' => $schedules['FT2-02'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-02'], 'schedule_id' => $schedules['FT2-02'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-02'], 'schedule_id' => $schedules['FT2-02'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-02'], 'schedule_id' => $schedules['FT2-02'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-02'], 'schedule_id' => $schedules['FT2-02'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-02'], 'schedule_id' => $schedules['FT2-02'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-02'], 'schedule_id' => $schedules['FT2-02'], 'day' => 'Sun'], ['labor' => false]);
	
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-03'], 'schedule_id' => $schedules['FT2-03'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-03'], 'schedule_id' => $schedules['FT2-03'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-03'], 'schedule_id' => $schedules['FT2-03'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-03'], 'schedule_id' => $schedules['FT2-03'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-03'], 'schedule_id' => $schedules['FT2-03'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-03'], 'schedule_id' => $schedules['FT2-03'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT2-03'], 'schedule_id' => $schedules['FT2-03'], 'day' => 'Sun'], ['labor' => false]);
	
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT'], 'schedule_id' => $schedules['PT'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT'], 'schedule_id' => $schedules['PT'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT'], 'schedule_id' => $schedules['PT'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT'], 'schedule_id' => $schedules['PT'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT'], 'schedule_id' => $schedules['PT'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT'], 'schedule_id' => $schedules['PT'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT'], 'schedule_id' => $schedules['PT'], 'day' => 'Sun'], ['labor' => false]);
	
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-01'], 'schedule_id' => $schedules['PT2-01'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-01'], 'schedule_id' => $schedules['PT2-01'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-01'], 'schedule_id' => $schedules['PT2-01'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-01'], 'schedule_id' => $schedules['PT2-01'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-01'], 'schedule_id' => $schedules['PT2-01'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-01'], 'schedule_id' => $schedules['PT2-01'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-01'], 'schedule_id' => $schedules['PT2-01'], 'day' => 'Sun'], ['labor' => false]);
	
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-02'], 'schedule_id' => $schedules['PT2-02'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-02'], 'schedule_id' => $schedules['PT2-02'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-02'], 'schedule_id' => $schedules['PT2-02'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-02'], 'schedule_id' => $schedules['PT2-02'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-02'], 'schedule_id' => $schedules['PT2-02'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-02'], 'schedule_id' => $schedules['PT2-02'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-02'], 'schedule_id' => $schedules['PT2-02'], 'day' => 'Sun'], ['labor' => false]);
	
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-03'], 'schedule_id' => $schedules['PT2-03'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-03'], 'schedule_id' => $schedules['PT2-03'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-03'], 'schedule_id' => $schedules['PT2-03'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-03'], 'schedule_id' => $schedules['PT2-03'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-03'], 'schedule_id' => $schedules['PT2-03'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-03'], 'schedule_id' => $schedules['PT2-03'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-03'], 'schedule_id' => $schedules['PT2-03'], 'day' => 'Sun'], ['labor' => false]);
	
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-04'], 'schedule_id' => $schedules['PT2-04'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-04'], 'schedule_id' => $schedules['PT2-04'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-04'], 'schedule_id' => $schedules['PT2-04'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-04'], 'schedule_id' => $schedules['PT2-04'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-04'], 'schedule_id' => $schedules['PT2-04'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-04'], 'schedule_id' => $schedules['PT2-04'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-04'], 'schedule_id' => $schedules['PT2-04'], 'day' => 'Sun'], ['labor' => false]);
	
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-05'], 'schedule_id' => $schedules['PT2-05'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-05'], 'schedule_id' => $schedules['PT2-05'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-05'], 'schedule_id' => $schedules['PT2-05'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-05'], 'schedule_id' => $schedules['PT2-05'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-05'], 'schedule_id' => $schedules['PT2-05'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-05'], 'schedule_id' => $schedules['PT2-05'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-05'], 'schedule_id' => $schedules['PT2-05'], 'day' => 'Sun'], ['labor' => false]);
	
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-06'], 'schedule_id' => $schedules['PT2-06'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-06'], 'schedule_id' => $schedules['PT2-06'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-06'], 'schedule_id' => $schedules['PT2-06'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-06'], 'schedule_id' => $schedules['PT2-06'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-06'], 'schedule_id' => $schedules['PT2-06'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-06'], 'schedule_id' => $schedules['PT2-06'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-06'], 'schedule_id' => $schedules['PT2-06'], 'day' => 'Sun'], ['labor' => false]);
	
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-07'], 'schedule_id' => $schedules['PT2-07'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-07'], 'schedule_id' => $schedules['PT2-07'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-07'], 'schedule_id' => $schedules['PT2-07'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-07'], 'schedule_id' => $schedules['PT2-07'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-07'], 'schedule_id' => $schedules['PT2-07'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-07'], 'schedule_id' => $schedules['PT2-07'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-07'], 'schedule_id' => $schedules['PT2-07'], 'day' => 'Sun'], ['labor' => false]);
	
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-08'], 'schedule_id' => $schedules['PT2-08'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-08'], 'schedule_id' => $schedules['PT2-08'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-08'], 'schedule_id' => $schedules['PT2-08'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-08'], 'schedule_id' => $schedules['PT2-08'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-08'], 'schedule_id' => $schedules['PT2-08'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-08'], 'schedule_id' => $schedules['PT2-08'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-08'], 'schedule_id' => $schedules['PT2-08'], 'day' => 'Sun'], ['labor' => false]);
	
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-09'], 'schedule_id' => $schedules['PT2-09'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-09'], 'schedule_id' => $schedules['PT2-09'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-09'], 'schedule_id' => $schedules['PT2-09'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-09'], 'schedule_id' => $schedules['PT2-09'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-09'], 'schedule_id' => $schedules['PT2-09'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-09'], 'schedule_id' => $schedules['PT2-09'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-09'], 'schedule_id' => $schedules['PT2-09'], 'day' => 'Sun'], ['labor' => false]);
	
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-10'], 'schedule_id' => $schedules['PT2-10'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-10'], 'schedule_id' => $schedules['PT2-10'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-10'], 'schedule_id' => $schedules['PT2-10'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-10'], 'schedule_id' => $schedules['PT2-10'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-10'], 'schedule_id' => $schedules['PT2-10'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-10'], 'schedule_id' => $schedules['PT2-10'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-10'], 'schedule_id' => $schedules['PT2-10'], 'day' => 'Sun'], ['labor' => false]);

		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-11'], 'schedule_id' => $schedules['PT2-11'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-11'], 'schedule_id' => $schedules['PT2-11'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-11'], 'schedule_id' => $schedules['PT2-11'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-11'], 'schedule_id' => $schedules['PT2-11'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-11'], 'schedule_id' => $schedules['PT2-11'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-11'], 'schedule_id' => $schedules['PT2-11'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['PT2-11'], 'schedule_id' => $schedules['PT2-11'], 'day' => 'Sun'], ['labor' => false]);

		WorkshiftFixed::updateOrCreate(['region_id' => $regions['TC'], 'schedule_id' => $schedules['TC'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['TC'], 'schedule_id' => $schedules['TC'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['TC'], 'schedule_id' => $schedules['TC'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['TC'], 'schedule_id' => $schedules['TC'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['TC'], 'schedule_id' => $schedules['TC'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['TC'], 'schedule_id' => $schedules['TC'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['TC'], 'schedule_id' => $schedules['TC'], 'day' => 'Sun'], ['labor' => false]);

		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT1'], 'schedule_id' => $schedules['FT1'], 'day' => 'Mon'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT1'], 'schedule_id' => $schedules['FT1'], 'day' => 'Tue'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT1'], 'schedule_id' => $schedules['FT1'], 'day' => 'Wed'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT1'], 'schedule_id' => $schedules['FT1'], 'day' => 'Thu'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT1'], 'schedule_id' => $schedules['FT1'], 'day' => 'Fri'], ['labor' => true]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT1'], 'schedule_id' => $schedules['FT1'], 'day' => 'Sat'], ['labor' => false]);
		WorkshiftFixed::updateOrCreate(['region_id' => $regions['FT1'], 'schedule_id' => $schedules['FT1'], 'day' => 'Sun'], ['labor' => false]);
    }
}
