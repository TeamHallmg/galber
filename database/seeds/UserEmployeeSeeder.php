<?php

use App\User;
use App\Employee;
use App\Models\Region;
use Illuminate\Database\Seeder;

class UserEmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = Region::get();

        foreach($regions as $region){
            for ($i = 0; $i < 5; $i++) { 
                $u = factory(User::class)->create();
                $employee = factory(Employee::class)->create([
                    'nombre' => $u->first_name,
                    'paterno' => $u->last_name,
                    'correoempresa' => $u->email,
                    'password' => $u->password,
                    'region_id' => $region->id,
                ]);
                $u->employee_id = $employee->id;
                $u->save();
            }
        }
    }
}
