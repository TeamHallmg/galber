<?php

use App\Models\Region;
use Illuminate\Database\Seeder;

class RegionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Region::firstOrCreate(['name' => 'Central', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'FT2-01', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'FT2-02', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'FT2-03', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'PT', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'PT2-01', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'PT2-02', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'PT2-03', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'PT2-04', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'PT2-05', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'PT2-06', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'PT2-07', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'PT2-08', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'PT2-09', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'PT2-10', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'PT2-11', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'TC', 'workshift' => '5x2']);
        Region::firstOrCreate(['name' => 'FT1', 'workshift' => '5x2']);
    }
}
