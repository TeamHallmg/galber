<?php


use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Role::firstOrCreate([
            'name' => 'vacancies_admin',
            'description' => 'Administrador de Vacantes'
        ]);

        Role::firstOrCreate([
            'name' => 'Admin Módulo Incidencias',
            'description' => 'Administrador de las opciones del módulo de Incidencias'
        ]);
    }
}
