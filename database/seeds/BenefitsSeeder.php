<?php

use Illuminate\Database\Seeder;
use App\Models\Vacations\Benefits;

class BenefitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Benefits::updateOrCreate(
            ['name' => 'Tiempo Extra'],
            ['context' => 'super', 'type' => 'time', 'code' => '1004', 'shortname' => 'TE', 'name' => 'Tiempo Extra']
        );
        Benefits::updateOrCreate(
            ['name' => 'Retardos'],
            ['context' => 'super', 'type' => 'time', 'code' => '1004', 'shortname' => 'R']
        );
        Benefits::updateOrCreate(
            ['name' => 'Vacaciones'],
            ['context' => 'user', 'type' => 'pending', 'code' => '1007', 'shortname' => 'V', 'group' => 'benefit']
        );
        Benefits::updateOrCreate(
            ['name' => 'Festivo región'],
            ['context' => 'admin', 'type' => 'day', 'code' => '1009', 'shortname' => 'FR']
        );
        Benefits::updateOrCreate(
            ['name' => 'Home Office'],
            ['context' => 'user', 'type' => 'day', 'code' => '1017', 'shortname' => 'HO', 'group' => 'benefit']
        );
        Benefits::updateOrCreate(
            ['name' => 'Trabajo Comprimido'],
            ['context' => 'user', 'type' => 'day', 'code' => '1018', 'shortname' => 'TC', 'group' => 'benefit']
        );
        Benefits::updateOrCreate(
            ['name' => 'Permiso sin Goce de Sueldo'],
            ['context' => 'user', 'type' => 'day','code' => '1019','shortname' => 'PSG','group' => 'benefit']
        );
        Benefits::updateOrCreate(
            ['name' => 'Permiso con Goce de Sueldo'],
            ['context' => 'user', 'type' => 'day', 'code' => '1019', 'shortname' => 'PGS', 'group' => 'benefit']
        );
        Benefits::updateOrCreate(
            ['name' => 'Permiso extraordinarios para Madres y Padres'],
            ['context' => 'user', 'type' => 'day', 'code' => '1019', 'shortname' => 'PE', 'group' => 'benefit']
        );
    }
}
