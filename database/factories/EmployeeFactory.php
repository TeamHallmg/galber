<?php

use App\Employee;
use Faker\Generator as Faker;

$factory->define(Employee::class, function (Faker $faker) {
    return [
        'idempleado' => $faker->unique()->randomNumber(5),
        'nombre' => $faker->name,
        'paterno' => $faker->lastName,
        'materno' => $faker->lastName,
        'rfc' => $faker->unique()->regexify('[A-Z]{4}[0-9]{6}[A-Z0-9]{3}'),
        'curp' => $faker->unique()->regexify('[A-Z]{4}[0-9]{6}[A-Z0-9]{3}(M|F)[A-Z]{5}[0-9]{2,4}'),
        'nss' => $faker->unique()->regexify('[0-9]{6}-[0-9]{4}-[0-9]'),
        'correoempresa' => $faker->unique()->safeEmail,
        'nacimiento' => $faker->dateTimeThisDecade(),
        'sexo' => $faker->randomElement(['M', 'F']),
        'telefono' => $faker->unique()->tollFreePhoneNumber,
        'celular' => $faker->unique()->phoneNumber,
        'ingreso' => $faker->dateTimeThisDecade(),
        'fechapuesto' => $faker->dateTimeThisDecade(),
        'rol' => 'employee',
        'password' => \Hash::make('admin'),
    ];
});
